﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

namespace Intelligensia.Cutscene
{
    public class CutsceneManager : MonoBehaviour
    {
        public static CutsceneManager instance;

        public string cutsceneName;

        [Header("Test")]
        [SerializeField] private Transform targetPlayPosition = default;
        [SerializeField] private Actor[] testActiveMembers = default;
        [SerializeField] private Actor[] testReserveMembers = default;
        [SerializeField] private bool testPlay = false;

        private CutsceneBase activeCutscene;
        private PlayableDirector activeDirector;

        public CutsceneBase ActiveCutscene => activeCutscene;
        public bool IsPlaying { get; private set; }


        private const string CutsceneInResources = "Cutscene/";


        private void Awake()
        {
            if(instance == null)
            {
                instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
        }


        private void Update()
        {
            if(testPlay)
            {
                PlayCutsceneAt(cutsceneName, targetPlayPosition.position, testActiveMembers, testReserveMembers);

                testPlay = false;
            }
        }

        public void PlayCutscene(string cutsceneName)
        {
            activeCutscene = GetCutscene(cutsceneName);

            if(activeCutscene != null)
            {
                activeCutscene.CutsceneEndedEvent += OnCutsceneEnded;

                activeCutscene.PlayCutscene();

                IsPlaying = true;
            }
        }

        public void PlayCutsceneAt(string cutsceneName, Vector3 position)
        {
            activeCutscene = GetCutscene(cutsceneName);
         
            if (activeCutscene != null)
            {
                activeCutscene.CutsceneEndedEvent += OnCutsceneEnded;
                activeCutscene.transform.position = position;

                activeCutscene.PlayCutscene();

                IsPlaying = true;
            }
        }

        public void PlayCutsceneAt(string cutsceneName, Vector3 position, Actor[] activeActors, Actor[] reserveActors)
        {
            activeCutscene = GetCutscene(cutsceneName);

            if (activeCutscene != null)
            {
                activeCutscene.CutsceneEndedEvent += OnCutsceneEnded;
                activeCutscene.transform.position = position;

                activeCutscene.SetupActors(activeActors, reserveActors);

                activeCutscene.PlayCutscene();

                IsPlaying = true;
            }
        }

        public CutsceneBase GetCutscene(string cutsceneName)
        {
            string path = CutsceneInResources + cutsceneName;
            CutsceneBase cutscenePrefab = Resources.Load<CutsceneBase>(path);

            if(cutscenePrefab != null)
            {
                return Instantiate(cutscenePrefab);
            }

            return null;
        }

        //Called by the TimeMachine Clip (of type Pause)
        public void PauseTimeline(PlayableDirector whichOne)
        {
            activeDirector = whichOne;
            activeDirector.playableGraph.GetRootPlayable(0).SetSpeed(0d);
            //gameMode = GameMode.DialogueMoment; //InputManager will be waiting for a spacebar to resume
            //UIManager.Instance.TogglePressSpacebarMessage(true);
        }

        //Called by the InputManager
        public void ResumeTimeline()
        {
            //UIManager.Instance.TogglePressSpacebarMessage(false);
            //UIManager.Instance.ToggleDialoguePanel(false);
            activeDirector.playableGraph.GetRootPlayable(0).SetSpeed(1d);
            //gameMode = GameMode.Gameplay;
        }

        private void OnCutsceneEnded(Transform transform)
        {
            activeCutscene.CutsceneEndedEvent -= OnCutsceneEnded;

            activeCutscene = null;

            IsPlaying = false;
        }
    }
}

