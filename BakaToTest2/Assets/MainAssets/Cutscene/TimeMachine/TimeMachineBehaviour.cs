using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;


namespace Intelligensia.Cutscene
{
	[Serializable]
	public class TimeMachineBehaviour : PlayableBehaviour
	{
		public TimeMachineAction action;
		public Condition condition;
		public string markerToJumpTo, markerLabel;
		public float timeToJumpTo;
		public Actor actor;

		[HideInInspector]
		public bool clipExecuted = false; //the user shouldn't author this, the Mixer does

		public bool ConditionMet()
		{
			switch (condition)
			{
				case Condition.Always:
					return true;

				case Condition.ActorHasNotArrived:
					//The Timeline will jump to the label or time if a specific Actor still has not arrived to its destination
					if (actor != null)
					{
						return !actor.HasArrived();
					}
					else
					{
						return true;
					}

				case Condition.Never:
				default:
					return false;
			}
		}

		public enum TimeMachineAction
		{
			Marker,
			JumpToTime,
			JumpToMarker,
			Pause,
		}

		public enum Condition
		{
			Always,
			Never,
			ActorHasNotArrived,
		}
	}
}

