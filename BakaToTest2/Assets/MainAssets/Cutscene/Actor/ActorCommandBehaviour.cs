﻿using UnityEngine;
using UnityEngine.Playables;


namespace Intelligensia.Cutscene
{
    [System.Serializable]
    public class ActorCommandBehaviour : PlayableBehaviour
    {
        public ActorCommand.CommandType commandType;
        public Transform targetObject; // For look at or follow target
        public Transform targetPosition; // For movement
        public ActorExpression expressionType; // For showing expression
        public bool returnToIdleAfterExpression;


        [HideInInspector]
        public bool commandExecuted = false; //the user shouldn't author this, the Mixer does

        //No logic in here, all logic is in the Track Mixer (ActorMixerBehaviour)
        //This is because each clip needs more than one clip in Edit mode,
        //to be able to tween the Actors' positions from one clip to the other
    }
}

