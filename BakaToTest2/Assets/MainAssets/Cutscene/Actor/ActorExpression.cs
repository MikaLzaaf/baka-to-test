﻿
public enum ActorExpression 
{
    Undefined,
    Happy,
    Angry,
    Sad,
    Surprised,
    HeadNod,
    Dismissive,
    HoldOutHand,
    LookAway,
    Rejected,
    Scared,
    Idle,
    Cocky
}
