﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;


namespace Intelligensia.Cutscene
{
    public class ActorCommandMixerBehaviour : PlayableBehaviour
    {
		private Actor trackBinding;
		private bool firstFrameHappened = false;
		private Vector3 defaultPosition, finalPosition, previousInputFinalPosition;
		private Vector3 newPosition;

		public override void ProcessFrame(Playable playable, FrameData info, object playerData)
		{
			trackBinding = playerData as Actor;

			if (trackBinding == null)
				return;

			if (!firstFrameHappened)
			{
				defaultPosition = trackBinding.transform.position;
				firstFrameHappened = true;
			}

			//different behaviour depending if Unity is in Play mode or not,
			//because NavMeshAgent is not available in Edit mode
			if (Application.isPlaying)
			{
				ProcessPlayModeFrame(playable);
			}
			else
			{
				ProcessEditModeFrame(playable);
			}
		}

		//Happens every frame in Edit mode.
		//Uses transform.position of the units to approximate what they would do in Play mode with the NavMeshAgent
		private void ProcessEditModeFrame(Playable playable)
		{
			previousInputFinalPosition = defaultPosition;
			int inputCount = playable.GetInputCount();
			
			for (int i = 0; i < inputCount; i++)
			{
				float inputWeight = playable.GetInputWeight(i);
				ScriptPlayable<ActorCommandBehaviour> inputPlayable = (ScriptPlayable<ActorCommandBehaviour>)playable.GetInput(i);
				ActorCommandBehaviour input = inputPlayable.GetBehaviour();

				if(input.commandType == ActorCommand.CommandType.LookAtTarget)
                {
					if (input.targetObject == null)
						return;

					if (inputWeight > 0f)
					{
						double progress = inputPlayable.GetTime() / inputPlayable.GetDuration();

						trackBinding.LookAtTargetGradually(input.targetObject, (float)progress);
					}
					
                }
				else if(input.commandType == ActorCommand.CommandType.ShowExpression)
                {
					// Show expression here
					return;
                }
				else
                {
					if (input.commandType == ActorCommand.CommandType.FollowTarget)
                    {
						//Force the finalPosition to the target object in case of a moving target
						if (input.targetObject != null)
						{
							input.targetPosition = input.targetObject;
						}
					}

					finalPosition = input.targetPosition.position;

					if (inputWeight > 0f)
					{
						double progress = inputPlayable.GetTime() / inputPlayable.GetDuration();

						newPosition = Vector3.Lerp(previousInputFinalPosition, finalPosition, (float)progress);

						trackBinding.SetPosition(newPosition);
					}
					else
					{
						previousInputFinalPosition = finalPosition; //cached to act as initial position for the next input
					}
				}
			}
		}

		//Happens in Play mode
		//Uses the NavMeshAgent to control the units, delegating their movement and animations to the AI
		private void ProcessPlayModeFrame(Playable playable)
		{
			int inputCount = playable.GetInputCount();

			for (int i = 0; i < inputCount; i++)
			{
				float inputWeight = playable.GetInputWeight(i);
				ScriptPlayable<ActorCommandBehaviour> inputPlayable = (ScriptPlayable<ActorCommandBehaviour>)playable.GetInput(i);
				ActorCommandBehaviour input = inputPlayable.GetBehaviour();

				//Make the Unit script execute the command
				if (inputWeight > 0f)
				{
					if (!input.commandExecuted)
					{
						ActorCommand c = null;

						if (input.commandType == ActorCommand.CommandType.ShowExpression)
                        {
							c = new ActorCommand(input.commandType, input.expressionType, input.returnToIdleAfterExpression);
						}
						else
                        {
							c = new ActorCommand(input.commandType, input.targetPosition, input.targetObject);
						}
 
                        trackBinding.ExecuteCommand(c);
                        
                        input.commandExecuted = true; //this prevents the command to be executed every frame of this clip
					}
				}
			}
		}

		public override void OnPlayableDestroy(Playable playable)
		{
			if (!Application.isPlaying)
			{
				firstFrameHappened = false;

				if (trackBinding == null)
					return;

				trackBinding.SetPosition(defaultPosition);
			}
		}
	}
}

