﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Intelligensia.Cutscene;

public class Actor : MonoBehaviour
{

    // What to do here?
    // - Set Animator to desired animation
    // - 
    public EntityCharacterController entityCharacterController;
    public HumanAnimatorController animatorController;
    public bool useGameObjectName = false;

    private bool isSkillAnimation = false;
    private bool isEnemyActor;

    public string actorName { get; private set; }


    private void Awake()
    {
        if(TryGetComponent(out BattleEntity entity))
        {
            isEnemyActor = entity.entityTag == EntityTag.Enemy;

            if(animatorController != null)
            {
                animatorController.SetEntityIsEnemy(isEnemyActor);
            }

            if (!useGameObjectName)
            {
                actorName = entity.characterName;
            }
        }
        else
        {
            actorName = gameObject.name;
        }
    }


    private void OnEnable()
    {
        if (OnSceneActorsRegistry.instance != null)
            OnSceneActorsRegistry.instance.AddActor(this);
    }

    private void OnDisable()
    {
        if(OnSceneActorsRegistry.instance != null)
            OnSceneActorsRegistry.instance.RemoveActor(this);
    }


    public void StartSkillAnimation(int skillIndex)
    {
        if (animatorController == null)
            return;

        isSkillAnimation = true;

        animatorController.StartSkillAnimation(skillIndex);
    }

    public void NextAnimation()
    {
        if (animatorController == null)
            return;

        if(isSkillAnimation)
        {
            animatorController.NextSkillAnimation();
        }
    }


    public void SuccessfulSkillAnimation(bool success)
    {
        if (animatorController == null)
            return;

        animatorController.SetSkillSuccess(success);
    }


    public void DoOrderAnimation(bool startOrder)
    {
        if (animatorController != null)
        {
            animatorController.DoOrder(startOrder);
        }
    }

    //Forces the position of the actor. Useful in Edit mode only (Play mode would use the Coroutine)
    public void SetPosition(Vector3 destination)
    {
        transform.position = destination;
    }

    public void ExecuteCommand(ActorCommand newCommand)
    {
        Debug.Log("Execute Command! " + newCommand.commandType.ToString());
        switch (newCommand.commandType)
        {
            case ActorCommand.CommandType.WalkTo:
                MoveTo(newCommand.destination.position, false);
                break;

            case ActorCommand.CommandType.RunTo:
                MoveTo(newCommand.destination.position, true);
                break;

            case ActorCommand.CommandType.LookAtTarget:
                LookAtTargetImmediately(newCommand.target);
                break;

            case ActorCommand.CommandType.FollowTarget:
                //MoveToAttack(c.target);
                break;

            case ActorCommand.CommandType.SetPosition:
                StartCoroutine(SettingPosition(newCommand.destination.position));
                break;

            case ActorCommand.CommandType.ShowExpression:
                SetExpression(newCommand.expressionType, newCommand.returnToIdleAfterExpression);
                break;
        }
    }


    public void MoveTo(Vector3 destination, bool runToDestination)
    {
        if(entityCharacterController != null)
        {
            entityCharacterController.AutoMove(runToDestination, destination);
        }
    }

    public bool HasArrived()
    {
        if(entityCharacterController != null)
        {
            return !entityCharacterController.IsProcessingAutoMovement;
        }

        return false;
    }

    public void LookAtTargetImmediately(Transform target)
    {
        if(entityCharacterController != null)
        {
            Vector3 direction = target.transform.position - transform.position;
            entityCharacterController.LookAtDirection(direction);
        }
    }

    public void LookAtTargetGradually(Transform target, float progress)
    {
        if (entityCharacterController != null)
        {
            Vector3 finalDirection = target.transform.position - transform.position;

            Vector3 currentDirection = Vector3.Lerp(transform.eulerAngles, finalDirection, progress);
     
            entityCharacterController.LookAtDirection(currentDirection);
        }
        //transform.eulerAngles = direction;
    }

    public void SetActorPosition(Vector3 destination)
    {
        StartCoroutine(SettingPosition(destination));
    }

    private IEnumerator SettingPosition(Vector3 destination)
    {
        if (entityCharacterController.IsProcessingAutoMovement)
            entityCharacterController.StopMoving();

        entityCharacterController.updateMover = false;

        yield return new WaitForFixedUpdate();

        SetPosition(destination);

        yield return new WaitForFixedUpdate();

        entityCharacterController.updateMover = true;
    }

    public void SetExpression(ActorExpression expression, bool returnToIdleAfterExpression)
    {
        if(animatorController != null)
        {
            if(expression != ActorExpression.Undefined)
            {
                if(expression == ActorExpression.Idle)
                {
                    animatorController.NoExpression();
                }
                else
                {
                    animatorController.ShowExpression(expression, returnToIdleAfterExpression);
                }
            }
        }
    }
}
