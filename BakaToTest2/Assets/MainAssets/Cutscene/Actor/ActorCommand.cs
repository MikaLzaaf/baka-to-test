﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Intelligensia.Cutscene
{
    [System.Serializable]
    public class ActorCommand
    {
        public CommandType commandType;
        public Transform destination;
        public Transform target;
        public ActorExpression expressionType;
        public bool returnToIdleAfterExpression;

        public ActorCommand()
        {

        }

        public ActorCommand(CommandType commandType, Transform targetPosition, Transform targetObject)
        {
            this.commandType = commandType;
            destination = targetPosition;
            target = targetObject;
        }

        public ActorCommand(CommandType commandType, ActorExpression expression, bool returnToIdleAfterExpression)
        {
            this.commandType = commandType;
            expressionType = expression;
            this.returnToIdleAfterExpression = returnToIdleAfterExpression;
        }

        //public ActorCommand(CommandType commandType, Transform targetPosition)
        //{
        //    this.commandType = commandType;
        //    destination = targetPosition;
        //}

        //public ActorCommand(CommandType commandType, Transform targetObject)
        //{
        //    this.commandType = commandType;
        //    target = targetObject;
        //}

        //public ActorCommand(CommandType commandType)
        //{
        //    this.commandType = commandType;
        //}

        public enum CommandType
        {
            WalkTo,
            RunTo,
            LookAtTarget,
            FollowTarget,
            SetPosition,
            ShowExpression
        }
    }
}

