﻿using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;


namespace Intelligensia.Cutscene
{
    public class ActorCommandClip : PlayableAsset, ITimelineClipAsset
    {
        [HideInInspector]
        public ActorCommandBehaviour template = new ActorCommandBehaviour();

        public ActorCommand.CommandType commandType;
        public ExposedReference<Transform> targetObject; // For look at or follow target
        public ExposedReference<Transform> targetPosition; // For movement
        public ActorExpression expressionType; // For showing expression
        public bool returnToIdleAfterExpression;

        public ClipCaps clipCaps
        {
            get { return ClipCaps.None; }
        }

        public override Playable CreatePlayable(PlayableGraph graph, GameObject owner)
        {
            var playable = ScriptPlayable<ActorCommandBehaviour>.Create(graph, template);
            ActorCommandBehaviour clone = playable.GetBehaviour();
            clone.commandType = commandType;
            clone.targetPosition = targetPosition.Resolve(graph.GetResolver());
            clone.targetObject = targetObject.Resolve(graph.GetResolver());
            clone.expressionType = expressionType;
            clone.returnToIdleAfterExpression = returnToIdleAfterExpression;
            return playable;
        }
    }
}

