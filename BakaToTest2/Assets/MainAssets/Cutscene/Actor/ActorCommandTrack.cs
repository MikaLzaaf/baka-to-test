﻿using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;


namespace Intelligensia.Cutscene
{
    [TrackColor(0f, 0.4866645f, 1f)]
    [TrackClipType(typeof(ActorCommandClip))]
    [TrackBindingType(typeof(Actor))]
    public class ActorCommandTrack : TrackAsset
    {
        public string actorName;
        public override Playable CreateTrackMixer(PlayableGraph graph, GameObject go, int inputCount)
        {
            foreach (var c in GetClips())
            {
                //Clips are renamed after the actionType of the clip itself
                ActorCommandClip clip = (ActorCommandClip)c.asset;
                c.displayName = clip.commandType.ToString();
            }

            return ScriptPlayable<ActorCommandMixerBehaviour>.Create(graph, inputCount);
        }
    }
}

