﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Intelligensia.Cutscene
{
    [CustomEditor(typeof(ActorCommandClip))]
    public class ActorCommandInspector : Editor
    {
		private SerializedProperty commandProp;
		private int typeIndex;

		private void OnEnable()
		{
            //SceneView.duringSceneGui += OnSceneGUI;
            commandProp = serializedObject.FindProperty("commandType");
		}

		public override void OnInspectorGUI()
		{
			EditorGUILayout.PropertyField(commandProp);

			typeIndex = serializedObject.FindProperty("commandType").enumValueIndex;
			ActorCommand.CommandType commandType = (ActorCommand.CommandType)typeIndex;

			//Draws only the appropriate information based on the Command Type
			switch (commandType)
			{
				case ActorCommand.CommandType.WalkTo:
				case ActorCommand.CommandType.RunTo:
				case ActorCommand.CommandType.SetPosition:
					EditorGUILayout.PropertyField(serializedObject.FindProperty("targetPosition"));
					break;

				case ActorCommand.CommandType.LookAtTarget:
				case ActorCommand.CommandType.FollowTarget:
					EditorGUILayout.PropertyField(serializedObject.FindProperty("targetObject")); 
					break;

				case ActorCommand.CommandType.ShowExpression:
					EditorGUILayout.PropertyField(serializedObject.FindProperty("expressionType"));
					EditorGUILayout.PropertyField(serializedObject.FindProperty("returnToIdleAfterExpression"));
					break;

					//case Actor.CommandType.Die:
					//case Actor.CommandType.Stop:
					//no information needed
					//break;
			}
			serializedObject.ApplyModifiedProperties();
		}

		//private void OnDisable()
		//{
  //          //SceneView.duringSceneGui -= OnSceneGUI;
		//	serializedObject.Dispose();
  //      }


		//Draws a position handle on the position associated with the ActorCommand
		//the handle can be moved to reposition the targetPosition property
		//private void OnSceneGUI(SceneView v)
		//{
		//	//Debug.Log("On scene gui");
		//	Debug.Log(serializedObject.targetObject);
		//	if ((ActorCommand.CommandType)typeIndex == ActorCommand.CommandType.WalkTo
		//		|| (ActorCommand.CommandType)typeIndex == ActorCommand.CommandType.RunTo
		//		|| (ActorCommand.CommandType)typeIndex == ActorCommand.CommandType.SetPosition)
		//	{
		//		EditorGUI.BeginChangeCheck();
	
		//		Vector3 gizmoPos = Handles.PositionHandle(serializedObject.FindProperty("targetPosition").vector3Value, Quaternion.identity);
		//		Handles.Label(gizmoPos, "Destination");

		//		if (EditorGUI.EndChangeCheck())
		//		{
		//			serializedObject.FindProperty("targetPosition").vector3Value = gizmoPos;
		//			serializedObject.ApplyModifiedProperties();
  //                  Repaint();
  //              }
		//	}
		//}
    }
}



