﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Playables;

namespace Intelligensia.Cutscene
{
    public class CutsceneBase : MonoBehaviour
    {
        // For NOW, only for PartyAttack Cutscene ok!


        // A cutscene should be able to 
        // - set the character position each time a sequence starts => use signal
        // - call the character animations on the spot => use signal
        // - rotate / moves the character towards a desired target => use signal
        // - skip the whole cutscene
        // - pause the cutscene
        // - (opt) display dialogues 

        public PlayableDirector playDirector;
        public Transform cutsceneCharacterEndingPosition;
        public bool destroyOnEnded = true;

        [Header("Test")]
        [SerializeField] private Actor[] testActiveMembers = default;
        [SerializeField] private Actor[] testReserveMembers = default;
        public bool playOnStart = false;

        public event Action<Transform> CutsceneEndedEvent;

        private Actor[] activeMembers;
        private Actor[] reserveMembers;

        private List<Vector3> AMOriginalPositions;

        private string ActiveMemberPrefix = "ActiveMember";
        private string ReserveMemberPrefix = "ReserveMember";


        private void Start()
        {
            if(playOnStart)
            {
                SetupActors(testActiveMembers, testReserveMembers);

                PlayCutscene();
            }
        }


        public void SetupActors(Actor[] activeMembersActor, Actor[] reserveMembersActor)
        {
            activeMembers = activeMembersActor;
            reserveMembers = reserveMembersActor;

            AMOriginalPositions = new List<Vector3>();

            for(int i = 0; i < activeMembers.Length; i++)
            {
                if(!activeMembers[i].gameObject.activeSelf)
                    activeMembers[i].gameObject.SetActive(true);

                AMOriginalPositions.Add(activeMembers[i].transform.position);

                string actorName = ActiveMemberPrefix + (i + 1).ToString();

                foreach (var playableAssetOutput in playDirector.playableAsset.outputs)
                {
                    if (playableAssetOutput.sourceObject is ActorCommandTrack actorCommandTrack)
                    {
                        if (actorCommandTrack.actorName == actorName)
                        {
                            playDirector.SetGenericBinding(playableAssetOutput.sourceObject, activeMembers[i]);
                            break;
                        }
                    }
                }
            }

            for (int i = 0; i < reserveMembers.Length; i++)
            {
                if (!reserveMembers[i].gameObject.activeSelf)
                    reserveMembers[i].gameObject.SetActive(true);

                string actorName = ReserveMemberPrefix + (i + 1).ToString();

                foreach (var playableAssetOutput in playDirector.playableAsset.outputs)
                {
                    if (playableAssetOutput.sourceObject is ActorCommandTrack actorCommandTrack)
                    {
                        if (actorCommandTrack.actorName == actorName)
                        {
                            playDirector.SetGenericBinding(playableAssetOutput.sourceObject, reserveMembers[i]);
                            break;
                        }
                    }
                }
            }
        }

        public void StopCutscene()
        {
            CutsceneEndedEvent?.Invoke(cutsceneCharacterEndingPosition);

            for(int i = 0; i < activeMembers.Length; i++)
            {
                activeMembers[i].SetActorPosition(AMOriginalPositions[i]);

                //activeMembers[i].transform.position = AMOriginalPositions[i];
            }

            for(int i = 0; i < reserveMembers.Length; i++)
            {
                reserveMembers[i].gameObject.SetActive(false);
            }

            if(destroyOnEnded)
            {
                Destroy(gameObject, 1f);
            }
        }

        public void PlayCutscene()
        {
            StartCoroutine(WaitBeforePlay());
        }

        private IEnumerator WaitBeforePlay()
        {
            yield return new WaitForSeconds(0.5f);

            if (playDirector != null)
            {
                playDirector.Play();
            }
        }

        public void WhiteFadeIn()
        {
            if(GameUIManager.instance != null)
            {
                GameUIManager.instance.ShowWhiteFade();
            }
        }
    }
}

