﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Intelligensia.NPC
{
    [System.Serializable]
    public class NPCState_InBattle : NPCState
    {

        public NPCState_InBattle(NPCStateController stateController) : base(stateController)
        {
            this.stateController = stateController;
        }
        public override void Enter()
        {
            if (BattleController.instance != null)
                BattleController.instance.BattleEndedEvent += OnBattleEnded;

            //stateController.interactable.TriggerBattle();
        }

        public override void Exit()
        {

        }

        public override void HandleUpdate()
        {

        }


        private void OnBattleEnded()
        {
            if (BattleController.instance != null)
                BattleController.instance.BattleEndedEvent -= OnBattleEnded;

            stateController.interactable.ToggleIsChallengedDialogue(false);

            stateController.OnInteractionEnded();
        }
    }
}
