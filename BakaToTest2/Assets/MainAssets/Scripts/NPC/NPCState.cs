﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Intelligensia.NPC
{
    [System.Serializable]
    public abstract class NPCState 
    {
        protected NPCStateController stateController;

        public NPCState(NPCStateController stateController)
        {
            this.stateController = stateController;
        }


        public abstract void Enter();
        public abstract void HandleUpdate();
        public abstract void Exit();
    }
}

