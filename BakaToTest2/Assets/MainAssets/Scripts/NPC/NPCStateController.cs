﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PixelCrushers.DialogueSystem;

namespace Intelligensia.NPC
{
    public class NPCStateController : MonoBehaviour
    {
        // What can NPC do?
        // - Talk with Player
        // - Talk with other NPC but Player can listen
        // - Walk around
        // - Trigger battle or invite to battle
        // - Give item
        // - Give info / quest info
        // - Increase bond level

        public EntityCharacterController characterController;
        public NPCInteractable interactable;
        public Actor npcActor;
        public DialogueSystemTrigger dialogueSystemTrigger;
        public NPCStateEnums startingState;

        [Header("Idle")]
        public ActorExpression idleExpression;

        [Header("Patrol")]
        public WaypointMover waypointMover;
        public bool walkDuringPatrol = true;

        public NPCStateEnums CurrentStateEnum { get; private set; }
        public NPCState CurrentState { get; private set; }

        public Transform currentInteractor { get; private set; }

        #region NPC States Variables

        private NPCState_Idle state_Idle;
        //private NPCState_InBattle state_InBattle;
        private NPCState_InConversation state_InConversation;
        private NPCState_Patrol state_Patrol;

        private Dictionary<NPCStateEnums, NPCState> npcStatesLookup;

        #endregion


        private void Start()
        {
            InitializeStates();
            
            if (interactable != null)
                interactable.InteractionStartedEvent += OnInteractionStarted;
        }

        private void OnDestroy()
        {
            if (interactable != null)
                interactable.InteractionStartedEvent -= OnInteractionStarted;
        }


        private void Update()
        {
            if (CurrentState != null)
            {
                CurrentState.HandleUpdate();
            }
        }

        #region NPC States

        private void InitializeStates()
        {
            state_Idle = new NPCState_Idle(this);
            //state_InBattle = new NPCState_InBattle(this);
            state_InConversation = new NPCState_InConversation(this);
            state_Patrol = new NPCState_Patrol(this);

            npcStatesLookup = new Dictionary<NPCStateEnums, NPCState>();
            npcStatesLookup.Add(NPCStateEnums.Idle, state_Idle);
            //npcStatesLookup.Add(NPCStateEnums.InBattle, state_InBattle);
            npcStatesLookup.Add(NPCStateEnums.InConversation, state_InConversation);
            npcStatesLookup.Add(NPCStateEnums.Patrol, state_Patrol);

            ChangeState(startingState);
        }

        public void ChangeState(NPCState newState)
        {
            if (CurrentState != null)
            {
                CurrentState.Exit();
            }

            CurrentState = newState;
            newState.Enter();
        }

        public void ChangeState(NPCStateEnums newState)
        {
            npcStatesLookup.TryGetValue(newState, out NPCState nextState);

            if (nextState != null)
            {
                CurrentState = nextState;
                CurrentStateEnum = newState;
                ChangeState(nextState);
            }
        }

        #endregion

        private void OnInteractionStarted(Transform interactor)
        {
            currentInteractor = interactor;
            
            //if(interactable.IsBattling)
            //    ChangeState(NPCStateEnums.InBattle);
            //else
                ChangeState(NPCStateEnums.InConversation);

            Debug.Log("Interaction started");
        }

        // Assign kat NPC gameObject > Dialogue System Events > OnConversationEnd
        public void OnInteractionEnded()
        {
            //if(interactable.IsBattling)
            //{
            //    ChangeState(state_InBattle);
            //}
            //else
            //{
                ChangeState(startingState);

                if (interactable != null)
                {
                    interactable.EndInteraction();
                }
                Debug.Log("Interaction ended");
            //}
        }
    }
}

