﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Intelligensia.NPC
{
    [System.Serializable]
    public class NPCState_Idle : NPCState
    {

        public NPCState_Idle(NPCStateController stateController) : base(stateController)
        {
            this.stateController = stateController;
        }

        public override void Enter()
        {
            if(stateController.npcActor != null)
            {
                stateController.npcActor.SetExpression(stateController.idleExpression, true);
            }
        }

        public override void Exit()
        {
            
        }

        public override void HandleUpdate()
        {
           
        }
    }
}

