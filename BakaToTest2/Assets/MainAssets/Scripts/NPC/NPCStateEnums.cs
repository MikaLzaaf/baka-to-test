﻿namespace Intelligensia.NPC
{
    public enum NPCStateEnums 
    {
        Idle,
        Patrol,
        InConversation,
        InBattle
    }
}
