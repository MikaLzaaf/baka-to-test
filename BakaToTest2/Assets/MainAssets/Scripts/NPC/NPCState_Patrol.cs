﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace Intelligensia.NPC
{
    [System.Serializable]
    public class NPCState_Patrol : NPCState
    {
        private WaypointMover waypointMover;

        private bool isIdling = false;
        private bool isWalking;

        public NPCState_Patrol(NPCStateController stateController) : base(stateController)
        {
            this.stateController = stateController;
            waypointMover = stateController.waypointMover;
        }

        public override void Enter()
        {
            isWalking = stateController.walkDuringPatrol;
        }

        public override void Exit()
        {
            StopPatrolling();
        }

        public override void HandleUpdate()
        {
            if (stateController == null || waypointMover == null)
                return;

            waypointMover.UpdatMover();

            if(waypointMover.isIdle)
            {
                if (!isIdling)
                    StopPatrolling();

                isIdling = true;
                return;
            }

            isIdling = false;

            Transform currentWaypoint = waypointMover.GetCurrentWaypoint();

            if (currentWaypoint == null)
            {
                return;
            }

            Vector3 directionToCurrentWaypoint = currentWaypoint.position - stateController.transform.position;

            GoToNextPoint(directionToCurrentWaypoint);
            //if (directionToCurrentWaypoint.sqrMagnitude <= Mathf.Pow(waypointMover.pickNextWaypointDistance, 2))
            //{
            //    GoToNextPoint(directionToCurrentWaypoint);
            //}
            //else
            //{
            //    GoToNextPoint(directionToCurrentWaypoint);
            //}
        }

        private void GoToNextPoint(Vector3 direction)
        {
            direction.y = 0;
            direction.Normalize();

            stateController.characterController.SetMovementDirection(direction, isWalking);
        }

        private void StopPatrolling()
        {
            stateController.characterController.StopMoving();
        }
    }
}
