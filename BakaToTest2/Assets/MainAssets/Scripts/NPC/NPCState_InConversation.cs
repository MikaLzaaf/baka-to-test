﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Intelligensia.NPC
{
    [System.Serializable]
    public class NPCState_InConversation : NPCState
    {

        public NPCState_InConversation(NPCStateController stateController) : base(stateController)
        {
            this.stateController = stateController;
        }

        public override void Enter()
        {
            stateController.characterController.StopMoving();

            // Look at interactor
            Vector3 direction = stateController.currentInteractor.position - stateController.transform.position;

            stateController.characterController.RotateTowards(direction);

            if(stateController.dialogueSystemTrigger != null)
            {
                stateController.dialogueSystemTrigger.TryStart(stateController.transform);
            }
        }

        public override void Exit()
        {
           
        }

        public override void HandleUpdate()
        {
            
        }
    }
}
