﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public static class DailyInfoManager 
{
    public static readonly int MaxChallengesCount = 3;

    public static event Action<int> ChallengesUpdateEvent;
    public static event Action<string> ChallengeRemovedEvent;
        
    public static Subjects activeSubject;
    public static DayEnum currentDay;
    public static int dayDate;
    public static int month;
    public static string currentSession;
    public static string currentWeather;

    public static ChallengeInfo currentChallengeInfo;

    public static List<ChallengeInfo> currentChallengeInfos { get; private set; }
    public static int currentChallengesCount
    {
        get
        {
            if (currentChallengeInfos == null)
                return 0;
            else
                return currentChallengeInfos.Count;
        }
    }


    public static void LoadChallengeData()
    {
        currentChallengeInfos = GameDataManager.gameData.challengeInfos;
    }


    public static void AddChallenge(ChallengeInfo challengeInfo)
    {
        if (currentChallengesCount >= MaxChallengesCount)
            return;

        currentChallengeInfos.Add(challengeInfo);

        ChallengesUpdateEvent?.Invoke(currentChallengesCount);
    }

    public static void RemoveChallenge(ChallengeInfo challengeInfo)
    {
        if (currentChallengesCount <= 0)
            return;

        currentChallengeInfos.Remove(challengeInfo);

        ChallengesUpdateEvent?.Invoke(currentChallengesCount);

        ChallengeRemovedEvent?.Invoke(challengeInfo.id);
    }

    public static bool IsChallengedAlready(ChallengeInfo challengeInfo)
    {
        for(int i = 0; i < currentChallengeInfos.Count; i++)
        {
            if (currentChallengeInfos[i].id == challengeInfo.id)
                return true;
        }

        return false;
    }

    public static ChallengeInfo GetIncompleteChallenge()
    {
        currentChallengeInfo = null;

        for (int i = 0; i < currentChallengeInfos.Count; i++)
        {
            if (!currentChallengeInfos[i].isCompleted)
            {
                currentChallengeInfo = currentChallengeInfos[i];
                break;
            }
        }

        return currentChallengeInfo;
    }

    public static bool HasNextChallenge()
    {
        return GetIncompleteChallenge() != null;
    }
}
