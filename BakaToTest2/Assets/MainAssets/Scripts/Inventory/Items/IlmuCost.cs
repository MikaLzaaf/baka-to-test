﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class IlmuCost 
{
    public Subjects subject;
    public int cost;
}
