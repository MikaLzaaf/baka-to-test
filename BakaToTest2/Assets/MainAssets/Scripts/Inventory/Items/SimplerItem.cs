﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SimplerItem
{
    public ItemBaseValue itemBase;
    public int amount = 1;


    public Item GetItem()
    {
        return new Item(itemBase.ItemID, amount);
    }
}
