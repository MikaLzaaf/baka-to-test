﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Item
{
    //private const string ItemDataInResources = "Items/BaseValues/";
    private const string PreventsInLocalizationId = "Prevents";
    private const string StatsDescriptionFormat = "[<color=#FED330>{0}</color=#FED330>]\n";

    public event Action<Item> ItemUsedEvent;

    protected ItemBaseValue _itemValue;
    public ItemBaseValue itemValue
    {
        get
        {
            if(_itemValue == null)
            {
                if (!string.IsNullOrEmpty(itemID))
                    _itemValue = BaseValueLoader.LoadItemData(itemID);
            }

            return _itemValue;
        }

        set
        {
            _itemValue = value;
        }
    }

    [SerializeField] protected string itemID;
    public string ItemID
    {
        get
        {
            if(string.IsNullOrEmpty(itemID))
            {
                if (_itemValue != null)
                    itemID = _itemValue.ItemID;
                else
                    itemID = "";
            }

            return itemID;
        }
        set
        {
            itemID = value;
        }
    }

    public int amount;

    //public bool isEnhanced;


    [SerializeField] protected int equippedAmount = 0;

    private bool isUsedByPlayer;

    public Item() { }

    public Item(string ItemID, int amount)
    {
        this.ItemID = ItemID;
        this.amount = amount;

        //this.isEnhanced = isEnhanced;
    }

    public bool IsIlmuItem()
    {
        if (itemValue == null)
            return false;

        return itemValue is IlmuItemBaseValue;
    }

    public bool IsSame(Item otherItem)
    {
        return itemValue.ItemID == otherItem.itemValue.ItemID;
    }

    #region Battle related

    public void Use(BattleEntity user, BattleEntity target, bool canUpdateAmount)
    {
        itemValue.Use(target);

        if(canUpdateAmount)
        {
            if (user.entityTag == EntityTag.Player)
                PlayerInventoryManager.instance.RemoveItem(itemValue.ItemID, 1);
            else if (user.entityTag == EntityTag.Enemy)
            {
                GenericEnemy enemy = (GenericEnemy)user;
                enemy.Consume(ItemID, 1);
            }
        }

        OnItemUsed();
    }

    //public void Use(BattleEntity[] targets)
    //{
    //    for(int i = 0; i < targets.Length; i++)
    //    {
    //        Use(targets[i]);
    //    }
    //}

    //public void Use(string itemId, BattleEntity[] targets, bool isUsedByPlayer)
    //{
    //    this.isUsedByPlayer = isUsedByPlayer;

    //    if(ItemID == itemId)
    //    {
    //        Use(targets);
    //    }
    //}

    //public void SubscribeToChannels()
    //{
    //    if (!itemValue.isConsumable)
    //        return;

    //    ConsumableItemBaseValue consumableItemBase = (ConsumableItemBaseValue)itemValue;

    //    if (consumableItemBase != null)
    //    {
    //        consumableItemBase.useItemInBattleEventChannel.OnItemUsage += Use;
    //    }
    //}

    //public void UnsubscribeToChannels()
    //{
    //    if (!itemValue.isConsumable)
    //        return;

    //    ConsumableItemBaseValue consumableItemBase = (ConsumableItemBaseValue)itemValue;

    //    if (consumableItemBase != null)
    //    {
    //        consumableItemBase.useItemInBattleEventChannel.OnItemUsage -= Use;
    //    }
    //}

    protected void OnItemUsed()
    {
        ItemUsedEvent?.Invoke(this);
    }

    #endregion

    public void Equip()
    {
        equippedAmount += 1;
    }

    public void Unequip()
    {
        equippedAmount -= 1;
    }

    public int GetEquipableAmount()
    {
        return amount - equippedAmount;
    }

    public string GetDescription()
    {
        string description = "";

        if (itemValue.ItemCategory == ItemCategory.Consumable)
        {
            ConsumableItemBaseValue consumableBaseValue = (ConsumableItemBaseValue)itemValue;

            string itemEffectType = consumableBaseValue.consumableType.ToString();

            string targetUser = consumableBaseValue.GetAimInDescription();

            string effectsDescription = consumableBaseValue.GetSupportEffectsDescription();

            string statsDescription = itemEffectType + " - " + targetUser + " - " + effectsDescription;

            if (!string.IsNullOrEmpty(statsDescription))
            {
                description += string.Format(StatsDescriptionFormat, statsDescription);
            }

        }
        else if (itemValue.ItemCategory == ItemCategory.Equipment)
        {
            EquipmentItemBaseValue equipmentBaseValue = (EquipmentItemBaseValue)itemValue;

            BasicStats statsEffective = equipmentBaseValue.statsEfective;

            string statsDescription = "";

            if (statsEffective.HasStats())
            {
                for (int i = 0; i < statsEffective.baseStats.Count; i++)
                {
                    statsDescription += statsEffective.baseStats[i].GetDescription();

                    if (i != statsEffective.baseStats.Count - 1)
                    {
                        statsDescription += "/";
                    }
                }
            }

            AcademicStats academicStats = equipmentBaseValue.academicStats;

            if (academicStats.HasStats())
            {
                if (!string.IsNullOrEmpty(statsDescription))
                    statsDescription += "/";

                for (int i = 0; i < academicStats.subjectsStats.Count; i++)
                {
                    statsDescription += academicStats.subjectsStats[i].GetDescription();

                    if (i != academicStats.subjectsStats.Count - 1)
                    {
                        statsDescription += "/";
                    }
                }
            }

            var statusEffectResistances = equipmentBaseValue.statusEffectResistances;

            if (statusEffectResistances.Count > 0)
            {
                if (!string.IsNullOrEmpty(statsDescription))
                    statsDescription += "/";

                statsDescription += LocalizationManager.Localize(PreventsInLocalizationId) + " ";

                for (int i = 0; i < statusEffectResistances.Count; i++)
                {
                    statsDescription += statusEffectResistances[i].GetDescription();

                    if (i != statusEffectResistances.Count - 1)
                    {
                        statsDescription += "/";
                    }
                }
            }

            if (!string.IsNullOrEmpty(statsDescription))
            {
                description += string.Format(StatsDescriptionFormat, statsDescription);
            }
        }
        else if (itemValue.ItemCategory == ItemCategory.Skill)
        {
            SkillItemBaseValue skillItemBase = (SkillItemBaseValue)itemValue;

            description += skillItemBase.baseSkill.GetFullDescription(false);
        }

        description += LocalizationManager.Localize(ItemID);

        return description;
    }

   
}