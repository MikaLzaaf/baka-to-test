﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PixelCrushers.DialogueSystem;

public class PlayerInventoryManager : Singleton<PlayerInventoryManager>
{
    public const int MaxItemQuantity = 99;
    public const int MaxIlmuQuantity = 999999;
    public const string IlmuPrefix = "Ilmu_";


    public List<Item> itemsInInventory { get; private set; }

    private Dictionary<string, Item> _allItemsLookup;
    private Dictionary<string, Item> allItemsLookup
    {
        get
        {
            if (_allItemsLookup == null)
            {
                _allItemsLookup = new Dictionary<string, Item>();

                if (itemsInInventory != null)
                {
                    foreach (Item item in itemsInInventory)
                    {
                        _allItemsLookup.Add(item.ItemID, item);
                    }
                }
            }

            return _allItemsLookup;
        }
    }

    //public List<PlayerIlmuPool> playerIlmuPools { get; private set; }
    
    //private Dictionary<CharacterId, PlayerIlmuPool> _ilmuPoolsLookup;
    //private Dictionary<CharacterId, PlayerIlmuPool> ilmuPoolsLookup
    //{
    //    get
    //    {
    //        if(_ilmuPoolsLookup == null)
    //        {
    //            _ilmuPoolsLookup = new Dictionary<CharacterId, PlayerIlmuPool>();

    //            if(playerIlmuPools != null)
    //            {
    //                foreach(PlayerIlmuPool ilmuPool in playerIlmuPools)
    //                {
    //                    _ilmuPoolsLookup.Add(ilmuPool.playerId, ilmuPool);
    //                }
    //            }
    //        }

    //        return _ilmuPoolsLookup;
    //    }
    //}

    public List<PlayerEquipment> playerEquipments { get; private set; }

    private Dictionary<CharacterId, PlayerEquipment> _equipmentsLookup;
    private Dictionary<CharacterId, PlayerEquipment> equipmentsLookup
    {
        get
        {
            if (_equipmentsLookup == null)
            {
                _equipmentsLookup = new Dictionary<CharacterId, PlayerEquipment>();

                if (playerEquipments != null)
                {
                    foreach (PlayerEquipment equipment in playerEquipments)
                    {
                        _equipmentsLookup.Add(equipment.playerId, equipment);
                    }
                }
            }

            return _equipmentsLookup;
        }
    }


    public void LoadFromSaveData()
    {
        itemsInInventory = GameDataManager.gameData.playerInventory.itemInInventory;
        //playerIlmuPools = GameDataManager.gameData.playerInventory.playerIlmuPools;
        playerEquipments = GameDataManager.gameData.playerInventory.playersEquipmentList;
    }

    #region Ilmu Management

    //private PlayerIlmuPool GetIlmuPool(CharacterId characterId)
    //{
    //    ilmuPoolsLookup.TryGetValue(characterId, out PlayerIlmuPool playerIlmuPool);
    //    return playerIlmuPool;
    //}

    //public Item[] GetIlmuItems(CharacterId characterId)
    //{
    //    PlayerIlmuPool ilmuPool = GetIlmuPool(characterId);

    //    if (ilmuPool == null)
    //        return null;

    //    return ilmuPool.ilmuOwned.ToArray();
    //}

    public Item GetIlmuBySubject(Subjects subject)
    {
        Item ilmu = GetItem(IlmuPrefix + subject.ToString());
        return ilmu;
    }

    //public void AddIlmu(CharacterId characterId, Item ilmuItem)
    //{
    //    PlayerIlmuPool ilmuPool = GetIlmuPool(characterId);

    //    if (ilmuPool == null)
    //        return;

    //    IlmuItemBaseValue ilmuItemBase = (IlmuItemBaseValue)ilmuItem.itemValue;

    //    if (ilmuItemBase == null)
    //        return;

    //    ilmuPool.AddIlmu(ilmuItemBase.subject, ilmuItem.amount);
    //}

    public void UseIlmuToPay(CharacterId characterId, IlmuCost[] ilmuCosts)
    {
        for(int i = 0; i < ilmuCosts.Length; i++)
        {
            string id = IlmuPrefix + ilmuCosts[i].subject.ToString();

            Item ilmuItem = GetItem(id);

            if (ilmuItem != null)
            {
                ilmuItem.amount = Mathf.Clamp(ilmuItem.amount - ilmuCosts[i].cost, 0, MaxIlmuQuantity);
            }
        }
    }

    #endregion

    #region Item Management

    private void AddInventoryEntry(Item item)
    {
        if (GetItem(item.ItemID) != null)
            return;

        itemsInInventory.Add(item);
        allItemsLookup.Add(item.itemValue.ItemID, item);
    }

    private void RemoveInventoryEntry(Item item)
    {
        itemsInInventory.Remove(item);
        allItemsLookup.Remove(item.itemValue.ItemID);
    }

    public Item GetItem(string itemID)
    {
        allItemsLookup.TryGetValue(itemID, out Item tempItem);

        return tempItem;
    }

    public void AddItem(string ItemID, int quantity, bool isIlmuItem = false)
    {
        Item tempItem = GetItem(ItemID);

        int maxQuantity = isIlmuItem ? MaxIlmuQuantity : MaxItemQuantity;

        if (tempItem != null)
        {
            if (tempItem.amount < maxQuantity)
            {
                tempItem.amount = Mathf.Clamp(tempItem.amount + quantity, tempItem.amount, maxQuantity);
            }
        }
        else
        {
            int amount = Mathf.Clamp(quantity, 0, maxQuantity);

            tempItem = new Item(ItemID, amount);

            AddInventoryEntry(tempItem); // Kena ubah bagi nampak enhanced dan bukan enhanced. E.g. FF13
        }
    }

    public void AddItem(Item item)
    {
        AddItem(item.ItemID, item.amount, item.IsIlmuItem());
    }

    public void RemoveItem(Item item)
    {
        RemoveItem(item.itemValue.ItemID, item.amount);
    }

    public void RemoveItem(string usedItemID, int amount, bool isIlmuItem = false)
    {
        Item tempItem = GetItem(usedItemID);

        int maxQuantity = isIlmuItem ? MaxIlmuQuantity : MaxItemQuantity;

        if (tempItem != null)
        {
            tempItem.amount = Mathf.Clamp(tempItem.amount - amount, 0, maxQuantity);

            if (tempItem.amount <= 0 && !isIlmuItem)
            {
                RemoveInventoryEntry(tempItem);
            }
        }
    }

    public bool IsFull(string itemId)
    {
        return IsFull(GetItem(itemId));
    }

    public bool IsFull(Item item)
    {
        if (item == null)
        {
            return false;
        }

        int maxQuantity = item.IsIlmuItem() ? MaxIlmuQuantity : MaxItemQuantity;

        return item.amount >= maxQuantity;
    }

    public bool HasItem(string itemId)
    {
        return GetItem(itemId) != null;
    }

    public Item[] GetItemsByCategory(ItemCategory itemCategory)
    {
        List<Item> itemsByCategory = new List<Item>();

        for (int i = 0; i < itemsInInventory.Count; i++)
        {
            if (itemsInInventory[i].itemValue.ItemCategory == itemCategory)
            {
                itemsByCategory.Add(itemsInInventory[i]);
            }
        }

        return itemsByCategory.ToArray();
    }

    #endregion

    #region Manage equipment

    public PlayerEquipment GetPlayerEquipment(CharacterId characterId)
    {
        equipmentsLookup.TryGetValue(characterId, out PlayerEquipment playerEquipment);

        return playerEquipment;
    }

    public List<Item> GetEquipmentListBySection(EquipmentSection section)
    {
        List<Item> equipment = new List<Item>();

        for (int i = 0; i < itemsInInventory.Count; i++)
        {
            if (itemsInInventory[i].itemValue.canEquip)
            {
                EquipmentItemBaseValue equipmentBase = itemsInInventory[i].itemValue as EquipmentItemBaseValue;

                if (equipmentBase.EquipmentSection == section)
                {
                    equipment.Add(itemsInInventory[i]);
                }
            }
        }

        return equipment;
    }

    public void UnequipItem(string itemId)
    {
        Item itemInInventory = GetItem(itemId);

        if (itemInInventory != null)
            itemInInventory.Unequip();
    }

    public void EquipItem(string itemId)
    {
        Item itemInInventory = GetItem(itemId);

        if (itemInInventory != null)
            itemInInventory.Equip();
    }

    #endregion

    #region Lua implementation

    void OnEnable()
    {
        // Make the functions available to Lua: (Replace these lines with your own.)
        Lua.RegisterFunction(nameof(AddItem), this, SymbolExtensions.GetMethodInfo(() => AddItem(string.Empty, (double)0)));
        Lua.RegisterFunction(nameof(RemoveItem), this, SymbolExtensions.GetMethodInfo(() => RemoveItem(string.Empty, (double)0)));
        Lua.RegisterFunction(nameof(HasItem), this, SymbolExtensions.GetMethodInfo(() => HasItem(string.Empty)));

    }

    void OnDisable()
    {
        Lua.UnregisterFunction(nameof(AddItem));
        Lua.UnregisterFunction(nameof(RemoveItem));
        Lua.UnregisterFunction(nameof(HasItem));
    }

    public void AddItem(string itemID, double amount)
    {
        AddItem(itemID, (int)amount);
    }

    public void RemoveItem(string itemID, double amount)
    {
        RemoveItem(itemID, (int)amount);
    }

    #endregion
}
