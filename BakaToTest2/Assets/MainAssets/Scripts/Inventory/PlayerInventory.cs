﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


[Serializable]
public class PlayerInventory
{
    //public const int MaxItemQuantity = 99;
    //public const int MaxIlmuQuantity = 999999;


    [SerializeField] private List<Item> _itemInInventory = new List<Item>();
    public List<Item> itemInInventory
    {
        get => _itemInInventory;
        set => _itemInInventory = value;
    }


    private Dictionary<string, Item> _allItemsLookup;
    private Dictionary<string, Item> allItemsLookup
    {
        get
        {
            if(_allItemsLookup == null)
            {
                _allItemsLookup = new Dictionary<string, Item>();

                if(itemInInventory != null)
                {
                    foreach(Item item in itemInInventory)
                    {
                        _allItemsLookup.Add(item.itemValue.ItemID, item);
                    }
                }
            }

            return _allItemsLookup;
        }
    }

    //[SerializeField] private List<PlayerIlmuPool> _playerIlmuPools = new List<PlayerIlmuPool>();
    //public List<PlayerIlmuPool> playerIlmuPools
    //{
    //    get => _playerIlmuPools;
    //    set => _playerIlmuPools = value;
    //}


    [SerializeField] private List<PlayerEquipment> _playersEquipmentList = new List<PlayerEquipment>();
    public List<PlayerEquipment> playersEquipmentList
    {
        get => _playersEquipmentList;
        set => _playersEquipmentList = value;
    }


    #region Manage item in iventory

    private void AddInventoryEntry(Item item)
    {
        itemInInventory.Add(item);
        allItemsLookup.Add(item.ItemID, item);
    }

    private Item GetItem(string itemID)
    {
        allItemsLookup.TryGetValue(itemID, out Item tempItem);

        return tempItem;
    }

    // For initializing purpose. Thus no need to clamp value.
    private void AddItem(string ItemID, int quantity)
    {
        Item tempItem = GetItem(ItemID);
        
        if (tempItem != null)
        {
            tempItem.amount = quantity;
        }
        else
        {
            tempItem = new Item(ItemID, quantity);

            AddInventoryEntry(tempItem);
        }
    }

    // For initializing purpose
    public void AddItem(Item item)
    {
        AddItem(item.ItemID, item.amount);
    }

    #endregion

    #region Ilmu Management

    //public bool HasIlmuPool(CharacterId characterId)
    //{
    //    for (int i = 0; i < playerIlmuPools.Count; i++)
    //    {
    //        if (playerIlmuPools[i].playerId == characterId)
    //            return true;
    //    }

    //    return false;
    //}

    //public void AddIlmuPool(PlayerIlmuPool playerIlmuPool)
    //{
    //    playerIlmuPools.Add(playerIlmuPool);
    //}

    #endregion

    //public PlayerEquipment GetPlayerEquipment(CharacterId characterId)
    //{
    //    playersEquipmentListLookup.TryGetValue(characterId, out PlayerEquipment playerEquipment);

    //    return playerEquipment;
    //}


    public void Save()
    {
        Debug.Log("Save Inventory");
        itemInInventory = PlayerInventoryManager.instance.itemsInInventory;
        //playerIlmuPools = PlayerInventoryManager.instance.playerIlmuPools;

        if (PlayerPartyManager.instance.partyMembers == null)
            return;
        // Only save equipments list & items list
        playersEquipmentList = new List<PlayerEquipment>();

        for(int i = 0; i < PlayerPartyManager.instance.partyMembers.Count; i++)
        {
            //PlayerEntity playerEntity = PlayerPartyManager.instance.partyMembers[i];
            ControllableEntity playerEntity = PlayerPartyManager.instance.partyMembers[i];

            playersEquipmentList.Add(playerEntity.playerEquipment);
        }

    }
}
