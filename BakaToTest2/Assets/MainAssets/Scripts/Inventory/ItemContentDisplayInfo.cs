﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemContentDisplayInfo : MonoBehaviour
{
    //UI (may put sprite reference here later)
    public Text numberText;
    public Text ItemIDText;
    public Text itemTypeText;
    public bool IsToggle
    {
        get
        {
            return GetComponentInChildren<Toggle>().isOn;
        }
    }

    public void Serialize(string number, string ItemID, string itemType)
    {
        this.numberText.text = number;
        this.ItemIDText.text = ItemID;
        this.itemTypeText.text = itemType;
    }

    public void OnToggleValueChange()
    {
        if(IsToggle)
        {
            ItemAdminManager.AddToList(this);
        }
        else
        {
            ItemAdminManager.RemoveFromList(this);
        }
    }

}
