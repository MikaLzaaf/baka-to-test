﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public static class ItemDataManager
{
    private const string DefaultInventoryDirectoryInResources = "Items/Data/DefaultInventoryInfo";
    //private const string DefaultIlmuPoolListDirectoryInResources = "Items/Data/DefaultIlmuPoolListInfo";
    private const string ItemDescriptionInResources = "Items/ItemDescription";
    //private const string ItemIconDirectoryInResources = "Images/ListItemIcons/";

    private const string ItemDescriptionKeyHeader = "ItemID";
    private const int ItemDescriptionKeyIndex = 0;

    private static Dictionary<string, Dictionary<string, string>> _itemDescriptionLookup = new Dictionary<string, Dictionary<string, string>>();
    public static Dictionary<string, Dictionary<string, string>> itemDescriptionLookup
    {
        get
        {
            return _itemDescriptionLookup;
        }
        private set
        {
            _itemDescriptionLookup = value;
        }
    }


    private static string currentLanguage
    {
        get
        {
            return LocalizationManager.currentLanguage;
        }
    }



    static ItemDataManager()
    {
        Initialize();
    }


    public static void Initialize()
    {
        TextAsset dataAsset = Resources.Load<TextAsset>(ItemDescriptionInResources);

        if (dataAsset != null)
        {
            itemDescriptionLookup = LocalizationManager.LoadLocalizationData(dataAsset, ItemDescriptionKeyIndex, ItemDescriptionKeyHeader);
        }
    }


    public static string GetItemDescriptionLocalized(string key)
    {
        return GetItemDescriptionLocalized(key, currentLanguage);
    }


    public static string GetItemDescriptionLocalized(string key, string language)
    {
        if (string.IsNullOrEmpty(key))
        {
            return null;
        }

        if (HasLanguage(language))
        {
            Dictionary<string, string> dataList = null;

            if (itemDescriptionLookup.TryGetValue(language, out dataList))
            {
                string result = null;

                if (dataList.TryGetValue(key, out result))
                {
                    return result;
                }
            }
        }

        return key;
    }


    public static bool HasLanguage(string language)
    {
        return LocalizationManager.HasLanguage(language);
    }


    public static bool HasKey(string key)
    {
        return HasKey(key, currentLanguage);
    }


    public static bool HasKey(string key, string language)
    {
        Dictionary<string, string> dataTable = null;

        if (itemDescriptionLookup.TryGetValue(language, out dataTable))
        {
            return dataTable.ContainsKey(key);
        }

        return false;
    }


    public static DefaultInventoryInfo LoadDefaultInventoryInfo()
    {
        DefaultInventoryInfo defaultInventoryInfo = DataSerializer.DeserializeXmlInResources<DefaultInventoryInfo>(DefaultInventoryDirectoryInResources);

        return defaultInventoryInfo;
    }

    //public static DefaultIlmuPoolListInfo LoadDefaultIlmuPoolListInfo()
    //{
    //    DefaultIlmuPoolListInfo defaultIlmuPoolListInfo = DataSerializer.DeserializeXmlInResources<DefaultIlmuPoolListInfo>(DefaultIlmuPoolListDirectoryInResources);

    //    return defaultIlmuPoolListInfo;
    //}
}
