﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemAdminManager : MonoBehaviour
{
    public static List<ItemContentDisplayInfo> IsToggleList = new List<ItemContentDisplayInfo>();

    private ItemBaseValue[] itemListReference;

    //may private, load from resource(optional if more faster)
    public GameObject itemContentPrefab;

    public Transform listParent;

    // Start is called before the first frame update
    void Start()
    {
        LoadAllItem();
        
    }

    //Load all item sciptableObject to reference list and then list it out
    //Items scriptable store in Resources folder
    private void LoadAllItem()
    {
        itemListReference = Resources.LoadAll<ItemBaseValue>("Items");

        if(itemListReference == null)
        {
            Debug.LogWarning("No item found in folder items");
            return;
        }

        //for(int i = 0; i < itemListReference.Length; i++)
        //{
        //    GameObject item = Instantiate(itemContentPrefab, listParent);
        //    item.GetComponent<ItemContentDisplayInfo>().Serialize((i + 1).ToString(), itemListReference[i].ItemID, itemListReference[i].ItemType.ToString());
        //}
    }

    #region May put in utility script

    public static void AddToList(ItemContentDisplayInfo item)
    {
        for(int i = 0; i < IsToggleList.Count; i++)
        {
            if(IsToggleList[i] == item)
            {
                Debug.LogWarning(string.Format("Item {0} already in the list...", item.ItemIDText.text));
                return;
            }
        }

        IsToggleList.Add(item);
    }

    public static void RemoveFromList(ItemContentDisplayInfo item)
    {
        for (int i = 0; i < IsToggleList.Count; i++)
        {
            if (IsToggleList[i] == item)
            {
                IsToggleList.Remove(item);
                return;
            }
        }

        Debug.LogWarning(string.Format("Item {0} not in the list...", item.ItemIDText.text));
    }

    #endregion
}
