﻿public enum EquipmentSection
{
    Undefined,
    WritingInstrument,
    Notepad,
    Shoes,
    Accessory
}