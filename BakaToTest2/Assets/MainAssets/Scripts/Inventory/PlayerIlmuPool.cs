﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class PlayerIlmuPool 
{
    public const int MaxIlmuQuantity = 999999;
    public const string IlmuPrefix = "Ilmu_";

    public CharacterId playerId;

    //[SerializeField] private List<string> equippedItemIds = default;

    [SerializeField] private List<Item> _ilmuOwned;
    public List<Item> ilmuOwned
    {
        get
        {
            if (_ilmuOwned == null)
                _ilmuOwned = new List<Item>();

            return _ilmuOwned;
        }
        private set
        {
            _ilmuOwned = value;
        }
    }

    private Dictionary<Subjects, Item> _ilmuLookup;
    private Dictionary<Subjects, Item> ilmuLookup
    {
        get
        {
            if (_ilmuLookup == null)
            {
                _ilmuLookup = new Dictionary<Subjects, Item>();

                if (ilmuOwned != null)
                {
                    foreach (Item item in ilmuOwned)
                    {
                        IlmuItemBaseValue ilmuBase = (IlmuItemBaseValue)item.itemValue;

                        _ilmuLookup.Add(ilmuBase.subject, item);
                    }
                }
            }

            return _ilmuLookup;
        }
    }

    public PlayerIlmuPool() { }

    public PlayerIlmuPool(CharacterId characterId, List<Item> ilmuItems)
    {
        playerId = characterId;
        ilmuOwned = ilmuItems;
    }


    public Item GetIlmu(Subjects subject)
    {
        ilmuLookup.TryGetValue(subject, out Item ilmu);
        return ilmu;
    }

    private void AddIlmuEntry(Item item, Subjects subject)
    {
        ilmuOwned.Add(item);
        ilmuLookup.Add(subject, item);
    }

    public void AddIlmu(Subjects subject, int amount)
    {
        Item ilmu = GetIlmu(subject);

        if (ilmu != null)
        {
            if (ilmu.amount < MaxIlmuQuantity)
            {
                ilmu.amount = Mathf.Clamp(ilmu.amount + amount, ilmu.amount, MaxIlmuQuantity);
            }
        }
        else
        {
            string itemId = IlmuPrefix + subject.ToString();

            ilmu = new Item(itemId, amount);

            AddIlmuEntry(ilmu, subject);
        }
    }

    public void DeductIlmu(Subjects subject, int amount)
    {
        Item ilmu = GetIlmu(subject);

        if (ilmu != null)
        {
            ilmu.amount = Mathf.Clamp(ilmu.amount - amount, 0, MaxIlmuQuantity);
        }
    }

    public void DeductIlmu(IlmuCost[] ilmuCosts)
    {
        for (int i = 0; i < ilmuCosts.Length; i++)
        {
            DeductIlmu(ilmuCosts[i].subject, ilmuCosts[i].cost);
        }
    }
}
