﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class PlayerEquipment
{
    public CharacterId playerId;

    [SerializeField] private List<string> equippedItemIds = default;

    private List<Item> _equippedItems;
    public List<Item> equippedItems
    {
        get
        {
            if(_equippedItems == null)
            {
                _equippedItems = new List<Item>();

                if(equippedItemIds != null)
                {
                    for (int i = 0; i < equippedItemIds.Count; i++)
                    {
                        Item equipment = PlayerInventoryManager.instance.GetItem(equippedItemIds[i]);

                        if (equipment != null)
                            _equippedItems.Add(equipment);
                    }
                }
            }   

            return _equippedItems;
        }
        private set
        {
            _equippedItems = value;
        }
    }

    private Dictionary<EquipmentSection, Item> _equippedItemLookup;
    private Dictionary<EquipmentSection, Item> equippedItemLookup
    {
        get
        {
            if (_equippedItemLookup == null)
            {
                _equippedItemLookup = new Dictionary<EquipmentSection, Item>();

                if (equippedItems != null)
                {
                    foreach (Item item in equippedItems)
                    {
                        EquipmentItemBaseValue equipmentBase = item.itemValue as EquipmentItemBaseValue;

                        _equippedItemLookup.Add(equipmentBase.EquipmentSection, item);
                    }
                }
            }

            return _equippedItemLookup;
        }
    }

    public PlayerEquipment() { }

    public PlayerEquipment(CharacterId characterId)
    {
        playerId = characterId;
    }


    public Item GetEquippedItem(EquipmentSection section)
    {
        equippedItemLookup.TryGetValue(section, out Item equipment);

        return equipment;
    }


    public bool IsSectionEquipped(EquipmentSection section)
    {
        Item equipment = GetEquippedItem(section);

        return equipment != null;
    }


    public void EquipItem(Item newEquipment)
    {
        EquipmentItemBaseValue equipmentBase = newEquipment.itemValue as EquipmentItemBaseValue;

        EquipmentSection section = equipmentBase.EquipmentSection;

        Item equippedItem = GetEquippedItem(section);
     
        if (equippedItem != null)
        {
            equippedItems.Remove(equippedItem);
            equippedItemIds.Remove(equippedItem.ItemID);

            equippedItems.Add(newEquipment);
            equippedItemIds.Add(newEquipment.ItemID);

            equippedItemLookup[section] = newEquipment;

            PlayerInventoryManager.instance.UnequipItem(equippedItem.ItemID);
        }
        else
        {
            equippedItemLookup.Add(section, newEquipment);
            equippedItems.Add(newEquipment);
            equippedItemIds.Add(newEquipment.ItemID);
        }

        newEquipment.Equip();

        // Update player stats data
        ControllableEntity playerEntity = PlayerPartyManager.instance.GetPartyMember(playerId);

        playerEntity.ApplyNewStatsData();
    }


    public void UnequipItem(EquipmentSection section)
    {
        Item equippedItem = GetEquippedItem(section);

        if (equippedItem != null)
        {
            equippedItems.Remove(equippedItem);
            equippedItemLookup.Remove(section);
            equippedItemIds.Remove(equippedItem.ItemID);

            PlayerInventoryManager.instance.UnequipItem(equippedItem.ItemID);
        }

        ControllableEntity playerEntity = PlayerPartyManager.instance.GetPartyMember(playerId);

        playerEntity.ApplyNewStatsData();
    }
}
