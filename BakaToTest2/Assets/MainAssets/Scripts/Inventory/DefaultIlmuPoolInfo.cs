﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DefaultIlmuPoolInfo 
{
    public CharacterId characterId;
    public List<Item> defaultIlmuItems;
}
