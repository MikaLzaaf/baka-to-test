﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

namespace Intelligensia.Battle
{
    public class BattleCameraUnit : MonoBehaviour
    {


        [SerializeField] protected CinemachineVirtualCamera vCamera = default;
        [SerializeField] protected CinemachineTargetGroup targetGroup = default;

        [SerializeField] protected BattleCameraID _cameraID;
        [SerializeField] protected CameraViewportLerper splitCameraLerper = default;

        [Header("Orbit")]
        [SerializeField] private bool orbitWhenLive = false;
        [SerializeField] private float orbitalSpeed = 2f;

        public BattleCameraID cameraID { get => _cameraID; }

        private BattleCameraTransition[] cameraTransitions;
        private CinemachineOrbitalTransposer orbitalTransposer;

        private int orbitPosition = 0;
        private bool isLive;

        private void Start()
        {
            cameraTransitions = GetComponents<BattleCameraTransition>();

            if (orbitWhenLive && vCamera != null)
                orbitalTransposer = vCamera.GetCinemachineComponent<CinemachineOrbitalTransposer>();
        }


        private void Update()
        {
            if(orbitWhenLive && isLive)
            {
                orbitalTransposer.m_XAxis.Value += orbitalSpeed * Time.deltaTime;
            }
        }

        public void SetFollowTarget(Transform target)
        {
            if(vCamera != null)
            {
                vCamera.m_Follow = target;
            }
        }

        public void SetLookTarget(Transform target)
        {
            if (vCamera != null)
            {
                vCamera.m_LookAt = target;
            }
        }

        public void SetFollowAndLookTarget(Transform target)
        {
            if (vCamera != null)
            {
                vCamera.m_Follow = target;
                vCamera.m_LookAt = target;
            }
        }

        public void PrepareTargetGroup(BattleEntity[] battleEntities)
        {
            if (targetGroup == null)
                return;

            var prevTargets = targetGroup.m_Targets;

            if (prevTargets.Length > 0)
            {
                for (int i = prevTargets.Length - 1; i >= 0; i--)
                {
                    targetGroup.RemoveMember(prevTargets[i].target);
                }
            }

            for (int i = 0; i < battleEntities.Length; i++)
            {
                targetGroup.AddMember(battleEntities[i].transform, 1f, 1f);
            }
        }

        //public void PreparePostSkillTargets()
        //{
        //    if (targetGroup == null)
        //        return;

        //    var prevTargets = targetGroup.m_Targets;

        //    if (prevTargets.Length > 0)
        //    {
        //        for (int i = prevTargets.Length - 1; i >= 0; i--)
        //        {
        //            targetGroup.RemoveMember(prevTargets[i].target);
        //        }
        //    }

        //    //Transform mainTarget = BattleInfoManager.entityOnTurn.transform;
        //    //targetGroup.AddMember(mainTarget, 2f, 0.5f);

        //    BattleEntity[] sideTargets = BattleInfoManager.GetTargetsSelected().ToArray();

        //    for (int i = 0; i < sideTargets.Length; i++)
        //    {
        //        targetGroup.AddMember(sideTargets[i].transform, 1f, 0f);
        //    }
        //}


        public void ActivateUnit(bool activate)
        {
            orbitPosition = 0;

            if (vCamera != null)
                isLive = activate;

            if (cameraTransitions == null || cameraTransitions.Length == 0)
                return;

            for(int i = 0; i < cameraTransitions.Length; i++)
            {
                if(activate)
                {
                    cameraTransitions[i].BeginTransition();
                }
                else
                {
                    cameraTransitions[i].StopTransition();
                }
            }
        }


       
        public void SetOrbitValue(float value)
        {
            CinemachineOrbitalTransposer orbital = vCamera.GetCinemachineComponent<CinemachineOrbitalTransposer>();

            if (orbital == null)
                return;

            orbital.m_XAxis.Value = value;
        }

        public void ChangeOrbitValue(bool turnToPlayerSide)
        {
            // Horizontally for now
            // Orbit Pos 0 = 50 (Player Side)
            // Orbit Pos 1 = 140 (Enemy Side)
            // Orbit Pos 2 = -140 (Enemy Side)
            // Orbit Pos 3 = -50 (Player Side)

            CinemachineOrbitalTransposer orbital = vCamera.GetCinemachineComponent<CinemachineOrbitalTransposer>();

            if (orbital == null)
                return;

            if(orbitPosition == 0 || orbitPosition == 2)
            {
                orbitPosition = turnToPlayerSide ? 3 : 1;

                orbital.m_XAxis.Value = turnToPlayerSide ? -50f : 140f;
            }
            else if (orbitPosition == 1 || orbitPosition == 3)
            {
                orbitPosition = turnToPlayerSide ? 0 : 2;

                orbital.m_XAxis.Value = turnToPlayerSide ? 50f : -140f;
            }
           
        }

        public void ToggleSplitCamera(bool active)
        {
            if(splitCameraLerper != null)
            {
                splitCameraLerper.StartLerping(active);
            }
        }

        public void ChangeCameraBodyFollowOffsetZ(float value)
        {
            if (vCamera == null)
                return;

            var body = vCamera.GetCinemachineComponent<CinemachineOrbitalTransposer>();
            body.m_FollowOffset.z = value;
        }
    }
}

