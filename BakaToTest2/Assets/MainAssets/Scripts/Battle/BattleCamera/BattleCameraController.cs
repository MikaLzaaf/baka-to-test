﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

namespace Intelligensia.Battle
{
    public class BattleCameraController : Singleton<BattleCameraController>
    {
        public enum MultipleTargetCameraEnum
        {
            AllEnemies,
            AllPlayers,
            All
        }


        
        //private BattleCameraUnit[] battleCameras = default;

        private BattleCameraUnit currentCameraUnit = null;


        private Dictionary<BattleCameraID, BattleCameraUnit> cameraLookup = new Dictionary<BattleCameraID, BattleCameraUnit>();


        private void Awake()
        {
            if (transform.childCount == 0)
                return;

            for (int i = 0; i < transform.childCount; i++)
            {
                BattleCameraUnit cameraUnit = transform.GetChild(i).GetComponent<BattleCameraUnit>();

                if(cameraUnit != null)
                    cameraLookup.Add(cameraUnit.cameraID, cameraUnit);
            }

#if UNITY_EDITOR
            if (transform.childCount != cameraLookup.Count)
                Debug.LogError("The amount of BattleCameraUnit is not equal to the children count.");
#endif
        }

        private BattleCameraUnit GetCameraUnit(BattleCameraID cameraID)
        {
            cameraLookup.TryGetValue(cameraID, out BattleCameraUnit cameraUnit);

            return cameraUnit;
        }

        public void SetLongIdleCamera(BattleEntity[] battleEntities)
        {
            BattleCameraUnit longIdleCamera = GetCameraUnit(BattleCameraID.LongIdle);

            longIdleCamera.PrepareTargetGroup(battleEntities);
        }

        public void SetPlayerIdleCamera(Transform target)
        {
            BattleCameraUnit playerIdleCamera = GetCameraUnit(BattleCameraID.PlayerIdle);

            playerIdleCamera.SetFollowAndLookTarget(target);
        }

        public void SetEnemyTurnDecisionCamera(Transform target)
        {
            BattleCameraUnit enemyTurnDecisionCamera = GetCameraUnit(BattleCameraID.EnemyTurnDecision);

            enemyTurnDecisionCamera.SetFollowAndLookTarget(target);
            //enemyTurnDecisionCamera.ChangeCameraBodyFollowOffsetZ();
        }

        //public void SetSplitCameraPlayer(Transform target)
        //{
        //    BattleCameraUnit splitCam = GetCameraUnit(BattleCameraID.FaceOffSplit);

        //    splitCam.SetFollowAndLookTarget(target);
        //}

        public void SetTargetSelectionCamera(Transform target, bool targetIsEnemy)
        {
            BattleCameraUnit selectedCameraUnit = targetIsEnemy ?
                GetCameraUnit(BattleCameraID.SingleSelectEnemyView) : 
                GetCameraUnit(BattleCameraID.SingleSelectPlayerView);

            selectedCameraUnit.SetFollowAndLookTarget(target);
        }


        public void SetMultipleTargetsCamera(MultipleTargetCameraEnum targetCameraEnum, BattleEntity[] targets)
        {
            if (targets.Length == 0)
                return;

            BattleCameraUnit selectedCameraUnit = null;

            if (targetCameraEnum == MultipleTargetCameraEnum.AllEnemies)
            {
                selectedCameraUnit = GetCameraUnit(BattleCameraID.AllEnemiesView);
            }
            else if (targetCameraEnum == MultipleTargetCameraEnum.AllPlayers)
            {
                selectedCameraUnit = GetCameraUnit(BattleCameraID.AllPlayersView);
            }
            else if (targetCameraEnum == MultipleTargetCameraEnum.All)
            {
                selectedCameraUnit = GetCameraUnit(BattleCameraID.AllCharactersView);
            }

            if(selectedCameraUnit != null)
            {
                selectedCameraUnit.PrepareTargetGroup(targets);
            }
        }

        public void SetWinCamera(BattleEntity[] battleEntities)
        {
            BattleCameraUnit winCamera = GetCameraUnit(BattleCameraID.Win);

            winCamera.PrepareTargetGroup(battleEntities);
        }

        public void SetLosingCamera(BattleEntity[] battleEntities)
        {
            BattleCameraUnit losingCamera = GetCameraUnit(BattleCameraID.Lose);

            losingCamera.PrepareTargetGroup(battleEntities);
        }

        public void SetTurnExecutionCamera(BattleEntity[] targets)
        {
            if(targets.Length == 1)
            {
                BattleCameraUnit selectedCameraUnit = GetCameraUnit(BattleCameraID.TurnExecution);

                selectedCameraUnit.SetFollowAndLookTarget(targets[0].transform);
            }
            else
            {
                BattleCameraUnit selectedCameraUnit = GetCameraUnit(BattleCameraID.MultipleTargetTurnExecution);

                selectedCameraUnit.PrepareTargetGroup(targets);

                float orbitValue = targets[0].entityTag == EntityTag.Enemy ? 0f : 180f;

                selectedCameraUnit.SetOrbitValue(orbitValue);
            }
        }

        //public void SetFaceOffCamera(BattleEntity playerSide, BattleEntity enemySide)
        //{
        //    BattleCameraUnit faceOffCameraUnit = GetCameraUnit(BattleCameraID.FaceOff);

        //    List<BattleEntity> targets = new List<BattleEntity>();

        //    if (playerSide != null)
        //        targets.Add(playerSide);

        //    if (enemySide != null)
        //        targets.Add(enemySide);

        //    faceOffCameraUnit.PrepareTargetGroup(targets.ToArray());
        //}

        // Used by the camera when live
        public void SetCurrentActiveCamera(BattleCameraUnit cameraUnit)
        {
            if(currentCameraUnit != null)
            {
                currentCameraUnit.ActivateUnit(false);
            }

            currentCameraUnit = cameraUnit;

            currentCameraUnit.ActivateUnit(true);
        }

        //public void SetStaggeredCamera(Transform target)
        //{
        //    BattleCameraUnit staggeredCamera = GetCameraUnit(BattleCameraID.Staggered);

        //    if(staggeredCamera != null)
        //    {
        //        staggeredCamera.SetFollowAndLookTarget(target);
        //    }
        //}

        public void SetNewClassmateEntersCamera(Transform target)
        {
            BattleCameraUnit newClassmateCamera = GetCameraUnit(BattleCameraID.NewClassmateEnters);

            if(newClassmateCamera != null)
                newClassmateCamera.SetFollowAndLookTarget(target);
        }

        public void SetDebateFocusCamera(Transform target, int debateIndex)
        {
            BattleCameraUnit debaterFocusCamera = debateIndex == 1 ? GetCameraUnit(BattleCameraID.DebaterPlayer) : 
                GetCameraUnit(BattleCameraID.DebaterEnemy);

            if (debaterFocusCamera != null)
                debaterFocusCamera.SetFollowAndLookTarget(target);
        }

        public void SetItemExecutionCameraOne(Transform target)
        {
            BattleCameraUnit useItemCamera1 = GetCameraUnit(BattleCameraID.UseItem1);

            if (useItemCamera1 != null)
                useItemCamera1.SetFollowAndLookTarget(target);
        }

        public void SetItemExecutionCameraTwo(Transform target)
        {
            BattleCameraUnit useItemCamera2 = GetCameraUnit(BattleCameraID.UseItem2);

            if (useItemCamera2 != null)
                useItemCamera2.SetFollowAndLookTarget(target);
        }
    }
}
