﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

namespace Intelligensia.Battle
{
    public class DoOrbitMovement : BattleCameraTransition
    {
        public float maxXMovement;
        public float maxYMovement;

        public float movementDuration;

        public float minStartingValue = 0f;
        public float maxStartingValue = 20f;

        public bool randomHorizontalDirection = false;
        public bool randomStartingValue = false;

        private CinemachineVirtualCamera vCamera = default;
        private CinemachineOrbitalTransposer orbital;

        private float elapsedTimeSinceStarted = 0f;
        private float horizontalDestinationValue = 0f;

        private bool isLive = false;

        private void Start()
        {
            vCamera = GetComponent<CinemachineVirtualCamera>();

            if (vCamera != null)
            {
                orbital = vCamera.GetCinemachineComponent<CinemachineOrbitalTransposer>();
            }
        }

        private void Update()
        {
            if (!isLive || orbital == null)
                return;

            elapsedTimeSinceStarted += UnityEngine.Time.deltaTime;

            if (elapsedTimeSinceStarted < movementDuration)
            {
                orbital.m_XAxis.Value = Mathf.Lerp(0f, horizontalDestinationValue, elapsedTimeSinceStarted / movementDuration);
            }
            else
            {
                orbital.m_XAxis.Value = horizontalDestinationValue;
            }
        }


        public override void BeginTransition()
        {
            ResetOrbitAxis();

            if (randomStartingValue)
            {
                if(orbital != null)
                {
                    orbital.m_XAxis.Value = Random.Range(minStartingValue, maxStartingValue);
                }
            }

            if (randomHorizontalDirection)
            {
                horizontalDestinationValue = Random.value > 0.5f ? maxXMovement : -maxXMovement;

                horizontalDestinationValue += orbital.m_XAxis.Value;
            }
            else
            {
                horizontalDestinationValue = maxXMovement;
            }

            elapsedTimeSinceStarted = 0f;
            isLive = true;
        }


        public override void StopTransition()
        {
            isLive = false;

            ResetOrbitAxis();
        }


        private void ResetOrbitAxis()
        {
            if (orbital != null)
            {
                orbital.m_XAxis.Value = 0f;
            }
        }
    }
}
