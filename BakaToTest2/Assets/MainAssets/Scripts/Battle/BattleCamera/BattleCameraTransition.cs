﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Intelligensia.Battle
{
    public abstract class BattleCameraTransition : MonoBehaviour
    {
        public abstract void BeginTransition();
        public abstract void StopTransition();
    }
}

