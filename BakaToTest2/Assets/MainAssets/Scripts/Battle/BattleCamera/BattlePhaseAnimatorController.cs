﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Intelligensia.Battle
{
    public class BattlePhaseAnimatorController : MonoBehaviour
    {
        // Used in Base Layer
        private readonly int AnimParam_NewTurn = Animator.StringToHash("newTurn");
        private readonly int AnimParam_NextPhase = Animator.StringToHash("nextPhase");
        private readonly int AnimParam_BattleResult = Animator.StringToHash("battleResult");
        private readonly int AnimParam_BattleOver = Animator.StringToHash("battleOver");
        private readonly int AnimParam_Unloading = Animator.StringToHash("unloading");
        private readonly int AnimParam_AddNewClassmate = Animator.StringToHash("addNewClassmate");

        // Used in Battle State Sub-Layer
        private readonly int AnimParam_IsPlayerTurn = Animator.StringToHash("isPlayerTurn");
        private readonly int AnimParam_TargetIsEnemy = Animator.StringToHash("targetIsEnemy");
        private readonly int AnimParam_EnemyActionIndex = Animator.StringToHash("enemyActionIndex");
        private readonly int AnimParam_LongIdle = Animator.StringToHash("hasLongIdle");
        private readonly int AnimParam_SelectTarget = Animator.StringToHash("isSelectingTarget");
        private readonly int AnimParam_TargetIsSelected = Animator.StringToHash("targetIsSelected");
        private readonly int AnimParam_PlayerActionIndex = Animator.StringToHash("playerActionIndex");
        private readonly int AnimParam_IsSelectingSingleTarget = Animator.StringToHash("isSelectingSingleTarget");
        private readonly int AnimParam_FreeExecutionIndex = Animator.StringToHash("freeExecutionIndex");
        private readonly int AnimParam_IsStaggered = Animator.StringToHash("isStaggered");
        private readonly int AnimParam_IsFreeHit = Animator.StringToHash("isFreeHit");

        // Used in Sub-Layer

        private readonly int AnimParam_StartDebate = Animator.StringToHash("startDebate");
        private readonly int AnimParam_DebateIndex = Animator.StringToHash("debateIndex");
        private readonly int AnimParam_IsMultipleTargetExecution = Animator.StringToHash("isMultipleTargetExecution");


        public Animator battleCamAnimator;


        public void NextPhase(bool active)
        {
            if (battleCamAnimator != null)
            {
                battleCamAnimator.SetBool(AnimParam_NextPhase, active);
            }
        }

        public void NewTurn(bool isPlayerTurn)
        {
            if (battleCamAnimator != null)
            {
                battleCamAnimator.SetBool(AnimParam_NewTurn, true);
                battleCamAnimator.SetBool(AnimParam_IsPlayerTurn, isPlayerTurn);
            }

            IsTargetSelected(false);
        }


        public void CloseNewTurn()
        {
            if (battleCamAnimator != null)
            {
                battleCamAnimator.SetBool(AnimParam_NewTurn, false);
            }
        }


        public void LongIdle(bool isLongIdle)
        {
            if (battleCamAnimator != null)
            {
                battleCamAnimator.SetBool(AnimParam_LongIdle, isLongIdle);
            }
        }


        public void SelectTarget(bool isSelectingTarget, bool targetIsEnemy = true)
        {
            if (battleCamAnimator != null)
            {
                battleCamAnimator.SetBool(AnimParam_TargetIsEnemy, targetIsEnemy);
                battleCamAnimator.SetBool(AnimParam_SelectTarget, isSelectingTarget);
            }
        }


        public void IsTargetSelected(bool isSelected)
        {
            if (battleCamAnimator != null)
            {
                battleCamAnimator.SetBool(AnimParam_TargetIsSelected, isSelected);
            }
        }


        public void SetPlayerActionIndex(int index)
        {
            if (battleCamAnimator != null)
            {
                battleCamAnimator.SetInteger(AnimParam_PlayerActionIndex, index);
            }
        }


        public void SetEnemyActionIndex(BattleSelectionType selectionType)
        {
            int index;

            if(selectionType == BattleSelectionType.Attack)
            {
                index = 1;
            }
            else if (selectionType == BattleSelectionType.Support)
            {
                index = 2;
            }
            else if (selectionType == BattleSelectionType.Item)
            {
                index = 3;
            }
            else if (selectionType == BattleSelectionType.Counter)
            {
                index = 4;
            }
            else if (selectionType == BattleSelectionType.Forfeit)
            {
                index = 5;
            }
            else if (selectionType == BattleSelectionType.Order)
            {
                index = 6;
            }
            else
            {
                index = -1;
            }

            if (battleCamAnimator != null)
            {
                battleCamAnimator.SetInteger(AnimParam_EnemyActionIndex, index);
            }
        }


        public void SetTargetSelectionRange(bool isSelectingSingleTarget)
        {
            if (battleCamAnimator != null)
            {
                battleCamAnimator.SetBool(AnimParam_IsSelectingSingleTarget, isSelectingSingleTarget);
            }
        }

        public void StartDebate(bool start)
        {
            if (battleCamAnimator != null)
            {
                battleCamAnimator.SetBool(AnimParam_StartDebate, start);
            }
        }

        public void SetDebateResult(int resultIndex)
        {
            if(battleCamAnimator != null)
            {
                battleCamAnimator.SetInteger("debateWinnerIndex", resultIndex);
            }
        }

        public void UseCounter()
        {
            SelectTarget(true, false);
            SetPlayerActionIndex(0);
            IsTargetSelected(true);
        }

        public void UsePartyAttack()
        {
            SelectTarget(true);
            SetPlayerActionIndex(4);
            IsTargetSelected(true);
        }


        public void SetBattleResult(int result)
        {
            if (battleCamAnimator != null)
            {
                battleCamAnimator.SetInteger(AnimParam_BattleResult, result);
                battleCamAnimator.SetTrigger(AnimParam_BattleOver);
            }
        }


        public void Unloading()
        {
            if (battleCamAnimator != null)
            {
                battleCamAnimator.SetTrigger(AnimParam_Unloading);
            }
        }


        public void SetFreeExecutionIndex(int index)
        {
            if (battleCamAnimator != null)
            {
                battleCamAnimator.SetInteger(AnimParam_FreeExecutionIndex, index);
            }
        }

        public void SetStaggered(bool isStaggered)
        {
            if(battleCamAnimator != null)
            {
                battleCamAnimator.SetBool(AnimParam_IsStaggered, isStaggered);
            }
        }

        public void SetFreeHit(bool isFreeHit)
        {
            if (battleCamAnimator != null)
            {
                battleCamAnimator.SetBool(AnimParam_IsFreeHit, isFreeHit);
            }
        }

        public void AddNewClassmate(bool add)
        {
            if (battleCamAnimator != null)
                battleCamAnimator.SetBool(AnimParam_AddNewClassmate, add);
        }

        public void DebateFocus(int debateIndex)
        {
            // 0 = Orbit, 1 = Player, 2 = Enemy
            if (battleCamAnimator != null)
                battleCamAnimator.SetInteger(AnimParam_DebateIndex, debateIndex);
        }

        public void SetExecution(bool isMultipleTargetExecution)
        {
            if (battleCamAnimator != null)
                battleCamAnimator.SetBool(AnimParam_IsMultipleTargetExecution, isMultipleTargetExecution);
        }
    }
}

