﻿
namespace Intelligensia.Battle
{
    public enum BattleCameraID
    {
        PlayerIdle,
        LongIdle,
        SingleSelectEnemyView,
        SingleSelectPlayerView,
        TurnExecution,
        AllEnemiesView,
        AllPlayersView,
        AllCharactersView,
        Win,
        Lose,
        EnemyTurnDecision,
        NewClassmateEnters,
        DebaterPlayer,
        DebaterEnemy,
        MultipleTargetTurnExecution,
        UseItem1,
        UseItem2,
        UseOrder
    }
}

