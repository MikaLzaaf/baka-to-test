﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Intelligensia.Battle
{
    [Serializable]
    public class PlayerTurnSequencePool
    {
        private const int MaxSequenceAmount = 2;

        public string id;
        public List<PlayerTurnSequence> turnSequences;


        public PlayerTurnSequencePool(string id)
        {
            this.id = id;
            turnSequences = new List<PlayerTurnSequence>();
        }


        public PlayerTurnSequence GetAvailableSequence()
        {
            for (int i = 0; i < turnSequences.Count; i++)
            {
                if (!turnSequences[i].IsShowing)
                {
                    return turnSequences[i];
                }
            }

            return null;
        }


        public void AddSequence(PlayerTurnSequence playerTurnSequence)
        {
            if (turnSequences.Count == MaxSequenceAmount)
                return;

            turnSequences.Add(playerTurnSequence);
        }

        public bool HasEnoughSequence()
        {
            return turnSequences.Count == MaxSequenceAmount;
        }
    }


    [Serializable]
    public class EnemyTurnSequencePool
    {
        private const int MaxSequenceAmount = 2;

        public string id;
        public List<EnemyTurnSequence> turnSequences;


        public EnemyTurnSequencePool(string id)
        {
            this.id = id;
            turnSequences = new List<EnemyTurnSequence>();
        }

        public EnemyTurnSequence GetAvailableSequence()
        {
            for (int i = 0; i < turnSequences.Count; i++)
            {
                if (!turnSequences[i].IsShowing)
                {
                    return turnSequences[i];
                }
            }

            return null;
        }


        public void AddSequence(EnemyTurnSequence enemyTurnSequence)
        {
            if (turnSequences.Count == MaxSequenceAmount)
                return;

            turnSequences.Add(enemyTurnSequence);
        }

        public bool HasEnoughSequence()
        {
            return turnSequences.Count == MaxSequenceAmount;
        }
    }
}
