﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Intelligensia.Battle
{
    public class OrderTurnSequence : TurnSequence
    {
        [Header("Order")]
        [SerializeField] protected int orderIndex = 0;

        [Header("Channels To Raise")]
        [SerializeField] protected FreeExecutionEndedEventChannelSO orderExecutedEventChannel;
        [SerializeField] protected UseOrderInBattleEventChannelSO useOrderInBattleEventChannel;

        [Header("Point")]
        [SerializeField] protected Transform holderSpawnPoint;

        private bool hasStarted = false;
        private bool orderHolderIsNPC = false;


        public override void Initialize(string skillId, Actor mainActor, Actor[] sideActors)
        {
            ClearTargetGroupContainer();

            InitializeActors(mainActor, sideActors);
            InitializeCameras();
        }


        protected override void InitializeActors(Actor mainActor, Actor[] sideActors)
        {
            if (!mainActor.gameObject.activeInHierarchy)
            {
                orderHolderIsNPC = true;

                this.mainActor = Instantiate(mainActor, holderSpawnPoint.position, holderSpawnPoint.rotation)
                    as Actor;
            }
            else
            {
                this.mainActor = mainActor;
                orderHolderIsNPC = false;
            }

            this.sideActors = sideActors;
        }

        public override void StartActing()
        {
            if (!hasStarted)
            {
                hasStarted = true;

                mainActor.DoOrderAnimation(true);
                //mainActor.StartSkillAnimation(skillIndex);
            }
        }


        public override void NextAct()
        {
            mainActor.DoOrderAnimation(false);
        }

        protected override void InitializeCameras()
        {

        }

        public override void FinishingAct()
        {
            base.FinishingAct();

            orderExecutedEventChannel?.RaiseEvent();

            hasStarted = false;

            if (orderHolderIsNPC)
            {
                Destroy(mainActor.gameObject);
            }
        }


        public void ApplyOrder()
        {
            //if (useOrderInBattleEventChannel != null)
            //{
            //    useOrderInBattleEventChannel.RaiseEvent(BattleInfoManager.selectedOrder);
            //}
        }
    }
}
