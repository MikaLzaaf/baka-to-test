﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

namespace Intelligensia.Battle
{
    public class EnemyTurnSequence : TurnSequence
    {
        [Header("Skill")]
        [SerializeField] protected string skillId = default;
        [SerializeField] protected int skillIndex = 0;

        //[Header("Channels To Raise")]
        //[SerializeField] protected TurnEndedEventChannelSO turnEndedEventChannel;

        private bool hasStarted = false;

        public override void Initialize(string skillId, Actor mainActor, Actor[] sideActors)
        {
            this.skillId = skillId;

            ClearTargetGroupContainer();

            InitializeActors(mainActor, sideActors);
            InitializeCameras();
        }


        protected override void InitializeCameras()
        {
            StartCoroutine(InitializingCameras());
        }

        private IEnumerator InitializingCameras()
        {
            vCameras = new List<CinemachineVirtualCamera>();
            CinemachineVirtualCameraBase[] vCameraBases = cameraParent.ChildCameras;
            for (int i = 0; i < vCameraBases.Length; i++)
            {
                CinemachineVirtualCamera vCam = vCameraBases[i].GetComponent<CinemachineVirtualCamera>();

                vCameras.Add(vCam);

                if (vCam.gameObject.tag == VCam_SingleTargetTag)
                {
                    vCam.m_Follow = mainActor.transform;
                    vCam.m_LookAt = mainActor.transform;
                }
                else if (vCam.gameObject.tag == VCam_TargetGroupTag)
                {
                    CinemachineTargetGroup targetGroup = Instantiate(targetGroupPrefab, targetGroupContainer);

                    targetGroup.AddMember(mainActor.transform, 1f, 0f);

                    for (int j = 0; j < sideActors.Length; j++)
                    {
                        targetGroup.AddMember(sideActors[j].transform, 1f, 0f);
                    }

                    vCam.m_Follow = targetGroup.transform;
                    vCam.m_LookAt = targetGroup.transform;
                }
                else if (vCam.gameObject.tag == VCam_DualTargetTag)
                {
                    vCam.m_Follow = mainActor.transform;
                    vCam.m_LookAt = sideActors[0].transform;
                }

                yield return new WaitForEndOfFrame();
            }
        }

        public override void StartActing()
        {
            if (!hasStarted)
            {
                hasStarted = true;

                mainActor.StartSkillAnimation(skillIndex);
            }
        }

        public override void FinishingAct()
        {
            base.FinishingAct();

            //turnEndedEventChannel?.RaiseEvent();

            hasStarted = false;
        }
    }
}
