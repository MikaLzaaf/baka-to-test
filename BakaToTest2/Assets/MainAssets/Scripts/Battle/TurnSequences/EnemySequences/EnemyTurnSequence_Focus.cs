﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

namespace Intelligensia.Battle
{
    public class EnemyTurnSequence_Focus : EnemyTurnSequence
    {
        public UseSkillInBattleEventChannelSO useSkillInBattleEventChannel;


        protected override void InitializeCameras()
        {
            base.InitializeCameras();

            // Initialize target for vcameras.
            vCameras = new List<CinemachineVirtualCamera>();
            CinemachineVirtualCameraBase[] vCameraBases = cameraParent.ChildCameras;
            for (int i = 0; i < vCameraBases.Length; i++)
            {
                CinemachineVirtualCamera vCam = vCameraBases[i].GetComponent<CinemachineVirtualCamera>();

                vCameras.Add(vCam);

                if (vCam.gameObject.tag == VCam_SingleTargetTag)
                {
                    vCam.m_Follow = mainActor.transform;
                    vCam.m_LookAt = mainActor.transform;
                }
                else if (vCam.gameObject.tag == VCam_TargetGroupTag)
                {
                    CinemachineTargetGroup targetGroup = Instantiate(targetGroupPrefab, targetGroupContainer);

                    for (int j = 0; j < sideActors.Length; j++)
                    {
                        targetGroup.AddMember(sideActors[j].transform, 1f, 0f);
                    }

                    vCam.m_Follow = targetGroup.transform;
                    vCam.m_LookAt = targetGroup.transform;
                }

            }
        }

        public void RaiseUseSkillEvent()
        {
            if (!BattleController.IsInBattle)
                return;

            //if (useSkillInBattleEventChannel != null)
            //{
            //    useSkillInBattleEventChannel.RaiseEvent(BattleInfoManager.selectedSkill.GetSkillId(),
            //        BattleInfoManager.GetTargetsSelected().ToArray(),
            //        BattleInfoManager.entityOnTurn);
            //}
        }
    }
}
