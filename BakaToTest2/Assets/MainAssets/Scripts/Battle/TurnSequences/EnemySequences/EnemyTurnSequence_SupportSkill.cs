﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Intelligensia.Battle
{
    public class EnemyTurnSequence_SupportSkill : EnemyTurnSequence
    {
        public UseSkillInBattleEventChannelSO useSkillInBattleEventChannel;

        public void RaiseUseSkillEvent()
        {
            BattleEntity skillUser = mainActor.GetComponent<BattleEntity>();

            useSkillInBattleEventChannel.RaiseEvent(BattleInfoManager.selectedSkill.GetSkillId(),
                BattleInfoManager.GetTargetsSelected().ToArray(),
                skillUser);
        }
    }
}

