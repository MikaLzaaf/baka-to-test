﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

namespace Intelligensia.Battle
{
    public class PlayerTurnSequence : TurnSequence
    {
        [Header("Skill")]
        [SerializeField] protected int skillIndex = 0;

        [Header("Minigame")]
        [SerializeField] protected SkillMinigame minigame;
        [SerializeField] protected Difficulty minigameDifficulty;
        [SerializeField] protected int vCamMinigameStarterIndex;
        [SerializeField] protected float minigameEndedDelayDuration = 0.5f;

        [Header("Channels To Raise")]
        [SerializeField] protected MinigameEndedEventChannelSO minigameEndedEventChannel;
        //[SerializeField] protected TurnEndedEventChannelSO turnEndedEventChannel;

        protected bool hasStarted = false;

        protected override void Initialize()
        {
            InitializeMinigame();
            InitializeCameras();

            Execute();
        }


        public override void Initialize(string skillId, Actor mainActor, Actor[] sideActors)
        {
            ClearTargetGroupContainer();

            InitializeMinigame();
            InitializeActors(mainActor, sideActors);
            InitializeCameras();
        }


        protected override void InitializeCameras()
        {
            // Initialize target for vcameras.
            vCameras = new List<CinemachineVirtualCamera>();
            CinemachineVirtualCameraBase[] vCameraBases = cameraParent.ChildCameras;
            for (int i = 0; i < vCameraBases.Length; i++)
            {
                CinemachineVirtualCamera vCam = vCameraBases[i].GetComponent<CinemachineVirtualCamera>();

                vCameras.Add(vCam);

                if (vCam.gameObject.tag == VCam_SingleTargetTag)
                {
                    vCam.m_Follow = mainActor.transform;
                    vCam.m_LookAt = mainActor.transform;
                }
                else if (vCam.gameObject.tag == VCam_TargetGroupTag)
                {
                    CinemachineTargetGroup targetGroup = Instantiate(targetGroupPrefab, targetGroupContainer);

                    targetGroup.AddMember(mainActor.transform, 1f, 0f);

                    for (int j = 0; j < sideActors.Length; j++)
                    {
                        targetGroup.AddMember(sideActors[j].transform, 1f, 0f);
                    }

                    vCam.m_Follow = targetGroup.transform;
                    vCam.m_LookAt = targetGroup.transform;
                }
                else if (vCam.gameObject.tag == VCam_DualTargetTag)
                {
                    vCam.m_Follow = mainActor.transform;
                    vCam.m_LookAt = sideActors[0].transform;
                }

            }
        }

        public override void StartActing()
        {
            if (!hasStarted)
            {
                hasStarted = true;

                mainActor.StartSkillAnimation(skillIndex);
            }
        }


        protected virtual void InitializeMinigame()
        {
            if(minigame != null)
            {
                minigame.Initialize(OnMinigameEnded, minigameDifficulty);
            }
        }

        public void StartMinigame()
        {
            minigame.gameObject.SetActive(true);

            if (hasEnded)
            {
                minigame.Replay();
                hasEnded = false;
            }
            else
            {
                minigame.StartMinigame();
            }
        }


        public void EndMinigame()
        {
            minigame.EndMinigame();
        }

        private void OnMinigameEnded(int result)
        {
            minigameEndedEventChannel?.RaiseEvent(minigame.Id, result);

            Debug.Log("Minigame ended " + result);
            minigame.gameObject.SetActive(false);

            hasEnded = true;
        }


        public override void FinishingAct()
        {
            base.FinishingAct();

            //turnEndedEventChannel?.RaiseEvent();

            hasStarted = false;
        }
    }
}
