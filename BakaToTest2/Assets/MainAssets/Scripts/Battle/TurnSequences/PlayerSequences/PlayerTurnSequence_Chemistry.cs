﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using Cinemachine;
using System;

namespace Intelligensia.Battle
{
    public class PlayerTurnSequence_Chemistry : PlayerTurnSequence
    {
        protected override void InitializeCameras()
        {
            base.InitializeCameras();

            MinigameDifficultyPresets minigamePreset = minigame.GetPreset(minigameDifficulty);
            //cameraParent.m_Instructions[vCamMinigameStarterIndex].m_Hold = minigamePreset.duration + minigameEndedDelayDuration;
            cameraParent.m_Instructions[vCamMinigameStarterIndex].m_Blend.m_Time = minigamePreset.duration + minigameEndedDelayDuration;
            // Initialize target for vcameras.
            vCameras = new List<CinemachineVirtualCamera>();
            CinemachineVirtualCameraBase[] vCameraBases = cameraParent.ChildCameras;
            for (int i = 0; i < vCameraBases.Length; i++)
            {
                CinemachineVirtualCamera vCam = vCameraBases[i].GetComponent<CinemachineVirtualCamera>();

                vCameras.Add(vCam);

                if (vCam.gameObject.tag == VCam_SingleTargetTag)
                {
                    vCam.m_Follow = mainActor.transform;
                    vCam.m_LookAt = mainActor.transform;
                }
                else if (vCam.gameObject.tag == VCam_TargetGroupTag)
                {
                    CinemachineTargetGroup targetGroup = Instantiate(targetGroupPrefab, targetGroupContainer);
                    targetGroup.AddMember(mainActor.transform, 1f, 0.5f);

                    for (int j = 0; j < sideActors.Length; j++)
                    {
                        targetGroup.AddMember(sideActors[j].transform, 1f, 0f);
                    }

                    vCam.m_Follow = targetGroup.transform;
                    vCam.m_LookAt = targetGroup.transform;
                }

            }
        }
    }
}
