﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Intelligensia.Battle
{
    public class PlayerTurnSequence_PartyAttack : PlayerTurnSequence
    {
        // Just add PartyAttackUsed event here
        public PartyAttackSkillSO partyAttackSkillSO;

        public void RaiseAttackUsedEvent()
        {
            Debug.Log("Party Attack Executed!");
            if (partyAttackSkillSO != null)
            {
                partyAttackSkillSO.ApplySkill(BattleInfoManager.activePlayers.ToArray(),
                    BattleInfoManager.activeEnemies.ToArray());

                Debug.Log("Party Attack Executed!");
            }
        }
    }

}

