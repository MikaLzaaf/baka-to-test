﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Intelligensia.Battle
{
    public class PlayerTurnSequence_SupportSkill : PlayerTurnSequence
    {
        public UseSkillInBattleEventChannelSO useSkillInBattleEventChannel;


        protected override void InitializeMinigame()
        {

        }


        public void RaiseUseSkillEvent()
        {
            if (!BattleController.IsInBattle)
                return;

            BattleEntity skillUser = mainActor.GetComponent<BattleEntity>();

            useSkillInBattleEventChannel.RaiseEvent(BattleInfoManager.selectedSkill.GetSkillId(),
                BattleInfoManager.GetTargetsSelected().ToArray(),
                skillUser);
        }
    }
}

