﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Cinemachine;

namespace Intelligensia.Battle
{
    public abstract class TurnSequence : MonoBehaviour
    {
        // What to do here?
        // - Set actors starting position and rotation if needed
        // - Grab minigame for the skill
        // - Set VCamHolder position to actors position if needed
        // - Set actors' animations when signal comes

        protected const string VCam_SingleTargetTag = "VCam_SingleTarget";
        protected const string VCam_TargetGroupTag = "VCam_TargetGroup";
        protected const string VCam_DualTargetTag = "VCam_DualTarget";

        [SerializeField] protected bool initOnStart = false;
        [SerializeField] protected bool deactivateOnFinish = false;

        [Header("Actors")]
        [SerializeField] protected Actor mainActor;
        [SerializeField] protected Actor[] sideActors;
        [SerializeField] protected Transform dummyActorsContainer;

        [Header("Cameras")]
        [Tooltip("The parent must be DISABLED before activation.")]
        [SerializeField] protected CinemachineBlendListCamera cameraParent;
        [SerializeField] protected CinemachineTargetGroup targetGroupPrefab;
        [SerializeField] protected Transform targetGroupContainer;

        protected bool hasEnded = false;

        protected List<CinemachineVirtualCamera> vCameras;

        public bool IsShowing => cameraParent.gameObject.activeSelf;
        public event Action SequenceEndedEvent;


        protected virtual void Start()
        {
            if (initOnStart)
            {
                Initialize();
            }
        }

        protected virtual void Initialize()
        {
            ClearTargetGroupContainer();
            InitializeCameras();

            Execute();
        }
        public abstract void Initialize(string skillId, Actor mainActor, Actor[] sideActors);


        protected virtual void InitializeActors(Actor mainActor, Actor[] sideActors)
        {
            this.mainActor = mainActor;
            this.sideActors = sideActors;
        }

        protected abstract void InitializeCameras();


        protected virtual void ClearTargetGroupContainer()
        {
            if(dummyActorsContainer.childCount > 0)
            {
                for(int i = dummyActorsContainer.childCount -1; i >= 0; i--)
                {
                    Destroy(dummyActorsContainer.GetChild(i).gameObject);
                }
            }

            int childCount = targetGroupContainer.childCount;

            if (childCount == 0)
                return;

            for (int i = childCount - 1; i >= 0; i--)
            {
                Destroy(targetGroupContainer.GetChild(i).gameObject);
            }
        }


        public void Execute()
        {
            ToggleSequence(true);
        }

        public abstract void StartActing();


        public virtual void NextAct()
        {
            mainActor.NextAnimation();
        }

        public virtual void FinishingAct()
        {
            mainActor.NextAnimation();

            if(deactivateOnFinish)
            {
                ToggleSequence(false);
            }

            SequenceEndedEvent?.Invoke();
        }


        public void ToggleSequence(bool active)
        {
            if(cameraParent != null)
                cameraParent.gameObject.SetActive(active);
        }


        public void RemoveActorFromCameraTargetGroups(Transform actor)
        {
            GameObject dummyActor = new GameObject();
            dummyActor.transform.SetParent(dummyActorsContainer);
            dummyActor.transform.SetPositionAndRotation(actor.position, actor.rotation);

            for(int i = 0; i < targetGroupContainer.childCount; i++)
            {
                CinemachineTargetGroup targetGroup = targetGroupContainer.GetChild(i).GetComponent<CinemachineTargetGroup>();

                if (targetGroup == null)
                    continue;

                int actorIndex = targetGroup.FindMember(actor);

                targetGroup.m_Targets[actorIndex].target = dummyActor.transform;
            }
        }
    }
}
