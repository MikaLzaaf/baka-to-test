﻿
namespace Intelligensia.Battle
{
    public enum BattlePhaseEnums
    {
        Initializing,
        BeginTurn,
        LongIdle,
        PlayerTurnDecision,
        EnemyTurnDecision,
        TargetSelection,
        TurnExecution,
        EndTurn,
        Win,
        Lose,
        Unloading,
        Forfeited,
        Brainstorming,
        Debate,
        FreeExecution,
    }
}

