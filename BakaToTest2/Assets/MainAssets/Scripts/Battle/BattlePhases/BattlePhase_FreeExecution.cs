﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Intelligensia.Battle
{
    public class BattlePhase_FreeExecution : BattlePhase
    {
        private BattleController battleController;
        private BattlePhaseAnimatorController animatorController;
        private BattleCameraController cameraController;
        private BattleUIManager menuController;

        private float orderToItemExecutionDelay = 1f;
        private float itemUserToTargetDelay = 2f;
        private float itemTargetFocusDuration = 2f;

        public BattlePhase_FreeExecution(BattlePhasesController phaseController) : base(phaseController)
        {
            this.phaseController = phaseController;
            battleController = phaseController.battleController;
            animatorController = phaseController.animatorController;
            cameraController = phaseController.cameraController;
            menuController = phaseController.menuController;
        }

        public override void Enter()
        {
            phaseController.StartCoroutine(FreeExecutioning(BattleInfoManager.entityOnFocus is PlayerEntity));
        }

        public override void Exit()
        {
            //battleController.FreeExecutionEndedEvent -= ChangeToTurnDecision;
            animatorController.NextPhase(true);
        }

        public override void HandleUpdate()
        {
            
        }

        private IEnumerator FreeExecutioning(bool isPlayerExecution)
        {
            if(isPlayerExecution)
            {
                if (BattleInfoManager.selectedItem != null)
                {
                    yield return phaseController.StartCoroutine(ItemExecution(isPlayerExecution));

                    BattleInfoManager.selectedItem = null;
                }
                else if (BattleInfoManager.selectedOrder != null)
                {
                    yield return phaseController.StartCoroutine(OrderExecution(isPlayerExecution));

                    BattleInfoManager.selectedOrder = null;
                }
            }
            else
            {
                var enemyActions = BattleInfoManager.enemyActionsRegistered;

                int actionIndex = 0; // 1 = Only has order, 2 = Only has item, 3 = has both

                for(int i = 0; i < enemyActions.Count; i++)
                {
                    if (enemyActions[i].selectionType == BattleSelectionType.Order)
                    {
                        if (actionIndex == 2)
                        {
                            actionIndex = 3;
                            break;
                        }
                        else
                            actionIndex = 1;
                    }
                    else if (enemyActions[i].selectionType == BattleSelectionType.Item)
                    {
                        if (actionIndex == 1)
                        {
                            actionIndex = 3;
                            break;
                        }
                        else
                            actionIndex = 2;
                    }
                }

                if (actionIndex == 1)
                    yield return phaseController.StartCoroutine(OrderExecution(isPlayerExecution));
                else if (actionIndex == 2)
                    yield return phaseController.StartCoroutine(ItemExecution(isPlayerExecution));
                else
                {
                    yield return phaseController.StartCoroutine(OrderExecution(isPlayerExecution));

                    animatorController.SetFreeExecutionIndex(2);
                    yield return new WaitForSeconds(orderToItemExecutionDelay);

                    yield return phaseController.StartCoroutine(ItemExecution(isPlayerExecution));
                }
            }

            animatorController.SetFreeExecutionIndex(0);

            if (isPlayerExecution)
                phaseController.ChangePhase(BattlePhaseEnums.PlayerTurnDecision);
            else
                phaseController.ChangePhase(BattlePhaseEnums.BeginTurn);
        }

        private IEnumerator ItemExecution(bool isPlayerExecution)
        {
            if(isPlayerExecution)
            {
                BattleAction itemAction = BattleInfoManager.GetFirstCharacterAction(true);

                if (itemAction.user.TryGetComponent(out Actor actor))
                    actor.StartSkillAnimation(1);

                cameraController.SetItemExecutionCameraOne(itemAction.user.transform);
                cameraController.SetItemExecutionCameraTwo(itemAction.targets[0].transform);

                yield return new WaitForSeconds(itemUserToTargetDelay);

                if (actor != null)
                    actor.NextAnimation();

                animatorController.SetFreeExecutionIndex(4);

                for(int i = 0; i < itemAction.targets.Count; i++)
                {
                    cameraController.SetItemExecutionCameraTwo(itemAction.targets[i].transform);

                    yield return new WaitForEndOfFrame();

                    itemAction.consumable.Use(itemAction.user, itemAction.targets[i], i == itemAction.targets.Count - 1);

                    yield return new WaitForSeconds(itemTargetFocusDuration);
                }
            }
            else
            {
                var itemActions = BattleInfoManager.GetEnemyItemActions();
 
                for(int i = 0; i < itemActions.Length; i++)
                {
                    animatorController.SetFreeExecutionIndex(2);

                    BattleAction itemAction = itemActions[i];

                    if (itemAction.user.TryGetComponent(out Actor actor))
                        actor.StartSkillAnimation(1);

                    cameraController.SetItemExecutionCameraOne(itemAction.user.transform);
                    cameraController.SetItemExecutionCameraTwo(itemAction.targets[0].transform);
        
                    yield return new WaitForSeconds(itemUserToTargetDelay);

                    if (actor != null)
                        actor.NextAnimation();

                    animatorController.SetFreeExecutionIndex(4);

                    for (int j = 0; j < itemAction.targets.Count; j++)
                    {
                        cameraController.SetItemExecutionCameraTwo(itemAction.targets[j].transform);

                        yield return new WaitForEndOfFrame();

                        itemAction.consumable.Use(itemAction.user, itemAction.targets[j], j == itemAction.targets.Count - 1);

                        yield return new WaitForSeconds(itemTargetFocusDuration);
                    }
                }
            }
        }

        private IEnumerator OrderExecution(bool isPlayerExecution)
        {
            var selectedOrder = BattleInfoManager.selectedOrder;

            menuController.helperCharacterView.Show(selectedOrder.orderHolder, ActorExpression.Cocky);
            yield return new WaitUntil(()=> menuController.helperCharacterView.viewState == ViewState.Ready);

            battleController.ApplyOrder(selectedOrder);

            yield return new WaitUntil(() => menuController.helperCharacterView.viewState == ViewState.Closed);

            if (isPlayerExecution)
                menuController.actionSelectionPanel.OnOrderExecutionEnded();
        }
    }
}

