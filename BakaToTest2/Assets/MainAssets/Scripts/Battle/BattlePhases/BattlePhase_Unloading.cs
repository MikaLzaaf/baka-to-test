﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Intelligensia.Battle
{
    public class BattlePhase_Unloading : BattlePhase
    {
        private BattleController battleController;
        private BattleUIManager menuController;


        public BattlePhase_Unloading(BattlePhasesController phaseController) : base(phaseController)
        {
            this.phaseController = phaseController;
            menuController = phaseController.menuController;
            battleController = phaseController.battleController;

        }


        public override void Enter()
        {
            //BattleInfoManager.allowBattleExecution = false;

            battleController.UnloadingBattleScene(phaseController.retry, DailyInfoManager.HasNextChallenge());

            //battleController.TurnNumberChangedEvent -= menuController.OnTurnNumberChanged;
            menuController.Unload();

            phaseController.animatorController.Unloading();

            if (DailyInfoManager.HasNextChallenge())
            {
                if (GameUIManager.instance != null)
                {
                    var challengeMenu = GameUIManager.instance.challengesMenuController;

                    challengeMenu.ShowNextChallenge();
                }
            }
        }

        public override void HandleUpdate()
        {

        }

        public override void Exit()
        {

        }
    }
}
