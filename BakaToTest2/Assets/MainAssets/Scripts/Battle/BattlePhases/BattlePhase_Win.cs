﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Intelligensia.Battle
{
    public class BattlePhase_Win : BattlePhase
    {
        private BattleController battleController;
        private BattleUIManager menuController;


        public BattlePhase_Win(BattlePhasesController phaseController) : base(phaseController)
        {
            this.phaseController = phaseController;
            battleController = phaseController.battleController;
            menuController = phaseController.menuController;
        }


        public override void Enter()
        {
            menuController.gameWinPanelController.ReturnToMapEvent += OnReturnToMap;

            phaseController.cameraController.SetWinCamera(BattleInfoManager.activePlayers.ToArray());
            Debug.Log(battleController + " battlecontroller");
            battleController.SetChallengeAsComplete();
            battleController.WinBattle();

            menuController.ShowGameWin();
        }

        public override void HandleUpdate()
        {

        }

        public override void Exit()
        {
            menuController.gameWinPanelController.ReturnToMapEvent -= OnReturnToMap;

            UINavigator.instance.ClearWindowsList();
        }


        private void OnReturnToMap()
        {
            //if(DailyInfoManager.HasNextChallenge())
            //{
            //    if (GameUIManager.instance != null)
            //    {
            //        var challengeMenu = GameUIManager.instance.challengesMenuController;

            //        challengeMenu.ShowNextChallenge();
            //    }
            //}
            //else
            //{
                Debug.Log("On return to map");
                phaseController.ChangePhase(BattlePhaseEnums.Unloading);
            //}
        }
    }
}
