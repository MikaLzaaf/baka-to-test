﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

namespace Intelligensia.Battle
{
    public class BattlePhase_Debate : BattlePhase
    {
        private BattleUIManager menuController;
        private BattleCameraController cameraController;

        public BattlePhase_Debate(BattlePhasesController phaseController) : base(phaseController)
        {
            this.phaseController = phaseController;
            menuController = phaseController.menuController;
            cameraController = phaseController.cameraController;
        }

        public override void Enter()
        {
            // Face-Off depends on several situations
            // - Attacker attacks Target that is not Guard/Attack -> Face-Off finishes instantly
            // - Attacker attacks Target that is Guard/Attack -> Face-Off accordingly
            //phaseController.hasFaceOff = true;

            //menuController.ToggleForFaceOff(true, true);

            //currentPlayer = BattleInfoManager.isPlayerTurn ? BattleInfoManager.entityOnTurn :
            //    BattleInfoManager.currentTargetHighlighted;

            //currentEnemy = BattleInfoManager.isPlayerTurn ? BattleInfoManager.currentTargetHighlighted : 
            //    BattleInfoManager.entityOnTurn;

            // Need to check if any side does not have any argument before proceed to debate.
            // If no argument at all from one side, then the side with at least one argument will automatically wins the round and execute their argument.

            menuController.debatePanel.OnDebateEndedEvent += OnDebateEnded;
            menuController.debatePanel.OnDebateArgumentSelectedEvent += OnDebateArgumentSelected;

            menuController.BeginDebate();

            phaseController.animatorController.StartDebate(true);
            phaseController.animatorController.DebateFocus(0);

            //cameraController.SetFaceOffCamera(currentPlayer, currentEnemy);

            //cameraController.SetSplitCameraPlayer(currentPlayer.transform);

            //menuController.BeginFaceOff(true, false);



            //menuController.faceOffPanel.FaceOffEndedEvent += OnFaceOffEnded;
        }

        public override void Exit()
        {
            phaseController.animatorController.SetFreeHit(false);
        }

        public override void HandleUpdate()
        {
            
        }

        private void OnDebateEnded(int resultIndex)
        {
            menuController.debatePanel.OnDebateEndedEvent -= OnDebateEnded;
            menuController.debatePanel.OnDebateArgumentSelectedEvent -= OnDebateArgumentSelected;

            phaseController.animatorController.SetDebateResult(resultIndex);
            phaseController.animatorController.StartDebate(false);

            //menuController.BeginFaceOff(false, false);
            //menuController.ToggleForFaceOff(false, true);

            phaseController.ChangePhase(BattlePhaseEnums.TurnExecution);
        }

        private void OnDebateArgumentSelected(BattleEntity user)
        {
            int debateIndex = user.entityTag == EntityTag.Player ? 1 : 2;

            phaseController.animatorController.DebateFocus(debateIndex);
            cameraController.SetDebateFocusCamera(user.transform, debateIndex);

            phaseController.StartCoroutine(RevertDebateFocus());
        }

        private IEnumerator RevertDebateFocus()
        {
            yield return new WaitForSeconds(1f);

            phaseController.animatorController.DebateFocus(0);
        }
    }
}
