﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Intelligensia.Battle
{
    public class BattlePhase_Initializing : BattlePhase
    {
        private BattleController battleController;
        private BattleUIManager menuController;
        private BattleCameraController cameraController;


        public BattlePhase_Initializing(BattlePhasesController phaseController) : base(phaseController)
        {
            this.phaseController = phaseController;
            battleController = phaseController.battleController;
            menuController = phaseController.menuController;
            cameraController = phaseController.cameraController;

        }


        public override void Enter()
        {
            battleController.InitializeBattle();

            //battleController.TurnNumberChangedEvent += menuController.OnTurnNumberChanged;

            phaseController.ToggleCameraBrain(true);

            phaseController.StartCoroutine(WaitUntilInitialized());
        }


        private IEnumerator WaitUntilInitialized()
        {
            yield return new WaitWhile(() => battleController.isInitializing);

            menuController.Initialize();

            // Initialize the cameras position and aiming first, if not they will look buggy and slow
            cameraController.SetTargetSelectionCamera(BattleInfoManager.activeEnemies[0].transform, true);

            cameraController.SetMultipleTargetsCamera(BattleCameraController.MultipleTargetCameraEnum.AllPlayers,
                BattleInfoManager.activePlayers.ToArray());
            cameraController.SetMultipleTargetsCamera(BattleCameraController.MultipleTargetCameraEnum.AllEnemies,
               BattleInfoManager.activeEnemies.ToArray());
            cameraController.SetMultipleTargetsCamera(BattleCameraController.MultipleTargetCameraEnum.All,
               BattleInfoManager.GetAllCharacters().ToArray());
            cameraController.SetTurnExecutionCamera(BattleInfoManager.activePlayers.ToArray());


            phaseController.animatorController.NextPhase(true);
            phaseController.ChangePhase(BattlePhaseEnums.BeginTurn);
        }


        public override void HandleUpdate()
        {

        }


        public override void Exit()
        {
            //phaseController.animatorController.NextPhase(false);
        }
    }
}
