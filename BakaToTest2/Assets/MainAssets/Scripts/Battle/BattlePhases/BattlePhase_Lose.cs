﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Intelligensia.Battle
{
    public class BattlePhase_Lose : BattlePhase
    {
        private BattleController battleController;
        private BattleUIManager menuController;


        public BattlePhase_Lose(BattlePhasesController phaseController) : base(phaseController)
        {
            this.phaseController = phaseController;
            battleController = phaseController.battleController;
            menuController = phaseController.menuController;
        }


        public override void Enter()
        {
            //battleController.TurnNumberChangedEvent -= menuController.OnTurnNumberChanged;

            menuController.gameOverPanelController.RetryEvent += OnRetry;

            menuController.ShowGameOver();
        }

        public override void HandleUpdate()
        {

        }

        public override void Exit()
        {
            menuController.gameOverPanelController.RetryEvent -= OnRetry;
        }


        private void OnRetry()
        {
            phaseController.retry = true;
            phaseController.ChangePhase(BattlePhaseEnums.Unloading);
        }
    }
}
