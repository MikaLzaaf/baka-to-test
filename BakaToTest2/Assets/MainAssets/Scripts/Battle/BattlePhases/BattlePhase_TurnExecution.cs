﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Intelligensia.Battle
{
    public class BattlePhase_TurnExecution : BattlePhase
    {
        private BattleController battleController;
        private BattlePhaseAnimatorController animatorController;
        private BattleCameraController cameraController;
        private BattleUIManager menuController;

        //private bool isFacingOff = false;

        public BattlePhase_TurnExecution(BattlePhasesController phaseController) : base(phaseController)
        {
            this.phaseController = phaseController;
            battleController = phaseController.battleController;
            animatorController = phaseController.animatorController;
            cameraController = phaseController.cameraController;
            menuController = phaseController.menuController;
        }


        public override void Enter()
        {
            // Turn Execution depends on several situations
            // - Player choose to Attack Enemy/Enemies -> Face-Off -> Turn Execution
            // - Enemy choose to Attack Player(s) -> Face-Off -> Turn Execution
            // - Player/Enemy choose to Guard -> Turn Execution
            // - Player/Enemy choose to use Item -> Turn Execution
            // - Player/Enemy choose to Support -> Turn Execution

            //if (!isFacingOff && battleController.actionSelected == BattleSelectionType.Attack)
            //{
            //    //if(BattleInfoManager.isPlayerTurn)
            //    //{
            //    //    phaseController.ChangePhase(BattlePhaseEnums.Brainstorming);
            //    //}
            //    //else
            //    //{
            //        phaseController.ChangePhase(BattlePhaseEnums.FaceOff);
            //    //}

            //    isFacingOff = true;

            //    return;
            //}

            battleController.TurnExecutionEvent += OnTurnExecution;
            battleController.ExecuteTurn(ChangeToEndTurn);

            //cameraController.SetTurnExecutionCamera(BattleInfoManager.entityOnFocus.transform);

            //InitializeCameraPostTurn();
        }

        public override void HandleUpdate()
        {

        }

        public override void Exit()
        {
            battleController.TurnExecutionEvent -= OnTurnExecution;
            //animatorController.IsTargetSelected(false);
            animatorController.SelectTarget(false, BattleInfoManager.currentTargetIsEnemy);
        }


        private void ChangeToEndTurn()
        {
            animatorController.NextPhase(true);
            phaseController.ChangePhase(BattlePhaseEnums.EndTurn);
        }


        private void OnTurnExecution(BattleAction[] argumentsToExecute)
        {
            animatorController.SetExecution(argumentsToExecute[0].targets.Count > 1);
            cameraController.SetTurnExecutionCamera(argumentsToExecute[0].targets.ToArray());
            menuController.turnExecutionPanel.Show(argumentsToExecute);
        }
    }
}
