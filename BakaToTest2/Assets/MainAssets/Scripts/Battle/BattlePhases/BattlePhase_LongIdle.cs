﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Intelligensia.Battle
{
    public class BattlePhase_LongIdle : BattlePhase
    {
        //private BattleController battleController;
        private BattleCameraController cameraController;
        private BattlePhaseAnimatorController animatorController;

        public BattlePhase_LongIdle(BattlePhasesController phaseController) : base(phaseController)
        {
            this.phaseController = phaseController;
            //battleController = phaseController.battleController;
            cameraController = phaseController.cameraController;
            animatorController = phaseController.animatorController;
        }

        public override void Enter()
        {
            phaseController.menuController.Close();

            //cameraController.shouldOrbit = true;

            cameraController.SetLongIdleCamera(BattleInfoManager.GetAllCharacters().ToArray());

            animatorController.LongIdle(true);
        }

        public override void HandleUpdate()
        {
            //if(GameInput.instance.GetAnyKey())
            //{
            //    phaseController.ChangePhase(BattlePhaseEnums.PlayerTurnDecision);
            //}
        }

        public override void Exit()
        {
            animatorController.LongIdle(false);

            //cameraController.shouldOrbit = false;

            //phaseController.menuController.Show();
        }
    }
}
