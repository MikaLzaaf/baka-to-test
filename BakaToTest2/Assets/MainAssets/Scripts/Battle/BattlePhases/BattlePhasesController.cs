﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using Intelligensia.Battle;

public class BattlePhasesController : Singleton<BattlePhasesController>
{
    // What to do here?
    // - Control Battle flow
    // - Control camera flow too
    
    public BattleCameraController cameraController;
    public BattlePhaseAnimatorController animatorController;
    public BattleUIManager menuController;
    public GameObject battleCameraBrain;

    public float delayNewTurnDuration = 1f;
    public float playerNotActiveLimitDuration = 5f;
    public Transform[] playerSpawnPositions;
    public Transform[] enemiesSpawnPositions;

    [Header("Test Battle")]
    public bool testBattleOnStart = false;
    public Subjects activeSubject;
    public DayEnum testDay;
    public string testWeather;
    public IlmuCost[] battleCost;
    public bool useTestSkills = false;
    public SkillItemBaseValue[] testingSkills;
    public bool useInfiniteHp = false;

    public ChallengeInfo challengeInfo;

    public List<BattlePartyMember> playerPartyMembers;
    //public List<BattlePartyMember> enemyPartyMembers;
    public int activeMembersCount;


    private Dictionary<BattlePhaseEnums, BattlePhase> phasesLookup;

    public BattlePhaseEnums CurrentPhaseEnum { get; private set; }
    public BattlePhase CurrentPhase { get; private set; }
    public BattleController battleController { get; private set; }

    public bool hasFaceOff { get; set; } = false;
    public bool retry { get; set; }
    public float elapsedAFKTime = 0f;

    #region BattlePhases Variables

    private BattlePhase_Initializing battlePhase_Initializing;
    private BattlePhase_BeginTurn battlePhase_BeginTurn;
    private BattlePhase_LongIdle battlePhase_LongIdle;
    private BattlePhase_PlayerTurnDecision battlePhase_PlayerTurnDecision;
    private BattlePhase_EnemyTurnDecision battlePhase_EnemyTurnDecision;
    private BattlePhase_TargetSelection battlePhase_TargetSelection;
    private BattlePhase_Debate battlePhase_Debate;
    private BattlePhase_TurnExecution battlePhase_TurnExecution;
    private BattlePhase_FreeExecution battlePhase_OrderExecution;
    //private BattlePhase_Staggered battlePhase_Staggered;
    private BattlePhase_EndTurn battlePhase_EndTurn;
    private BattlePhase_Forfeited battlePhase_Forfeited;
    private BattlePhase_Win battlePhase_Win;
    private BattlePhase_Lose battlePhase_Lose;
    private BattlePhase_Unloading battlePhase_Unloading;

    #endregion


    private void Start()
    {
        battleController = BattleController.instance;
        battleController.SetSpawnPositions(playerSpawnPositions, enemiesSpawnPositions);

        if(testBattleOnStart)
        {
            battleController.DebugBattle(this, challengeInfo);
            //BattleInfoManager.allowBattleExecution = true;
        }

        Initialize();


        //MainCameraController.instance.ChangeToNextCamera(null);
    }


    private void Update()
    {
        if(CurrentPhase != null)
        {
            CurrentPhase.HandleUpdate();
        }
    }


    public void Initialize()
    {
        battlePhase_Initializing = new BattlePhase_Initializing(this);
        battlePhase_BeginTurn = new BattlePhase_BeginTurn(this);
        battlePhase_LongIdle = new BattlePhase_LongIdle(this);
        battlePhase_PlayerTurnDecision = new BattlePhase_PlayerTurnDecision(this);
        battlePhase_EnemyTurnDecision = new BattlePhase_EnemyTurnDecision(this);
        battlePhase_TargetSelection = new BattlePhase_TargetSelection(this);
        battlePhase_Debate = new BattlePhase_Debate(this);
        battlePhase_TurnExecution = new BattlePhase_TurnExecution(this);
        battlePhase_OrderExecution = new BattlePhase_FreeExecution(this);
        //battlePhase_Staggered = new BattlePhase_Staggered(this);
        battlePhase_EndTurn = new BattlePhase_EndTurn(this);
        battlePhase_Forfeited = new BattlePhase_Forfeited(this);
        battlePhase_Win = new BattlePhase_Win(this);
        battlePhase_Lose = new BattlePhase_Lose(this);
        battlePhase_Unloading = new BattlePhase_Unloading(this);

        phasesLookup = new Dictionary<BattlePhaseEnums, BattlePhase>();
        phasesLookup.Add(BattlePhaseEnums.Initializing, battlePhase_Initializing);
        phasesLookup.Add(BattlePhaseEnums.BeginTurn, battlePhase_BeginTurn);
        phasesLookup.Add(BattlePhaseEnums.LongIdle, battlePhase_LongIdle);
        phasesLookup.Add(BattlePhaseEnums.PlayerTurnDecision, battlePhase_PlayerTurnDecision);
        phasesLookup.Add(BattlePhaseEnums.EnemyTurnDecision, battlePhase_EnemyTurnDecision);
        phasesLookup.Add(BattlePhaseEnums.TargetSelection, battlePhase_TargetSelection);
        phasesLookup.Add(BattlePhaseEnums.TurnExecution, battlePhase_TurnExecution);
        phasesLookup.Add(BattlePhaseEnums.FreeExecution, battlePhase_OrderExecution);
        phasesLookup.Add(BattlePhaseEnums.Debate, battlePhase_Debate);
        phasesLookup.Add(BattlePhaseEnums.EndTurn, battlePhase_EndTurn);
        phasesLookup.Add(BattlePhaseEnums.Forfeited, battlePhase_Forfeited);
        phasesLookup.Add(BattlePhaseEnums.Win, battlePhase_Win);
        phasesLookup.Add(BattlePhaseEnums.Lose, battlePhase_Lose);
        phasesLookup.Add(BattlePhaseEnums.Unloading, battlePhase_Unloading);

        ChangePhase(BattlePhaseEnums.Initializing);
    }

    public void ChangePhase(BattlePhase newPhase)
    {
        if(CurrentPhase != null)
        {
            CurrentPhase.Exit();
        }
        
        CurrentPhase = newPhase;
        newPhase.Enter();
    }


    public void ChangePhase(BattlePhaseEnums newPhase)
    {
        phasesLookup.TryGetValue(newPhase, out BattlePhase nextPhase);

        if (nextPhase != null)
        {
            CurrentPhaseEnum = newPhase;
            ChangePhase(nextPhase);
        }
    }


    public void ReturnFromAFK(InputAction.CallbackContext context)
    {
        if (!IsAFK())
            return;


        if (context.performed)
        {
            ChangePhase(BattlePhaseEnums.PlayerTurnDecision);
        }
    }

    public bool IsAFK()
    {
        return CurrentPhaseEnum == BattlePhaseEnums.LongIdle;
    }


    public void ToggleCameraBrain(bool active)
    {
        if (battleCameraBrain != null)
            battleCameraBrain.SetActive(active);
    }
}
