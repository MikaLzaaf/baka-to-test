﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Intelligensia.Battle
{
    public class BattlePhase_Forfeited : BattlePhase
    {
        private BattleController battleController;
        private BattleUIManager menuController;

        public BattlePhase_Forfeited(BattlePhasesController phaseController) : base(phaseController)
        {
            this.phaseController = phaseController;
            battleController = phaseController.battleController;
            menuController = phaseController.menuController;
        }


        public override void Enter()
        {
            battleController.SetChallengeAsComplete();

            phaseController.animatorController.SetBattleResult(0);

            List<PlayerEntity> activePlayers = BattleInfoManager.activePlayers;
            activePlayers.AddRange(BattleInfoManager.defeatedPlayerList);

            menuController.forfeitPanel.OnSkipResultEvent += Done;

            menuController.forfeitPanel.InitializePanel(activePlayers.ToArray(), BattleInfoManager.reservePlayers.ToArray(), 
                battleController.currentChallengeInfo.ilmuRewards);
        }

        public override void HandleUpdate()
        {

        }

        public override void Exit()
        {

        }


        public void Done()
        {
            menuController.forfeitPanel.OnSkipResultEvent -= Done;

            //if (DailyInfoManager.HasNextChallenge())
            //{
            //    if(GameUIManager.instance != null)
            //    {
            //        var challengeMenu = GameUIManager.instance.challengesMenuController;

            //        challengeMenu.ShowNextChallenge();
            //    }
            //}
            //else
            //{
                phaseController.ChangePhase(BattlePhaseEnums.Unloading);
            //}
        }
    }
}
