﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Intelligensia.Battle
{
    public class BattlePhase_BeginTurn : BattlePhase
    {
        private BattleController battleController;
        private BattleCameraController cameraController;
        private BattlePhaseAnimatorController animatorController;

        private BattlePhaseEnums nextPhase;



        public BattlePhase_BeginTurn(BattlePhasesController phaseController)
            : base(phaseController)
        {
            this.phaseController = phaseController;
            battleController = phaseController.battleController;
            cameraController = phaseController.cameraController;
            animatorController = phaseController.animatorController;
        }


        public override void Enter()
        {
            battleController.BeginTurn(battleController.enemyHasDecidedAction);

            if (!battleController.enemyHasDecidedAction)
            {
                cameraController.SetPlayerIdleCamera(BattleInfoManager.entityOnFocus.transform);

                animatorController.NewTurn(false);

                nextPhase = BattlePhaseEnums.EnemyTurnDecision;

                phaseController.StartCoroutine(WaitToChangePhase());
            }
            else
            {
                animatorController.NewTurn(true);

                UINavigator.instance.ClearWindowsList();

                nextPhase = BattlePhaseEnums.PlayerTurnDecision;

                phaseController.StartCoroutine(WaitUntilActionIsAllowed());
            }
        }

        public override void HandleUpdate()
        {

        }

        public override void Exit()
        {
            animatorController.NextPhase(false);
            animatorController.CloseNewTurn();
        }

        private IEnumerator WaitToChangePhase()
        {
            yield return new WaitForSeconds(0.1f);

            phaseController.ChangePhase(nextPhase);
        }

        private IEnumerator WaitUntilActionIsAllowed()
        {
            //yield return new WaitUntil(() => BattleInfoManager.allowBattleExecution);

            animatorController.NextPhase(true);

            yield return new WaitForSeconds(phaseController.delayNewTurnDuration);

            phaseController.ChangePhase(nextPhase);
        }
    }
}

