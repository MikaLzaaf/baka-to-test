﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Intelligensia.Battle;

namespace Intelligensia.Battle
{
    public class BattlePhase_PlayerTurnDecision : BattlePhase
    {
        private BattleController battleController;
        private BattleUIManager menuController;
        private BattleCameraController cameraController;


        public BattlePhase_PlayerTurnDecision(BattlePhasesController phaseController) : base(phaseController)
        {
            this.phaseController = phaseController;
            battleController = phaseController.battleController;
            menuController = phaseController.menuController;
            cameraController = phaseController.cameraController;
        }

        public override void Enter()
        {
            menuController.Show();
            menuController.ToggleActionSelectionPanel(true);

            menuController.OnBeginDebateEvent += OnBeginDebate;

            menuController.actionSelectionPanel.OnPreviousMemberEvent += OnPreviousMemberSelected;
            menuController.actionSelectionPanel.OnNextMemberEvent += OnNextMemberSelected;

            menuController.itemSelectionPanel.ItemSelectedEvent += ItemTargetSelection;
            menuController.attackSkillSelectionPanel.AttackSkillsSelectedEvent += SkillTargetSelection;
            menuController.supportSkillSelectionPanel.SupportSkillSelectedEvent += SkillTargetSelection;
            //menuController.counterSkillSelectionPanel.CounterSkillSelectedEvent += OnGuardActionSelected;
            menuController.orderSelectionPanel.OrderSelectedEvent += OnOrderSelected;
            menuController.forfeitPanel.OnForfeitedEvent += OnForfeited;
            menuController.partyAttackPanel.PartyAttackSelectedEvent += OnPartyAttackSelected;

            phaseController.elapsedAFKTime = 0f;

            //battleController.actionSelected = BattleSelectionType.Null;

            //MainCameraController.instance.SetCameraBlend(0f, true);
        }

        public override void HandleUpdate()
        {
            if (!menuController.readyToReceiveActionInput)
                return;

            if (!UINavigator.instance.inBattleIdle || MessageBox.instance.viewState == ViewState.Ready)
            {
                if (phaseController.elapsedAFKTime > 0f)
                    phaseController.elapsedAFKTime = 0f;

                return;
            }

            phaseController.elapsedAFKTime += UnityEngine.Time.deltaTime;

            if (phaseController.elapsedAFKTime >= phaseController.playerNotActiveLimitDuration)
            {
                phaseController.ChangePhase(BattlePhaseEnums.LongIdle);
            }
        }

        public override void Exit()
        {
            menuController.OnBeginDebateEvent -= OnBeginDebate;

            menuController.actionSelectionPanel.OnPreviousMemberEvent -= OnPreviousMemberSelected;
            menuController.actionSelectionPanel.OnNextMemberEvent -= OnNextMemberSelected;

            menuController.itemSelectionPanel.ItemSelectedEvent -= ItemTargetSelection;
            menuController.attackSkillSelectionPanel.AttackSkillsSelectedEvent -= SkillTargetSelection;
            menuController.supportSkillSelectionPanel.SupportSkillSelectedEvent -= SkillTargetSelection;
            //menuController.counterSkillSelectionPanel.CounterSkillSelectedEvent -= OnGuardActionSelected;
            menuController.orderSelectionPanel.OrderSelectedEvent -= OnOrderSelected;
            menuController.forfeitPanel.OnForfeitedEvent -= OnForfeited;
            menuController.partyAttackPanel.PartyAttackSelectedEvent -= OnPartyAttackSelected;
        }


        private void ItemTargetSelection(Item selectedItem)
        {
            phaseController.animatorController.NextPhase(false);

            phaseController.ChangePhase(BattlePhaseEnums.TargetSelection);
        }


        private void SkillTargetSelection()
        {
            phaseController.ChangePhase(BattlePhaseEnums.TargetSelection);
        }


        private void OnOrderSelected(BattleOrder selectedOrder)
        {
            //menuController.battleInfoPanel.Show();
            phaseController.animatorController.NextPhase(false);

            phaseController.ChangePhase(BattlePhaseEnums.FreeExecution);
        }

        //private void OnGuardActionSelected()
        //{
        //    //BattleInfoManager.entityOnTurn.Guard(true);

        //    phaseController.animatorController.UseCounter();

        //    phaseController.ChangePhase(BattlePhaseEnums.TurnExecution);
        //}

        private void OnForfeited()
        {
            if(!battleController.currentChallengeInfo.isGameOverBattle)
                phaseController.ChangePhase(BattlePhaseEnums.Forfeited);
            else
                phaseController.ChangePhase(BattlePhaseEnums.Lose);
        }

        private void OnPartyAttackSelected()
        {
            phaseController.animatorController.UsePartyAttack();

            phaseController.ChangePhase(BattlePhaseEnums.TurnExecution);
        }

        private void OnPreviousMemberSelected()
        {
            SwitchMember(-1);
        }

        private void OnNextMemberSelected()
        {
            SwitchMember(1);
        }

        private void SwitchMember(int value)
        {
            int currentMemberIndex = BattleInfoManager.activePlayers.IndexOf((PlayerEntity)BattleInfoManager.entityOnFocus);

            int switchIndex = currentMemberIndex + value;

            if (switchIndex >= BattleInfoManager.activePlayers.Count)
                switchIndex = 0;
            else if (switchIndex < 0)
                switchIndex = BattleInfoManager.activePlayers.Count - 1;

            PlayerEntity switchedMember = BattleInfoManager.activePlayers[switchIndex];

            BattleInfoManager.entityOnFocus = switchedMember;

            cameraController.SetPlayerIdleCamera(switchedMember.transform);

            menuController.SwitchingMember();
            //menuController.actionSelectionPanel.PrepareSelectableActionLabels();
        }

        private void OnBeginDebate()
        {
            phaseController.ChangePhase(BattlePhaseEnums.Debate);
        }
    }
}
