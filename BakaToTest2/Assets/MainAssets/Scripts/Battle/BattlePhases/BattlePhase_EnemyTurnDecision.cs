﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Intelligensia.Battle
{
    public class BattlePhase_EnemyTurnDecision : BattlePhase
    {
        BattleController battleController;
        BattlePhaseAnimatorController animatorController;

        private float decisionDelay = 0.2f;
        //private bool isDecidingAction;

        public BattlePhase_EnemyTurnDecision(BattlePhasesController phaseController) : base(phaseController)
        {
            this.phaseController = phaseController;
            battleController = phaseController.battleController;
            animatorController = phaseController.animatorController;
        }

        public override void Enter()
        {
            animatorController.NextPhase(false);
            phaseController.StartCoroutine(DecideAction());

            //MainCameraController.instance.SetCameraBlend(0f, true);
        }

        public override void HandleUpdate()
        {

        }

        public override void Exit()
        {

        }

        //private void StopDecidingAction()
        //{
        //    isDecidingAction = false;
        //}

        private IEnumerator DecideAction()
        {
            // TODO: Enemy decides action based on their personality. 
            //       For example, Timid = Less attack, more support / Brave = More aggressive, will do more attack

            // TODO: Enemy decides target based on GLOBAL DIFFICULTY. 
            //       EASY   = Random selection,
            //       NORMAL = Focused selection with a bit randomness, 
            //       HARD   = Really focused selection (will always target player's weaknesses and select the best action) 
            yield return new WaitForFixedUpdate();

            bool stillHasAvailableArguments = true;
            BattleEntity lastSelectedEntity = null;

            while(BattleInfoManager.enemyArgumentActionsRegistered.Count < BattleController.MaxArgumentAmount && stillHasAvailableArguments)
            {
                int highestScore = 0;
                BattleEntity entityWithHighestScore = null;

                for(int i = 0; i < BattleInfoManager.activeEnemies.Count; i++)
                {
                    BattleInfoManager.entityOnFocus = BattleInfoManager.activeEnemies[i];

                    var enemy = BattleInfoManager.activeEnemies[i];
                    enemy.DecideActionInBattle();

                    int currentScore = enemy.targetingBehavior.currentTargetingScore.score;
                    
                    if(lastSelectedEntity != null)
                    {
                        if (enemy.characterName == lastSelectedEntity.characterName) // To distribute the actions executed fairly
                            currentScore -= 1;
                    }
                    
                    if (currentScore > highestScore)
                    {
                        highestScore = currentScore;
                        entityWithHighestScore = enemy;
                    }

                    yield return new WaitForEndOfFrame();
                }

                if (entityWithHighestScore != null)
                {
                    BattleInfoManager.entityOnFocus = entityWithHighestScore;

                    BattleSelectionType selectionType = entityWithHighestScore.targetingBehavior.currentTargetingScore.selectionType;

                    if(selectionType == BattleSelectionType.Item)
                    {
                        BattleInfoManager.selectedItem = entityWithHighestScore.targetingBehavior.currentTargetingScore.consumable;

                        BattleInfoManager.CreateNewAction(entityWithHighestScore.targetingBehavior.currentTargetingScore.selectionType,
                            entityWithHighestScore.targetingBehavior.currentTargetingScore.consumable,
                            false);
                    }
                    else if (selectionType == BattleSelectionType.Counter)
                    {
                        BattleInfoManager.CreateNewAction(entityWithHighestScore.targetingBehavior.currentTargetingScore.selectionType,
                            entityWithHighestScore.targetingBehavior.currentTargetingScore.selectionType.ToString(),
                            false);
                    }
                    else
                    {
                        BattleInfoManager.selectedSkill = entityWithHighestScore.targetingBehavior.currentTargetingScore.skill;

                        BattleInfoManager.CreateNewAction(entityWithHighestScore.targetingBehavior.currentTargetingScore.selectionType,
                            entityWithHighestScore.targetingBehavior.currentTargetingScore.skill,
                            false);
                    }

                    BattleInfoManager.AddActionSelected(entityWithHighestScore.targetingBehavior.currentTargetingScore.target);

                    lastSelectedEntity = entityWithHighestScore;
                }
                else
                    stillHasAvailableArguments = false;

                yield return new WaitForEndOfFrame();
            }

            //BattleInfoManager.entityOnFocus = null;
            yield return new WaitForSeconds(decisionDelay);

            battleController.StopEnemyDecidingAction();

            yield return new WaitForFixedUpdate();

            if (EnemyHasFreeExecution())
                phaseController.ChangePhase(BattlePhaseEnums.FreeExecution);
            else
                phaseController.ChangePhase(BattlePhaseEnums.BeginTurn);
        }

        private bool EnemyHasFreeExecution()
        {
            var enemyActions = BattleInfoManager.enemyActionsRegistered;

            for (int i = 0; i < enemyActions.Count; i++)
            {
                if (enemyActions[i].selectionType == BattleSelectionType.Order)
                {
                    animatorController.SetFreeExecutionIndex(3);
                    return true;
                }
                else if (enemyActions[i].selectionType == BattleSelectionType.Item)
                {
                    animatorController.SetFreeExecutionIndex(2);
                    return true;
                }
            }

            return false;
        }
    }
}
