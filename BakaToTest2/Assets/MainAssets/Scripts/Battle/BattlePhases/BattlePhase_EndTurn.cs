﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Intelligensia.Battle
{
    public class BattlePhase_EndTurn : BattlePhase
    {
        private BattleController battleController;
        private BattlePhaseAnimatorController animatorController;
        private BattleUIManager menuController;
        private BattleCameraController cameraController;

        private int endCondition = -1;

        private int characterRemovedCondition = -1;

        public BattlePhase_EndTurn(BattlePhasesController phaseController) : base(phaseController)
        {
            this.phaseController = phaseController;
            battleController = phaseController.battleController;
            animatorController = phaseController.animatorController;
            menuController = phaseController.menuController;
            cameraController = phaseController.cameraController;

            if(battleController.currentChallengeInfo.isClassBattle)
            {
                battleController.RemoveCharacterInBattleEvent -= OnCharacterRemovedInClassBattleEvent;
                battleController.RemoveCharacterInBattleEvent += OnCharacterRemovedInClassBattleEvent;
            }
        }

        private void OnCharacterRemovedInClassBattleEvent(BattleEntity character)
        {
            // For characterRemovedCondition
            // 1 = Player character is removed
            // 2 = Enemy character is removed
            // 3 = No removal
            if (character.entityTag == EntityTag.Player)
            {
                characterRemovedCondition = 1;
            }
            else if (character.entityTag == EntityTag.Enemy)
            {
                characterRemovedCondition = 2;
            }
        }

        public override void Enter()
        {
            //battleController.RemoveCharacterInBattleEvent += menuController.turnDisplayPanel.OnCharacterRemoved;
            //if (phaseController.hasFaceOff)
            //{
            //    BattleInfoManager.allowBattleExecution = false;

            //    //battleController.StartCoroutine(WaitUntilReturn());
            //}

            //BattleInfoManager.allowBattleExecution = false;
            animatorController.SetPlayerActionIndex(-1);

            //check if conditions to end battle have been achieved
            // 1 = if recent targets are destroyed
            // 2 = if all party members are defeated / in class battle, if player leader is destroyed
            // 3 = if all enemies are defeated / in class battle, if enemy leader is destroyed
            // 4 = if no one is defeated

            endCondition = battleController.EndTurnCheck();

            if (endCondition == 1)
            {
                battleController.StartCoroutine(DelayNewTurn(true));
            }
            else if (endCondition == 2)
            {
                BattlePhaseEnums nextPhase = battleController.currentChallengeInfo.isGameOverBattle ? BattlePhaseEnums.Lose : BattlePhaseEnums.Forfeited;

                battleController.StartCoroutine(DelayResult(nextPhase));
            }
            else if (endCondition == 3)
            {
                battleController.StartCoroutine(DelayResult(BattlePhaseEnums.Win));
            }
            else if (endCondition == 4)
            {
                battleController.StartCoroutine(DelayNewTurn(false));
                //ChangeToBeginTurn();
                //battleController.StartCoroutine(WaitUntilReturn());
            }
        }

        public override void HandleUpdate()
        {

        }

        public override void Exit()
        {
            //battleController.RemoveCharacterInBattleEvent -= menuController.turnDisplayPanel.OnCharacterRemoved;
            animatorController.SetDebateResult(-1);
            animatorController.SetEnemyActionIndex(BattleSelectionType.Null);
        }


        private IEnumerator DelayNewTurn(bool hasCharacterRemoved)
        {
            if (battleController.CanSpawnReserve() && (characterRemovedCondition == 1 || characterRemovedCondition == 2))
            {
                animatorController.AddNewClassmate(true);

                menuController.newClassmateEntersView.Show();

                yield return new WaitUntil(() => menuController.newClassmateEntersView.viewState == ViewState.TransitioningReady);

                if (battleController.nextReserveSpawnIsPlayer)
                    battleController.SpawnNextReservePlayer();
                else
                    battleController.SpawnNextReserveEnemy();

                cameraController.SetNewClassmateEntersCamera(BattleInfoManager.newClassmate.transform);

                yield return new WaitUntil(() => menuController.newClassmateEntersView.viewState == ViewState.Closed);

                characterRemovedCondition = -1;
                BattleInfoManager.newClassmate = null;

                animatorController.AddNewClassmate(false);
            }

            yield return new WaitForSeconds(phaseController.delayNewTurnDuration);

            battleController.ProceedNextTurn();

            if(hasCharacterRemoved)
            {
                cameraController.SetMultipleTargetsCamera(BattleCameraController.MultipleTargetCameraEnum.All,
                  BattleInfoManager.GetAllCharacters().ToArray());
                cameraController.SetMultipleTargetsCamera(BattleCameraController.MultipleTargetCameraEnum.AllPlayers,
                       BattleInfoManager.activePlayers.ToArray());
                cameraController.SetMultipleTargetsCamera(BattleCameraController.MultipleTargetCameraEnum.AllEnemies,
                   BattleInfoManager.activeEnemies.ToArray());
            }

            phaseController.ChangePhase(BattlePhaseEnums.BeginTurn);
        }

        //private IEnumerator WaitUntilReturn()
        //{
        //    //BattleEntity attacker = BattleInfoManager.entityOnTurn;
        //    BattleEntity defender = BattleInfoManager.currentTargetHighlighted;

        //    //if (attacker != null)
        //    //{
        //    //    if(attacker.gameObject.activeSelf)
        //    //        yield return attacker.StartCoroutine(attacker.MoveCharacter(attacker.originalBattleRotation, attacker.originalBattlePosition, true));
        //    //}

        //    if(defender != null)
        //    {
        //        if(defender.gameObject.activeSelf)
        //            yield return defender.StartCoroutine(defender.MoveCharacter(defender.originalBattleRotation, defender.originalBattlePosition, true));
        //    }

        //    phaseController.hasFaceOff = false;

        //    BattleInfoManager.allowBattleExecution = true;
        //}

        private IEnumerator DelayResult(BattlePhaseEnums battlePhaseEnums)
        {
            yield return new WaitForSeconds(phaseController.delayNewTurnDuration);

            if (battlePhaseEnums == BattlePhaseEnums.Lose)
            {
                phaseController.ChangePhase(BattlePhaseEnums.Lose);
                animatorController.SetBattleResult(2);
            }
            else if (battlePhaseEnums == BattlePhaseEnums.Win)
            {
                phaseController.ChangePhase(BattlePhaseEnums.Win);
                animatorController.SetBattleResult(1);
            }
            else if (battlePhaseEnums == BattlePhaseEnums.Forfeited)
            {
                phaseController.ChangePhase(BattlePhaseEnums.Forfeited);
                //animatorController.SetBattleResult(1);
            }
        }

        //private void ChangeToBeginTurn()
        //{
        //    // Add helper here
        //    //AddArgumentHelper();

        //    phaseController.ChangePhase(BattlePhaseEnums.BeginTurn);
        //}


        //private void AddArgumentHelper()
        //{
        //    if (!BattleInfoManager.isPlayerTurn)
        //        return;

        //    //PlayerEntity currentHelper = (PlayerEntity)BattleInfoManager.entityOnTurn;

        //    //if (currentHelper == null)
        //    //    return;

        //    //float helpProbability = currentHelper.GetRelationshipLevelProbability();

        //    //bool gainSupport = Random.value < helpProbability;

        //    //if(gainSupport)
        //    //{
        //    //    //BattleUIManager.instance.battleInfoPanel.AddHelper(currentHelper);

        //    //    BattleInfoManager.AddArgumentHelper(currentHelper);
        //    //}
        //}
    }
}
