﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Intelligensia.Battle
{
    public class BattlePhase_TargetSelection : BattlePhase
    {
        private BattleCameraController cameraController;
        private BattlePhaseAnimatorController animatorController;
        private BattleUIManager menuController;

        private bool currentTargetIsEnemy = true;
        private bool isItemTargetSelected;


        public BattlePhase_TargetSelection(BattlePhasesController phaseController) : base(phaseController)
        {
            this.phaseController = phaseController;
            cameraController = phaseController.cameraController;
            animatorController = phaseController.animatorController;
            menuController = phaseController.menuController;
        }

        public override void Enter()
        {
            menuController.targetSelectionPanel.OnItemTargetSelectedEvent += OnItemTargetSelected;
            menuController.targetSelectionPanel.MoveSelectionEvent += OnMoveSelection;
            menuController.targetSelectionPanel.SelectMultipleTargetsEvent += OnMultipleTargetsSelection;

            menuController.BeginTargetSelection();

            currentTargetIsEnemy = BattleInfoManager.currentTargetIsEnemy;

            bool isAimingToSingleTarget = BattleInfoManager.currentAim == AimedTo.SingleEnemy
                || BattleInfoManager.currentAim == AimedTo.SinglePartyMember
                || BattleInfoManager.currentAim == AimedTo.Self;

            animatorController.SetTargetSelectionRange(isAimingToSingleTarget);
            animatorController.SelectTarget(true, currentTargetIsEnemy);

            isItemTargetSelected = false;
        }

        public override void HandleUpdate()
        {

        }

        public override void Exit()
        {
            CancelTargetSelection();

            menuController.targetSelectionPanel.OnItemTargetSelectedEvent -= OnItemTargetSelected;
            menuController.targetSelectionPanel.MoveSelectionEvent -= OnMoveSelection;
            menuController.targetSelectionPanel.SelectMultipleTargetsEvent -= OnMultipleTargetsSelection;
        }


        private void OnMoveSelection(BattleEntity selectedTarget)
        {
            cameraController.SetTargetSelectionCamera(selectedTarget.transform, currentTargetIsEnemy);
            menuController.enemyStatusPanelController.OnTargetSelectionMove(selectedTarget);
        }

        private void OnMultipleTargetsSelection()
        {
            if(currentTargetIsEnemy)
            {
                menuController.enemyStatusPanelController.SelectAllTargets();
            }
        }


        private void OnItemTargetSelected()
        {
            isItemTargetSelected = true;
            // TODO : change phase to item execution instead
            animatorController.SetFreeExecutionIndex(2);
            phaseController.ChangePhase(BattlePhaseEnums.FreeExecution);
            

            //animatorController.IsTargetSelected(true);
            //phaseController.ChangePhase(BattlePhaseEnums.TurnExecution);

            //menuController.battleInfoPanel.Close();
        }


        private void CancelTargetSelection()
        {
            animatorController.SelectTarget(false, currentTargetIsEnemy);

            menuController.CancelTargetSelection(isItemTargetSelected);
        }
    }
}
