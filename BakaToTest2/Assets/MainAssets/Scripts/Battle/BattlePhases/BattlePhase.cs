﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Intelligensia.Battle
{
    public abstract class BattlePhase
    {
        protected BattlePhasesController phaseController;

        protected BattlePhase(BattlePhasesController phaseController)
        {
            this.phaseController = phaseController;
        }

        public abstract void Enter();
        public abstract void HandleUpdate();
        public abstract void Exit();

        //protected void DisplayOnUI(/*UIManager.Alignment alignment*/)
        //{
        //    //UIManager.Instance.Display(this, alignment);
        //}

    }
}

