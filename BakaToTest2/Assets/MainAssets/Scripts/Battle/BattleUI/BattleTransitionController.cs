﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Intelligensia.Battle;

public class BattleTransitionController : ViewController
{
    private const string BattleSceneName = "BattleScene";

    private bool battleStarted = false;
    private bool retryBattle = false;

    public void ShowTransition(bool beginBattle = true)
    {
        Show();

        if (beginBattle)
        {
            battleStarted = true; 
        }
        else
        {
            battleStarted = false;
        }
    }


    public void ShowRetryTransition()
    {
        retryBattle = true;

        Show();   
    }


    public override void Close()
    {
        base.Close();
    }


    protected override void OnViewReady()
    {
        base.OnViewReady();

        if(retryBattle)
        {
            StartCoroutine(ReloadLevel());
            return;
        }

        StartCoroutine(LoadLevel());
    }


    protected override void OnViewClosed()
    {
        base.OnViewClosed();

        //if(battleStarted)
        //{
        //    BattleInfoManager.allowBattleExecution = true;
        //}
    }


    private IEnumerator LoadLevel()
    {
        AsyncOperation loader = battleStarted ?  SceneManager.LoadSceneAsync(BattleSceneName, LoadSceneMode.Additive) : SceneManager.UnloadSceneAsync(BattleSceneName);

        while (!loader.isDone)
        {
            yield return null;
        }

        yield return new WaitForSeconds(autoCloseDelay);

        Close();
    }


    private IEnumerator ReloadLevel()
    {
        AsyncOperation unloader = SceneManager.UnloadSceneAsync(BattleSceneName);

        while (!unloader.isDone)
        {
            yield return null;
        }

        AsyncOperation loader = SceneManager.LoadSceneAsync(BattleSceneName, LoadSceneMode.Additive);

        while (!loader.isDone)
        {
            yield return null;
        }

        yield return new WaitForSeconds(autoCloseDelay);

        retryBattle = false;

        battleStarted = true;

        Close();
    }
}
