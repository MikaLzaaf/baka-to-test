﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;
using UnityEngine.UI;

namespace Intelligensia.Battle
{
    public class ConflictDescriptor : MonoBehaviour
    {
        public TextMeshProUGUI descriptionText;
        public CanvasGroup canvasGroup;
        public Image background;

        public float slideHorizontalDuration = 0.3f;
        public float slideHorizontalDistance = 100f;

        public float slideVerticalDuration = 0.5f;
        public float slideVerticalDistance = 100f;

        public float fadeOutDuration = 0.5f;
        public Color positiveColor;
        public Color negativeColor;

        private bool shouldMoveToleft;

        public bool IsTransitioning { get; private set; }

        public void Show(string description, bool isPositive, bool moveToLeft)
        {
            descriptionText.text = description;

            ToggleBackgroundColor(isPositive);

            shouldMoveToleft = moveToLeft;

            SlideVertically();

            IsTransitioning = true;
        }

        private void SlideVertically()
        {
            float targetValue = transform.localPosition.y - slideVerticalDistance;

            transform.DOLocalMoveY(targetValue, slideVerticalDuration).OnComplete(SlideHorizontally);
        }

        private void SlideHorizontally()
        {
            Vector3 destination = transform.localPosition;

            float targetValue = shouldMoveToleft ? destination.x - slideHorizontalDistance : destination.x + slideHorizontalDistance;

            transform.DOLocalMoveX(targetValue, slideHorizontalDuration).OnComplete(FadeOut);
        }

        private void FadeOut()
        {
            IsTransitioning = false;

            StartCoroutine(FadingOut());
        }

        private IEnumerator FadingOut()
        {
            float elapsedTime = 0f;

            while (elapsedTime < fadeOutDuration)
            {
                elapsedTime += UnityEngine.Time.deltaTime;

                if(canvasGroup != null)
                {
                    canvasGroup.alpha = Mathf.Lerp(0, 1, elapsedTime / fadeOutDuration);
                }

                yield return null;
            }

            if (canvasGroup != null)
            {
                canvasGroup.alpha = 0;
            }

            Recycle();
        }

        private void Recycle()
        {
            ObjectPool.Recycle(gameObject);
        }

        private void ToggleBackgroundColor(bool isPositive)
        {
            if(background != null)
            {
                background.color = isPositive ? positiveColor : negativeColor;
            }
        }
    }

}

