﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CharacterInfoProgressBar : MonoBehaviour
{

    public Image unlockedBar;
    public GameObject lockedIcon;
    public GameObject unlockedIcon;
    public TextMeshProUGUI infoNumberText;

    public Color lockedColor;
    public Color unlockedColor;


    public void Initialize(CharacterInfo info, int index)
    {
        ToggleLockedIcon(info.isLocked);

        if(info.isLocked)
        {
            unlockedBar.color = lockedColor;
        }
        else
        {
            unlockedBar.color = unlockedColor;
        }

        infoNumberText.text = index.ToString();
    }


    private void ToggleLockedIcon(bool isLocked)
    {
        if(lockedIcon != null)
        {
            lockedIcon.SetActive(isLocked);
        }

        if(unlockedIcon != null)
        {
            unlockedIcon.SetActive(!isLocked);
        }
    }
}
