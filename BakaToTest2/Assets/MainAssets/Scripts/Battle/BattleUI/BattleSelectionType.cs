﻿
namespace Intelligensia.Battle
{
    public enum BattleSelectionType
    {
        Support,
        Attack,
        Item,
        Forfeit,
        Counter,
        Order,
        Analyze,
        NextMember,
        PreviousMember,
        PartyAttack,
        Reset,
        StartDebate,
        Null
    }
}

