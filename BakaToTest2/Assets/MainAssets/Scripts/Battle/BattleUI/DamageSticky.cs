﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace Intelligensia.Battle
{
    public class DamageSticky : MonoBehaviour
    {
        [Header("Evade Display")]
        public TextMeshProUGUI evadeText;
        public DamagePopupTweener evadeTweener;

        [Header("Fatigue Display")]
        public TextMeshProUGUI rightSideFatigueText;
        public TextMeshProUGUI leftSideFatigueText;
        public string fatigueTextFormat;
        //public DamagePopupTweener rightSideDamageTweener;
        //public DamagePopupTweener leftSideDamageTweener;
        public Gradient gradient;
        public FillIndicatorLerper fillIndicatorLerper;
        public Image subjectIcon;

        [Header("Knockout Display")]
        public GameObject[] knockoutIndicators;

        [Header("Pointer")]
        public UIArrowPointer pointer;
        public float delayBeforeDisableDuration = 3f;

        [Header("Camera Shake")]
        public CameraShaker cameraShaker;
        //public float waitForRecycleDuration = 3f;

        private bool isTargetNearLeftSide = true;


        private void Awake()
        {
            if (pointer != null)
            {
                pointer.TargetIsNearingScreenBorder += ChangeTextSide;
            }
        }

        private void OnDestroy()
        {
            if (pointer != null)
            {
                pointer.TargetIsNearingScreenBorder -= ChangeTextSide;
            }
        }


        public void Show(BattleEntity target, float damageAmount, DamageHitCategory hitCategory, bool tweenOnShow = true, float targetFatiguePreviously = 0f)
        {
            gameObject.SetActive(true);

            pointer.target = target.transform;

            int fatigueWhole = Mathf.FloorToInt(damageAmount);
            int residue = (int)((damageAmount - fatigueWhole) * 10);

            string formattedText = string.Format(fatigueTextFormat, fatigueWhole, residue);

            rightSideFatigueText.text = formattedText;
            leftSideFatigueText.text = formattedText;

            ChangeFatigueTextColor(target.fatigue, target.stamina);
            SetSubjectIcon();

            if(hitCategory == DamageHitCategory.FatalCriticalHit)
                ToggleKnockoutIndicators(targetFatiguePreviously >= 1f);
   
            if (tweenOnShow)
            {
                if (hitCategory == DamageHitCategory.Missed)
                {
                    StartCoroutine(TweenMissHit(hitCategory));
                }
                else if(hitCategory != DamageHitCategory.Undefined)
                {
                    StartCoroutine(TweenDamage(hitCategory));
                }
                else
                {
                    ShowCurrentFatigueValue();
                }

                float targetAmount = target.fatigue / target.stamina;

                UpdateFillIndicator(targetAmount, targetFatiguePreviously);
            }
        }


        private void ChangeFatigueTextColor(float fatigue, float stamina)
        {
            float gradientPoint = fatigue / stamina;

            rightSideFatigueText.color = gradient.Evaluate(gradientPoint);
            leftSideFatigueText.color = gradient.Evaluate(gradientPoint);
        }


        private void ChangeTextSide(UIArrowPointer.ScreenBorder screenBorder)
        {
            if (screenBorder == UIArrowPointer.ScreenBorder.Left)
            {
                isTargetNearLeftSide = true;

                rightSideFatigueText.gameObject.SetActive(true);
                leftSideFatigueText.gameObject.SetActive(false);
            }
            else if (screenBorder == UIArrowPointer.ScreenBorder.Right)
            {
                isTargetNearLeftSide = false;

                rightSideFatigueText.gameObject.SetActive(false);
                leftSideFatigueText.gameObject.SetActive(true);
            }
        }


        private IEnumerator TweenDamage(DamageHitCategory hitCategory)
        {
            if (evadeText.gameObject.activeSelf)
                evadeText.gameObject.SetActive(false);

            if (hitCategory == DamageHitCategory.FatalCriticalHit ||
                hitCategory == DamageHitCategory.NonFatalCriticalHit)
                cameraShaker.Shake();

            yield return new WaitForEndOfFrame();

            if (isTargetNearLeftSide)
            {
                rightSideFatigueText.gameObject.SetActive(true);
                leftSideFatigueText.gameObject.SetActive(false);
                //StartCoroutine(Closing());
                //rightSideDamageTweener.StartTween(hitCategory, rightSideFatigueText.color, Close);
            }
            else
            {
                rightSideFatigueText.gameObject.SetActive(false);
                leftSideFatigueText.gameObject.SetActive(true);
                //StartCoroutine(Closing());
                //leftSideDamageTweener.StartTween(hitCategory, leftSideFatigueText.color, Close);
            }

            yield return StartCoroutine(Closing());
        }


        private IEnumerator TweenMissHit(DamageHitCategory hitCategory)
        {
            yield return new WaitForEndOfFrame();

            rightSideFatigueText.gameObject.SetActive(false);
            leftSideFatigueText.gameObject.SetActive(false);

            evadeText.gameObject.SetActive(true);
            //evadeTweener.StartTween(hitCategory, Color.white, Close);
            StartCoroutine(Closing());
        }

        private void ShowCurrentFatigueValue()
        {
            if (isTargetNearLeftSide)
            {
                rightSideFatigueText.gameObject.SetActive(true);
                leftSideFatigueText.gameObject.SetActive(false);
            }
            else
            {
                rightSideFatigueText.gameObject.SetActive(false);
                leftSideFatigueText.gameObject.SetActive(true);
            }
        }

        private void UpdateFillIndicator(float targetAmount, float startAmount = 0f)
        {
            if (fillIndicatorLerper != null)
            {
                fillIndicatorLerper.StartLerp(targetAmount, startAmount);
            }
        }

        private void ToggleKnockoutIndicators(bool active)
        {
            if (knockoutIndicators.Length == 0)
                return;

            for (int i = 0; i < knockoutIndicators.Length; i++)
            {
                knockoutIndicators[i].SetActive(active);
            }
        }

        private void SetSubjectIcon()
        {
            if(subjectIcon != null)
            {
                subjectIcon.sprite = IconLoader.GetSubjectIcon(BattleInfoManager.currentActiveSubject);
            }
        }


        private void Close()
        {
            if (!gameObject.activeInHierarchy)
                return;

            StartCoroutine(Closing());
        }

        private IEnumerator Closing()
        {
            yield return new WaitForSeconds(delayBeforeDisableDuration);

            ObjectPool.Recycle(gameObject);
        }
    }
}

