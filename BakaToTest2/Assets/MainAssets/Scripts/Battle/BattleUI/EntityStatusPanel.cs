﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Intelligensia.Battle;

public class EntityStatusPanel : MonoBehaviour
{
    public GameObject simpleDisplayContent;

    [Header("Fatigue Display")]
    public TextMeshProUGUI currentFatigueText;
    public string fatigueTextFormat;
    public DamagePopupTweener damageTweener;
    public Gradient gradient;

    [Header("Pointer")]
    public UIArrowPointer pointer;
    public Vector3 targetOffset;

    public float delayBeforeDisableDuration = 0.5f;

    private BattleEntity ownerEntity;


    public void Initialize(BattleEntity entity)
    {
        gameObject.SetActive(true);

        ownerEntity = entity;

        if (pointer != null)
        {
            pointer.target = ownerEntity.transform;
        }

        int fatigueWhole = Mathf.FloorToInt(ownerEntity.fatigue);
        int residue = (int)((ownerEntity.fatigue - fatigueWhole) * 10);

        string formattedText = string.Format(fatigueTextFormat, fatigueWhole, residue);

        currentFatigueText.text = formattedText;

        ownerEntity.FatigueUpdateEvent += OnFatigueChanged;

        gameObject.SetActive(false);
    }


    public void Detach()
    {
        ownerEntity.FatigueUpdateEvent -= OnFatigueChanged;

        gameObject.SetActive(false);
    }


    private void OnFatigueChanged(float fatigue)
    {
        gameObject.SetActive(true);

        int fatigueWhole = Mathf.FloorToInt(fatigue);
        int residue = (int)((fatigue - fatigueWhole) * 10);

        string formattedText = string.Format(fatigueTextFormat, fatigueWhole, residue, 
            ownerEntity.stamina);

        currentFatigueText.text = formattedText;

        ChangeFatigueTextColor(fatigue);

        damageTweener.StartTween(currentFatigueText.color, Close);
    }

    private void ChangeFatigueTextColor(float fatigue)
    {
        float gradientPoint = fatigue / ownerEntity.stamina;

        currentFatigueText.color = gradient.Evaluate(gradientPoint);
    }

    public void Close()
    {
        StartCoroutine(Closing());
    }


    private IEnumerator Closing()
    {
        yield return new WaitForSeconds(delayBeforeDisableDuration);

        gameObject.SetActive(false);
    }
}
