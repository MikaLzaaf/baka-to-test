﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Intelligensia.Cutscene;

namespace Intelligensia.Battle
{
    public class BattleUIPanel_PartyAttack : UIViewController
    {
        public float delayToSequence = 1f;

        public event Action PartyAttackSelectedEvent;


        private const string PartyAttackCutsceneName = "PartyAttack";


        protected override void OnViewReady()
        {
            base.OnViewReady();

            BattleUIManager.instance.ToggleForFaceOff(true, true);

            StartCoroutine(ShowCutscene());
        }

        private IEnumerator ShowCutscene()
        {
            Vector3 cutscenePlayPosition = BattleInfoManager.activeEnemies[0].transform.position;

            Actor[] activeActors = PrepareActors(true);
            Actor[] reserveActors = PrepareActors(false);

            CutsceneManager.instance.PlayCutsceneAt(PartyAttackCutsceneName, cutscenePlayPosition, activeActors, reserveActors);

            yield return new WaitWhile(() => CutsceneManager.instance.IsPlaying);

            yield return new WaitForSeconds(delayToSequence);

            BattleUIManager.instance.ToggleForFaceOff(false, true);

            //BattleInfoManager.currentStressPoint -= 100;

            PartyAttackSelectedEvent?.Invoke();

            UINavigator.instance.ClearPreviousWindows();

            UINavigator.instance.OpenPreviousWindow();
        }


        private Actor[] PrepareActors(bool isActiveActors)
        {
            List<Actor> actors = new List<Actor>();

            var characters = isActiveActors ? BattleInfoManager.activePlayers : BattleInfoManager.reservePlayers;

            for (int i = 0; i < characters.Count; i++)
            {
                Actor actor = characters[i].GetComponent<Actor>();

                if (actor != null)
                    actors.Add(actor);
            }

            return actors.ToArray();
        }
    }
}

