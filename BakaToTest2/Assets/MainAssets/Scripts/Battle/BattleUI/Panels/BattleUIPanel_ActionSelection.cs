﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.InputSystem;
using TMPro;
using Intelligensia.Battle.StatusEffects;

namespace Intelligensia.Battle
{
    public class BattleUIPanel_ActionSelection : UIViewController
    {
        [Header("HoldInput")]
        [SerializeField] private HoldButtonIndicator guardHoldInput = default;
        [SerializeField] private HoldButtonIndicator forfeitHoldInput = default;
        [SerializeField] private HoldButtonIndicator resetHoldInput = default;

        [Header("Action Selection Canvas Group")]
        [SerializeField] private BattleUISubPanel_ActionSelectionLabel[] actionSelectionLabels = default;

        [Header("Listening Channels")]
        [SerializeField] private TurnEndedEventChannelSO turnEndedEventChannel = default;

        public event Action<bool> OnPanelReady;
        public event Action OnNextMemberEvent;
        public event Action OnPreviousMemberEvent;

        private BattlePhasesController phasesController;
        private BattleUIManager uiManager;
        private BattleController battleController;

        private Dictionary<BattleSelectionType, BattleUISubPanel_ActionSelectionLabel> actionSelectionLabelsLookup;

        private bool isHoldingButton = false;
        private bool canStartDebate = false;

        private const float ForfeitHoldDuration = 1f;
        private const float DefaultHoldDuration = 0.6f;

        private const string BattleActionControlsInputMap = "Battle Action Controls";
        private const string MenuControlsInputMap = "Menu Controls";



        protected override void Awake()
        {
            base.Awake();

            phasesController = BattlePhasesController.instance;
            uiManager = BattleUIManager.instance;
            battleController = BattleController.instance;

            if(actionSelectionLabels.Length > 0)
            {
                actionSelectionLabelsLookup = new Dictionary<BattleSelectionType, BattleUISubPanel_ActionSelectionLabel>();
               
                for (int i = 0; i < actionSelectionLabels.Length; i++)
                {
                    actionSelectionLabelsLookup.Add(actionSelectionLabels[i].actionType, actionSelectionLabels[i]);
                }
            }

            if (turnEndedEventChannel != null)
            {
                turnEndedEventChannel.OnTurnEnded += OnTurnExecutionEnded;
            }
        }


        protected override void OnViewReady()
        {
            base.OnViewReady();

            OnPanelReady?.Invoke(true);

            SwitchInputActionMap(true);

            PrepareSelectableActionLabels();

            isHoldingButton = false;

            canStartDebate = true;

            //var startDebateLabel = GetActionLabel(BattleSelectionType.StartDebate);
            //startDebateLabel.ToggleInteractability(true);
        }


        protected override void OnViewClosed()
        {
            base.OnViewClosed();
         
            OnPanelReady?.Invoke(false);

            if (BattlePhasesController.instance.CurrentPhaseEnum != BattlePhaseEnums.LongIdle)
            {
                SwitchInputActionMap(false);
            }

            //var startDebateLabel = GetActionLabel(BattleSelectionType.StartDebate);
            //startDebateLabel.ToggleInteractability(false);
        }


        public override void Show()
        {
            base.Show();

            //BattleUIManager.instance.battleInfoPanel.Show();

            SetActionSelected(BattleSelectionType.Null);

            var partyAttackLabel = GetActionLabel(BattleSelectionType.PartyAttack);

            partyAttackLabel.gameObject.SetActive(BattleInfoManager.currentStressPoint >= 100);

            //UpdateHelperAmount();
        }


        public override void Close()
        {
            base.Close();

            //BattleUIManager.instance.battleInfoPanel.Close();
        }

        private void OnTurnExecutionEnded()
        {
            // Reset selectable actions
            if (actionSelectionLabels.Length == 0)
                return;

            for(int i = 0; i < actionSelectionLabels.Length; i++)
            {
                //if (battleController.isClassBattleMode)
                //{
                //    if (actionSelectionLabels[i].actionType == BattleSelectionType.SwapMember)
                //        continue;
                //}
                   
                actionSelectionLabels[i].ToggleInteractability(true);
            }

            //isInSameTurn = false;
        }

        public void OnOrderExecutionEnded()
        {
            // Disable order action
            ToggleActionLabel(BattleSelectionType.Order, false);
        }

        private void SetActionSelected(BattleSelectionType battleSelectionType)
        {
            if(battleController != null)
                battleController.actionSelected = battleSelectionType;

            BattleInfoManager.entityOnFocus.actionSelected = battleSelectionType;
        }

        public void PrepareSelectableActionLabels()
        {
            // Need to disable/enable certain actions depending on 
            // - Status effects on the player
            // - Current turn phase e.g. After OrderExecution, After Swap Member / Weapon

            BattleAction characterAction = BattleInfoManager.GetFirstCharacterAction(true);

            if (characterAction != null)
            {
                if (characterAction.selectionType == BattleSelectionType.Attack || characterAction.selectionType == BattleSelectionType.Support)
                {
                    ToggleActionLabel(BattleSelectionType.Attack, true);
                    ToggleActionLabel(BattleSelectionType.Support, true);
                    ToggleActionLabel(BattleSelectionType.Item, false);
                    ToggleActionLabel(BattleSelectionType.Counter, false);
                }
                else
                {
                    ToggleActionLabel(BattleSelectionType.Attack, false);
                    ToggleActionLabel(BattleSelectionType.Support, false);
                    ToggleActionLabel(BattleSelectionType.Item, false);
                    ToggleActionLabel(BattleSelectionType.Counter, false);
                }

                if(characterAction.selectionType != BattleSelectionType.Item)
                {
                    var resetActionLabel = GetActionLabel(BattleSelectionType.Reset);
                    resetActionLabel.gameObject.SetActive(true);
                }
            }
            else
            {
                ToggleActionLabel(BattleSelectionType.Attack, true);
                ToggleActionLabel(BattleSelectionType.Support, true);
                ToggleActionLabel(BattleSelectionType.Item, true);
                ToggleActionLabel(BattleSelectionType.Counter, true);

                var resetActionLabel = GetActionLabel(BattleSelectionType.Reset);
                resetActionLabel.gameObject.SetActive(false);
            }
        }

        private void ToggleActionLabel(BattleSelectionType battleSelectionType, bool active)
        {
            BattleUISubPanel_ActionSelectionLabel actionLabel = GetActionLabel(battleSelectionType);

            if(actionLabel != null)
                actionLabel.ToggleInteractability(active);
        }

        private BattleUISubPanel_ActionSelectionLabel GetActionLabel(BattleSelectionType battleSelectionType)
        {
            actionSelectionLabelsLookup.TryGetValue(battleSelectionType, out BattleUISubPanel_ActionSelectionLabel actionLabel);

            return actionLabel;
        }

        private void NextMember()
        {
            SwitchMember(1);
        }

        private void PreviousMember()
        {
            SwitchMember(-1);
        }

        private void SwitchMember(int memberIndex)
        {
            if (BattleInfoManager.activePlayers.Count <= 1)
                return;

            if (memberIndex > 0)
                OnNextMemberEvent?.Invoke();
            else if (memberIndex < 0)
                OnPreviousMemberEvent?.Invoke();
        }

        private void StartDebate()
        {
            if (!GetActionLabel(BattleSelectionType.StartDebate).IsSelectable)
                return;

            uiManager.AskConfirmationForDebate();
        }

        #region Inputs

        private void SwitchInputActionMap(bool active)
        {
            if (active)
            {
                uiManager.SwitchInputActionMap(BattleActionControlsInputMap);
                uiManager.actionInput.actions.FindActionMap(MenuControlsInputMap).Disable();

                uiManager.OnRightTabPressedEvent -= NextMember;
                uiManager.OnLeftTabPressedEvent -= PreviousMember;
                uiManager.OninfoButtonPressedEvent -= StartDebate;
            }
            else
            {
                uiManager.SwitchInputActionMap(MenuControlsInputMap);
                uiManager.actionInput.actions.FindActionMap(BattleActionControlsInputMap).Disable();

                if(canStartDebate)
                {
                    uiManager.OnRightTabPressedEvent += NextMember;
                    uiManager.OnLeftTabPressedEvent += PreviousMember;
                    uiManager.OninfoButtonPressedEvent += StartDebate;
                }
            }
        }

        // Cross - Attack, Circle - Guard(Hold), Triangle - Support, Square - Item
        // L1 - Forfeit/Retreat (Hold), R1 - Order
        // L2 - Analyze, R2 - Weapon Swap, Left Directional Button - Swap Member
        // Touchpad - Team Attack

        public void OnSupport(InputAction.CallbackContext context)
        {
            if (!canProcessInput)
                return;

            if (isHoldingButton || BattlePhasesController.instance.IsAFK())
                return;

            if (!GetActionLabel(BattleSelectionType.Support).IsSelectable)
                return;

            if (context.performed)
            {
                SetActionSelected(BattleSelectionType.Support);

                uiManager.ShowSupportSkillSelectionPanel();

                phasesController.animatorController.SetPlayerActionIndex(1);
            }
        }

        public void OnItem(InputAction.CallbackContext context)
        {
            if (!canProcessInput)
                return;

            if (isHoldingButton || BattlePhasesController.instance.IsAFK())
                return;

            if (!GetActionLabel(BattleSelectionType.Item).IsSelectable)
                return;

            if (context.performed)
            {
                uiManager.BeginItemSelection();

                SetActionSelected(BattleSelectionType.Item);
     
                phasesController.animatorController.SetPlayerActionIndex(2);
            }
        }

        public void OnAttack(InputAction.CallbackContext context)
        {
            if (!canProcessInput)
                return;

            if (isHoldingButton || BattlePhasesController.instance.IsAFK())
                return;

            if (!GetActionLabel(BattleSelectionType.Attack).IsSelectable)
                return;

            if (context.performed)
            {
                SetActionSelected(BattleSelectionType.Attack);

                uiManager.ShowAttackSkillSelectionPanel();

                phasesController.animatorController.SetPlayerActionIndex(1);
            }
        }

        public void OnGuard(InputAction.CallbackContext context)
        {
            if (!canProcessInput)
                return;

            if (BattlePhasesController.instance.IsAFK())
                return;

            if (!GetActionLabel(BattleSelectionType.Counter).IsSelectable)
                return;

            switch (context.phase)
            {
                case InputActionPhase.Performed:
                    SetActionSelected(BattleSelectionType.Counter);

                    //uiManager.ShowCounterSkillSelectionPanel();

                    BattleInfoManager.CreateNewAction(BattleSelectionType.Counter, BattleSelectionType.Counter.ToString(), true);
                    BattleInfoManager.AddActionSelected(BattleInfoManager.entityOnFocus);

                    BattleInfoManager.entityOnFocus.Guard(true);

                    PrepareSelectableActionLabels();

                    isHoldingButton = false;

                    //Close();
                    break;

                case InputActionPhase.Started:
                    if (guardHoldInput != null)
                    {
                        guardHoldInput.StartHolding(DefaultHoldDuration);
                    }

                    isHoldingButton = true;
                    break;

                case InputActionPhase.Canceled:
                    if (!isHoldingButton)
                        return;

                    isHoldingButton = false;

                    guardHoldInput.CancelHolding();
                    break;
            }
        }

        public void OnOrder(InputAction.CallbackContext context)
        {
            if (!canProcessInput)
                return;

            if (isHoldingButton || BattlePhasesController.instance.IsAFK())
                return;

            if (!GetActionLabel(BattleSelectionType.Order).IsSelectable)
                return;

            if (context.performed)
            {
                // open Order panel
                uiManager.ShowOrderPanel();

                SetActionSelected(BattleSelectionType.Order);

                phasesController.animatorController.SetFreeExecutionIndex(1);
            }
        }

        public void OnForfeit(InputAction.CallbackContext context)
        {
            if (!canProcessInput)
                return;

            if (BattlePhasesController.instance.IsAFK())
                return;

            if (!GetActionLabel(BattleSelectionType.Forfeit).IsSelectable)
                return;

            switch (context.phase)
            {
                case InputActionPhase.Performed:
                    SetActionSelected(BattleSelectionType.Forfeit);

                    uiManager.ShowForfeitPanel();
                    break;

                case InputActionPhase.Started:
                    if (forfeitHoldInput != null)
                    {
                        forfeitHoldInput.StartHolding(ForfeitHoldDuration);
                    }

                    isHoldingButton = true;
                    break;

                case InputActionPhase.Canceled:
                    if (!isHoldingButton)
                        return;

                    isHoldingButton = false;

                    forfeitHoldInput.CancelHolding();
                    break;
            }
        }

        public void OnAnalyze(InputAction.CallbackContext context)
        {
            if (!canProcessInput)
                return;

            if (isHoldingButton || BattlePhasesController.instance.IsAFK())
                return;

            if (!GetActionLabel(BattleSelectionType.Analyze).IsSelectable)
                return;

            if (context.performed)
            {
                canStartDebate = false;

                uiManager.ShowAnalyzeScreen();
            }
        }

        public void OnPartyAttack(InputAction.CallbackContext context)
        {
            if (!canProcessInput)
                return;

            if (isHoldingButton || BattlePhasesController.instance.IsAFK())
                return;

            if (!GetActionLabel(BattleSelectionType.PartyAttack).IsSelectable)
                return;

            if (context.performed)
            {
                if(BattleInfoManager.currentStressPoint >= 100)
                {
                    SetActionSelected(BattleSelectionType.PartyAttack);

                    uiManager.ShowPartyAttackPanel();
                }
            }
        }

        public void OnReset(InputAction.CallbackContext context)
        {
            if (!canProcessInput || !GetActionLabel(BattleSelectionType.Reset).gameObject.activeSelf)
                return;

            if (BattlePhasesController.instance.IsAFK())
                return;

            if (!GetActionLabel(BattleSelectionType.Reset).IsSelectable)
                return;

            switch (context.phase)
            {
                case InputActionPhase.Performed:
                    BattleInfoManager.RemoveCharacterActions();

                    PrepareSelectableActionLabels();
                    isHoldingButton = false;
                    break;

                case InputActionPhase.Started:
                    if (resetHoldInput != null)
                        resetHoldInput.StartHolding(DefaultHoldDuration);

                    isHoldingButton = true;
                    break;

                case InputActionPhase.Canceled:
                    if (!isHoldingButton)
                        return;

                    isHoldingButton = false;

                    resetHoldInput.CancelHolding();
                    break;
            }
        }

        public void OnNextMember(InputAction.CallbackContext context)
        {
            if (!canProcessInput)
                return;

            if (isHoldingButton || BattlePhasesController.instance.IsAFK())
                return;

            if (context.performed)
            {
                SwitchMember(1);
            }
        }

        public void OnPreviousMember(InputAction.CallbackContext context)
        {
            if (!canProcessInput)
                return;

            if (isHoldingButton || BattlePhasesController.instance.IsAFK())
                return;

            if (context.performed)
            {
                SwitchMember(-1);
            }
        }

        public void OnStartDebate(InputAction.CallbackContext context)
        {
            if (!canProcessInput)
                return;

            if (isHoldingButton || BattlePhasesController.instance.IsAFK())
                return;

            if (!GetActionLabel(BattleSelectionType.StartDebate).IsSelectable)
                return;

            if (context.performed)
            {
                uiManager.AskConfirmationForDebate();
            }
        }
        #endregion

    }
}
