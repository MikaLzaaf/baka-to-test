﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Intelligensia.UI.PlayerMenu;

namespace Intelligensia.Battle
{
    public class BattleUIPanel_WinBattle : UIViewController
    {
        public Action ReturnToMapEvent;

        [Header("Item List")]
        [SerializeField] private InventoryListItem listItemPrefab = default;
        [SerializeField] private Transform itemListContainer = default;

        [Header("Ilmu List")]
        [SerializeField] private BattleUIItem_IlmuChanges[] ilmuChanges = default;

        private Dictionary<Subjects, BattleUIItem_IlmuChanges> ilmuAcquiredLookup;


        protected override void Awake()
        {
            base.Awake();

            ilmuAcquiredLookup = new Dictionary<Subjects, BattleUIItem_IlmuChanges>();

            if (ilmuChanges == null)
                return;

            for(int i = 0; i < ilmuChanges.Length; i++)
            {
                ilmuAcquiredLookup.Add(ilmuChanges[i].subject, ilmuChanges[i]);
            }
        }

        public override void Show()
        {
            base.Show();

            GenerateItemDropList();

            GenerateIlmuGainedList();
        }

        protected override void OnViewReady()
        {
            base.OnViewReady();

            BattleUIManager.instance.OnSubmitButtonPressedEvent += ReturnToMap;

            BattleUIManager.instance.SetInputActiveState(true);
        }


        private void GenerateItemDropList()
        {
            Item[] enemyItemDrops = BattleInfoManager.currentChallengeInfo.obtainableItems;

            for (int i = 0; i < enemyItemDrops.Length; i++)
            {
                InventoryListItem itemList = Instantiate(listItemPrefab, itemListContainer, false);
                itemList.Initialize(enemyItemDrops[i], true);

                PlayerInventoryManager.instance.AddItem(enemyItemDrops[i]);
            }
        }


        private void GenerateIlmuGainedList()
        {
            for (int i = 0; i < ilmuChanges.Length; i++)
            {
                ilmuChanges[i].ResetValue(true, ilmuChanges[i].subject);
            }

            IlmuCost[] enemyIlmuDrops = BattleInfoManager.currentChallengeInfo.ilmuRewards;

            for(int i = 0; i < enemyIlmuDrops.Length; i++)
            {
                var ilmuChanges = GetIlmuChanges(enemyIlmuDrops[i].subject);

                ilmuChanges.InitializeGain(enemyIlmuDrops[i]);
            }
        }

        private BattleUIItem_IlmuChanges GetIlmuChanges(Subjects subject)
        {
            ilmuAcquiredLookup.TryGetValue(subject, out BattleUIItem_IlmuChanges ilmuChanges);
            return ilmuChanges;
        }

        public void ReturnToMap()
        {
            BattleUIManager.instance.OnSubmitButtonPressedEvent -= ReturnToMap;
            Debug.Log("Return to map");
            ReturnToMapEvent?.Invoke();

            Close();
        }


    }
}
