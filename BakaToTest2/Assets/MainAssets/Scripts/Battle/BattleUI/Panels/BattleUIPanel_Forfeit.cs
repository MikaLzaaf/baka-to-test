﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


namespace Intelligensia.Battle
{
    public class BattleUIPanel_Forfeit : UIViewController
    {
        private enum PanelType
        {
            Confirming,
            Forfeiting
        }


        public event Action OnForfeitedEvent;
        public event Action OnSkipResultEvent;

        [Header("Panels")]
        [SerializeField] private GameObject confirmationPanel = default;
        [SerializeField] private GameObject forfeitingPanel = default;

        [Header("Active Members List")]
        [SerializeField] private BattleUIItem_ForfeitResult activeMemberListItemPrefab = default;
        [SerializeField] private Transform activeListContainer = default;
        [SerializeField] private ViewController activeMemberPanel = default;

        [Header("Reserve Members List")]
        [SerializeField] private BattleUIItem_ForfeitResult reserveMemberListItemPrefab = default;
        [SerializeField] private Transform reserveListContainer = default;
        [SerializeField] private ViewController reserveMemberPanel = default;


        [Header("Ilmu Loss Items")]
        [SerializeField] private BattleUIItem_IlmuChanges[] ilmuLosses = default;

        private List<BattleUIItem_ForfeitResult> activeMemberListItems;
        private List<BattleUIItem_ForfeitResult> reserveMemberListItems;

        private Dictionary<Subjects, BattleUIItem_IlmuChanges> ilmuLossLookup;

        private int resultIndex = 0;


        protected override void Awake()
        {
            base.Awake();

            ilmuLossLookup = new Dictionary<Subjects, BattleUIItem_IlmuChanges>();

            if(ilmuLosses != null)
            {
                for(int i = 0; i < ilmuLosses.Length; i++)
                {
                    ilmuLossLookup.Add(ilmuLosses[i].subject, ilmuLosses[i]);
                }
            }
        }

        public override void Show()
        {
            base.Show();

            TogglePanels(PanelType.Confirming);

            resultIndex = 0;
        }

        protected override void OnViewReady()
        {
            base.OnViewReady();

            BattleUIManager.instance.OnCancelButtonPressedEvent += Cancel;
        }

        public override void Close()
        {
            base.Close();

            if(resultIndex == 0)
                BattleUIManager.instance.OnCancelButtonPressedEvent -= Cancel;
        }

        public void InitializePanel(PlayerEntity[] activeMembers, PlayerEntity[] reserveMembers, IlmuCost[] battleCost)
        {
            resultIndex = 1;

            BattleUIManager.instance.OnCancelButtonPressedEvent -= Cancel;

            BattleUIManager.instance.OnSubmitButtonPressedEvent += NextResult;
            BattleUIManager.instance.OnCancelButtonPressedEvent += Skip;

            TogglePanels(PanelType.Forfeiting);

            GenerateIlmuLosses(battleCost);

            GenerateActiveMemberResult(activeMembers, ilmuLosses);

            GenerateReserveMemberResult(reserveMembers, ilmuLosses);

            ToggleResults(true);

            BattleUIManager.instance.Close();
            BattleUIManager.instance.SetInputActiveState(true);
        }

        private void GenerateIlmuLosses(IlmuCost[] battleCost)
        {
            for (int i = 0; i < ilmuLosses.Length; i++)
            {
                ilmuLosses[i].ResetValue(false, ilmuLosses[i].subject);
            }

            for (int i = 0; i < battleCost.Length; i++)
            {
                BattleUIItem_IlmuChanges ilmuLoss = GetIlmuLoss(battleCost[i].subject);

                ilmuLoss.InitializeLoss(battleCost[i]);
            }
        }

        private void GenerateActiveMemberResult(PlayerEntity[] activeMembers, BattleUIItem_IlmuChanges[] ilmuLosses)
        {
            if (activeMemberListItems != null && activeMemberListItems.Count > 0)
            {
                if (activeMemberListItems.Count > 0)
                {
                    for (int i = activeMemberListItems.Count - 1; i >= 0; i--)
                    {
                        Destroy(activeMemberListItems[i].gameObject);
                    }
                }

                activeMemberListItems.Clear();
            }
            else
                activeMemberListItems = new List<BattleUIItem_ForfeitResult>();

            for (int i = 0; i < activeMembers.Length; i++)
            {
                BattleUIItem_ForfeitResult listItem = Instantiate(activeMemberListItemPrefab, activeListContainer, false);
                activeMemberListItems.Add(listItem);

                listItem.InitializeItem(activeMembers[i], ilmuLosses);
            }
        }

        private void GenerateReserveMemberResult(PlayerEntity[] reserveMembers, BattleUIItem_IlmuChanges[] ilmuLosses)
        {
            if (reserveMemberListItems != null && reserveMemberListItems.Count > 0)
            {
                if (reserveMemberListItems.Count > 0)
                {
                    for (int i = reserveMemberListItems.Count - 1; i >= 0; i--)
                    {
                        Destroy(reserveMemberListItems[i].gameObject);
                    }
                }

                reserveMemberListItems.Clear();
            }
            else
                reserveMemberListItems = new List<BattleUIItem_ForfeitResult>();

            for (int i = 0; i < reserveMembers.Length; i++)
            {
                BattleUIItem_ForfeitResult listItem = Instantiate(reserveMemberListItemPrefab, reserveListContainer, false);
                reserveMemberListItems.Add(listItem);

                listItem.InitializeItem(reserveMembers[i], ilmuLosses);
            }
        }

        private BattleUIItem_IlmuChanges GetIlmuLoss(Subjects subject)
        {
            ilmuLossLookup.TryGetValue(subject, out BattleUIItem_IlmuChanges ilmuLoss);
            return ilmuLoss;
        }

        private void TogglePanels(PanelType panelType)
        {
            if (confirmationPanel != null)
                confirmationPanel.SetActive(panelType == PanelType.Confirming);

            if (forfeitingPanel != null)
                forfeitingPanel.SetActive(panelType == PanelType.Forfeiting);
        }

        private void ToggleResults(bool showActiveMember)
        {
            if (activeMemberPanel != null)
            {
                if (showActiveMember)
                    activeMemberPanel.Show();
                else
                    activeMemberPanel.Close();
            }

            if (reserveMemberPanel != null)
            {
                if (!showActiveMember)
                    reserveMemberPanel.Show();
                else
                    reserveMemberPanel.Close();
            }
        }

        private void NextResult()
        {
            resultIndex += 1;

            if(resultIndex >= 3)
            {
                Skip();
            }
            else if(resultIndex == 2)
            {
                ToggleResults(false);
            }
        }

        private void Skip()
        {
            BattleUIManager.instance.OnSubmitButtonPressedEvent -= NextResult;
            BattleUIManager.instance.OnCancelButtonPressedEvent -= Skip;

            // Unload battle scene
            OnSkipResultEvent?.Invoke();

            Close();
        }


        #region Methods on Button

        public void Forfeit()
        {
            UINavigator.instance.ClearWindowsList();

            OnForfeitedEvent?.Invoke();
        }

        public void Cancel()
        {
            if(canProcessInput)
                UINavigator.instance.OpenPreviousWindow();
        }

        #endregion
    }
}

