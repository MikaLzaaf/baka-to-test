using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Intelligensia.Battle
{
    public class BattleUIPanel_TurnExecution : ViewController
    {
        [Header("Execution Stuff")]
        [SerializeField] private BattleUIItem_TurnExecution[] listItems = default;
        [SerializeField] private float delayBetweenArgument = 0.4f;
        [SerializeField] private float delayAfterExecution = 0.4f;

        private BattleAction[] argumentsToExecute = { };

        public void Show(BattleAction[] argumentsToExecute)
        {
            base.Show();

            this.argumentsToExecute = argumentsToExecute;

            for(int i = 0; i < listItems.Length; i++)
            {
                listItems[i].CloseImmediately();
            }
        }

        public override void Close()
        {
            for (int i = 0; i < listItems.Length; i++)
            {
                if (listItems[i].gameObject.activeSelf)
                    listItems[i].Close();
            }

            base.Close();
        }

        protected override void OnViewReady()
        {
            base.OnViewReady();

            StartCoroutine(ShowExecution());
        }

        private IEnumerator ShowExecution()
        {
            for(int i = 0; i < argumentsToExecute.Length; i++)
            {
                listItems[i].InitializeItem(argumentsToExecute[i]);

                yield return new WaitUntil(() => listItems[i].viewState == ViewState.Ready);

                yield return new WaitForSeconds(delayBetweenArgument);

                if (argumentsToExecute[i].IsTargetDestroyed())
                    break;
            }

            yield return new WaitForSeconds(delayAfterExecution);

            Close();
        }
    }
}

