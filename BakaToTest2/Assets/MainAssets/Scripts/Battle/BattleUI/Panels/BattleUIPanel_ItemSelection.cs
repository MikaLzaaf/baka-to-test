﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;

namespace Intelligensia.Battle
{
    public class BattleUIPanel_ItemSelection : UIViewController
    {
        [Header("List Items")]
        [SerializeField] private BattleUIItem_ItemSelect listItemPrefab = default;
        [SerializeField] private float listItemsTransitionInterval = 0.05f;
        [SerializeField] private float listItemsTransitionDuration = 0.05f;
        [SerializeField] private float listItemsTransitionSlideOffset = -100;

        [Header("Others")]
        [SerializeField] private BattleUISubPanel_ListItemDescription listItemDescriptionPanel = default;
        [SerializeField] private Image highlightedPlayerIcon = default;
        [SerializeField] private TextMeshProUGUI highlightedPlayerNameText = default;

        public event Action<Item> ItemSelectedEvent;

        private List<BattleUIItem_ItemSelect> listItems;

        private Sequence listItemsTransitionSequence;


        public override void Show()
        {
            base.Show();

            UpdateItemDescription(null);

            GenerateList();
        }

        protected override void OnViewReady()
        {
            base.OnViewReady();

            BattleUIManager.instance.OnCancelButtonPressedEvent += OnCancelPressed;

            if (listItems.Count > 0)
            {
                UINavigator.instance.SetDefaultSelectedInRuntime(listItems[0].gameObject);
            }
            else
                UpdateItemDescription(null);
        }


        public override void Close()
        {
            base.Close();

            BattleUIManager.instance.OnCancelButtonPressedEvent -= OnCancelPressed;

            //if (listItemDescriptionPanel != null)
            //{
            //    listItemDescriptionPanel.Close();
            //}
        }

        private void OnCancelPressed()
        {
            if(canProcessInput)
                UINavigator.instance.OpenPreviousWindow();
        }

        //private void NextList()
        //{
        //    if (!canProcessInput)
        //        return;

        //    //if (tabController != null)
        //    //{
        //    //    tabController.SwitchTab(1);
        //    //}

        //    ClearList();
        //    GenerateList();
        //}

        //private void PreviousList()
        //{
        //    if (!canProcessInput)
        //        return;

        //    //if (tabController != null)
        //    //{
        //    //    tabController.SwitchTab(-1);
        //    //}

        //    ClearList();
        //    GenerateList();
        //}

        public void GenerateList(bool setDefaultSelected = false)
        {
            ClearList();

            UpdatePlayerHighlightedDisplay((PlayerEntity)BattleInfoManager.entityOnFocus);

            Item[] currentItemList = PlayerInventoryManager.instance.GetItemsByCategory(ItemCategory.Consumable);

            if (listItemsTransitionSequence != null)
                listItemsTransitionSequence.Kill();

            listItemsTransitionSequence = DOTween.Sequence();

            for (int i = 0; i < currentItemList.Length; i++)
            {
                BattleUIItem_ItemSelect listItem = Instantiate(listItemPrefab);
                listItems.Add(listItem);

                listItem.transform.SetParent(content, false);

                listItem.Initialize(currentItemList[i], this);

                listItem.onClick.AddListener(delegate
                {
                    OnListItemClick(listItem);
                });

                var item = listItem;
                float targetAlphaValue = item.canvasGroup.alpha;

                item.canvasGroup.alpha = 0;

                listItemsTransitionSequence.Insert(i * listItemsTransitionInterval, item.canvasGroup.DOFade(targetAlphaValue, listItemsTransitionDuration));
                listItemsTransitionSequence.Insert(i * listItemsTransitionInterval, item.transform.DOLocalMoveX(item.transform.localPosition.x + listItemsTransitionSlideOffset, listItemsTransitionDuration).From());
            }

            listItemsTransitionSequence.OnComplete(delegate
            {
                UINavigator.instance.SetupVerticalNavigation(listItems, true);

                if (setDefaultSelected)
                    UINavigator.instance.SetDefaultSelectedInRuntime(listItems[0].gameObject);
            });
        }

        private void ClearList()
        {
            if (listItems != null && listItems.Count > 0)
            {
                if (listItems.Count > 0)
                {
                    for (int i = listItems.Count - 1; i >= 0; i--)
                    {
                        Destroy(listItems[i].gameObject);
                    }
                }

                listItems.Clear();
            }
            else
            {
                listItems = new List<BattleUIItem_ItemSelect>();
            }
        }

        private void OnListItemClick(BattleUIItem_ItemSelect listItem)
        {
            BattleInfoManager.selectedItem = listItem.item;

            BattleInfoManager.CreateNewAction(BattleSelectionType.Item, listItem.item, true);

            ItemSelectedEvent?.Invoke(listItem.item);
        }


        public void UpdateItemDescription(Item selectedItem)
        {
            if(listItemDescriptionPanel != null)
                listItemDescriptionPanel.ShowDescription(selectedItem);
        }

        private void UpdatePlayerHighlightedDisplay(PlayerEntity playerEntity)
        {
            highlightedPlayerIcon.sprite = PlayerInfoManager.LoadCharacterIcon(playerEntity.characterId);
            highlightedPlayerNameText.text = playerEntity.characterName;
        }
    }
}
