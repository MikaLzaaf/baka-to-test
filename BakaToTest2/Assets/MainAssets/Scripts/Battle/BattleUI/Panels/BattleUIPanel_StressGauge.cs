﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace Intelligensia.Battle
{
    public class BattleUIPanel_StressGauge : ViewController
    {
        private struct StressStruct
        {
            public int targetStressValue;

            public StressStruct(int targetValue) 
            {
                targetStressValue = targetValue;
            }
        }

        public FillIndicatorLerper fillIndicatorLerper;

        public TextMeshProUGUI stressValueText;

        private int _currentStressValue = 0;
        private int currentStressValue
        {
            get
            {
                return _currentStressValue;
            }
            set
            {
                _currentStressValue = Mathf.Clamp(value, 0, BattleInfoManager.MaxStressPoint);

                stressValueText.text = _currentStressValue.ToString();
            }
        }

        private Queue<StressStruct> stressQueues = new Queue<StressStruct>();

        //private const int MaxStressPoint = 100;
        //private const float StressPointDenominator = 10f;


        protected override void Awake()
        {
            base.Awake();

            BattleInfoManager.StressPointUpdatedEvent += UpdateStressQueue;
        }


        private void OnDestroy()
        {
            BattleInfoManager.StressPointUpdatedEvent -= UpdateStressQueue;
        }


        protected override void OnViewReady()
        {
            base.OnViewReady();

            if(stressQueues.Count > 0)
            {
                int endValue = 0;

                for(int i = 0; i < stressQueues.Count; i++)
                {
                    var q = stressQueues.Dequeue();

                    endValue = q.targetStressValue;
                }

                UpdateStressValue(endValue);
            }
        }


        public void InitializePanel()
        {
            currentStressValue = BattleInfoManager.currentStressPoint;

            float stressPercentage = (float)currentStressValue / BattleInfoManager.MaxStressPoint;

            fillIndicatorLerper.StartLerp(stressPercentage, stressPercentage);

            stressQueues = new Queue<StressStruct>();
        }


        public void UpdateStressQueue(int currentStressValue)
        {
            int targetValue = currentStressValue;

            StressStruct stressQueue = new StressStruct(targetValue);
            stressQueues.Enqueue(stressQueue);

            if (viewState == ViewState.Ready)
                UpdateStressValue(stressQueues.Dequeue().targetStressValue);
        }

        private void UpdateStressValue(int value)
        {
            currentStressValue = value;

            float stressPercentage = (float)currentStressValue / BattleInfoManager.MaxStressPoint;

            fillIndicatorLerper.StartLerp(stressPercentage);
        }

    }
}

