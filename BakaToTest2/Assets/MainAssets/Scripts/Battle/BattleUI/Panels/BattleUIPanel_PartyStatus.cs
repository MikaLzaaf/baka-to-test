﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace Intelligensia.Battle
{
    public class BattleUIPanel_PartyStatus : ViewController
    {
        // Initialize setiap satu item
        // - setiap item ada hp, mana, nama (at least)

        [Header("Party")]
        [SerializeField] private BattleUIItem_PartyStatus itemPrefab = default;
        [SerializeField] private Transform container = default;
        [SerializeField] private TextMeshProUGUI argumentSideText = default;
        [SerializeField] private CanvasGroup nextCharacterInputLabel = default;
        [SerializeField] private CanvasGroup prevCharacterInputLabel = default;



        private Dictionary<BattleEntity, BattleUIItem_PartyStatus> listLookup;
        private List<BattleUIItem_PartyStatus> partyList = new List<BattleUIItem_PartyStatus>();

        private BattleUIItem_PartyStatus currentHighlightedPlayer;

        //private BattleUIItem_PartyStatus memberToSwap;


        protected override void Awake()
        {
            base.Awake();

            BattleInfoManager.EntityOnFocusedEvent += OnPlayerHighlighted;
        }

        private void OnDestroy()
        {
            BattleInfoManager.EntityOnFocusedEvent -= OnPlayerHighlighted;
        }

        //public override void Show()
        //{
        //    base.Show();

        //    if(currentHighlightedPlayer != null)
        //    {
        //        currentHighlightedPlayer.ShowCurrentTurnDisplay();
        //    }
        //}

        protected override void OnViewReady()
        {
            base.OnViewReady();

            if (currentHighlightedPlayer != null)
            {
                currentHighlightedPlayer.ShowCurrentTurnDisplay();
            }
        }

        //public void Close(bool isBattleEnded)
        //{
        //    Close();
        //}

        public void OnOneRoundEnded()
        {
            SetArgumentSideLabel();
        }

        private void SetArgumentSideLabel()
        {
            if(argumentSideText != null)
                argumentSideText.text = BattleController.instance.playerArgumentSide.ToString();
        }


        public void GenerateBattlePartyList()
        {
            SetArgumentSideLabel();

            // Clear existing pages
            if (partyList != null && partyList.Count > 0)
            {
                ClearList();
            }
            else
            {
                partyList = new List<BattleUIItem_PartyStatus>();
                listLookup = new Dictionary<BattleEntity, BattleUIItem_PartyStatus>();
            }

            List<PlayerEntity> players = BattleInfoManager.activePlayers;

            for (int i = 0; i < players.Count; i++)
            {
                BattleUIItem_PartyStatus listItem = Instantiate(itemPrefab);
                partyList.Add(listItem);
                listLookup.Add(players[i], listItem);

                listItem.transform.SetParent(container, false);

                listItem.Initialize(players[i]);
            }

            UINavigator.instance.SetupHorizontalNavigation(partyList, true);

            ToggleInputLabels(partyList.Count > 1);

            if (prevCharacterInputLabel != null)
                prevCharacterInputLabel.transform.SetAsFirstSibling();

            if (nextCharacterInputLabel != null)
                nextCharacterInputLabel.transform.SetAsLastSibling();
        }

        public void AddPlayerList(PlayerEntity player)
        {
            BattleUIItem_PartyStatus listItem = Instantiate(itemPrefab);
            partyList.Add(listItem);
            listLookup.Add(player, listItem);

            listItem.transform.SetParent(this.transform, false);

            listItem.Initialize(player);

            listItem.OnDefeatedEvent += RemoveItem;
        }

        private void ClearList()
        {
            for (int i = 0; i < partyList.Count; i++)
            {
                Destroy(partyList[i].gameObject);
            }

            partyList.Clear();
            listLookup.Clear();
        }

        private void RemoveItem(BattleUIItem_PartyStatus listItem)
        {
            listItem.OnDefeatedEvent -= RemoveItem;

            partyList.Remove(listItem);
            listLookup.Remove(listItem.playerEntity);

            Destroy(listItem.gameObject);

            ToggleInputLabels(partyList.Count > 1);
        }

        public void OnPlayerHighlighted(BattleEntity playerEntity)
        {
            if(currentHighlightedPlayer != null)
            {
                currentHighlightedPlayer.ResetDisplay();
                currentHighlightedPlayer = null;
            }

            listLookup.TryGetValue(playerEntity, out currentHighlightedPlayer);
    
            if (currentHighlightedPlayer != null && viewState == ViewState.Ready)
            {
                currentHighlightedPlayer.ShowCurrentTurnDisplay();
            }
        }

        public void ToggleInputLabels(bool active)
        {
            if (prevCharacterInputLabel != null)
                prevCharacterInputLabel.alpha = active ? 1f : 0.5f;

            if(nextCharacterInputLabel != null)
                nextCharacterInputLabel.alpha = active ? 1f : 0.5f;
        }

        //public void OnTurnNumberChanged(BattleEntity currentTurnEntity, BattleEntity nextTurnEntity)
        //{
        //    if (partyList.Count == 0)
        //        return;

        //    if (currentTurnPlayer != null)
        //    {
        //        currentTurnPlayer.ResetDisplay();
        //        currentTurnPlayer = null;
        //    }

        //    listLookup.TryGetValue(currentTurnEntity, out currentTurnPlayer);

        //    if (currentTurnPlayer != null)
        //    {
        //        currentTurnPlayer.ShowCurrentTurnDisplay();
        //    }

        //    listLookup.TryGetValue(nextTurnEntity, out BattleUIItem_PartyStatus nextTurnPlayer);

        //    if (nextTurnPlayer != null)
        //    {
        //        nextTurnPlayer.ShowNextTurnDisplay();
        //    }
        //}

        //public void OnSwappingDone()
        //{
        //    listLookup.TryGetValue(BattleInfoManager.swapOutMember, out memberToSwap);

        //    if (memberToSwap != null)
        //    {
        //        listLookup.Remove(memberToSwap.playerEntity);
        //        listLookup.Add(BattleInfoManager.swapInMember, memberToSwap);

        //        memberToSwap.Initialize(BattleInfoManager.swapInMember);

        //        memberToSwap.ShowCurrentTurnDisplay();
        //    }
        //}
    }
}
