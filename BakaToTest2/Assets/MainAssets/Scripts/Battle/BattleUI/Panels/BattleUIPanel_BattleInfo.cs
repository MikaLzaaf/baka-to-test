﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

namespace Intelligensia.Battle
{
    public class BattleUIPanel_BattleInfo : ViewController
    {
        [Header("Texts")]
        public TextMeshProUGUI dayText;
        public TextMeshProUGUI weatherText;
        //public TextMeshProUGUI subjectNameText;

        [Header("Icons")]
        public Image weatherIcon;
        //public Image subjectIcon;

        [Header("Sub Panels")]
        public BattleUISubPanel_OrderInfo orderInfoPanel;
        public BattleUISubPanel_ActiveSubjectInfo activeSubjectInfoPanel;
        public BattleUIPanel_StressGauge stressGaugePanel;
        //public BattleUISubPanel_ArgumentHelpersDisplay argumentHelpersDisplayPanel;
        //public BattleUISubPanel_SkillLabel attackLabelPanel;
        public BattleUISubPanel_ClassBattleStatus classBattleStatusPanel;

        //[Header("Event Channel")]
        //[SerializeField] private OneBattleRoundEventChannelSO oneBattleRoundEventChannel = default;


        private DayEnum _currentDay;
        public DayEnum currentDay
        {
            get
            {
                return _currentDay;
            }
            set
            {
                _currentDay = value;

                if(dayText != null)
                {
                    dayText.text = _currentDay.ToString();
                }
            }
        }

        private string _weather;
        public string weather
        {
            get
            {
                return _weather;
            }
            set
            {
                _weather = value;

                if (weatherText != null)
                {
                    weatherText.text = _weather;
                }

                if(weatherIcon != null)
                {
                    weatherIcon.sprite = IconLoader.GetWeatherIcon(_weather);
                }
            }
        }

        //private Subjects _activeSubject;
        //public Subjects activeSubject
        //{
        //    get
        //    {
        //        return _activeSubject;
        //    }
        //    set
        //    {
        //        _activeSubject = value;

        //        if(activeSubjectInfoPanel != null)
        //        {
        //            activeSubjectInfoPanel.UpdateSubjectInfo(_activeSubject);
        //        }

        //        //if(subjectNameText != null)
        //        //{
        //        //    subjectNameText.text = _activeSubject.ToString();
        //        //}

        //        //if(subjectIcon != null)
        //        //{
        //        //    subjectIcon.sprite = IconLoader.GetSubjectIcon(_activeSubject);
        //        //}
        //    }
        //}


        protected override void Awake()
        {
            base.Awake();

            BattleInfoManager.ActiveSubjectChangedEvent += ChangeSubject;
        }


        private void OnDestroy()
        {
            BattleInfoManager.ActiveSubjectChangedEvent -= ChangeSubject;
        }

        public override void Show()
        {
            base.Show();

            if(activeSubjectInfoPanel != null)
            {
                activeSubjectInfoPanel.Show();
            }

            if(stressGaugePanel != null)
            {
                stressGaugePanel.Show();
            }

            //if(argumentHelpersDisplayPanel != null)
            //{
            //    argumentHelpersDisplayPanel.Show();
            //}

            if(classBattleStatusPanel != null && BattleController.instance.currentChallengeInfo.isClassBattle)
            {
                classBattleStatusPanel.Show();
            }
        }


        public override void Close()
        {
            base.Close();

            if(activeSubjectInfoPanel != null)
            {
                activeSubjectInfoPanel.Close();
            }

            if(stressGaugePanel != null)
            {
                stressGaugePanel.Close();
            }

            //if (argumentHelpersDisplayPanel != null)
            //{
            //    argumentHelpersDisplayPanel.Close();
            //}

            if (classBattleStatusPanel != null && BattleController.instance.currentChallengeInfo.isClassBattle)
            {
                classBattleStatusPanel.Close();
            }
        }


        public void InitializePanel()
        {
            InitializePanel(BattleInfoManager.currentDay, BattleInfoManager.currentWeather,
                BattleInfoManager.currentActiveSubject);

            //if(oneBattleRoundEventChannel != null)
            //    oneBattleRoundEventChannel.OnFullRoundEvent += OnOneRoundEnded;
        }

        public void InitializePanel(DayEnum day, string weather, Subjects sub)
        {
            currentDay = day;
            //this.weather = weather;
            //activeSubject = sub;
            if (activeSubjectInfoPanel != null)
            {
                activeSubjectInfoPanel.UpdateSubjectInfo(sub);
            }

            Show();

            stressGaugePanel.InitializePanel();

            if (BattleController.instance.currentChallengeInfo.isClassBattle)
                classBattleStatusPanel?.InitializePanel();
        }

        //public void UnloadPanel()
        //{
        //    if (oneBattleRoundEventChannel != null)
        //        oneBattleRoundEventChannel.OnFullRoundEvent -= OnOneRoundEnded;
        //}


        public void ShowOrderInfo(BattleOrder selectedOrder)
        {
            Show();

            if(orderInfoPanel != null)
            {
                orderInfoPanel.Show(selectedOrder);
            }
        }

        //public void ToggleSkillLabel(bool active)
        //{
        //    if (attackLabelPanel == null)
        //        return;

        //    if(active)
        //    {
        //        attackLabelPanel.Show();
        //    }
        //    else
        //    {
        //        attackLabelPanel.Close();
        //    }
        //}

        public void OnOneRoundEnded()
        {
            if (orderInfoPanel != null)
            {
                orderInfoPanel.UpdateDuration();
            }
        }


        public void ChangeSubject(Subjects toSubject)
        {
            if (activeSubjectInfoPanel != null)
            {
                activeSubjectInfoPanel.UpdateSubjectInfo(toSubject);
            }
        }


        public void ChangeWeather(string toWeather)
        {
            weather = toWeather;
        }

        public void ChangeDay(DayEnum day)
        {
            currentDay = day;
        }

        #region Argument Helpers Display

        //public void AddHelper(PlayerEntity helperEntity)
        //{
        //    if (argumentHelpersDisplayPanel != null)
        //    {
        //        argumentHelpersDisplayPanel.AddHelper(helperEntity);
        //    }
        //}

        //public void RemoveHelper(PlayerEntity helperEntity)
        //{
        //    if (argumentHelpersDisplayPanel != null)
        //    {
        //        argumentHelpersDisplayPanel.RemoveHelper(helperEntity);
        //    }
        //}

        //public void InspectHelper(PlayerEntity helperEntity)
        //{
        //    if(argumentHelpersDisplayPanel != null)
        //    {
        //        argumentHelpersDisplayPanel.InspectItem(helperEntity);
        //    }
        //}

        #endregion
    }
}
