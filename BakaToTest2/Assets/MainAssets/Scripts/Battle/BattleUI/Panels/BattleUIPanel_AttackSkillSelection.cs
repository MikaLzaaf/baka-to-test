﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;

namespace Intelligensia.Battle
{
    public class BattleUIPanel_AttackSkillSelection : UIViewController
    {
        [Header("List Items")]
        [SerializeField] private BattleUIItem_SkillSelect listItemPrefab = default;
        [SerializeField] private float listItemsTransitionInterval = 0.05f;
        [SerializeField] private float listItemsTransitionDuration = 0.05f;
        [SerializeField] private float listItemsTransitionSlideOffset = -100;

        [Header("Others")]
        [SerializeField] private BattleUISubPanel_ListItemDescription listItemDescriptionPanel = default;
        [SerializeField] private Image highlightedPlayerIcon = default;
        [SerializeField] private TextMeshProUGUI highlightedPlayerNameText = default;

        public event Action AttackSkillsSelectedEvent;

        private List<BattleUIItem_SkillSelect> listItems;

        private Sequence listItemsTransitionSequence;

        public override void Show()
        {
            base.Show();

            GenerateList();
        }

        protected override void OnViewReady()
        {
            base.OnViewReady();

            //if (listItemDescriptionPanel != null)
            //{
            //    BattleUIManager.instance.OnRightTriggerPressedEvent += OnRightTriggered;
            //    BattleUIManager.instance.OnLeftTriggerPressedEvent += OnLeftTriggered;
            //    //BattleUIManager.instance.OninfoButtonPressedEvent += listItemDescriptionPanel.ToggleHelperInfo;
            //}

            BattleUIManager.instance.OnCancelButtonPressedEvent += OnCancelPressed;

            if (listItems.Count > 0)
            {
                UINavigator.instance.SetDefaultSelectedInRuntime(listItems[0].gameObject);
            }
            else
                UpdateSkillDescription(null);
        }

        public override void Close()
        {
            base.Close();

            //if (listItemDescriptionPanel != null)
            //{
            //    BattleUIManager.instance.OnRightTriggerPressedEvent -= OnRightTriggered;
            //    BattleUIManager.instance.OnLeftTriggerPressedEvent -= OnLeftTriggered;
            //    //BattleUIManager.instance.OninfoButtonPressedEvent -= listItemDescriptionPanel.ToggleHelperInfo;
            //}

            BattleUIManager.instance.OnCancelButtonPressedEvent -= OnCancelPressed;

            //if (listItemDescriptionPanel != null)
            //{
            //    listItemDescriptionPanel.Close();
            //}
        }

        public void GenerateList(bool setDefaultSelected = false)
        {
            ClearList();

            PlayerEntity playerEntity = (PlayerEntity)BattleInfoManager.entityOnFocus;

            UpdatePlayerHighlightedDisplay(playerEntity);

            InBattleSkill[] currentSkillList = playerEntity.GetOffensiveSkillsBySubject(Subjects.BahasaMelayu, true);

            if (listItemsTransitionSequence != null)
            {
                listItemsTransitionSequence.Kill();
            }

            listItemsTransitionSequence = DOTween.Sequence();

            for (int i = 0; i < currentSkillList.Length; i++)
            {
                BattleUIItem_SkillSelect listItem = Instantiate(listItemPrefab);
                listItems.Add(listItem);

                listItem.transform.SetParent(content, false);
            
                listItem.Initialize(currentSkillList[i], this);

                listItem.onClick.AddListener(delegate
                {
                    OnListItemClick(listItem);
                });

                var item = listItem;

                float targetAlphaValue = item.canvasGroup.alpha;

                item.canvasGroup.alpha = 0;

                listItemsTransitionSequence.Insert(i * listItemsTransitionInterval, item.canvasGroup.DOFade(targetAlphaValue, listItemsTransitionDuration));
                listItemsTransitionSequence.Insert(i * listItemsTransitionInterval, 
                    item.transform.DOLocalMoveX(item.transform.localPosition.x + listItemsTransitionSlideOffset, listItemsTransitionDuration).From());
            }

            listItemsTransitionSequence.OnComplete(delegate
            {
                UINavigator.instance.SetupVerticalNavigation(listItems, true);

                if(setDefaultSelected)
                    UINavigator.instance.SetDefaultSelectedInRuntime(listItems[0].gameObject);
            });
        }

        private void ClearList()
        {
            if (listItems != null && listItems.Count > 0)
            {
                if (listItems.Count > 0)
                {
                    for (int i = listItems.Count - 1; i >= 0; i--)
                    {
                        Destroy(listItems[i].gameObject);
                    }
                }

                listItems.Clear();
            }
            else
            {
                listItems = new List<BattleUIItem_SkillSelect>();
            }
        }

        private void OnListItemClick(BattleUIItem_SkillSelect listItem)
        {
            //BattleInfoManager.currentAim = listItem.skill.GetSkillAim();
            BattleInfoManager.selectedSkill = listItem.skill;

            if (BattleInfoManager.HasSameAction(listItem.skill.GetSkillName(), true, out int sameIndex))
            { 
                BattleInfoManager.RemoveActionSelected(sameIndex);

                ResetListDisplay();
            }
            else
            {
                if (!listItem.isSelectable)
                    return;

                BattleInfoManager.CreateNewAction(BattleSelectionType.Attack, listItem.skill, true);

                AttackSkillsSelectedEvent?.Invoke();
            }
        }

        private void ResetListDisplay()
        {
            for(int i = 0; i < listItems.Count; i++)
            {
                listItems[i].ResetDisplay();
            }
        }

        public void UpdateSkillDescription(InBattleSkill skillSelected)
        {
            if(listItemDescriptionPanel != null)
                listItemDescriptionPanel.ShowDescription(skillSelected);
        }

        private void UpdatePlayerHighlightedDisplay(PlayerEntity playerEntity)
        {
            highlightedPlayerIcon.sprite = PlayerInfoManager.LoadCharacterIcon(playerEntity.characterId);
            highlightedPlayerNameText.text = playerEntity.characterName;
        }

        private void OnCancelPressed()
        {
            if(canProcessInput)
                UINavigator.instance.OpenPreviousWindow();
        }
    }
}
