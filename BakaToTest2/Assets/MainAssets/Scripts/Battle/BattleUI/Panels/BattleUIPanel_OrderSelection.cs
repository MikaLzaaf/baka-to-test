﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;


namespace Intelligensia.Battle
{
    public class BattleUIPanel_OrderSelection : UIViewController
    {
        [Header("List Items")]
        [SerializeField] private BattleUIItem_OrderSelect listItemPrefab = default;
        [SerializeField] private float listItemsTransitionInterval = 0.05f;
        [SerializeField] private float listItemsTransitionDuration = 0.05f;
        [SerializeField] private float listItemsTransitionSlideOffset = -100;

        [Header("Others")]
        [SerializeField] private BattleUISubPanel_ListItemDescription listItemDescriptionPanel = default;
        [SerializeField] private Image highlightedPlayerIcon = default;
        [SerializeField] private TextMeshProUGUI highlightedPlayerNameText = default;

        public event Action<BattleOrder> OrderSelectedEvent;

        private List<BattleUIItem_OrderSelect> listItems;
        private bool isOrderSelected = false;
        //private bool isGenerated = false;
        private Sequence listItemsTransitionSequence;

        public override void Show()
        {
            base.Show();

            UpdateOrderDescription(null);

            GenerateList();

            isOrderSelected = false;
        }

        protected override void OnViewReady()
        {
            base.OnViewReady();

            BattleUIManager.instance.OnCancelButtonPressedEvent += OnCancelPressed;

            if (listItems.Count > 0)
            {
                UINavigator.instance.SetDefaultSelectedInRuntime(listItems[0].gameObject);
            }
            else
                UpdateOrderDescription(null);
        }


        public override void Close()
        {
            base.Close();

            //if (tabController != null)
            //{
            //    BattleUIManager.instance.OnRightTabPressedEvent -= NextList;
            //    BattleUIManager.instance.OnLeftTabPressedEvent -= PreviousList;
            //}

            BattleUIManager.instance.OnCancelButtonPressedEvent -= OnCancelPressed;

            //if (listItemDescriptionPanel != null)
            //{
            //    listItemDescriptionPanel.Close();
            //}

            BattlePhasesController.instance.animatorController.
                SetFreeExecutionIndex(isOrderSelected ? 3 : 0);
        }

        private void OnCancelPressed()
        {
            if(canProcessInput)
                UINavigator.instance.OpenPreviousWindow();
        }

        //private void NextList()
        //{
        //    if (!canProcessInput)
        //        return;

        //    //if (tabController != null)
        //    //{
        //    //    tabController.SwitchTab(1);
        //    //}

        //    ClearList();
        //    GenerateList();
        //}

        //private void PreviousList()
        //{
        //    if (!canProcessInput)
        //        return;

        //    //if (tabController != null)
        //    //{
        //    //    tabController.SwitchTab(-1);
        //    //}

        //    ClearList();
        //    GenerateList();
        //}


        public void GenerateList()
        {
            ClearList();

            UpdatePlayerHighlightedDisplay((PlayerEntity)BattleInfoManager.entityOnFocus);

            BattleOrder[] currentOrders = BattleOrdersManager.instance.battleOrdersLearned.ToArray();

            if (listItemsTransitionSequence != null)
                listItemsTransitionSequence.Kill();

            listItemsTransitionSequence = DOTween.Sequence();

            for (int i = 0; i < currentOrders.Length; i++)
            {
                BattleUIItem_OrderSelect listItem = Instantiate(listItemPrefab);
                listItems.Add(listItem);

                listItem.transform.SetParent(content, false);

                listItem.Initialize(currentOrders[i], this);

                listItem.onClick.AddListener(delegate
                {
                    OnListItemClicked(listItem);
                });

                var item = listItem;

                item.canvasGroup.alpha = 0;

                listItemsTransitionSequence.Insert(i * listItemsTransitionInterval, item.canvasGroup.DOFade(1, listItemsTransitionDuration));
                listItemsTransitionSequence.Insert(i * listItemsTransitionInterval, item.transform.DOLocalMoveX(item.transform.localPosition.x + listItemsTransitionSlideOffset, listItemsTransitionDuration).From());
            }

            listItemsTransitionSequence.OnComplete(delegate
            {
                UINavigator.instance.SetupVerticalNavigation(listItems, true);
            });
        }

        private void ClearList()
        {
            if (listItems != null && listItems.Count > 0)
            {
                if (listItems.Count > 0)
                {
                    for (int i = listItems.Count - 1; i >= 0; i--)
                    {
                        Destroy(listItems[i].gameObject);
                    }
                }

                listItems.Clear();
            }
            else
            {
                listItems = new List<BattleUIItem_OrderSelect>();
            }
        }


        private void OnListItemClicked(BattleUIItem_OrderSelect listItem)
        {
            if (!listItem.isSelectable)
                return;

            isOrderSelected = true;

            BattleInfoManager.selectedOrder = listItem.battleOrder;

            OrderSelectedEvent?.Invoke(listItem.battleOrder);

            UINavigator.instance.ClearPreviousWindows();

            UINavigator.instance.OpenPreviousWindow();
        }

        public void UpdateOrderDescription(BattleOrder selectedOrder)
        {
            if(listItemDescriptionPanel != null)
                listItemDescriptionPanel.ShowDescription(selectedOrder);
        }

        private void UpdatePlayerHighlightedDisplay(PlayerEntity playerEntity)
        {
            highlightedPlayerIcon.sprite = PlayerInfoManager.LoadCharacterIcon(playerEntity.characterId);
            highlightedPlayerNameText.text = playerEntity.characterName;
        }
    }
}
