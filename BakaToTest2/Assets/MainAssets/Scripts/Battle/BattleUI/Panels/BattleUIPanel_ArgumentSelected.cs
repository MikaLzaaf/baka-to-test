using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using DG.Tweening;
using TMPro;
using UnityEngine.Events;

namespace Intelligensia.Battle
{
    public class BattleUIPanel_ArgumentSelected : ViewController
    {
        [Header("Prefab")]
        [SerializeField] private BattleUIItem_ArgumentSelected listItemPrefab = default;

        [Header("Others")]
        [SerializeField] private Transform container = default;
        [SerializeField] private TextMeshProUGUI argumentCountText = default;
        //[SerializeField] private float listItemsTransitionDuration = 0.05f;
        //[SerializeField] private float listItemsTransitionSlideOffset = -100;

        public UnityEvent FullArgumentsEvent;

        private List<BattleUIItem_ArgumentSelected> listItems;

        //private Sequence listItemsTransitionSequence;

        private const string ArgumentCountFormat = "{0} / {1}";


        protected override void Awake()
        {
            base.Awake();

            BattleInfoManager.BattleArgumentsAddedEvent += AddToList;
            BattleInfoManager.BattleArgumentsRemovedEvent += RemoveFromList;

            ClearList();
        }

        private void OnDestroy()
        {
            BattleInfoManager.BattleArgumentsAddedEvent -= AddToList;
            BattleInfoManager.BattleArgumentsRemovedEvent -= RemoveFromList;
        }

        protected override void OnViewReady()
        {
            base.OnViewReady();

            if (BattleInfoManager.HasFullArguments())
                StartCoroutine(WaitUntilCurrentWindowIsReady());
        }

        public void ClearList()
        {
            if (listItems != null && listItems.Count > 0)
            {
                if (listItems.Count > 0)
                {
                    for (int i = listItems.Count - 1; i >= 0; i--)
                    {
                        Destroy(listItems[i].gameObject);
                    }
                }

                listItems.Clear();
            }
            else
            {
                listItems = new List<BattleUIItem_ArgumentSelected>();
            }

            UpdateArgumentCount();
        }

        private void AddToList(BattleAction battleAction)
        {
            //if (listItemsTransitionSequence != null)
            //    listItemsTransitionSequence.Kill();

            //listItemsTransitionSequence = DOTween.Sequence();

            BattleUIItem_ArgumentSelected listItem = Instantiate(listItemPrefab);
            listItems.Add(listItem);

            listItem.transform.SetParent(container, false);

            listItem.InitializeItem(battleAction);

            UpdateArgumentCount();

        }

        private void RemoveFromList(BattleAction battleAction)
        {
            //if (listItemsTransitionSequence != null)
            //    listItemsTransitionSequence.Kill();

            //listItemsTransitionSequence = DOTween.Sequence();

            var item = GetItem(battleAction.actionName);

            if (item == null)
                return;

            listItems.Remove(item);
            Destroy(item.gameObject);

            UpdateArgumentCount();
        }

        private BattleUIItem_ArgumentSelected GetItem(string actionName)
        {
            for(int i = 0; i < listItems.Count; i++)
            {
                if (listItems[i].battleAction.actionName == actionName)
                    return listItems[i];
            }

            return null;
        }

        private void UpdateArgumentCount()
        {
            if (argumentCountText != null)
                argumentCountText.text = string.Format(ArgumentCountFormat, listItems.Count, BattleController.MaxArgumentAmount);
        }

        private IEnumerator WaitUntilCurrentWindowIsReady()
        {
            yield return new WaitUntil(() => UINavigator.instance.isWindowOpening);

            yield return new WaitForSeconds(0.5f);
            FullArgumentsEvent?.Invoke();
        }
    }
}

