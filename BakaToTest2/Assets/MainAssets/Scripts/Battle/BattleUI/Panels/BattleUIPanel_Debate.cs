using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;
using UnityEngine.UI;
using System;

namespace Intelligensia.Battle
{
    public class BattleUIPanel_Debate : UIViewController
    {
        public event Action<int> OnDebateEndedEvent;
        public event Action<BattleEntity> OnDebateArgumentSelectedEvent;

        [Header("Top")]
        [SerializeField] private TextMeshProUGUI playerArgumentSideText = default;
        [SerializeField] private TextMeshProUGUI enemyArgumentSideText = default;

        [Header("List Items")]
        [SerializeField] private BattleUIItem_Debate playerPrefab = default;
        [SerializeField] private BattleUIItem_Debate enemyPrefab = default;
        [SerializeField] private float listItemsTransitionDuration = 0.05f;
        [SerializeField] private float listItemsTransitionSlideOffset = -100;

        [Header("Thinking Display")]
        [SerializeField] private GameObject startSign = default;
        [SerializeField] private Transform thinkingSignTransform = default;
        [SerializeField] private float minThinkingDuration = 2f;
        [SerializeField] private float maxThinkingDuration = 5f;

        [Header("Others")]
        [SerializeField] private BattleUISubPanel_DebateArgumentSelection debateArgumentSelectionPanel = default;
        [SerializeField] private ViewController debateStartView = default;
        [SerializeField] private ScrollRect scrollRect = default;
        [SerializeField] private GameObject helpArgumentPrefab = default;
        [SerializeField] private BattleUISubPanel_DebateWinner debateWinnerView = default;
        [SerializeField] private BattleUISubPanel_DebateBottom bottomPanel = default;
        [SerializeField] private float delayAfterDebateEnds = 2f;
        [SerializeField] private int topicSubjectMatchedEffect = 1;


        private List<BattleUIItem_Debate> listItems;
        private List<BattleAction> enemyArgumentActions;
        private List<GameObject> helpArguments;

        private Sequence listItemsTransitionSequence;

        private int debateTurn = 0;
        private int playerTurnPassed = 0;
        private int enemyTurnPassed = 0;

        private int playerArgumentsAmount;
        private int enemyArgumentsAmount;

        private int _playerDebatePointModifier;
        private int playerDebatePointModifier
        {
            get => _playerDebatePointModifier;
            set
            {
                _playerDebatePointModifier = value;
                bottomPanel?.SetDPModifier(true, _playerDebatePointModifier);
            }
        }

        private int _enemyDebatePointModifier;
        private int enemyDebatePointModifier
        {
            get => _enemyDebatePointModifier;
            set
            {
                _enemyDebatePointModifier = value;
                bottomPanel?.SetDPModifier(false, _enemyDebatePointModifier);
            }
        }

        private int _playerTopicSubjectPointModifier;
        private int playerTopicSubjectPointModifier
        {
            get => _playerTopicSubjectPointModifier;
            set
            {
                _playerTopicSubjectPointModifier = value;

                bottomPanel?.SetTopicSubjectModifier(true, _playerTopicSubjectPointModifier);
            }
        }

        private int _enemyTopicSubjectPointModifier;
        private int enemyTopicSubjectPointModifier
        {
            get => _enemyTopicSubjectPointModifier;
            set
            {
                _enemyTopicSubjectPointModifier = value;

                bottomPanel?.SetTopicSubjectModifier(false, _enemyTopicSubjectPointModifier);
            }
        }

        private int _playerPoint;
        private int playerPoint
        {
            get => _playerPoint;
            set
            {
                _playerPoint = value;

                bottomPanel?.SetDebatePoint(true, _playerPoint);
            }
        }

        private int _enemyPoint;
        private int enemyPoint
        {
            get => _enemyPoint;
            set
            {
                _enemyPoint = value;

                bottomPanel?.SetDebatePoint(false, _enemyPoint);
            }
        }


        private const string ArgumentSideFormat = "{0} : {1}";
        private const string OpponentSidePrefix = "Opponent";
        private const string PlayerSidePrefix = "Player";

        private const string DebateHelperTextFormat = "<color=#FCA311>{0}</color> interjects with the <color=#EB3B5A>{1}</color> argument.";

        public override void Show()
        {
            debateWinnerView?.CloseImmediately();

            base.Show();

            ResetPanel();

            ClearList();

            enemyArgumentActions = new List<BattleAction>(BattleInfoManager.enemyArgumentActionsRegistered);

            playerArgumentsAmount = BattleInfoManager.argumentActionsRegistered.Count;
            enemyArgumentsAmount = enemyArgumentActions.Count;

            ClearHelpArguments();
        }

        protected override void OnViewReady()
        {
            base.OnViewReady();

            bottomPanel?.TogglePlayerArgumentIcons(true, playerArgumentsAmount);
            bottomPanel?.ToggleEnemyArgumentIcons(true, enemyArgumentsAmount);

            StartCoroutine(BeginDebate());
        }

        private IEnumerator BeginDebate()
        {
            if (debateStartView == null)
                yield return null;
            else
            {
                debateStartView.Show();

                yield return new WaitUntil(() => debateStartView.viewState == ViewState.Closed);
            }

            debateTurn = 0;
            playerTurnPassed = 0;
            enemyTurnPassed = 0;

            playerDebatePointModifier = 0;
            playerTopicSubjectPointModifier = 0;
            enemyDebatePointModifier = 0;
            enemyTopicSubjectPointModifier = 0;

            if(enemyArgumentsAmount == 0 && playerArgumentsAmount > 0)
            {
                yield return StartCoroutine(ShowWinnerView(1, 0));
                Close();
            }
            else if(enemyArgumentsAmount > 0 && playerArgumentsAmount == 0)
            {
                yield return StartCoroutine(ShowWinnerView(2, 0));
                Close();
            }
            else
            {
                ToggleStartSign(true);
                BeginSelectingArgument();
            }
        }

        private void BeginSelectingArgument()
        {
            if (((int)BattleController.instance.playerArgumentSide == (debateTurn % 2) || enemyTurnPassed >= enemyArgumentsAmount) && 
                playerTurnPassed < playerArgumentsAmount)
            {
                debateArgumentSelectionPanel?.Show();
            }
            else if(((int)BattleController.instance.enemyArgumentSide == (debateTurn % 2) || playerTurnPassed >= playerArgumentsAmount) && 
                enemyTurnPassed < enemyArgumentsAmount)
            {
                StartCoroutine(EnemyArgumentSelection());
            }

            int nextArgumentType = 2;

            if (listItems.Count == 0)
                nextArgumentType = 0;
            else if (listItems.Count == (playerArgumentsAmount + enemyArgumentsAmount - 1))
                nextArgumentType = 1;

            ToggleThinkingSign(true, nextArgumentType);

            StartCoroutine(ApplyScrollPosition(scrollRect, 0f));
        }

        private IEnumerator EnemyArgumentSelection()
        {
            float randomDuration = UnityEngine.Random.Range(minThinkingDuration, maxThinkingDuration);
            yield return new WaitForSeconds(randomDuration);

            // TODO : Craft enemy thinking. For now select randomly
            int randomIndex = UnityEngine.Random.Range(0, enemyArgumentActions.Count);

            BattleAction actionSelected = enemyArgumentActions[randomIndex];
            enemyArgumentActions.RemoveAt(randomIndex);

            OnArgumentSelected(actionSelected);
        }

        public void ClearList()
        {
            if (listItems != null && listItems.Count > 0)
            {
                if (listItems.Count > 0)
                {
                    for (int i = listItems.Count - 1; i >= 0; i--)
                    {
                        Destroy(listItems[i].gameObject);
                    }
                }

                listItems.Clear();
            }
            else
            {
                listItems = new List<BattleUIItem_Debate>();
            }
        }

        public void AddToList(BattleAction battleAction)
        {
            battleAction.skill.ApplyCooldown();

            var prefab = battleAction.isPlayerAction ? playerPrefab : enemyPrefab;

            BattleUIItem_Debate listItem = Instantiate(prefab);
            listItems.Add(listItem);

            listItem.transform.SetParent(content, false);

            int argumentType = 0;

            if (listItems.Count == 1)
                argumentType = 1;
            else if (listItems.Count == (playerArgumentsAmount + enemyArgumentsAmount))
                argumentType = 2;

            listItem.InitializeItem(battleAction.user, battleAction.actionName, battleAction.skill.GetSkillPoint(), argumentType);

            var item = listItem;

            item.canvasGroup.alpha = 0;

            float endValue = battleAction.isPlayerAction ? item.transform.localPosition.x + listItemsTransitionSlideOffset : 
                item.transform.localPosition.x - listItemsTransitionSlideOffset;

            if (listItemsTransitionSequence != null)
                listItemsTransitionSequence.Kill();

            listItemsTransitionSequence = DOTween.Sequence();

            listItemsTransitionSequence.Insert(0, item.canvasGroup.DOFade(1, listItemsTransitionDuration));
            listItemsTransitionSequence.Insert(0, item.transform.DOLocalMoveX(endValue, listItemsTransitionDuration).From());

            listItemsTransitionSequence.OnComplete(delegate
            {
                if(battleAction.skill.isEnhancedArgument)
                {
                    ApplyDebateEffect(battleAction, out bool canActivateEffect);

                    if (canActivateEffect)
                        item.ActivateEffect(delegate { ProcessArgument(battleAction); },
                        delegate { ProcessHelper(battleAction); });
                    else
                        ProcessArgument(battleAction, true);
                }
                else
                {
                    ProcessArgument(battleAction);
                }
            });

            StartCoroutine(ApplyScrollPosition(scrollRect, 0f));
        }

        private IEnumerator ApplyScrollPosition(ScrollRect sr, float verticalPos)
        {
            yield return new WaitForEndOfFrame();
            sr.verticalNormalizedPosition = verticalPos;
            LayoutRebuilder.ForceRebuildLayoutImmediate((RectTransform)sr.transform);
            yield return new WaitForEndOfFrame();
        }

        private void ResetPanel()
        {
            playerPoint = 0;
            bottomPanel?.ShowPointUpdateAnimation(true);

            enemyPoint = 0;
            bottomPanel?.ShowPointUpdateAnimation(false);

            ToggleStartSign(false);

            bottomPanel?.TogglePlayerArgumentIcons(false, 4);
            bottomPanel?.ToggleEnemyArgumentIcons(false, 4);

            bottomPanel?.SetTopicSubject();

            if (playerArgumentSideText != null)
                playerArgumentSideText.text = string.Format(ArgumentSideFormat, PlayerSidePrefix, BattleController.instance.playerArgumentSide.ToString());

            if (enemyArgumentSideText != null)
                enemyArgumentSideText.text = string.Format(ArgumentSideFormat, OpponentSidePrefix, BattleController.instance.enemyArgumentSide.ToString());

            if (debateArgumentSelectionPanel != null)
                debateArgumentSelectionPanel.CloseImmediately();
        }

        private void ToggleStartSign(bool active)
        {
            if (startSign != null)
                startSign.SetActive(active);
        }

        private void ToggleThinkingSign(bool active, int nextArgumentType = 2)
        {
            if (thinkingSignTransform == null)
                return;
            // nextArgumentType : 0 = starting argument, 1 = closing, 2 = normal
            if(active)
            {
                if(nextArgumentType != 2)
                    thinkingSignTransform.GetChild(nextArgumentType).gameObject.SetActive(true);

                thinkingSignTransform.SetAsLastSibling();
                thinkingSignTransform.gameObject.SetActive(true);
            }
            else
            {
                thinkingSignTransform.gameObject.SetActive(false);

                thinkingSignTransform.GetChild(0).gameObject.SetActive(false);
                thinkingSignTransform.GetChild(1).gameObject.SetActive(false);
            }
        }

        // Add on subPanel_debateArgumentSelection ListItemClickedEvent
        public void OnArgumentSelected(BattleAction action)
        {
            ToggleThinkingSign(false);

            AddToList(action);

            OnDebateArgumentSelectedEvent?.Invoke(action.user);
        }

        private void ProcessArgument(BattleAction battleAction, bool processHelperManually = false)
        {
            if (battleAction.isPlayerAction)
            {
                playerPoint += battleAction.skill.GetSkillPoint();

                bottomPanel?.ShowPointUpdateAnimation(true);
                bottomPanel?.CloseOnePlayerArgumentIcon();

                playerTurnPassed += 1;
            }
            else
            {
                enemyPoint += battleAction.skill.GetSkillPoint();

                bottomPanel?.ShowPointUpdateAnimation(false);
                bottomPanel?.CloseOneEnemyArgumentIcon();

                enemyTurnPassed += 1;
            }

            debateTurn += 1;

            if (!battleAction.skill.isEnhancedArgument || processHelperManually)
                ProcessHelper(battleAction);
        }

        private void ApplyDebateEffect(BattleAction battleAction, out bool canActivateEffect)
        {
            canActivateEffect = false;

            switch (battleAction.skill.GetDebateEffect())
            {
                case DebateEffectType.IncreaseAllyArgument:
                    EntityTag allyTag = battleAction.isPlayerAction ? EntityTag.Player : EntityTag.Enemy;

                    var lastListItem = GetLastListItem(allyTag);

                    if (lastListItem == null)
                        return;

                    lastListItem.ChangePoint(battleAction.skill.GetDebateEffectValue());

                    if (battleAction.isPlayerAction)
                        playerPoint += battleAction.skill.GetDebateEffectValue();
                    else
                        enemyPoint += battleAction.skill.GetDebateEffectValue();

                    canActivateEffect = true;
                    break;

                case DebateEffectType.DecreaseOpponentArgument:
                    EntityTag opponentTag = battleAction.isPlayerAction ? EntityTag.Enemy : EntityTag.Player;

                    var opponentLastListItem = GetLastListItem(opponentTag);

                    if (opponentLastListItem == null)
                        return;

                    opponentLastListItem.ChangePoint(battleAction.skill.GetDebateEffectValue());

                    if (battleAction.isPlayerAction)
                    {
                        enemyPoint += battleAction.skill.GetDebateEffectValue();
                        bottomPanel?.ShowPointUpdateAnimation(false);
                    }
                    else
                    {
                        playerPoint += battleAction.skill.GetDebateEffectValue();
                        bottomPanel?.ShowPointUpdateAnimation(true);
                    }

                    canActivateEffect = true;
                    break;

                case DebateEffectType.IncreaseTopicSubjectEffect:
                    if (battleAction.isPlayerAction)
                        playerTopicSubjectPointModifier += battleAction.skill.GetDebateEffectValue();
                    else
                        enemyTopicSubjectPointModifier += battleAction.skill.GetDebateEffectValue();

                    canActivateEffect = true;
                    break;

                case DebateEffectType.RemovePreviousOpponentArgument:
                    opponentTag = battleAction.isPlayerAction ? EntityTag.Enemy : EntityTag.Player;

                    for (int i = listItems.Count - 1; i >= 0; i--)
                    {
                        if (listItems[i].character.entityTag == opponentTag)
                        {
                            if (opponentTag == EntityTag.Enemy)
                            {
                                enemyPoint -= listItems[i].argumentPoint;
                                bottomPanel?.ShowPointUpdateAnimation(false);
                            }
                            else
                            {
                                playerPoint -= listItems[i].argumentPoint;
                                bottomPanel?.ShowPointUpdateAnimation(true);
                            }

                            listItems.RemoveAt(i);
                            canActivateEffect = true;
                            break;
                        }    
                    }

                    break;

                case DebateEffectType.IncreaseAllyArgumentFromStart:

                    if(listItems.Count == 1) // Only when starting
                    {
                        if (battleAction.isPlayerAction)
                            playerDebatePointModifier += battleAction.skill.GetDebateEffectValue();
                        else
                            enemyDebatePointModifier += battleAction.skill.GetDebateEffectValue();

                        canActivateEffect = true;
                    }

                    break;

                case DebateEffectType.DecreaseOpponentArgumentFromStart:

                    if (listItems.Count == 1) // Only when starting
                    {
                        if (battleAction.isPlayerAction)
                            enemyDebatePointModifier += battleAction.skill.GetDebateEffectValue();
                        else
                            playerDebatePointModifier += battleAction.skill.GetDebateEffectValue();

                        canActivateEffect = true;
                    }
                        
                    break;

                case DebateEffectType.IncreaseAllyArgumentUponClosing:
                    // Only activate on the last argument in debate
                    if(listItems.Count == (playerArgumentsAmount + enemyArgumentsAmount))
                    {
                        allyTag = battleAction.isPlayerAction ? EntityTag.Player : EntityTag.Enemy;

                        int totalPoint = 0;
                        for (int i = 0; i < listItems.Count - 1; i++)
                        {
                            if (listItems[i].character.entityTag == allyTag)
                            {
                                listItems[i].ChangePoint(battleAction.skill.GetDebateEffectValue());
                                totalPoint += battleAction.skill.GetDebateEffectValue();
                            }
                        }

                        if (totalPoint == 0)
                            return;

                        if (allyTag == EntityTag.Player)
                            playerPoint += totalPoint;
                        else
                            enemyPoint += totalPoint;

                        canActivateEffect = true;
                    }

                    break;

                case DebateEffectType.DecreaseOpponentArgumentUponClosing:
                    // Only activate on the last argument in debate
                    if (listItems.Count == (playerArgumentsAmount + enemyArgumentsAmount))
                    {
                        opponentTag = battleAction.isPlayerAction ? EntityTag.Enemy : EntityTag.Player;

                        int totalPoint = 0;
                        for (int i = 0; i < listItems.Count - 1; i++)
                        {
                            if (listItems[i].character.entityTag == opponentTag)
                            {
                                listItems[i].ChangePoint(battleAction.skill.GetDebateEffectValue());
                                totalPoint += battleAction.skill.GetDebateEffectValue();
                            }
                        }

                        if (totalPoint == 0)
                            return;

                        if (opponentTag == EntityTag.Enemy)
                        {
                            enemyPoint += totalPoint;
                            bottomPanel?.ShowPointUpdateAnimation(false);
                        }
                        else
                        {
                            playerPoint += totalPoint;
                            bottomPanel?.ShowPointUpdateAnimation(true);
                        }

                        canActivateEffect = true;
                    }
                    
                    break;

                default:
                    break;
            }
        }

        private void ProcessHelper(BattleAction battleAction)
        {
            StartCoroutine(WaitForAnyHelper(battleAction.skill.GetSubjectBased(), battleAction.isPlayerAction, battleAction.user));
        }

        private IEnumerator WaitForAnyHelper(Subjects subjectBased, bool isPlayerArgument, BattleEntity user)
        {
            int pointModifier = isPlayerArgument ? playerDebatePointModifier : enemyDebatePointModifier;
            // Process if last list item subject matches current topic subject
            if (subjectBased == BattleInfoManager.currentActiveSubject)
            {
                yield return new WaitForSeconds(0.5f); // For now just delay for 1s

                int pointsGained = topicSubjectMatchedEffect;
                pointsGained += isPlayerArgument ? playerTopicSubjectPointModifier : enemyTopicSubjectPointModifier;
                pointsGained += pointModifier;

                if (isPlayerArgument)
                {
                    var lastListItem = listItems[listItems.Count - 1]; // Current argument
                    lastListItem.ChangePoint(pointsGained);

                    playerPoint += pointsGained;
                }
                else
                {
                    var lastListItem = listItems[listItems.Count - 1];// Current argument
                    lastListItem.ChangePoint(pointsGained);

                    enemyPoint += pointsGained;
                }

                if (pointModifier > 0)
                    bottomPanel?.MoveDPModifierContainer(isPlayerArgument);

                bottomPanel?.MoveTopicSubjectContainer(isPlayerArgument);
            }
            else if(pointModifier != 0 && listItems.Count != 1)
            {
                yield return new WaitForSeconds(0.5f); // For now just delay for 1s

                if (pointModifier > 0)
                    bottomPanel?.MoveDPModifierContainer(isPlayerArgument);

                if (isPlayerArgument)
                {
                    var lastListItem = listItems[listItems.Count - 1]; // Current argument
                    lastListItem.ChangePoint(pointModifier);

                    playerPoint += pointModifier;
                }
                else
                {
                    var lastListItem = listItems[listItems.Count - 1];// Current argument
                    lastListItem.ChangePoint(pointModifier);

                    enemyPoint += pointModifier;
                }
            }

            yield return new WaitForSeconds(0.5f); // For now just delay for 1s

            // Process which helper will help
            for (int i = 0; i < BattleInfoManager.activePlayers.Count; i++)
            {
                if (user.characterName == BattleInfoManager.activePlayers[i].characterName)
                    continue;

                bool hasHelped = false;

                for (int j = 0; j < helpArguments.Count; j++)
                {
                    if (BattleInfoManager.activePlayers[i].characterName == helpArguments[j].name)
                    {
                        hasHelped = true;
                        break;
                    }
                }
    
                if (hasHelped || !BattleInfoManager.activePlayers[i].GetDebateHelper().CanUseSupportOnArgument(isPlayerArgument))
                    continue;

                if (UnityEngine.Random.value <= BattleInfoManager.activePlayers[i].GetRelationshipLevelProbability()) // For testing only
                {
                    ApplyHelper(BattleInfoManager.activePlayers[i]);

                    yield return StartCoroutine(ApplyScrollPosition(scrollRect, 0f));

                    BattleUIManager.instance.helperCharacterView.Show(BattleInfoManager.activePlayers[i], ActorExpression.HoldOutHand);
                    yield return new WaitUntil(() => BattleUIManager.instance.helperCharacterView.viewState == ViewState.Closed);

                    break;
                }
            }

            yield return new WaitForSeconds(0.5f); // For now just delay for 1s

            if (debateTurn < playerArgumentsAmount + enemyArgumentsAmount)
                BeginSelectingArgument();
            else
            {
                int result = 0;
                int winnerPoint = 0;

                if (playerPoint > enemyPoint)
                {
                    result = 1;
                    winnerPoint = playerPoint;
                }
                else if (enemyPoint > playerPoint)
                {
                    result = 2;
                    winnerPoint = enemyPoint;
                }

                yield return StartCoroutine(ShowWinnerView(result, winnerPoint));

                Close();
            }
        }

        private IEnumerator ShowWinnerView(int result, int winnerPoint)
        {
            debateWinnerView.Show(result, winnerPoint);

            yield return new WaitUntil(() => debateWinnerView.viewState == ViewState.Ready);
            yield return new WaitForSeconds(delayAfterDebateEnds);

            OnDebateEndedEvent?.Invoke(result);
        }

        private void ClearHelpArguments()
        {
            if (helpArguments != null && helpArguments.Count > 0)
            {
                if (helpArguments.Count > 0)
                {
                    for (int i = helpArguments.Count - 1; i >= 0; i--)
                    {
                        Destroy(helpArguments[i].gameObject);
                    }
                }

                helpArguments.Clear();
            }
            else
                helpArguments = new List<GameObject>();
        }

        private BattleUIItem_Debate GetLastListItem(EntityTag targetTag)
        {
            for(int i = listItems.Count - 1; i >= 0; i--)
            {
                if (listItems[i].character.entityTag == targetTag && i != listItems.Count - 1) // Only need the one before the last one
                    return listItems[i];
            }

            return null;
        }

        private void ApplyHelper(PlayerEntity helperEntity)
        {
            DebateHelper debateHelper = helperEntity.GetDebateHelper();
            // TODO : Check if the argument has any effect on other argument or if there is a helper decided to help
            GameObject helper = Instantiate(helpArgumentPrefab, content, false);
            helper.name = helperEntity.characterName;
            helper.GetComponent<TextMeshProUGUI>().text = string.Format(DebateHelperTextFormat, helper.name, debateHelper.ArgumentName);

            helpArguments.Add(helper);

            if (listItemsTransitionSequence != null)
                listItemsTransitionSequence.Kill();

            listItemsTransitionSequence = DOTween.Sequence();

            listItemsTransitionSequence.Insert(0, helper.transform.DOLocalMoveX(helper.transform.localPosition.x + listItemsTransitionSlideOffset, listItemsTransitionDuration).From());
            listItemsTransitionSequence.OnComplete(delegate
            {
                var lastListItem = listItems[listItems.Count - 1];

                if(debateHelper.debateSupportType == DebateHelper.DebateSupportType.DeductOpponentArgument)
                {
                    lastListItem.ChangePoint(debateHelper.DebateSupportValue);

                    enemyPoint += debateHelper.DebateSupportValue;
                    bottomPanel?.ShowPointUpdateAnimation(false);
                }
                else if (debateHelper.debateSupportType == DebateHelper.DebateSupportType.AddAllyArgument)
                {
                    lastListItem.ChangePoint(debateHelper.DebateSupportValue);

                    playerPoint += debateHelper.DebateSupportValue;
                    bottomPanel?.ShowPointUpdateAnimation(true);
                }
                else if (debateHelper.debateSupportType == DebateHelper.DebateSupportType.VoidOpponentArgument)
                {
                    lastListItem.ChangePoint(lastListItem.argumentPoint);

                    enemyPoint -= lastListItem.argumentPoint;
                    bottomPanel?.ShowPointUpdateAnimation(false);
                }
                else if (debateHelper.debateSupportType == DebateHelper.DebateSupportType.DoubleAllyArgument)
                {
                    lastListItem.ChangePoint(lastListItem.argumentPoint);

                    playerPoint += lastListItem.argumentPoint;
                    bottomPanel?.ShowPointUpdateAnimation(true);
                }
            });
        }
    }
}

