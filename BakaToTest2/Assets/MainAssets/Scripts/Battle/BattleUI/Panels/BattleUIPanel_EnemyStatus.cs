﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace Intelligensia.Battle
{
    public class BattleUIPanel_EnemyStatus : ViewController
    {
        [Header("Enemy")]
        [SerializeField] private BattleUIItem_EnemyStatus listItemPrefab = default;
        [SerializeField] private Transform container = default;
        [SerializeField] private TextMeshProUGUI argumentSideText = default;


        private Dictionary<BattleEntity, BattleUIItem_EnemyStatus> listLookup;
        private List<BattleUIItem_EnemyStatus> enemyList;

        private BattleUIItem_EnemyStatus currentTurnEnemy;


        protected override void OnViewReady()
        {
            base.OnViewReady();

            if(currentTurnEnemy != null)
            {
                currentTurnEnemy.ShowCurrentTurnDisplay();
            }
        }

        public override void Close()
        {
            base.Close();

            if(currentTurnEnemy != null)
            {
                currentTurnEnemy.ResetDisplay();
            }
        }

        public void OnOneRoundEnded()
        {
            SetArgumentSideLabel();
        }

        private void SetArgumentSideLabel()
        {
            if (argumentSideText != null)
                argumentSideText.text = BattleController.instance.enemyArgumentSide.ToString();
        }

        public void GenerateEnemyList()
        {
            SetArgumentSideLabel();

            if (enemyList != null && enemyList.Count > 0)
            {
                ClearList();
            }
            else
            {
                enemyList = new List<BattleUIItem_EnemyStatus>();
                listLookup = new Dictionary<BattleEntity, BattleUIItem_EnemyStatus>();
            }

            List<GenericEnemy> enemies = BattleInfoManager.activeEnemies;

            for (int i = 0; i < enemies.Count; i++)
            {
                AddEnemyList(enemies[i]);
            }
        }

        public void AddEnemyList(GenericEnemy enemy)
        {
            BattleUIItem_EnemyStatus listItem = Instantiate(listItemPrefab);
            enemyList.Add(listItem);
            listLookup.Add(enemy, listItem);

            listItem.transform.SetParent(container, false);

            listItem.Initialize(enemy);

            listItem.OnDefeatedEvent += RemoveItem;
        }


        private void ClearList()
        {
            for (int i = 0; i < enemyList.Count; i++)
            {
                Destroy(enemyList[i].gameObject);
            }

            enemyList.Clear();
            listLookup.Clear();
        }

        private void RemoveItem(BattleUIItem_EnemyStatus listItem)
        {
            listItem.OnDefeatedEvent -= RemoveItem;

            enemyList.Remove(listItem);
            listLookup.Remove(listItem.enemyEntity);

            Destroy(listItem.gameObject);
        }


        public void OnTurnNumberChanged(BattleEntity currentTurnEntity, BattleEntity nextTurnEntity)
        {
            if (enemyList.Count == 0)
                return;

            if (currentTurnEnemy != null)
            {
                currentTurnEnemy.ResetDisplay();
                currentTurnEnemy = null;
            }

            listLookup.TryGetValue(currentTurnEntity, out currentTurnEnemy);

            if (currentTurnEnemy != null)
            {
                currentTurnEnemy.ShowCurrentTurnDisplay();
            }

            listLookup.TryGetValue(nextTurnEntity, out BattleUIItem_EnemyStatus nextTurnEnemy);

            if (nextTurnEnemy != null)
            {
                nextTurnEnemy.ShowNextTurnDisplay();
            }
        }


        public void OnTargetSelectionMove(BattleEntity entity)
        {
            for (int i = 0; i < enemyList.Count; i++)
            {
                if (enemyList[i].enemyEntity == entity)
                {
                    enemyList[i].ToggleHighlight(true);
                }
                else
                {
                    enemyList[i].ToggleHighlight(false);
                }
            }
        }

        public void OnTargetSelectionCancel()
        {
            for (int i = 0; i < enemyList.Count; i++)
            {
                enemyList[i].ToggleHighlight(false);
            }
        }

        public void SelectAllTargets()
        {
            for (int i = 0; i < enemyList.Count; i++)
            {
                enemyList[i].ToggleHighlight(true);
            }
        }
    }
}
