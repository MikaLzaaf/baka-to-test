﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.InputSystem;
using Intelligensia.Battle;

namespace Intelligensia.Battle
{
    public class BattleUIPanel_TargetSelection : UIViewController
    {
        public Action OnItemTargetSelectedEvent;
        public Action<BattleEntity> MoveSelectionEvent;
        public Action SelectMultipleTargetsEvent;


        [SerializeField] private BattleUIItem_TargetSelect listItemPrefab = default;

        private List<BattleUIItem_TargetSelect> listItems;

        private AimedTo lastAim;
        public int lastTargetIndex { get; private set; }
        //private int lastItemCount = -1;
        //private bool prevTargetIsEnemy = false;
        //private bool currentTargetIsEnemy = true;

        //private const string BattleTargetSelectionControlsMap = "Battle Target Selection Controls";



        public override void Show()
        {
            base.Show();

            //currentTargetIsEnemy = BattleInfoManager.currentTargetIsEnemy;

            if (BattleInfoManager.currentAim == AimedTo.AllEnemies ||
                BattleInfoManager.currentAim == AimedTo.AllPartyMembers ||
                BattleInfoManager.currentAim == AimedTo.All)
            {
                GenerateMultipleTargets();
            }
            else if (BattleInfoManager.currentAim == AimedTo.Self)
            {
                GenerateSelfTarget();
            }
            else
            {
                GenerateSingleTargetList();
            }

            lastAim = BattleInfoManager.currentAim;
        }

        protected override void OnViewReady()
        {
            base.OnViewReady();

            BattleUIManager.instance.OnCancelButtonPressedEvent += CancelTargetSelection;
            BattleUIManager.instance.OninfoButtonPressedEvent += ToggleAnalyzePanel;
        }


        public override void Close()
        {
            base.Close();

            BattleUIManager.instance.OnCancelButtonPressedEvent -= CancelTargetSelection;
            BattleUIManager.instance.OninfoButtonPressedEvent -= ToggleAnalyzePanel;

            if(lastAim == AimedTo.AllEnemies || lastAim == AimedTo.AllPartyMembers || lastAim == AimedTo.All)
            {
                for(int i = 0; i < listItems.Count; i++)
                {
                    if(listItems[i].battleEntity is GenericEnemy)
                    {
                        GenericEnemy enemy = listItems[i].battleEntity as GenericEnemy;
                        enemy.ToggleStatusPanel(false);
                    }
                    else if (listItems[i].battleEntity is PlayerEntity)
                    {
                        listItems[i].battleEntity.RecycleSticky();
                    }
                }
            }
        }


        private void CancelTargetSelection()
        {
            if (!canProcessInput)
                return;

            BattlePhasesController.instance.ChangePhase(BattlePhaseEnums.PlayerTurnDecision);

            BattlePhasesController.instance.animatorController.IsTargetSelected(false);
        }


        private void OnListItemClick(BattleUIItem_TargetSelect listItem)
        {
            BattleInfoManager.AddActionSelected(listItem.battleEntity);

            if (BattleInfoManager.selectedItem != null)
            {
                OnItemTargetSelectedEvent?.Invoke();

                UINavigator.instance.ClearPreviousWindows();

                UINavigator.instance.OpenPreviousWindow();
            }
            else
                CancelTargetSelection();


            //OnTargetSelectedEvent?.Invoke();
        }


        public void MoveSelection(BattleEntity target, int targetIndex)
        {
            MoveSelectionEvent?.Invoke(target);

            lastTargetIndex = targetIndex;
            //BattleInfoManager.currentTargetHighlighted = target;
        }


        private void GenerateMultipleTargets()
        {
            List<BattleEntity> listTarget = BattleInfoManager.GetTargetList();

            ResetList();

            for (int i = 0; i < listTarget.Count; i++)
            {
                BattleUIItem_TargetSelect listItem = Instantiate(listItemPrefab);
                listItems.Add(listItem);

                listItem.transform.SetParent(this.transform, false);

                listItem.Initialize(this, listTarget[i], false, 0);

                listItem.onClick.AddListener(delegate
                {
                    OnListItemClick(listItem);
                });

                if (listTarget[i] is GenericEnemy)
                {
                    GenericEnemy enemy = listTarget[i] as GenericEnemy;
                    enemy.ToggleStatusPanel(true);
                }
                else if (listTarget[i] is PlayerEntity)
                {
                    listTarget[i].DisplayCurrentFatigue();
                }
            }

            SelectMultipleTargetsEvent?.Invoke();
        }


        private void GenerateSingleTargetList()
        {
            List<BattleEntity> listTarget = BattleInfoManager.GetTargetList();

            ResetList();

            for (int i = 0; i < listTarget.Count; i++)
            {
                BattleUIItem_TargetSelect listItem = Instantiate(listItemPrefab);
                listItems.Add(listItem);

                listItem.transform.SetParent(this.transform, false);

                listItem.Initialize(this, listTarget[i], true, i);

                listItem.onClick.AddListener(delegate
                {
                    OnListItemClick(listItem);
                });
            }

            UINavigator.instance.SetupHorizontalNavigation(listItems, true);
        }


        private void GenerateSelfTarget()
        {
            ResetList();

            BattleUIItem_TargetSelect listItem = Instantiate(listItemPrefab);
            listItems.Add(listItem);

            listItem.transform.SetParent(this.transform, false);

            listItem.Initialize(this, BattleInfoManager.entityOnFocus, true, 0);

            listItem.onClick.AddListener(delegate
            {
                OnListItemClick(listItem);
            });

            UINavigator.instance.SetupHorizontalNavigation(listItems, true);
        }


        private void ResetList()
        {
            if (listItems != null && listItems.Count > 0)
            {
                ClearList();
            }
            else
            {
                listItems = new List<BattleUIItem_TargetSelect>();
            }
        }

        private void ClearList()
        {
            if (listItems.Count > 0)
            {
                for (int i = listItems.Count - 1; i >= 0; i--)
                {
                    Destroy(listItems[i].gameObject);
                }
            }

            listItems.Clear();
        }

        private void ToggleAnalyzePanel()
        {
            if(canProcessInput)
                BattleUIManager.instance.ShowAnalyzeScreen();
        }
    }
}
