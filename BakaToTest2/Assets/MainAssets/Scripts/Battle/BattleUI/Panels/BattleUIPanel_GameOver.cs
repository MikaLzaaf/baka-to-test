﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Intelligensia.Battle
{
    public class BattleUIPanel_GameOver : UIViewController
    {
        public Action RetryEvent;


        protected override void OnViewClosed()
        {
            base.OnViewClosed();

            UINavigator.instance.ClearWindowsList();
        }


        public void Retry()
        {
            Close();

            RetryEvent?.Invoke();

            //GameUIManager.instance.ShowBattleTransition(false, true);
        }


        public void Quit()
        {
            // Back to title screen

            Close();

        }


        public void ReloadGame()
        {
            // Load saved game file

            Close();
        }

    }
}
