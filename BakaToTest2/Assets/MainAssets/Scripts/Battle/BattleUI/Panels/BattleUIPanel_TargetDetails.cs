﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using TMPro;

namespace Intelligensia.Battle
{
    public class BattleUIPanel_TargetDetails : UIViewController
    {
        [Header("Input")]
        [SerializeField] private PlayerInput panelInput = default;

        [Header("Texts")]
        [SerializeField] private TextMeshProUGUI nameText = default;
        [SerializeField] private TextMeshProUGUI classNameText = default;
        [SerializeField] private TextMeshProUGUI overallGradeText = default;

        [Header("Text Format")]
        [SerializeField] private string classNameFormat = default;

        [Header("Buttons")]
        [SerializeField] private GameObject nextEnemyIcon = default;
        [SerializeField] private GameObject prevEnemyIcon = default;

        [Header("Containers")]
        [SerializeField] private Transform subjectEfficacyContainer = default;
        [SerializeField] private Transform characterInfoContainer = default;
        [SerializeField] private Transform droppedMaterialContainer = default;
        [SerializeField] private Transform dropItemsContainer = default;

        [Header("Prefabs")]
        [SerializeField] private BattleUIItem_DropMaterial dropIlmuPrefab = default;
        [SerializeField] private BattleUIItem_SubjectEfficacy subjectEfficacyPrefab = default;
        [SerializeField] private BattleUIItem_CharacterInfo characterInfoPrefab = default;
        [SerializeField] private BattleUIItem_DropItem dropItemPrefab = default;

        private List<BattleUIItem_DropMaterial> listIlmuItems;
        private List<BattleUIItem_SubjectEfficacy> listSubjects;
        private List<BattleUIItem_CharacterInfo> listInfos;
        private List<BattleUIItem_DropItem> listDropItems;

        private GenericEnemy enemy;
        private Transform modelTransform;
        private Vector2 inputRotationDirection;

        private int enemiesCount = 0;
        private int targetIndex = 0;


        public override void Show()
        {
            base.Show();

            enemy = BattleInfoManager.activeEnemies[0] as GenericEnemy;

            enemiesCount = BattleInfoManager.activeEnemies.Count;
            targetIndex = 0;

            ToggleButtonIcon(enemiesCount > 1);

            UpdateDisplay();

            UpdateModel();

            BattleUIManager.instance.SetInputActiveState(false);

            SetInputActiveState(true);
        }




        public override void Close()
        {
            base.Close();

            ModelCameraController.instance.Deactivate();

            SetInputActiveState(false);

            BattleUIManager.instance.SetInputActiveState(true);
        }

        // TODO : Add level, exp and description to battle entity. Also add separate model system later
        private void UpdateDisplay()
        {
            nameText.text = enemy.characterName;

            classNameText.text = string.Format(classNameFormat, enemy.CharacterStatsBase.studentClass);

            overallGradeText.text = enemy.characterStatsData.GetOverallGrade();

            GenerateSubjectEfficacies();

            GenerateCharacterInfos();

            GenerateDropIlmu();

            GenerateDropItems();

        }


        private void UpdateModel()
        {
            ModelCameraController.instance.Activate(ModelPositionOnScreen.Centre, enemy);

            modelTransform = ModelCameraController.instance.entityModel.transform;
        }

        protected void Update()
        {
            if (inputRotationDirection.sqrMagnitude > 0)
            {
                modelTransform.Rotate(inputRotationDirection);
            }
        }

        private void GenerateSubjectEfficacies()
        {
            if (listSubjects != null && listSubjects.Count > 0)
            {
                if (listSubjects.Count > 0)
                {
                    for (int i = listSubjects.Count - 1; i >= 0; i--)
                    {
                        Destroy(listSubjects[i].gameObject);
                    }
                }

                listSubjects.Clear();
            }
            else
            {
                listSubjects = new List<BattleUIItem_SubjectEfficacy>();
            }

            List<SubjectStats> currentStatsList = enemy.characterStatsData.currentAcademicStats.subjectsStats;

            for (int i = 0; i < currentStatsList.Count; i++)
            {
                BattleUIItem_SubjectEfficacy listItem = Instantiate(subjectEfficacyPrefab);
                listSubjects.Add(listItem);

                listItem.transform.SetParent(subjectEfficacyContainer, false);

                listItem.Initialize(currentStatsList[i]);
            }
        }

        private void GenerateCharacterInfos()
        {
            if (listInfos != null && listInfos.Count > 0)
            {
                if (listInfos.Count > 0)
                {
                    for (int i = listInfos.Count - 1; i >= 0; i--)
                    {
                        Destroy(listInfos[i].gameObject);
                    }
                }

                listInfos.Clear();
            }
            else
            {
                listInfos = new List<BattleUIItem_CharacterInfo>();
            }

            List<CharacterInfo> characterInfos = enemy.CharacterStatsBase.characterInfos;

            for (int i = 0; i < characterInfos.Count; i++)
            {
                BattleUIItem_CharacterInfo listItem = Instantiate(characterInfoPrefab);
                listInfos.Add(listItem);

                listItem.transform.SetParent(characterInfoContainer, false);

                CharacterInfoData infoData = enemy.characterStatsData.GetCharacterInfoData(characterInfos[i].infoId);

                listItem.Initialize(characterInfos[i], infoData);
            }
        }

        private void GenerateDropIlmu()
        {
            if (listIlmuItems != null && listIlmuItems.Count > 0)
            {
                if (listIlmuItems.Count > 0)
                {
                    for (int i = listIlmuItems.Count - 1; i >= 0; i--)
                    {
                        Destroy(listIlmuItems[i].gameObject);
                    }
                }

                listIlmuItems.Clear();
            }
            else
            {
                listIlmuItems = new List<BattleUIItem_DropMaterial>();
            }

            IlmuCost[] currentItemList = enemy.ilmuDrop;

            for (int i = 0; i < currentItemList.Length; i++)
            {
                BattleUIItem_DropMaterial listItem = Instantiate(dropIlmuPrefab);
                listIlmuItems.Add(listItem);

                listItem.transform.SetParent(droppedMaterialContainer, false);

                listItem.Initialize(currentItemList[i]);
            }
        }

        private void GenerateDropItems()
        {
            if (listDropItems != null && listDropItems.Count > 0)
            {
                if (listDropItems.Count > 0)
                {
                    for (int i = listDropItems.Count - 1; i >= 0; i--)
                    {
                        Destroy(listDropItems[i].gameObject);
                    }
                }

                listDropItems.Clear();
            }
            else
            {
                listDropItems = new List<BattleUIItem_DropItem>();
            }

            List<SimplerItem> currentItemList = enemy.itemDrop;

            for (int i = 0; i < currentItemList.Count; i++)
            {
                BattleUIItem_DropItem listItem = Instantiate(dropItemPrefab, dropItemsContainer);
                listDropItems.Add(listItem);

                listItem.transform.SetParent(dropItemsContainer, false);

                listItem.Initialize(currentItemList[i].GetItem());
            }
        }

        private void ToggleButtonIcon(bool active)
        {
            if (prevEnemyIcon != null)
                prevEnemyIcon.SetActive(active);

            if (nextEnemyIcon != null)
                nextEnemyIcon.SetActive(active);
        }

        private void SwitchTarget(int switchValue)
        {
            targetIndex += switchValue;

            if (targetIndex >= enemiesCount)
            {
                targetIndex = 0;
            }
            else if (targetIndex < 0)
            {
                targetIndex = enemiesCount - 1;
            }

            enemy = BattleInfoManager.activeEnemies[targetIndex];
        }


        private void SetInputActiveState(bool active)
        {
            if (panelInput == null)
                return;

            switch (active)
            {
                case false:
                    panelInput.DeactivateInput();
                    break;

                case true:
                    panelInput.ActivateInput();
                    break;
            }
        }

        public void RotateModel(InputAction.CallbackContext context)
        {
            Vector2 direction = context.ReadValue<Vector2>();

            inputRotationDirection = new Vector2(direction.y, direction.x);
            inputRotationDirection.x = 0f;
        }

        public void OnCancel(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                UINavigator.instance.OpenPreviousWindow();
            }
        }

        public void NextTarget(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                if (enemiesCount <= 1)
                    return;

                SwitchTarget(1);

                UpdateDisplay();

                UpdateModel();
            }
        }

        public void PreviousTarget(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                if (enemiesCount <= 1)
                    return;

                SwitchTarget(-1);

                UpdateDisplay();

                UpdateModel();
            }
        }
    }
}
