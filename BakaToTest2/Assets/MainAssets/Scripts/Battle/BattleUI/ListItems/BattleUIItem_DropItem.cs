﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace Intelligensia.Battle
{
    public class BattleUIItem_DropItem : MonoBehaviour
    {
        private enum DropItemType
        {
            NormalItem,
            Argument
        }

        [SerializeField] private Image itemIcon = default;
        [SerializeField] private TextMeshProUGUI itemNameText = default;

        [SerializeField] private GameObject background_Normal = default;
        [SerializeField] private GameObject background_Argument = default;



        public void Initialize(Item item)
        {
            if(item == null)
            {
                gameObject.SetActive(false);
                return;
            }

            itemIcon.sprite = IconLoader.GetItemIcon(item.itemValue.ItemIcon);
            itemNameText.text = item.itemValue.ItemName;

            if (item.itemValue is SkillItemBaseValue)
                ToggleBackground(DropItemType.Argument);
            else
                ToggleBackground(DropItemType.NormalItem);
        }

        private void ToggleBackground(DropItemType dropItemType)
        {
            if (background_Normal != null)
                background_Normal.SetActive(dropItemType == DropItemType.NormalItem);

            if (background_Argument != null)
                background_Argument.SetActive(dropItemType == DropItemType.Argument);
        }

    }

}
