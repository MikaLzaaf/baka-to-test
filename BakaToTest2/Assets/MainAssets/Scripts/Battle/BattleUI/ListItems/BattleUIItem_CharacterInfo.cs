﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace Intelligensia.Battle
{
    public class BattleUIItem_CharacterInfo : MonoBehaviour
    {
        public enum InfoStatus
        {
            Normal,
            InEffect,
            Locked
        }

        [SerializeField] private GameObject background_Normal = default;
        [SerializeField] private GameObject background_InEffect = default;
        [SerializeField] private GameObject background_Locked = default;

        [SerializeField] private TextMeshProUGUI infoDetailText = default;

        [Header("Effect Description")]
        [SerializeField] private GameObject effectBg_Normal = default;
        [SerializeField] private GameObject effectBg_InEffect = default;
        [SerializeField] private GameObject effectBg_Locked = default;

        [SerializeField] private TextMeshProUGUI effectDescriptionText = default;

        public void Initialize(CharacterInfo characterInfo, CharacterInfoData characterInfoData)
        {
            infoDetailText.text = characterInfo.GetLocalizedInfo();
            effectDescriptionText.text = characterInfo.GetEffectDescription();

            if (characterInfoData.isLocked)
            {
                ToggleBackground(InfoStatus.Locked);
            }
            else if (characterInfoData.isInEffect)
            {
                ToggleBackground(InfoStatus.InEffect);
            }
            else
                ToggleBackground(InfoStatus.Normal);
        }

        private void ToggleBackground(InfoStatus infoStatus)
        {
            if(background_Normal != null)
                background_Normal.SetActive(infoStatus == InfoStatus.Normal);

            if (effectBg_Normal != null)
                effectBg_Normal.SetActive(infoStatus == InfoStatus.Normal);

            if (background_InEffect != null)
                background_InEffect.SetActive(infoStatus == InfoStatus.InEffect);

            if (effectBg_InEffect != null)
                effectBg_InEffect.SetActive(infoStatus == InfoStatus.InEffect);

            if (background_Locked != null)
                background_Locked.SetActive(infoStatus == InfoStatus.Locked);

            if (effectBg_Locked != null)
                effectBg_Locked.SetActive(infoStatus == InfoStatus.Locked);
        }
    }
}
