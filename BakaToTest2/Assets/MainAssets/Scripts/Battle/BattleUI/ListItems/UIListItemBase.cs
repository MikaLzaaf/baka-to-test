﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

public abstract class UIListItemBase : Button, ISelectHandler
{
    public CanvasGroup canvasGroup;
    public Image icon;
    public TextMeshProUGUI nameText;
    public TextMeshProUGUI rightSideText;

    private bool _isSelectable = true;
    public bool isSelectable
    {
        get => _isSelectable;
        protected set
        {
            _isSelectable = value;

            if (canvasGroup != null)
                canvasGroup.alpha = _isSelectable ? 1f : 0.2f;
        }
    }



    protected virtual void InitializeBase(string name, string rightSide, string itemIcon)
    {
        nameText.text = name;
        rightSideText.text = rightSide;
        icon.sprite = IconLoader.GetItemIcon(itemIcon);
    }


}
