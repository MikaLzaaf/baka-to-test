using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace Intelligensia.Battle
{
    public class BattleUIItem_ClassBattleStatus : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI characterNameText = default;

        [Header("Class Color")]
        [SerializeField] private Color playerColor = default;
        [SerializeField] private Color opponentColor = default;

        public void InitializeItem(string characterName, bool isPlayerSide)
        {
            if (characterNameText != null)
            {
                characterNameText.text = characterName;

                characterNameText.color = isPlayerSide ? playerColor : opponentColor;
            }
        }
    }
}

