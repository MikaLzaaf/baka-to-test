﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace Intelligensia.Battle
{
    public class BattleUIItem_ForfeitChanges : MonoBehaviour
    {
        [Header("Display")]
        public Subjects subject;
        [SerializeField] private TextMeshProUGUI currentScoreText = default;

        //[Header("Changes")]
        //[SerializeField] private GameObject scoreChangesLabel = default;


        public void InitializeItem(bool shouldDeductScore, int newScore = 0)
        {
            if (shouldDeductScore)
                currentScoreText.text = newScore.ToString();

            ToggleScoreChanges(shouldDeductScore);
        }


        private void ToggleScoreChanges(bool active)
        {
            //if (scoreChangesLabel != null)
            //    scoreChangesLabel.SetActive(active);
            gameObject.SetActive(active);
        }
    }
}

