﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;
using Intelligensia.Battle.StatusEffects;


namespace Intelligensia.Battle
{
    public class BattleUIItem_EnemyStatus : MonoBehaviour
    {
        [Header("Indicator")]
        public GameObject nextIndicator;
        public GameObject nowIndicator;
        public FillIndicatorLerper fillIndicatorLerper;
        public GameObject breakIndicator;

        [Header("Action Icon")]
        public Image battleActionIcon;
        //public float jumpPower = 20f;
        //public int jumpAmount = 1;
        //public float jumpDuration = 0.3f;

        [Header("UI")]
        public TextMeshProUGUI characterNameText;
        public TextMeshProUGUI livesAmountText;
        public Image[] backgrounds;
        public CanvasGroup canvasGroup;
        public Image subjectIcon;
        public GameObject leaderLabel;

        [Header("Status Effects")]
        public BattleUIItem_StatusEffect statusEffectPrefab;
        public Transform statusEffectContainer;

        [Header("Highlight")]
        public GameObject highlight;
        public Color highlightedColor = Color.yellow;
        public Color initialBackgroundColor;
        public float listItemsTransitionDuration = 0.2f;
        public float listItemsTransitionSlideOffset = 100;

        //[Header("Format")]
        //public string fatigueTextFormat;

        [Header("On Defeated Tween")]
        public float xPositionOffset = 100f;
        public float tweenDuration = 0.5f;

        public event Action<BattleUIItem_EnemyStatus> OnDefeatedEvent;

        public BattleEntity enemyEntity { get; private set; }
        
        private Vector3? initialPosition;
        private List<BattleUIItem_StatusEffect> statusEffects;
        private Dictionary<StatusEffect, BattleUIItem_StatusEffect> effectsLookup;


        private void Awake()
        {
            BattleInfoManager.ActiveSubjectChangedEvent += UpdateSubjectIcon;
        }

        private void OnDestroy()
        {
            BattleInfoManager.ActiveSubjectChangedEvent -= UpdateSubjectIcon;

            if (enemyEntity != null)
            {
                enemyEntity.FatigueUpdateEvent -= OnFatigueChanged;
                enemyEntity.OnDeathEvent -= OnDefeated;
                enemyEntity.ActionSelectedUpdateEvent -= UpdateBattleActionIcon;
                //enemyEntity.SubjectKnockoutEvent -= OnKnockoutEvent;
                enemyEntity.statusEffectController.EffectAddedEvent -= OnStatusEffectAdded;
                enemyEntity.statusEffectController.EffectRemovedEvent -= OnStatusEffectRemoved;
            }
        }

        public void Initialize(BattleEntity entity)
        {
            enemyEntity = entity;

            characterNameText.text = enemyEntity.characterName;

            UpdateBattleActionIcon(enemyEntity.actionSelected);
            UpdateSubjectIcon(BattleInfoManager.currentActiveSubject);
            UpdateLivesAmount();

            enemyEntity.FatigueUpdateEvent += OnFatigueChanged;
            enemyEntity.OnDeathEvent += OnDefeated;
            enemyEntity.ActionSelectedUpdateEvent += UpdateBattleActionIcon;
            //enemyEntity.SubjectKnockoutEvent += OnKnockoutEvent;
            enemyEntity.statusEffectController.EffectAddedEvent += OnStatusEffectAdded;
            enemyEntity.statusEffectController.EffectRemovedEvent += OnStatusEffectRemoved;

            // TODO Init character icon based on his/her current fatigue
            float targetAmount = enemyEntity.fatigue / enemyEntity.stamina;

            ToggleDefeatedIndicator(false);
            ToggleLeaderLabel(enemyEntity.IsLeader);

            //ChangeFatigueTextColor(targetAmount);
            UpdateFillIndicator(targetAmount);

            InitializeExistingEffects();
        }

        private void ToggleNowIndicator(bool active)
        {
            if (nowIndicator != null)
            {
                nowIndicator.SetActive(active);
            }
        }

        private void ToggleNextIndicator(bool active)
        {
            if (nextIndicator != null)
            {
                nextIndicator.SetActive(active);
            }
        }


        public void ShowNextTurnDisplay()
        {
            ToggleNowIndicator(false);
            ToggleNextIndicator(true);
        }

        public void ShowCurrentTurnDisplay()
        {
            ToggleNextIndicator(false);
            ToggleNowIndicator(true);

            SlideToHighlight(true);
        }

        public void ToggleHighlight(bool active)
        {
            if (highlight != null)
            {
                if (highlight.activeSelf == active)
                    return;

                highlight.SetActive(active);
            }

            for(int i = 0; i < backgrounds.Length; i++)
            {
                backgrounds[i].color = active ? highlightedColor : initialBackgroundColor;
            }
  
            SlideToHighlight(active);
        }

        
        private void ToggleDefeatedIndicator(bool active)
        {
            if(canvasGroup != null)
            {
                canvasGroup.alpha = active ? 0.2f : 1f;

                if(active)
                {
                    //currentFatigueText.text = CharacterDefeatedDisplay;
                }
            }
        }

        private void ToggleSubjectBreakIndicator(bool active)
        {
            if (breakIndicator != null)
                breakIndicator.SetActive(active);
        }

        private void ToggleLeaderLabel(bool active)
        {
            if(leaderLabel != null)
                leaderLabel.SetActive(active);
        }


        private void OnFatigueChanged(float fatigue)
        {
            //int fatigueWhole = Mathf.FloorToInt(fatigue);
            //int residue = (int)((fatigue - fatigueWhole) * 10);

            //string formattedText = string.Format(fatigueTextFormat, fatigueWhole, residue);

            //currentFatigueText.text = formattedText;

            float targetAmount = fatigue / enemyEntity.stamina;

            //ToggleOverfatiguedIndicators(entityIsOverfatigued);
            //ChangeFatigueTextColor(targetAmount);
            UpdateFillIndicator(targetAmount);

            //damageTweener.StartTween(currentFatigueText.color);
        }


        private void OnDefeated()
        {
            Debug.Log("Defeated");
            ToggleDefeatedIndicator(true);
  
            //ChangeFatigueTextColor(0f);
            UpdateFillIndicator(0f);

            ResetDisplay();

            enemyEntity.FatigueUpdateEvent -= OnFatigueChanged;
            enemyEntity.OnDeathEvent -= OnDefeated;

            PlayDefeatedSequence();
        }

        private void PlayDefeatedSequence()
        {
            Sequence defeatedSequence = DOTween.Sequence();

            Tween lateralTween = transform.DOLocalMoveX(transform.localPosition.x + xPositionOffset, tweenDuration);
            Tween fadeOut = canvasGroup.DOFade(0, tweenDuration);

            defeatedSequence.Append(lateralTween).Insert(0f, fadeOut).OnComplete(delegate
            {
                OnDefeatedEvent?.Invoke(this);
            });

            defeatedSequence.Play();
        }

        //private void OnKnockoutEvent()
        //{
        //    UpdateLivesAmount();

        //    ToggleSubjectBreakIndicator(true);
        //}


        //private void ChangeFatigueTextColor(float targetAmount)
        //{
        //    if (currentFatigueText == null)
        //        return;

        //    if (enemyEntity.isDestroyed)
        //    {
        //        currentFatigueText.color = Color.white;
        //    }
        //    else
        //    {
        //        currentFatigueText.color = gradient.Evaluate(targetAmount);
        //    }
        //}

        private void UpdateLivesAmount()
        {
            //livesAmountText.text = enemyEntity.currentLivesAmount.ToString();
        }

        private void UpdateSubjectIcon(Subjects currentSubject)
        {
            if (subjectIcon == null)
                return;

            subjectIcon.sprite = IconLoader.GetSubjectIcon(currentSubject);

            UpdateBreakIndicator(currentSubject);
        }

        private void UpdateFillIndicator(float targetAmount)
        {
            if (fillIndicatorLerper == null)
                return;

            if (enemyEntity.isDestroyed)
            {
                fillIndicatorLerper.ResetIndicator();
            }
            else
            {
                fillIndicatorLerper.StartLerp(targetAmount);
            }
        }

        private void SlideToHighlight(bool slide)
        {
            if (!initialPosition.HasValue)
                initialPosition = transform.localPosition;

            if (slide)
            {
                transform.DOLocalMoveY(initialPosition.Value.y + listItemsTransitionSlideOffset,
                    listItemsTransitionDuration);
            }
            else
            {
                transform.DOLocalMoveY(initialPosition.Value.y, listItemsTransitionDuration);
            }
        }


        public void ResetDisplay()
        {
            ToggleNextIndicator(false);
            ToggleNowIndicator(false);

            SlideToHighlight(false);
        }

        private void UpdateBattleActionIcon(BattleSelectionType actionSelected)
        {
            if (battleActionIcon == null)
                return;

            battleActionIcon.sprite = IconLoader.GetBattleActionIcon(actionSelected.ToString());

            //Vector3 bounceLocation = battleActionIcon.transform.parent.localPosition;
            //battleActionIcon.transform.parent.DOLocalJump(bounceLocation, jumpPower, jumpAmount, jumpDuration);
            //battleActionIcon.transform.parent.DOPunchScale(punchScale, tweenDuration, punchVibrato);
        }

        private void UpdateBreakIndicator(Subjects currentSubject)
        {
            if (enemyEntity != null)
            {
                ToggleSubjectBreakIndicator(enemyEntity.IsKnockoutOnSubject(currentSubject));
            }
        }

        #region Status Effects

        private void InitializeExistingEffects()
        {
            for(int i = 0; i < enemyEntity.statusEffectController.Effects.Count; i++)
            {
                OnStatusEffectAdded(enemyEntity.statusEffectController.Effects[i]);
            }
        }

        private BattleUIItem_StatusEffect GetEffect(StatusEffect statusEffect)
        {
            effectsLookup.TryGetValue(statusEffect, out BattleUIItem_StatusEffect uIItem_StatusEffect);

            return uIItem_StatusEffect;
        }

        private void OnStatusEffectAdded(StatusEffect statusEffect)
        {
            if (statusEffects == null)
            {
                statusEffects = new List<BattleUIItem_StatusEffect>();
                effectsLookup = new Dictionary<StatusEffect, BattleUIItem_StatusEffect>();
            }

            if (GetEffect(statusEffect) != null)
                return;

            BattleUIItem_StatusEffect item = ObjectPool.Spawn(statusEffectPrefab) as BattleUIItem_StatusEffect;
            item.transform.SetParent(statusEffectContainer, false);

            item.InitializeItem(statusEffect);

            statusEffects.Add(item);
            effectsLookup.Add(statusEffect, item);
        }

        private void OnStatusEffectRemoved(StatusEffect statusEffect)
        {
            BattleUIItem_StatusEffect item = GetEffect(statusEffect);

            if (item == null)
                return;

            statusEffects.Remove(item);
            effectsLookup.Remove(statusEffect);

            Destroy(item.gameObject);

            UpdateBreakIndicator(BattleInfoManager.currentActiveSubject);
        }

        #endregion


    }
}
