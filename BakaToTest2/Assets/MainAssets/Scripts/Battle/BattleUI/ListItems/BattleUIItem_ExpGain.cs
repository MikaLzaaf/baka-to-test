﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

namespace Intelligensia.Battle
{
    public class BattleUIItem_ExpGain : MonoBehaviour
    {
        public GameObject expContent;
        public Text playerNameText;
        public Text expGainText;
        public Text playerLevelText;
        public Slider expBar;

        public float expTweenDuration = 1f;

        private PlayerEntity playerEntity;
        private Sequence resultTweenSequence;
        private int addedTotalExp;

        private int _totalExpGained;
        public int totalExpGained
        {
            get
            {
                return _totalExpGained;
            }
            set
            {
                _totalExpGained = value;

                expGainText.text = _totalExpGained.ToString();
            }
        }


        public void InitializeExpGained(PlayerEntity player)
        {
            playerEntity = player;

            playerNameText.text = playerEntity.characterName;
            //playerLevelText.text = playerEntity.playerStatsData.currentLevel.ToString();

            //expBar.minValue = playerEntity.playerStatsData.CalculateCurrentBaseExp() * 1f;
            //expBar.maxValue = playerEntity.playerStatsData.CalculateTotalExpToNextLevel() * 1f;
            //expBar.value = playerEntity.playerStatsData.currentExp;

            //totalExpGained = playerEntity.isDestroyed ? 0 : BattleInfoManager.CalculateTotalExpGained(playerEntity.playerStatsData.currentLevel);

            //playerEntity.playerStatsData.currentExp += totalExpGained;

            StartCoroutine(AddExperience());
        }


        private IEnumerator AddExperience()
        {
            yield return new WaitForSeconds(1f);

            //if(playerEntity.playerStatsData.currentExp >= expBar.maxValue)
            //{
            //    playerEntity.playerStatsData.LevelUp();

            //    expBar.minValue = expBar.maxValue;
            //expBar.maxValue = playerEntity.playerStatsData.CalculateTotalExpToNextLevel();
            //expBar.value = playerEntity.playerStatsData.currentExp;

            //playerLevelText.text = playerEntity.playerStatsData.currentLevel.ToString();
            //}

            //resultTweenSequence = DOTween.Sequence();
            //resultTweenSequence.Append(TweenExp(addedTotalExp));
        }


        private Tween TweenExp(int targetScore)
        {
            Sequence sequence = DOTween.Sequence();

            //sequence.OnStart(delegate
            //{
            //scorePanel.gameObject.SetActive(true);
            //totalExpGained = (int) expBar.minValue;
            //});

            //Tween tween = scorePanel.DOScale(scorePanelTweenScale, scorePanelTweenDuration).From();
            //sequence.Append(tween);

            if (targetScore > 0)
            {
                Tween tweenExpGainedToZero = DOTween.
                    To(() => this.totalExpGained, i => this.totalExpGained = i, 0, expTweenDuration).
                    OnStart(delegate
                    {
                    //scoreCountAudio.Play();
                }).
                    OnComplete(delegate
                    {
                    //scoreCountAudio.Stop();
                });

                sequence.Append(tweenExpGainedToZero);

                Tween tween = DOTween.
                    To(() => this.expBar.value, i => this.expBar.value = i, targetScore, expTweenDuration).
                    OnStart(delegate
                    {
                    //scoreCountAudio.Play();
                }).
                    OnComplete(delegate
                    {
                    //scoreCountAudio.Stop();
                });

                sequence.Append(tween);
            }

            return sequence;
        }

    }
}