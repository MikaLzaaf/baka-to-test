﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;

namespace Intelligensia.Battle
{
    public class BattleUIItem_SkillSelect : UIListItemBase, ISelectHandler
    {
        private enum SelectType
        {
            Attack,
            Support,
            Counter
        }

        [SerializeField] private GameObject cooldownContainer = default;
        [SerializeField] private TextMeshProUGUI cooldownDurationText = default;

        [SerializeField] private GameObject selectedLabel = default;
        [SerializeField] private TextMeshProUGUI selectedByText = default;

        private BattleUIPanel_AttackSkillSelection attackSelectionPanel;
        private BattleUIPanel_SupportSkillSelection supportSelectionPanel;
        //private BattleUIPanel_CounterSkillSelection counterSelectionPanel;

        private SelectType selectionType;
        private string RightSideTextFormat = "{0} <size=60%>SP";
        private const string SelectedByTextFormat = "Selected by <color=#FED330>{0}</color>";

        public InBattleSkill skill { get; set; }

        public void Initialize(InBattleSkill skill, BattleUIPanel_AttackSkillSelection battleUIPanel_SkillSelection)
        {
            attackSelectionPanel = battleUIPanel_SkillSelection;

            this.skill = skill;

            if (skill == null)
                return;

            string rightSide = string.Format(RightSideTextFormat, skill.GetSkillPoint());
            InitializeBase(skill.GetSkillName(), rightSide, skill.GetListIcon());

            selectionType = SelectType.Attack;

            ResetDisplay();
        }

        //public void Initialize(InBattleSkill skill, BattleUIPanel_CounterSkillSelection counterSkillSelection)
        //{
        //    counterSelectionPanel = counterSkillSelection;

        //    this.skill = skill;

        //    if (skill == null)
        //        return;

        //    string rightSide = string.Format(RightSideTextFormat, skill.GetSkillPoint());
        //    InitializeBase(skill.GetSkillName(), rightSide, skill.GetListIcon());

        //    selectionType = SelectType.Counter;

        //    ResetDisplay();
        //}


        public void Initialize(InBattleSkill skill, BattleUIPanel_SupportSkillSelection supportSkillSelectionPanel)
        {
            supportSelectionPanel = supportSkillSelectionPanel;

            this.skill = skill;

            if (skill == null)
                return;

            InitializeBase(skill.GetSkillName(), "", skill.GetListIcon());

            selectionType = SelectType.Support;

            ResetDisplay();
        }

        public void ToggleSelectability(bool active)
        {
            isSelectable = active;
        }

        private void ToggleSelectedLabel(bool active, string characterName = "")
        {
            if (selectedLabel == null)
                return;

            selectedLabel.SetActive(active);

            if(active)
                selectedByText.text = string.Format(SelectedByTextFormat, characterName);
        }    

        private void ToggleCooldown(bool active)
        {
            if (cooldownContainer == null)
                return;

            cooldownContainer.SetActive(active);

            if(active)
                cooldownDurationText.text = skill.ElapsedCooldownDuration.ToString();
        }

        public void ResetDisplay()
        {
            ToggleCooldown(skill.isOnCooldown);

            var sameAction = BattleInfoManager.GetSameArgumentAction(skill.GetSkillName());

            if (sameAction != null && !skill.isOnCooldown)
                ToggleSelectedLabel(true, sameAction.user.characterName);
            else
                ToggleSelectedLabel(false);

            bool hasGuardOrUseItem = false;

            BattleAction characterAction = BattleInfoManager.GetFirstCharacterAction(true);
            if(characterAction != null)
            {
                if (characterAction.selectionType == BattleSelectionType.Item || characterAction.selectionType == BattleSelectionType.Counter)
                    hasGuardOrUseItem = true;
            }    

            ToggleSelectability(sameAction == null && !skill.isOnCooldown && !hasGuardOrUseItem && !BattleInfoManager.HasFullArguments());
        }

        public void UpdateDescription() // Assigned on EventTrigger
        {
            if (selectionType == SelectType.Attack)
            {
                attackSelectionPanel.UpdateSkillDescription(skill);
            }
            else if (selectionType == SelectType.Support)
            {
                supportSelectionPanel.UpdateSkillDescription(skill);
            }
            //else if (selectionType == SelectType.Counter)
            //{
            //    counterSelectionPanel.UpdateSkillDescription(skill);
            //}
        }

        public void NullDescription()// Assigned on EventTrigger
        {
            if (selectionType == SelectType.Attack)
            {
                attackSelectionPanel.UpdateSkillDescription(null);
            }
            else if (selectionType == SelectType.Support)
            {
                supportSelectionPanel.UpdateSkillDescription(null);
            }
            //else if (selectionType == SelectType.Counter)
            //{
            //    counterSelectionPanel.UpdateSkillDescription(null);
            //}
        }
    }
}
