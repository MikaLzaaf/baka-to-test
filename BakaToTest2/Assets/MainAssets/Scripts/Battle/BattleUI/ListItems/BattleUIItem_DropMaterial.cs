﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace Intelligensia.Battle
{
    public class BattleUIItem_DropMaterial : MonoBehaviour
    {
        [SerializeField] private Image materialIcon = default;
        [SerializeField] private TextMeshProUGUI materialAmountText = default;

        [SerializeField] private Color malayColor = default;
        [SerializeField] private Color englishColor = default;
        [SerializeField] private Color mathsColor = default;
        [SerializeField] private Color historyColor = default;
        [SerializeField] private Color peColor = default;
        [SerializeField] private Color physicsColor = default;
        [SerializeField] private Color biologyColor = default;
        [SerializeField] private Color chemistryColor = default;

        public void Initialize(IlmuCost ilmu)
        {
            if (ilmu != null)
            {
                materialIcon.sprite = IconLoader.GetSubjectIcon(ilmu.subject);

                materialAmountText.text = ilmu.cost.ToString();

                SetColor(ilmu.subject);
            }
            else
            {
                gameObject.SetActive(false);
            }
        }

        private void SetColor(Subjects subject)
        {
            if (materialIcon == null)
                return;

            if(subject == Subjects.BahasaMelayu)
            {
                materialIcon.color = malayColor;
            }
            else if (subject == Subjects.English)
            {
                materialIcon.color = englishColor;
            }
            else if (subject == Subjects.Mathematics)
            {
                materialIcon.color = mathsColor;
            }
            else if (subject == Subjects.History)
            {
                materialIcon.color = historyColor;
            }
            else if (subject == Subjects.PE)
            {
                materialIcon.color = peColor;
            }
            else if (subject == Subjects.Physics)
            {
                materialIcon.color = physicsColor;
            }
            else if (subject == Subjects.Biology)
            {
                materialIcon.color = biologyColor;
            }
            else if (subject == Subjects.Chemistry)
            {
                materialIcon.color = chemistryColor;
            }
        }
    }
}
