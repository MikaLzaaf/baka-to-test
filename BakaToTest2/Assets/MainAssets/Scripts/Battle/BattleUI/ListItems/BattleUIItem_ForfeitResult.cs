﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace Intelligensia.Battle
{
    public class BattleUIItem_ForfeitResult : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI nameText = default;
        [SerializeField] private Image characterIcon = default;
        [SerializeField] private BattleUIItem_ForfeitChanges[] forfeitChanges = default;

        [Header("Deduct Subject Score")]
        [SerializeField] int deductAmount = 1;

        private Dictionary<Subjects, BattleUIItem_ForfeitChanges> changesLookup;


        public void InitializeItem(PlayerEntity playerEntity, BattleUIItem_IlmuChanges[] ilmuLosses)
        {
            if (playerEntity == null)
                return;

            InitializeDictionary();

            nameText.text = playerEntity.characterName;

            if(characterIcon != null)
                characterIcon.sprite = PlayerInfoManager.LoadCharacterIcon(playerEntity.characterId);

            for(int i = 0; i < ilmuLosses.Length; i++)
            {
                var changesDisplay = GetChanges(ilmuLosses[i].subject);

                if (ilmuLosses[i].ShouldApplyPenalty)
                {
                    int newScore = 1;

                    var player = PlayerPartyManager.instance.GetPartyMember(playerEntity.characterId);

                    if (player != null)
                    {
                        player.playerStatsData.DeductSubjectScore(ilmuLosses[i].subject, deductAmount);

                        newScore = player.playerStatsData.currentAcademicStats.GetSubjectStats(ilmuLosses[i].subject).Score;
                    }

                    changesDisplay.InitializeItem(true, newScore);
                }
                else
                    changesDisplay.InitializeItem(false);
            }
        }


        private BattleUIItem_ForfeitChanges GetChanges(Subjects subject)
        {
            changesLookup.TryGetValue(subject, out BattleUIItem_ForfeitChanges item_ForfeitChanges);
            return item_ForfeitChanges;
        }

        private void InitializeDictionary()
        {
            changesLookup = new Dictionary<Subjects, BattleUIItem_ForfeitChanges>();

            for (int i = 0; i < forfeitChanges.Length; i++)
            {
                changesLookup.Add(forfeitChanges[i].subject, forfeitChanges[i]);
            }
        }
    }
}

