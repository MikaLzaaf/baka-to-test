﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


namespace Intelligensia.Battle
{
    public class BattleUIItem_IlmuChanges : MonoBehaviour
    {
        public Subjects subject;
        [SerializeField] private TextMeshProUGUI ilmuOwnedText = default;
        [SerializeField] private TextMeshProUGUI ilmuChangesText = default;

        [SerializeField] private GameObject penaltyLabel = default;


        public bool ShouldApplyPenalty { get; private set; }


        public void InitializeLoss(IlmuCost battleCost)
        {
            Item ilmu = PlayerInventoryManager.instance.GetIlmuBySubject(subject);

            if (ilmu == null)
                return;

            int leftoverIlmuAmount = ilmu.amount - battleCost.cost;

            int costDeducted = battleCost.cost;

            bool shouldDeductSubjectScore = false;

            if (leftoverIlmuAmount < 0)
            {
                costDeducted += leftoverIlmuAmount;

                leftoverIlmuAmount = 0;
                shouldDeductSubjectScore = true;
            }

            TogglePenaltyLabel(shouldDeductSubjectScore);

            ilmuOwnedText.text = leftoverIlmuAmount.ToString();
            ilmuChangesText.text = "- " + costDeducted.ToString();

            // Update ilmu owned
            PlayerInventoryManager.instance.AddItem(ilmu.ItemID, leftoverIlmuAmount, true);
        }

        public void InitializeGain(IlmuCost ilmuGain)
        {
            TogglePenaltyLabel(false);

            Item ilmu = PlayerInventoryManager.instance.GetIlmuBySubject(subject);

            if (ilmu == null)
                return;

            int newIlmuAmount = ilmu.amount + ilmuGain.cost;

            ilmuOwnedText.text = newIlmuAmount.ToString();
            ilmuChangesText.text = "+ " + ilmuGain.cost;

            // Update ilmu owned
            PlayerInventoryManager.instance.AddItem(ilmu.ItemID, newIlmuAmount, true);
        }

        public void ResetValue(bool isGaining, Subjects subject)
        {
            Item ilmu = PlayerInventoryManager.instance.GetIlmuBySubject(subject);

            ilmuOwnedText.text = ilmu != null ? ilmu.amount.ToString() : "0";
            ilmuChangesText.text = isGaining ? "+ 0" : "- 0";
        }


        private void TogglePenaltyLabel(bool active)
        {
            ShouldApplyPenalty = active;

            if (penaltyLabel != null)
                penaltyLabel.SetActive(active);
        }
    }
}

