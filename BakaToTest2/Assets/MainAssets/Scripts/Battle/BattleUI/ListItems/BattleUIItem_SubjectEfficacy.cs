﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace Intelligensia.Battle
{
    public class BattleUIItem_SubjectEfficacy : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI efficacyGradeText = default;
        [SerializeField] private Image subjectIcon = default;
        [SerializeField] private GameObject activeSubjectIndicator = default;
        [SerializeField] private GameObject overfatigueHighlighter = default;

        [SerializeField] private Color lockedColor = default;
        [SerializeField] private Color advantageColor = default;
        [SerializeField] private Color disadvantageColor = default;

        private const float EfficacyDivider = 20f;
        private const string HiddenValue = "?";




        public void Initialize(SubjectStats stats)
        {
            float efficacy = (float)stats.finalValue / EfficacyDivider;

            efficacyGradeText.text = stats.IsHidden ? HiddenValue : GradeChecker.GetGrade(Mathf.RoundToInt(efficacy));

            efficacyGradeText.color = stats.IsHidden ? lockedColor : GetTextColor(efficacy);

            subjectIcon.sprite = stats.GetIcon();

            ToggleIndicator(BattleInfoManager.currentActiveSubject == stats.Subject);

            ToggleOverfatigueHighlight(stats.IsOverfatigue());
        }


        private Color GetTextColor(float efficacy)
        {
            Color textColor = lockedColor;

            if (efficacy >= 75)
            {
                textColor = disadvantageColor;
            }
            else
            {
                textColor = advantageColor;
            }

            return textColor;
        }

        private void ToggleIndicator(bool active)
        {
            if(activeSubjectIndicator != null)
            {
                activeSubjectIndicator.SetActive(active);
            }
        }

        private void ToggleOverfatigueHighlight(bool active)
        {
            if (overfatigueHighlighter != null)
                overfatigueHighlighter.SetActive(active);
        }
    }
}
