﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace Intelligensia.Battle
{
    public class BattleUIItem_TargetSelect : Button, ISelectHandler
    {
        //private bool isSelected = false;
        private bool isSingleTarget = true;

        private BattleUIPanel_TargetSelection targetSelectionPanel;
        public BattleEntity battleEntity { get; private set; }

        private int targetIndex;


        protected override void OnEnable()
        {
            base.OnEnable();

            //isSelected = false;
        }


        protected override void OnDisable()
        {
            base.OnDisable();

            if (battleEntity is GenericEnemy)
            {
                GenericEnemy enemy = battleEntity as GenericEnemy;
                enemy.ToggleStatusPanel(false);
            }

            battleEntity.RecycleSticky();
        }


        public override void OnSubmit(BaseEventData eventData)
        {
            base.OnSubmit(eventData);

            //isSelected = true;
        }


        public void Initialize(BattleUIPanel_TargetSelection panel, BattleEntity entity, bool isSingleTarget, int targetIndex)
        {
            targetSelectionPanel = panel;
            battleEntity = entity;

            this.isSingleTarget = isSingleTarget;
            this.targetIndex = targetIndex; 
        }

        public override void OnSelect(BaseEventData eventData)
        {
            base.OnSelect(eventData);

            if (!isSingleTarget)
                return;

            targetSelectionPanel.MoveSelection(battleEntity, targetIndex);

            if (battleEntity is GenericEnemy)
            {
                GenericEnemy enemy = battleEntity as GenericEnemy;
                enemy.ToggleStatusPanel(true);
            }
            else if(battleEntity is PlayerEntity)
            {
                battleEntity.DisplayCurrentFatigue();
            }
        }


        public override void OnDeselect(BaseEventData eventData)
        {
            base.OnDeselect(eventData);

            if (!isSingleTarget)
                return;

            if (battleEntity is GenericEnemy)
            {
                GenericEnemy enemy = battleEntity as GenericEnemy;
                enemy.ToggleStatusPanel(false);
            }
            else if (battleEntity is PlayerEntity)
            {
                battleEntity.RecycleSticky();
            }
        }


        //public void ShowEnemySimpleStatusPanel()
        //{
        //    battleEntity.DisplayCurrentFatigue();
        //}
    }
}
