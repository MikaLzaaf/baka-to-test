﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

namespace Intelligensia.Battle
{
    public class BattleUIItem_OrderSelect : UIListItemBase, ISelectHandler
    {
        [SerializeField] private GameObject warningContainer = default;

        private BattleUIPanel_OrderSelection orderSelectionPanel;

        public BattleOrder battleOrder { get; private set; }

        private BattleEntity orderHolder;

        private string RightSideTextFormat = "{0} <size=60%>SG";


        public void Initialize(BattleOrder order, BattleUIPanel_OrderSelection panel_OrderSelection)
        {
            if (order == null)
                return;

            battleOrder = order;
            orderSelectionPanel = panel_OrderSelection;

            string rightSide = string.Format(RightSideTextFormat, order.orderCost);
            InitializeBase(order.orderName, rightSide, order.ListIcon);

            ToggleSelectability(BattleInfoManager.currentStressPoint >= battleOrder.orderCost);
            //Debug.Log("init");
            //if (battleOrder.orderHolder == null)
            //    return;

            //orderHolder = BattleInfoManager.GetOrderActorInBattle(battleOrder.orderHolder.characterName);

            // TODO : Later add if the holder is involved in Battle, check if the holder is alive or dead.
            //        If alive, order can be used. If not, then it cannot be used.

            //if (orderHolder != null)
            //{
            //    orderHolder.OnDeathEvent += OnHolderDead;
            //    orderHolder.OnRevivedEvent += OnHolderRevived;
            //}
            
        }

        //protected override void OnDestroy()
        //{
        //    base.OnDestroy();

        //    if (orderHolder != null)
        //    {
        //        orderHolder.OnDeathEvent -= OnHolderDead;
        //        orderHolder.OnRevivedEvent -= OnHolderRevived;
        //    }
        //}


        //public override void OnSelect(BaseEventData eventData)
        //{
        //    base.OnSelect(eventData);
        //    //Debug.Log("On Select // is selectable " + isSelectable);
        //    if (isSelectable)
        //    {
        //        orderSelectionPanel.UpdateOrderDescription(battleOrder);
        //    }
        //    else
        //    {
        //        orderSelectionPanel.UpdateOrderDescription(null);
        //    }
        //}

        public void UpdateDescription()
        {
            orderSelectionPanel.UpdateOrderDescription(battleOrder);
        }

        public void NullDescription()
        {
            orderSelectionPanel.UpdateOrderDescription(null);
        }

        private void OnHolderDead()
        {
            interactable = false;
        }

        private void OnHolderRevived()
        {
            interactable = true;
        }

        public void ToggleSelectability(bool active)
        {
            isSelectable = active;

            ToggleWarning(!active);
        }

        private void ToggleWarning(bool active)
        {
            if(warningContainer != null)
            {
                warningContainer.SetActive(active);
            }
        }
    }
}
