﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace Intelligensia.Battle
{
    public class BattleUIItem_ItemSelect : UIListItemBase, ISelectHandler
    {
        private const string ItemAmountPrefix = "x ";

        private BattleUIPanel_ItemSelection itemSelectionPanel;

        public Item item { get; set; }


        public void Initialize(Item item, BattleUIPanel_ItemSelection battleUIPanel_ItemSelection)
        {
            string itemAmount = ItemAmountPrefix + item.amount.ToString();

            InitializeBase(item.itemValue.ItemName, itemAmount, item.itemValue.ItemIcon);

            itemSelectionPanel = battleUIPanel_ItemSelection;
            this.item = item;
        }


        //public override void OnSelect(BaseEventData eventData)
        //{
        //    base.OnSelect(eventData);

        //    itemSelectionPanel.UpdateItemDescription(item);
        //}

        public void UpdateDescription()
        {
            itemSelectionPanel.UpdateItemDescription(item);
        }

        public void NullDescription()
        {
            itemSelectionPanel.UpdateItemDescription(null);
        }
    }
}
