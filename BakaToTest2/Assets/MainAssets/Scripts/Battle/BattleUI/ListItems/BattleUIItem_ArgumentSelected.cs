using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace Intelligensia.Battle
{
    public class BattleUIItem_ArgumentSelected : Button
    {
        [SerializeField] private TextMeshProUGUI actionNameText = default;
        [SerializeField] private TextMeshProUGUI spText = default;
        [SerializeField] private Image actionTypeIcon = default;
        [SerializeField] private Image characterIcon = default;
        public CanvasGroup canvasGroup = default;

        private BattleUISubPanel_DebateArgumentSelection debateArgumentSelectionPanel;
        public BattleAction battleAction { get; private set; }


        private string RightSideTextFormat = "{0} <size=60%>SP";

        public void InitializeItem(BattleAction battleAction, BattleUISubPanel_DebateArgumentSelection debateArgumentSelectionPanel = null)
        {
            this.battleAction = battleAction;
            this.debateArgumentSelectionPanel = debateArgumentSelectionPanel;

            actionNameText.text = battleAction.actionName;
            actionTypeIcon.sprite = IconLoader.GetBattleActionIcon(battleAction.selectionType.ToString());

            PlayerEntity playerEntity = (PlayerEntity)battleAction.user;

            characterIcon.sprite = PlayerInfoManager.LoadCharacterIcon(playerEntity.characterId);

            if (debateArgumentSelectionPanel != null)
                SetupForDebate(battleAction.skill.GetSkillPoint());
        }

        private void SetupForDebate(int point)
        {
            actionTypeIcon.gameObject.SetActive(false);

            spText.text = string.Format(RightSideTextFormat, point);
            spText.gameObject.SetActive(true);
        }

        public void UpdateDescription() // Added in EventTrigger 
        {
            if (debateArgumentSelectionPanel != null)
                debateArgumentSelectionPanel.UpdateSkillDescription(battleAction.skill);
        }

        public void NullDescription() // Added in EventTrigger 
        {
            if (debateArgumentSelectionPanel != null)
                debateArgumentSelectionPanel.UpdateSkillDescription(null);
        }
    }
}

