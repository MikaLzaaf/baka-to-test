using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace Intelligensia.Battle
{
    public class BattleUIItem_TurnExecution : ViewController
    {
        [Header("Execution Stuff")]
        [SerializeField] private Transform cameraView = default;
        [SerializeField] private Transform cameraPivot = default;
        [SerializeField] private TextMeshProUGUI argumentNameText = default;
        [SerializeField] private Vector3 pivotRotationOffset = default;
        [SerializeField] private Vector3 positionOffset = default;
        [SerializeField] private Vector3 rotationOffset = default;

        private Transform originalParent;

        public override void Close()
        {
            base.Close();

            transform.SetParent(originalParent);
        }


        public void InitializeItem(BattleAction battleAction)
        {
            if (originalParent == null)
                originalParent = transform.parent;

            argumentNameText.text = battleAction.actionName;

            cameraPivot.SetParent(battleAction.user.transform);

            cameraPivot.localPosition = Vector3.zero;
            cameraPivot.localRotation = Quaternion.Euler(pivotRotationOffset);

            cameraView.localPosition = positionOffset;
            cameraView.localRotation = Quaternion.Euler(rotationOffset);

            Show();

            // Apply action here
            battleAction.Execute();
        }


    }
}

