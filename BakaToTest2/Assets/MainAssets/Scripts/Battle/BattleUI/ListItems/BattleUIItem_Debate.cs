using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

namespace Intelligensia.Battle
{
    public class BattleUIItem_Debate : MonoBehaviour
    {
        //[SerializeField] MiniCharacterWindow characterWindow = default;

        [SerializeField] TextMeshProUGUI characterNameText = default;
        [SerializeField] TextMeshProUGUI argumentNameText = default;
        [SerializeField] TextMeshProUGUI argumentPointText = default;
        [SerializeField] Image argumentPointFrame = default;
        public CanvasGroup canvasGroup;

        [Header("Displays")]
        [SerializeField] private GameObject startingDisplay = default;
        [SerializeField] private GameObject closingDisplay = default;

        [Header("Affected Effect")]
        [SerializeField] Color pointAffectedColor = default;
        [SerializeField] float jumpDuration = 0.3f;
        [SerializeField] float jumpPower = 0.3f;
        [SerializeField] float jumpDistance = 10f;
        [SerializeField] ViewController changeValueView = default;
        [SerializeField] TextMeshProUGUI changeValueText = default;

        [Header("Activation Effect")]
        [SerializeField] Color activationColor = default;
        [SerializeField] float activationScale = 2f;
        [SerializeField] float activationDuration = 1f;
        [SerializeField] float shakeDuration = 1f;
        [SerializeField] float shakeStrength = 1f;
        [SerializeField] int vibrato = 1;
        [SerializeField] GameObject enhancedArgumentIcon = default;

        [Header("Character Thought")]
        [SerializeField] GameObject thoughtContainer = default;
        [SerializeField] TextMeshProUGUI characterThoughtText = default;
        //[SerializeField] CanvasGroup thoughtCanvasGroup = default;

        private Color originalColor;
        public int argumentPoint { get; private set; }
        public BattleEntity character { get; private set; }


        public void InitializeItem(BattleEntity battleEntity, string argumentName, int point, int argumentType)
        {
            if (argumentNameText.text != null)
                argumentNameText.text = argumentName;

            character = battleEntity;
            argumentPoint = point;

            if (characterNameText != null)
                characterNameText.text = character.characterName;

            if (argumentPointText != null)
                argumentPointText.text = "+" + argumentPoint.ToString();

            ToggleThought(false); // TODO : Show character thoughts during debate

            if (argumentPointFrame != null)
                originalColor = argumentPointFrame.color;

            ToggleStartingDisplay(argumentType == 1);
            ToggleClosingDisplay(argumentType == 2);
        }

        private void ToggleThought(bool active)
        {
            if (thoughtContainer != null)
                thoughtContainer.SetActive(active);
        }

        public void ChangePoint(int value)
        {
            argumentPoint += value;
        
            if (argumentPointText != null)
            {
                if(argumentPoint > 0)
                    argumentPointText.text = "+" + argumentPoint.ToString();
                else
                    argumentPointText.text = argumentPoint.ToString();
            }

            if (argumentPointFrame != null)
            {
                argumentPointFrame.color = pointAffectedColor;

                Vector3 endValue = argumentPointFrame.transform.localPosition + (Vector3.up * jumpDistance);
                argumentPointFrame.transform.DOLocalJump(endValue, jumpPower, 1, jumpDuration);
            }

            ShowChangeValue(value);
        }

        private void ShowChangeValue(int value)
        {
            if(changeValueText != null)
            {
                if (value > 0)
                    changeValueText.text = "+" + value.ToString();
                else
                    changeValueText.text = value.ToString();
            }

            changeValueView?.Show();
        }

        public void ActivateEffect(System.Action actionOnScaleUp, System.Action actionOnComplete)
        {
            if (argumentPointFrame == null)
                return;

            argumentPointFrame.color = activationColor;

            if (enhancedArgumentIcon != null)
                enhancedArgumentIcon.SetActive(true);

            Sequence activationSequence = DOTween.Sequence();

            Tween scaleUp = argumentPointFrame.transform.DOScale(activationScale, activationDuration).
                OnComplete(delegate {
                    actionOnScaleUp?.Invoke();
            });
            Tween shake = argumentPointFrame.transform.DOShakePosition(shakeDuration, shakeStrength, vibrato);
            Tween scaleDown = argumentPointFrame.transform.DOScale(1f, activationDuration);

            activationSequence.Append(shake).Insert(0f, scaleUp).Insert(shakeDuration - activationDuration, scaleDown).OnComplete(delegate
            {
                argumentPointFrame.color = originalColor;
                actionOnComplete?.Invoke();
            });
        }

        private void ToggleStartingDisplay(bool active)
        {
            if (startingDisplay != null)
                startingDisplay.SetActive(active);
        }

        private void ToggleClosingDisplay(bool active)
        {
            if (closingDisplay != null)
                closingDisplay.SetActive(active);
        }
    }
}

