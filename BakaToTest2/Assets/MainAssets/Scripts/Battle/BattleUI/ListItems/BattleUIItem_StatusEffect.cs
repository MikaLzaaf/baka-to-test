﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Intelligensia.Battle.StatusEffects;


namespace Intelligensia.Battle
{
    public class BattleUIItem_StatusEffect : MonoBehaviour
    {
        [Header("Temporary Effect")]
        public GameObject tempDisplay;
        public Image tempStatusEffectIcon;

        [Header("Permanent Effect")]
        public GameObject permanentDisplay;
        public Image permanentStatusEffectIcon;

        [Header("Indicators")]
        public GameObject upArrow;
        public GameObject downArrow;

        public StatusEffect statusEffect { get; private set; }

        private bool isAilmentType = false;
        private bool showArrow = false;
        private bool isPermanentEffect = false;


        private void OnDestroy()
        {
            if (statusEffect != null)
            {
                statusEffect.ReactivateEvent -= ReActivateEffect;
            }
        }

        public void InitializeItem(StatusEffect statusEffect)
        {
            this.statusEffect = statusEffect;

            if(this.statusEffect != null)
            {
                this.statusEffect.ReactivateEvent += ReActivateEffect;
            }

            isPermanentEffect = statusEffect.duration > 100;

            string statusEffectName = "";

            if(statusEffect is StatusAilment)
            {
                StatusAilment ailment = (StatusAilment)statusEffect;

                isAilmentType = true;

                if(ailment.statusAilmentType == StatusAilmentType.DefenseDown || ailment.statusAilmentType == StatusAilmentType.AttackDown
                    || ailment.statusAilmentType == StatusAilmentType.StatusResistanceDown || ailment.statusAilmentType == StatusAilmentType.KnockoutChanceUp
                    || ailment.statusAilmentType == StatusAilmentType.KnockoutChanceUpBig)
                {
                    showArrow = true;
                }

                statusEffectName = ailment.statusAilmentType.ToString();
            }
            else if(statusEffect is StatusBoost)
            {
                StatusBoost boost = (StatusBoost)statusEffect;

                isAilmentType = false;
                showArrow = true;

                statusEffectName = boost.statusBoostType.ToString();
            }

            ActivateEffect(statusEffectName);
        }

        private void ReActivateEffect()
        {

        }

        private void ActivateEffect(string statusEffectName)
        {
            if (string.IsNullOrEmpty(statusEffectName))
                return;

            ToggleDisplay();

            if(isPermanentEffect)
            {
                if (permanentStatusEffectIcon != null)
                    permanentStatusEffectIcon.sprite = IconLoader.GetStatusEffectIcon(statusEffectName);
            }
            else
            {
                if (tempStatusEffectIcon != null)
                    tempStatusEffectIcon.sprite = IconLoader.GetStatusEffectIcon(statusEffectName);
            }
          
            if (upArrow != null)
                upArrow.SetActive(!isAilmentType && showArrow);

            if (downArrow != null)
                downArrow.SetActive(isAilmentType && showArrow);
        }

        private void ToggleDisplay()
        {
            if(tempDisplay != null)
            {
                tempDisplay.SetActive(!isPermanentEffect);
            }

            if (permanentDisplay != null)
            {
                permanentDisplay.SetActive(isPermanentEffect);
            }
        }
    }
}

