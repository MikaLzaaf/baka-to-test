﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using TMPro;
using DG.Tweening;
using Intelligensia.Battle.StatusEffects;


namespace Intelligensia.Battle
{
    public class BattleUIItem_PartyStatus : Button, ISelectHandler
    {

        [Header("Indicator")]
        public GameObject nextIndicator;
        public GameObject nowIndicator;
        public GameObject highlight;
        public FillIndicatorLerper fillIndicatorLerper;
        public GameObject breakIndicator;
        public Shadow[] highlightOnCurrentTurn;

        [Header("Action Icon")]
        public Image battleActionIcon;
        public float jumpPower = 20f;
        public int jumpAmount = 1;
        public float jumpDuration = 0.3f;

        [Header("UI")]
        public Image characterIcon;
        public TextMeshProUGUI livesAmountText;
        public CanvasGroup canvasGroup;
        public Image subjectIcon;
        public GameObject leaderLabel;
        public float listItemsTransitionDuration = 0.2f;
        public float listItemsTransitionSlideOffset = 50f;

        //public string fatigueTextFormat;

        [Header("Status Effects")]
        public BattleUIItem_StatusEffect statusEffectPrefab;
        public Transform statusEffectContainer;

        [Header("On Defeated Tween")]
        public float yPositionOffset = 100f;
        public float tweenDuration = 0.5f;

        public event Action<BattleUIItem_PartyStatus> OnDefeatedEvent;
        public event Action<BattleUIItem_PartyStatus> SwapMemberEvent;

        private Vector3? initialPosition;
        private List<BattleUIItem_StatusEffect> statusEffects;
        private Dictionary<StatusEffect, BattleUIItem_StatusEffect> effectsLookup;

        public PlayerEntity playerEntity { get; private set; }

        //private const string FatigueTextOldFormat = "<size=100%>{0}<space=.01em><size=60%>.{1}<voffset=1em><size=40%><color=red>/ {2}";
        //private const string CharacterDefeatedDisplay = "K.O";


        protected override void Awake()
        {
            base.Awake();

            //initialPosition = transform.localPosition;

            BattleInfoManager.ActiveSubjectChangedEvent += UpdateSubjectIcon;
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            BattleInfoManager.ActiveSubjectChangedEvent -= UpdateSubjectIcon;

            if (playerEntity != null)
            {
                playerEntity.FatigueUpdateEvent -= OnFatigueChanged;
                playerEntity.ActionSelectedUpdateEvent -= UpdateBattleActionIcon;
                playerEntity.OnDeathEvent -= OnDefeated;
                //playerEntity.SubjectKnockoutEvent -= OnKnockoutEvent;
                playerEntity.statusEffectController.EffectAddedEvent -= OnStatusEffectAdded;
                playerEntity.statusEffectController.EffectRemovedEvent -= OnStatusEffectRemoved;
            }
        }


        public override void OnSubmit(BaseEventData eventData)
        {
            base.OnSubmit(eventData);

            SwapMemberEvent?.Invoke(this);
        }

        public override void OnSelect(BaseEventData eventData)
        {
            base.OnSelect(eventData);

            ToggleHighlight(true);
        }


        public override void OnDeselect(BaseEventData eventData)
        {
            base.OnDeselect(eventData);

            ToggleHighlight(false);
        }


        public void Initialize(PlayerEntity playerEntity)
        {
            if (this.playerEntity != null)
                this.playerEntity.FatigueUpdateEvent -= OnFatigueChanged;

            this.playerEntity = playerEntity;
            

            UpdateLivesAmount();
            UpdateStatusEffects();
            UpdateSubjectIcon(BattleInfoManager.currentActiveSubject);

            battleActionIcon.sprite = IconLoader.GetBattleActionIcon(playerEntity.actionSelected.ToString());

            playerEntity.FatigueUpdateEvent += OnFatigueChanged;
            playerEntity.ActionSelectedUpdateEvent += UpdateBattleActionIcon;
            playerEntity.OnDeathEvent += OnDefeated;
            //playerEntity.SubjectKnockoutEvent += OnKnockoutEvent;
            playerEntity.statusEffectController.EffectAddedEvent += OnStatusEffectAdded;
            playerEntity.statusEffectController.EffectRemovedEvent += OnStatusEffectRemoved;

            float targetAmount = playerEntity.fatigue / playerEntity.stamina;

            // TODO Init character icon based on his/her current fatigue
            ToggleDefeatedIndicator(false);
            ToggleLeaderLabel(playerEntity.IsLeader);

            UpdateFillIndicator(targetAmount);

            UpdateCharacterIcon(targetAmount);
        }


        private void OnFatigueChanged(float fatigue)
        {
            //int fatigueWhole = Mathf.FloorToInt(fatigue);
            //int residue = (int)((fatigue - fatigueWhole) * 10);

            //string formattedText = string.Format(fatigueTextFormat, fatigueWhole, residue, playerEntity.stamina);

            //currentFatigueText.text = formattedText;

            float targetAmount = fatigue / playerEntity.stamina;
            // TODO Need to add icon and color changes when reaching certain threshold. Add other visual feedback too
            //ChangeFatigueTextColor(targetAmount);
            UpdateFillIndicator(targetAmount);
            UpdateCharacterIcon(targetAmount);

            //damageTweener.StartTween(currentFatigueText.color);

        }

        //private void OnKnockoutEvent()
        //{
        //    UpdateLivesAmount();

        //    ToggleSubjectBreakIndicator(true);
        //}

        private void UpdateFillIndicator(float targetAmount)
        {
            if (fillIndicatorLerper == null)
                return;

            if (playerEntity.isDestroyed)
            {
                fillIndicatorLerper.ResetIndicator();
            }
            else
            {
                fillIndicatorLerper.StartLerp(targetAmount);
            }
        }

        private void UpdateCharacterIcon(float targetAmount, bool isDefeated = false)
        {
            if (characterIcon == null)
                return;

            characterIcon.sprite = playerEntity.CharacterStatsBase.GetBattleIcon(targetAmount, isDefeated);
        }

        private void UpdateSubjectIcon(Subjects currentSubject)
        {
            if (subjectIcon == null)
                return;

            subjectIcon.sprite = IconLoader.GetSubjectIcon(currentSubject);

            UpdateBreakIndicator(currentSubject);
        }

        private void UpdateLivesAmount()
        {
            //livesAmountText.text = playerEntity.currentLivesAmount.ToString();
        }

        private void UpdateBreakIndicator(Subjects currentSubject)
        {
            if (playerEntity != null)
                ToggleSubjectBreakIndicator(playerEntity.IsKnockoutOnSubject(currentSubject));
        }

        private void UpdateStatusEffects()
        {
            if (playerEntity == null || playerEntity.statusEffectController == null)
                return;

            if(statusEffects != null && statusEffects.Count > 0)
            {
                for(int i = 0; i < statusEffects.Count; i++)
                {
                    Destroy(statusEffects[i].gameObject);
                }

                statusEffects.Clear();
                effectsLookup.Clear();
            }
            else
            {
                statusEffects = new List<BattleUIItem_StatusEffect>();
                effectsLookup = new Dictionary<StatusEffect, BattleUIItem_StatusEffect>();
            }

            var statusInEffect = playerEntity.statusEffectController.Effects;

            for(int i = 0; i < statusInEffect.Count; i++)
            {
                OnStatusEffectAdded(statusInEffect[i]);
            }
        }

        private void ToggleNowIndicator(bool active)
        {
            if (nowIndicator != null)
                nowIndicator.SetActive(active);
        }


        private void ToggleNextIndicator(bool active)
        {
            if (nextIndicator != null)
                nextIndicator.SetActive(active);
        }

        private void ToggleHighlight(bool active)
        {
            if (highlight != null)
                highlight.SetActive(active);
        }

        private void ToggleLeaderLabel(bool active)
        {
            if (leaderLabel != null)
                leaderLabel.SetActive(active);
        }

        public void ShowNextTurnDisplay()
        {
            ToggleNowIndicator(false);
            ToggleNextIndicator(true);
        }


        public void ShowCurrentTurnDisplay()
        {
            ToggleNextIndicator(false);
            ToggleNowIndicator(true);

            SlideToHighlight(true);
        }

        private void SlideToHighlight(bool slide)
        {
            if (!initialPosition.HasValue)
                initialPosition = transform.localPosition;

            if (slide)
            {
                transform.DOLocalMoveY(initialPosition.Value.y + listItemsTransitionSlideOffset,
                    listItemsTransitionDuration);

                for(int i = 0; i < highlightOnCurrentTurn.Length; i++)
                {
                    highlightOnCurrentTurn[i].enabled = true;
                }
            }
            else
            {
                transform.DOLocalMoveY(initialPosition.Value.y, 
                    listItemsTransitionDuration);

                for (int i = 0; i < highlightOnCurrentTurn.Length; i++)
                {
                    highlightOnCurrentTurn[i].enabled = false;
                }
            }
        }

        public void ResetDisplay()
        {
            ToggleNextIndicator(false);
            ToggleNowIndicator(false);

            SlideToHighlight(false);
        }

        private void UpdateBattleActionIcon(BattleSelectionType actionSelected)
        {
            if (battleActionIcon == null)
                return;

            battleActionIcon.sprite = IconLoader.GetBattleActionIcon(actionSelected.ToString());

            Vector3 bounceLocation = battleActionIcon.transform.parent.localPosition;
            battleActionIcon.transform.parent.DOLocalJump(bounceLocation, jumpPower, jumpAmount, jumpDuration);
        }

        private void OnDefeated()
        {
            Debug.Log("Defeated");
            ToggleDefeatedIndicator(true);

            //ChangeFatigueTextColor(0f);
            UpdateFillIndicator(0f);
            UpdateCharacterIcon(0f, true);

            ResetDisplay();

            playerEntity.FatigueUpdateEvent -= OnFatigueChanged;
            playerEntity.OnDeathEvent -= OnDefeated;
            playerEntity.ActionSelectedUpdateEvent -= UpdateBattleActionIcon;

            if(BattleController.instance.currentChallengeInfo.isClassBattle)
                PlayDefeatedSequence();
        }

        private void PlayDefeatedSequence()
        {
            Sequence defeatedSequence = DOTween.Sequence();

            Tween verticalTween = transform.DOLocalMoveY(transform.localPosition.y + yPositionOffset, tweenDuration);
            Tween fadeOut = canvasGroup.DOFade(0, tweenDuration);

            defeatedSequence.Append(verticalTween).Insert(0f, fadeOut).OnComplete(delegate
            {
                OnDefeatedEvent?.Invoke(this);
            });

            defeatedSequence.Play();
        }

        private void ToggleDefeatedIndicator(bool active)
        {
            if (canvasGroup != null)
            {
                canvasGroup.alpha = active ? 0.2f : 1f;

                //if (active)
                //{
                //    currentFatigueText.text = CharacterDefeatedDisplay;
                //}
            }
        }

        private void ToggleSubjectBreakIndicator(bool active)
        {
            if(breakIndicator != null)
            {
                breakIndicator.SetActive(active);
            }
        }

        #region Status Effects

        private BattleUIItem_StatusEffect GetEffect(StatusEffect statusEffect)
        {
            effectsLookup.TryGetValue(statusEffect, out BattleUIItem_StatusEffect uIItem_StatusEffect);

            return uIItem_StatusEffect;
        }

        private void OnStatusEffectAdded(StatusEffect statusEffect)
        {
            if (statusEffects == null)
            {
                statusEffects = new List<BattleUIItem_StatusEffect>();
                effectsLookup = new Dictionary<StatusEffect, BattleUIItem_StatusEffect>();
            }

            if (GetEffect(statusEffect) != null)
                return;

            BattleUIItem_StatusEffect item = ObjectPool.Spawn(statusEffectPrefab) as BattleUIItem_StatusEffect;
            item.transform.SetParent(statusEffectContainer, false);

            item.InitializeItem(statusEffect);

            statusEffects.Add(item);
            effectsLookup.Add(statusEffect, item);
        }

        private void OnStatusEffectRemoved(StatusEffect statusEffect)
        {
            BattleUIItem_StatusEffect item = GetEffect(statusEffect);

            if (item == null)
                return;

            statusEffects.Remove(item);
            effectsLookup.Remove(statusEffect);

            Destroy(item.gameObject);

            UpdateBreakIndicator(BattleInfoManager.currentActiveSubject);
        }

        #endregion
    }
}
