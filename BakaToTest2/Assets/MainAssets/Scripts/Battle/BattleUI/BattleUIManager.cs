﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.InputSystem;
using System;
using Intelligensia.Battle;

public class BattleUIManager : Singleton<BattleUIManager>
{
    public event Action OnCancelButtonPressedEvent;
    public event Action OnRightTabPressedEvent;
    public event Action OnLeftTabPressedEvent;
    public event Action OnRightTriggerPressedEvent;
    public event Action OnLeftTriggerPressedEvent;
    public event Action OninfoButtonPressedEvent;
    public event Action OnNorthButtonPressedEvent;
    public event Action OnSubmitButtonPressedEvent;
    public event Action OnBeginDebateEvent;



    public PlayerInput actionInput;

    [Header("Event Channel")]
    [SerializeField] private OneBattleRoundEventChannelSO oneBattleRoundEventChannel = default;


    [Header("No Input Panel")]
    public BattleUIPanel_PartyStatus partyStatusPanelController;
    public BattleUIPanel_EnemyStatus enemyStatusPanelController;
    public BattleUIPanel_BattleInfo battleInfoPanel;
    public BattleUIPanel_TurnExecution turnExecutionPanel;
    public ViewController newClassmateEntersView;
    public ViewController pausedPanel;
    public BattleUISubPanel_HelperCharacterView helperCharacterView;

    [Header("Can Input Panel")]
    public BattleUIPanel_ActionSelection actionSelectionPanel;
    public BattleUIPanel_TargetSelection targetSelectionPanel;
    public BattleUIPanel_ItemSelection itemSelectionPanel;
    public BattleUIPanel_OrderSelection orderSelectionPanel;
    public BattleUIPanel_Forfeit forfeitPanel;
    public BattleUIPanel_WinBattle gameWinPanelController;
    public BattleUIPanel_GameOver gameOverPanelController;
    public BattleUIPanel_TargetDetails targetDetailsPanel;
    public BattleUIPanel_AttackSkillSelection attackSkillSelectionPanel;
    public BattleUIPanel_SupportSkillSelection supportSkillSelectionPanel;
    //public BattleUIPanel_CounterSkillSelection counterSkillSelectionPanel;
    //public BattleUIPanel_Staggered staggeredPanel;
    //public BattleUIPanel_FaceOff faceOffPanel;
    //public BattleUIPanel_SwapMember swapMemberPanel;
    public BattleUIPanel_PartyAttack partyAttackPanel;
    public BattleUIPanel_ArgumentSelected argumentSelectedPanel;
    public BattleUIPanel_Debate debatePanel;

    public bool readyToReceiveActionInput = false;

    public bool isPaused { get; private set; }

    private const string PausedInputMap = "Paused Controls";
    private const string BattleActionsInputMap = "Battle Action Controls";

    private const string ConfirmationForDebate = "Start Debate?";


    private void Start()
    {
        if(actionSelectionPanel != null)
            actionSelectionPanel.OnPanelReady += OnActionSelectionPanelReady;
    }

    private void OnDestroy()
    {
        if (actionSelectionPanel != null)
            actionSelectionPanel.OnPanelReady -= OnActionSelectionPanelReady;

        SetInputActiveState(false);
    }

    public void Show()
    {
        if(!gameObject.activeSelf)
        {
            gameObject.SetActive(true);

            SwitchInputActionMap(actionInput.defaultActionMap);
        }

        enemyStatusPanelController.Show();
        partyStatusPanelController.Show();

        argumentSelectedPanel.Show();

        SetInputActiveState(true);
    }

    public void Close()
    {
        ToggleActionSelectionPanel(false);

        enemyStatusPanelController.Close();
        partyStatusPanelController.Close();

        battleInfoPanel.Close();
        argumentSelectedPanel.Close();
    }


    public void Initialize()
    {
        if (oneBattleRoundEventChannel != null)
            oneBattleRoundEventChannel.OnFullRoundEvent += OnOneRoundEnded;

        enemyStatusPanelController.GenerateEnemyList();
        partyStatusPanelController.GenerateBattlePartyList();

        battleInfoPanel.InitializePanel();

        Show();
    }

    public void Unload()
    {
        if (oneBattleRoundEventChannel != null)
            oneBattleRoundEventChannel.OnFullRoundEvent -= OnOneRoundEnded;
    }


    public void ToggleActionSelectionPanel(bool active)
    {
        if(active && !UINavigator.instance.hasPreviousWindows)
        {
            UINavigator.instance.OpenNewWindow(actionSelectionPanel);
            battleInfoPanel?.Show();
        }
        else if (active && UINavigator.instance.hasPreviousWindows)
        {
            UINavigator.instance.OpenPreviousWindow();
            battleInfoPanel?.Show();
        }
        else if(!active)
        {
            actionSelectionPanel?.Close();
        }
    }


    private void OnActionSelectionPanelReady(bool ready)
    {
        readyToReceiveActionInput = ready;

        //if (BattleInfoManager.activePlayers.Count > 1)
        //    partyStatusPanelController.ToggleInputLabels(ready);

        if (ready)
        {
            //BattleInfoManager.entityOnFocus.Guard(false);

            if (argumentSelectedPanel != null && argumentSelectedPanel.viewState != ViewState.Ready)
                argumentSelectedPanel.Show();
        }
    }

    #region Target selection 
    public void BeginTargetSelection()
    {
        BattleInfoManager.PrepareForTargetSelection();

        if (argumentSelectedPanel != null)
            argumentSelectedPanel.Close();

        if(targetSelectionPanel != null)
            UINavigator.instance.OpenNewWindow(targetSelectionPanel, targetSelectionPanel.content, targetSelectionPanel.lastTargetIndex);
    }

    public void CancelTargetSelection(bool isItemTargetSelected)
    {
        if (argumentSelectedPanel != null && !isItemTargetSelected)
            argumentSelectedPanel.Show();

        if (enemyStatusPanelController != null)
            enemyStatusPanelController.OnTargetSelectionCancel();
    }

    #endregion

    #region Show Panels

    public void BeginItemSelection()
    {
        if (itemSelectionPanel != null)
            UINavigator.instance.OpenNewWindow(itemSelectionPanel, itemSelectionPanel.content);
    }

    public void ShowForfeitPanel()
    {
        if (argumentSelectedPanel != null)
            argumentSelectedPanel.Close();

        if (forfeitPanel != null)
            UINavigator.instance.OpenNewWindow(forfeitPanel, forfeitPanel.content);
    }

    public void ShowAttackSkillSelectionPanel()
    {
        if (attackSkillSelectionPanel != null)
            UINavigator.instance.OpenNewWindow(attackSkillSelectionPanel, attackSkillSelectionPanel.content);
    }

    public void ShowSupportSkillSelectionPanel()
    {
        if (supportSkillSelectionPanel != null)
            UINavigator.instance.OpenNewWindow(supportSkillSelectionPanel, supportSkillSelectionPanel.content);
    }

    //public void ShowCounterSkillSelectionPanel()
    //{
    //    if(counterSkillSelectionPanel != null)
    //        UINavigator.instance.OpenNewWindow(counterSkillSelectionPanel, counterSkillSelectionPanel.content);
    //}

    public void ShowOrderPanel()
    {
        if(orderSelectionPanel != null)
            UINavigator.instance.OpenNewWindow(orderSelectionPanel, orderSelectionPanel.content);
    }

    public void ShowAnalyzeScreen()
    {
        if (argumentSelectedPanel != null)
            argumentSelectedPanel.Close();

        if (targetDetailsPanel != null)
            UINavigator.instance.OpenNewWindow(targetDetailsPanel);
    }

    //public void ShowSwapMemberPanel()
    //{
    //    if(swapMemberPanel != null)
    //        UINavigator.instance.OpenNewWindow(swapMemberPanel, swapMemberPanel.content);
    //}

    public void ShowPartyAttackPanel()
    {
        if(partyAttackPanel != null)
            UINavigator.instance.OpenNewWindow(partyAttackPanel);
    }

    public void ShowGameWin()
    {
        Close();

        if (gameWinPanelController != null)
            UINavigator.instance.OpenNewWindow(gameWinPanelController);
    }

    public void ShowGameOver()
    {
        Close();

        if (gameOverPanelController != null)
            UINavigator.instance.OpenNewWindow(gameOverPanelController, gameOverPanelController.content);
    }

    //public void BeginFaceOff(bool isBeginning, bool isFreeHit)
    //{
    //    if (faceOffPanel == null)
    //        return;

    //    if(isBeginning)
    //        faceOffPanel.Show(isFreeHit);
    //    else
    //        faceOffPanel.Close();
    //}

    #endregion

    #region Inputs
    public void OnCancelPressed(InputAction.CallbackContext context)
    {
        if(context.performed)
        {
            OnCancelButtonPressedEvent?.Invoke();
        }
    }

    public void OnRightTabPressed(InputAction.CallbackContext context)
    {
        if(context.performed)
        {
            OnRightTabPressedEvent?.Invoke();
        }
    }

    public void OnLeftTabPressed(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            OnLeftTabPressedEvent?.Invoke();
        }
    }

    public void OnRightTriggerPressed(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            OnRightTriggerPressedEvent?.Invoke();
        }
    }

    public void OnLeftTriggerPressed(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            OnLeftTriggerPressedEvent?.Invoke();
        }
    }

    public void OnInfoButtonPressed(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            OninfoButtonPressedEvent?.Invoke();
        }
    }

    public void OnNorthButtonPressed(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            OnNorthButtonPressedEvent?.Invoke();
        }
    }

    public void OnSubmitButtonPressed(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            OnSubmitButtonPressedEvent?.Invoke();
        }
    }

    public void TogglePause(InputAction.CallbackContext context)
    {
        if (BattlePhasesController.instance.CurrentPhaseEnum == BattlePhaseEnums.LongIdle)
            return;

        if (context.performed)
        {
            isPaused = !isPaused;

            if (isPaused)
            {
                pausedPanel.Show();
                SwitchInputActionMap(PausedInputMap);
            }
            else
            {
                pausedPanel.Close();
                SwitchInputActionMap(BattleActionsInputMap);
            }

            Time.timeScale = isPaused ? 0 : 1;

            BattlePhasesController.instance.elapsedAFKTime = 0f;
        }
    }

    public void SetInputActiveState(bool isPlayerTurn)
    {
        if (actionInput == null)
            return;

        switch (isPlayerTurn)
        {
            case false:
                actionInput.DeactivateInput();
                break;

            case true:
                actionInput.ActivateInput();
                break;
        }
    }


    public void SwitchInputActionMap(string actionMapName)
    {
        if (actionInput == null)
            return;

        actionInput.SwitchCurrentActionMap(actionMapName);
    }

    #endregion

    public void SwitchingMember()
    {
        if(UINavigator.instance.CurrentWindow is BattleUIPanel_ActionSelection)
        {
            actionSelectionPanel.PrepareSelectableActionLabels();
        }
        else
        {
            if (UINavigator.instance.CurrentWindow is BattleUIPanel_AttackSkillSelection)
            {
                attackSkillSelectionPanel.GenerateList(true);
            }
            else if (UINavigator.instance.CurrentWindow is BattleUIPanel_SupportSkillSelection)
            {
                supportSkillSelectionPanel.GenerateList(true);
            }
            else if (UINavigator.instance.CurrentWindow is BattleUIPanel_ItemSelection)
            {
                itemSelectionPanel.GenerateList(true);
            }
        }
    }

    public void OnOneRoundEnded(BattleEntity currentTurnEntity)
    {
        battleInfoPanel.OnOneRoundEnded();
        partyStatusPanelController.OnOneRoundEnded();
        enemyStatusPanelController.OnOneRoundEnded();

        argumentSelectedPanel.ClearList();
    }

    public void ToggleForFaceOff(bool active, bool toggleInfoPanel = false)
    {
        if (active)
        {
            partyStatusPanelController.Close();
            enemyStatusPanelController.Close();

            if(toggleInfoPanel)
                battleInfoPanel.Close();
        }
        else
        {
            partyStatusPanelController.Show();
            enemyStatusPanelController.Show();

            if(toggleInfoPanel)
                battleInfoPanel.Show();
        }
    }

    // Also called when 4 arguments have been selected in the FullArgumentsEvent in ArgumentSelectedPanel
    public void AskConfirmationForDebate()
    {
        SetInputActiveState(false);
     
        MessageBox.instance.Show(ConfirmationForDebate, MessageBoxButtons.YesNo, ChangePhaseToDebate);
    }

    private void ChangePhaseToDebate(MessageBoxResult result)
    {
        if (result == MessageBoxResult.Yes)
            OnBeginDebateEvent?.Invoke();
        else
        {
            UINavigator.instance.SetDefaultSelectedInRuntime();

            SetInputActiveState(true);
        }
    }

    public void BeginDebate()
    {
        SetInputActiveState(true);

        Close();
  
        if (debatePanel != null)
            UINavigator.instance.OpenNewWindow(debatePanel);

        UINavigator.instance.ClearPreviousWindows();
    }
}
