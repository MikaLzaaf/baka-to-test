using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Intelligensia.Battle
{
    public class BattleUISubPanel_HelperCharacterView : ViewController
    {
        [Header("Character View Stuff")]
        [SerializeField] private Transform cameraView = default;
        [SerializeField] private Transform cameraPivot = default;
        [SerializeField] private Vector3 pivotRotationOffset = default;
        [SerializeField] private Vector3 positionOffset = default;
        [SerializeField] private Vector3 rotationOffset = default;

        private Transform originalParent;
        private BattleEntity helper;
        private NonControllableEntity orderHelper;
        private ActorExpression actorExpression;

        public override void Close()
        {
            base.Close();

            cameraPivot.SetParent(originalParent);
        }

        public override void Show()
        {
            base.Show();

            if (helper != null)
            {
                if (helper.TryGetComponent(out Actor actor))
                {
                    actor.SetExpression(actorExpression, false);
                }
            }
        }

        protected override void OnViewReady()
        {
            base.OnViewReady();

          
            
            if(orderHelper != null)
            {
                if (orderHelper.TryGetComponent(out Actor actor))
                {
                    actor.SetExpression(actorExpression, false);
                }
            }
        }

        protected override void OnViewClosed()
        {
            if (orderHelper != null)
            {
                orderHelper.Death();
                orderHelper = null;
            }

            if(helper != null)
            {
                if (helper.TryGetComponent(out Actor actor))
                {
                    actor.SetExpression(ActorExpression.Idle, false);
                }
            }    

            base.OnViewClosed();
        }


        public void Show(BattleEntity helper, ActorExpression actorExpression)
        {
            if (originalParent == null)
                originalParent = cameraPivot.parent;

            this.helper = helper;
            this.actorExpression = actorExpression;

            if (helper == null)
                return;

            cameraPivot.SetParent(helper.transform);

            cameraPivot.localPosition = Vector3.zero;
            cameraPivot.localRotation = Quaternion.Euler(pivotRotationOffset);

            cameraView.localPosition = positionOffset;
            cameraView.localRotation = Quaternion.Euler(rotationOffset);

            Show();
        }

        public void Show(NonControllableEntity helperPrefab, ActorExpression actorExpression)
        {
            if (originalParent == null)
                originalParent = transform.parent;

            this.actorExpression = actorExpression;

            Show();

            StartCoroutine(InstantiatingHelper(helperPrefab));
        }

        private IEnumerator InstantiatingHelper(NonControllableEntity helperPrefab)
        {
            orderHelper = Instantiate(helperPrefab, null);
            orderHelper.transform.position = new Vector3(50f, 100f, 0f);

            yield return new WaitForEndOfFrame();

            cameraPivot.SetParent(orderHelper.transform);

            cameraPivot.localPosition = Vector3.zero;
            cameraPivot.localRotation = Quaternion.Euler(pivotRotationOffset);

            cameraView.localPosition = positionOffset;
            cameraView.localRotation = Quaternion.Euler(rotationOffset);
        }
    }
}

