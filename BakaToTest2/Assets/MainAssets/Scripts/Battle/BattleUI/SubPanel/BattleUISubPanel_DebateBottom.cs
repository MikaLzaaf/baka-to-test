using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using DG.Tweening;

namespace Intelligensia.Battle
{
    public class BattleUISubPanel_DebateBottom : MonoBehaviour
    {
        [SerializeField] private Color affectedColor = default;
        [SerializeField] private Color playerColor = default;
        [SerializeField] private Color enemyColor = default;

        [Header("Topic Subject")]
        [SerializeField] private TextMeshProUGUI topicSubjectNameText = default;
        [SerializeField] private Image topicSubjectIcon = default;
        [SerializeField] private Transform topicSubjectContainer = default;
        [SerializeField] private TextMeshProUGUI playerTopicSubjectModifierText = default;
        [SerializeField] private TextMeshProUGUI enemyTopicSubjectModifierText = default;
        [SerializeField] private float moveDistance = 50f;
        [SerializeField] private float moveDuration = 0.2f;
        [SerializeField] private float upDownInterval = 1f;

        [Header("Arguments Count")]
        [SerializeField] private GameObject[] playerArgumentsCountIcon = default;
        [SerializeField] private GameObject[] enemyArgumentsCountIcon = default;
        [SerializeField] private float smallJumpPower = 20f;
        [SerializeField] private float smallJumpDuration = 0.3f;

        [Header("Debate Point")]
        [SerializeField] private TextMeshProUGUI playerDebatePointText = default;
        [SerializeField] private TextMeshProUGUI enemyDebatePointText = default;
        [SerializeField] private Transform playerDebatePointTransform = default;
        [SerializeField] private Transform enemyDebatePointTransform = default;
        [SerializeField] private Vector3 rotatePower = Vector3.zero;
        [SerializeField] private float rotateDuration = 0.3f;

        [Header("DP Modifier")]
        [SerializeField] private GameObject playerDPModifierContainer = default;
        [SerializeField] private GameObject enemyDPModifierContainer = default;
        [SerializeField] private TextMeshProUGUI playerDPModifierText = default;
        [SerializeField] private TextMeshProUGUI enemyDPModifierText = default;
        [SerializeField] private float dpMoveDistance = 50f;
        [SerializeField] private float dpMoveDuration = 0.2f;



        public void SetTopicSubject()
        {
            if (topicSubjectNameText != null)
                topicSubjectNameText.text = BattleInfoManager.currentActiveSubject.ToString();

            if (topicSubjectIcon != null)
                topicSubjectIcon.sprite = IconLoader.GetSubjectIcon(BattleInfoManager.currentActiveSubject);
        }

        public void SetTopicSubjectModifier(bool isPlayerSide, int value)
        {
            if (isPlayerSide)
                playerTopicSubjectModifierText.text = (value + 1).ToString();
            else
                enemyTopicSubjectModifierText.text = (value + 1).ToString();
        }

        public void MoveTopicSubjectContainer(bool isPlayerSide)
        {
            Image targetBackground = isPlayerSide ? playerTopicSubjectModifierText.transform.parent.GetComponent<Image>() : 
                enemyTopicSubjectModifierText.transform.parent.GetComponent<Image>();
            targetBackground.color = affectedColor;

            Sequence upDownSequence = DOTween.Sequence();

            float originalYPos = topicSubjectContainer.localPosition.y;
            Tween up = topicSubjectContainer.DOLocalMoveY(originalYPos + moveDistance, moveDuration);
            Tween down = topicSubjectContainer.DOLocalMoveY(originalYPos, moveDuration);

            upDownSequence.Append(up).AppendInterval(upDownInterval).Append(down).OnComplete(delegate {
                targetBackground.color = isPlayerSide ? playerColor : enemyColor;
            });
        }

        public void SetDebatePoint(bool isPlayerPoint, int value)
        {
            if (isPlayerPoint)
                playerDebatePointText.text = value.ToString();
            else
                enemyDebatePointText.text = value.ToString();
        }

        public void ShowPointUpdateAnimation(bool isPlayerPoint)
        {
            if (isPlayerPoint)
                playerDebatePointTransform.DOLocalRotate(rotatePower, rotateDuration, RotateMode.FastBeyond360);
            else
                enemyDebatePointTransform.DOLocalRotate(rotatePower, rotateDuration, RotateMode.FastBeyond360);
        }

        public void SetDPModifier(bool isPlayerSide, int value)
        {
            if(isPlayerSide)
            {
                playerDPModifierText.text = value.ToString();
                playerDPModifierContainer.SetActive(value > 0);
            }
            else
            {
                enemyDPModifierText.text = value.ToString();
                enemyDPModifierContainer.SetActive(value > 0);
            }
        }

        public void MoveDPModifierContainer(bool isPlayerSide)
        {
            Transform container = isPlayerSide ? playerDPModifierContainer.transform : enemyDPModifierContainer.transform;
   
            Image targetBackground = isPlayerSide ? playerDPModifierText.transform.parent.GetComponent<Image>() : 
                enemyDPModifierText.transform.parent.GetComponent<Image>();

            targetBackground.color = affectedColor;

            Sequence upDownSequence = DOTween.Sequence();

            float originalYPos = container.localPosition.y;
            Tween up = container.DOLocalMoveY(originalYPos + dpMoveDistance, dpMoveDuration);
            Tween down = container.DOLocalMoveY(originalYPos, dpMoveDuration);

            upDownSequence.Append(up).AppendInterval(upDownInterval).Append(down).OnComplete(delegate {
                targetBackground.color = isPlayerSide ? playerColor : enemyColor;
            });
        }

        public void TogglePlayerArgumentIcons(bool active, int iconAmount)
        {
            for (int i = 0; i < iconAmount; i++)
            {
                playerArgumentsCountIcon[i].SetActive(active);
            }
        }

        public void CloseOnePlayerArgumentIcon()
        {
            for (int i = 0; i < playerArgumentsCountIcon.Length; i++)
            {
                if (playerArgumentsCountIcon[i].activeSelf)
                {
                    Vector3 bounceLocation = playerArgumentsCountIcon[i].transform.position;
                    playerArgumentsCountIcon[i].transform.DOJump(bounceLocation, smallJumpPower, 1, smallJumpDuration).OnComplete(
                        delegate
                        {
                            playerArgumentsCountIcon[i].SetActive(false);
                        });
                    break;
                }
            }
        }

        public void ToggleEnemyArgumentIcons(bool active, int iconAmount)
        {
            for (int i = 0; i < iconAmount; i++)
            {
                enemyArgumentsCountIcon[i].SetActive(active);
            }
        }

        public void CloseOneEnemyArgumentIcon()
        {
            for (int i = enemyArgumentsCountIcon.Length - 1; i >= 0; i--)
            {
                if (enemyArgumentsCountIcon[i].activeSelf)
                {
                    Vector3 bounceLocation = enemyArgumentsCountIcon[i].transform.position;
                    enemyArgumentsCountIcon[i].transform.DOJump(bounceLocation, smallJumpPower, 1, smallJumpDuration).OnComplete(
                        delegate
                        {
                            enemyArgumentsCountIcon[i].SetActive(false);
                        });
                    break;
                }
            }
        }
    }
}

