﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace Intelligensia.Battle
{
    public class BattleUISubPanel_ClassBattleStatus : ViewController
    {
        [Header("Class Name")]
        public TextMeshProUGUI playerClassNameText;
        public TextMeshProUGUI opponentClassNameText;

        [Header("Class Sequence")]
        [SerializeField] private Transform playerSequenceContainer = default;
        [SerializeField] private Transform opponentSequenceContainer = default;

        [Header("Prefab")]
        [SerializeField] private BattleUIItem_ClassBattleStatus listItemPrefab = default;


        private PlayerEntity playerLeader;
        private GenericEnemy enemyLeader;

        private List<BattleUIItem_ClassBattleStatus> playerClassmates;
        private List<BattleUIItem_ClassBattleStatus> opponentClassmates;


        private void OnDestroy()
        {
            if(BattleController.instance != null)
                BattleController.instance.RemoveCharacterInBattleEvent -= OnRemoveCharacterInBattleEvent;
        }

        public void InitializePanel()
        {
            BattleController.instance.RemoveCharacterInBattleEvent += OnRemoveCharacterInBattleEvent;

            GenerateList(true);
            GenerateList(false);

            playerLeader = BattleInfoManager.playerClassLeader;
            enemyLeader = BattleInfoManager.enemyClassLeader;

            if (enemyLeader == null || playerLeader == null)
                return;

            playerClassNameText.text = playerLeader.CharacterStatsBase.studentClass;
            opponentClassNameText.text = enemyLeader.CharacterStatsBase.studentClass;
        }

        private void OnRemoveCharacterInBattleEvent(BattleEntity character)
        {
            if (character == playerLeader || character == enemyLeader)
                return;

            if(character.entityTag == EntityTag.Player)
            {
                Destroy(playerClassmates[0].gameObject);
                playerClassmates.RemoveAt(0);
            }
            else if(character.entityTag == EntityTag.Enemy)
            {
                Destroy(opponentClassmates[0].gameObject);
                opponentClassmates.RemoveAt(0);
            }
        }

        private void GenerateList(bool isPlayerList)
        {
            if(isPlayerList)
            {
                if (playerClassmates != null && playerClassmates.Count > 0)
                {
                    if (playerClassmates.Count > 0)
                    {
                        for (int i = playerClassmates.Count - 1; i >= 0; i--)
                        {
                            Destroy(playerClassmates[i]);
                        }
                    }

                    playerClassmates.Clear();
                }
                else
                    playerClassmates = new List<BattleUIItem_ClassBattleStatus>();

                for (int i = 0; i < BattleInfoManager.reservePlayers.Count; i++)
                {
                    BattleUIItem_ClassBattleStatus listItem = Instantiate(listItemPrefab, playerSequenceContainer);

                    listItem.InitializeItem(BattleInfoManager.reservePlayers[i].characterName, true);

                    playerClassmates.Add(listItem);
                }
            }
            else
            {
                if (opponentClassmates != null && opponentClassmates.Count > 0)
                {
                    if (opponentClassmates.Count > 0)
                    {
                        for (int i = opponentClassmates.Count - 1; i >= 0; i--)
                        {
                            Destroy(opponentClassmates[i]);
                        }
                    }

                    opponentClassmates.Clear();
                }
                else
                    opponentClassmates = new List<BattleUIItem_ClassBattleStatus>();

                for (int i = 0; i < BattleInfoManager.reserveEnemies.Count; i++)
                {
                    BattleUIItem_ClassBattleStatus listItem = Instantiate(listItemPrefab, opponentSequenceContainer);

                    listItem.InitializeItem(BattleInfoManager.reserveEnemies[i].characterName, true);
     
                    opponentClassmates.Add(listItem);
                }
            }
        }
    }
}

