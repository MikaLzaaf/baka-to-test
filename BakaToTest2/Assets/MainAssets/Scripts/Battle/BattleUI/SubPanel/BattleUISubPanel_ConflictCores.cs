﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;


namespace Intelligensia.Battle
{
    public class BattleUISubPanel_ConflictCores : MonoBehaviour
    {
        public enum CoreSwitchesType
        {
            Undefined,
            NoneToAttack,
            NoneToDefend,
            AttackToNone,
            DefendToNone,
            AttackToDefend,
            DefendToAttack
        }


        public BattleUIItem_ConflictCore corePrefab;
        public Transform coresContainer;
        public ViewController buttonsView;
        public GameObject[] coreArrowLabels;

        [Header("Core Descriptors")]
        public CoreDescriptorView[] coreDescriptors;
        public ScrollRect descriptorScrollRect;
        public float scrollingDuration = 1f;
        public float delayBeforeScrollClose = 0.5f;

        public event Action<CoreSwitchesType> CoreSwitchesEvent;

        private List<BattleUIItem_ConflictCore> listItems;
        private BattleUIPanel_FaceOff.FaceOffFactor lastFactor;

        private Dictionary<BattleUIPanel_FaceOff.FaceOffFactor, CoreDescriptorView> _descriptorsLookup;
        private Dictionary<BattleUIPanel_FaceOff.FaceOffFactor, CoreDescriptorView> descriptorsLookup
        {
            get
            {
                if(_descriptorsLookup == null)
                {
                    _descriptorsLookup = new Dictionary<BattleUIPanel_FaceOff.FaceOffFactor, CoreDescriptorView>();

                    if(coreDescriptors.Length > 0)
                    {
                        for(int i = 0; i < coreDescriptors.Length; i++)
                        {
                            _descriptorsLookup.Add(coreDescriptors[i].faceOffFactor, coreDescriptors[i]);
                        }
                    }
                }

                return _descriptorsLookup;
            }
        }

        public BattleUIItem_ConflictCore inspectedListItem { get; set; }



        public void InitializePanel()
        {
            if(listItems != null && listItems.Count > 0)
            {
                for(int i = 0; i < listItems.Count; i++)
                {
                    Destroy(listItems[i].gameObject);
                }

                listItems.Clear();
            }
            else
            {
                listItems = new List<BattleUIItem_ConflictCore>();
            }

            lastFactor = BattleUIPanel_FaceOff.FaceOffFactor.Undefined;

            ToggleButtons(false);

            CloseAllDescriptors();
        }

        public void AddCore(BattleUIPanel_FaceOff.FaceOffFactor factor)
        {
            BattleUIItem_ConflictCore itemList = Instantiate(corePrefab, coresContainer);
            itemList.InitializeItem(this);

            listItems.Add(itemList);

            if(factor != lastFactor)
            {
                ShowCoreDescription(factor);

                lastFactor = factor;
            }
        }

        public int RemoveCore(int removeAmount)
        {
            int removed = 0;

            for(int i = listItems.Count - 1; i >= 0; i--)
            {
                if (removeAmount == 0)
                    break;

                Destroy(listItems[i].gameObject);
                listItems.RemoveAt(i);

                removeAmount -= 1;
                removed += 1;
            }

            return removed;
        }

        public void BeginSettingCores()
        {
            ToggleButtons(true);

            BattleUIManager.instance.OnRightTabPressedEvent += SetCoreToAttackCore;
            BattleUIManager.instance.OnLeftTabPressedEvent += SetCoreToDefendCore;
            BattleUIManager.instance.OnCancelButtonPressedEvent += SetCoreToNone;

            UINavigator.instance.SetupNavigation(listItems);
            UINavigator.instance.SetDefaultSelectedInRuntime(listItems[0].gameObject);
        }

        public void FinishedSettingCores()
        {
            BattleUIManager.instance.OnRightTabPressedEvent -= SetCoreToAttackCore;
            BattleUIManager.instance.OnLeftTabPressedEvent -= SetCoreToDefendCore;
            BattleUIManager.instance.OnCancelButtonPressedEvent -= SetCoreToNone;

            UINavigator.instance.SetDefaultSelectedInRuntime(null);

            ToggleButtons(false);
        }

        public void ToggleButtons(bool active)
        {
            if (active)
                buttonsView?.Show();
            else
                buttonsView?.Close();

            if (coreArrowLabels == null)
                return;

            for(int i = 0; i < coreArrowLabels.Length; i++)
            {
                coreArrowLabels[i].SetActive(active);
            }
        }

        public void SetEnemyCores()
        {
            for(int i = 0; i < listItems.Count; i++)
            {
                // For now randomly set the cores
                if(UnityEngine.Random.value > 0.5)
                {
                    listItems[i].ShowCoreIcon(1);
                    CoreSwitchesEvent?.Invoke(CoreSwitchesType.NoneToDefend);
                }
                else
                {
                    listItems[i].ShowCoreIcon(2);
                    CoreSwitchesEvent?.Invoke(CoreSwitchesType.NoneToAttack);
                }
            }
        }

        private void CloseAllDescriptors(bool closeImmediately = true)
        {
            for(int i = 0; i < coreDescriptors.Length; i++)
            {
                if(closeImmediately)
                    coreDescriptors[i].CloseImmediately();
                else
                    coreDescriptors[i].Close();
            }
        }

        private void ShowCoreDescription(BattleUIPanel_FaceOff.FaceOffFactor faceOffFactor)
        {
            descriptorsLookup.TryGetValue(faceOffFactor, out CoreDescriptorView descriptor);

            if(descriptor != null)
            {
                descriptor.Show();
            }
        }

        public void BeginScrollingCoreDescriptor()
        {
            if (descriptorScrollRect == null)
                return;

            StartCoroutine(ScrollingDescriptor());
        }

        private IEnumerator ScrollingDescriptor()
        {
            float elapsedTime = 0f;

            while(elapsedTime < scrollingDuration)
            {
                elapsedTime += UnityEngine.Time.deltaTime;

                descriptorScrollRect.verticalNormalizedPosition = elapsedTime / scrollingDuration;

                yield return null;
            }

            descriptorScrollRect.verticalNormalizedPosition = 1;

            yield return new WaitForSeconds(delayBeforeScrollClose);

            CloseAllDescriptors(false);
        }

        #region Inputs

        public void SetCoreToNone()
        {
            if (inspectedListItem == null || inspectedListItem.coreType == 0)
                return;

            CoreSwitchesType switchesType = CoreSwitchesType.Undefined;

            if (inspectedListItem.coreType == 2) // previously attack core
                switchesType = CoreSwitchesType.AttackToNone;
            else if(inspectedListItem.coreType == 1)
                switchesType = CoreSwitchesType.DefendToNone;

            inspectedListItem.ShowCoreIcon(0);

            CoreSwitchesEvent?.Invoke(switchesType);
        }

        public void SetCoreToDefendCore()
        {
            if (inspectedListItem == null || inspectedListItem.coreType == 1)
                return;

            CoreSwitchesType switchesType = CoreSwitchesType.NoneToDefend;

            if (inspectedListItem.coreType == 2) // previously attack core
                switchesType = CoreSwitchesType.AttackToDefend;

            inspectedListItem.ShowCoreIcon(1);

            CoreSwitchesEvent?.Invoke(switchesType);
        }

        public void SetCoreToAttackCore()
        {
            if (inspectedListItem == null || inspectedListItem.coreType == 2)
                return;

            CoreSwitchesType switchesType = CoreSwitchesType.NoneToAttack;

            if (inspectedListItem.coreType == 1) // previously defend core
                switchesType = CoreSwitchesType.DefendToAttack;

            inspectedListItem.ShowCoreIcon(2);

            CoreSwitchesEvent?.Invoke(switchesType);
        }

        #endregion
    }
}

