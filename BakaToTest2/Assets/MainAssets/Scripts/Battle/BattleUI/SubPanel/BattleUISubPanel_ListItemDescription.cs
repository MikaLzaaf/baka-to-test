﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

namespace Intelligensia.Battle
{
    public class BattleUISubPanel_ListItemDescription : MonoBehaviour
    {
        [Header("UI")]
        [SerializeField] private Image itemIcon = default;
        [SerializeField] private TextMeshProUGUI itemTitleText = default;
        [SerializeField] private TextMeshProUGUI targetRangeText = default;
        [SerializeField] private TextMeshProUGUI itemRoleText = default;
        [SerializeField] private TextMeshProUGUI itemDescriptionText = default;
        [SerializeField] private TextMeshProUGUI cooldownText = default;

        [SerializeField] private string cooldownTextFormat = "Cooldown Duration : {0} turn(s)";

        [Header("Debate Effect")]
        [SerializeField] private ViewController debateEffectView = default;
        [SerializeField] private TextMeshProUGUI debateEffectText = default;

        [Header("Order Description")]
        public TextMeshProUGUI orderHolderText;


        private const string OrderCostSuffix = " Stress!";
        private const string OrderDurationSuffix = " Turn(s)";

        private const string OrderMidFormat = "{0} {1}";

        private const string ArgumentSuffix = " Argument";


        public void ShowDescription(InBattleSkill selectedSkill)
        {
            if (selectedSkill == null)
            {
                ToggleEmptyDescription(true);
                return;
            }

            ToggleEmptyDescription(false);

            itemIcon.sprite = IconLoader.GetSubjectIcon(selectedSkill.GetSubjectBased());

            itemTitleText.text = selectedSkill.GetSkillName();
            targetRangeText.text = ListItemDescriptionInfoManager.GetTargetAmountDescription(selectedSkill.GetSkillAim());
            itemRoleText.text = selectedSkill.GetRoleInDebate() + ArgumentSuffix;
            itemDescriptionText.text = selectedSkill.GetSkillDescription();
            cooldownText.text = string.Format(cooldownTextFormat, selectedSkill.GetCooldownDuration());

            if(selectedSkill.isEnhancedArgument)
            {
                debateEffectText.text = selectedSkill.GetDebateEffectDescription(); 
                debateEffectView.Show();
            }
            else
            {
                debateEffectText.text = "";
                debateEffectView.Close();
            }
        }

        public void ShowDescription(BattleOrder selectedOrder)
        {
            if (selectedOrder == null)
            {
                ToggleEmptyDescription(true);
                return;
            }

            ToggleEmptyDescription(false);

            itemIcon.sprite = selectedOrder.orderIcon;

            itemTitleText.text = selectedOrder.orderName;
            targetRangeText.text = string.Format(OrderMidFormat, selectedOrder.orderCost, OrderCostSuffix);
            itemRoleText.text = string.Format(OrderMidFormat, selectedOrder.GetDurationString(), OrderDurationSuffix);
            orderHolderText.text = selectedOrder.orderHolder == null ? "MIA" : selectedOrder.orderHolder.GetName();
            itemDescriptionText.text = selectedOrder.orderDescription;
        }

        public void ShowDescription(Item selectedItem)
        {
            if (selectedItem == null)
            {
                ToggleEmptyDescription(true);
                return;
            }

            ToggleEmptyDescription(false);

            itemIcon.sprite = IconLoader.GetItemIcon(selectedItem.itemValue.ItemIcon);

            itemTitleText.text = selectedItem.itemValue.ItemName;
            targetRangeText.text = ListItemDescriptionInfoManager.GetTargetAmountDescription(selectedItem.itemValue.GetAim());
            itemDescriptionText.text = selectedItem.itemValue.Description;
            itemRoleText.enabled = false;
        }

        private void ToggleEmptyDescription(bool isEmpty)
        {
            if(itemIcon != null)
                itemIcon.enabled = !isEmpty;

            if(itemTitleText != null)
                itemTitleText.enabled = !isEmpty;

            if(targetRangeText != null)
                targetRangeText.enabled = !isEmpty;

            if (itemRoleText != null)
                itemRoleText.enabled = !isEmpty;

            if (itemDescriptionText != null)
                itemDescriptionText.enabled = !isEmpty;

            if(cooldownText != null)
                cooldownText.enabled = !isEmpty;

            if(orderHolderText != null)
                orderHolderText.enabled = !isEmpty;

            if (isEmpty && debateEffectView != null)
            {
                debateEffectView.Close();
                debateEffectText.text = "";
            }
        }
    }
}

