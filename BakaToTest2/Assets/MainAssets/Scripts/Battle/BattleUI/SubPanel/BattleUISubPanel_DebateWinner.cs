using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace Intelligensia.Battle
{
    public class BattleUISubPanel_DebateWinner : ViewController
    {

        [Header("Winner View")]
        [SerializeField] private Image pointBorder = default;
        [SerializeField] private Image winnerNameFrame = default;
        [SerializeField] private Color playerColor = default;
        [SerializeField] private Color enemyColor = default;
        [SerializeField] private Color drawColor = default;
        [SerializeField] private TextMeshProUGUI winnerNameText = default;
        [SerializeField] private TextMeshProUGUI winnerPointText = default;


        private const string PlayerWinner = "PLAYER";
        private const string EnemyWinner = "OPPONENT";
        private const string Draw = "DRAW";

        public void Show(int result, int winnerPoint)
        {
            if(result == 0) // Draw
            {
                BattleInfoManager.debateWinner = EntityTag.Untagged;

                winnerNameText.text = Draw;

                pointBorder.color = drawColor;
                winnerNameFrame.color = drawColor;

                winnerPointText.text = "";
            }
            else // Player or Opponent wins
            {
                BattleInfoManager.debateWinner = result == 1 ? EntityTag.Player : EntityTag.Enemy;
                winnerNameText.text = result == 1 ? PlayerWinner : EnemyWinner;

                pointBorder.color = result == 1 ? playerColor : enemyColor;
                winnerNameFrame.color = result == 1 ? playerColor : enemyColor;

                winnerPointText.text = winnerPoint.ToString();
            }
            
            Show();
        }
    }

}

