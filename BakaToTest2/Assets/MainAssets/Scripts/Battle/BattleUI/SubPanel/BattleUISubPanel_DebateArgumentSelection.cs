using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.Events;

namespace Intelligensia.Battle
{
    public class BattleUISubPanel_DebateArgumentSelection : UIViewController
    {
        [Header("List Items")]
        [SerializeField] BattleUIItem_ArgumentSelected listItemPrefab = default;
        [SerializeField] private float listItemsTransitionInterval = 0.05f;
        [SerializeField] private float listItemsTransitionDuration = 0.05f;
        [SerializeField] private float listItemsTransitionSlideOffset = -100;

        [Header("Others")]
        [SerializeField] private BattleUISubPanel_ListItemDescription listItemDescriptionPanel = default;

        public UnityEvent<BattleAction> OnListItemClickedEvent;

        private List<BattleUIItem_ArgumentSelected> listItems;
        private List<BattleAction> argumentSelected;

        private Sequence listItemsTransitionSequence;


        public override void Show()
        {
            base.Show();

            GenerateList();
        }

        private void ClearList()
        {
            if (listItems != null && listItems.Count > 0)
            {
                if (listItems.Count > 0)
                {
                    for (int i = listItems.Count - 1; i >= 0; i--)
                    {
                        Destroy(listItems[i].gameObject);
                    }
                }

                listItems.Clear();
            }
            else
            {
                listItems = new List<BattleUIItem_ArgumentSelected>();
            }
        }

        private void GenerateList()
        {
            ClearList();

            if (argumentSelected == null || argumentSelected.Count == 0)
                argumentSelected = new List<BattleAction>(BattleInfoManager.argumentActionsRegistered);

            if (listItemsTransitionSequence != null)
                listItemsTransitionSequence.Kill();

            listItemsTransitionSequence = DOTween.Sequence();

            for (int i = 0; i < argumentSelected.Count; i++)
            {
                BattleUIItem_ArgumentSelected listItem = Instantiate(listItemPrefab);
                listItems.Add(listItem);

                listItem.transform.SetParent(content, false);

                listItem.InitializeItem(argumentSelected[i], this);

                listItem.onClick.AddListener(delegate
                {
                    OnListItemClick(listItem);
                });

                var item = listItem;

                item.canvasGroup.alpha = 0;

                listItemsTransitionSequence.Insert(i * listItemsTransitionInterval, item.canvasGroup.DOFade(1, listItemsTransitionDuration));
                listItemsTransitionSequence.Insert(i * listItemsTransitionInterval,
                    item.transform.DOLocalMoveX(item.transform.localPosition.x + listItemsTransitionSlideOffset, listItemsTransitionDuration).From());
            }

            listItemsTransitionSequence.OnComplete(delegate
            {
                UINavigator.instance.SetupVerticalNavigation(listItems, true);

                if (listItems.Count > 0)
                {
                    UINavigator.instance.SetDefaultSelectedInRuntime(listItems[0].gameObject);
                }
                else
                    UpdateSkillDescription(null);
            });
        }

        public void UpdateSkillDescription(InBattleSkill skillSelected)
        {
            if (listItemDescriptionPanel != null)
                listItemDescriptionPanel.ShowDescription(skillSelected);
        }

        private void OnListItemClick(BattleUIItem_ArgumentSelected listItem)
        {
            argumentSelected.Remove(listItem.battleAction);

            OnListItemClickedEvent?.Invoke(listItem.battleAction);

            Close();
        }
    }

}
