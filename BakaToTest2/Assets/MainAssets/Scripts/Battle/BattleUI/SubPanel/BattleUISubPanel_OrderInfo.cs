﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace Intelligensia.Battle
{
    public class BattleUISubPanel_OrderInfo : ViewController
    {
        public TextMeshProUGUI orderNameText;
        public TextMeshProUGUI orderDescriptionText;
        public TextMeshProUGUI orderDurationText;

        private bool hasOrderToDisplay = false;

        private const string InfinityCodeSymbol = "\u221E";

        private int activeDuration = 0;
        private int ActiveDuration
        {
            get
            {
                return activeDuration;
            }
            set
            {
                activeDuration = value;
       
                orderDurationText.text = activeDuration < 0 ? InfinityCodeSymbol : activeDuration.ToString();
            }
        }


        public void Show(BattleOrder battleOrder)
        {
            if (battleOrder == null)
                return;

            ActiveDuration = battleOrder.orderDuration;

            orderNameText.text = battleOrder.orderName;
            orderDescriptionText.text = battleOrder.orderDescription;

            hasOrderToDisplay = true;

            Show();
        }




        public void UpdateDuration()
        {
            if (!hasOrderToDisplay)
                return;

            if(ActiveDuration > 0)
                ActiveDuration -= 1;

            if(ActiveDuration == 0)
            {
                hasOrderToDisplay = false;

                BattleInfoManager.currentCombatModifier = null;

                Close();
            }
        }
    }
}

