﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

namespace Intelligensia.Battle
{
    public class BattleUISubPanel_ActiveSubjectInfo : ViewController
    {
        public TextMeshProUGUI subjectNameText;
        public Image subjectIcon;


        public void UpdateSubjectInfo(Subjects activeSubject)
        {
            if (subjectNameText != null)
            {
                subjectNameText.text = StringHelper.SplitByCapitalizeFirstLetter(activeSubject.ToString());
            }

            if (subjectIcon != null)
            {
                subjectIcon.sprite = IconLoader.GetSubjectIcon(activeSubject);
            }
        }
    }
}

