﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Intelligensia.Battle
{
    public class BattleUISubPanel_ActionSelectionLabel : MonoBehaviour
    {
        public BattleSelectionType actionType;
        public CanvasGroup canvasGroup;


        public void ToggleInteractability(bool active)
        {
            if(canvasGroup != null)
            {
                canvasGroup.interactable = active;

                canvasGroup.alpha = active ? 1f : 0.5f;
            }
        }

        public bool IsSelectable
        {
            get
            {
                if (canvasGroup == null)
                    return false;

                return canvasGroup.interactable;
            }
        }
    }
}

