﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

namespace Intelligensia.Battle
{
    public class EnemyStatusPanel : MonoBehaviour
    {
        public TextMeshProUGUI subjectGradeText;

        [Header("Fatigue Display")]
        public FillIndicatorLerper fillIndicatorLerper;
        public Image subjectIcon;
        public TextMeshProUGUI livesAmountText;
        public GameObject breakIndicator;
        public Image battleActionIcon;

        [Header("Decision Icon")]
        public Image decisionImage;
        //public float jumpPower = 20f;
        //public int jumpAmount = 1;
        //public float jumpDuration = 0.3f;

        [Header("Info Effect Active")]
        public TextMeshProUGUI infoEffectActiveAmountText;

        [Header("Display Containers")]
        public GameObject fullDetailsDisplay;
        public GameObject decisionIndicator;

        //[Header("Attack Type Display")]
        //public GameObject freeHitLabel;
        //public GameObject conflictLabel;

        [Header("Knockout Chance")]
        [SerializeField] private TextMeshProUGUI knockoutChanceAmountText = default;
        [SerializeField] private GameObject knockoutChanceIndicator = default;

        //private BattleEntity lastEntityOnTurn;
        private GenericEnemy enemy;
        //private Sequence actionSelectedIconSequence;

        private Subjects lastActiveSubject = Subjects.Biology;

        private bool isKnockedOut = false;


        private void OnDestroy()
        {
            if (enemy != null)
            {
                enemy.FatigueUpdateEvent -= OnFatigueChanged;
                //enemy.SubjectKnockoutEvent -= OnKnockoutEvent;
                enemy.ActionSelectedUpdateEvent -= UpdateBattleActionIcon;
            }
        }

        public void ShowDecision()
        {
            ToggleFullDetails(false);

            gameObject.SetActive(true);
        }

        public void Show()
        {
            ToggleFullDetails(true);

            gameObject.SetActive(true);

            InitializeGrade();

            //ShowAttackType();

            UpdateSubjectIcon(BattleInfoManager.currentActiveSubject);
            //UpdateLivesAmount();
            UpdateInfoEffectActiveAmount();
            UpdateKnockoutChanceIndicator(enemy.IsOverfatigued && !isKnockedOut);
        }


        public void Close()
        {
            gameObject.SetActive(false);
        }

        public void Initialize(GenericEnemy enemy)
        {
            gameObject.SetActive(true);

            this.enemy = enemy;

            enemy.FatigueUpdateEvent += OnFatigueChanged;
            //enemy.SubjectKnockoutEvent += OnKnockoutEvent;
            enemy.ActionSelectedUpdateEvent += UpdateBattleActionIcon;

            UpdateFillIndicator(enemy.fatigue / enemy.stamina);

            UpdateBattleActionIcon(enemy.actionSelected);

            gameObject.SetActive(false);
        }


        private void InitializeGrade()
        {
            // TODO Need to add Active Subject Controller later
            if (lastActiveSubject != BattleInfoManager.currentActiveSubject)
            {
                lastActiveSubject = BattleInfoManager.currentActiveSubject;
            }

            //if (lastEntityOnTurn == BattleInfoManager.entityOnTurn)
            //{
            //    return;
            //}

            UpdateSubjectGrade();

            //string confidenceGrade = ConfidenceInfoManager.DetermineConfidenceGrade(this.enemy,
            //    BattleInfoManager.entityOnTurn, BattleInfoManager.currentActiveSubject, false);

            //UpdateConfidenceIndicator(confidenceGrade);

            //lastEntityOnTurn = BattleInfoManager.entityOnTurn;
        }

        //private void ShowAttackType()
        //{
        //    BattleEntity attacker = BattleInfoManager.playerHighlighted;

        //    bool isFreeHitAttack = BattleInfoManager.CanFreeHit(attacker, enemy);

        //    if (freeHitLabel != null)
        //    {
        //        freeHitLabel.SetActive(isFreeHitAttack);
        //    }

        //    if (conflictLabel != null)
        //    {
        //        conflictLabel.SetActive(!isFreeHitAttack);
        //    }
        //}

        private void UpdateFillIndicator(float targetAmount)
        {
            if (fillIndicatorLerper == null)
                return;

            if (enemy.isDestroyed)
            {
                fillIndicatorLerper.ResetIndicator();
            }
            else
            {
                fillIndicatorLerper.StartLerp(targetAmount);
            }
        }

        private void OnFatigueChanged(float fatigue)
        {
            UpdateFillIndicator(enemy.fatigue / enemy.stamina);
        }

        //private void OnKnockoutEvent()
        //{
        //    //UpdateLivesAmount();

        //    ToggleSubjectBreakIndicator(true);
        //}

        //private void UpdateLivesAmount()
        //{
        //    livesAmountText.text = enemy.currentLivesAmount.ToString();
        //}

        private void UpdateSubjectGrade()
        {
            SubjectStats subject = enemy.GetSubjectStats(BattleInfoManager.currentActiveSubject);

            if (subjectGradeText != null)
            {
                subjectGradeText.text = subject.GetGrade();
            }
        }

        private void UpdateSubjectIcon(Subjects currentSubject)
        {
            if (subjectIcon == null)
                return;

            subjectIcon.sprite = IconLoader.GetSubjectIcon(currentSubject);

            if (enemy != null)
            {
                ToggleSubjectBreakIndicator(enemy.IsKnockoutOnSubject(currentSubject));
            }
        }

        private void ToggleSubjectBreakIndicator(bool active)
        {
            if (breakIndicator != null)
            {
                breakIndicator.SetActive(active);
            }

            isKnockedOut = active;
        }

        private void UpdateBattleActionIcon(BattleSelectionType actionSelected)
        {
            if (battleActionIcon != null)
            {
                battleActionIcon.sprite = IconLoader.GetBattleActionIcon(actionSelected.ToString());
            }

            if(decisionImage != null)
            {
                decisionImage.sprite = IconLoader.GetBattleActionIcon(actionSelected.ToString());

                //if (actionSelectedIconSequence != null)
                //{
                //    if (actionSelectedIconSequence.IsPlaying())
                //        return;
                //}

                //actionSelectedIconSequence = DOTween.Sequence();

                //Vector3 bounceLocation = decisionImage.transform.parent.localPosition;
                //actionSelectedIconSequence.Append(decisionImage.transform.parent.DOLocalJump(bounceLocation, jumpPower, jumpAmount, jumpDuration));
            }
        }

        private void ToggleFullDetails(bool showFullDetails)
        {
            if(fullDetailsDisplay != null)
            {
                fullDetailsDisplay.SetActive(showFullDetails);
            }

            if(decisionIndicator != null)
            {
                decisionIndicator.SetActive(!showFullDetails);
            }
        }

        private void UpdateInfoEffectActiveAmount()
        {
            if(infoEffectActiveAmountText != null)
            {
                infoEffectActiveAmountText.text = enemy.characterStatsData.GetActiveInfoEffectCount().ToString();
            }
        }

        private void UpdateKnockoutChanceIndicator(bool active)
        {
            if(knockoutChanceIndicator != null)
            {
                knockoutChanceIndicator.SetActive(active);
            }

            knockoutChanceAmountText.text = active ? DamageCalculator.GetEffectiveCritRate(BattleInfoManager.entityOnFocus, enemy).ToString() : "0";
        }
    }
}

