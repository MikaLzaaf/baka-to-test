using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Intelligensia.Battle.Targeting;

namespace Intelligensia.Battle
{
    public class TargetingBehavior : MonoBehaviour
    {

        [SerializeField] TargetingInfo[] targetingInfos = default;

        [SerializeField, Range(0f, 100f)] float beginToHealThreshold = 50f;

        public TargetingScore currentTargetingScore { get; private set; }
        //private ActionScore lastActionScore;


        public void SetHighestPriorityAction(BattleEntity user, BattleEntity[] opponents, BattleEntity[] allies)
        {
            for(int i = 0; i < targetingInfos.Length; i++)
            {
                currentTargetingScore = targetingInfos[i].GetScoredAction(user, opponents, allies);

                if (currentTargetingScore.actionName != ActionName.Undefined)
                    break;
            }
        }


        public int CalculateTargetScore(BattleEntity user, BattleEntity[] targets, BattleEntity[] allies)
        {
            //lastActionScore = currentTargetingScore;

            int targetScore = 0;
            int currentScore = 0;

            //List<ActionScore> actionScores = new List<ActionScore>();
            //int actionScore = 0;

            bool userFatigueIsMoreThanHalf = (user.fatigue / user.stamina) > 0.5f;
            string userIntellectual = GradeChecker.ConvertGradeToResult(user.GetOverallGrade());

            PersonalityType userPersonality = user.CharacterStatsBase.personality;

            if (userPersonality == PersonalityType.Aggressive)
            {
                // User fatigue < 50% -> Attack opponent with highest stamina
                // User fatigue > 50% -> Attack opponent with lowest stamina
                // Always uses highest SP argument that is available
                // Never use support arguments or guard

                if (userIntellectual == GradeChecker.GreatResult)
                {
                    // Begins by checking if any of the allies need healing
                    for (int i = 0; i < allies.Length; i++)
                    {
                        if (allies[i].fatigue >= beginToHealThreshold)
                        {
                            Item[] consumables = null;

                            if (user is GenericEnemy)
                            {
                                GenericEnemy enemy = (GenericEnemy)user;
                                consumables = enemy.GetConsumables();
                            }
                            else
                                consumables = PlayerInventoryManager.instance.GetItemsByCategory(ItemCategory.Consumable);

                            if (consumables == null)
                                continue;

                            for (int j = 0; j < consumables.Length; j++)
                            {
                                ConsumableItemBaseValue consumableItemBase = (ConsumableItemBaseValue)consumables[j].itemValue;

                                if (consumableItemBase == null)
                                    continue;

                                if (consumableItemBase.CanCureFatigue())
                                {
                                    currentScore = 4;

                                    if (allies[i] == user)
                                        currentScore = 6;

                                    TargetingScore score = new TargetingScore(ActionName.HealTarget, currentScore, allies[i], consumables[j]);
                                    UpdateActionScore(score);
                                    break;
                                }
                            }
                        }
                    }

                    BattleEntity targetEntity = targets[0];

                    // Then check the opponents for attacking
                    for (int i = 0; i < targets.Length; i++)
                    {
                        if (userFatigueIsMoreThanHalf)
                        {
                            if (targets[i].stamina < targetEntity.stamina)
                                targetEntity = targets[i];
                        }
                        else
                        {
                            if (targets[i].stamina > targetEntity.stamina)
                                targetEntity = targets[i];
                        }
                    }

                    var offenses = user.GetAvailableOffense();

                    if (offenses.Length > 0)
                    {
                        InBattleSkill offense = offenses[0];

                        for (int i = 0; i < offenses.Length; i++)
                        {
                            if (offenses[i].GetSkillPoint() > offense.GetSkillPoint())
                                offense = offenses[i];
                        }

                        currentScore = 4;

                        TargetingScore score = new TargetingScore(ActionName.AttackTarget, currentScore, targetEntity, offense, BattleSelectionType.Attack);
                        UpdateActionScore(score);
                    }
                }
                else
                {
                    BattleEntity targetEntity = targets[0];

                    // Then check the opponents for attacking
                    for (int i = 0; i < targets.Length; i++)
                    {
                        if (userFatigueIsMoreThanHalf)
                        {
                            if (targets[i].stamina < targetEntity.stamina)
                                targetEntity = targets[i];
                        }
                        else
                        {
                            if (targets[i].stamina > targetEntity.stamina)
                                targetEntity = targets[i];
                        }
                    }

                    var offenses = user.GetAvailableOffense();

                    if (offenses.Length > 0)
                    {
                        InBattleSkill offense = offenses[0];

                        for (int i = 0; i < offenses.Length; i++)
                        {
                            if (offenses[i].GetSkillPoint() > offense.GetSkillPoint())
                                offense = offenses[i];
                        }

                        currentScore = 4;

                        TargetingScore score = new TargetingScore(ActionName.AttackTarget, currentScore, targetEntity, offense, BattleSelectionType.Attack);
                        UpdateActionScore(score);
                    }
                }
            }

            return targetScore;
        }

        private void UpdateActionScore(TargetingScore newScore)
        {
            // To discourage the usage of similar action type
            //if (lastActionScore.actionName == newScore.actionName)
            //    newScore.score -= 1;

            if (currentTargetingScore.score < newScore.score)
            {
                currentTargetingScore = newScore;
            }
        }

        public enum ActionName
        {
            Undefined,
            HealTarget, // Healing can only be done via items
            CureTargetUsingItem,
            CureTargetUsingSkill,
            AttackTarget
        }


        public struct TargetingScore
        {
            public ActionName actionName;
            public BattleSelectionType selectionType;
            public int score;
            public BattleEntity target;
            public Item consumable;
            public InBattleSkill skill;

            public TargetingScore(int score)
            {
                actionName = ActionName.Undefined;
                this.score = score;
                target = null;
                skill = null;

                consumable = null;
                selectionType = BattleSelectionType.Null;
            }

            public TargetingScore(ActionName actionName, int score, BattleEntity target, InBattleSkill skill, BattleSelectionType selectionType)
            {
                this.actionName = actionName;
                this.score = score;
                this.target = target;
                this.skill = skill;
                this.selectionType = selectionType;

                consumable = null;
            }

            public TargetingScore(ActionName actionName, int score, BattleEntity target, Item consumable)
            {
                this.actionName = actionName;
                this.score = score;
                this.target = target;
                this.consumable = consumable;

                selectionType = BattleSelectionType.Item;
                skill = null;
            }
        }
    }
}

