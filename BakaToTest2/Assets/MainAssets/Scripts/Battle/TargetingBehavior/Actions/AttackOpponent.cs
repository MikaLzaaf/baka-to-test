using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Intelligensia.Battle.Targeting
{
    [CreateAssetMenu(fileName = "Attack Opponent", menuName = "ScriptableObject/Targeting Action/Attack Opponent")]
    public class AttackOpponent : TargetingActionSO
    {
        [SerializeField] bool attackLowestStaminaOpponent = false;
        [SerializeField] bool attackHighestStaminaOpponent = false;


        public override TargetingBehavior.TargetingScore GetHighestScoredAction(BattleEntity user, BattleEntity target)
        {
            BattleAction characterAction = BattleInfoManager.GetFirstCharacterAction(user.entityTag == EntityTag.Player);

            if (characterAction != null) // Attack can only be used together with support actions.
            {
                if (characterAction.selectionType == BattleSelectionType.Item || characterAction.selectionType == BattleSelectionType.Counter)
                   return new TargetingBehavior.TargetingScore(0);
            }

            int currentScore = 0;

            BattleEntity targetEntity = null;

            if (target != null)
                targetEntity = target;
            else
            {
                if (user.entityTag == EntityTag.Player)
                {
                    targetEntity = BattleInfoManager.activeEnemies[0];

                    for (int i = 0; i < BattleInfoManager.activeEnemies.Count; i++)
                    {
                        if (attackLowestStaminaOpponent)
                        {
                            if (BattleInfoManager.activeEnemies[i].stamina < targetEntity.stamina)
                                targetEntity = BattleInfoManager.activeEnemies[i];
                        }
                        else if (attackHighestStaminaOpponent)
                        {
                            if (BattleInfoManager.activeEnemies[i].stamina > targetEntity.stamina)
                                targetEntity = BattleInfoManager.activeEnemies[i];
                        }
                    }
                }
                else if(user.entityTag == EntityTag.Enemy)
                {
                    targetEntity = BattleInfoManager.activePlayers[0];

                    for (int i = 0; i < BattleInfoManager.activePlayers.Count; i++)
                    {
                        if (attackLowestStaminaOpponent)
                        {
                            if (BattleInfoManager.activePlayers[i].stamina < targetEntity.stamina)
                                targetEntity = BattleInfoManager.activePlayers[i];
                        }
                        else if (attackHighestStaminaOpponent)
                        {
                            if (BattleInfoManager.activePlayers[i].stamina > targetEntity.stamina)
                                targetEntity = BattleInfoManager.activePlayers[i];
                        }
                    }
                }
            }

            var offenses = user.GetAvailableOffense();

            if (offenses.Length > 0)
            {
                InBattleSkill offense = offenses[0];

                for (int i = 0; i < offenses.Length; i++)
                {
                    if (offenses[i].GetSkillPoint() > offense.GetSkillPoint())
                        offense = offenses[i];
                }

                currentScore = 4;

                TargetingBehavior.TargetingScore score = new TargetingBehavior.TargetingScore(TargetingBehavior.ActionName.AttackTarget, 
                    currentScore, targetEntity, offense, BattleSelectionType.Attack);

                return score;
            }

            return new TargetingBehavior.TargetingScore(0);
        }
    }

}

