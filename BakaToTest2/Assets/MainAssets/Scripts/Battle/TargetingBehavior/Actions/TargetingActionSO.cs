using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Intelligensia.Battle.Targeting
{
    public abstract class TargetingActionSO : ScriptableObject
    {
        public abstract TargetingBehavior.TargetingScore GetHighestScoredAction(BattleEntity user, BattleEntity target);
    }
}
