using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Intelligensia.Battle.Targeting
{
    [CreateAssetMenu(fileName = "New Targeting Condition", menuName = "ScriptableObject/Targeting Action/Use Item To Heal")]
    public class UseItemToHeal : TargetingActionSO
    {
        [SerializeField] bool useToHealSelf = false;

        public override TargetingBehavior.TargetingScore GetHighestScoredAction(BattleEntity user, BattleEntity target)
        {
            TargetingBehavior.TargetingScore activeActionScore = new TargetingBehavior.TargetingScore(0);

            if (target == null && !useToHealSelf)
                return activeActionScore;

            BattleAction characterAction = BattleInfoManager.GetFirstCharacterAction(user.entityTag == EntityTag.Player);

            if (characterAction != null) // Item action can only be used once per character.
                return activeActionScore;

            Item[] consumables = null;

            if(user.entityTag == EntityTag.Player)
                consumables = PlayerInventoryManager.instance.GetItemsByCategory(ItemCategory.Consumable);
            else if(user.entityTag == EntityTag.Enemy)
            {
                GenericEnemy enemy = (GenericEnemy)user;
                consumables = enemy.GetConsumables();
            }

            if (consumables == null)
                return activeActionScore;

            int currentScore = 0;

            for (int j = 0; j < consumables.Length; j++)
            {
                ConsumableItemBaseValue consumableItemBase = (ConsumableItemBaseValue)consumables[j].itemValue;

                if (consumableItemBase == null)
                    continue;

                if (consumableItemBase.CanCureFatigue())
                {
                    currentScore = 4;

                    if (target == user || useToHealSelf)
                        currentScore = 6;

                    BattleEntity endTarget = useToHealSelf ? user : target;
                    TargetingBehavior.TargetingScore currentActionScore = new TargetingBehavior.TargetingScore(TargetingBehavior.ActionName.HealTarget, currentScore, endTarget, consumables[j]);

                    return currentActionScore;
                }
            }

            return activeActionScore;
        }
    }
}

