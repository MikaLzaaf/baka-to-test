using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Intelligensia.Battle.Targeting
{
    [System.Serializable]
    public class TargetingInfo
    {
        public TargetingConditionSO condition;
        public TargetingActionSO action;


        public TargetingBehavior.TargetingScore GetScoredAction(BattleEntity user, BattleEntity[] opponents, BattleEntity[] allies)
        {
            if(condition == null || action == null)
            {
                Debug.LogError("There is no condition or action set.");
                return new TargetingBehavior.TargetingScore(0);
            }

            if (!condition.IsConditionMet(user, opponents, allies, out BattleEntity target))
                return new TargetingBehavior.TargetingScore(0);

            return action.GetHighestScoredAction(user, target);
        }

         
    }
}

