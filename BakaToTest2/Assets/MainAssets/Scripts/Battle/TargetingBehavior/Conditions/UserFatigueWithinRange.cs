using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Intelligensia.Battle.Targeting
{
    [CreateAssetMenu(fileName = "User Fatigue Within Range", menuName = "ScriptableObject/Targeting Condition/UserFatigueWithinRange")]
    public class UserFatigueWithinRange : TargetingConditionSO
    {
        [SerializeField] float minRange = 0f;
        [SerializeField] float maxRange = 50f;

        public override bool IsConditionMet(BattleEntity user, BattleEntity[] opponents, BattleEntity[] allies, out BattleEntity target)
        {
            target = null;

            return user.fatigue >= minRange && user.fatigue <= maxRange;
        }
    }
}

