using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Intelligensia.Battle.Targeting
{
    [CreateAssetMenu(fileName = "AllyFatigueAboveThreshold", menuName = "ScriptableObject/Targeting Condition/Ally Fatigue Above Threshold")]
    public class AllyFatigueAboveThreshold : TargetingConditionSO
    {
        [SerializeField] float threshold = 50f;

        public override bool IsConditionMet(BattleEntity user, BattleEntity[] opponents, BattleEntity[] allies, out BattleEntity target)
        {
            target = null;

            for (int i = 0; i < allies.Length; i++)
            {
                if (allies[i].fatigue >= threshold)
                {
                    target = allies[i];

                    return true;
                }
            }

            return false;
        }
    }
}

