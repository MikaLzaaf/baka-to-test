using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Intelligensia.Battle.Targeting
{
    [CreateAssetMenu(fileName = "OverallGradeIsGreat", menuName = "ScriptableObject/Targeting Condition/Overall Grade Comparer")]
    public class OverallGradeComparer : TargetingConditionSO
    {
        [StringInList(typeof(PropertyDrawersHelper), "AllGradeResults")] public string resultType;


        public override bool IsConditionMet(BattleEntity user, BattleEntity[] opponents, BattleEntity[] allies, out BattleEntity target)
        {
            target = null;

            string userIntellectual = GradeChecker.ConvertGradeToResult(user.GetOverallGrade());

            return userIntellectual == resultType;
        }
    }
}
