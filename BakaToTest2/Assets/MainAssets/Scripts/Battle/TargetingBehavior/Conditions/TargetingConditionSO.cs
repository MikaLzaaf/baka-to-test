using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Intelligensia.Battle.Targeting
{
    
    public abstract class TargetingConditionSO : ScriptableObject
    {
        public abstract bool IsConditionMet(BattleEntity user, BattleEntity[] opponents, BattleEntity[] allies, out BattleEntity target);
    }
}

