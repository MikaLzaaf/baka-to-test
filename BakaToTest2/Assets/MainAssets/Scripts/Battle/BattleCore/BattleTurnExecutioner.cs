﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Intelligensia.Battle
{
    public class BattleTurnExecutioner : MonoBehaviour
    {
        // What to do here?
        // - Execute turn sequence selected : Skill, Focus, Item, Retreat

        public ViewController switchToPlayerTurnViewEffect;
        public float lastSequenceDeactivateDelayDuration = 0.5f;

        public UnityEvent OnSequenceEndedEvent;

        private const string EnemySequencePrefabsInResources = "TurnSequencePrefabs/EnemySequence/";
        private const string PlayerSequencePrefabsInResources = "TurnSequencePrefabs/PlayerSequence/";
        private const string OrderSequencePrefabsInResources = "TurnSequencePrefabs/OrderSequence/";

        private const string EnemyPrefix = "Enemy_";
        private const string PlayerPrefix = "Player_";
        private const string OrderPrefix = "Order_";
        private const string ItemUsageSuffix = "ItemUsage";
        private const string CounterPrefix = "Counter";

        private const string SupportSkillPrefix = "SupportSkill";
        private const string AttackSkillPrefix = "AttackSkill";
        private const string PartyAttackPrefix = "PartyAttack";


        private Dictionary<string, PlayerTurnSequencePool> playerSequencesLookup = new Dictionary<string, PlayerTurnSequencePool>();
        private Dictionary<string, EnemyTurnSequencePool> enemySequencesLookup = new Dictionary<string, EnemyTurnSequencePool>();
        private Dictionary<string, OrderTurnSequence> orderSequencesLookup = new Dictionary<string, OrderTurnSequence>();

        public TurnSequence currentSequence { get; private set; }
        public TurnSequence lastSequence { get; private set; }


        public void PrepareForBattle()
        {
            playerSequencesLookup = new Dictionary<string, PlayerTurnSequencePool>();
            enemySequencesLookup = new Dictionary<string, EnemyTurnSequencePool>();
            orderSequencesLookup = new Dictionary<string, OrderTurnSequence>();
        }


        //public void InitializeOrderSequence(bool executeImmediately = true)
        //{
        //    string orderId = BattleInfoManager.selectedOrder.orderID;

        //    PreparingOrderSequence(orderId, executeImmediately);
        //}

        public void InitializeItemSequence()
        {
            bool isEnemyTurn = BattleInfoManager.entityOnFocus.entityTag == EntityTag.Enemy;

            string sequenceId = ItemUsageSuffix;

            PreparingSequence(sequenceId, isEnemyTurn, BattleInfoManager.entityOnFocus);
        }

        //public void InitializeCounterSequence()
        //{
        //    bool isEnemyTurn = BattleInfoManager.entityOnTurn.entityTag == EntityTag.Enemy;

        //    string id = CounterPrefix;

        //    PreparingSequence(id, isEnemyTurn, BattleInfoManager.entityOnTurn);
        //}

        public void InitializePartyAttackSequence(bool isEnemyTurn) // Only for Player side for now
        {
            //bool isEnemyTurn = BattleInfoManager.entityOnTurn.entityTag == EntityTag.Enemy;

            string id = PartyAttackPrefix;

            //PreparingPartyAttackSequence(id);
            PreparingSequence(id, isEnemyTurn, BattleInfoManager.entityOnFocus);
        }

        public void InitializeSkillSequence(BattleSelectionType skillType, BattleEntity mainEntity)
        {
            bool isEnemyTurn = mainEntity.entityTag == EntityTag.Enemy;

            string id = skillType == BattleSelectionType.Attack ? AttackSkillPrefix : SupportSkillPrefix;

            PreparingSequence(id, isEnemyTurn, mainEntity);
        }

        private PlayerTurnSequencePool GetPlayerPool(string id)
        {
            playerSequencesLookup.TryGetValue(id, out PlayerTurnSequencePool playerTurnSequencePool);

            return playerTurnSequencePool;
        }

        private EnemyTurnSequencePool GetEnemyPool(string id)
        {
            enemySequencesLookup.TryGetValue(id, out EnemyTurnSequencePool enemyTurnSequencePool);

            return enemyTurnSequencePool;
        }

        private EnemyTurnSequence GetEnemySequencePrefab(string sequenceId)
        {
            string path = EnemySequencePrefabsInResources + EnemyPrefix + sequenceId;

            EnemyTurnSequence prefab = Resources.Load<EnemyTurnSequence>(path) as EnemyTurnSequence;

            return prefab;
        }
        private PlayerTurnSequence GetPlayerSequencePrefab(string sequenceId)
        {
            string path = PlayerSequencePrefabsInResources + PlayerPrefix + sequenceId;

            PlayerTurnSequence prefab = Resources.Load<PlayerTurnSequence>(path) as PlayerTurnSequence;

            return prefab;
        }

        private OrderTurnSequence GetOrderSequencePrefab(string orderId)
        {
            Debug.Log(orderId + " order id");
            string path = OrderSequencePrefabsInResources + OrderPrefix + orderId;

            OrderTurnSequence prefab = Resources.Load<OrderTurnSequence>(path) as OrderTurnSequence;

            return prefab;
        }

        private PlayerTurnSequence GetPlayerSequence(string sequenceId)
        {
            playerSequencesLookup.TryGetValue(sequenceId, out PlayerTurnSequencePool playerPool);

            if (playerPool == null)
                return null;

            PlayerTurnSequence sequence = playerPool.GetAvailableSequence();

            return sequence;
        }

        private EnemyTurnSequence GetEnemySequence(string sequenceId)
        {
            enemySequencesLookup.TryGetValue(sequenceId, out EnemyTurnSequencePool enemyPool);

            if (enemyPool == null)
                return null;

            EnemyTurnSequence sequence = enemyPool.GetAvailableSequence();

            return sequence;
        }

        private TurnSequence GetTurnSequence(string sequenceId, bool isEnemyTurn)
        {
            TurnSequence sequence = null;

            sequence = isEnemyTurn ? (TurnSequence)GetEnemySequence(sequenceId) :
                (TurnSequence)GetPlayerSequence(sequenceId);

            if (sequence == null)
            {
                AddTurnSequence(sequenceId, isEnemyTurn);

                sequence = isEnemyTurn ? (TurnSequence)GetEnemySequence(sequenceId) :
                    (TurnSequence)GetPlayerSequence(sequenceId);
            }

            return sequence;
        }

        private TurnSequence GetOrderSequence(string orderId)
        {
            orderSequencesLookup.TryGetValue(orderId, out OrderTurnSequence orderTurnSequence);

            if (orderTurnSequence == null)
            {
                orderTurnSequence = Instantiate(GetOrderSequencePrefab(orderId), transform);
                //orderTurnSequence.gameObject.SetActive(false);

                orderSequencesLookup.Add(orderId, orderTurnSequence);
            }

            return orderTurnSequence;
        }

        private void AddTurnSequence(string sequenceId, bool isEnemyTurn)
        {
            if (isEnemyTurn)
            {
                EnemyTurnSequencePool enemyPool = GetEnemyPool(sequenceId);

                if (enemyPool == null)
                {
                    enemyPool = new EnemyTurnSequencePool(sequenceId);
                    enemySequencesLookup.Add(sequenceId, enemyPool);
                }

                if (!enemyPool.HasEnoughSequence())
                {
                    EnemyTurnSequence enemyTurnSequence = Instantiate(GetEnemySequencePrefab(sequenceId), transform);
                    enemyTurnSequence.ToggleSequence(false);

                    enemyPool.AddSequence(enemyTurnSequence);
                }
            }
            else
            {
                PlayerTurnSequencePool playerPool = GetPlayerPool(sequenceId);

                if (playerPool == null)
                {
                    playerPool = new PlayerTurnSequencePool(sequenceId);
                    playerSequencesLookup.Add(sequenceId, playerPool);

                }

                if (!playerPool.HasEnoughSequence())
                {
                    PlayerTurnSequence playerTurnSequence = Instantiate(GetPlayerSequencePrefab(sequenceId), transform);
                    playerTurnSequence.ToggleSequence(false);

                    playerPool.AddSequence(playerTurnSequence);
                }
            }
        }

        private void PreparingSequence(string sequenceId, bool isEnemyTurn, BattleEntity mainEntity)
        {
            if (currentSequence != null)
            {
                lastSequence = currentSequence;
                currentSequence = null;
            }

            Actor mainActor = mainEntity.GetComponent<Actor>();
            Actor[] sideActors = SelectSideActors();

            currentSequence = GetTurnSequence(sequenceId, isEnemyTurn);

            BattleUIManager.instance.SetInputActiveState(false);

            currentSequence.Initialize(sequenceId, mainActor, sideActors);

            ExecuteTurnSequence();
        }

        //private void PreparingPartyAttackSequence(string sequenceId)
        //{
        //    if (currentSequence != null)
        //    {
        //        lastSequence = currentSequence;
        //        currentSequence = null;
        //    }

        //    List<Actor> allActors = new List<Actor>();

        //    for (int i = 0; i < BattleInfoManager.activePlayers.Count; i++)
        //    {
        //        Actor actor = BattleInfoManager.activePlayers[i].GetComponent<Actor>();

        //        if (actor != null)
        //        {
        //            allActors.Add(actor);
        //        }
        //    }

        //    for (int i = 0; i < BattleInfoManager.reservePlayers.Count; i++)
        //    {
        //        Actor actor = BattleInfoManager.reservePlayers[i].GetComponent<Actor>();

        //        if (actor != null)
        //        {
        //            allActors.Add(actor);
        //        }
        //    }

        //    for (int i = 0; i < BattleInfoManager.activeEnemies.Count; i++)
        //    {
        //        Actor actor = BattleInfoManager.activeEnemies[i].GetComponent<Actor>();

        //        if (actor != null)
        //        {
        //            allActors.Add(actor);
        //        }
        //    }

        //    currentSequence = GetTurnSequence(sequenceId, false);

        //    BattleUIManager.instance.SetInputActiveState(false);

        //    currentSequence.Initialize(sequenceId, allActors.ToArray());

        //    ExecuteTurnSequence();
        //}

        private void PreparingOrderSequence(string orderSequenceId, bool executeImmediately)
        {
            if (currentSequence != null)
            {
                lastSequence = currentSequence;
                currentSequence = null;
            }

            //if (BattleInfoManager.selectedOrder.orderHolder == null)
            //{
            //    Debug.LogError("The Selected Order does not have orderHolder!");
            //    return;
            //}

            //string actorName = BattleInfoManager.selectedOrder.orderHolder.GetName();

            //BattleEntity mainActorEntity = BattleInfoManager.GetOrderActorInBattle(actorName);

            //if(mainActorEntity == null)
            //{
            //    mainActorEntity = BattleInfoManager.selectedOrder.orderHolder;
            //}

            //Actor mainActor = mainActorEntity.GetComponent<Actor>();
            //Actor[] sideActors = SelectSideActors();

            //currentSequence = GetOrderSequence(orderSequenceId);

            //BattleUIManager.instance.SetInputActiveState(false);

            //currentSequence.Initialize(orderSequenceId, mainActor, sideActors);
            //Debug.Log("Current seq " + currentSequence);
            //if (executeImmediately)
            //{
            //    ExecuteTurnSequence();
            //}
        }


        public void ExecuteTurnSequence()
        {
            if (currentSequence == null)
                return;

            currentSequence.SequenceEndedEvent += OnSequenceEnded;

            //BattleUIManager.instance.battleInfoPanel.ToggleSkillLabel(true);

            currentSequence.Execute();

            StartCoroutine(DeactivateLastSequence());
        }

        private void OnSequenceEnded()
        {
            if(currentSequence != null)
            {
                currentSequence.SequenceEndedEvent -= OnSequenceEnded;
                currentSequence.ToggleSequence(false);
            }

            //BattleUIManager.instance.battleInfoPanel.ToggleSkillLabel(false);

            OnSequenceEndedEvent?.Invoke();
        }

        private IEnumerator DeactivateLastSequence()
        {
            if (lastSequence == currentSequence)
                yield break;

            yield return new WaitForSeconds(lastSequenceDeactivateDelayDuration);

            if (lastSequence != null)
            {
                lastSequence.ToggleSequence(false);
            }
        }

        private Actor[] SelectSideActors()
        {
            List<Actor> sideActors = new List<Actor>();

            List<BattleEntity> sideEntities = BattleInfoManager.GetTargetsSelected();

            if(sideEntities != null && sideEntities.Count > 0)
            {
                for (int i = 0; i < sideEntities.Count; i++)
                {
                    Actor sideActor = sideEntities[i].GetComponent<Actor>();
                    sideActors.Add(sideActor);
                }
            }
            
            return sideActors.ToArray();
        }


        public void RemoveActorFromCameraTargetGroups(Transform actor)
        {
            if(currentSequence != null)
            {
                currentSequence.RemoveActorFromCameraTargetGroups(actor);
            }
        }
    }
}
