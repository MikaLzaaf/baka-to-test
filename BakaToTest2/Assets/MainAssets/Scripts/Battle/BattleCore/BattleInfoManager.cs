﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Intelligensia.Battle;
using System;

namespace Intelligensia.Battle
{
    public static class BattleInfoManager
    {
        // What MUST HAVE here?
        // - All PCs and Enemies List
        // - Defeated Enemies List
        // - 

        public static readonly int MaxStressPoint = 100;
        public static readonly float BattleSceneHeight = -100f;



        public static event Action<int> StressPointUpdatedEvent;
        public static event Action<Subjects> ActiveSubjectChangedEvent;
        public static event Action<BattleAction> BattleArgumentsAddedEvent;
        public static event Action<BattleAction> BattleArgumentsRemovedEvent;
        public static event Action<BattleEntity> EntityOnFocusedEvent;

        public static List<PlayerEntity> activePlayers;
        public static List<PlayerEntity> reservePlayers;
        public static List<GenericEnemy> activeEnemies;
        public static List<GenericEnemy> reserveEnemies;

        public static List<GenericEnemy> defeatedEnemyList;
        public static List<PlayerEntity> defeatedPlayerList;
        public static List<PlayerEntity> survivedPlayerList;

        public static BattleEntity newClassmate;
        public static PlayerEntity playerClassLeader;
        public static GenericEnemy enemyClassLeader;

        public static BattleCombatModifier currentCombatModifier;
        public static EntityTag debateWinner;

        private static BattleEntity currentTargetHighlighted;
        private static BattleAction inProgressAction;

        public static bool currentTargetIsEnemy { get; private set; } = true;
        public static AimedTo currentAim { get; private set; }

        public static DayEnum currentDay { get; private set; }
        public static string currentWeather { get; private set; }
        public static ChallengeInfo currentChallengeInfo { get; private set; }

        public static List<BattleAction> enemyActionsRegistered { get; private set; }
        public static List<BattleAction> enemyArgumentActionsRegistered { get; private set; }
        public static List<BattleAction> battleActionsRegistered { get; private set; }
        public static List<BattleAction> argumentActionsRegistered { get; private set; }


        private static Subjects _currentActiveSubject;
        public static Subjects currentActiveSubject
        {
            get => _currentActiveSubject;
            set
            {
                _currentActiveSubject = value;

                ActiveSubjectChangedEvent?.Invoke(_currentActiveSubject);
            }
        }

        private static int _currentStressPoint;
        public static int currentStressPoint
        {
            get => _currentStressPoint;
            set
            {
                _currentStressPoint = Mathf.Clamp(value, 0, MaxStressPoint);

                StressPointUpdatedEvent?.Invoke(_currentStressPoint);
            }
        }

        private static Item _selectedItem;
        public static Item selectedItem
        {
            get => _selectedItem;
            set
            {
                _selectedItem = value;

                if (_selectedItem != null)
                {
                    currentAim = _selectedItem.itemValue.GetAim();
                }
            }
        }

        private static InBattleSkill _selectedSkill;
        public static InBattleSkill selectedSkill
        {
            get => _selectedSkill;
            set
            {
                _selectedSkill = value;

                if (_selectedSkill != null)
                {
                    currentAim = _selectedSkill.GetSkillAim();
                }
            }
        }

        private static BattleOrder _selectedOrder;
        public static BattleOrder selectedOrder
        {
            get => _selectedOrder;
            set
            {
                _selectedOrder = value;

                if (_selectedOrder != null)
                {
                    currentAim = _selectedOrder.aimedTo;
                }

                if(_selectedOrder != null)
                {
                    currentStressPoint -= _selectedOrder.orderCost;
                }
            }
        }

        private static BattleEntity _entityOnFocus;
        public static BattleEntity entityOnFocus
        {
            get => _entityOnFocus;
            set
            {
                _entityOnFocus = value;

                if(_entityOnFocus != null)
                    EntityOnFocusedEvent?.Invoke(_entityOnFocus);
            }
        }


        public static void Initialize()
        {
            activePlayers = new List<PlayerEntity>();
            reservePlayers = new List<PlayerEntity>();
            activeEnemies = new List<GenericEnemy>();
            reserveEnemies = new List<GenericEnemy>();

            defeatedEnemyList = new List<GenericEnemy>();
            defeatedPlayerList = new List<PlayerEntity>();
            survivedPlayerList = new List<PlayerEntity>();

            selectedItem = null;
            selectedSkill = null;
            selectedOrder = null;

            currentChallengeInfo = null;

            newClassmate = null;

            currentCombatModifier = new BattleCombatModifier();

            enemyActionsRegistered = new List<BattleAction>();
            enemyArgumentActionsRegistered = new List<BattleAction>();
            battleActionsRegistered = new List<BattleAction>();
            argumentActionsRegistered = new List<BattleAction>();
        }

        public static void ResetAllSelectedInfos()
        {
            selectedItem = null;
            selectedSkill = null;
            selectedOrder = null;

            entityOnFocus = activePlayers[0];
        }


        public static void PreBattleInfoSetup(DayEnum day, string weather, Subjects activeSub, int stressPoint, ChallengeInfo challengeInfo)
        {
            currentDay = day;
            currentWeather = weather;
            currentActiveSubject = activeSub;
            currentStressPoint = stressPoint;

            currentChallengeInfo = challengeInfo;
        }

        public static bool IsEnemiesDefeated()
        {
            if (currentChallengeInfo.isClassBattle)
            {
                for (int i = 0; i < activeEnemies.Count; i++)
                {
                    if (activeEnemies[i].IsLeader && activeEnemies[i].isDestroyed)
                        return true;
                }
            }

            return activeEnemies.Count == 0;
        }

        public static bool IsPlayerDefeated()
        {
            if(currentChallengeInfo.isClassBattle)
            {
                for (int i = 0; i < activePlayers.Count; i++)
                {
                    if (activePlayers[i].IsLeader && activePlayers[i].isDestroyed)
                        return true;
                }
            }
               
            return activePlayers.Count == 0;
        }

        public static bool IsAnyoneDefeated()
        {
            for(int i = 0; i < activePlayers.Count; i++)
            {
                if (activePlayers[i].isDestroyed)
                    return true;
            }

            for (int i = 0; i < activeEnemies.Count; i++)
            {
                if (activeEnemies[i].isDestroyed)
                    return true;
            }

            return false;
        }

        public static List<BattleEntity> GetAllCharacters()
        {
            List<BattleEntity> allCharacters = new List<BattleEntity>();

            allCharacters.AddRange(activePlayers);
            allCharacters.AddRange(activeEnemies);

            return allCharacters;
        }

        #region Targets Stuff

        public static void PrepareForTargetSelection()
        {
            if (currentAim == AimedTo.SingleEnemy || currentAim == AimedTo.AllEnemies)
                currentTargetIsEnemy = true;
            else
                currentTargetIsEnemy = false;
        }

        // Used to generate list on target selection panel for player side only
        public static List<BattleEntity> GetTargetList()
        {
            List<BattleEntity> targetList = new List<BattleEntity>();

            if (currentTargetIsEnemy)
                targetList.AddRange(activeEnemies);
            else
                targetList.AddRange(activePlayers);

            return targetList;
        }

        // Mostly used for Camera targets
        public static List<BattleEntity> GetTargetsSelected()
        {
            List<BattleEntity> targetsSelected = new List<BattleEntity>();

            if (currentAim == AimedTo.AllEnemies)
            {
                if (debateWinner == EntityTag.Player)
                    targetsSelected.AddRange(activeEnemies);
                else if (debateWinner == EntityTag.Enemy)
                    targetsSelected.AddRange(activePlayers);
            }
            else if (currentAim == AimedTo.AllPartyMembers)
            {
                //if (isPlayerTurn)
                //    targetsSelected.AddRange(activePlayers);
                //else
                //    targetsSelected.AddRange(activeEnemies);
            }
            else if (currentAim == AimedTo.Self)
            {
                targetsSelected.Add(entityOnFocus);
            }
            else if (currentAim == AimedTo.All)
            {
                targetsSelected.AddRange(activePlayers);
                targetsSelected.AddRange(activeEnemies);
            }
            else if (currentAim == AimedTo.SinglePartyMember)
            {
                //targetsSelected.Add(currentTargetHighlighted);
            }
            else if(currentAim == AimedTo.SingleEnemy)
            {
                if(debateWinner == EntityTag.Enemy)
                {
                    //BattleEntity target = isPlayerTurn ? entityOnTurn : currentTargetHighlighted;
                    //targetsSelected.Add(target);
                }
                else if(debateWinner == EntityTag.Player)
                {
                    //BattleEntity target = isPlayerTurn ? currentTargetHighlighted : entityOnTurn;
                    //targetsSelected.Add(target);
                }
            }

            return targetsSelected;
        }

        #endregion

        public static bool IsOrderHolderDefeated(string characterName)
        {
            for (int i = 0; i < defeatedPlayerList.Count; i++)
            {
                if (defeatedPlayerList[i].characterName == characterName)
                {
                    return true;
                }
            }

            return false;
        }

        public static GenericEnemy GetNextReserveEnemy()
        {
            if (reserveEnemies.Count <= 0)
                return null;

            GenericEnemy nextReserveEnemy = reserveEnemies[0];
            reserveEnemies.RemoveAt(0);

            return nextReserveEnemy;
        }

        public static PlayerEntity GetNextReservePlayer()
        {
            if (reservePlayers.Count <= 0)
                return null;

            PlayerEntity nextReservePlayer = reservePlayers[0];
            reservePlayers.RemoveAt(0);

            return nextReservePlayer;
        }

        #region Battle Actions

        public static void CreateNewAction(BattleSelectionType selectionType, string actionName, bool isPlayerAction)
        {
            inProgressAction = new BattleAction(selectionType, actionName, entityOnFocus, isPlayerAction);

            if (currentTargetHighlighted != null)
            {
                AddActionSelected(currentTargetHighlighted);
                currentTargetHighlighted = null;
            }
        }

        public static void CreateNewAction(BattleSelectionType selectionType, Item consumable, bool isPlayerAction)
        {
            inProgressAction = new BattleAction(selectionType, consumable, entityOnFocus, isPlayerAction);

            if (currentTargetHighlighted != null)
            {
                AddActionSelected(currentTargetHighlighted);
                currentTargetHighlighted = null;
            }
        }

        public static void CreateNewAction(BattleSelectionType selectionType, InBattleSkill skill, bool isPlayerAction)
        {
            inProgressAction = new BattleAction(selectionType, skill, entityOnFocus, isPlayerAction);

            if (currentTargetHighlighted != null) 
            {
                AddActionSelected(currentTargetHighlighted);
                currentTargetHighlighted = null;
            }
        }

        public static BattleAction GetFirstCharacterAction(bool isPlayerAction)
        {
            if(isPlayerAction)
            {
                for (int i = 0; i < battleActionsRegistered.Count; i++)
                {
                    if (battleActionsRegistered[i].user == entityOnFocus)
                        return battleActionsRegistered[i];
                }
            }
            else
            {
                for (int i = 0; i < enemyActionsRegistered.Count; i++)
                {
                    if (enemyActionsRegistered[i].user == entityOnFocus)
                        return enemyActionsRegistered[i];
                }
            }
            
            return null;
        }

        public static BattleAction[] GetEnemyItemActions()
        {
            List<BattleAction> itemActions = new List<BattleAction>();

            for (int i = 0; i < enemyActionsRegistered.Count; i++)
            {
                if (enemyActionsRegistered[i].selectionType == BattleSelectionType.Item)
                    itemActions.Add(enemyActionsRegistered[i]);
            }

            return itemActions.ToArray();
        }

        public static BattleAction GetSameArgumentAction(string actionName, bool isPlayerActions = true)
        {
            if(isPlayerActions)
            {
                for (int i = 0; i < argumentActionsRegistered.Count; i++)
                {
                    if (argumentActionsRegistered[i].actionName == actionName)
                        return argumentActionsRegistered[i];
                }
            }
            else
            {
                for (int i = 0; i < enemyArgumentActionsRegistered.Count; i++)
                {
                    if (enemyArgumentActionsRegistered[i].actionName == actionName)
                        return enemyArgumentActionsRegistered[i];
                }
            }

            return null;
        }

        public static bool HasSameAction(string actionName, bool isPlayerAction, out int sameIndex)
        {
            bool hasSameAction = false;

            sameIndex = -1;

            if(isPlayerAction)
            {
                for (int i = 0; i < argumentActionsRegistered.Count; i++)
                {
                    // Only for checking any arguments with the same name. For items/guard no need to check.
                    if (argumentActionsRegistered[i].actionName == actionName)
                    {
                        hasSameAction = true;
                        sameIndex = i;
                        break;
                    }
                }
            }
            else
            {
                for (int i = 0; i < enemyArgumentActionsRegistered.Count; i++)
                {
                    // Only for checking any arguments with the same name. For items/guard no need to check.
                    if (enemyArgumentActionsRegistered[i].actionName == actionName)
                    {
                        hasSameAction = true;
                        sameIndex = i;
                        break;
                    }
                }
            }
            

            return hasSameAction;
        }

        public static bool HasFullArguments()
        {
            if (argumentActionsRegistered == null)
                return false;

            if (argumentActionsRegistered.Count >= BattleController.MaxArgumentAmount)
                return true;

            return false;
        }

        public static void AddActionSelected(BattleEntity targetEntity)
        {
            if(inProgressAction == null)
            {
                currentTargetHighlighted = targetEntity;
                return;
            }

            if(currentAim == AimedTo.AllEnemies)
            {
                if (inProgressAction.isPlayerAction)
                    inProgressAction.AddTarget(activeEnemies.ToArray());
                else
                    inProgressAction.AddTarget(activePlayers.ToArray());
            }
            else if (currentAim == AimedTo.AllPartyMembers)
            {
                if (inProgressAction.isPlayerAction)
                    inProgressAction.AddTarget(activePlayers.ToArray());
                else
                    inProgressAction.AddTarget(activeEnemies.ToArray());
            }
            else
                inProgressAction.AddTarget(targetEntity);

            if(entityOnFocus.entityTag == EntityTag.Player)
            {
                battleActionsRegistered.Add(inProgressAction);

                if (inProgressAction.selectionType == BattleSelectionType.Attack || inProgressAction.selectionType == BattleSelectionType.Support)
                {
                    argumentActionsRegistered.Add(inProgressAction);
                    BattleArgumentsAddedEvent?.Invoke(inProgressAction);
                }
            }
            else if (entityOnFocus.entityTag == EntityTag.Enemy)
            {
                enemyActionsRegistered.Add(inProgressAction);

                if (inProgressAction.selectionType == BattleSelectionType.Attack || inProgressAction.selectionType == BattleSelectionType.Support)
                {
                    enemyArgumentActionsRegistered.Add(inProgressAction);
                }
            }

            inProgressAction = null;
        }

        public static void RemoveActionSelected(int removalIndex)
        {
            BattleAction actionToRemove = argumentActionsRegistered[removalIndex];

            if (actionToRemove.selectionType == BattleSelectionType.Support ||
                actionToRemove.selectionType == BattleSelectionType.Attack)
            {
                BattleArgumentsRemovedEvent?.Invoke(actionToRemove);

                argumentActionsRegistered.RemoveAt(removalIndex);
            }

            battleActionsRegistered.Remove(actionToRemove);
        }

        public static void RemoveCharacterActions()
        {
            for (int i = battleActionsRegistered.Count - 1; i >= 0; i--)
            {
                if (battleActionsRegistered[i].user == entityOnFocus)
                {
                    if (battleActionsRegistered[i].selectionType == BattleSelectionType.Counter)
                        entityOnFocus.Guard(false);

                    if (battleActionsRegistered[i].selectionType != BattleSelectionType.Item)
                        battleActionsRegistered.RemoveAt(i);
                }
            }

            for(int i = argumentActionsRegistered.Count - 1; i >= 0; i--)
            {
                BattleArgumentsRemovedEvent?.Invoke(argumentActionsRegistered[i]);
                argumentActionsRegistered.RemoveAt(i);
            }
        }

        public static void RemoveAllActionsSelected()
        {
            enemyActionsRegistered.Clear();
            enemyArgumentActionsRegistered.Clear();
            battleActionsRegistered.Clear();
            argumentActionsRegistered.Clear();

            for(int i = 0; i < activeEnemies.Count; i++)
            {
                if (activeEnemies[i].isGuarding)
                    activeEnemies[i].Guard(false);
            }

            for (int i = 0; i < activePlayers.Count; i++)
            {
                if (activePlayers[i].isGuarding)
                    activePlayers[i].Guard(false);
            }
        }

        #endregion
    }
}
