﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Intelligensia.Battle;

public static class DamageCalculator
{
    //private static readonly int SubjectDefaultDamage = 100;
    //private static readonly int SubjectDefaultDivider = 50;
    private static readonly int AgilityMinValue = 80;

    private static readonly int OverfatiguedCritRateBonus = 30;
    private static readonly int OverfatiguedCritRateIncreaseEachRound = 3;
    //private static readonly int OverfatiguedHitRateBonus = 20;





    public static int CalculateHitProbability(int skillAccuracy, int attackerAccuracy, int defenderAgility)
    {
        float defenderEvadeProbability = Mathf.Clamp(100 - defenderAgility, AgilityMinValue, 100);

        // Multiply defenderEvadeProbability with current Combat Modifier in Battle if it's Player turn
        if (BattleController.IsInBattle)
        {
            if (BattleInfoManager.currentCombatModifier != null && BattleInfoManager.debateWinner == EntityTag.Player)
            {
                defenderEvadeProbability *= BattleInfoManager.currentCombatModifier.agilityModifier;
            }
        }
        Debug.Log("Attaker acc : " + attackerAccuracy + " // defender evade : " + defenderEvadeProbability);
        float hitProbability = ((float)skillAccuracy / 100f) * ((float)attackerAccuracy / 100f) / ((float)defenderEvadeProbability / 100f);

        return (int)(hitProbability * 100);
    }


    public static float CalculateDamage(float baseDamage, BattleEntity attacker, BattleEntity target, int skillAccuracy, bool isFatal, bool isGuaranteedCrit, out DamageHitCategory hitCategory)
    {
        hitCategory = DamageHitCategory.NormalHit;

        // Base Damage to apply is based on the INT of attacker + base damage of skill
        float damageToApply = attacker.intelligence + baseDamage;

        // Multiply Base DMG to Apply with current Combat Modifier in Battle if it's Player turn
        if (BattleController.IsInBattle)
        {
            if(BattleInfoManager.currentCombatModifier != null && BattleInfoManager.debateWinner == EntityTag.Player)
            {
                damageToApply *= BattleInfoManager.currentCombatModifier.damageModifier;
            }
        }    

        // Get the attacker Confidence Factor value
        //string attackerConfidenceGrade = ConfidenceInfoManager.DetermineConfidenceGrade(target, attacker);

        //ConfidenceFactorInBattle attackerConfidenceFactor = ConfidenceInfoManager.GetConfidenceFactorInBattle(attackerConfidenceGrade);

        // Get the random hit value
        //int hit = Random.Range(0, 100);

        //// Calculate the skill hit probability
        //int hitProbability = CalculateHitProbability(skillAccuracy, attackerConfidenceFactor.accuracy, attacker.agility);
 
        //if(IsTargetOverfatigued(target))
        //{
        //    hitProbability += OverfatiguedHitRateBonus;
        //}
        //Debug.Log("Hit  : " + hit + " // Hit probability : " + hitProbability);
        //if (hit > hitProbability && !isGuaranteedCrit)
        //{
        //    hitCategory = DamageHitCategory.Missed;
        //}

        // Only if skill hits
        if (hitCategory == DamageHitCategory.NormalHit)
        {
            // Get the random critical hit value
            int criticalHit = Random.Range(0, 100);

            // Calculate the effective critical hit probability
            int effectiveCritRate = GetEffectiveCritRate(attacker, target);

            // Add effectiveCritRate with current Combat Modifier in Battle if it's Player turn
            if (BattleController.IsInBattle)
            {
                if (BattleInfoManager.currentCombatModifier != null && BattleInfoManager.debateWinner == EntityTag.Player)
                {
                    effectiveCritRate += BattleInfoManager.currentCombatModifier.critRateModifier;
                }
            }

            if (criticalHit <= effectiveCritRate || isGuaranteedCrit)
            {
                if (BattleController.IsInBattle)
                {
                    if (BattleInfoManager.currentCombatModifier != null && BattleInfoManager.debateWinner == EntityTag.Player)
                    {
                        damageToApply *= BattleInfoManager.currentCombatModifier.critDamageModifier;
                    }
                }
                // Multiply damage value with critical DMG multiplier based on confidence factor grade
                //damageToApply *= attackerConfidenceFactor.criticalDamageMultiplier;

                hitCategory = isFatal ? DamageHitCategory.FatalCriticalHit :
                    DamageHitCategory.NonFatalCriticalHit;
            }
        }

        return damageToApply;
    }

    public static float CalculatePartyAttackDamage(float baseDamage, BattleEntity[] attackers, BattleEntity target)
    {
        float damageToApply = 0f;

        for(int i = 0; i < attackers.Length; i++)
        {
            damageToApply += CalculateDamage(baseDamage, attackers[i], target, 100, false, false, out DamageHitCategory hitCategory);
        }

        return damageToApply;
    }

    //private static bool IsTargetOverfatigued(BattleEntity target)
    //{
    //    return target.fatigue >= target.stamina;
    //}

    public static int GetEffectiveCritRate(BattleEntity attacker, BattleEntity target)
    {
        // Calculate the effective critical hit probability
        int effectiveCritRate = Mathf.RoundToInt(attacker.criticalRate + attacker.critRateModifier +
            target.receiveCritModifier + (target.totalRoundInOverfatigued * OverfatiguedCritRateIncreaseEachRound));

        if (target.IsOverfatigued)
            effectiveCritRate += OverfatiguedCritRateBonus;

        return effectiveCritRate;
    }
}
