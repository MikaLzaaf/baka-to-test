using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Intelligensia.Battle
{
    public class BattleAction
    {
        public BattleSelectionType selectionType;
        public string actionName;
        public BattleEntity user;
        public List<BattleEntity> targets;
        public InBattleSkill skill;
        public Item consumable;
        public bool isPlayerAction;
        
        public BattleAction() { }



        public BattleAction(BattleSelectionType selectionType, string actionName, BattleEntity user, bool isPlayerAction)
        {
            this.selectionType = selectionType;
            this.actionName = actionName;
            this.user = user;
            this.isPlayerAction = isPlayerAction;

            skill = null;
            consumable = null;
        }

        public BattleAction(BattleSelectionType selectionType, Item consumable, BattleEntity user, bool isPlayerAction)
        {
            this.selectionType = selectionType;
            this.consumable = consumable;
            this.user = user;
            this.isPlayerAction = isPlayerAction;

            skill = null;
            actionName = null;
        }

        public BattleAction(BattleSelectionType selectionType, InBattleSkill skill, BattleEntity user, bool isPlayerAction)
        {
            this.selectionType = selectionType;
            this.skill = skill;
            this.user = user;
            this.isPlayerAction = isPlayerAction;

            actionName = skill.GetSkillName();
            consumable = null;
        }


        public void Execute()
        {
            for(int i = 0; i < targets.Count; i++)
            {
                if (selectionType == BattleSelectionType.Attack || selectionType == BattleSelectionType.Support)
                    skill.ApplySkill(targets[i]);
            }
           
        }

        public void AddTarget(BattleEntity target)
        {
            if(targets == null)
                targets = new List<BattleEntity>();

            targets.Add(target);
        }

        public void AddTarget(BattleEntity[] targets)
        {
            for(int i = 0; i < targets.Length; i++)
            {
                AddTarget(targets[i]);
            }
        }

        public bool IsTargetDestroyed()
        {
            for(int i = 0; i < targets.Count; i++)
            {
                if (!targets[i].isDestroyed)
                    return false;
            }

            return true;
        }

        public bool HasSameTarget(BattleAction otherAction)
        {
            if (targets == null || otherAction.targets == null)
                return false;

            if (targets.Count != otherAction.targets.Count) 
                return false;

            // Only for actions with single target since other kind of action is with all targets.
            if (skill.GetSkillAim() == AimedTo.SingleEnemy || skill.GetSkillAim() == AimedTo.SinglePartyMember || skill.GetSkillAim() == AimedTo.Self)
            {
                if (targets[0] != otherAction.targets[0])
                    return false;
            }

            return true;
        }
    }
}

