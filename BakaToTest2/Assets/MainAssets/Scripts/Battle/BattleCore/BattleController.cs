﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Cinemachine;
using Intelligensia.Battle;

public class BattleController : Singleton<BattleController>
{
    // Change this class to a singleton and DontDestroyOnLoad class


    // What to do here?
    // - Spawn enemies and players
    // - Change Battle Turns
    // - Initialize Battle
    // - Cleanup Battle
    // - Store references to BattleInfo etc.

    public static int MaxArgumentAmount = 4;
    public static bool IsInBattle = false;

    #region Events

    public event Action BattleEndedEvent;
    public event Action<BattleEntity> RemoveCharacterInBattleEvent;
    //public event Action FreeExecutionEndedEvent;
    public event Action<BattleAction[]> TurnExecutionEvent;
    #endregion

    public CharacterSpawnerSO characterSpawner;
    public BattleTurnExecutioner turnExecutioner;
    public GameObject battleEnvironment;

    [Header("Event Channels")]
    public TurnEndedEventChannelSO turnEndedEventChannel;
    //public FreeExecutionEndedEventChannelSO freeExecutionEndedEventChannel;
    public OneBattleRoundEventChannelSO oneBattleRoundEventChannel;

    //[Header("KO Ripple Value")]
    //[SerializeField] private float knockoutRippleValue = 3f;

    public Subjects activeSubject => _activeSubject;
    public BattleSelectionType actionSelected { get; set; }
    public ArgumentSide playerArgumentSide { get; private set; }
    public ArgumentSide enemyArgumentSide { get; private set; }
    public ChallengeInfo currentChallengeInfo { get; private set; }
    public bool useInfiniteHp { get; private set; }
    public bool isInitializing { get; private set; }
    public bool enemyHasDecidedAction { get; private set; }
    public bool nextReserveSpawnIsPlayer { get; private set; }



    private List<BattlePartyMember> playersToSpawn = new List<BattlePartyMember>();
    private BattlePartyMember[] enemiesToSpawn;

    private Transform[] playerSpawnPositions;
    private Transform[] enemiesSpawnPositions;
    private SkillItemBaseValue[] testingSkills;
    private int activeMembersCount = 0;

    private int reserveSpawnIndex = 0;

    
    private Subjects _activeSubject;
    private DayEnum currentDay;
    private string currentWeather;

    private Action callbackOnTurnEnded;

    private bool isBattleTriggered = false;



    public void TriggerBattle(ChallengeInfo challengeInfo, bool reloadScene = false)
    {
        if (challengeInfo == null)
            return;

        currentChallengeInfo = challengeInfo;

        LoadingScreen.instance.StartLoadingBattleScene(reloadScene);

        MainCameraController.instance.ToggleCameraBrain(false);

        GameUIManager.instance.mapController.SetMapMode(MapUIController.MapMode.Deactivate);

        PlayerPartyManager.instance.PrepareForBattle();
        turnExecutioner.PrepareForBattle();

        enemiesToSpawn = currentChallengeInfo.opponents;
        playersToSpawn = PlayerPartyManager.instance.GetBattlePartyMembers();

        _activeSubject = DailyInfoManager.activeSubject;
        currentDay = DailyInfoManager.currentDay;
        currentWeather = DailyInfoManager.currentWeather;

        isBattleTriggered = true;
    }

    public void DebugBattle(BattlePhasesController phasesController, ChallengeInfo challengeInfo)
    {
        if (isBattleTriggered)
            return;

        if (challengeInfo == null)
            return;

        currentChallengeInfo = challengeInfo;

        enemiesToSpawn = currentChallengeInfo.opponents;
        playersToSpawn = phasesController.playerPartyMembers.Count > 0 ? phasesController.playerPartyMembers :
            PlayerPartyManager.instance.GetBattlePartyMembers(); // Later need to change based on manually selected members

        testingSkills = phasesController.useTestSkills ? phasesController.testingSkills : null;
        activeMembersCount = phasesController.activeMembersCount;

        useInfiniteHp = phasesController.useInfiniteHp;

        _activeSubject = phasesController.activeSubject;
        currentDay = phasesController.testDay;
        currentWeather = phasesController.testWeather;

        PlayerPartyManager.instance.PrepareForBattle();
        turnExecutioner.PrepareForBattle();
    }

    public void SetSpawnPositions(Transform[] playerSpawnPoints, Transform[] enemiesSpawnPoints)
    {
        playerSpawnPositions = playerSpawnPoints;
        enemiesSpawnPositions = enemiesSpawnPoints;
    }

    #region Initialize Phase

    public void InitializeBattle()
    {
        isInitializing = true;

        if (turnEndedEventChannel != null)
            turnEndedEventChannel.OnTurnEnded += OnTurnExecutionEnded;

        IsInBattle = true;

        BattleInfoManager.Initialize();
        BattleInfoManager.PreBattleInfoSetup(currentDay, currentWeather, activeSubject, 100, currentChallengeInfo);

        ToggleBattleEnvironment(true);

        SpawnEnemies();

        SpawnPlayers();

        // Need to assign side first here
        AssignArgumentSides(true);

        CheckCharacterInfoConditions(); // If Character info matches the conditions, then apply its effects

        BattleInfoManager.ActiveSubjectChangedEvent -= OnActiveSubjectChanged;
        BattleInfoManager.ActiveSubjectChangedEvent += OnActiveSubjectChanged;

        isInitializing = false;
    }

    private void SpawnEnemies()
    {
        List<GenericEnemy> activeEnemies = new List<GenericEnemy>();
        List<GenericEnemy> reserveEnemies = new List<GenericEnemy>();

        for (int i = 0; i < enemiesToSpawn.Length; i++)
        {
            GenericEnemy enemyPrefab = (GenericEnemy)enemiesToSpawn[i].GetCharacterPrefab();

            if (enemyPrefab == null) continue;

            GenericEnemy enemy = SpawnEnemy(enemyPrefab, i);
            enemy.IsLeader = currentChallengeInfo.isClassBattle ? enemiesToSpawn[i].isLeader : false;

            if (enemy.IsLeader)
                BattleInfoManager.enemyClassLeader = enemy;

            enemy.OnDefeatedEvent += RemoveCharacterInBattle;

            if (i >= enemiesSpawnPositions.Length)
            {
                reserveEnemies.Add(enemy);

                enemy.gameObject.SetActive(false);
            }
            else
                activeEnemies.Add(enemy);
        }

        BattleInfoManager.activeEnemies = activeEnemies;
        BattleInfoManager.reserveEnemies = reserveEnemies;
    }
    private GenericEnemy SpawnEnemy(GenericEnemy enemyPrefab, int spawnIndex)
    {
        GenericEnemy enemy;

        if (spawnIndex >= enemiesSpawnPositions.Length)
        {
            enemy = characterSpawner.SpawnEnemy(enemyPrefab, Vector3.zero,
                                                      Quaternion.identity, true);
        }
        else
        {
            enemy = characterSpawner.SpawnEnemy(enemyPrefab, enemiesSpawnPositions[spawnIndex].position,
                                                       enemiesSpawnPositions[spawnIndex].rotation, true);
        }

        enemy.ToggleBattleMode(true);
        enemy.infiniteHp = useInfiniteHp;

        return enemy;
    }

    private void SpawnPlayers()
    {
        List<PlayerEntity> activePlayers = new List<PlayerEntity>();
        List<PlayerEntity> reserveMembers = new List<PlayerEntity>();

        int activeMemberAmount = activeMembersCount > 0 ? activeMembersCount : PlayerPartyManager.instance.activeMembersCount;

        for (int i = 0; i < playersToSpawn.Count; i++)
        {
            PlayerEntity playerPrefab = (PlayerEntity)playersToSpawn[i].GetCharacterPrefab();

            if (playerPrefab == null) continue;

            PlayerEntity playerEntity = SpawnPlayer(playerPrefab, i);
            playerEntity.IsLeader = currentChallengeInfo.isClassBattle ? playersToSpawn[i].isLeader : false;

            if (playerEntity.IsLeader)
                BattleInfoManager.playerClassLeader = playerEntity;

            playerEntity.OnDefeatedEvent += RemoveCharacterInBattle;

            if (i >= activeMemberAmount)
            {
                reserveMembers.Add(playerEntity);

                playerEntity.gameObject.SetActive(false);
            }
            else
                activePlayers.Add(playerEntity);
        }

        BattleInfoManager.activePlayers = activePlayers;
        BattleInfoManager.reservePlayers = reserveMembers;
    }
    private PlayerEntity SpawnPlayer(PlayerEntity playerPrefab, int spawnIndex)
    {
        PlayerEntity playerEntity;

        if (spawnIndex >= playerSpawnPositions.Length)
        {
            playerEntity = characterSpawner.SpawnPlayer(playerPrefab, Vector3.zero,
                                                                Quaternion.identity, true);
        }
        else
        {
            playerEntity = characterSpawner.SpawnPlayer(playerPrefab, playerSpawnPositions[spawnIndex].position,
                                                                playerSpawnPositions[spawnIndex].rotation, true);
        }

        ControllableEntity controllableEntity = PlayerPartyManager.instance.GetPartyMember(playerEntity.characterId);

        PlayerStatsData characterStatsData = controllableEntity != null ? controllableEntity.playerStatsData : 
            new PlayerStatsData(playerPrefab.characterId, playerPrefab.CharacterStatsBase);

        playerEntity.CopyStats(characterStatsData);

        if (testingSkills != null)
        {
            for (int j = 0; j < testingSkills.Length; j++)
            {
                Item tempItem = new Item(testingSkills[j].ItemID, 1);

                if(!playerEntity.playerStatsData.IsSkillEquipped(tempItem))
                    playerEntity.playerStatsData.EquipSkill(tempItem);
            }
        }

        playerEntity.ToggleBattleMode();
        playerEntity.infiniteHp = useInfiniteHp;

        return playerEntity;
    }

    private void AssignArgumentSides(bool firstTimeAssign)
    {
        if (currentChallengeInfo == null)
            return;

        if(firstTimeAssign)
        {
            playerArgumentSide = currentChallengeInfo.isRequestedByOpponent ? ArgumentSide.Negative : ArgumentSide.Affirmative;
            enemyArgumentSide = currentChallengeInfo.isRequestedByOpponent ? ArgumentSide.Affirmative : ArgumentSide.Negative;
        }
        else
        {
            var nextPlayerArgumentSide = ((int)playerArgumentSide + 1) % 2;
            playerArgumentSide = (ArgumentSide)nextPlayerArgumentSide;

            var nextEnemyArgumentSide = ((int)enemyArgumentSide + 1) % 2;
            enemyArgumentSide = (ArgumentSide)nextEnemyArgumentSide;
        }
    }

    private void OnActiveSubjectChanged(Subjects subject)
    {
        CheckCharacterInfoConditions();
    }

    private void CheckCharacterInfoConditions()
    {
        var allCharacters = BattleInfoManager.GetAllCharacters();

        for (int i = 0; i < allCharacters.Count; i++)
        {
            BattleEntity turnEntity = allCharacters[i];

            if (CharacterInfoEffectsApplier.IsCharacterInfoAffecting<EffectsStatsTag>(turnEntity, null, out EffectsTag[] effectsTags))
            {
                if (effectsTags != null && effectsTags.Length > 0)
                    CharacterInfoEffectsApplier.ApplyInfoEffect(turnEntity, effectsTags);
            }
        }
    }

    private void ToggleBattleEnvironment(bool active)
    {
        if (battleEnvironment != null)
        {
            battleEnvironment.SetActive(active);
        }
    }

    #endregion

    #region Begin Turn Phase

    public void BeginTurn(bool enemyHasDecidedAction)
    {
        BattleInfoManager.ResetAllSelectedInfos();

        // Only raise the Full Round Event if entityOnTurn is not staggered. If is staggered, the event will raise in BattlePhase_Staggered
        //if (!BattleInfoManager.entityOnTurn.IsStaggered())
        if(!enemyHasDecidedAction)
        {
            RaiseOneFullRoundEvent(); // TODO Later staggered duration need to be changed to 2 round.

            BattleInfoManager.RemoveAllActionsSelected();
        }

        actionSelected = BattleSelectionType.Null;
    }

    public void RaiseOneFullRoundEvent()
    {
        oneBattleRoundEventChannel.RaiseFullRoundEvent(null);
    }

    #endregion

    #region Unloading Phase

    public void UnloadingBattleScene(bool retry, bool hasNextChallenge)
    {
        if(!retry)
        {
#if UNITY_EDITOR
            if (LoadingScreen.instance == null && IsInBattle)
            {
                UnityEditor.EditorApplication.isPlaying = false;
                return;
            }
#endif

            UpdateStatsAfterBattle();

            RemoveAllEntities();
      
            if(!hasNextChallenge)
            {
                LoadingScreen.instance.LoadingNewSceneCompletedEvent += OnUnloadingCompleted;
                LoadingScreen.instance.UnloadBattleScene();

                PlayerPartyManager.instance.PrepareAfterBattle();

                IsInBattle = false;
            }

            if (turnEndedEventChannel != null)
            {
                turnEndedEventChannel.OnTurnEnded -= OnTurnExecutionEnded;
            }

            isBattleTriggered = false;

            currentChallengeInfo = null;

            BattleEndedEvent?.Invoke();
        }
    }

    private void OnUnloadingCompleted(string scene)
    {
        LoadingScreen.instance.LoadingNewSceneCompletedEvent -= OnUnloadingCompleted;

        MainCameraController.instance.ToggleCameraBrain(true);
        MainCameraController.instance.ChangeCameraToDefault();

        GameUIManager.instance.mapController.SetMapMode(MapUIController.MapMode.Mini);

        PlayerPartyManager.instance.TogglePlayerInput(true);
    }


    private void UpdateStatsAfterBattle()
    {
        for (int i = 0; i < BattleInfoManager.survivedPlayerList.Count; i++)
        {
            PlayerEntity survivedEntity = BattleInfoManager.survivedPlayerList[i];

            ControllableEntity controllablePlayerEntity = PlayerPartyManager.instance.GetPartyMember(survivedEntity.characterId);

            if (controllablePlayerEntity != null)
                controllablePlayerEntity.CopyStats(survivedEntity.playerStatsData);
        }
    }

    private void RemoveAllEntities()
    {
        for(int i = 0; i < BattleInfoManager.activeEnemies.Count; i++)
        {
            Destroy(BattleInfoManager.activeEnemies[i].gameObject);
        }

        for (int i = 0; i < BattleInfoManager.reserveEnemies.Count; i++)
        {
            Destroy(BattleInfoManager.reserveEnemies[i].gameObject);
        }

        for (int i = 0; i < BattleInfoManager.activePlayers.Count; i++)
        {
            Destroy(BattleInfoManager.activePlayers[i].gameObject);
        }

        for (int i = 0; i < BattleInfoManager.reservePlayers.Count; i++)
        {
            Destroy(BattleInfoManager.reservePlayers[i].gameObject);
        }
    }

    #endregion

    #region Turn Execution Phase

    public void ExecuteTurn(Action callbackOnActionEnded)
    {
        callbackOnTurnEnded = callbackOnActionEnded;

        StartCoroutine(ExecutingTurn());
    }

    private IEnumerator ExecutingTurn()
    {
        var argumentsToExecute = BattleInfoManager.debateWinner == EntityTag.Enemy ? 
            BattleInfoManager.enemyArgumentActionsRegistered : BattleInfoManager.argumentActionsRegistered;

        bool isDoneExecuting = false;
        while(!isDoneExecuting)
        {
            List<BattleAction> sameTargetArguments = new List<BattleAction>();

            int count = argumentsToExecute.Count;

            for (int i = argumentsToExecute.Count - 1; i >= 0; i--)
            {
                if (i == count - 1)
                {
                    sameTargetArguments.Add(argumentsToExecute[i]);
                    argumentsToExecute.RemoveAt(i);
                }
                else
                {
                    if (argumentsToExecute[i].HasSameTarget(sameTargetArguments[0]))
                    {
                        sameTargetArguments.Add(argumentsToExecute[i]);
                        argumentsToExecute.RemoveAt(i);
                    }
                }
            }

            TurnExecutionEvent?.Invoke(sameTargetArguments.ToArray());

            yield return new WaitForSeconds(0.5f);

            yield return new WaitUntil(() => BattleUIManager.instance.turnExecutionPanel.viewState == ViewState.Closed);

            if (argumentsToExecute.Count == 0)
                isDoneExecuting = true;
        }

        OnTurnExecutionEnded();
    }

    private void OnTurnExecutionEnded()
    {
        callbackOnTurnEnded?.Invoke();
        callbackOnTurnEnded = null;
    }

    #endregion

    #region Free Execution

    public void ApplyOrder(BattleOrder selectedOrder)
    {
        if(selectedOrder.GetOrderType() == BattleOrderType.Academical)
        {
            Subjects prevSubject = activeSubject;

            BattleOrder_Academical academicalOrder = (BattleOrder_Academical)selectedOrder;

            BattleInfoManager.currentActiveSubject = academicalOrder.academicSubject;
            _activeSubject = academicalOrder.academicSubject;

            BattleUIManager.instance.battleInfoPanel.ChangeSubject(academicalOrder.academicSubject);

            var allCharacters = BattleInfoManager.GetAllCharacters();

            for(int i = 0; i < allCharacters.Count; i++)
            {
                allCharacters[i].ResetFatigue(activeSubject, prevSubject);
            }
        }
        else if (selectedOrder.GetOrderType() == BattleOrderType.Combat)
        {
            BattleOrder_Combat combatOrder = (BattleOrder_Combat)selectedOrder;

            BattleInfoManager.currentCombatModifier = combatOrder.combatModifier;
        }

        BattleUIManager.instance.battleInfoPanel.ShowOrderInfo(selectedOrder);
    }

    #endregion

    #region End Turn Phase

    public void RemoveCharacterInBattle(BattleEntity battleCharacter)
    {
        battleCharacter.OnDefeatedEvent -= RemoveCharacterInBattle;

        if (battleCharacter.entityTag == EntityTag.Enemy)
        {
            GenericEnemy enemy = battleCharacter as GenericEnemy;

            int characterIndex = BattleInfoManager.activeEnemies.IndexOf(enemy);
            BattleInfoManager.activeEnemies.RemoveAt(characterIndex);

            // To check if enemy run away or defeated --- later call method when enemy forfeited
            if (enemy.isDestroyed)
                BattleInfoManager.defeatedEnemyList.Add(enemy);

            // If is in Class Battle, spawn the next in line reserve member
            if (currentChallengeInfo.isClassBattle && !enemy.IsLeader)
            {
                reserveSpawnIndex = characterIndex;
                nextReserveSpawnIsPlayer = false;
            }
        }
        else
        {
            PlayerEntity playerEntity = battleCharacter as PlayerEntity;

            int characterIndex = BattleInfoManager.activePlayers.IndexOf(playerEntity);

            BattleInfoManager.activePlayers.RemoveAt(characterIndex);
            BattleInfoManager.defeatedPlayerList.Add(playerEntity);

            if (currentChallengeInfo.isClassBattle && !playerEntity.IsLeader)
            {
                reserveSpawnIndex = characterIndex;
                nextReserveSpawnIsPlayer = true;
            }
        }

        RemoveCharacterInBattleEvent?.Invoke(battleCharacter);
    }

    public int EndTurnCheck()
    {
        int endCondition = -1;

        //check if conditions to end battle have been achieved
        // 1 = if recent targets are destroyed
        // 2 = if all party members are defeated / in class battle, if player leader is destroyed
        // 3 = if all enemies are defeated / in class battle, if enemy leader is destroyed
        // 4 = if no one is destroyed

        if (BattleInfoManager.IsPlayerDefeated())
            endCondition = 2;
        else if (BattleInfoManager.IsEnemiesDefeated())
            endCondition = 3;
        else if (BattleInfoManager.IsAnyoneDefeated())
            endCondition = 1;
        else
            endCondition = 4;

        return endCondition;
    }

    public void ProceedNextTurn()
    {
        enemyHasDecidedAction = false;

        AssignArgumentSides(false);
    }

    public void SetChallengeAsComplete()
    {
        if(DailyInfoManager.currentChallengeInfo != null)
            DailyInfoManager.currentChallengeInfo.isCompleted = true;
    }

    #endregion

    #region Enemy Turn Phase

    public void StopEnemyDecidingAction()
    {
        enemyHasDecidedAction = true;
    }

    #endregion

    #region Class Battle methods

    public bool CanSpawnReserve()
    {
        return reserveSpawnIndex > -1;
    }

    public void SpawnNextReserveEnemy()
    {
        GenericEnemy nextEnemy = BattleInfoManager.GetNextReserveEnemy();

        if (nextEnemy == null)
            return;

        nextEnemy.transform.SetPositionAndRotation(enemiesSpawnPositions[reserveSpawnIndex].position, enemiesSpawnPositions[reserveSpawnIndex].rotation);
        nextEnemy.gameObject.SetActive(true);

        nextEnemy.ToggleBattleMode(true);

        BattleInfoManager.activeEnemies.Add(nextEnemy);

        BattleInfoManager.newClassmate = nextEnemy;
        BattleUIManager.instance.enemyStatusPanelController.AddEnemyList(nextEnemy);

        reserveSpawnIndex = -1;
    }

    public void SpawnNextReservePlayer()
    {
        PlayerEntity nextPlayer = BattleInfoManager.GetNextReservePlayer();

        if (nextPlayer == null)
            return;

        nextPlayer.transform.SetPositionAndRotation(playerSpawnPositions[reserveSpawnIndex].position, playerSpawnPositions[reserveSpawnIndex].rotation);
        nextPlayer.gameObject.SetActive(true);

        nextPlayer.ToggleBattleMode();

        BattleInfoManager.activePlayers.Add(nextPlayer);

        BattleInfoManager.newClassmate = nextPlayer;
        BattleUIManager.instance.partyStatusPanelController.AddPlayerList(nextPlayer);

        reserveSpawnIndex = -1;
    }

    #endregion

    public void WinBattle()
    {
        //turnExecutioner.currentSequence.ToggleSequence(false);

        for(int i = 0; i < BattleInfoManager.activePlayers.Count; i++)
        {
            BattleInfoManager.activePlayers[i].WinCelebration();
        }
    }
}
