﻿using UnityEngine;
using System.Collections;

public enum StatusEffectPositionAnchor
{
	Top,
	Center,
	Bottom
}
