﻿using UnityEngine;
using System.Collections;

namespace Intelligensia.Battle.StatusEffects
{
	public class StatusBoost : StatusEffect
	{
		[Header("Status Boost")]
		public StatusBoostType statusBoostType;
		public Transform activationEffectPrefab;
		public Vector3 activationEffectOffset;


		public override void OnAdded()
		{
			base.OnAdded();

			TriggerActivationEffect();
		}


		public override void ReActivate()
		{
			base.ReActivate();

			TriggerActivationEffect();
		}


		protected void TriggerActivationEffect()
		{
			if (activationEffectPrefab != null)
			{
				ObjectPool.Spawn(activationEffectPrefab, transform.position + activationEffectOffset);
			}
		}
	}
}

