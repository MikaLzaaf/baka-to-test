﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Intelligensia.Battle.StatusEffects
{
    public class StatusAilment : StatusEffect
    {
        public StatusAilmentType statusAilmentType;

        public string GetAilmentName()
        {
            return StringHelper.SplitByCapitalizeFirstLetter(statusAilmentType.ToString());
        }
    }
}

