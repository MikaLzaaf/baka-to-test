﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class StatusEffectResistance : ModifiableStats
{
    private const string StatusEffectIconInResources = "Images/StatusEffectIcons/";

    public StatusAilmentType statusEffectType;

    [Range(0, 100)]
    public int resistance;

    public bool isImmune = false;

    protected override int GetStatsAmount()
    {
        return resistance;
    }


    public Sprite GetIcon()
    {
        string path = StatusEffectIconInResources + statusEffectType.ToString();

        Sprite icon = Resources.Load<Sprite>(path);

        return icon;
    }

    public string GetDescription()
    {
        return statusEffectType.ToString();
    }
}
