﻿using UnityEngine;
using System.Collections;
using System;

namespace Intelligensia.Battle.StatusEffects
{
    public abstract class StatusEffect : MonoBehaviour
    {
        public float duration = 1;
        public bool removeOnHurt = false;
        public StatusEffectPositionAnchor positionAnchor;

        public StatusEffectController effectController { get; set; }

        public event Action ReactivateEvent;


        protected float elapsedTime;


        public void Initialize(StatusEffectController effectController)
        {
            Initialize(effectController, this.duration);
        }

        public virtual void Initialize(StatusEffectController effectController, float duration)
        {
            this.effectController = effectController;
            this.duration = duration;

            elapsedTime = 0;

            if (effectController != null)
            {
                effectController.owner.oneBattleRoundEventChannel.OnFullRoundEvent += UpdateDuration;
            }
        }


        public virtual void ReActivate()
        {
            elapsedTime = 0;

            ReactivateEvent?.Invoke();
        }

        protected virtual void UpdateDuration(BattleEntity fullRoundEntity)
        {
            if (fullRoundEntity == effectController.owner)
            {
                elapsedTime += 1;

                if (elapsedTime > duration)
                {
                    effectController.owner.oneBattleRoundEventChannel.OnFullRoundEvent -= UpdateDuration;

                    effectController.Remove(this);
                }
            }
        }


        //protected virtual void Update()
        //{
        //    elapsedTime += Time.deltaTime;

        //    if (elapsedTime > duration)
        //    {
        //        elapsedTime = 0;

        //        effectController.Remove(this);
        //    }
        //}


        protected virtual void OnEnable()
        {
        }


        protected virtual void OnDisable()
        {
            elapsedTime = 0;
        }


        public virtual void OnAdded()
        {
        }


        public virtual void OnRemoved()
        {
        }
    }
}

