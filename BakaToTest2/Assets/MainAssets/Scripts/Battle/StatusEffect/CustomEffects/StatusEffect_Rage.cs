﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Intelligensia.Battle.StatusEffects
{
	public class StatusEffect_Rage : StatusAilment
	{
		public float defenseMultiplier = 0.8f;
		public float attackMultiplier = 1.2f;
		public float receiveCritModifier = 10f;

		private float originalReceiveCritModifier;
		private float originalDefenseMultiplier;
		private float originalAttackMultiplier;

		public override void OnAdded()
		{
			base.OnAdded();

			originalDefenseMultiplier = effectController.owner.initialDefenseMultiplier;
			originalAttackMultiplier = effectController.owner.initialAttackMultiplier;
			originalReceiveCritModifier = effectController.owner.initialReceiveCritModifier;

			effectController.owner.defenseMultiplier = defenseMultiplier;
			effectController.owner.attackMultiplier = attackMultiplier;
			effectController.owner.receiveCritModifier = receiveCritModifier;
		}


		public override void OnRemoved()
		{
			base.OnRemoved();

			effectController.owner.defenseMultiplier = originalDefenseMultiplier;
			effectController.owner.attackMultiplier = originalAttackMultiplier;
			effectController.owner.receiveCritModifier = originalReceiveCritModifier;
		}
	}
}

