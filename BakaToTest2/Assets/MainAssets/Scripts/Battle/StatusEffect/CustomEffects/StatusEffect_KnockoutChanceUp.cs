﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Intelligensia.Battle.StatusEffects
{
    public class StatusEffect_KnockoutChanceUp : StatusAilment
    {
        public float receiveCritModifier = 5f;

        private float originalReceiveCritModifier;

        public override void OnAdded()
        {
            base.OnAdded();

            originalReceiveCritModifier = effectController.owner.receiveCritModifier;

            effectController.owner.receiveCritModifier = receiveCritModifier;
        }


        public override void OnRemoved()
        {
            base.OnRemoved();

            effectController.owner.receiveCritModifier = originalReceiveCritModifier;
        }
    }
}