﻿using UnityEngine;
using System.Collections;

namespace Intelligensia.Battle.StatusEffects
{
    public class StatusEffect_StatusResistanceUp : StatusBoost
    {
        public float statusResistanceMultiplier = 1.5f;


        private float originalStatusResistanceMultiplier;


        public override void OnAdded()
        {
            base.OnAdded();

            originalStatusResistanceMultiplier = effectController.owner.initialStatusResistanceMultiplier;

            if (duration > 100)
            {
                effectController.owner.permanentStatusResistanceMultiplier = statusResistanceMultiplier;
            }
            else
            {
                effectController.owner.statusResistanceMultiplier = statusResistanceMultiplier;
            }
        }


        public override void OnRemoved()
        {
            base.OnRemoved();

            if (duration > 100)
            {
                effectController.owner.permanentStatusResistanceMultiplier = originalStatusResistanceMultiplier;
            }
            else
            {
                effectController.owner.statusResistanceMultiplier = originalStatusResistanceMultiplier;
            }
        }
    }
}

