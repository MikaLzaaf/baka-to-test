﻿using System.Collections;
using UnityEngine;

namespace Intelligensia.Battle.StatusEffects
{
	public class StatusEffect_BreakLivesUp : StatusAilment
	{
		public int breakMultiplier = 2;

		private int originalBreakMultiplier;

		public override void OnAdded()
		{
			base.OnAdded();

			originalBreakMultiplier = effectController.owner.initialBreakAmount;

			if (duration > 100)
			{
				effectController.owner.permanentLiveBreakAmount = breakMultiplier;
			}
			else
			{
				effectController.owner.liveBreakAmount = breakMultiplier;
			}
		}


		public override void OnRemoved()
		{
			base.OnRemoved();

			if (duration > 100)
			{
				effectController.owner.permanentLiveBreakAmount = originalBreakMultiplier;
			}
			else
			{
				effectController.owner.liveBreakAmount = originalBreakMultiplier;
			}

		}
	}
}

