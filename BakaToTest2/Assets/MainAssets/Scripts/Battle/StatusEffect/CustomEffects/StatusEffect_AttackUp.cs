﻿using UnityEngine;
using System.Collections;

namespace Intelligensia.Battle.StatusEffects
{
	public class StatusEffect_AttackUp : StatusBoost
	{
		public float attackMultiplier = 1.2f;

		private float originalAttackMultiplier;

		public override void OnAdded()
		{
			base.OnAdded();

			originalAttackMultiplier = effectController.owner.initialAttackMultiplier;

			if (duration > 100)
			{
				effectController.owner.permanentAttackMultiplier = attackMultiplier;
			}
			else
			{
				effectController.owner.attackMultiplier = attackMultiplier;
			}
		}


		public override void OnRemoved()
		{
			base.OnRemoved();

			if (duration > 100)
			{
				effectController.owner.permanentAttackMultiplier = originalAttackMultiplier;
			}
			else
			{
				effectController.owner.attackMultiplier = originalAttackMultiplier;
			}
		}
	}
}

