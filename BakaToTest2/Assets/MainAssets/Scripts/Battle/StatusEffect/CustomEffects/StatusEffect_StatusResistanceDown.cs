﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Intelligensia.Battle.StatusEffects
{
    public class StatusEffect_StatusResistanceDown : StatusAilment
    {
        public float statusResistanceMultiplier = 0.5f;


        private float originalStatusResistanceMultiplier;


        public override void OnAdded()
        {
            base.OnAdded();

            originalStatusResistanceMultiplier = effectController.owner.initialStatusResistanceMultiplier;

            if (duration > 100)
            {
                effectController.owner.permanentStatusResistanceMultiplier = statusResistanceMultiplier;
            }
            else
            {
                effectController.owner.statusResistanceMultiplier = statusResistanceMultiplier;
            }
        }


        public override void OnRemoved()
        {
            base.OnRemoved();

            if (duration > 100)
            {
                effectController.owner.permanentStatusResistanceMultiplier = originalStatusResistanceMultiplier;
            }
            else
            {
                effectController.owner.statusResistanceMultiplier = originalStatusResistanceMultiplier;
            }
        }
    }
}

