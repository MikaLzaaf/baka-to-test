﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Intelligensia.Battle.StatusEffects
{
	public class StatusEffect_DefenseDown : StatusAilment
	{
		public float defenseMultiplier = 0.8f;

		private float originalDefenseMultiplier;


		public override void OnAdded()
		{
			base.OnAdded();

			originalDefenseMultiplier = effectController.owner.initialDefenseMultiplier;

			if (duration > 100)
			{
				effectController.owner.permanentDefenseMultiplier = defenseMultiplier;
			}
			else
			{
				effectController.owner.defenseMultiplier = defenseMultiplier;
			}
		}


		public override void OnRemoved()
		{
			base.OnRemoved();

			if (duration > 100)
			{
				effectController.owner.permanentDefenseMultiplier = originalDefenseMultiplier;
			}
			else
			{
				effectController.owner.defenseMultiplier = originalDefenseMultiplier;
			}

		}
	}
}

