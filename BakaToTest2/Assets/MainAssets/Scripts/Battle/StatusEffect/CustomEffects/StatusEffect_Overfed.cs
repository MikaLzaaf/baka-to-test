﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Intelligensia.Battle.StatusEffects
{
	public class StatusEffect_Overfed : StatusAilment
	{
		public float defenseMultiplier = 0.8f;
		public float receiveCritModifier = 10f;

		private float originalReceiveCritModifier;
		private float originalDefenseMultiplier;

		public override void OnAdded()
		{
			base.OnAdded();

			originalDefenseMultiplier = effectController.owner.initialDefenseMultiplier;
			originalReceiveCritModifier = effectController.owner.initialReceiveCritModifier;

			effectController.owner.defenseMultiplier = defenseMultiplier;
			effectController.owner.receiveCritModifier = receiveCritModifier;
		}


		public override void OnRemoved()
		{
			base.OnRemoved();

			effectController.owner.defenseMultiplier = originalDefenseMultiplier;
			effectController.owner.receiveCritModifier = originalReceiveCritModifier;
		}
	}

}
