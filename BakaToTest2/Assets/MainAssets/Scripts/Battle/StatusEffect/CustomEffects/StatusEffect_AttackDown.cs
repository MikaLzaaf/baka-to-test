﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Intelligensia.Battle.StatusEffects
{
	public class StatusEffect_AttackDown : StatusAilment
	{
		public float attackMultiplier = 0.8f;


		private float originalAttackMultiplier;


		public override void OnAdded()
		{
			base.OnAdded();

			originalAttackMultiplier = effectController.owner.initialAttackMultiplier;

			if (duration > 100)
			{
				effectController.owner.permanentAttackMultiplier = attackMultiplier;
			}
			else
			{
				effectController.owner.attackMultiplier = attackMultiplier;
			}
		}


		public override void OnRemoved()
		{
			base.OnRemoved();

			if (duration > 100)
			{
				effectController.owner.permanentAttackMultiplier = originalAttackMultiplier;
			}
			else
			{
				effectController.owner.attackMultiplier = originalAttackMultiplier;
			}
		}
	}
}

