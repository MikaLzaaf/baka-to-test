﻿using UnityEngine;
using System.Collections;

namespace Intelligensia.Battle.StatusEffects
{
	public class StatusEffect_DefenseUp : StatusBoost
	{
		public float defenseMultiplier = 1.2f;

		private float originalDefenseMultiplier;


		public override void OnAdded()
		{
			base.OnAdded();

			originalDefenseMultiplier = effectController.owner.initialDefenseMultiplier;

			if (duration > 100)
			{
				effectController.owner.permanentDefenseMultiplier = defenseMultiplier;
			}
			else
			{
				effectController.owner.defenseMultiplier = defenseMultiplier;
			}
		}


		public override void OnRemoved()
		{
			base.OnRemoved();

			if (duration > 100)
			{
				effectController.owner.permanentDefenseMultiplier = originalDefenseMultiplier;
			}
			else
			{
				effectController.owner.defenseMultiplier = originalDefenseMultiplier;
			}
		}
	}
}

