﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Intelligensia.Battle.StatusEffects
{
    public class StatusEffect_Sleep : StatusAilment
    {
		public float receiveCritModifier = 10f;

		private float originalReceiveCritModifier;

		public override void OnAdded()
		{
			base.OnAdded();

			effectController.owner.actionSelected = BattleSelectionType.Null;

			originalReceiveCritModifier = effectController.owner.initialReceiveCritModifier;
			effectController.owner.receiveCritModifier = receiveCritModifier;
		}


		public override void OnRemoved()
		{
			base.OnRemoved();

			effectController.owner.receiveCritModifier = originalReceiveCritModifier;
		}
	}
}

