﻿using UnityEngine;
using System.Collections;

namespace Intelligensia.Battle.StatusEffects
{
    public class StatusEffect_Stun : StatusAilment
    {
		public float receiveCritModifier = 100f;

		private float originalReceiveCritModifier;
		public Subjects subjectOnAdded { get; private set; }

		public override void OnAdded()
		{
			base.OnAdded();

			subjectOnAdded = BattleInfoManager.currentActiveSubject;

			SubjectStats currentSubjectStats = effectController.owner.GetSubjectStats(subjectOnAdded);
			if (currentSubjectStats != null)
			{
				currentSubjectStats.IsKnockedOut = true;
			}

			effectController.owner.actionSelected = BattleSelectionType.Null;

			originalReceiveCritModifier = effectController.owner.initialReceiveCritModifier;
			effectController.owner.receiveCritModifier = receiveCritModifier;
		}


		public override void OnRemoved()
		{
			base.OnRemoved();

			SubjectStats currentSubjectStats = effectController.owner.GetSubjectStats(subjectOnAdded);
			if (currentSubjectStats != null)
			{
				currentSubjectStats.IsKnockedOut = false;
			}

			effectController.owner.receiveCritModifier = originalReceiveCritModifier;
		}
	}
}

