﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Intelligensia.Battle.StatusEffects
{
    public class StatusEffect_KnockoutChanceDownBig : StatusBoost
    {
        public float receiveCritModifier = -10f;

        private float originalReceiveCritModifier;

        public override void OnAdded()
        {
            base.OnAdded();

            originalReceiveCritModifier = effectController.owner.receiveCritModifier;

            effectController.owner.receiveCritModifier = receiveCritModifier;
        }


        public override void OnRemoved()
        {
            base.OnRemoved();

            effectController.owner.receiveCritModifier = originalReceiveCritModifier;
        }
    }
}
