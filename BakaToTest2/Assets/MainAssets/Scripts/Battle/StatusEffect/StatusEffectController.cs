﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Intelligensia.Battle.StatusEffects;

public class StatusEffectController : MonoBehaviour
{
    public event Action<StatusEffect> EffectAddedEvent;
    public event Action<StatusEffect> EffectRemovedEvent;


    private const string StatusEffectPrefabDirectorynResources = "StatusEffects";


    public BattleEntity owner;

	public StatusEffectSize effectSize = StatusEffectSize.Medium;

	public Vector3 positionOffsetTop;
	public Vector3 positionOffsetCenter;
	public Vector3 positionOffsetBottom;

	public List<StatusAilmentType> statusAilmentImmunityList = new List<StatusAilmentType>();

    public List<StatusEffectResistance> statusEffectResistances; // Value depends on equipment & inner resistance


    private Dictionary<StatusAilmentType, StatusEffectResistance> _statusEffectResistancesLookup;
    public Dictionary<StatusAilmentType, StatusEffectResistance> statusEffectResistancesLookup
    {
        get
        {
            if (_statusEffectResistancesLookup == null)
            {
                _statusEffectResistancesLookup = new Dictionary<StatusAilmentType, StatusEffectResistance>();

                if (statusEffectResistances != null && statusEffectResistances.Count > 0)
                {
                    foreach (StatusEffectResistance statResistance in statusEffectResistances)
                    {
                        _statusEffectResistancesLookup.Add(statResistance.statusEffectType, statResistance);
                    }
                }
            }

            return _statusEffectResistancesLookup;
        }
    }

    protected List<StatusEffect> effects = new List<StatusEffect>();
    public List<StatusEffect> Effects => effects;



    private StatusEffectResistance GetResistance(StatusAilmentType ailmentType)
    {
        statusEffectResistancesLookup.TryGetValue(ailmentType, out StatusEffectResistance effectResistance);

        return effectResistance;
    }


    public void RecalculateEffectiveResistance(PlayerEquipment playerEquipment)
    {
        if (owner == null || playerEquipment == null || playerEquipment.equippedItems.Count == 0)
            return;

        PlayerEntity playerEntity = (PlayerEntity)owner;
        statusEffectResistances = playerEntity.playerStatsData.currentInnerStatusEffectResistances;

        List<Item> playerEquipments = playerEquipment.equippedItems;

        for (int i = 0; i < playerEquipments.Count; i++)
        {
            EquipmentItemBaseValue equipmentBase = playerEquipments[i].itemValue as EquipmentItemBaseValue;

            for(int j = 0; j < equipmentBase.statusEffectResistances.Count; j++)
            {
                StatusEffectResistance equipmentResistance = equipmentBase.statusEffectResistances[j];

                StatusEffectResistance ownerResistance = GetResistance(equipmentResistance.statusEffectType);

                ownerResistance.AddModifier(equipmentBase.EquipmentSection.ToString(), equipmentResistance.resistance);
            }
        }
    }


	public bool IsImmune<T>() where T : StatusEffect
	{
		if (statusAilmentImmunityList == null ||
		    statusAilmentImmunityList.Count == 0)
		{
			return false;
		}

		if (typeof(T) == typeof(StatusEffect_Stun) && statusAilmentImmunityList.Contains(StatusAilmentType.Stun))
			return true;

        if (typeof(T) == typeof(StatusEffect_Confused) && statusAilmentImmunityList.Contains(StatusAilmentType.Confused))
            return true;

        if (typeof(T) == typeof(StatusEffect_Overfed) && statusAilmentImmunityList.Contains(StatusAilmentType.Overfed))
            return true;

        if (typeof(T) == typeof(StatusEffect_Sleep) && statusAilmentImmunityList.Contains(StatusAilmentType.Sleep))
            return true;

        if (typeof(T) == typeof(StatusEffect_Rage) && statusAilmentImmunityList.Contains(StatusAilmentType.Rage))
            return true;

        if (typeof(T) == typeof(StatusEffect_AttackDown) && statusAilmentImmunityList.Contains(StatusAilmentType.AttackDown))
            return true;

        if (typeof(T) == typeof(StatusEffect_DefenseDown) && statusAilmentImmunityList.Contains(StatusAilmentType.DefenseDown))
            return true;

        if (typeof(T) == typeof(StatusEffect_StatusResistanceDown) && statusAilmentImmunityList.Contains(StatusAilmentType.StatusResistanceDown))
            return true;

        if (typeof(T) == typeof(StatusEffect_BreakLivesUp) && statusAilmentImmunityList.Contains(StatusAilmentType.BreakLivesUp))
            return true;

        return false;
	}


    public void InflictStatusAilment(BattleEntity caster, SkillEffects_Offensive skillEffect)
    {
        // Need to calculate hit probability first before add ailment
        // (Caster's Intelligence + Target's Intelligence) x (Status Effect Hit Probability - Target's Resistance) %
        // E.g. (97 + 97) x (50% - 50%) = 0% 

        for(int i = 0; i < skillEffect.statusEffectsToInflict.Count; i++)
        {
            StatusAilment statusAilment = skillEffect.statusEffectsToInflict[i];

            StatusEffectResistance effectResistance = GetResistance(statusAilment.statusAilmentType);

            if (effectResistance != null && effectResistance.isImmune)
                continue;

            int resistanceValue = effectResistance == null ? 0 : Mathf.RoundToInt(effectResistance.finalValue * owner.statusResistanceMultiplier * owner.permanentStatusResistanceMultiplier);

            int ailmentHitRate = (caster.intelligence + owner.intelligence) * ((skillEffect.statusEffectHitProbability - resistanceValue) / 100);
            //Debug.Log(caster.intelligence + " // " + owner.intelligence + " // " + skillEffect.statusEffectHitProbability + " // " + resistanceValue);
            int hit = UnityEngine.Random.Range(0, 100);
            //Debug.Log("Ailment Hit rate " + hit + " // " + ailmentHitRate);
            if (hit < ailmentHitRate)
            {
                AddStatusAilment(statusAilment.statusAilmentType);
            }
        }
    }

    public void AddStatusAilment(StatusAilmentType statusAilmentType, float? duration = null)
	{
        if (statusAilmentType == StatusAilmentType.Stun)
		{
			Add<StatusEffect_Stun>(duration);
		}
		else if (statusAilmentType == StatusAilmentType.Confused)
		{
            Add<StatusEffect_Confused>(duration);
        }
        else if (statusAilmentType == StatusAilmentType.Overfed)
        {
            Add<StatusEffect_Overfed>(duration);
        }
        else if (statusAilmentType == StatusAilmentType.Sleep)
        {
            Add<StatusEffect_Sleep>(duration);
        }
        else if (statusAilmentType == StatusAilmentType.Rage)
        {
            Add<StatusEffect_Rage>(duration);
        }
        else if (statusAilmentType == StatusAilmentType.AttackDown)
        {
            if (HasEffect<StatusEffect_AttackUp>()) // Remove the opposite effect first for any stats down ailment
                RemoveEffect<StatusEffect_AttackUp>();
            else
                Add<StatusEffect_AttackDown>(duration);
        }
        else if (statusAilmentType == StatusAilmentType.DefenseDown)
        {
            if (HasEffect<StatusEffect_DefenseUp>()) // Remove the opposite effect first for any stats down ailment
                RemoveEffect<StatusEffect_DefenseUp>();
            else
                Add<StatusEffect_DefenseDown>(duration);
        }
        else if (statusAilmentType == StatusAilmentType.StatusResistanceDown) 
        {
            if (HasEffect<StatusEffect_StatusResistanceUp>()) // Remove the opposite effect first for any stats down ailment
                RemoveEffect<StatusEffect_StatusResistanceUp>();
            else
                Add<StatusEffect_StatusResistanceDown>(duration);
        }
        else if (statusAilmentType == StatusAilmentType.BreakLivesUp)
        {
            Add<StatusEffect_BreakLivesUp>(duration);
        }
        else if (statusAilmentType == StatusAilmentType.KnockoutChanceUp)
        {
            if (HasEffect<StatusEffect_KnockoutChanceDown>()) // Remove the opposite effect first for any stats down ailment
                RemoveEffect<StatusEffect_KnockoutChanceDown>();
            else if(HasEffect<StatusEffect_KnockoutChanceDownBig>()) // If already has the BigDown effect, remove the BigDown effect and add the normal Down effect to replace it
            {
                RemoveEffect<StatusEffect_KnockoutChanceDownBig>();
                Add<StatusEffect_KnockoutChanceDown>(duration, true);
            }
            else
                Add<StatusEffect_KnockoutChanceUp>(duration, true);
        }
        else if (statusAilmentType == StatusAilmentType.KnockoutChanceUpBig)
        {
            if (HasEffect<StatusEffect_KnockoutChanceDownBig>()) // Remove the opposite effect first for any stats down ailment
                RemoveEffect<StatusEffect_KnockoutChanceDownBig>();
            else if (HasEffect<StatusEffect_KnockoutChanceDown>()) // If already has the normal Down effect, remove the Down effect and add the normal Up effect to replace it
            {
                RemoveEffect<StatusEffect_KnockoutChanceDown>();
                Add<StatusEffect_KnockoutChanceUp>(duration, true);
            }
            else
                Add<StatusEffect_KnockoutChanceUpBig>(duration, true);
        }
    }

    public void AddStatusBoost(StatusBoostType statusBoostType, float? duration = null)
    {
        if (statusBoostType == StatusBoostType.AttackUp)
        {
            if (HasEffect<StatusEffect_AttackDown>()) // Remove the opposite effect first for any stats down ailment
                RemoveEffect<StatusEffect_AttackDown>();
            else
                Add<StatusEffect_AttackUp>(duration);
        }
        else if (statusBoostType == StatusBoostType.DefenseUp)
        {
            if (HasEffect<StatusEffect_DefenseDown>()) // Remove the opposite effect first for any stats down ailment
                RemoveEffect<StatusEffect_DefenseDown>();
            else
                Add<StatusEffect_DefenseUp>(duration);
        }
        else if (statusBoostType == StatusBoostType.StatusResistanceUp)
        {
            if (HasEffect<StatusEffect_StatusResistanceDown>()) // Remove the opposite effect first for any stats down ailment
                RemoveEffect<StatusEffect_StatusResistanceDown>();
            else
                Add<StatusEffect_StatusResistanceUp>(duration);
        }
        else if (statusBoostType == StatusBoostType.KnockoutChanceDown)
        {
            if (HasEffect<StatusEffect_KnockoutChanceUp>()) // Remove the opposite effect first for any stats down ailment
                RemoveEffect<StatusEffect_KnockoutChanceUp>();
            else if (HasEffect<StatusEffect_KnockoutChanceUpBig>())
            {
                RemoveEffect<StatusEffect_KnockoutChanceUpBig>();
                Add<StatusEffect_KnockoutChanceUp>(duration, true);
            }
            else
                Add<StatusEffect_KnockoutChanceDown>(duration, true);
        }
        else if (statusBoostType == StatusBoostType.KnockoutChanceDownBig)
        {
            if (HasEffect<StatusEffect_KnockoutChanceUpBig>()) // Remove the opposite effect first for any stats down ailment
                RemoveEffect<StatusEffect_KnockoutChanceUpBig>();
            else if (HasEffect<StatusEffect_KnockoutChanceUp>())
            {
                RemoveEffect<StatusEffect_KnockoutChanceUp>();
                Add<StatusEffect_KnockoutChanceDown>(duration, true);
            }
            else
                Add<StatusEffect_KnockoutChanceDownBig>(duration, true);
        }
    }

    protected void Add<T>(float? duration = null, bool canStack = false) where T : StatusEffect
    {
		if (!enabled)
		{
			return;
		}

		// Don't add the effect if it has the immunity
		if (IsImmune<T>())
		{
			return;
		}

        if(!canStack)
        {
            // Overwite existing effect of the same type
            T existingEffect = GetEffect<T>(duration > 100);

            if (existingEffect != null)
            {
                if (duration.HasValue)
                {
                    existingEffect.duration = duration.Value;
                }

                // Reset the effect elapsed time
                existingEffect.ReActivate();
                return;
            }
        }
		
        // Create a new effect
        StatusEffect effect = CreateStatusEffect<T>();

        if (effect != null)
        {
			if (duration.HasValue)
			{
				effect.Initialize(this, duration.Value);
			}
			else
			{
				effect.Initialize(this);
			}

			effects.Add(effect);
			effect.OnAdded();

            EffectAddedEvent?.Invoke(effect);
        }
    }
   
    internal void Add<T>(string v) where T : StatusEffect
    {
        throw new NotImplementedException();
    }

    public bool HasEffect<T>(bool hasPermanentEffect = false) where T : StatusEffect
    {
        for (int i = 0; i < effects.Count; i++)
        {
            if(hasPermanentEffect)
            {
                if (effects[i] is T && effects[i].duration >= 100)
                {
                    return true;
                }
            }
            else
            {
                if (effects[i] is T && effects[i].duration < 100)
                {
                    return true;
                }
            }
        }

        return false;
    }

    public bool HasStaggeringEffect()
    {
        for(int i = 0; i < effects.Count; i++)
        {
            if(effects[i] is StatusEffect_Stun || effects[i] is StatusEffect_Confused || effects[i] is StatusEffect_Overfed ||
                effects[i] is StatusEffect_Sleep || effects[i] is StatusEffect_Rage)
            {
                return true;
            }
        }

        return false;
    }

    public bool HasEffectWithName(string name)
    {
        for (int i = 0; i < effects.Count; i++)
        {
            if (effects[i].name.Contains(name))
            {
                return true;
            }
        }

        return false;
    }

    public bool HasAnyEffect()
    {
        return effects.Count > 0;
    }

    public bool HasAilment(out StatusAilment statusAilment)
    {
        statusAilment = null;

        for (int i = 0; i < effects.Count; i++)
        {
            if (effects[i] is StatusAilment)
            {
                statusAilment = (StatusAilment)effects[i];
                return true;
            }
        }

        return false;
    }

    public bool HasBoost(out StatusBoost statusBoost)
    {
        statusBoost = null;

        for (int i = 0; i < effects.Count; i++)
        {
            if (effects[i] is StatusBoost)
            {
                statusBoost = (StatusBoost)effects[i];
                return true;
            }
        }

        return false;
    }

    protected T GetEffect<T>(bool getPermanentEffect = false) where T : StatusEffect
	{
		for (int i = 0; i < effects.Count; i++)
		{
            if(getPermanentEffect)
            {
                if (effects[i] is T && effects[i].duration >= 100)
                {
                    return effects[i] as T;
                }
            }
            else
            {
                if (effects[i] is T && effects[i].duration < 100)
                {
                    return effects[i] as T;
                }
            }
		}

		return null;
	}

    public void Remove(StatusEffect effect)
    {
        if (effect != null)
        {
            effects.Remove(effect);
			effect.OnRemoved();

            EffectRemovedEvent?.Invoke(effect);

            ObjectPool.Recycle(effect.gameObject);
        }
    }

    public void RemoveEffect<T>(bool removePermanetEffect = false) where T : StatusEffect
    {
        for (int i = 0; i < effects.Count; i++)
        {
            if(removePermanetEffect)
            {
                if (effects[i] is T && effects[i].duration >= 100)
                {
                    Remove(effects[i]);
                    break;
                }
            }
            else
            {
                if (effects[i] is T && effects[i].duration < 100)
                {
                    Remove(effects[i]);
                    break;
                }
            }
        }
    }

    public void RemoveAilment(StatusAilment ailment)
    {
        for (int i = 0; i < effects.Count; i++)
        {
            if (effects[i] is StatusAilment)
            {
                StatusAilment effect = (StatusAilment)effects[i];

                if(effect.statusAilmentType == ailment.statusAilmentType)
                {
                    Remove(effects[i]);
                    break;
                }
            }
        }
    }

    public void RemoveEffectsOnHurt()
    {
        for (int i = 0; i < effects.Count; i++)
        {
            StatusEffect effect = effects[i];

            if (effect != null)
            {
                if (effect.removeOnHurt)
                {
                    Remove(effect);
                    i--;
                }
            }
        }
    }

    public void ClearAll()
    {
        for (int i = 0; i < effects.Count; i++)
        {
            StatusEffect effect = effects[i];

            if (effect != null)
            {
                Remove(effect);
                i--;
            }
        }

        effects.Clear();
    }

    public StatusEffect CreateStatusEffect<T>() where T : StatusEffect
    {
		// Load prefab of the specified size
		string path = string.Format("{0}/{1}_{2}", StatusEffectPrefabDirectorynResources, typeof(T).Name, effectSize);
		GameObject prefab = Resources.Load<GameObject>(path);

        // Load prefab without specify the size
        if (prefab == null)
		{
			path = string.Format("{0}/{1}", StatusEffectPrefabDirectorynResources, typeof(T).Name);
			prefab = Resources.Load<GameObject>(path);
		}

        if (prefab != null)
        {
            GameObject effectObject = ObjectPool.Spawn(prefab);
            effectObject.transform.SetParent(this.transform);

			StatusEffect effect = effectObject.GetComponent<StatusEffect>();
			effectObject.transform.position = owner.transform.position + GetPositionOffset(effect.positionAnchor);

            return effect;
        }

        return null;
    }

    public Vector3 GetPositionOffset(StatusEffectPositionAnchor anchor)
	{
		Vector3 offset = Vector3.zero;

		switch (anchor)
		{
		case StatusEffectPositionAnchor.Top:
			offset = positionOffsetTop;
			break;
		case StatusEffectPositionAnchor.Center:
			offset = positionOffsetCenter;
			break;
		case StatusEffectPositionAnchor.Bottom:
			offset = positionOffsetBottom;
			break;
		}

		return offset;
	}
}
