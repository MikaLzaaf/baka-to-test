﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Intelligensia.Battle
{
    [System.Serializable]
    public class BattleCombatModifier 
    {
        // The EXCLUDED modifiers are : 
        // - Speed (Referencing to Persona 5)
        // - Item effectiveness (Complicated for balancing)
        // - Skill effectiveness (Complicated for balancing)

        public float damageModifier = 1f;
        public float concentrationModifier = 1f;
        public float agilityModifier = 1f;
        public int critRateModifier = 0;
        public float critDamageModifier = 1f;

        public BattleCombatModifier()
        {

        }
    }
}
