﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Intelligensia.Battle
{
    [CreateAssetMenu(fileName = "New Combat Order", menuName = "ScriptableObject/Battle Order/Combat")]
    public class BattleOrder_Combat : BattleOrder
    {
        public BattleCombatModifier combatModifier;


        public override BattleOrderType GetOrderType()
        {
            return BattleOrderType.Combat;
        }
    }
}

