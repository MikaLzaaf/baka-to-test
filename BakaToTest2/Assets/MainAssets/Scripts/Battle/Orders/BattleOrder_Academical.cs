﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Intelligensia.Battle
{
    [CreateAssetMenu(fileName = "New Academical Order", menuName = "ScriptableObject/Battle Order/Academical")]
    public class BattleOrder_Academical : BattleOrder
    {
        public Subjects academicSubject;


        public override BattleOrderType GetOrderType()
        {
            return BattleOrderType.Academical;
        }
    }
}

