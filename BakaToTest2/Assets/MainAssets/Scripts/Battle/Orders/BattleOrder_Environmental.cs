﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Intelligensia.Battle
{
    [CreateAssetMenu(fileName = "New Academical Order", menuName = "ScriptableObject/Battle Order/Environmental")]
    public class BattleOrder_Environmental : BattleOrder
    {

        public InfoTag[] environmentInfoTags;


        public override BattleOrderType GetOrderType()
        {
            return BattleOrderType.Environmental;
        }
    }

}

