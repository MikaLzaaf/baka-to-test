﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


namespace Intelligensia.Battle
{
    public abstract class BattleOrder : ScriptableObject
    {
        // Should be unique for each person
        public string orderID => name;
        public string orderName;
        public string orderDescription;
        public Sprite orderIcon;

        [SerializeField]
        private UIListItemIcons listIcon = default;
        public string ListIcon
        {
            get { return listIcon.ToString(); }
        }
        //public BattleOrderType orderType;
        public AimedTo aimedTo = AimedTo.All;
        public int orderDuration = -1;


        public int orderCost = 1;
        //public InfoTag[] orderInfoTags;

        public NonControllableEntity orderHolder;

        public UseOrderInBattleEventChannelSO useOrderInBattleEventChannel;

        private Action<BattleOrder> callbackOnUsage;

        private const string InfinityCodeSymbol = "\u221E";


        public abstract BattleOrderType GetOrderType();


        public void SubscribeToChannels(Action<BattleOrder> callbackOnUsage)
        {
            this.callbackOnUsage = callbackOnUsage;

            if (useOrderInBattleEventChannel != null)
            {
                useOrderInBattleEventChannel.OnOrderUsage += ApplyOrder;
            }
        }


        public void UnsubscribeToChannels()
        {
            this.callbackOnUsage = null;

            if (useOrderInBattleEventChannel != null)
            {
                useOrderInBattleEventChannel.OnOrderUsage -= ApplyOrder;
            }
        }


        public void ApplyOrder(BattleOrder battleOrder)
        {
            callbackOnUsage?.Invoke(battleOrder);
        }

        public string GetDurationString()
        {
            return orderDuration < 0 ? InfinityCodeSymbol : orderDuration.ToString();
        }
    }

    public enum BattleOrderType
    {
        Undefined,
        Environmental,
        Academical,
        Combat
    }
}

