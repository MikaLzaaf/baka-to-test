﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Intelligensia.Battle;

public class BattleOrdersManager : Singleton<BattleOrdersManager>
{
    // This manager is structurally the same as PlayerInventoryManager since it is only used for managing BattleOrders

    // Orders can be learned by :
    // 1. Making acquaintances with the teachers
    // 2. Learning from a book
    // 3. Some other undefined method yet



    [Header("Testing")]
    public bool isTesting = false;
    public List<BattleOrder> testOrders;

    public List<BattleOrder> battleOrdersLearned { get; private set; }

    private Dictionary<string, BattleOrder> _ordersLookup;
    private Dictionary<string, BattleOrder> ordersLookup
    {
        get
        {
            if(_ordersLookup == null)
            {
                _ordersLookup = new Dictionary<string, BattleOrder>();

                if(battleOrdersLearned != null)
                {
                    foreach(BattleOrder order in battleOrdersLearned)
                    {
                        _ordersLookup.Add(order.orderName, order);
                    }
                }
            }

            return _ordersLookup;
        }
    }


    private const string BattleOrdersInResources = "BattleOrders/";


    private void Start()
    {
        if(isTesting)
        {
            battleOrdersLearned = testOrders;
        }
    }



    public BattleOrder GetOrder(string orderName)
    {
        ordersLookup.TryGetValue(orderName, out BattleOrder battleOrder);

        return battleOrder;
    }


    public void AddOrder(BattleOrder order)
    {
        if(GetOrder(order.orderName) != null)
            return;

        battleOrdersLearned.Add(order);
        ordersLookup.Add(order.orderName, order);
    }


    public void RemoveOrder(BattleOrder order)
    {
        if (GetOrder(order.orderName) == null)
            return;

        battleOrdersLearned.Remove(order);
        ordersLookup.Remove(order.orderName);
    }


    public BattleOrder[] GetOrderByType(BattleOrderType orderType)
    {
        List<BattleOrder> categoryOrders = new List<BattleOrder>();

        for(int i = 0; i <battleOrdersLearned.Count; i++)
        {
            if(battleOrdersLearned[i].GetOrderType() == orderType)
            {
                categoryOrders.Add(battleOrdersLearned[i]);
            }
        }

        return categoryOrders.ToArray();
    }


    public void LoadFromSaveData()
    {
        string[] ordersLearned = GameDataManager.gameData.battleOrdersLearned.ToArray();

        if (ordersLearned.Length == 0)
            return;

        for(int i = 0; i < ordersLearned.Length; i++)
        {
            BattleOrder battleOrder = Resources.Load<BattleOrder>(BattleOrdersInResources + ordersLearned);

            if(battleOrder != null)
            {
                AddOrder(battleOrder);
            }
        }
    }


    public List<string> Save()
    {
        if (battleOrdersLearned == null || battleOrdersLearned.Count == 0)
            return null;

        List<string> ordersLearned = new List<string>();

        for(int i = 0; i < battleOrdersLearned.Count; i++)
        {
            ordersLearned.Add(battleOrdersLearned[i].orderName);
        }

        return ordersLearned;
    }
}
