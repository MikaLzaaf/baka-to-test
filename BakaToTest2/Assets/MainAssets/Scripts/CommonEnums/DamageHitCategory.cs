﻿public enum DamageHitCategory
{
    Undefined,
    NormalHit,
    FatalCriticalHit,
    NonFatalCriticalHit,
    Missed,
    Recovery
}
