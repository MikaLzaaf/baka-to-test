public enum DebateEffectType 
{
    // All Effect must follow these rules;
    // 1) Issuing argument cannot directly increase/decrease its point. Can only be done indirectly like depending on the Active Subject.
    // 2) A normal argument can only affect
    // - the previous argument presented from both sides if any
    // - the current issuing argument
    // 3) A starting / closing argument may affect the whole arguments presented by either side
    None,
    IncreaseAllyArgument,
    DecreaseOpponentArgument,
    IncreaseTopicSubjectEffect,
    RemovePreviousOpponentArgument,
    IncreaseAllyArgumentFromStart,
    DecreaseOpponentArgumentFromStart,
    IncreaseAllyArgumentUponClosing,
    DecreaseOpponentArgumentUponClosing
}
