﻿public enum FieldEffectType
{
    Undefined,
    Rainy,
    Noisy,
    Silent
}
