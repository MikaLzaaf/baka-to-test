﻿public enum Difficulty
{
    Easy,
    Normal,
    Hard,
    Expert
}
