﻿
public enum StatusBoostType 
{
    Undefined,
    AttackUp,
    DefenseUp,
    StatusResistanceUp,
    KnockoutChanceDown,
    KnockoutChanceDownBig
}
