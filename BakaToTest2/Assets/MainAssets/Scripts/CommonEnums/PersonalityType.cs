﻿
public enum PersonalityType
{
    Aggressive,
    Cheerful,
    Cautious,
    Timid
}
