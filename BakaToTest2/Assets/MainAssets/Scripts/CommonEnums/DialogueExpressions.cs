﻿
public enum DialogueExpressions
{
    Undefined,
    Happy,
    Angry,
    Sad,
    Surprised,
    Scared,
    Disgusted,
    Indifferent
}
