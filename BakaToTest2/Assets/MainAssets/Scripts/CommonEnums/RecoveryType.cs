﻿public enum RecoveryType
{
    None,
    CureFatigue,
    CureStatusEffect, // Remove Stun, Hunger
    AddStatusBuff // Like ATK up, DEF up
}
