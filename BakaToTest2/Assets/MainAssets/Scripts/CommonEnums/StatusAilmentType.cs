﻿public enum StatusAilmentType
{
    Undefined,
    Stun,
    Confused,
    Overfed,
    Sleep,
    Rage,
    AttackDown,
    DefenseDown,
    StatusResistanceDown,
    BreakLivesUp,
    KnockoutChanceUp,
    KnockoutChanceUpBig
}
