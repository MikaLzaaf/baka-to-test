using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NonControllableEntity : Entity
{
    [Header("Name")]
    [SerializeField] private string entityName = default;
    [SerializeField] CharacterStatsBaseValue statsBaseValue = default;

    public string GetName()
    {
        if (!string.IsNullOrEmpty(entityName))
            return entityName;

        if(statsBaseValue != null)
            return statsBaseValue.characterName;

        return "No Name";
    }


}
