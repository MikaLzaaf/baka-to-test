﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class EntityModelBinder : MonoBehaviour
{
    public event Action<EntityModel> EntityModelSetEvent;


    //public Entity entity;
    public EntityAnimatorController animatorController;
    [Tooltip("The model that is manually picked and does not need to be instantiated. The model must also be parented to this gameObject.")]
    public EntityModel defaultEntityModel;
    [Tooltip("The model prefab that will be instantiated on Awake.")]
    public EntityModel modelPrefab;


#if UNITY_EDITOR
    public Mesh previewMesh;
    public string previewIcon;
#endif


    public EntityModel selectedEntityModel { get; protected set; }


    protected virtual void Awake()
    {
        if (defaultEntityModel != null)
        {
            selectedEntityModel = defaultEntityModel;

            OnEntityModelSet();
        }
        else if (modelPrefab != null)
        {
            ClearModel();

            InstantiateEntityModel();

            OnEntityModelSet();
        }
    }


    public EntityModel InstantiateEntityModel(Transform parent = null)
    {
        if (modelPrefab == null)
        {
            return null;
        }

        EntityModel modelObject = (EntityModel)Instantiate(modelPrefab);

        // Ensure that the name of the model is same as the prefab
        modelObject.name = modelPrefab.name;

        Transform modelParent = parent == null ? this.transform : parent;

        modelObject.transform.SetParent(modelParent, false);

        // Follow the position and rotation of the prefab
        modelObject.transform.localPosition = modelPrefab.transform.position;
        modelObject.transform.localRotation = modelPrefab.transform.rotation;

        selectedEntityModel = modelObject;

        return modelObject;
    }


    protected virtual void OnEntityModelSet()
    {
        //selectedEntityModel.Initialize(entity);

        animatorController.animator = selectedEntityModel.animator;

        //entity.mainRenderer = selectedEntityModel.mainRenderer;

        //RendererFlasher playerRendererFlasher = entity.objectFlasher as RendererFlasher;
        //if (playerRendererFlasher != null)
        //{
        //    playerRendererFlasher.ClearRenderers();
        //    playerRendererFlasher.AddRenderer(entity.mainRenderer);
        //}

        EntityModelSetEvent?.Invoke(selectedEntityModel);
    }


    public void ClearModel()
    {
        // Destroy any model in child
        int count = transform.childCount;

        if (count > 0)
        {
            for (int i = count - 1; i >= 0; i--)
            {
                Destroy(transform.GetChild(i).gameObject);
            }
        }
    }


    //#if UNITY_EDITOR
    //    private void OnDrawGizmos()
    //    {
    //        if (Application.isPlaying)
    //        {
    //            return;
    //        }

    //        if (!string.IsNullOrEmpty(previewIcon))
    //        {
    //            Gizmos.DrawIcon(entity.transform.position + entity.statusEffectController.positionOffsetTop, previewIcon);
    //        }

    //        if (previewMesh != null)
    //        {
    //            Gizmos.color = Colors.Orangered;
    //            Gizmos.DrawWireMesh(previewMesh, transform.position, transform.rotation);
    //        }
    //    }
    //#endif
}
