﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class EntityModel : MonoBehaviour
{
    public Animator animator;
    public Renderer mainRenderer;


    public virtual void Initialize(Entity ownerEntity)
    {
    }
}
