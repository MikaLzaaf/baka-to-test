﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;
using Intelligensia.Battle;
using Random = UnityEngine.Random;

/// <summary>
/// PlayerEntity is supposed to be only for Battle. Not for exploration. Need to separate this into two.
/// </summary>
public class PlayerEntity : BattleEntity
{
    [Header("Player Behaviours")]
    public CharacterId characterId;
    public PlayerCostumeController costumeController;
    //public CharacterInputController playerActionController;

    private const int StressPointAddOnHurt = 5;


    public PlayerCharacterController playerCharacterController
    {
        get
        {
            return (PlayerCharacterController)characterController;
        }
    }

    private PlayerEquipment _playerEquipment;
    public PlayerEquipment playerEquipment
    {
        get
        {
            if(_playerEquipment == null)
            {
                _playerEquipment = new PlayerEquipment(characterId);
            }

            return _playerEquipment;
        }
        set
        {
            _playerEquipment = value;
        }
    }
    
    public PlayerStatsData playerStatsData { get; private set; }

    public override CharacterStatsData characterStatsData
    {
        get
        {
            return playerStatsData;
        }
        protected set
        {
            playerStatsData = (PlayerStatsData)value;
        }
    }


    // Flow of Initializing PlayerEntity in BattleController
    // - Spawn PlayerEntity
    // - ToggleBattleMode (Init Lives & Prepare Battle Skills here)
    // - CopyStats from current PlayerData
    // - Assign turn

    public void CopyStats(PlayerStatsData characterStatsData)
    {
        this.playerStatsData = characterStatsData;

        InitializeStats();
    }

    protected override void InitializeStats()
    {
        entityStats = playerStatsData.GetEffectiveBasicStats();
        academicStats = playerStatsData.GetEffectiveAcademicStats();
        //subjectStats = playerStatsData.currentSubjectStats;

        playerEquipment = PlayerInventoryManager.instance.GetPlayerEquipment(characterId);
        statusEffectController.RecalculateEffectiveResistance(playerEquipment);

        initialAttackMultiplier = attackMultiplier;
        initialDefenseMultiplier = defenseMultiplier;
        initialBreakAmount = liveBreakAmount;
    }

    public override void ApplyDamage(float damage, DamageHitCategory hitCategory, Subjects subjectApplied = Subjects.BahasaMelayu, bool breakDefense = false)
    {
        base.ApplyDamage(damage, hitCategory, subjectApplied, breakDefense);

        if (BattleController.IsInBattle)
        {
            if (hitCategory == DamageHitCategory.FatalCriticalHit || hitCategory == DamageHitCategory.NonFatalCriticalHit)
                BattleInfoManager.currentStressPoint += StressPointAddOnHurt;
        }
    }



    #region Battle related


    public void ToggleBattleMode()
    {
        //playerActionController.canInputAction = false;

        int idleIndex = Random.Range(1, 4);

        animatorController.Idle(idleIndex);

        playerCharacterController.SwitchToRunAnimation(true);

        //playerActionController.SetInputActiveState(false);

        ToggleBattleMode(true);
    }

   

    protected override void PrepareSkillsForBattle()
    {
        inBattleOffenses = new List<InBattleSkill>();
        inBattleSupports = new List<InBattleSkill>();

        //var equippedSkills = GetEquippedSkills();

        for (int i = 0; i < playerStatsData.skillsEquipped.Count; i++)
        {
            Item skillItem = playerStatsData.skillsEquipped[i];

            if (skillItem.itemValue == null)
                continue;

            SkillItemBaseValue skilItemBase = (SkillItemBaseValue)skillItem.itemValue;

            if (skilItemBase.baseSkill == null)
                continue;

            if (skilItemBase.baseSkill is OffensiveSkillSO)
            {
                InBattleSkill offense = new InBattleSkill((OffensiveSkillSO)skilItemBase.baseSkill, this, skilItemBase.IsEnhanced);

                inBattleOffenses.Add(offense);
            }
            else if (skilItemBase.baseSkill is SupportSkillSO)
            {
                InBattleSkill support = new InBattleSkill((SupportSkillSO)skilItemBase.baseSkill, this, skilItemBase.IsEnhanced);

                inBattleSupports.Add(support);
            }
        }
    }


    // TODO : This is based on bond level actually. Later need to create & integrate them here.
    public float GetRelationshipLevelProbability()
    {
        return 1f;
    }
   
    public InBattleSkill[] GetOffensiveSkillsBySubject(Subjects subject, bool shouldGetAllSkills = false)
    {
        List<InBattleSkill> offensiveSkillsLearned = new List<InBattleSkill>();

        for(int i = 0; i < inBattleOffenses.Count; i++)
        {
            if(inBattleOffenses[i].GetSubjectBased() == subject || shouldGetAllSkills)
            {
                offensiveSkillsLearned.Add(inBattleOffenses[i]);
            }
        }

        return offensiveSkillsLearned.ToArray();
    }

    public InBattleSkill GetInBattleSkill(string id)
    {
        for(int i = 0; i < inBattleOffenses.Count; i++)
        {
            if(inBattleOffenses[i].GetSkillId() == id)
            {
                return inBattleOffenses[i];
            }
        }

        return null;
    }

    protected override void UnhideSubjectStats(Subjects subject)
    {
        base.UnhideSubjectStats(subject);

        SubjectStats subjectStats = playerStatsData.currentAcademicStats.GetSubjectStats(subject);

        if (subjectStats != null && subjectStats.IsHidden)
        {
            subjectStats.UnhideStats();
        }
    }

    public DebateHelper GetDebateHelper()
    {
        return CharacterStatsBase.GetDebateHelper(playerStatsData.debateHelperLevel);
    }

    #endregion

    #region Useless for now
    //public void Vanish()
    //{
    //    if (destructionEffectPrefab != null)
    //    {
    //        ObjectPool.Spawn(destructionEffectPrefab, transform.position);
    //    }

    //    if (characterController != null)
    //    {
    //        characterController.StopMoving();
    //    }

    //    this.gameObject.SetActive(false);

    //    //AudioManager.PlaySoundOneShot(deathSound);

    //    //if (isProcessingHurt)
    //    //{
    //    //    EndProcessingHurt();
    //    //}
    //}


    public IEnumerator Respawn(float activeDelay, bool resetStats)
    {
        //Vector3 spawnPosition = startPosition;
        //Quaternion spawnRotation = startRotation;

        //if (lastCheckpoint != null)
        //{
        //    spawnPosition = lastCheckpoint.spawnPoint.position;
        //    spawnRotation = lastCheckpoint.spawnPoint.rotation;
        //}

        //transform.position = spawnPosition;
        //transform.rotation = spawnRotation;

        yield return new WaitForSeconds(activeDelay);

        OnReadyToActivateWhenRespawning(resetStats);
    }


    protected virtual void OnReadyToActivateWhenRespawning(bool resetStats)
    {
        this.gameObject.SetActive(true);

        //if (respawnDamage > 0)
        //{
            //ApplyDamage(respawnDamage, transform.position, null, true);
        //}

        if (resetStats)
        {
            //ResetStats();
        }

        //if (spawnEffectPrefab != null)
        //{
        //    ObjectPool.Spawn(spawnEffectPrefab, transform.position + spawnEffectPositionOffset);
        //}
    }


    //public override void ResetStats()
    //{
    //    base.ResetStats();

    //    energy = energyMax;
    //}

    #endregion

   
}
