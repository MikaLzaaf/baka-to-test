﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationStateMachine : StateMachineBehaviour {

    protected const string AnimState_Dying = "Dying";
    protected const string AnimState_Action = "Action";
    protected const string AnimState_Attack = "Attack";

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);

        EntityAnimatorController animatorController = GetPlayerAnimatorController(animator);

        if (stateInfo.IsName(AnimState_Action + animatorController.generalActionSelected.ToString()))
        {
            animatorController.OnActionComplete();
        }
        else if(stateInfo.IsName(AnimState_Dying))
        {
            animatorController.OnDyingComplete();
        }
        else if (stateInfo.IsName(AnimState_Attack + animatorController.attackTypeSelected.ToString()))
        {
            animatorController.OnAttackComplete();
        }
    }


    private EntityAnimatorController GetPlayerAnimatorController(Animator animator)
    {
        EntityAnimatorController playerAnimatorController = animator.GetComponent<EntityAnimatorController>();

        if (playerAnimatorController == null)
        {
            playerAnimatorController = animator.transform.parent.GetComponent<EntityAnimatorController>();
        }

        return playerAnimatorController;
    }
}
