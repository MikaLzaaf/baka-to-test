﻿using UnityEngine;
using System;
using System.Collections.Generic;
using Random = UnityEngine.Random;
using System.Collections;
using DG.Tweening;
using Intelligensia.Battle;
using Intelligensia.Battle.StatusEffects;
using BehaviorDesigner.Runtime;

public abstract class BattleEntity : Entity
{
    protected readonly int SubjectDefaultDamage = 100;
    protected readonly int SubjectDefaultDivider = 50;
    protected readonly int AgilityMinValue = 50;

    public event Action<float> FatigueUpdateEvent;
    public event Action<BattleSelectionType> ActionSelectedUpdateEvent;
    public event Action<BattleEntity> OnDefeatedEvent;

    [Header("On Defeated Variables")]
    //[SerializeField] private float slowedTimeScaleOnDefeated = 0.2f;
    //[SerializeField] private float cameraShakeForceOnDefeated = 10f;
    [SerializeField] private float explosionForceOnDefeated = 100f;

    [Header("Event Channel")]
    public OneBattleRoundEventChannelSO oneBattleRoundEventChannel;

    [Header("Camera Shake")]
    public CameraShaker cameraShaker;

    [Header("Extra")]
    public StatusEffectController statusEffectController;
    public TargetingBehavior targetingBehavior;
    [SerializeField] protected CharacterStatsBaseValue statsBaseValue = default;
    [SerializeField] private DamageSticky damageStickyPrefab = default;
    //[SerializeField] private BehaviorTree behaviorTree = default;
    [SerializeField, Range(0f, 1f)] private float guardMultiplier = default;

    public bool infiniteHp = false;

    protected BasicStats entityStats;
    protected AcademicStats academicStats;
    protected BattleSelectionType _actionSelected = BattleSelectionType.Null;

    protected bool isInitialized = false;
    public bool isGuarding { get; protected set; }

    private DamageSticky spawnedSticky;
    public Vector3 originalBattlePosition { get; private set; } = Vector3.zero;
    public Quaternion originalBattleRotation { get; private set; } = Quaternion.identity;
    public bool IsLeader { get; set; }
    public int totalRoundInOverfatigued { get; private set; }

    public new HumanAnimatorController animatorController => (HumanAnimatorController)base.animatorController;
    public CharacterStatsBaseValue CharacterStatsBase => statsBaseValue;

    #region Status Effects variables
    //public int currentLivesAmount { get; protected set; } = 0;
    public int liveBreakAmount { get; set; } = 1;
    public float attackMultiplier { get; set; } = 1;
    public float defenseMultiplier { get; set; } = 1;
    public float statusResistanceMultiplier { get; set; } = 1;
    public float receiveCritModifier { get; set; } = 0;
    public float critRateModifier { get; set; } = 0;

    public int permanentLiveBreakAmount { get; set; } = 0;
    public float permanentAttackMultiplier { get; set; } = 1;
    public float permanentDefenseMultiplier { get; set; } = 1;
    public float permanentStatusResistanceMultiplier { get; set; } = 1;

    public float initialAttackMultiplier { get; protected set; }
    public float initialDefenseMultiplier { get; protected set; }
    public int initialBreakAmount { get; protected set; }
    public float initialStatusResistanceMultiplier { get; protected set; }
    public float initialReceiveCritModifier { get; protected set; }
    public float initialCritRateModifier { get; protected set; }

    #endregion

    #region Entity Stats variables

    public string characterName => statsBaseValue.characterName;
    public int speed => entityStats.speed;
    public int intelligence => entityStats.intelligence;

    public float criticalRate => entityStats.criticalRate; // A part of CONFIDENCE
    public int stamina // Similar to Max Fatigue allowed
    {
        get
        {
            //return GetSubjectStats(currentActiveSubject).score;
            if (BattleController.instance != null)
            {
                return academicStats.GetStatsValue(BattleController.instance.activeSubject);
            }
            else
            {
                return 1;
            }
        }
    }
    #endregion

    #region Instantiated Variables (Variables that are needed and instantiated before battle starts)

    public List<InBattleSkill> inBattleOffenses { get; protected set; }
    public List<InBattleSkill> inBattleSupports { get; protected set; }

    public virtual CharacterStatsData characterStatsData { get; protected set; }

    #endregion

    [SerializeField] private float _fatigue; // Acts like inverse health bar for each subject
    public float fatigue
    {
        get => _fatigue;
        set
        {
            _fatigue = Mathf.Max(0f, value);

            FatigueUpdateEvent?.Invoke(_fatigue);
        }
    }
    public bool IsOverfatigued => fatigue >= stamina;
    public BattleSelectionType actionSelected
    {
        get
        {
            return _actionSelected;
        }
        set
        {
            _actionSelected = value;

            // Prevent Order, Analyze, SwapWeapon & SwapWeapon actions from calling the Event
            if((int)_actionSelected < 5 || (int)actionSelected > 8) 
                ActionSelectedUpdateEvent?.Invoke(_actionSelected);
        }
    }


    protected abstract void PrepareSkillsForBattle();


    protected virtual void Start()
    {
        if (oneBattleRoundEventChannel != null)
            oneBattleRoundEventChannel.OnFullRoundEvent += UpdateRoundInOverfatigued;
    }

    protected void OnDestroy()
    {
        if (oneBattleRoundEventChannel != null)
            oneBattleRoundEventChannel.OnFullRoundEvent -= UpdateRoundInOverfatigued;
    }

    public void DecideActionInBattle()
    {
        //if (behaviorTree == null)
        //    return;

        //callbackOnActionDecided = callbackOnDecided;

        //behaviorTree.OnBehaviorEnd += BehaviorTree_OnBehaviorEnd;
        //behaviorTree.EnableBehavior();

        if (targetingBehavior == null)
            return;

        if(entityTag == EntityTag.Player)
        {
            targetingBehavior.SetHighestPriorityAction(this, BattleInfoManager.activeEnemies.ToArray(), 
                BattleInfoManager.activePlayers.ToArray());
        }
        else if (entityTag == EntityTag.Enemy)
        {
            targetingBehavior.SetHighestPriorityAction(this, BattleInfoManager.activePlayers.ToArray(),
                BattleInfoManager.activeEnemies.ToArray());
        }
    }

    //private void BehaviorTree_OnBehaviorEnd(Behavior behavior)
    //{
    //    behaviorTree.OnBehaviorEnd -= BehaviorTree_OnBehaviorEnd;
    //    callbackOnActionDecided?.Invoke();

    //    callbackOnActionDecided = null;
    //}

    public SubjectStats GetSubjectStats(Subjects subject)
    {
        SubjectStats subjectStats = academicStats.GetSubjectStats(subject);

        return subjectStats;
    }

    public string GetOverallGrade()
    {
        return academicStats.GetOverallGrade();
    }

    public InBattleSkill[] GetAvailableOffense()
    {
        List<InBattleSkill> offensesNotInCooldown = new List<InBattleSkill>();

        for (int i = 0; i < inBattleOffenses.Count; i++)
        {
            if (!inBattleOffenses[i].isOnCooldown && 
                !BattleInfoManager.HasSameAction(inBattleOffenses[i].GetSkillName(), entityTag == EntityTag.Player, out int index))
            {
                offensesNotInCooldown.Add(inBattleOffenses[i]);
            }
        }

        return offensesNotInCooldown.ToArray();
    }

    public InBattleSkill[] GetAvailableSupport()
    {
        List<InBattleSkill> supportsNotInCooldown = new List<InBattleSkill>();

        for (int i = 0; i < inBattleSupports.Count; i++)
        {
            if (!inBattleSupports[i].isOnCooldown && 
                !BattleInfoManager.HasSameAction(inBattleSupports[i].GetSkillName(), entityTag == EntityTag.Player, out int index))
            {
                supportsNotInCooldown.Add(inBattleOffenses[i]);
            }
        }

        return supportsNotInCooldown.ToArray();
    }

    protected virtual void InitializeStats()
    {
        characterStatsData = GameDataManager.gameData.GetBestiaryStatsData(characterName);

        if(characterStatsData == null)
        {
            characterStatsData = new CharacterStatsData(characterName, CharacterStatsBase);
            GameDataManager.gameData.AddBestiaryStatsData(this);
        }

        entityStats = characterStatsData.currentStats;
        academicStats = characterStatsData.currentAcademicStats;

        statusEffectController.statusEffectResistances = characterStatsData.currentInnerStatusEffectResistances;

        initialAttackMultiplier = attackMultiplier;
        initialDefenseMultiplier = defenseMultiplier;
        initialBreakAmount = liveBreakAmount;
        initialStatusResistanceMultiplier = statusResistanceMultiplier;
        initialReceiveCritModifier = receiveCritModifier;
        initialCritRateModifier = critRateModifier;
    }

    public virtual void ToggleBattleMode(bool active)
    {
        characterController.applyGravity = !active;
        characterController.maxVerticalSpeed = active ? 0f : 3f;
        characterController.StopMoving();

        animatorController.Defeated(false, null);

        originalBattlePosition = transform.position;
        originalBattleRotation = transform.rotation;

        if (active)
        {
            characterStatsData.ResetSubjectsFatigue();

            PrepareSkillsForBattle();
        }
    }

    public virtual void ApplyDamage(float damage, DamageHitCategory hitCategory, Subjects subjectApplied, bool breakDefense = false)
    {
        float fatigueBeforeHit = fatigue;

        damage *= attackMultiplier * permanentAttackMultiplier;
        //Debug.Log("Damage after atk multiplier : " + damage);
        damage *= ResistanceMultiplier(subjectApplied);
        //Debug.Log("Damage after res multiplier : " + damage);

        UnhideSubjectStats(subjectApplied);
    
        if (!breakDefense)
        {
            float defenseValue = entityStats.concentration * defenseMultiplier * permanentDefenseMultiplier;

            // Multiply defenseValue with current Combat Modifier in Battle if it's Player turn
            if (BattleController.IsInBattle)
            {
                if(BattleInfoManager.currentCombatModifier != null && entityTag == EntityTag.Player)
                {
                    defenseValue *= BattleInfoManager.currentCombatModifier.concentrationModifier;
                }
            }

            damage -= defenseValue;
        }
        //Debug.Log("Damage after defense value : " + damage);

        if (isGuarding)
        {
            damage *= guardMultiplier;
        }
       
        if (damage <= 0)
        {
            //Debug.Log("Damage kesian ...");
            damage = 0.1f;
        }

        AddFatigue(damage, hitCategory);

        OnHurt();

        // If fatigue > stamina, the rate of critical hit (knockout blow) to inflict at this entity will be increased
        // If during this hit, the fatigue exceeds stamina & this hit is a crit hit, then knockout blow will occur.
        if (fatigueBeforeHit >= stamina)
        {
            if(hitCategory == DamageHitCategory.FatalCriticalHit && !infiniteHp)
            {
                //currentLivesAmount -= liveBreakAmount + permanentLiveBreakAmount;

                //SubjectKnockoutEvent?.Invoke();

                //if (currentLivesAmount <= 0)
                //{
                //    currentLivesAmount = 0;

                    OnDefeated();
                //}
                //else
                //{
                //    // The character will be in STAGGERED state for one turn and not allowed to do any action
                //    // The party members of the character will receive a certain amount of fatigue as well
                //    // Cannot be STAGGERED continuosly however, can break lives continuously
                //    if (BattleController.instance != null)
                //    {
                //        BattleController.instance.KnockoutEffect(this, entityTag);
                //    }

                //    if(!statusEffectController.HasEffect<StatusEffect_Stun>())
                //    {
                //        // Add Stun Ailment
                //        statusEffectController.AddStatusAilment(StatusAilmentType.Stun, 1);
                //    }
                    
                //}
            }
        }
    }

    protected override void OnHurt()
    {
        base.OnHurt();

        if (animatorController != null)
        {
            animatorController.OnHurt();
        }

        if (cameraShaker != null)
            cameraShaker.Shake();
    }

    protected virtual void OnDefeated()
    {
        if (isDestroyed)
            return;

        isDestroyed = true;

        Debug.Log("On defeated");

        OnDefeatedEvent?.Invoke(this);

        StartCoroutine(Defeated());
    }

    private IEnumerator Defeated()
    {
        //yield return new WaitForSeconds(0.5f);

        //if (Time.timeScale > slowedTimeScaleOnDefeated)
        //{
        //    Time.timeScale = slowedTimeScaleOnDefeated;
        //}

        if (animatorController != null)
        {
            //animatorController.Defeated(true, Death);
            animatorController.Defeated(true, null);
        }

        if (spawnedSticky != null)
        {
            spawnedSticky.pointer.target = null;
            //spawnedSticky.cameraShaker.Shake(cameraShakeForceOnDefeated);
        }

        cameraShaker.Shake();

        //if (BattleController.instance != null)
        //{
        //    BattleController.instance.turnExecutioner.RemoveActorFromCameraTargetGroups(transform);
        //}

        float endPoint = transform.position.y + explosionForceOnDefeated;

        transform.DOMove(new Vector3(transform.position.x, endPoint, transform.position.z), 0.5f);

        yield return new WaitForSeconds(1f);

        //if(Time.timeScale < 1f)
        //{
        //    Time.timeScale = 1f;
        //}


        OnDeath();
    }

    protected virtual int ResistanceMultiplier(Subjects subject)
    {
        int subjectResistance = academicStats.GetStatsValue(subject);

        int subjectMultiplier = Mathf.RoundToInt((SubjectDefaultDamage - subjectResistance) / SubjectDefaultDivider);

        return subjectMultiplier;
    }

    public void AddFatigue(float amount, DamageHitCategory hitCategory = DamageHitCategory.Undefined)
    {
        float previousFatigueNormalized = fatigue / stamina;

        fatigue += amount;

        SubjectStats currentSubjectStats = GetSubjectStats(BattleInfoManager.currentActiveSubject);
        if (currentSubjectStats != null)
        {
            currentSubjectStats.lastSubjectFatigue = fatigue;
        }

        RecycleSticky();

        spawnedSticky = ObjectPool.Spawn(damageStickyPrefab, transform);
        spawnedSticky.Show(this, amount, hitCategory, true, previousFatigueNormalized);
    }

    protected virtual void UnhideSubjectStats(Subjects subject)
    {
        SubjectStats subjectStats = GetSubjectStats(subject);

        if(subjectStats != null && subjectStats.IsHidden)
        {
            subjectStats.UnhideStats();
        }
    }

    public void Guard(bool onGuard, InBattleSkill counterSkill = null)
    {
        animatorController.Guard(onGuard);

        isGuarding = onGuard;
    }

    /// <summary>
    /// Used when selecting player's allies only
    /// </summary>
    public void DisplayCurrentFatigue()
    {
        spawnedSticky = ObjectPool.Spawn(damageStickyPrefab, transform);
        spawnedSticky.Show(this, fatigue, DamageHitCategory.Undefined, true, fatigue / stamina);
    }

    public void RecycleSticky()
    {
        if(spawnedSticky != null)
        {
            ObjectPool.Recycle(spawnedSticky.gameObject);
            spawnedSticky = null;
        }
    }

    public void ResetFatigue(Subjects currentSubject, Subjects previousSubject)
    {
        SubjectStats prevSubjectStats = GetSubjectStats(previousSubject);
        if(prevSubjectStats != null)
        {
            prevSubjectStats.lastSubjectFatigue = fatigue;
        }

        SubjectStats currentSubjectStats = GetSubjectStats(currentSubject);
        if (currentSubjectStats != null)
        {
            fatigue = currentSubjectStats.GetSubjectFatigue();

            // If Active Subject changes back to previously knocked out Subject, the Subject Stats knocked out status will be removed
            currentSubjectStats.IsKnockedOut = false;
        }
        else
            fatigue = 0f;
    }

    public bool IsKnockoutOnSubject(Subjects currentSubject)
    {
        var currentSubjectStats = GetSubjectStats(currentSubject);

        if (currentSubjectStats == null)
            return false;

        return (currentSubjectStats.GetSubjectFatigue() >= currentSubjectStats.finalValue) && currentSubjectStats.IsKnockedOut;
    }

    public bool IsStaggered()
    {
        if (statusEffectController == null) return false;

        return statusEffectController.HasStaggeringEffect();
    }

    private void UpdateRoundInOverfatigued(BattleEntity fullRoundEntity)
    {
        if (fullRoundEntity != this)
            return;

        if (IsOverfatigued && !statusEffectController.HasEffect<StatusEffect_Stun>())
            totalRoundInOverfatigued += 1;
        else
            totalRoundInOverfatigued = 0;
    }

    #region Movement

    public IEnumerator SettingPosition(Vector3 destination, Quaternion rotationTarget)
    {
        if (characterController.IsProcessingAutoMovement)
            characterController.StopMoving();

        characterController.updateMover = false;
        characterController.StopRotating();

        yield return new WaitForSeconds(0.1f);

        transform.SetPositionAndRotation(destination, rotationTarget);

        yield return new WaitForSeconds(0.1f);

        characterController.updateMover = true;
    }

    public IEnumerator MoveCharacter(Transform lookAtTarget, Vector3 destination, bool runToDestination)
    {
        characterController.AutoMove(runToDestination, destination);

        yield return new WaitUntil(() => !characterController.IsProcessingAutoMovement);

        yield return StartCoroutine(SettingPosition(destination, Quaternion.identity));

        if(lookAtTarget != null)
        {
            Vector3 lookDirection = lookAtTarget.position - transform.position;
            characterController.LookAtDirection(lookDirection);
        }
    }

    public IEnumerator MoveCharacter(Quaternion lookAtRotation, Vector3 destination, bool runToDestination)
    {
        characterController.AutoMove(runToDestination, destination);

        yield return new WaitUntil(() => !characterController.IsProcessingAutoMovement);

        yield return StartCoroutine(SettingPosition(destination, lookAtRotation));
    }

    #endregion


    public void WinCelebration()
    {
        animatorController.WinCelebration();
    }
}
