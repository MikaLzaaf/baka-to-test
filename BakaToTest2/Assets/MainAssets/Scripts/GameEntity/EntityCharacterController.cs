﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityCharacterController : MonoBehaviour
{
    public EntityAnimatorController animatorController;
    public CharacterController mover;

    public float maxPlanarSpeed = 10f;
    public float maxVerticalSpeed = 1f;

    public float planarDrag = 10f;
    public float runAccel = 10f;
    public float walkAccel = 5f;
    public float speedMultiplier = 1f;
    public float gravityScale = 2f;
    public float faceRotationSpeed = 0.1f;
    public float faceRotationInputDuration = 0.1f;

    public Vector3 velocity;
    public Vector3 externalForce;

    public bool applyGravity = true;
    public bool updateMover = true;
    public bool canMove = true;
    public bool canRotate = true;



    protected Vector3 planarMovementDirection;
    protected Quaternion autoLookDirection;
    protected Vector3 faceLookDirection;
    float faceInputDuration = 0;
    float faceRotationTime = 0;
    float rotationProgress = -1;

    protected bool lastDefaultMovement = false;
    protected bool isRotating = false;
    private bool hasNoInput = false;
    protected bool isProcessingAutoRotation = false;

    protected bool isProcessingAutoMovement = false;
    public bool IsProcessingAutoMovement => isProcessingAutoMovement;


    protected float gravityFactor = 1;
    public Vector3 gravity
    {
        get
        {
            return Physics.gravity * gravityScale * gravityFactor;
        }
    }

    public Vector3 desiredMovement { get; set; }
    public Vector3 deltaMovement { get; set; }

    private bool isRunning = false;
    public bool IsRunning
    {
        get
        {
            return isRunning;
        }
        set
        {
            if (IsProcessingAutoMovement)
                return;

            isRunning = value;

            animatorController.WalkOrRun(isRunning);
        }
    }

    public bool IsMoving
    {
        get
        {
            return planarMovementDirection.sqrMagnitude > 0.1f * 0.1f;
        }
    }


    protected virtual void OnEnable()
    {
        //if(animatorController != null)
        //{
        //    animatorController.WalkOrRun(false);
        //}
    }


    protected virtual void Update()
    {
        UpdateMovement();

        UpdateFaceDirection();

        UpdateAnimation();

        ProcessAutoRotation();
    }


    protected virtual void UpdateMovement()
    {
        // Split velocity to planar and vertical
        Vector3 planarVelocity = new Vector3(velocity.x, 0f, velocity.z);
        Vector3 verticalVelocity = new Vector3(0f, velocity.y, 0f);

        // Calculate friction
        Vector3 planarFriction = planarVelocity * planarDrag * Time.deltaTime;
        // Clamp friction to planarVelocity
        if (planarFriction.sqrMagnitude > planarVelocity.sqrMagnitude)
        {
            planarFriction = planarVelocity;
        }

        if (canMove)
        {
            // Calculate acceleration
            float accel = isRunning ? runAccel : walkAccel;

            accel *= speedMultiplier;

            // Later check if attacking or in air, then multiply with their multiplier

            Vector3 accelVel = planarMovementDirection * accel * Time.deltaTime;

            planarVelocity += accelVel;
        }

        // Apply friction
        planarVelocity -= planarFriction;


        // Clamp planar speed
        float finalMaxPlanarSpeed = maxPlanarSpeed * speedMultiplier;

        if (planarVelocity.sqrMagnitude > finalMaxPlanarSpeed * finalMaxPlanarSpeed)
        {
            planarVelocity = planarVelocity.normalized * finalMaxPlanarSpeed;
        }

        if (applyGravity)
        {
            verticalVelocity += gravity * Time.deltaTime;
        }

        // Clamp vertical speed
        if (verticalVelocity.sqrMagnitude > maxVerticalSpeed * maxVerticalSpeed)
        {
            verticalVelocity = verticalVelocity.normalized * maxVerticalSpeed;
        }


        velocity = planarVelocity + verticalVelocity;

        desiredMovement = (velocity + externalForce) * Time.deltaTime;

        if (updateMover)
        {
            MoveTo(transform.position + desiredMovement);
        }
    }


    protected void MoveTo(Vector3 position)
    {
        Vector3 previousPosition = transform.position;

        Vector3 positionDelta = position - transform.position;
        mover.Move(positionDelta);

        deltaMovement = transform.position - previousPosition;
    }


    protected virtual void UpdateFaceDirection()
    {
        if (!canRotate)
            return;

        if (planarMovementDirection.sqrMagnitude < 0.1f * 0.1f)
        {
            if (isRotating)
                hasNoInput = true;
            else
                return;
        }
        else
            hasNoInput = false;

        isRotating = true;

        if (!hasNoInput)
        {
            faceRotationTime = 0;
            faceLookDirection = planarMovementDirection;

            faceInputDuration += Time.deltaTime;
        }
        else
            faceInputDuration = 0;
            

        if (isRotating)
        {
            if(faceInputDuration >= faceRotationInputDuration)
            {
                faceInputDuration = 0;
                faceRotationTime = 1.0f;
            }
            else
                faceRotationTime += Time.deltaTime * faceRotationSpeed;

            transform.rotation = Quaternion.Lerp(transform.rotation,
                   Quaternion.LookRotation(faceLookDirection, transform.up), faceRotationTime);
        }

        if (faceRotationTime >= 1)
            isRotating = false;

        //if (planarMovementDirection.sqrMagnitude < 0.1f * 0.1f)
        //        return;
        
        //Vector3 lookDirection = planarMovementDirection;

        //float rotationSpeed = faceRotationSpeed;

        // Later add if attacking, then change rotation speed by multiply with its multiplier

        //transform.rotation = Quaternion.Lerp(transform.rotation,
        //    Quaternion.LookRotation(faceLookDirection, transform.up), faceRotationSpeed * Time.deltaTime);
    }


    protected void UpdateAnimation()
    {
        animatorController.UpdateMovement(IsMoving);
    }

    private void ProcessAutoRotation()
    {
        if (!isProcessingAutoRotation)
            return;

        if (rotationProgress < 1 && rotationProgress >= 0)
        {
            rotationProgress += Time.deltaTime * faceRotationSpeed;

            // Here we assign the interpolated rotation to transform.rotation
            // It will range from startRotation (rotationProgress == 0) to endRotation (rotationProgress >= 1)
            transform.rotation = Quaternion.Lerp(transform.rotation, autoLookDirection, rotationProgress);
        }
        else if (rotationProgress >= 1)
        {
            isProcessingAutoRotation = false;
        }
    }

    public void StopMoving()
    {
        velocity = Vector3.zero;
        externalForce = Vector3.zero;

        planarMovementDirection = Vector3.zero;

        isProcessingAutoMovement = false;
        IsRunning = false;
    }

    public void StopRotating()
    {
        faceLookDirection = Vector3.zero;
        planarMovementDirection = Vector3.zero;

        isRotating = false;
        faceRotationTime = 1.0f;
    }

    public virtual void AutoMove(bool runToDestination, Vector3 destination, bool changeDestinationOnMoving = false)
    {
        if (IsProcessingAutoMovement && !changeDestinationOnMoving)
            return;

        StartCoroutine(MoveToDestination(runToDestination, destination));
    }

    protected IEnumerator MoveToDestination(bool runToDestination, Vector3 destination)
    {
        IsRunning = runToDestination;
        isProcessingAutoMovement = true;

        Vector3 direction = destination - transform.position;

        LookAtDirection(direction);
        direction.y = 0;

        direction.Normalize();
        planarMovementDirection = direction;

        Vector3 distance = destination - transform.position;
        distance.y = 0;

        while (distance.sqrMagnitude > 2f && IsProcessingAutoMovement)
        {
            distance = destination - transform.position;
            distance.y = 0;

            LookAtDirection(direction);
            yield return null;
        }

        StopMoving();
    }

    public void SwitchToRunAnimation(bool active)
    {
        if (active)
        {
            lastDefaultMovement = isRunning;
            isRunning = true;
        }
        else
        {
            isRunning = lastDefaultMovement;
        }

        animatorController.WalkOrRun(isRunning);
    }


    public void LookAtDirection(Vector3 lookDirection)
    {
        lookDirection.y = 0;

        if(lookDirection != Vector3.zero)
            transform.rotation = Quaternion.LookRotation(lookDirection, transform.up);
    }

    public void RotateTowards(Vector3 direction)
    {
        direction.Normalize();
        autoLookDirection = Quaternion.LookRotation(direction);

        rotationProgress = 0;
        isProcessingAutoRotation = true;
    }

    public void SetMovementDirection(Vector3 direction, bool isWalking = true)
    {
        planarMovementDirection = direction;
        IsRunning = !isWalking;
    }
}
