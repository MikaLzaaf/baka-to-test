﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnSkillAnimationEnded : StateMachineBehaviour
{


    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);

        HumanAnimatorController animatorController = GetHumanAnimatorController(animator);

        animatorController.OnSkillFinished();
    }


    private HumanAnimatorController GetHumanAnimatorController(Animator animator)
    {
        HumanAnimatorController humanAnimatorController = animator.GetComponent<HumanAnimatorController>();

        if (humanAnimatorController == null)
        {
            humanAnimatorController = animator.transform.parent.GetComponent<HumanAnimatorController>();
        }

        return humanAnimatorController;
    }
}
