﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class HumanAnimatorController : EntityAnimatorController
{
    // Apa animation yg diperlukan utk satu human yg leh battle?
    // - Celebrate
    // - Jump
    // - Dash
    // - Interaction with others
    // - Every skills animation

    protected static readonly int AnimParam_MinigameProgress = Animator.StringToHash("minigameProgress");
    protected static readonly int AnimParam_SkillIndex = Animator.StringToHash("skillIndex");
    protected static readonly int AnimParam_SuccessfulSkill = Animator.StringToHash("successfulSkill");
    protected static readonly int AnimParam_EntityIsEnemy = Animator.StringToHash("entityIsEnemy");
    protected static readonly int AnimParam_Guard = Animator.StringToHash("isGuarding");

    protected static readonly int AnimParam_Order = Animator.StringToHash("doOrder");
    protected static readonly int AnimParam_IsDefeated = Animator.StringToHash("isDefeated");

    protected static readonly int AnimParam_TitrationMinigame = Animator.StringToHash("titrationMinigameProgress");
    protected static readonly int AnimState_PreTitrationMinigame = Animator.StringToHash("PreTitration");

    protected static readonly int AnimState_VictoryCelebration = Animator.StringToHash("Celebration");
    protected static readonly int AnimState_LeapBack = Animator.StringToHash("LeapBack");
    protected static readonly int AnimState_Evade = Animator.StringToHash("Evade");

    protected Action callbackOnSkillFinished;

    public void WinCelebration()
    {
        animator.CrossFade(AnimState_VictoryCelebration, 0.1f);
    }

    public void LeapBack()
    {
        animator.CrossFade(AnimState_LeapBack, 0.1f);
    }

    public void Guard(bool onGuard)
    {
        animator.SetBool(AnimParam_Guard, onGuard);
    }


    public void Evade()
    {
        animator.CrossFade(AnimState_Evade, 0.1f);
    }


    public void Defeated(bool isDefeated, Action onDefeatedComplete)
    {
        animator.SetBool(AnimParam_IsDefeated, isDefeated);

        if (onDefeatedComplete != null)
        {
            OnDyingCompleted -= onDefeatedComplete;
            OnDyingCompleted += onDefeatedComplete;
        }
    }


    public void DoOrder(bool startOrder)
    {
        animator.SetBool(AnimParam_Order, startOrder);
    }


    public void PreTitration(Action callbackOnEnded)
    {
        animator.SetInteger(AnimParam_TitrationMinigame, 0);
        animator.CrossFade(AnimState_PreTitrationMinigame, 0.5f);

        if(callbackOnEnded != null)
        {
            callbackOnSkillFinished = callbackOnEnded;
        }
    }

    public void ProceedTitrationMinigame()
    {
        int value = animator.GetInteger(AnimParam_TitrationMinigame);
        value += 1;

        animator.SetInteger(AnimParam_TitrationMinigame, value);
    }


    public void OnSkillFinished()
    {
        callbackOnSkillFinished?.Invoke();
    }


    public void StartSkillAnimation(int skillIndex)
    {
        animator.SetInteger(AnimParam_SkillIndex, skillIndex);
        animator.SetInteger(AnimParam_MinigameProgress, 0);
    }


    public void NextSkillAnimation()
    {
        animator.SetInteger(AnimParam_SkillIndex, 0);

        int value = animator.GetInteger(AnimParam_MinigameProgress);
        value += 1;

        animator.SetInteger(AnimParam_MinigameProgress, value);
    }


    public void SetSkillSuccess(bool success)
    {
        animator.SetBool(AnimParam_SuccessfulSkill, success);

        NextSkillAnimation();
    }


    public void SetEntityIsEnemy(bool isEnemy)
    {
        if (animator != null && animator.isActiveAndEnabled)
            animator.SetBool(AnimParam_EntityIsEnemy, isEnemy);
    }
}
