﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Intelligensia.UI.PlayerMenu;

public class ControllableEntity : Entity
{
    [Header("Player Behaviours")]
    public CharacterId characterId;
    public PlayerCostumeController costumeController;
    public CharacterInputController playerActionController;
    [SerializeField] protected CharacterStatsBaseValue statsBaseValue;

    public PlayerEntity battleEntityPrefab;

    public new PlayerCharacterController characterController
    {
        get
        {
            return (PlayerCharacterController)base.characterController;
        }
    }
    public new HumanAnimatorController animatorController
    {
        get
        {
            return (HumanAnimatorController)base.animatorController;
        }
    }

    private PlayerEquipment _playerEquipment;
    public PlayerEquipment playerEquipment
    {
        get
        {
            if (_playerEquipment == null)
            {
                _playerEquipment = new PlayerEquipment(characterId);
            }

            return _playerEquipment;
        }
        set
        {
            _playerEquipment = value;
        }
    }

    private PlayerStatsData _playerStatsData;
    public PlayerStatsData playerStatsData
    {
        get
        {
            if (_playerStatsData == null)
            {
                _playerStatsData = new PlayerStatsData(characterId, statsBaseValue);
            }

            return _playerStatsData;
        }
        set
        {
            _playerStatsData = value;
        }
    }

    public string characterName => statsBaseValue.characterName;


    public void CopyStats(PlayerStatsData characterStatsData)
    {
        this.playerStatsData = characterStatsData;

        //ApplyNewStatsData();
    }

    public void ApplyNewStatsData()
    {
        //entityStats = characterStatsData.GetEffectiveStats();
        //subjectStats = characterStatsData.currentSubjectStats;

        playerEquipment = PlayerInventoryManager.instance.GetPlayerEquipment(characterId);
        //statusEffectController.RecalculateEffectiveResistance(playerEquipment);
    }


    public void ApplySavedData()
    {
        GameData gameData = GameDataManager.gameData;

        if (gameData == null)
        {
            return;
        }

        PlayerStatsData statsData = GameDataManager.gameData.GetPlayerStatsData(characterId);

        if (statsData != null)
        {
            playerStatsData = statsData;
        }
        else
        {
            // Stats data is null. So create a new one.
            playerStatsData = new PlayerStatsData(characterId, statsBaseValue);
            //InitializeStats();
        }
        
        ApplyNewStatsData();
    }

    //protected void InitializeStats()
    //{
    //    characterStatsData = new CharacterStatsData(characterId, statsBaseValue);

    //    //InitializeLives();
    //}

    public void BuyNewSkill(SkillTreeUnitData skillTreeUnitData, AcademicSkillCategory academicSkillCategory)
    {
        if (skillTreeUnitData == null || skillTreeUnitData.BaseSkillTreeUnit == null)
            return;

        SkillTreeData skillTree = playerStatsData.GetSubjectSkillTree(academicSkillCategory);
        skillTree.BuySkillTreeUnitData(skillTreeUnitData.id);

        if (academicSkillCategory == AcademicSkillCategory.AttackArgument || academicSkillCategory == AcademicSkillCategory.SupportArgument)
        {
            BaseSkill baseSkill = skillTreeUnitData.GetBase<BaseSkill>();

            if (baseSkill == null)
                return;

            string itemId = "";

            if (baseSkill is OffensiveSkillSO)
                itemId = "OffenseSkill_" + baseSkill.GetPathToSkillItemBaseValue();
            else if (baseSkill is SupportSkillSO)
                itemId = "SupportSkill_" + baseSkill.GetPathToSkillItemBaseValue();

            SkillItemBaseValue skillItemValue = (SkillItemBaseValue)BaseValueLoader.LoadItemData(itemId);

            if (skillItemValue == null)
                return;

            PlayerInventoryManager.instance.AddItem(skillItemValue.ItemID, 1);
            Debug.Log("Buy new skill");
        }
        else if(academicSkillCategory == AcademicSkillCategory.Stats) // stats upgrade
        {
            if(skillTreeUnitData.IsType<SubjectStatsUpgradeSO>())
            {
                SubjectStatsUpgradeSO statsUpgradeSO = skillTreeUnitData.GetBase<SubjectStatsUpgradeSO>();

                playerStatsData.UpgradeStats(statsUpgradeSO.subjectStatsToUpgrade, statsUpgradeSO.baseStatsToUpgrade);
                Debug.Log("Buy subject stats upgrade");
            }
            else if (skillTreeUnitData.IsType<BaseStatsUpgradeSO>())
            {
                BaseStatsUpgradeSO statsUpgradeSO = skillTreeUnitData.GetBase<BaseStatsUpgradeSO>();

                playerStatsData.UpgradeStats(null, statsUpgradeSO.statsToUpgrade);
                Debug.Log("Buy Base stats upgrade");
            }
        }

        
    }
}
