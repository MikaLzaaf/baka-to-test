﻿public enum EntityTag
{
    Untagged,
    Player,
    Enemy,
    NPC
}
