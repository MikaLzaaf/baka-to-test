﻿using UnityEngine;
using System;

public class Entity : MonoBehaviour 
{

    public Action OnHurtEvent;
    public Action OnDeathEvent;
    public Action OnRevivedEvent;

    [Header("Entity Base")]
    public EntityTag entityTag;
    public DestroyMethod destroyMethod;
    public bool isDestroyed = false;
    public Transform destructionEffectPrefab;
    public Vector3 destructionEffectOffset;
    public Vector3 spawnPositionOffset;

    //public Renderer mainRenderer;

    [Header("Basic Behaviours")]
    public EntityAnimatorController animatorController;
    public EntityCharacterController characterController;
    public EntityModelBinder entityModelBinder;

    //protected Rigidbody _rigidbody;

    //public virtual Vector3 velocity
    //{
    //    get
    //    {
    //        if(_rigidbody != null)
    //        {
    //            return _rigidbody.velocity;
    //        }

    //        return Vector3.zero;
    //    }
    //    set
    //    {
    //        if (_rigidbody != null)
    //        {
    //            _rigidbody.velocity = value;
    //        }
    //    }
    //}

    //protected virtual void Start()
    //{
    //    _rigidbody = GetComponent<Rigidbody>();
    //}


    //public virtual void ApplyDamage(float damage, DamageHitCategory hitCategory, Subjects subjectApplied = Subjects.BahasaMelayu, bool breakDefense = false)
    //{
        //if(!breakDefense)
        //{
        //    damage -= defense;
        //}

        //if(damage <= 0)
        //{
        //    return;
        //}

        //currentHealth -= damage;

        //OnHurt();

        //if(currentHealth <= 0)
        //{
        //    OnDeath();
        //}
    //}


    protected virtual void OnDeath()
    {
        isDestroyed = true;

        if (OnDeathEvent != null)
        {
            OnDeathEvent.Invoke();
        }

        Death();
    }


    protected virtual void OnHurt()
    {
        if(OnHurtEvent != null)
        {
            OnHurtEvent.Invoke();
        }
    }


    public void Death()
    {
        if (destroyMethod == DestroyMethod.Deactivate)
        {
            Vanish();
        }
        else if(destroyMethod == DestroyMethod.Destroy)
        {
            Destroy(gameObject);
        }
    }


    public void ShowDestructionEffect()
    {
        if(destructionEffectPrefab != null)
        {
            ObjectPool.Spawn(destructionEffectPrefab, transform.position + destructionEffectOffset);
        }
    }

    public void Vanish()
    {
        ShowDestructionEffect();

        if (characterController != null)
        {
            characterController.StopMoving();
        }

        this.gameObject.SetActive(false);

        //AudioManager.PlaySoundOneShot(deathSound);

        //if (isProcessingHurt)
        //{
        //    EndProcessingHurt();
        //}
    }


    //public void SetInteractionCamera(int index)
    //{
    //    NPCInteractable npc = GetComponent<NPCInteractable>();

    //    if (npc != null)
    //    {
    //        npc.SetInteractionCamera(index);
    //    }
    //}
}
