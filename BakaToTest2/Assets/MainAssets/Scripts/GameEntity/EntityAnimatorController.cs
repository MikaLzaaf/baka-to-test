﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class EntityAnimatorController : MonoBehaviour 
{
    // Apa animation yg semua makhluk ada?
    // - Walking
    // - Running
    // - Idle
    // - Expression
    // - Dying


    protected static readonly int AnimParam_Walking = Animator.StringToHash("walking");
    protected static readonly int AnimParam_Running = Animator.StringToHash("running");
    protected static readonly int AnimParam_Idle = Animator.StringToHash("idleType");
    protected static readonly int AnimParam_HurtType = Animator.StringToHash("hurtType");
    //protected static readonly int AnimParam_Action = Animator.StringToHash("action");
    //protected static readonly int AnimParam_ActionType = Animator.StringToHash("actionType");
    protected static readonly int AnimParam_ShouldMove = Animator.StringToHash("shouldMove");

    protected static readonly int AnimParam_ShowExpression = Animator.StringToHash("showExpression");
    protected static readonly int AnimParam_NoExpression = Animator.StringToHash("noExpression");
    protected static readonly int AnimParam_ExpressionType = Animator.StringToHash("expressionType");

    protected static readonly int AnimState_Hurt = Animator.StringToHash("Hurt");
    protected static readonly int AnimState_Dying = Animator.StringToHash("Dying");

    protected const string ExpressionsLayerName = "Expressions";

    protected readonly int idleFightTypes = 3;

    public Animator animator;

    public Action OnActionCompleted;
    public Action OnDyingCompleted;
    public Action OnAttackCompleted;


    protected Coroutine coroutineOnAnimatorStopPlaying;

    public int generalActionSelected { get; set; }
    public int attackTypeSelected { get; set; }


    protected void Start()
    {
        // Set animation to normal idle
        //Idle(0);
       

    }


    public void Idle(float idleType)
    {
        if(idleType > 0f && idleType <= idleFightTypes)
        {
            idleType = idleType / idleFightTypes;
        }

        animator.SetFloat(AnimParam_Idle, idleType);
    }


    //public void ExecuteGeneralAction(int actionType)
    //{
    //    animator.SetTrigger(AnimParam_Action);

    //    animator.SetInteger(AnimParam_ActionType, actionType);

    //    generalActionSelected = actionType;
    //}


    public void ShowExpression(ActorExpression expression, bool returnToIdleAfterExpression)
    {
        animator.ResetTrigger(AnimParam_NoExpression);
        animator.SetTrigger(AnimParam_ShowExpression);

        animator.SetInteger(AnimParam_ExpressionType, (int)expression);

        if(returnToIdleAfterExpression)
        {
            if(coroutineOnAnimatorStopPlaying != null)
            {
                StopCoroutine(coroutineOnAnimatorStopPlaying);
            }

            coroutineOnAnimatorStopPlaying = StartCoroutine(WaitUntilAnimatorStopPlaying(animator.GetLayerIndex(ExpressionsLayerName), expression.ToString(), NoExpression));
        }
    }


    public void NoExpression()
    {
        if (coroutineOnAnimatorStopPlaying != null)
        {
            StopCoroutine(coroutineOnAnimatorStopPlaying);
        }

        animator.SetInteger(AnimParam_ExpressionType, 0);
        animator.SetTrigger(AnimParam_NoExpression);
    }

    private bool IsAnimatorPlaying(Animator anim, int animLayer, string stateName)
    {
        if (anim.GetCurrentAnimatorStateInfo(animLayer).IsTag(stateName) &&
                anim.GetCurrentAnimatorStateInfo(animLayer).normalizedTime < 0.95f)
            return true;
        else
            return false;
    }

    private IEnumerator WaitUntilAnimatorStopPlaying(int animatedLayer, string stateName, Action callbackAfterStop)
    {
        yield return new WaitForSeconds(1f);

        yield return new WaitUntil(() => !IsAnimatorPlaying(animator, animatedLayer, stateName));

        callbackAfterStop?.Invoke();

        coroutineOnAnimatorStopPlaying = null;
    }

    //public void ExecuteAttack(int attackType, Action onAttackComplete)
    //{
    //    animator.SetTrigger(AnimParam_Attack);

    //    animator.SetInteger(AnimParam_AttackType, attackType);

    //    attackTypeSelected = attackType;

    //    if(onAttackComplete != null)
    //    {
    //        OnAttackCompleted -= onAttackComplete;
    //        OnAttackCompleted += onAttackComplete;
    //    }
    //}


    public void OnActionComplete()
    {
        if(OnActionCompleted != null)
        {
            OnActionCompleted.Invoke();
        }
    }


    public void OnDyingComplete()
    {
        if(OnDyingCompleted != null)
        {
            OnDyingCompleted.Invoke();
        }
    }


    public void OnAttackComplete()
    {
        if(OnAttackCompleted != null)
        {
            OnAttackCompleted.Invoke();
        }
    }


    public void WalkOrRun(bool isRunning)
    {
        if(animator != null && animator.isActiveAndEnabled)
        {
            animator.SetBool(AnimParam_Walking, !isRunning);
            animator.SetBool(AnimParam_Running, isRunning);
        }
    }


    public void UpdateMovement(bool isMoving)
    {
        if (animator != null && animator.isActiveAndEnabled)
            animator.SetBool(AnimParam_ShouldMove, isMoving);
    }


    public void Death(Action onDeath)
    {
        if (animator != null && animator.isActiveAndEnabled)
            animator.CrossFade(AnimState_Dying, 0.5f);

        if(onDeath != null)
        {
            OnDyingCompleted -= onDeath;
            OnDyingCompleted += onDeath;
        }
    }

    public void OnHurt()
    {
        float hurtType = UnityEngine.Random.Range(0, 4);

        if (animator != null && animator.isActiveAndEnabled)
        {
            animator.SetFloat(AnimParam_HurtType, hurtType);
            animator.CrossFade(AnimState_Hurt, 0.1f);
        }
    }
   
}
