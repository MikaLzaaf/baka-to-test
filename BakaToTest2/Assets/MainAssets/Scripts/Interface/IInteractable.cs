﻿using UnityEngine;

public interface IInteractable
{
    bool canInteract { get; set; }

    void OnReceivedInteract(GameObject obj);
}
