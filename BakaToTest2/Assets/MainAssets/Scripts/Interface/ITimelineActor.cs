﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ITimelineActor : ITimelineActorAction
{
    Transform GetTransform();

    GameObject GetGameObject();

    GameObject GetAnimatorHolder();
}
