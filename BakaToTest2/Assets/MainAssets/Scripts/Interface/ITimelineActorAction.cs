﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ITimelineActorAction 
{
    HumanAnimatorController actorAnimatorController { get; }

    void LeapBackToPosition(Vector3 targetPosition, Quaternion targetRotation);
    IEnumerator LeapingBack(Vector3 targetPosition, Quaternion targetRotation);


}
