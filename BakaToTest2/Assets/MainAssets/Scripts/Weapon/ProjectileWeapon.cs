﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileWeapon : RangedWeapon {

    public Projectile projectilePrefab;

    public Transform firingPoint;
    public Transform fireEffectPrefab;

    public int speedMultiplier = 1;
    public int lifeMultiplier = 1;

    public float firingInterval = 0.5f;
    public bool autoFire = false;


    void Update()
    {
        if(autoFire)
        {
            if(Time.time - lastFiringTime > firingInterval)
            {
                Fire();
            }
        }
    }


    public void Fire()
    {
        Fire(transform.forward);
    }


    public void FireAt(Vector3 position)
    {
        Vector3 direction = position - firingPoint.position;

        Fire(direction);
    }


    public void Fire(Vector3 direction)
    {
        if (!enabled)
            return;

        lastFiringTime = Time.time; 

        Vector3 firingPosition = firingPoint.position;

        SpawnProjectile(projectilePrefab, firingPoint, direction, fireEffectPrefab, speedMultiplier, lifeMultiplier);
    }
}
