﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangedWeapon : MonoBehaviour {

    public Entity ownerEntity;

    public float lastFiringTime { get; protected set; }


    public Projectile SpawnProjectile(Projectile projectilePrefab, Transform firingPoint, Vector3 direction, Transform fireEffectPrefab, int speedMultiplier = 1, int lifeMultiplier = 1)
    {
        Projectile projectile = ObjectPool.Spawn(projectilePrefab, firingPoint);

        if(ownerEntity != null)
        {
            projectile.ownerEntity = ownerEntity;
            projectile.entityTag = ownerEntity.entityTag;
        }

        projectile.transform.rotation = Quaternion.LookRotation(direction);

        //projectile.velocity = projectilePrefab.velocity.magnitude * direction.normalized * speedMultiplier;

        projectile.life *= lifeMultiplier;

        return projectile;
    }
}
