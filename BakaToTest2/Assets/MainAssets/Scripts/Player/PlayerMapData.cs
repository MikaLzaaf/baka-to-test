﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerMapData 
{
    public CharacterId lastSelectedCharacter = CharacterId.MC;


    [SerializeField] private float savedXPosition = 0f;
    [SerializeField] private float savedYPosition = 0f;
    [SerializeField] private float savedZPosition = 0f;

    [SerializeField] private float savedXRotation = 0f;
    [SerializeField] private float savedYRotation = 0f;
    [SerializeField] private float savedZRotation = 0f;

    [SerializeField] private string mainAreaName;



    public PlayerMapData()
    {

    }

    public void Save()
    {
        if(PlayerPartyManager.instance == null)
            return;

        ControllableEntity player = PlayerPartyManager.instance.selectedPlayer;

        Save(player);

        if (MapManager.instance == null)
            return;

        mainAreaName = MapManager.instance.currentMapArea.areaName;
        Debug.Log("Saving " + mainAreaName);
    }


    public void Save(ControllableEntity selectedPlayer)
    {
        if (selectedPlayer == null)
            return;

        SavePlayerPositionAndRotation(selectedPlayer.transform);

        lastSelectedCharacter = selectedPlayer.characterId;
    }


    private void SavePlayerPositionAndRotation(Transform player)
    {
        savedXPosition = player.position.x;
        savedYPosition = player.position.y;
        savedZPosition = player.position.z;

        savedXRotation = player.eulerAngles.x;
        savedYRotation = player.eulerAngles.y;
        savedZRotation = player.eulerAngles.z;
    }


    public Vector3 LoadPlayerPosition()
    {
        return new Vector3(savedXPosition, savedYPosition, savedZPosition);
    }

    public Vector3 LoadPlayerRotation()
    {
        return new Vector3(savedXRotation, savedYRotation, savedZRotation);
    }

    public string LoadMapName()
    {
        return mainAreaName;
    }

    //public List<CharacterId> LoadPartyMemberList()
    //{
    //    return partyMembersList;
    //}
}
