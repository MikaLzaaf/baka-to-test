﻿using UnityEngine;
using System;
using System.Collections;

[Serializable]
public class PlayerLevelStats
{
	public const int BaseLevel = 1;
	public const int LevelMax = 9999;
	public const int BaseExp = 10;
	public const float LevelCurveExponent = 2f;


	private int _level = BaseLevel;
	public int level
	{
		get
		{
			return _level;
		}
		set
		{
			_level = Mathf.Clamp(value, BaseLevel, LevelMax);
		}
	}


	private int _exp = BaseExp;
	public int exp
	{
		get
		{
			return _exp;
		}
		set
		{
			_exp = value;
		}
	}


	public int GetExpToLevel(int targetLevel)
	{
		return Mathf.RoundToInt(BaseExp * Mathf.Pow(targetLevel, LevelCurveExponent));
	}


	public float GetExpProgressToNextLevel()
	{
		int expToNextLevel = GetExpToLevel(level + 1);
		int expToPreviousLevel = GetExpToLevel(level);

		float progress = (float)(exp - expToPreviousLevel) / (expToNextLevel - expToPreviousLevel);

		return progress;
	}


	public bool IncreaseExp(int expGained)
	{
		int previousLevel = level;

		exp += expGained;

		if (level < LevelMax)
		{
			while (true)
			{
				if (exp >= GetExpToLevel(level + 1))
				{
					// Level up
					level += 1;
				}
				else
				{
					break;
				}
			}
		}

		bool isLevelUp = level > previousLevel;

		return isLevelUp;
	}


	public void SetLevel(int level)
	{
		this.level = level;

		exp = GetExpToLevel(level - 1);
	}

}
