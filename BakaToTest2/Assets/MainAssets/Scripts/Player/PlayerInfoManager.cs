﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public static class PlayerInfoManager
{

	private const string ControllablePlayerPrefix = "Player";
	private const string ControllablePlayerDirectoryInResources = "ControllableEntities/Players";
	private const string CharacterIconDirectoryInResources = "Characters/Icons";
	private const string CharacterPortraitDirectoryInResources = "Characters/Portrait";

	private const string BattleEntitiesPrefabDirectoryInResources = "BattleEntities";
	private const string ClassPrefix = "Class_";
	private const string BattleEntityPrefix = "BattleEntity_";


	private const string ClassmatesDataInResources = "Data/ClassmatesData";
	private const string KeyHeader = "A";
	private const int KeyIndex = 0;

	public const string LL = "AA";

    public static readonly string[] classesNames = {"Undefined", "A", "B", "C", "D", "E", "F" };

    private static Dictionary<string, List<string>> studentsInClasses;


    static PlayerInfoManager()
    {
		Initialize();
	}

	private static void Initialize()
	{
		studentsInClasses = new Dictionary<string, List<string>>();

		TextAsset dataAsset = Resources.Load<TextAsset>(ClassmatesDataInResources);

		if (dataAsset != null)
		{
			LoadClassmatesData(dataAsset);
		}
	}

	private static void LoadClassmatesData(TextAsset dataAsset)
	{
		using (StringReader reader = new StringReader(dataAsset.text))
		{
			CsvParser parser = new CsvParser();

			string[][] data = parser.Parse(reader);
			string[] headers = null;

			if (data == null || data.Length == 0)
			{
				return;
			}

			headers = data[0];

			if (headers == null || headers.Length <= 1)
			{
				return;
			}

			if (headers[KeyIndex] != KeyHeader)
			{
				Debug.LogWarning("The header of first column should be: " + KeyHeader);
				return;
			}

			// Add classes
			for (int column = KeyIndex; column < headers.Length; column++)
			{
				string className = headers[column];

				if (!studentsInClasses.ContainsKey(className))
				{
					studentsInClasses[className] = new List<string>();
				}
			}

			for (int row = 1; row < data.Length; row++)
			{
				string[] dataRow = data[row];

				for (int column = KeyIndex; column < dataRow.Length; column++)
				{
					string value = dataRow[column];
					string className = headers[column];

					studentsInClasses[className].Add(value);
				}
			}
		}
	}

    public static string[] GetClassmatesNames(string className)
    {
		studentsInClasses.TryGetValue(className, out List<string> classmates);

		return classmates.ToArray();
    }

	//public static string[] GetClassNames()
 //   {
	//	return classesNames;
 //   }

    public static Sprite LoadCharacterIcon(CharacterId charId)
    {
        string path = CharacterIconDirectoryInResources + "/" + charId.ToString();

        Sprite iconSprite = Resources.Load<Sprite>(path);

        if (iconSprite == null)
        {
            Debug.LogError("Failed to load item icon at Resources/" + path);
        }

        return iconSprite;
    }

    public static Sprite LoadCharacterPortrait(CharacterId charId)
    {
        string path = CharacterPortraitDirectoryInResources + "/" + charId.ToString();

        Sprite iconSprite = Resources.Load<Sprite>(path);

        if (iconSprite == null)
        {
            Debug.LogError("Failed to load item icon at Resources/" + path);
        }

        return iconSprite;
    }

    public static GameObject LoadControllablePlayerPrefab(CharacterId playerId)
    {
        string path = string.Format("{0}/{1}{2}", ControllablePlayerDirectoryInResources, ControllablePlayerPrefix, playerId.ToString());

        return Resources.Load<GameObject>(path);
    }

    public static BattleEntity LoadBattleEntityPrefab(string className, string entityName)
    {
        string classPath = ClassPrefix + className;

        string entityPath = BattleEntityPrefix + entityName;

        string path = string.Format("{0}/{1}/{2}", BattleEntitiesPrefabDirectoryInResources, classPath, entityPath);

        return Resources.Load<BattleEntity>(path);
    }
}
