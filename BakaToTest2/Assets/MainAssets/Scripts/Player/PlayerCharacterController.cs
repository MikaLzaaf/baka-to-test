﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCharacterController : EntityCharacterController 
{

    public bool canInputMovement
    {
        get
        {
            return !PlayerPartyManager.playerCannotMove && !isProcessingAutoMovement;
        }
    }

    private Transform cameraTransform;
    private Vector2 inputDirection;

    private Vector3 lastMovementDirection;
    private Vector2 lastMovementInput;



    private void Start()
    {
        cameraTransform = Camera.main.transform;
    }
  

    protected override void Update()
    {
        ProcessMovementInput();

        base.Update();
    }


    public void ProcessMovementInput()
    {
        if(!canInputMovement)
        {
            if (isProcessingAutoMovement)
                return;

            if(inputDirection != Vector2.zero)
            {
                inputDirection = Vector2.zero;
                planarMovementDirection = Vector3.zero;
            }

            return;
        }

        if(cameraTransform == null)
        {
            cameraTransform = Camera.main.transform;
        }

        // Convert to camera direction or world view
        planarMovementDirection = ConvertInputToWorldDirection(inputDirection, cameraTransform);
    }


    public void InputMovement(Vector2 input)
    {
        inputDirection = input;
    }


    private Vector3 ConvertInputToWorldDirection(Vector2 inputDirection, Transform cameraTransform)
    {
        if (MainCameraController.instance.isCameraChangedSuddenly)
        {
            return lastMovementDirection;
        }

        Vector3 forward = cameraTransform.TransformDirection(Vector3.forward);
        forward.y = 0;
        forward = forward.normalized;

        Vector3 right = new Vector3(forward.z, 0f, -forward.x);

        Vector3 worldDirection = forward * inputDirection.y + right * inputDirection.x;
        worldDirection.Normalize();

        lastMovementDirection = worldDirection;

        return worldDirection;
    }


    //public void SetPosition(Vector3 destination)
    //{
    //    StartCoroutine(SettingPosition(destination));
    //}

    public void SetPositionAndRotation(Vector3 destination, Quaternion rotation)
    {
        StartCoroutine(SettingPositionAndRotation(destination, rotation));
    }    

    private IEnumerator SettingPositionAndRotation(Vector3 destination, Quaternion rotation)
    {
        if (IsProcessingAutoMovement)
            StopMoving();

        updateMover = false;
        StopRotating();

        yield return new WaitForFixedUpdate();

        transform.SetPositionAndRotation(destination, rotation);

        yield return new WaitForFixedUpdate();

        updateMover = true;
    }
}
