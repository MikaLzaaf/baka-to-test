﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;


public class CharacterInputController : MonoBehaviour 
{

    //public EntityAnimatorController animatorController;
    public PlayerCharacterController playerCharacterController;
    public InteractionDetectionController interactionDetection;

    [Header("Player Input")]
    public PlayerInput playerInput;
    public bool cursorLocked = true;
    public bool canInputAction = true;


    private const string MenuControlsInputMap = "Menu Controls";
    private const string PlayerControlsInputMap = "Player Controls";

    private ObjectInteractableBase currentInteractable;


    private void Start()
    {
        if(GameUIManager.instance != null)
        {
            GameUIManager.instance.ViewStateChangedEvent -= OnViewStateChanged;
            GameUIManager.instance.ViewStateChangedEvent += OnViewStateChanged;
        }
    }


    private void OnDisable()
    {
        if (GameUIManager.instance != null)
        {
            GameUIManager.instance.ViewStateChangedEvent -= OnViewStateChanged;
        }
    }


    private void OnViewStateChanged(ViewState viewState)
    {
        if(viewState == ViewState.Ready)
        {
            TogglePlayerControllability(false);
            //SwitchInputActionMap(MenuControlsInputMap);
        }
        else
        {
            TogglePlayerControllability(true);
            //SwitchInputActionMap(PlayerControlsInputMap);
        }
    }

    private void CheckForNearbyInteraction()
    {
        if(interactionDetection != null)
        {
            interactionDetection.CheckInteraction(out currentInteractable);

            if(currentInteractable != null)
            {
                // Stop character from moving and block movement input
                playerCharacterController.StopMoving();
                PlayerPartyManager.instance.TogglePlayerInput(false);

                currentInteractable.InteractionEndedEvent += OnInteractionEnded;
            }
        }
    }

    private void CheckForNearbyChallenge()
    {
        if (interactionDetection != null)
        {
            interactionDetection.CheckChallengeAvailability(out currentInteractable);

            if (currentInteractable != null)
            {
                // Stop character from moving and block movement input
                playerCharacterController.StopMoving();
                PlayerPartyManager.instance.TogglePlayerInput(false);

                currentInteractable.InteractionEndedEvent += OnInteractionEnded;
            }
        }
    }

    private void OnInteractionEnded()
    {
        currentInteractable.InteractionEndedEvent -= OnInteractionEnded;
        currentInteractable = null;

        PlayerPartyManager.instance.TogglePlayerInput(true);

        if(interactionDetection != null)
        {
            interactionDetection.ReactivateTrigger();
        }

        MainCameraController.instance.ActivateThirdPersonGroupCamera(false);
    }


    private void TogglePlayerControllability(bool active)
    {
        playerCharacterController.canMove = active;
        playerCharacterController.canRotate = active;

        SetInputActiveState(active);
    }


    #region Player Input related

    public void SetInputActiveState(bool isPlayerTurn)
    {
        if (playerInput == null)
            return;

        switch (isPlayerTurn)
        {
            case false:
                playerInput.enabled = false;
                break;

            case true:
                playerInput.enabled = true;
                break;
        }
    }

    public void SwitchInputActionMap(string actionMapName)
    {
        if (playerInput == null)
            return;

        playerInput.SwitchCurrentActionMap(actionMapName);
    }


    public void SwitchTravelCharacter(InputAction.CallbackContext callbackContext)
    {
        Debug.Log("Switch character");

        if (callbackContext.performed)
        {
            PlayerPartyManager.instance.SwitchPlayer(1);
        }
    }

    public void CheckNearbyInteraction(InputAction.CallbackContext callbackContext)
    {
        if (!canInputAction)
            return;

        if(callbackContext.performed)
        {
            CheckForNearbyInteraction();
        }
    }

    public void CheckNearbyChallenge(InputAction.CallbackContext callbackContext)
    {
        if (!canInputAction)
            return;

        if (callbackContext.performed)
        {
            CheckForNearbyChallenge();
        }
    }

    public void OnMovement(InputAction.CallbackContext callbackContext)
    {
        if (playerCharacterController == null)
            return;

        playerCharacterController.InputMovement(callbackContext.ReadValue<Vector2>());
    }

    public void OnSprinting(InputAction.CallbackContext callbackContext)
    {
        if (playerCharacterController == null)
            return;

        if (callbackContext.started)
        {
            playerCharacterController.IsRunning = true;
        }
        else if(callbackContext.canceled)
        {
            playerCharacterController.IsRunning = false;
        }
    }

    public void OpenMainMenu(InputAction.CallbackContext callbackContext)
    {
        if(callbackContext.performed)
        {
            GameUIManager.instance.OpenMainMenu();
        }
    }

    public void OpenFullMap(InputAction.CallbackContext callbackContext)
    {
        if (callbackContext.performed)
        {
            GameUIManager.instance.OpenFullMap();
        }
    }

    public void OpenChallengeMenu(InputAction.CallbackContext callbackContext)
    {
        if (callbackContext.performed)
        {
            GameUIManager.instance.ToggleChallengesMenu(true);
        }
    }

    public void OnCancelMenu(InputAction.CallbackContext callbackContext)
    {
        if (callbackContext.performed)
        {
            //UINavigator.instance.OpenPreviousWindow();
        }
    }

    #endregion

    private void OnApplicationFocus(bool hasFocus)
    {
        //SetCursorState(cursorLocked);
    }

    private void SetCursorState(bool newState)
    {
        Cursor.lockState = newState ? CursorLockMode.Locked : CursorLockMode.None;
    }
}
