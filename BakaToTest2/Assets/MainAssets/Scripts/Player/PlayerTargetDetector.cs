﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTargetDetector : MonoBehaviour {

    public Entity ownerEntity;

    public float targetDetectionRadius = 10f;
    public float lineOfSightRadius = 0.5f;
    public float targetRangeAngle = 120f;
    public LayerMask targetLayers;
    public LayerMask obstacleLayers;

    public Transform nearestTarget { get; protected set; }

    private List<Transform> _detectedTargets = new List<Transform>();
    public List<Transform> detectedTargets
    {
        get
        {
            return _detectedTargets;
        }
        set
        {
            _detectedTargets = value;
        }
    }



    public Transform FindNearestTarget()
    {
        detectedTargets.Clear();

        var targetsInRange = Physics.OverlapSphere(ownerEntity.transform.position, targetDetectionRadius, targetLayers);

        if (targetsInRange == null || targetsInRange.Length == 0)
        {
            return null;
        }

        nearestTarget = null;
        float nearestSqrDistance = Mathf.Infinity;

        for(int i = 0; i < targetsInRange.Length; i++)
        {
            Transform target = targetsInRange[i].transform;

            if(target == ownerEntity.transform)
            {
                continue;
            }

            if(!IsValidTarget(target))
            {
                continue;
            }

            Vector3 directionToTarget = target.position - ownerEntity.transform.position;

            float angle = Vector3.Angle(directionToTarget, ownerEntity.transform.forward);

            if(angle > targetRangeAngle)
            {
                continue;
            }

            if(!IsInLineOfSight(target))
            {
                continue;
            }

            detectedTargets.Add(target);

            float sqrDistance = directionToTarget.sqrMagnitude;

            if(sqrDistance < nearestSqrDistance)
            {
                nearestSqrDistance = sqrDistance;
                nearestTarget = target;
            }
        }

        return nearestTarget;
    }


    public bool IsInLineOfSight(Transform target)
    {
        RaycastHit hit;

        Vector3 directionToTarget = target.position - transform.position;

        if(Physics.SphereCast(transform.position, lineOfSightRadius, directionToTarget, out hit, directionToTarget.magnitude, targetLayers, QueryTriggerInteraction.Ignore))
        {
            if(hit.collider.transform != target)
            {
                return false;
            }
        }

        if(Physics.SphereCast(target.position, lineOfSightRadius, -directionToTarget, out hit, directionToTarget.magnitude, obstacleLayers, QueryTriggerInteraction.Ignore))
        {
            if(hit.collider.transform != ownerEntity.transform)
            {
                return false;
            }
        }

        return true;
    }


    public virtual bool IsValidTarget(Transform target)
    {
        return true;
    }
}
