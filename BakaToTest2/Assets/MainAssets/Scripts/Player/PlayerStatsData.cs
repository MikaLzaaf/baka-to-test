﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Intelligensia.UI.PlayerMenu;

[System.Serializable]
public class PlayerStatsData : CharacterStatsData
{
    /// <summary> This is used for storing any progress done by the player on this character. 
    /// Stuffs like currentStats, skills learned are stored here.</summary> 

    public CharacterId characterId;

    public List<Item> skillsEquipped;

    public List<SkillTreeData> skillTreeDatas;

    public int debateHelperLevel = 1;

    [SerializeField] private int argumentWeight = 0;
    [SerializeField] private int argumentMaxWeight = 0;

    private const string ArgumentWeightTextFormat = "{0} / <color=#FCA311>{1}</color>";

    public PlayerStatsData()
    {

    }

    public PlayerStatsData(CharacterId characterId, CharacterStatsBaseValue characterStatsBase)
    {
        this.characterId = characterId;

        currentStats = characterStatsBase.baseStats;

        currentAcademicStats = characterStatsBase.baseAcademicStats;

        currentInnerStatusEffectResistances = characterStatsBase.baseInnerStatusEffectResistances;

        characterInfoDatas = new List<CharacterInfoData>();

        for (int i = 0; i < characterStatsBase.characterInfos.Count; i++)
        {
            CharacterInfoData characterInfoData = new CharacterInfoData(characterStatsBase.characterInfos[i]);
            characterInfoDatas.Add(characterInfoData);
        }

        skillTreeDatas = new List<SkillTreeData>();

        for(int i = 0; i < characterStatsBase.baseSkillTrees.Count; i++)
        {
            SkillTreeBaseValueSO skillTreeBase = characterStatsBase.baseSkillTrees[i];

            SkillTreeData skillTree = new SkillTreeData(skillTreeBase.skillTreeUnitDatas, skillTreeBase.skillCategory);

            skillTreeDatas.Add(skillTree);
        }

        for(int i = 0; i < characterStatsBase.defaultSkillItems.Count; i++)
        {
            SkillItemBaseValue skillItemValue = characterStatsBase.defaultSkillItems[i];

            PlayerInventoryManager.instance.AddItem(skillItemValue.ItemID, 1);
        }

        RecalculateArgumentMaxWeight();

        skillsEquipped = new List<Item>();
    }

    #region GetStats methods
    /// <summary>
    /// Mostly used for displaying basic stats in the PlayerStatsPanel
    /// </summary>
    /// <returns></returns>
    public BasicStats GetEffectiveBasicStats()
    {
        // Clone the current stats to avoid referencing it & accidentally changes it's value
        BasicStats effectiveStats = DeepCopier.DeepCopy(currentStats);

        ControllableEntity character = PlayerPartyManager.instance.GetPartyMember(characterId);

        if (character == null)
            return effectiveStats;

        PlayerEquipment playerEquipment = character.playerEquipment;

        if (playerEquipment != null)
        {
            List<Item> playerEquipments = playerEquipment.equippedItems;

            if (playerEquipments.Count > 0)
            {
                for (int i = 0; i < playerEquipments.Count; i++)
                {
                    EquipmentItemBaseValue equipmentBase = playerEquipments[i].itemValue as EquipmentItemBaseValue;

                    effectiveStats.UpdateStatsModifiers(equipmentBase.EquipmentSection.ToString(), equipmentBase.statsEfective, true);
                }
            }
        }

        return effectiveStats;
    }

    public BasicStats GetPreviewBasicStats(List<Item> previewEquipments)
    {
        // Clone the current stats to avoid referencing it & accidentally changes it's value
        BasicStats newStats = DeepCopier.DeepCopy(currentStats);

        if (previewEquipments.Count > 0)
        {
            for (int i = 0; i < previewEquipments.Count; i++)
            {
                EquipmentItemBaseValue equipmentBase = previewEquipments[i].itemValue as EquipmentItemBaseValue;
                newStats.UpdateStatsModifiers(equipmentBase.EquipmentSection.ToString(), equipmentBase.statsEfective);
                //newStats.AddStatsValue(equipmentBase.statsEfective);
            }
        }

        return newStats;
    }

    public AcademicStats GetEffectiveAcademicStats()
    {
        // Clone the current stats to avoid referencing it & accidentally changes it's value
        AcademicStats effectiveStats = DeepCopier.DeepCopy(currentAcademicStats);

        ControllableEntity character = PlayerPartyManager.instance.GetPartyMember(characterId);

        if (character == null)
            return effectiveStats;

        PlayerEquipment playerEquipment = character.playerEquipment;

        if (playerEquipment != null)
        {
            List<Item> playerEquipments = playerEquipment.equippedItems;

            if (playerEquipments.Count > 0)
            {
                for (int i = 0; i < playerEquipments.Count; i++)
                {
                    EquipmentItemBaseValue equipmentBase = playerEquipments[i].itemValue as EquipmentItemBaseValue;

                    effectiveStats.UpdateStatsModifiers(equipmentBase.EquipmentSection.ToString(), equipmentBase.academicStats, true);
                }
            }
        }

        return effectiveStats;
    }

    public AcademicStats GetPreviewAcademicStats(List<Item> previewEquipments)
    {
        // Clone the current stats to avoid referencing it & accidentally changes it's value
        AcademicStats newStats = DeepCopier.DeepCopy(currentAcademicStats);

        if (previewEquipments.Count > 0)
        {
            for (int i = 0; i < previewEquipments.Count; i++)
            {
                EquipmentItemBaseValue equipmentBase = previewEquipments[i].itemValue as EquipmentItemBaseValue;
                newStats.UpdateStatsModifiers(equipmentBase.EquipmentSection.ToString(), equipmentBase.academicStats);
                //newStats.AddStatsValue(equipmentBase.statsEfective);
            }
        }

        return newStats;
    }

    #endregion

    #region Skill Tree stuff

    public SkillTreeData GetSubjectSkillTree(AcademicSkillCategory category)
    {
        if (skillTreeDatas == null)
            return null;

        for (int i = 0; i < skillTreeDatas.Count; i++)
        {
            if (skillTreeDatas[i].skillCategory == category)
            {
                return skillTreeDatas[i];
            }
        }

        return null;
    }


    public void UpgradeStats(SubjectStats subjectStatsToUpgrade, BaseStats baseStatsToUpgrade)
    {
        if(subjectStatsToUpgrade != null)
        {
            SubjectStats currentSubjectStats = currentAcademicStats.GetSubjectStats(subjectStatsToUpgrade.Subject);

            currentSubjectStats.AddScore(subjectStatsToUpgrade.finalValue);

            RecalculateArgumentMaxWeight();
        }

        if(baseStatsToUpgrade != null)
        {
            BaseStats currentBaseStats = currentStats.GetBaseStats(baseStatsToUpgrade.StatsName);

            currentBaseStats.Upgrade(baseStatsToUpgrade.finalValue);
        }
    }

    #endregion

    #region Skill Item stuff

    public void EquipSkill(Item skillItem)
    {
        skillsEquipped.Add(skillItem);

        skillItem.Equip();

        RecalculateArgumentWeight();
    }

    public void UnequipSkill(Item skillItem)
    {
        skillsEquipped.Remove(skillItem);

        PlayerInventoryManager.instance.UnequipItem(skillItem.ItemID);

        RecalculateArgumentWeight();
    }

    public bool IsSkillEquipped(Item skillItem)
    {
        for(int i = 0; i < skillsEquipped.Count; i++)
        {
            if (skillsEquipped[i].ItemID == skillItem.ItemID)
                return true;
        }

        return false;
    }

    private void RecalculateArgumentWeight()
    {
        int currentWeight = 0;

        for (int i = 0; i < skillsEquipped.Count; i++)
        {
            SkillItemBaseValue skillItemBase = (SkillItemBaseValue)skillsEquipped[i].itemValue;

            if (skillItemBase == null)
                continue;

            BaseSkill baseSkill = skillItemBase.baseSkill;

            if (baseSkill == null)
                continue;

            currentWeight += baseSkill.skillPoint;
        }

        argumentWeight = currentWeight;
    }

    private void RecalculateArgumentMaxWeight()
    {
        argumentMaxWeight = currentAcademicStats.GetOverallScore();
    }

    public string GetArgumentWeightDescription()
    {
        return string.Format(ArgumentWeightTextFormat, argumentWeight, argumentMaxWeight);
    }

    public bool WillOverlimit(Item skillItem)
    {
        SkillItemBaseValue skillItemBase = (SkillItemBaseValue)skillItem.itemValue;

        if (skillItemBase == null || skillItemBase.baseSkill == null)
            return false;

        return argumentWeight + skillItemBase.baseSkill.skillPoint >= argumentMaxWeight;
    }

    #endregion


    #region Battle stuff

    public void DeductSubjectScore(Subjects subject, int deductAmount)
    {
        SubjectStats subjectStats = currentAcademicStats.GetSubjectStats(subject);

        subjectStats.AddScore(deductAmount);
    }

    #endregion
}
