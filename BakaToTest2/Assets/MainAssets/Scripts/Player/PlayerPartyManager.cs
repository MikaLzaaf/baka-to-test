﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public class PlayerPartyManager : Singleton<PlayerPartyManager>
{
    // What to do here?
    // To store all player party data during play time

    public event Action<CharacterId> PlayerChangedEvent;

    public CharacterSpawnerSO characterSpawner;
    public CharacterId debugCharacter;
    public bool addNewCharacterOnStart = false;

    public bool spawnCharacterAtDifferentPosition = false;
    public Transform spawnPoint;

    public bool isInBattleScene = false;

    private int selectedPLayerIndex = 0;

    private static bool _playerCannotMove;
    public static bool playerCannotMove
    {
        get
        {
            return _playerCannotMove;
        }
        set
        {
            _playerCannotMove = value;
        }
    }

    public List<CharacterId> partyMembersIdList { get; set; }

    public List<ControllableEntity> partyMembers { get; set; }
    public int activeMembersCount { get; set; } = 1;

    private Dictionary<CharacterId, ControllableEntity> _playersLookup;
    public Dictionary<CharacterId, ControllableEntity> playersLookup
    {
        get
        {
            if(_playersLookup == null)
            {
                _playersLookup = new Dictionary<CharacterId, ControllableEntity>();

                if(partyMembers != null)
                {
                    foreach(ControllableEntity entity in partyMembers)
                    {
                        _playersLookup.Add(entity.characterId, entity);
                    }
                } 
            }

            return _playersLookup;
        }
        private set
        {
            _playersLookup = value;
        }
    }


    public ControllableEntity selectedPlayer
    {
        get
        {
            if (partyMembers == null || selectedPLayerIndex < 0 || selectedPLayerIndex >= partyMembers.Count)
            {
                return null;
            }

            return partyMembers[selectedPLayerIndex];
        }
    }


    private PlayerMapData _playerMapData;
    public PlayerMapData playerMapData
    {
        get
        {
            if (_playerMapData == null)
            {
                _playerMapData = new PlayerMapData();
            }

            return _playerMapData;
        }
        set
        {
            _playerMapData = value;
        }
    }


    private void Start()
    {
        if (selectedPlayer != null && selectedPlayer.transform != null && MainCameraController.instance != null)
        {
            MainCameraController.instance.SetThirdPersonFollowAndLookAtTarget(selectedPlayer.transform);
        }
    }

    public void LoadFromSaveData()
    {
        partyMembersIdList = new List<CharacterId>(GameDataManager.gameData.PartyMemberList);

        if (partyMembersIdList.Count == 0)
            return;

        activeMembersCount = GameDataManager.gameData.activeMembersCount;

        if (BattleController.IsInBattle || isInBattleScene)
        {
            partyMembers = new List<ControllableEntity>();

            for (int i = 0; i < partyMembersIdList.Count; i++)
            {
                AddCharacterToParty(partyMembersIdList[i]);
            }

            return;
        }

        playerMapData = GameDataManager.gameData.playerMapData;

        partyMembers = new List<ControllableEntity>();

        int count = partyMembersIdList.Count;

        for (int i = 0; i < count; i++)
        {
            Quaternion spawnRotation = Quaternion.Euler(playerMapData.LoadPlayerRotation());

            ControllableEntity playerEntity = characterSpawner.SpawnControllablePlayer(partyMembersIdList[i], playerMapData.LoadPlayerPosition(),
                                                                  spawnRotation, true);

            AddCharacterToParty(playerEntity);
          
            if (partyMembersIdList[i] != playerMapData.lastSelectedCharacter)
            {
                playerEntity.gameObject.SetActive(false);
            }
            else
            {
                selectedPLayerIndex = i;

                if(spawnCharacterAtDifferentPosition)
                {
                    playerEntity.transform.position = spawnPoint.position;
                }
            }
        }
       
        if (addNewCharacterOnStart)
        {
            AddCharacterToParty(debugCharacter);
        }
    }

    public void RemovePlayerFromParty(CharacterId characterId)
    {
        if (!partyMembersIdList.Contains(characterId))
            return;

        partyMembersIdList.Remove(characterId);

        ControllableEntity playerEntity = GetPartyMember(characterId);

        if (playerEntity != null)
        {
            partyMembers.Remove(playerEntity);
            playersLookup.Remove(characterId);

            Destroy(playerEntity.gameObject);
        }
    }

    public ControllableEntity GetPartyMember(CharacterId playerId)
    {
        playersLookup.TryGetValue(playerId, out ControllableEntity playerEntity);

        return playerEntity;
    }

    public void AddCharacterToParty(CharacterId characterId)
    {
        if (characterId == CharacterId.Undefined)
            return;

        Vector3 spawnPoint = Vector3.zero;
        Quaternion spawnRotation = Quaternion.identity;

        if(selectedPlayer != null)
        {
            spawnPoint = selectedPlayer.transform.position;
            spawnRotation = selectedPlayer.transform.rotation;
        }

        ControllableEntity playerEntity = characterSpawner.SpawnControllablePlayer(characterId, spawnPoint, spawnRotation, true);

        playerEntity.gameObject.SetActive(false);
        
        AddCharacterToParty(playerEntity);
    }

    public void AddCharacterToParty(ControllableEntity playerEntity)
    {
        if (playersLookup.ContainsKey(playerEntity.characterId))
            return;

        playersLookup.Add(playerEntity.characterId, playerEntity);
        partyMembers.Add(playerEntity);

        if (!partyMembersIdList.Contains(playerEntity.characterId))
        {
            partyMembersIdList.Add(playerEntity.characterId);
        }

        playerEntity.ApplySavedData();
    }

    public void SwitchPlayer(CharacterId playerId)
    {
        ControllableEntity playerEntity = GetPartyMember(playerId);

        if (playerEntity != null)
        {
            SwitchPlayer(playerEntity);
        }
    }

    public void SwitchPlayer(ControllableEntity playerEntity)
    {
        int index = partyMembers.IndexOf(playerEntity) - selectedPLayerIndex;
        SwitchPlayer(index);
    }

    public void SwitchPlayer(int value)
    {
        int nextIndex = (selectedPLayerIndex + value) % partyMembers.Count;
        Debug.Log(nextIndex + " // " + selectedPLayerIndex + " // count " + partyMembers.Count);

        //PlayerEntity otherEntity = partyMembers[nextIndex];
        ControllableEntity otherEntity = partyMembers[nextIndex];

        if (selectedPlayer == otherEntity)
        {
            return;
        }

        if (selectedPlayer == null || otherEntity == null)
        {
            return;
        }

        // Copy position and rotation
        otherEntity.transform.position = selectedPlayer.transform.position;
        otherEntity.transform.rotation = selectedPlayer.transform.rotation;

        // Toggle active
        selectedPlayer.gameObject.SetActive(false);

        otherEntity.gameObject.SetActive(true);
        //otherEntity.actionController.SetInputActiveState(true);

        selectedPLayerIndex = nextIndex;

        // Change camera target
        MainCameraController.instance.SetThirdPersonFollowAndLookAtTarget(otherEntity.transform);

        PlayerChangedEvent?.Invoke(otherEntity.characterId);
    }

    public void SpawnPlayerOnMap(Vector3 position, Quaternion rotation)
    {
        if(selectedPlayer != null)
        {
            selectedPlayer.transform.SetParent(null);

            selectedPlayer.characterController.SetPositionAndRotation(position, rotation);

            MainCameraController.instance.SetThirdPersonFollowAndLookAtTarget(selectedPlayer.transform);
        }
    }
   
    public void PrepareForBattle()
    {
        playerCannotMove = true;

        if(selectedPlayer != null)
        {
            playerMapData.Save(selectedPlayer);

            selectedPlayer.gameObject.SetActive(false);
        }
    }

    public void PrepareAfterBattle()
    {
        playerCannotMove = false;

        if (selectedPlayer != null)
        {
            selectedPlayer.gameObject.SetActive(true);
        }
    }

    public void TogglePlayerInput(bool active)
    {
        if(selectedPlayer != null)
        {
            selectedPlayer.playerActionController.SetInputActiveState(active);
            playerCannotMove = !active;
        }
    }

    public List<BattlePartyMember> GetBattlePartyMembers()
    {
        List<BattlePartyMember> battleMemberPrefabs = new List<BattlePartyMember>();

        for (int i = 0; i < partyMembers.Count; i++)
        {
            bool isLeader = partyMembers[i].characterId == CharacterId.MC;

            BattlePartyMember partyMember = new BattlePartyMember("F", partyMembers[i].characterName, isLeader);

            battleMemberPrefabs.Add(partyMember);
        }

        return battleMemberPrefabs;
    }

    public void RemovePartyMembers()
    {
        int countt = partyMembers.Count;

        for (int i = countt - 1; i >= 0; i--)
        {
            DestroyImmediate(partyMembers[i].gameObject);
            partyMembers.RemoveAt(i);
            
        }
    }
}
