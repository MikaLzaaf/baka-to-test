﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PlayerCostumeController : EntityModelBinder
{
    public string initialSelectedModelName;


    protected override void Awake()
    {
        if (defaultEntityModel != null)
        {
            selectedEntityModel = defaultEntityModel;

            OnEntityModelSet();
        }
        else if (modelPrefab != null)
        {
            ClearModel();

            InstantiateEntityModel();

            OnEntityModelSet();
        }

        if (selectedEntityModel == null)
        {
            string defaultPlayerModelName =
                defaultEntityModel != null ? defaultEntityModel.name : null;

            // Set the initial selected model if it is not the default model
            // Set for different costumes later
            if (!string.IsNullOrEmpty(initialSelectedModelName) &&
                initialSelectedModelName != defaultPlayerModelName)
            {
                SetPlayerModel(initialSelectedModelName);

                // Delete the default model
                if (defaultEntityModel != null)
                {
                    Destroy(defaultEntityModel.gameObject);
                }
            }
            else if(string.IsNullOrEmpty(initialSelectedModelName) && modelPrefab != null)
            {
                InstantiateEntityModel();

                if (defaultEntityModel != null)
                {
                    Destroy(defaultEntityModel.gameObject);
                }
            }
            // Set the default model
            else if (defaultEntityModel != null)
            {
                selectedEntityModel = defaultEntityModel;

                OnEntityModelSet();
                //SetPlayerModel(null);
                //OnPlayerModelSet();
            }
        }


        // Load default model if no model is selected
        //if (selectedPlayerModel == null)
        //{
        //    PlayerInfo playerInfo = PlayerInfoManager.GetPlayerInfo(playerEntity.characterId);

        //    if (playerInfo != null)
        //    {
        //        SetPlayerModel(playerInfo.defaultCostumeId);
        //    }
        //}
    }


    private void SetPlayerModel(string modelName)
    {
        // Ignore same model
        if (selectedEntityModel != null && selectedEntityModel.name == modelName)
        {
            return;
        }

        // Destroy current model
        if (selectedEntityModel != null && selectedEntityModel.gameObject != null)
        {
            Destroy(selectedEntityModel.gameObject);
            selectedEntityModel = null;
        }

        //GameObject playerModelPrefab = PlayerInfoManager.LoadPlayerModelPrefab(modelName);
        GameObject playerModelPrefab = defaultEntityModel.gameObject;

        if (playerModelPrefab == null)
        {
            Debug.LogError("Failed to load player model prefab: " + modelName);
            return;
        }

        GameObject playerModelObject = (GameObject)Instantiate(playerModelPrefab);

        // Ensure that the name of the model is same as the prefab
        playerModelObject.name = playerModelPrefab.name;

        playerModelObject.transform.SetParent(this.transform, false);

        // Follow the position and rotation of the prefab
        playerModelObject.transform.localPosition = playerModelPrefab.transform.position;
        playerModelObject.transform.localRotation = playerModelPrefab.transform.rotation;

        selectedEntityModel = playerModelObject.GetComponent<EntityModel>();

        OnEntityModelSet();
    }


    public void SpawnModel()
    {

    }




    protected override void OnEntityModelSet()
    {
        base.OnEntityModelSet();

        //animatorController.WalkOrRun(true);
        animatorController.Idle(0);
    }

}
