﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DayObject : MonoBehaviour
{
    public enum EventPhase
    {
        Null,
        Begin,
        Middle,
        End
    }

    private System.DateTime date = default;

    [SerializeField] private Image beginLineEvent;
    [SerializeField] private Image middleLineEvent;
    [SerializeField] private Image endLineEvent;

    [SerializeField] private Image eventIcon;
    [SerializeField] private Image background;

    [SerializeField] private TextMeshProUGUI dayText;

    [SerializeField] private Color defaultDayTextColor = default;
    [SerializeField] private Color currentDayTextColor = default;
    [SerializeField] private Color selectedDayTextColor = default;

    private Color previousColor = default;

    private const string backgroundResourcesPath = "Calendar/BackgroundDay/";

    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Button>()
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SetLineEvent(EventPhase phase)
    {
        switch (phase)
        {
            case EventPhase.Null:

                if (beginLineEvent.enabled) beginLineEvent.enabled = false;
                if (middleLineEvent.enabled) middleLineEvent.enabled = false;
                if (endLineEvent.enabled) endLineEvent.enabled = false;

                break;

            case EventPhase.Begin:

                if (!beginLineEvent.enabled) beginLineEvent.enabled = true;
                if (middleLineEvent.enabled) middleLineEvent.enabled = false;
                if (endLineEvent.enabled) endLineEvent.enabled = false;

                break;

            case EventPhase.Middle:

                if (beginLineEvent.enabled) beginLineEvent.enabled = false;
                if (!middleLineEvent.enabled) middleLineEvent.enabled = true;
                if (endLineEvent.enabled) endLineEvent.enabled = false;

                break;

            case EventPhase.End:

                if (beginLineEvent.enabled) beginLineEvent.enabled = false;
                if (middleLineEvent.enabled) middleLineEvent.enabled = false;
                if (!endLineEvent.enabled) endLineEvent.enabled = true;

                break;
        }
    }

    public void SetDayText(string day)
    {
        dayText.text = !string.IsNullOrEmpty(day) ? day : null;
    }

    public void SetDayDate(System.DateTime newDate, bool currentDate)
    {
        date = newDate;

        SetDayText(date.Day.ToString());

        if (currentDate) SetCurrentDayColor();
        else SetDefaultDayColor();
    }

    public void SetEventIcon(bool active)
    {
        eventIcon.enabled = active;
    }

    public void SetBackground(string backgroundName)
    {
        if (!string.IsNullOrEmpty(backgroundName))
        {
            background.sprite = Resources.Load(backgroundResourcesPath + backgroundName) as Sprite;
            background.enabled = true;
        }
        else
        {
            background.enabled = false;
        }
    }

    public void SetCurrentDayColor()
    {
        dayText.color = currentDayTextColor;
    }

    public void SetDefaultDayColor()
    {
        dayText.color = defaultDayTextColor;
    }

    public System.DateTime GetDate()
    {
        return date;
    }

    public void SelectDay()
    {
        previousColor = dayText.color;
        dayText.color = selectedDayTextColor;
    }

    public void UnSelectDay()
    {
        dayText.color = previousColor;
    }

    public void ResetDefault()
    {
        SetLineEvent(EventPhase.Null);
        SetEventIcon(false);
        SetDayDate(default, false);
        SetDayText(null);
        // SetBackground(null);
    }
}
