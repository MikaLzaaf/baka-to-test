﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Events;


/// <summary>
/// Handle world time logic...
/// </summary>
namespace Intelligensia.Time
{
    #region enum
    public enum DaySession
    {
        Subuh = 0,
        Pagi = 1,
        Duha = 2,
        Tengahari = 3,
        Zuhur = 4,
        Asar = 5,
        Magrib = 6,
        Isyak = 7,
        Tengahmalam = 8
        
    }
    #endregion

    #region Struct

    public struct EventObj
    {
        public string name;
        public string description;
        public UnityEvent triggerEvent;

        public EventObj(string _name, string _description, UnityEvent _triggerEvent)
        {
            name = _name;
            description = _description;
            triggerEvent = _triggerEvent;
        }
    }
    #endregion


    public class TimeManager : Singleton<TimeManager>
    {
       
        #region externalClass
        [System.Serializable]
        public class TimeObj
        {
            public int year;
            public int month;
            public int day;

            public TimeObj() { }

            public TimeObj(int _year, int _month, int _day)
            {
                year = _year;
                month = _month;
                day = _day;
            }

            public void ChangeDay(int day)
            {
                this.day = day;
                IsDayReachMax();
            }

            public void IsDayReachMax()
            {
                if(day >= DateTime.DaysInMonth(year, month))
                {
                    day = 0;
                    month += 1;
                    IsMonthReachMax();
                }
            }

            public void IsMonthReachMax()
            {
                if(month >= 12)
                {
                    month = 0;
                }
            }
        }
        #endregion


        #region Fields & Property

        public const string dateFormat = "dd-mm-yyyy";

        private TimeObj _currentDateTime;
        public TimeObj currentDateTime
        {
            get
            {
                return _currentDateTime;
            }
        }

        [SerializeField]
        private TimeObj defaultDateTime;

        private DaySession _currentDaySession;
        public DaySession currentDaySession
        {
            get
            {
                return _currentDaySession;
            }       
        }

        public WorldTimeEventStorage eventStorage;

        /// <summary>
        /// Dictionary <year, <month, <day, <daySession, List eventObj>>>>
        /// </summary>
        private Dictionary<int, Dictionary<int, Dictionary<int, Dictionary<DaySession, List <EventObj>>>>> eventList;

        #endregion

        #region Events

        public UnityEvent OnDaySessionChange;
        public UnityEvent OnDateChange;

        #endregion
        private void Awake()
        {
            SetDefaultDate();
        }

        // Start is called before the first frame update
        void Start()
        {
            eventList = new Dictionary<int, Dictionary<int, Dictionary<int, Dictionary<DaySession, List<EventObj>>>>>();

            SerializeEventFromStorage();
        }

        // Update is called once per frame
        void Update()
        {

        }

       

        public void AdvanceTime()
        {

        }

        private void AdvanceDaySession()
        {
            int session = (int)currentDaySession;
            session += 1;

            if(session > GetDaySessionCount())
            {
                session = 0;
                AdvanceDate();
            }

            ChangeDaySessionByNumber(session);

            OnDaySessionChange?.Invoke();
        }

        private void AdvanceDate()
        {
            _currentDateTime.ChangeDay(_currentDateTime.day + 1);

            OnDateChange?.Invoke();
        }

        private void ChangeDaySessionByNumber(int targetDaySession)
        {
            _currentDaySession = (DaySession)targetDaySession;
        }

        private void ChangeDaySessionByName(string targetDaySession)
        {
            _currentDaySession = (DaySession)Enum.Parse(typeof(DaySession), targetDaySession);
        }

        public int GetDaySessionCount()
        {
            return Enum.GetValues(typeof(DaySession)).Length;
        }

        public string GetCurrentDaySessionName()
        {
            return GetDaySessionName(currentDaySession);
        }

        public string GetDaySessionName(DaySession session)
        {
            return session.ToString();
        }

        private void SetDefaultDate()
        {
            defaultDateTime = new TimeObj();
            defaultDateTime.day = System.DateTime.Now.Day;
            defaultDateTime.month = System.DateTime.Now.Month;
            defaultDateTime.year = System.DateTime.Now.Year;
            _currentDateTime = defaultDateTime;
        }

        /// <summary>
        /// Load date from save...
        /// </summary>
        private void LoadDate(TimeObj loadDate)
        {
            _currentDateTime = loadDate;
        }

        private void SerializeEventFromStorage()
        {
            for(int i = 0; i < eventStorage.events.Count; i++)
            {
                int year = eventStorage.events[i].year;
                int month = eventStorage.events[i].month;
                int day = eventStorage.events[i].day;
                
                for(int k = 0; k < eventStorage.events[i].events.Count; k++)
                {
                    DaySession daySession = eventStorage.events[i].events[k].daySession;
                    var list = eventStorage.events[i].events[k].events;
                    CheckDictionaryAvailability(year, month, day, daySession);

                    for (int p = 0; p < list.Count; p++)
                    {
                        AddEventSingle(year, month, day, daySession, new EventObj(list[p].name, list[p].description, list[p].triggerEvent));
                    }
                }
            }
        }

        private void CheckDictionaryAvailability(int year, int month, int day, DaySession daySession)
        {
            if (!eventList.ContainsKey(year))
                eventList.Add(year, new Dictionary<int, Dictionary<int, Dictionary<DaySession, List<EventObj>>>>());

            if (!eventList[year].ContainsKey(month))
                eventList[year].Add(month, new Dictionary<int, Dictionary<DaySession, List<EventObj>>>());

            if (!eventList[year][month].ContainsKey(day))
                eventList[year][month].Add(day, new Dictionary<DaySession, List<EventObj>>());

            if (!eventList[year][month][day].ContainsKey(daySession))
                eventList[year][month][day].Add(daySession, new List<EventObj>());
        }

        public void AddEventList(int year, int month, int day, DaySession daySession, List<EventObj> ev, bool replace)
        {
            if(replace)
            {
                eventList[year][month][day][daySession] = ev;
            }
            else
            {
                for(int i = 0; i < ev.Count; i++)
                {
                    eventList[year][month][day][daySession].Add(ev[i]);
                }
            }
        }

        public void AddEventSingle(int year, int month, int day, DaySession daySession, EventObj ev)
        {
            var list = eventList[year][month][day][daySession];

            for (int i = 0; i < list.Count; i++)
            {
                if(list[i].name == ev.name)
                {
                    Debug.LogError(string.Format("Same event name({0}) in day session({1}). Date {2}",
                        ev.name,
                        daySession.ToString(),
                        (string.Format("{0}/{1}/{2}", day, month, year))
                        ));
                    return;
                }
            }
            eventList[year][month][day][daySession].Add(ev);
        }

        public void RemoveEvent(int year, int month, int day, DaySession daySession, EventObj ev)
        {
            if (eventList[year][month][day][daySession].Contains(ev))
                eventList[year][month][day][daySession].Remove(ev);
        }

        public void RemoveAllEventOfDaySession(int year, int month, int day, DaySession daySession)
        {
            eventList[year][month][day][daySession].Clear();
        }

        public void RemoveAllCalendarEvents()
        {
            eventList.Clear();
        }

        public List<EventObj> GetEventList(int year, int month, int day, DaySession daySession)
        {
            return eventList[year][month][day][daySession];
        }

        public int GetEventCountByDaySession(int year, int month, int day, DaySession daySession)
        {
            return eventList[year][month][day][daySession].Count;
        }
        
        public int GetEventCountByDay(int year, int month, int day)
        {
            int count = 0;
            List<EventObj> temp;

            for (int i = 0; i < GetDaySessionCount(); i++)
            {
                eventList[year][month][day].TryGetValue((DaySession)i, out temp);
                count += temp.Count;
            }

            return count;
        }
    }
}

