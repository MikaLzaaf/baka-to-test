﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Intelligensia.Time;

/// <summary>
/// Handle inside calendar logic...
/// </summary>
public class CalendarController : MonoBehaviour
{
    #region variables & property field
    public TimeManager.TimeObj currentWorldTime
    {
        get
        {
            return TimeManager.instance.currentDateTime;
        }
    }

    private TimeManager.TimeObj _currentCalendarTime;

    public TimeManager.TimeObj currentCalendarTime
    {
        get
        {
            return _currentCalendarTime;
        }
    }
#endregion

    #region delegates
    public delegate void Delegate_OnDaySelected(TimeManager.TimeObj time);
    public delegate void Delegate_OnEventSelected(TimeManager.TimeObj time, List<TimeManager.TimeObj> evs);
    public delegate void Delegate_OnMonthChanged(TimeManager.TimeObj time);
    public delegate void Delegate_OnNowDay(TimeManager.TimeObj time);
    public delegate void Delegate_OnInsertDays(int firstIndex, int totalDays);
    public Delegate_OnDaySelected delegate_ondayselected;
    public Delegate_OnEventSelected delegate_oneventselected;
    public Delegate_OnMonthChanged delegate_onmonthchanged;
    public Delegate_OnNowDay delegate_onnowday;
    public Delegate_OnInsertDays delegate_oninsertdays;
    #endregion

    // Start is called before the first frame update
    void Start()
    {
       /* ChangeCalendarTime(currentWorldTime);
        delegate_oninsertdays?.Invoke(GetIndexOfFirstSlotInMonth(), GetTotalDays());*/
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void InitCalendar()
    {
       /* // Add Event Listeners
        addEventsListener();

        // Update Calendar with Current Data
        UpdateCalendar(currentWorldTime.month, currentWorldTime.year);

        // Mark Current Day
        markSelectionDay(currentWorldTime.day);

        // Update Label Event
        updateUiLabelEvents(currentWorldTime.year, currentWorldTime.month, currentWorldTime.day);*/
    }

    private void UpdateCalendar(int month_number, int year)
    {
        // Populate day slots
/*        PopulateAllSlot(month_number, year);
*/    
        
    }

    public void RefreshCalendar()
    {
/*        PopulateAllSlot(currentDateTime.month, currentDateTime.month);
*/    }

   private void DisableAllSlot()
    {
/*        for (int i = 0; i < max_day_slots; i++)
            disableSlot(i + 1);*/
    }

    public void ChangeCalendarTime(TimeManager.TimeObj timeObj)
    {
        _currentCalendarTime = timeObj;
    }

	public void NextMonth()
	{
        /*unmarkSelctionDay(currentTime.day);

        currentCalendarTime.month = (currentCalendarTime.month + 1) % 13;
		if (currentCalendarTime.month == 0)
		{
			currentCalendarTime.year++;
			currentCalendarTime.month = 1;
		}


		currentCalendarTime.day = 1;
		currentCalendarTime.dayOfWeek = GetDayOfWeek(currentTime.year, currentTime.month, currentTime.day);
		currentCalendarTime.dayOffset = GetIndexOfFirstSlotInMonth(currentTime.year, currentTime.month);

		updateCalendar(currentTime.month, currentTime.year);

		markSelectionDay(currentTime.day);

		// Update label event
		updateUiLabelEvents(currentTime.year, currentTime.month, currentTime.day);

		// Send Callback
		if (delegate_onmonthchanged != null)
			delegate_onmonthchanged(currentCalendarTime);*/
	}

	void evtListener_PreviousMonth()
	{
/*		unmarkSelctionDay(currentTime.day);

		currentCalendarTime.month = (currentCalendarTime.month - 1) % 13;
		if (currentCalendarTime.month == 0)
		{
			currentCalendarTime.year--;
			currentCalendarTime.month = 12;
		}

		currentCalendarTime.day = 1;
		currentCalendarTime.dayOfWeek = getDayOfWeek(currentTime.year, currentTime.month, currentTime.day);
		currentCalendarTime.dayOffset = getIndexOfFirstSlotInMonth(currentTime.year, currentTime.month);

		updateCalendar(currentTime.month, currentTime.year);

		markSelectionDay(currentTime.day);

		// Update label event
		updateUiLabelEvents(currentTime.year, currentTime.month, currentTime.day);

		// Send Callback
		if (delegate_onmonthchanged != null)
			delegate_onmonthchanged(currentCalendarTime);*/

	}

	void evtListener_DaySelected()
	{
		// Unmark old slot
/*		unmarkSelctionDay(currentTime.day);

		// Update current day
		string slot_name = EventSystem.current.currentSelectedGameObject.name;
		int slot_position = int.Parse(slot_name.Substring(5, (slot_name.Length - 5)));
		currentCalendarTime.day = getDayInSlot(slot_position);
		currentCalendarTime.dayOfWeek = getDayOfWeek(currentTime.year, currentTime.month, currentTime.day);

		// Mark current slot
		markSelectionDay(currentCalendarTime.day);

		// Update label event
		updateUiLabelEvents(currentTime.year, currentTime.month, currentTime.day);

		// Send Callback
		if (delegate_ondayselected != null)
			delegate_ondayselected(currentCalendarTime);

		// Send Callback
		if (getEventList(currentTime.year, currentTime.month, currentTime.day).Count > 0)
			if (delegate_oneventselected != null)
				delegate_oneventselected(currentTime, getEventList(currentTime.year, currentTime.month, currentTime.day));*/
	}

	void evtListener_GoToNowday()
	{
		// Unmark old slot
/*		unmarkSelctionDay(currentTime.day);

		// Set Current Time
		setCurrentTime();

		// Update Calendar
		updateCalendar(currentTime.month, currentTime.year);

		// Mark Selection Day
		markSelectionDay(currentTime.day);

		// Update label event
		updateUiLabelEvents(currentTime.year, currentTime.month, currentTime.day);

		// Send Callback
		if (delegate_onnowday != null)
			delegate_onnowday(currentTime);*/
	}

    public string GetMonthStringFromNumber(int month_number)
    {
        string month = "";

        if (month_number == 1) month = "Genuary";
        if (month_number == 2) month = "February";
        if (month_number == 3) month = "March";
        if (month_number == 4) month = "April";
        if (month_number == 5) month = "May";
        if (month_number == 6) month = "June";
        if (month_number == 7) month = "July";
        if (month_number == 8) month = "August";
        if (month_number == 9) month = "September";
        if (month_number == 10) month = "October";
        if (month_number == 11) month = "November";
        if (month_number == 12) month = "December";

        return month;
    }

    public string GetMonthStringFromNumber()
    {
        return GetMonthStringFromNumber(currentCalendarTime.month);
    }

    public string GetDayOfWeek(int year, int month, int day)
    {
        System.DateTime dateValue = new System.DateTime(year, month, day);

        return dateValue.DayOfWeek.ToString();
    }

    public string GetDayOfWeek()
    {
        return GetDayOfWeek(currentCalendarTime.year, currentCalendarTime.month, currentCalendarTime.day);
    }

    public int GetIndexOfFirstSlotInMonth(int year, int month)
    {
        int indexOfFirstSlot = 0;
        string dayOfWeek = GetNameOfDay(year, month, 1);

        if (dayOfWeek == "Sunday") indexOfFirstSlot = 0;
        if (dayOfWeek == "Monday") indexOfFirstSlot = 1;
        if (dayOfWeek == "Tuesday") indexOfFirstSlot = 2;
        if (dayOfWeek == "Wednesday") indexOfFirstSlot = 3;
        if (dayOfWeek == "Thursday") indexOfFirstSlot = 4;
        if (dayOfWeek == "Friday") indexOfFirstSlot = 5;
        if (dayOfWeek == "Saturday") indexOfFirstSlot = 6;
        

        return indexOfFirstSlot;
    }

    public string GetNameOfDay(int year, int month, int day)
    {
        System.DateTime dateValue = new System.DateTime(year, month, day);

        return dateValue.DayOfWeek.ToString();
    }

    public int GetIndexOfFirstSlotInMonth()
    {
        return GetIndexOfFirstSlotInMonth(currentCalendarTime.year, currentCalendarTime.month);
    }

    public int GetTotalDays()
    {
        return System.DateTime.DaysInMonth(currentCalendarTime.year, currentCalendarTime.month);
    }
}
