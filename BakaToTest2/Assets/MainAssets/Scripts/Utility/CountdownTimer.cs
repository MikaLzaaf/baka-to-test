﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using TMPro;

public class CountdownTimer : ViewController
{
    public event Action CountdownEndedEvent;

    [Header("UI")]
    public Image countdownFill;
    public TextMeshProUGUI countdownText;

    [Header("Parameters")]
    public float duration = 5f;
    public string textFormat = "{0} : {1}";
    public bool displayTime = false;
    public bool displayUsingBar = true;
    public bool autoCloseAfterFinish = false;

    private float elapsedTimeSinceStarted = 0f;
    private bool canCountdown = false;


    private void ResetTimer()
    {
        elapsedTimeSinceStarted = 0;

        if (displayUsingBar)
        {
            countdownFill.fillAmount = 1f;
        }

        if (displayTime)
        {
            if (countdownText != null)
            {
                countdownText.text = "";
            }
        }
    }

    public void StartCountdown()
    {
        ResetTimer();

        canCountdown = true;
    }

    public void StopCountdown()
    {
        if (!canCountdown)
            return;

        canCountdown = false;

        ResetTimer();

        if (autoCloseAfterFinish)
            Close();
    }

    public void StopCountdown(bool callEvent)
    {
        if (callEvent)
            CountdownEndedEvent?.Invoke();

        StopCountdown();
    }

    private void Update()
    {
        if(canCountdown)
        {
            elapsedTimeSinceStarted += Time.deltaTime;

            UpdateBar();

            UpdateText();

            if (elapsedTimeSinceStarted >= duration)
            {
                CountdownEndedEvent?.Invoke();

                StopCountdown();
            }
        }
    }


    private void UpdateBar()
    {
        if (!displayUsingBar || countdownFill == null)
            return;

        countdownFill.fillAmount = (duration - elapsedTimeSinceStarted) / duration;
    }

    private void UpdateText()
    {
        if (!displayTime || countdownText == null)
            return;

        countdownText.text = (duration - elapsedTimeSinceStarted).ToString("F2");
    }
}
