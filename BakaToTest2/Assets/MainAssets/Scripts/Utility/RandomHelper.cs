using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class RandomHelper
{
	public static int[] GetRandomArrayIndexes(int indexesCount, int arrayLength)
	{
		if (indexesCount <= 0)
		{
			return null;	
		}
		
		
		int clampedIndexesCount = Mathf.Clamp(indexesCount, 0, arrayLength);
		
		int[] selectedIndexes = new int[clampedIndexesCount];
		
		
		List<int> availableIndexes = new List<int>();
		
		for (int i = 0; i < arrayLength; i++)
		{
			availableIndexes.Add(i);
		}
		
		
		for (int i = 0; i < selectedIndexes.Length; i++)
		{
			int randomIndexPosition = Random.Range(0, availableIndexes.Count);
			
			selectedIndexes[i] = availableIndexes[randomIndexPosition];
			
			availableIndexes.RemoveAt(randomIndexPosition);
		}
		
		
		return selectedIndexes;
	}


	public static int ChooseProbability(params float[] probs)
	{
		float total = 0;

		foreach (float elem in probs)
		{
			total += elem;
		}

		float randomPoint = Random.value * total;

		for (int i = 0; i < probs.Length; i++)
		{
			if (randomPoint < probs[i]) 
			{
				return i;
			}
			else
			{
				randomPoint -= probs[i];
			}
		}

		return probs.Length - 1;
	}

}
