﻿using UnityEngine;
using System;
using System.Collections;

public static class CoroutineExtensions
{
	public static IEnumerator WaitFor(this MonoBehaviour target, float seconds, Action action)
	{
		yield return new WaitForSeconds(seconds);

		if (action != null)
		{
			action();
		}
	}


	public static IEnumerator WaitFor(this MonoBehaviour target, IEnumerator coroutine, Action action)
	{
		yield return target.StartCoroutine(coroutine);
		
		if (action != null)
		{
			action();
		}
	}


	public static IEnumerator WaitForWhere(this MonoBehaviour target, Func<bool> predicate, Action action)
	{
		while (!predicate())
		{
			yield return null;
		}

		if (action != null)
		{
			action();
		}
	}
}
