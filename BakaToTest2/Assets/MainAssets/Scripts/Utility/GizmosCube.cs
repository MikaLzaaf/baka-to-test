﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GizmosCube : MonoBehaviour 
{
    public bool shaded = true;
    public Color shadeColor = Color.grey;
    
	public bool wireframe = true;
    public Color wireframeColor = Color.green;

    public bool drawOnSelected = false;


#if UNITY_EDITOR
	private void OnDrawGizmos()
	{
		if (!drawOnSelected)
		{
            DrawCube();
		}
	}


	private void OnDrawGizmosSelected()
	{
		if (drawOnSelected)
		{
            DrawCube();
		}
	}


    private void DrawCube()
    {
        Gizmos.matrix *= transform.localToWorldMatrix;

        if (shaded)
        {
            Gizmos.color = shadeColor;
            Gizmos.DrawCube(Vector3.zero, Vector3.one);
        }

        if (wireframe)
        {
            Gizmos.color = wireframeColor;
            Gizmos.DrawWireCube(Vector3.zero, Vector3.one);
        }
    }
#endif
}
