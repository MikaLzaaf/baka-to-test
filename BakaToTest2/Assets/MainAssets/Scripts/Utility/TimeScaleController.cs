﻿using UnityEngine;
using System;
using System.Collections;

public class TimeScaleController : MonoBehaviour
{
    public enum UpdateMode
    {
        Normal,
        UnscaledTime
    }


    public event Action<UpdateMode> UpdateModeChangedEvent;


	public bool setDeltatImeToZeroWhenTimeScaleIsZero = true;


	public float deltaTime
	{
		get
		{
			if (updateMode == UpdateMode.UnscaledTime)
			{
				if (setDeltatImeToZeroWhenTimeScaleIsZero && Time.timeScale == 0)
				{
					return 0;
				}

				return Time.unscaledDeltaTime;
			}

			return Time.deltaTime;
		}
	}

	/*
    public float deltaTimeMultiplier
    {
        get
        {
            if (updateMode == UpdateMode.UnscaledTime)
            {
				if (setDeltatImeToZeroWhenTimeScaleIsZero && Time.timeScale == 0)
                {
                    return 0;
                }

                return 1 / Time.timeScale;
            }

            return 1;
        }
    }
	*/

    public UpdateMode _updateMode = UpdateMode.Normal;
    public UpdateMode updateMode
    {
        get
        {
            return _updateMode;
        }
        set
        {
            _updateMode = value;

            if (UpdateModeChangedEvent != null)
            {
                UpdateModeChangedEvent(_updateMode);
            }
        }
    }

}