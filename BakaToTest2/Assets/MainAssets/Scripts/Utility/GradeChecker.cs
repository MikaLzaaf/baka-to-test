﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GradeChecker 
{
    private static readonly string GradeSuffix_S = "S";
    private static readonly string GradeSuffix_APlus = "A+";
    private static readonly string GradeSuffix_A = "A";
    private static readonly string GradeSuffix_AMinus = "A-";
    private static readonly string GradeSuffix_BPlus = "B+";
    private static readonly string GradeSuffix_B = "B";
    private static readonly string GradeSuffix_BMinus = "B-";
    private static readonly string GradeSuffix_CPlus = "C+";
    private static readonly string GradeSuffix_C = "C";
    private static readonly string GradeSuffix_CMinus = "C-";
    private static readonly string GradeSuffix_DPlus = "D+";
    private static readonly string GradeSuffix_D = "D";
    private static readonly string GradeSuffix_DMinus = "D-";
    private static readonly string GradeSuffix_E = "E";
    private static readonly string GradeSuffix_F = "F";


    public static readonly string BadResult = "Bad";
    public static readonly string GoodResult = "Good";
    public static readonly string GreatResult = "Great";


    public static string GetGrade(int score)
    {
        string grade = "";

        if (score > 110)
        {
            grade = GradeSuffix_S;
        }
        else if (score >= 90)
        {
            grade = GradeSuffix_APlus;
        }
        else if (score >= 80)
        {
            grade = GradeSuffix_A;
        }
        else if (score >= 75)
        {
            grade = GradeSuffix_AMinus;
        }
        else if (score >= 70)
        {
            grade = GradeSuffix_BPlus;
        }
        else if (score >= 65)
        {
            grade = GradeSuffix_B;
        }
        else if (score >= 60)
        {
            grade = GradeSuffix_BMinus;
        }
        else if (score >= 55)
        {
            grade = GradeSuffix_CPlus;
        }
        else if (score >= 50)
        {
            grade = GradeSuffix_C;
        }
        else if (score >= 45)
        {
            grade = GradeSuffix_CMinus;
        }
        else if (score >= 40)
        {
            grade = GradeSuffix_DPlus;
        }
        else if (score >= 35)
        {
            grade = GradeSuffix_D;
        }
        else if (score >= 30)
        {
            grade = GradeSuffix_DMinus;
        }
        else if (score >= 25)
        {
            grade = GradeSuffix_E;
        }
        else
        {
            grade = GradeSuffix_F;
        }

        return grade;
    }


    public static string ConvertGradeToResult(string grade)
    {
        string result;

        if(grade == GradeSuffix_F || grade == GradeSuffix_E || grade == GradeSuffix_DMinus ||
            grade == GradeSuffix_D || grade == GradeSuffix_DPlus || grade == GradeSuffix_CMinus)
        {
            result = BadResult;
        }
        else if (grade == GradeSuffix_C || grade == GradeSuffix_CPlus || grade == GradeSuffix_BMinus ||
            grade == GradeSuffix_B || grade == GradeSuffix_BPlus )
        {
            result = GoodResult;
        }
        else if(grade == GradeSuffix_AMinus || grade == GradeSuffix_A || grade == GradeSuffix_APlus || 
            grade == GradeSuffix_S)
        {
            result = GreatResult;
        }
        else
        {
            result = "Unknown";
        }

        return result;
    }
    

    public static int GetGradeScore(string grade)
    {
        int score;

        if (grade == GradeSuffix_S)
        {
            score = 110;
        }
        else if (grade == GradeSuffix_APlus)
        {
            score = 90;
        }
        else if (grade == GradeSuffix_A)
        {
            score = 80;
        }
        else if (grade == GradeSuffix_AMinus)
        {
            score = 75;
        }
        else if (grade == GradeSuffix_BPlus)
        {
            score = 70;
        }
        else if (grade == GradeSuffix_B)
        {
            score = 65;
        }
        else if (grade == GradeSuffix_BMinus)
        {
            score = 60;
        }
        else if (grade == GradeSuffix_CPlus)
        {
            score = 55;
        }
        else if (grade == GradeSuffix_C)
        {
            score = 50;
        }
        else if (grade == GradeSuffix_CMinus)
        {
            score = 45;
        }
        else if (grade == GradeSuffix_DPlus)
        {
            score = 40;
        }
        else if (grade == GradeSuffix_D)
        {
            score = 35;
        }
        else if (grade == GradeSuffix_DMinus)
        {
            score = 30;
        }
        else if (grade == GradeSuffix_E)
        {
            score = 25;
        }
        else
        {
            score = 20;
        }

        return score;
    }


    public static bool IsGradeOneBetter(string gradeOne, string gradeTwo)
    {
        return GetGradeScore(gradeOne) >= GetGradeScore(gradeTwo);
    }
}
