﻿using UnityEngine;
using System.Collections;

public class AutoRecycle : MonoBehaviour 
{
	public float duration;


	private float elapsed;


	private void Update()
	{
		elapsed += Time.deltaTime;

		if (elapsed > duration)
		{
			elapsed = 0;

			ObjectPool.Recycle(this.gameObject);
		}
	}
}
