﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetParentOnEnable : MonoBehaviour
{
    public Transform parent;
    public Vector3 positionOffset;
    private void OnEnable()
    {
        transform.SetParent(parent, false);
        transform.localPosition += positionOffset;
    }
}
