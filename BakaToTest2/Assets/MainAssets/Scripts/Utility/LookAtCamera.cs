﻿using UnityEngine;
using System.Collections;

public class LookAtCamera : MonoBehaviour
{
    public Camera targetCamera;
    public float lookDirectionSign = 1;
    public Vector3 upwards = Vector3.up;
    public bool followTargetCameraUpwards = false;


    private Transform targetCameraTransform;
    private Vector3 lookUpwards;


    private void Awake()
    {
        if (targetCamera == null)
        {
            targetCamera = Camera.main;
            targetCameraTransform = targetCamera.transform;
        }
    }


    private void Update()
    {
        lookUpwards = followTargetCameraUpwards ? targetCameraTransform.up : upwards;

        if (targetCamera != null)
        {
            transform.rotation =
                Quaternion.LookRotation((targetCameraTransform.position - transform.position) * lookDirectionSign, lookUpwards);
        }
        
    }
}
