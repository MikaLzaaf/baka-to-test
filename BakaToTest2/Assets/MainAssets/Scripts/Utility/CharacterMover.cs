﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMover : MonoBehaviour
{
    public Transform destination;
    public bool runToMove = false;

    private void OnTriggerEnter(Collider other)
    {
        if(other.TryGetComponent(out EntityCharacterController entityCaracterController))
        {
            entityCaracterController.AutoMove(runToMove, destination.position);
        }
    }
}
