﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using System.IO;

/// <summary>
/// nead change/tweak a bit for more friendly
/// </summary>
public static class JsonSerialization 
{
    private const string FileType = ".json";


	public static void JsonSerialize<T>(string path, string folderName, T data, string saveName)
	{
        try
        {
            var streamWriter = new StreamWriter (Path.Combine(path,folderName,saveName+".txt"));
            string jsonString = JsonUtility.ToJson(data);
		    streamWriter.Write (jsonString);
		    streamWriter.Close ();
		    Debug.Log ("Done Serialize" + saveName);
		    Debug.Log (path);
        }
        catch(Exception exc)
        {
            Debug.LogWarning(exc);
        }
	}


    public static void JsonSerialize<T>(string path, T data)
    {
        try
        {
            //if(!File.Exists(path + FileType))
            //{
            //    File.Create(path + FileType).Dispose();
            //}

            if (!File.Exists(path))
            {
                File.Create(path).Dispose();
            }

            //StreamWriter outStream = System.IO.File.CreateText(path + FileType);
            //var streamWriter = new StreamWriter(path + FileType);
            string jsonString = JsonUtility.ToJson(data);
            Debug.Log(jsonString);
            File.WriteAllText(path, jsonString);
            //streamWriter.Write(jsonString);
            //streamWriter.Close();
        }
        catch (Exception exc)
        {
            Debug.LogWarning(exc);
        }
    }

//	public static T JsonDeserialize<T>(string saveName)
//	{
//		if (!File.Exists (path + @"\" + folderName + @"\" + saveName + ".txt"))
//			return default(T);
//		var streamReader = new StreamReader (path + @"\" + folderName + @"\" + saveName + ".txt");
//		string value = streamReader.ReadToEnd ();
//		streamReader.Close ();
//		Debug.Log (value);
//		var result = JsonUtility.FromJson<T> (value);
//
//
//		Debug.Log ("Done Deserialize" + saveName);
//
//		return result;
//	}

	public static T JsonDeserialize<T>(string path, string folderName, string saveName)
	{
        try
        {
            var streamReader = new StreamReader(Path.Combine(path, folderName, saveName + ".txt"));
            string fileData = streamReader.ReadToEnd();
            streamReader.Close();
            var result = JsonUtility.FromJson<T>(fileData);
            return result;
        }
        catch(Exception exc)
        {
            Debug.LogWarning(exc);
        }

        return default;
	}


    public static T JsonDeserialize<T>(string path)
    {
        try
        {
            //var streamReader = new StreamReader(path + FileType);
            var streamReader = new StreamReader(path);
            string fileData = streamReader.ReadToEnd();
            Debug.Log(fileData);
            streamReader.Close();
            var result = JsonUtility.FromJson<T>(fileData);
            return result;
        }
        catch (Exception exc)
        {
            Debug.LogWarning(exc);
        }

        return default;
    }


    public static T JsonDeserializeInResources<T>(string dataFileInResources)
    {
        T dataObject = default(T);

        TextAsset dataJson = Resources.Load<TextAsset>(dataFileInResources);

        if (dataJson != null)
        {
            dataObject = JsonUtility.FromJson<T>(dataJson.text);
        }
        else
        {
            Debug.Log("Data is null");
        }

         return dataObject;
    }
}
