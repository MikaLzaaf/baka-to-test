﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class SlideOnEnable : MonoBehaviour
{
    public Vector3 slideEndPosition;
    public float slideDuration = 0.5f;
    public bool snapAtTheEnd = false;

    private Vector3 originalPosition;


    private void OnEnable()
    {
        originalPosition = transform.localPosition;

        transform.DOLocalMove(originalPosition + slideEndPosition, slideDuration, snapAtTheEnd);
    }


    private void OnDisable()
    {
        transform.localPosition = originalPosition;
    }
}
