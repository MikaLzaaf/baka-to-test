﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class HealthBar : MonoBehaviour
{
    public event Action<float> OnValueChanged;

    [SerializeField] private Transform healthBar = default;
    [SerializeField] private float maxValue;
    //[SerializeField] private float minValue = 0;

    public float value
    {
        get
        {
            return healthBar.localScale.x;
        }
    }

    public void Initialize(float maxValue)
    {
        this.maxValue = maxValue;
    }

    
    public void SetValue(float value)
    {
        float valueNormalized = value / maxValue;

        healthBar.localScale = new Vector3(valueNormalized, 1f);

        OnValueChanged?.Invoke(valueNormalized);
    }
}
