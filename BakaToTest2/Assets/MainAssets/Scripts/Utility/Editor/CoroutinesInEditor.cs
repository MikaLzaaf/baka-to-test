﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

[InitializeOnLoad]
public static class CoroutinesInEditor
{
    public static void ExecuteCoroutine(IEnumerator coroutine)
    {
        EditorCoroutines.Execute(coroutine);
    }
}

