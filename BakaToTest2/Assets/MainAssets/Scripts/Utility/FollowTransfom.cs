﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowTransfom : MonoBehaviour
{
    public Transform targetTransform;

    public Vector3 positionOffset;

    public bool followPosition = true;
    public bool followRotation = true;
    public bool followScale = true;



    private void LateUpdate()
    {
        if (targetTransform == null)
            return;

        if(followPosition && followRotation)
        {
            transform.SetPositionAndRotation(targetTransform.position + positionOffset, targetTransform.localRotation);
        }
        else
        {
            if (followPosition)
            {
                transform.position = targetTransform.position;
            }

            if (followRotation)
            {
                transform.rotation = targetTransform.rotation;
            }
        }

        if(followScale)
        {
            transform.localScale = targetTransform.localScale;
        }
    }
}
