﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class SpawnQTEOnEnable : MonoBehaviour
{

    public PlayableDirector playableDirector;
    public QuickTimeEventController quickTimeController;

    public float slowScale = 0.4f;
    public float triggerDuration = 2f;
    public bool triggerUsesSlowMotion = true;
    public bool isTimerType = false;

    // Start is called before the first frame update
    void OnEnable()
    {
        quickTimeController.SpawnQTETrigger(triggerDuration, quickTimeController.ExecuteQTE, false, triggerUsesSlowMotion);

        if (playableDirector != null)
        {
            //playableDirector.played += SetSpeed;
            SetSpeed(playableDirector, slowScale); // in case play on awake is set
        }

        //timeScale = 1f;
    }


    private void OnDisable()
    {
        Debug.Log("Disabling QTE");
        SetSpeed(playableDirector, 1f);
    }

    void SetSpeed(PlayableDirector director, float scale)
    {
        if (director != null && director.playableGraph.IsValid())
        {
            director.playableGraph.GetRootPlayable(0).SetSpeed(scale);
        }
    }

    //void OnValidate()
    //{
    //    SetSpeed(GetComponent<PlayableDirector>());
    //}
}
