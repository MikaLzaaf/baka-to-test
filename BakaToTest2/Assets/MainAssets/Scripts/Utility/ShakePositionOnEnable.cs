﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ShakePositionOnEnable : MonoBehaviour
{

    public float duration = 0.5f;
    public float strength = 1;
    public int vibrato = 10;
    public float randomness = 90;
    public bool snap;
    public bool fadeOut = true;

    [Header("Other Shakers")]
    public ShakePositionOnEnable[] otherShakers;
    public bool activateOtherShakersOnEnable = true;

    private void OnEnable()
    {
        if(activateOtherShakersOnEnable)
        {
            for (int i = 0; i < otherShakers.Length; i++)
            {
                otherShakers[i].enabled = true;
            }
        }

        transform.DOShakePosition(duration, strength, vibrato, randomness, snap, fadeOut);
    }

    private void OnDisable()
    {
        if (activateOtherShakersOnEnable)
        {
            for (int i = 0; i < otherShakers.Length; i++)
            {
                otherShakers[i].enabled = false;
            }
        }
    }


}
