﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIRadarChart : MonoBehaviour
{
    //[SerializeField] private int numberOfCorners = default;
    [SerializeField] private float radarChartSize = 100f;
    [SerializeField] private CanvasRenderer canvasRenderer = default;
    [SerializeField] private Material radarMaterial = default;
    [SerializeField] private Texture2D radarTexture = default;

    // For testing
    [SerializeField] private bool displayOnStart = false;
    [SerializeField] private float[] normalizedValues = default;


    private void Start()
    {
        if(normalizedValues.Length > 0 && displayOnStart)
            UpdateRadarVisual();
    }


    

    [ContextMenu("Update Radar")]
    private void UpdateRadarVisual()
    {
        UpdateRadarVisual(normalizedValues);
    }


    public void UpdateRadarVisual(float[] normalizedValues)
    {
        Mesh mesh = new Mesh();

        Vector3[] vertices = new Vector3[normalizedValues.Length + 1];
        Vector2[] uv = new Vector2[normalizedValues.Length + 1];
        int[] triangles = new int[3 * normalizedValues.Length];

        float angleIncrement = 360f / normalizedValues.Length;

        // Origin for chart
        vertices[0] = Vector3.zero;

        // Corners of chart
        for (int i = 0; i < normalizedValues.Length; i++)
        {
            Vector3 vertex = Quaternion.Euler(0, 0, -angleIncrement * i) * Vector3.up * radarChartSize * normalizedValues[i];
            vertices[i + 1] = vertex;
        }

        for(int i = 0; i < uv.Length; i++)
        {
            if (i == 0)
                uv[i] = Vector2.zero;
            else
                uv[i] = Vector2.one;
        }


        int count = 2;

        for (int i = 0; i < triangles.Length; i++)
        {
            if (i % 3 == 0)
            {
                triangles[i] = 0;
                count--;
            }
            else
            {
                if (count >= vertices.Length)
                    count = 1;

                triangles[i] = count;
                count++;
            }
        }

        mesh.vertices = vertices;
        mesh.uv = uv;
        mesh.triangles = triangles;

        canvasRenderer.SetMesh(mesh);
        canvasRenderer.SetMaterial(radarMaterial, radarTexture);
    }
}
