using UnityEngine;
using System.Collections;


/// <summary>
/// This class provides functions related to the calculations of projectile trajectory.
/// </summary>
public static class ProjectileTrajectoryHelper
{
	/// <summary>
	/// Calculates the required firing angle for the projectile to reach the target position.
	/// </summary>
	public static float? CalculateFiringAngleInDegree(float projectileSpeed, Vector3 firingPosition, Vector3 targetPosition, float gravity)
	{		
		float verticalDisplacement = targetPosition.y - firingPosition.y;
		
		float horizontalDisplacement = (new Vector3(firingPosition.x, targetPosition.y, firingPosition.z) - targetPosition).magnitude;
		
		
		float firingAngleInRadian = Mathf.Atan(
		                         	(Mathf.Pow(projectileSpeed, 2) + 
		                          	Mathf.Sqrt(Mathf.Pow(projectileSpeed, 4) - (gravity * ((gravity * Mathf.Pow(horizontalDisplacement, 2)) + (2 * verticalDisplacement * Mathf.Pow(projectileSpeed, 2)))))) / 
		                         	(gravity * horizontalDisplacement));	
		
		
		if (!float.IsNaN(firingAngleInRadian))
		{
			return firingAngleInRadian * Mathf.Rad2Deg;
		}
		else
		{
			return null;	
		}
	}
	
	
	/// <summary>
	/// Calculates the required speed for the projectile to reach the target position.
	/// </summary>
	public static float? CalculateProjectileSpeed(float firingAngleInDegree, Vector3 firingPosition, Vector3 targetPosition, float gravity)
	{
		float firingAngleInRadian = firingAngleInDegree * Mathf.Deg2Rad;
		
		float verticalDisplacement = targetPosition.y - firingPosition.y;
		
		float horizontalDisplacement = (new Vector3(firingPosition.x, targetPosition.y, firingPosition.z) - targetPosition).magnitude;
		
		
		float projectileSpeed = Mathf.Sqrt(
		                                   ((gravity * Mathf.Pow(horizontalDisplacement, 2)) * (Mathf.Pow(Mathf.Tan(firingAngleInRadian), 2) + 1)) / 
		                                   (2 * (horizontalDisplacement * Mathf.Tan(firingAngleInRadian) - verticalDisplacement)));
				
		
		if (!float.IsNaN(projectileSpeed))
		{
			return projectileSpeed;
		}
		else
		{		
			return null;
		}
	}
	
	
	/// <summary>
	/// Calculates the velocity of the projectile based on the provided arguments.
	/// </summary>
	public static Vector3 CalculateProjectileVelocity(float firingAngleInDegree, float projectileSpeed, Vector3 firingPosition, Vector3 targetPosition)
	{				
		float yRotationAngleInDegree = Mathf.Atan2((targetPosition.z - firingPosition.z),
														(targetPosition.x - firingPosition.x)) * Mathf.Rad2Deg;
		
		return CalculateProjectileVelocity(firingAngleInDegree, projectileSpeed, yRotationAngleInDegree);
	}
	
	
	/// <summary>
	/// Calculates the velocity of the projectile based on the provided arguments.
	/// </summary>
	public static Vector3 CalculateProjectileVelocity(float firingAngleInDegree, float projectileSpeed, float yRotationAngleInDegree)
	{				
		Vector3 projectileVelocity = new Vector3();
		projectileVelocity.y = Mathf.Sin(firingAngleInDegree * Mathf.Deg2Rad);
		projectileVelocity.x = Mathf.Cos(firingAngleInDegree * Mathf.Deg2Rad);		
		
		projectileVelocity = Quaternion.AngleAxis(yRotationAngleInDegree, -Vector3.up) * projectileVelocity;
		
		projectileVelocity = projectileVelocity.normalized * projectileSpeed;
		
		return projectileVelocity;
	}
	
	
	public static Vector3 CalculateProjectileVelocityWithAngle(float firingAngleInDegree, Vector3 firingPosition, Vector3 targetPosition, float gravity)
	{
		float? projectileSpeed = CalculateProjectileSpeed(firingAngleInDegree, firingPosition, targetPosition, gravity);

		if (projectileSpeed.HasValue)
		{
			return CalculateProjectileVelocity(firingAngleInDegree, projectileSpeed.Value, firingPosition, targetPosition);
		}
		else
		{
			return Vector3.zero;	
		}
	}
	
	
	public static Vector3 CalculateProjectileVelocityWithSpeed(float projectileSpeed, Vector3 firingPosition, Vector3 targetPosition, float gravity)
	{
		float? firingAngleInDegree = CalculateFiringAngleInDegree(projectileSpeed, firingPosition, targetPosition, gravity);
		
		if (firingAngleInDegree.HasValue)
		{
			return CalculateProjectileVelocity(firingAngleInDegree.Value, projectileSpeed, firingPosition, targetPosition);
		}
		else
		{
			return Vector3.zero;	
		}
	}	
	
	
	/// <summary>
	/// Calculates the position of the projectile at specified time.
	/// </summary>
	public static Vector3 CalculateProjectilePositionAtTime(float time, Vector3 initialPosition, Vector3 velocity, float gravity)
	{
		Vector3 position = initialPosition + (velocity * time);
		
		position.y += (0.5f * -Mathf.Abs(gravity) * Mathf.Pow(time, 2));
		
		return position;
	}
	
}
