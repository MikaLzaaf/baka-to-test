using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

public static class StringHelper
{	
	private const string Ellipsis = "...";
	

	public static string ReverseString(string s)
	{
		char[] arr = s.ToCharArray();
		Array.Reverse(arr);
		return new string(arr);
	}


	/// <summary>
	/// Word wraps the given text to fit within the specified width.
	/// </summary>
	/// <param name="text">Text to be word wrapped</param>
	/// <param name="width">Width, in characters, to which the text
	/// should be word wrapped</param>
	/// <returns>The modified text</returns>
	public static string WrapWord(string text, int width)
	{
	    int pos, next;
	    StringBuilder sb = new StringBuilder();
	
	    // Lucidity check
	    if (width < 1)
	        return text;
	
	    // Parse each line of text
	    for (pos = 0; pos < text.Length; pos = next)
	    {
	        // Find end of line
	        int eol = text.IndexOf(Environment.NewLine, pos);
	        if (eol == -1)
	            next = eol = text.Length;
	        else
	            next = eol + Environment.NewLine.Length;
	
	        // Copy this line of text, breaking into smaller lines as needed
	        if (eol > pos)
	        {
	            do
	            {
	                int len = eol - pos;
	                if (len > width)
	                    len = BreakLine(text, pos, width);
	                sb.Append(text, pos, len);
	                sb.Append(Environment.NewLine);
	
	                // Trim whitespace following break
	                pos += len;
	                while (pos < eol && Char.IsWhiteSpace(text[pos]))
	                    pos++;
	            } while (eol > pos);
	        }
	        else sb.Append(Environment.NewLine); // Empty line
	    }
	    return sb.ToString();
	}
	
	
	/// <summary>
	/// Locates position to break the given line so as to avoid
	/// breaking words.
	/// </summary>
	/// <param name="text">String that contains line of text</param>
	/// <param name="pos">Index where line of text starts</param>
	/// <param name="max">Maximum line length</param>
	/// <returns>The modified line length</returns>
	private static int BreakLine(string text, int pos, int max)
	{
	    // Find last whitespace in line
	    int i = max;
	    while (i >= 0 && !Char.IsWhiteSpace(text[pos + i]))
	        i--;
	
	    // If no whitespace found, break at maximum length
	    if (i < 0)
	        return max;
	
	    // Find start of whitespace
	    while (i >= 0 && Char.IsWhiteSpace(text[pos + i]))
	        i--;
	
	    // Return length of text before whitespace
	    return i + 1;
	}	
		
	
	public static string WrapTextMesh(TextMesh textMesh, string text, float maxLineWidth, 
		int maxLines = int.MaxValue, bool appendEllipsisWhenOverflow = true)
	{
		if (string.IsNullOrEmpty(text))
		{
			return text;	
		}
		
		
		string originalText = textMesh.text;
		
		
		Renderer textMeshRenderer = textMesh.GetComponent<Renderer>();
		
	   	StringBuilder result = new StringBuilder();
		
		bool isNewLinePreviously = false;
	
		float spaceWidth = MeasureTextMeshWidth(textMesh, textMeshRenderer, " ");
		
		
		int currentLine = 1;

		
		string[] lines = text.Split(Environment.NewLine.ToCharArray());
		
		for (int i = 0; i < lines.Length; i++)
		{
			// Overflow
			if (currentLine > maxLines)
			{
				break;	
			}
			
			
			if (string.IsNullOrEmpty(lines[i]))
			{
				if (isNewLinePreviously)
				{
					result.AppendLine();
					
					currentLine++;
					
					// Overflow
					if (currentLine > maxLines)
					{
						break;	
					}
					
					isNewLinePreviously = false;
				}
				else
				{
					isNewLinePreviously = true;
				}
				
				continue;
			}
			
			isNewLinePreviously = false;
			
			
			
			float lineWidth = 0;
			
			string[] words = lines[i].Split(' ');

			string word = null;
			
			for (int j = 0; j < words.Length; j++)
			{
				word = words[j];
				
				// Overflow
				if (currentLine > maxLines)
				{
					break;	
				}
				
				
				float wordSize = MeasureTextMeshWidth(textMesh, textMeshRenderer, word);
		 
				if (lineWidth + wordSize < maxLineWidth)
				{
					result.Append(word);
					result.Append(' ');
					lineWidth += wordSize + spaceWidth;
				}
				// Split the individual word
				else if (wordSize > maxLineWidth || (!string.IsNullOrEmpty(word) && currentLine == maxLines))
				{
					int wordPartStartIndex = 0;					
					int wordPartLength = 1;
					
					float wordPartSize = 0;
					
					while (true)
					{
						wordPartSize = MeasureTextMeshWidth(textMesh, textMeshRenderer, word.Substring(wordPartStartIndex, wordPartLength));
						
						//Debug.Log(lineWidth + " " + wordPartSize + " " + word.Substring(wordPartStartIndex, wordPartLength));
						
						if (lineWidth + wordPartSize > maxLineWidth)
						{			
							//Debug.Log(word.Substring(wordPartStartIndex, wordPartLength));
							
							if (wordPartStartIndex == 0 && wordPartLength == 1)
							{
								if (currentLine < maxLines)
								{
									result.AppendLine();
									lineWidth = 0;
									
									wordPartLength = 0;
								}
								
								currentLine++;
							}
							else
							{
								result.Append(word.Substring(wordPartStartIndex, wordPartLength - 1));
								
								if (currentLine < maxLines)
								{
									result.AppendLine();
									lineWidth = 0;
									
									wordPartStartIndex += wordPartLength - 1;
									wordPartLength = 0;
								}
								
								currentLine++;
							}
							
							
							
							// Overflow
							if (currentLine > maxLines)
							{
								break;	
							}
						}
						
						wordPartLength++;
						
						if (wordPartStartIndex + wordPartLength > word.Length)
						{
							string lastWordPart = word.Substring(wordPartStartIndex, word.Length - wordPartStartIndex);
							
							result.Append(lastWordPart);
							result.Append(' ');

							lineWidth = MeasureTextMeshWidth(textMesh, textMeshRenderer, lastWordPart) + spaceWidth;
							
							break;
						}
					}
				}
				else
				{
					result.AppendLine();
					
					result.Append(word);
					result.Append(' ');
					lineWidth = wordSize + spaceWidth;
					
					currentLine++;
				}
			}			
			
			
			if (i < lines.Length - 1)
			{
				result.AppendLine();	
				
				currentLine++;
			}
		}
		
		
		string output = result.ToString();
		
		
		if (currentLine > maxLines && appendEllipsisWhenOverflow)
		{
			int lastNewLineIndex = output.LastIndexOf(Environment.NewLine);
			
			int lastLineStartIndex = lastNewLineIndex + 1;
			
			if (lastLineStartIndex < output.Length - 1)
			{
				string lastLine = output.Substring(lastLineStartIndex, output.Length - lastLineStartIndex).Trim();
					
				float lastLineWidth = MeasureTextMeshWidth(textMesh, textMeshRenderer, lastLine);
				
				float ellipsisWidth = MeasureTextMeshWidth(textMesh, textMeshRenderer, Ellipsis);
				
				if (lastLine.Length > 0 && lastLineWidth + ellipsisWidth > maxLineWidth)
				{
					int ellipsisInsertIndex = lastLine.Length - 1;
					
					string lastLinePart = null;
					
					float lastLinePartWidth = 0;
					
					while (true)
					{
						if (ellipsisInsertIndex < 0)
						{
							break;	
						}
						

						if (!char.IsWhiteSpace(lastLine[ellipsisInsertIndex]))
						{
							lastLinePart = lastLine.Substring(0, ellipsisInsertIndex + 1);
							
							lastLinePartWidth = MeasureTextMeshWidth(textMesh, textMeshRenderer, lastLinePart);
							
							if (lastLinePartWidth + ellipsisWidth <= maxLineWidth)
							{								
								break;	
							}
						}
						
						ellipsisInsertIndex--;
					}
					
					if (ellipsisInsertIndex > 0)
					{
						int retainedWordsCount = lastLineStartIndex + ellipsisInsertIndex + 1;
						
						if (retainedWordsCount <= output.Length)
						{		
							output = output.Substring(0, retainedWordsCount) + Ellipsis;	
						}
					}
				}
			}
		}
		
		
		textMesh.text = originalText;
		
		
		return output.TrimEnd();
	}
	
	
	private static float MeasureTextMeshWidth(TextMesh textMesh, Renderer textMeshRenderer, string text)
	{
		if (string.IsNullOrEmpty(text))
		{
			return 0;
		}
		
		
		// If the text contains while spaces only
		if (text.Trim().Length == 0)
		{
			textMesh.text = string.Format("a{0}a", text);
			float width1 = textMeshRenderer.bounds.size.x;	
						
			textMesh.text = "aa";
			float width2 = textMeshRenderer.bounds.size.x;	
			
			return width1 - width2;
		}
		else
		{		
			textMesh.text = text;
			
			return textMeshRenderer.bounds.size.x;
		}
	}


	public static string Implode(this IList input, string delimiter)
	{
		if (input.Count == 0) return string.Empty;
		if (input.Count == 1) return input[0].ToString();
		
		var sb = new StringBuilder();
		for (int i = 0; i < input.Count - 1; i++)
		{
			sb.AppendFormat("{0}{1}", input[i],delimiter);
		}
		sb.Append(input[input.Count - 1]);
		return sb.ToString();
	}

	/// <summary>
	/// Add space between each word that starts with a capitalize first letter.
	/// </summary>
	/// <param name="word"></param>
	/// <returns></returns>
	public static string SplitByCapitalizeFirstLetter(string word)
	{
		string result = "";

		for (int i = 0; i < word.Length; i++)
		{
			if (char.IsUpper(word[i]))
			{
				result += ' ';
			}

			result += word[i];
		}

		return result.Trim();
	}

	public static string UpperFirst(string text)
	{
		return char.ToUpper(text[0]) +
			((text.Length > 1) ? text.Substring(1).ToLower() : string.Empty);
	}
}