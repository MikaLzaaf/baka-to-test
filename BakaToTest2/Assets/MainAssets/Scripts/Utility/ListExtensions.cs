﻿using System.Collections;
using System.Collections.Generic;
using System;

public static class ListExtensions 
{
    private static readonly Random rng = new Random();

    //Fisher - Yates shuffle
    public static void Shuffle<T>(this IList<T> list)
    {
        int n = list.Count;
        while (n > 1)
        {
            n--;
            int k = rng.Next(n + 1);
            T value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
    }

    public static void ShuffleRange<T>(this IList<T> list, int startIndex, int count)
    {
        int n = startIndex + count;
        int limit = startIndex + 1;
        while (n > limit)
        {
            n--;
            int k = rng.Next(startIndex, n + 1);
            T value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
    }
}
