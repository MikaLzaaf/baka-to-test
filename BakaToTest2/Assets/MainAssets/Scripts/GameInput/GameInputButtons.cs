﻿public enum GameInputButtons
{
    Undefined,
    Horizontal,
    Vertical,
    Triangle,
    Circle,
    Cross,
    Square,
    OptionMenu,
    Dash,
    LeftTop,
    MouseX,
    MouseY


}
// TODO : Add input name same as gamepad PS4