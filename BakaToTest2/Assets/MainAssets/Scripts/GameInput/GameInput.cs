﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CnControls;

public class GameInput : MonoBehaviour {

    public static GameInput instance;

#if UNITY_EDITOR
    //private static bool useTouchControlInEditor = true;
#endif

    // TODO : Add input name same as gamepad PS4

    //private const string HorizontalAxis = "Horizontal"; // Left stick x axis
    //private const string VerticalAxis = "Vertical"; // Left stick y axis

    public bool enableInput = true;
    public bool flipMovementAxisX = false;

    private Vector2 movementInputAxis;
    private Vector2 actionInputAxis;

    private bool isCameraChangedSuddenly = false;
    private Vector3 lastMovementDirection;
    private Vector2 lastMovementInput;


    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public Vector2 GetRightStickMovementInput()
    {
        movementInputAxis.x = Input.GetAxis(GameInputButtons.MouseY.ToString());
        movementInputAxis.y = Input.GetAxis(GameInputButtons.MouseX.ToString());


        //if (flipMovementAxisX)
        //{
        //    movementInputAxis.x *= -1;
        //}

        return movementInputAxis;
    }

    public Vector2 GetMovementInput()
    {
        movementInputAxis.x = Input.GetAxis(GameInputButtons.Horizontal.ToString());
        movementInputAxis.y = Input.GetAxis(GameInputButtons.Vertical.ToString());

        if(flipMovementAxisX)
        {
            movementInputAxis.x *= -1;
        }

        return movementInputAxis;
    }

    public Vector3 ConvertInputToWorldDirection(Vector2 inputDirection, Transform cameraTransform)
    {
        if(isCameraChangedSuddenly)
        {
            return lastMovementDirection;
        }

        Vector3 forward = cameraTransform.TransformDirection(Vector3.forward);
        forward.y = 0;
        forward = forward.normalized;

        Vector3 right = new Vector3(forward.z, 0f, -forward.x);

        Vector3 worldDirection = forward * inputDirection.y + right * inputDirection.x;
        worldDirection.Normalize();

        lastMovementDirection = worldDirection;

        return worldDirection;
    }


    public bool GetButtonInput(GameInputButtons inputButton)
    {
        return Input.GetButtonDown(inputButton.ToString());
    }

    public bool GetAnyKey()
    {
        return Input.anyKeyDown;
    }

    public bool GetAnyInputExcept(GameInputButtons inputButton)
    {
        return Input.anyKeyDown && !Input.GetButtonDown(inputButton.ToString());
    }


    public void DelaySuddenInputChanges()
    {
        isCameraChangedSuddenly = true;

        lastMovementInput = movementInputAxis;

        StartCoroutine(DelayingInput());
    }


    private IEnumerator DelayingInput()
    {
        yield return new WaitForSeconds(0.5f);

        yield return new WaitUntil(() => lastMovementInput != movementInputAxis);

        isCameraChangedSuddenly = false;
    }
}
