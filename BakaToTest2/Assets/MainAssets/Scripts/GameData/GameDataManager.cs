﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class GameDataManager : Singleton<GameDataManager>
{
    // Will change this later
    public static string storyProgressID = "Chapter_0";

    public static event Action GameDataSavedEvent;
    public static event Action GameDataDeletedEvent;

    //private static bool UseBinarySerialization = true;
    private static bool UseDebugGameDataOnCreateNewGameData = false;

    private const string RelativeGameDataFilePath = "GameData.json"; // Can add file type at the end if needed
    private const string DefaultGameDataInResources = "Data/DefaultGameData";
    private const string DebugGameDataInResources = "Data/DebugGameData";

    private static string GameDataFilePath
    {
        get
        {
            return Application.persistentDataPath + "/" + RelativeGameDataFilePath;
        }
    }

    private static GameData _gameData;
    public static GameData gameData
    {
        get
        {
            return _gameData;
        }
        private set
        {
            _gameData = value;
        }
    }


    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;
        }

        transform.parent = null;
        DontDestroyOnLoad(this.gameObject);

        if (gameData == null)
        {
            Load();
        }
    }


    private void OnApplicationQuit()
    {
        Save();
    }


    public static bool IsGameDataFileExisted()
    {
        return File.Exists(GameDataFilePath);
    }


    public static void Load()
    {
        // Create a new game data if the data file not exists
        if (!File.Exists(GameDataFilePath))
        {
            CreateGameData();
            return;
        }

        // Since we use json this time
        gameData = JsonSerialization.JsonDeserialize<GameData>(GameDataFilePath);

        //if (UseBinarySerialization)
        //{
        //    gameData = DataSerializer.DeserializeBinaryData(GameDataFilePath) as GameData;
        //}
        //else
        //{
        //    gameData = DataSerializer.DeserializeXmlFromFile<GameData>(GameDataFilePath);
        //}


        if (gameData == null)
        {
            Debug.LogError("Failed to load data at " + GameDataFilePath);

            // Reset the game data if it cannot be loaded
            Reset();

            return;
        }

        gameData.OnLoaded();

        //Debug.Log("Loaded game data.");
    }


    public static void LoadGameData(GameData data)
    {
        if (data == null)
        {
            return;
        }

        // Load local game data, since we need to compare it with the cloud save,
        // to make sure that the cloud save will not overwrite the local save if
        // the local save has more progress
        if (gameData == null)
        {
            if (IsGameDataFileExisted())
            {
                Load();
            }
        }

        // Don't overwrite existing game data if the progress of the new game data is older
        if (gameData != null)
        {
            if (data.totalPlayTime < gameData.totalPlayTime)
            {
                return;
            }
        }

        gameData = data;

        gameData.OnLoaded();
    }


    public static void Save()
    {
        if (gameData == null)
        {
            Debug.LogError("Game data not exists.");
            return;
        }

        // Don't save game data if it is tampered
        //if (IsGameDataTampered())
        //{
        //    Debug.Log("Not saving. Game data is tampered.");
        //    return;
        //}

        if (BattleController.IsInBattle)
        {
            return;
        }

        gameData.OnPreSave();


        //if (UseBinarySerialization)
        //{
        //    DataSerializer.SerializeBinaryData(gameData, GameDataFilePath);
        //}
        //else
        //{
        //    DataSerializer.SerializeXmlToFile<GameData>(gameData, GameDataFilePath);
        //}

        JsonSerialization.JsonSerialize<GameData>(GameDataFilePath, gameData);

        GameDataSavedEvent?.Invoke();

        //Debug.Log("Saved game data.");
    }


    public static void Delete()
    {
        if (File.Exists(GameDataFilePath))
        {
            File.Delete(GameDataFilePath);
        }

        PlayerPrefs.DeleteAll();

        if (GameDataDeletedEvent != null)
        {
            GameDataDeletedEvent();
        }

        Debug.Log("Deleted game data.");
    }


    public static void Reset()
    {
        Delete();

        // Re-create the game data
        CreateGameData();
    }


    public static void CreateGameData()
    {
        if (UseDebugGameDataOnCreateNewGameData)
        {
            CreateGameData(DebugGameDataInResources);
        }
        else
        {
            CreateGameData(DefaultGameDataInResources);
        }
    }


    public static GameData LoadGameDataInResources(string gameDataInResources)
    {
        //return JsonSerialization.JsonDeserializeInResources<GameData>(gameDataInResources);

        return DataSerializer.DeserializeXmlInResources<GameData>(gameDataInResources);
        //return JsonSerialization.JsonDeserialize<GameData>(gameDataInResources);
    }


    //public static GameData LoadDefaultGameDataInResources()
    //{
    //    return DataSerializer.DeserializeXmlInResources<GameData>(DefaultGameDataInResources);
    //}


    public static void CreateGameData(string gameDataInResources)
    {
        string dataDirectory = Path.GetDirectoryName(GameDataFilePath);

        if (!Directory.Exists(dataDirectory))
        {
            Directory.CreateDirectory(dataDirectory);
        }
        Debug.Log(gameDataInResources);
        // Try to load default game data
        gameData = LoadGameDataInResources(gameDataInResources);

        if (gameData == null)
        {
            gameData = new GameData();
        }

        gameData.OnLoaded();

        Save();
    }


#if UNITY_EDITOR
    [MenuItem("Custom/GameData/Reveal Game Data")]
    private static void RevealGameData()
    {
        UnityEditor.EditorUtility.RevealInFinder(GameDataFilePath);
    }

    [MenuItem("Custom/GameData/Reset Game Data (Default)")]
    private static void ResetGameDataDefault_Editor()
    {
        ResetGameDataDefault();
    }


    [MenuItem("Custom/GameData/Reset Game Data (Debug)")]
    private static void ResetGameDataDebug_Editor()
    {
        ResetGameDataDebug();
    }
#endif

    public static void ResetGameDataDefault()
    {
        Delete();
        CreateGameData(DefaultGameDataInResources);
    }

    public static void ResetGameDataDebug()
    {
        Delete();
        CreateGameData(DebugGameDataInResources);

        Save();
    }
}
