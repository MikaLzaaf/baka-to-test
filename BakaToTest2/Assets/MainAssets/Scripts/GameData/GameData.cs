﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

[Serializable]
public class GameData 
{
    public const string CurrencyName = "Ringgit";
    public const string CurrencyUnit = "RM";
    public const int MaxCurrency = 999999999;

    [SerializeField] private int _currency = 0;
    public int currency
    {
        get
        {
            return _currency;
        }
        set
        {
            _currency = value;
        }
    }

    public string formattedCurrencyWithUnit
    {
        get
        {
            return GetFormattedCurrencyWithUnit(_currency);
        }
    }

    [SerializeField] private PlayerInventory _playerInventory;
    public PlayerInventory playerInventory
    {
        get
        {
            if (_playerInventory == null)
            {
                _playerInventory = new PlayerInventory();
            }

            return _playerInventory;
        }
        set
        {
            _playerInventory = value;
        }
    }


    [SerializeField] private PlayerMapData _playerMapData;
    public PlayerMapData playerMapData
    {
        get
        {
            if (_playerMapData == null)
            {
                _playerMapData = new PlayerMapData();
            }

            return _playerMapData;
        }
        set
        {
            _playerMapData = value;
        }
    }


    [SerializeField] private List<string> _battleOrdersLearned = new List<string>();
    public List<string> battleOrdersLearned
    {
        get
        {
            if (_battleOrdersLearned == null)
            {
                _battleOrdersLearned = new List<string>();
            }

            return _battleOrdersLearned;
        }
        set
        {
            _battleOrdersLearned = value;
        }
    }


    [SerializeField] private List<ChallengeInfo> _challengeInfos = new List<ChallengeInfo>();
    public List<ChallengeInfo> challengeInfos
    {
        get
        {
            if (_challengeInfos == null)
            {
                _challengeInfos = new List<ChallengeInfo>();
            }

            return _challengeInfos;
        }
        set
        {
            _challengeInfos = value;
        }
    }

    #region Party Data

    [SerializeField] private List<PlayerStatsData> _partyStatsDataList = new List<PlayerStatsData>();
    public List<PlayerStatsData> partyStatsDataList
    {
        get
        {
            if (_partyStatsDataList == null)
            {
                _partyStatsDataList = new List<PlayerStatsData>();
            }

            return _partyStatsDataList;
        }
        set
        {
            _partyStatsDataList = value;
        }
    }


    private Dictionary<CharacterId, PlayerStatsData> _partyDataLookup;
    private Dictionary<CharacterId, PlayerStatsData> partyDataLookup
    {
        get
        {
            if (_partyDataLookup == null)
            {
                _partyDataLookup = new Dictionary<CharacterId, PlayerStatsData>();

                if (partyStatsDataList != null)
                {
                    foreach (PlayerStatsData playerdata in partyStatsDataList)
                    {
                        _partyDataLookup.Add(playerdata.characterId, playerdata);
                    }
                }
            }

            return _partyDataLookup;
        }
    }


    [SerializeField] private List<CharacterId> partyMemberList = new List<CharacterId>();
    public List<CharacterId> PartyMemberList
    {
        get
        {
            if (partyMemberList == null)
            {
                partyMemberList = new List<CharacterId>();
            }

            return partyMemberList;
        }
        set
        {
            partyMemberList = value;
            Debug.Log("added party member " + partyMemberList.Count);
        }
    }


    #endregion

    #region Bestiary Data

    [SerializeField] private List<CharacterStatsData> _bestiaryStatsDataList = new List<CharacterStatsData>();
    public List<CharacterStatsData> bestiaryStatsDataList
    {
        get
        {
            if (_bestiaryStatsDataList == null)
            {
                _bestiaryStatsDataList = new List<CharacterStatsData>();
            }

            return _bestiaryStatsDataList;
        }
        set
        {
            _bestiaryStatsDataList = value;
        }
    }


    private Dictionary<string, CharacterStatsData> _bestiaryDataLookup;
    private Dictionary<string, CharacterStatsData> bestiaryDataLookup
    {
        get
        {
            if (_bestiaryDataLookup == null)
            {
                _bestiaryDataLookup = new Dictionary<string, CharacterStatsData>();

                if (partyStatsDataList != null)
                {
                    foreach (CharacterStatsData playerdata in bestiaryStatsDataList)
                    {
                        _bestiaryDataLookup.Add(playerdata.characterName, playerdata);
                    }
                }
            }

            return _bestiaryDataLookup;
        }
    }

    #endregion

    [NonSerialized]
    private float lastSavedTime = 0;

    [SerializeField] private int _activeMembersCount = 1;
    public int activeMembersCount
    {
        get
        {
            return _activeMembersCount;
        }
        set
        {
            _activeMembersCount = value;
        }
    }

    [SerializeField] private float _totalPlayTime = 0;
    public float totalPlayTime
    {
        get
        {
            return _totalPlayTime;
        }
        set
        {
            _totalPlayTime = value;
        }
    }


    [SerializeField] private Difficulty globalDifficulty = Difficulty.Normal;
    public Difficulty GlobalDifficulty
    {
        get
        {
            return globalDifficulty;
        }
    }

    


    public void OnLoaded()
    {
        currency = _currency;
        
        if(_playerInventory == null)
        {
            InitializePlayerInventory();
        }

        if(PartyMemberList.Count == 0)
        {
            AddPartyMember(CharacterId.MC);
            //return;
        }

        PlayerInventoryManager.instance.LoadFromSaveData();

        PlayerPartyManager.instance.LoadFromSaveData();

        BattleOrdersManager.instance.LoadFromSaveData();

        if(MapManager.instance != null)
            MapManager.instance.LoadFromSaveData();

        DailyInfoManager.LoadChallengeData();
    }


    public void OnPreSave()
    {
        _currency = currency;

        // Record total play time
        float playTime = Time.realtimeSinceStartup - lastSavedTime;

        if (playTime > 0)
        {
            totalPlayTime += playTime;
        }

        lastSavedTime = Time.realtimeSinceStartup;

        playerMapData.Save();

        playerInventory.Save();

        battleOrdersLearned = BattleOrdersManager.instance.Save();

        challengeInfos = DailyInfoManager.currentChallengeInfos;
       
        SavePartyData();
    }


    public static string GetFormattedCurrencyWithUnit(int currency)
    {
        return string.Format("{0} {1}", CurrencyUnit, currency);
    }


    private void InitializePlayerInventory()
    {
        DefaultInventoryInfo defaultInventoryInfo = ItemDataManager.LoadDefaultInventoryInfo();

        foreach (Item item in defaultInventoryInfo.defaultInventoryItems)
        {
            playerInventory.AddItem(item);
        }

        //DefaultIlmuPoolListInfo defaultIlmuPoolListInfo = ItemDataManager.LoadDefaultIlmuPoolListInfo();

        //for(int i = 0; i < defaultIlmuPoolListInfo.defaultIlmuPoolInfos.Count; i++)
        //{
        //    DefaultIlmuPoolInfo defaultIlmuPoolInfo = defaultIlmuPoolListInfo.defaultIlmuPoolInfos[i];

        //    if (playerInventory.HasIlmuPool(defaultIlmuPoolInfo.characterId))
        //        continue;

        //    PlayerIlmuPool playerIlmuPool = new PlayerIlmuPool(defaultIlmuPoolInfo.characterId, defaultIlmuPoolInfo.defaultIlmuItems);

        //    playerInventory.AddIlmuPool(playerIlmuPool);
        //}
    }


#region Player Data related

    private void SavePartyData()
    {
        partyMemberList = PlayerPartyManager.instance.partyMembersIdList;

        if (partyMemberList != null && partyMemberList.Count > 0)
        {
            partyStatsDataList = new List<PlayerStatsData>();
            _partyDataLookup = new Dictionary<CharacterId, PlayerStatsData>();

            for (int i = 0; i < PlayerPartyManager.instance.partyMembers.Count; i++)
            {
                ControllableEntity playerEntity = PlayerPartyManager.instance.partyMembers[i];

                AddPlayerStatsData(playerEntity);
            }
        }

        activeMembersCount = PlayerPartyManager.instance.activeMembersCount;

#if UNITY_EDITOR
        if (!UnityEditor.EditorApplication.isPlaying)
        {
            PlayerPartyManager.instance.RemovePartyMembers();
        }
#endif


    }

    private void AddPlayerStatsData(ControllableEntity entity)
    {
        PlayerStatsData characterData = GetPlayerStatsData(entity.characterId);

        if (characterData == null)
        {
            partyDataLookup.Add(entity.characterId, entity.playerStatsData);
            partyStatsDataList.Add(entity.playerStatsData);
        }
    }

    private void RemovePlayerStatsData(ControllableEntity entity)
    {
        PlayerStatsData characterData = GetPlayerStatsData(entity.characterId);

        if (characterData != null)
        {
            partyStatsDataList.Remove(characterData);
            partyDataLookup.Remove(entity.characterId);
        }
    }

    public PlayerStatsData GetPlayerStatsData(CharacterId entityName)
    {
        partyDataLookup.TryGetValue(entityName, out PlayerStatsData playerData);

        return playerData;
    }

    public void AddPartyMember(CharacterId characterId)
    {
        if (characterId == CharacterId.Undefined || PartyMemberList.Contains(characterId))
        {
            return;
        }

        PartyMemberList.Add(characterId);
    }

    private void RemovePartyMember(CharacterId characterId)
    {
        for (int i = PartyMemberList.Count - 1; i >= 0; i--)
        {
            if (PartyMemberList[i] == characterId)
            {
                PartyMemberList.RemoveAt(i);
            }
        }
    }
#endregion

#region Bestiary Data related

    public void AddBestiaryStatsData(BattleEntity entity)
    {
        CharacterStatsData characterData = GetBestiaryStatsData(entity.characterName);

        if (characterData == null)
        {
            bestiaryDataLookup.Add(entity.characterName, entity.characterStatsData);
            bestiaryStatsDataList.Add(entity.characterStatsData);
        }
    }

    private void RemoveBestiaryStatsData(BattleEntity entity)
    {
        CharacterStatsData characterData = GetBestiaryStatsData(entity.characterName);

        if (characterData != null)
        {
            bestiaryStatsDataList.Remove(characterData);
            bestiaryDataLookup.Remove(entity.characterName);
        }
    }

    public CharacterStatsData GetBestiaryStatsData(string entityName)
    {
        bestiaryDataLookup.TryGetValue(entityName, out CharacterStatsData characterData);

        return characterData;
    }

#endregion

}
