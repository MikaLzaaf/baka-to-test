﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EnemyTrigger : MonoBehaviour {

    public CollisionMessageRelay detectionTrigger;
    public ChallengeInfo challengeInfo;
    //public List<BattlePartyMember> enemiesToBattle;

    public bool canTriggerBattle = true;
    //public bool isGameOverBattle = false;
    //public IlmuCost[] battleCost = default;

    private bool isTriggered = false;


    private void Start()
    {
        if (detectionTrigger != null)
        {
            detectionTrigger.TriggerEnterEvent -= TriggerEnter;
            detectionTrigger.TriggerEnterEvent += TriggerEnter;
        }
    }


    private void OnDisable()
    {
        isTriggered = false;
    }


    private void TriggerEnter(CollisionMessageRelay messageRelay, Collider collider)
    {
        if(!canTriggerBattle)
        {
            return;
        }

        Entity otherEntity = collider.GetComponent<Entity>();

        if(otherEntity != null)
        {
            if(otherEntity.entityTag == EntityTag.Player && !isTriggered)
            {
                TriggerBattle();
            }
        }
    }


    public void TriggerBattle(bool deactivateAfterBattle = true)
    {
        isTriggered = true;

        BattleController.instance.TriggerBattle(challengeInfo);

        this.gameObject.SetActive(!deactivateAfterBattle);
    }


    public void Activate(bool active)
    {
        detectionTrigger.gameObject.SetActive(active);
    }

}
