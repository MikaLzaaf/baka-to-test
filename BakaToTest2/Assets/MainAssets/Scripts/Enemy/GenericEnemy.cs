﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Intelligensia.Battle;

public class GenericEnemy : BattleEntity
{
    [Header("Enemy Behaviours")]
    public EnemyStatusPanel statusPanel;
    public List<SimplerItem> consumables;
    public List<SimplerItem> itemDrop;
    public IlmuCost[] ilmuDrop;

    //public int expDrop = 10;

    public Item[] GetConsumables()
    {
        List<Item> items = new List<Item>();

        for(int i = 0; i < consumables.Count; i++)
        {
            items.Add(consumables[i].GetItem());
        }

        return items.ToArray();
    }

    public void Consume(string itemId, int quantity)
    {
        for (int i = 0; i < consumables.Count; i++)
        {
            if(consumables[i].itemBase.ItemID == itemId)
            {
                consumables[i].amount -= quantity;

                if (consumables[i].amount <= 0)
                    consumables.RemoveAt(i);

                break;
            }
        }
    }


    #region Battle related

    public override void ToggleBattleMode(bool active)
    {
        if (active)
        {
            InitializeStats();

            base.ToggleBattleMode(active);

            statusPanel.Initialize(this);

            //battleTrigger.Activate(false);

            int idleIndex = Random.Range(1, 4);

            animatorController.Idle(idleIndex);

            //confidenceGrade = ConfidenceInfoManager.DetermineConfidenceGradeWithoutInfo(this, 
            //    BattleInfoManager.currentActiveSubject);
        }
        else
        {
            base.ToggleBattleMode(active);

            statusPanel.gameObject.SetActive(false);

            if(!isDestroyed)
            {
                //battleTrigger.Activate(true);
            }

            animatorController.Idle(0);
        }

        characterController.SwitchToRunAnimation(active);
    }

    public void ToggleStatusPanel(bool active)
    {
        if (statusPanel != null)
        {
            if (active)
            {
                statusPanel.Show();
            }
            else
            {
                statusPanel.Close();
                RecycleSticky();
            }
        }
    }

    protected override void PrepareSkillsForBattle()
    {
        // For Non-Player
        // - Use CharacterStatsBase to get the skills for duplicate
        inBattleOffenses = new List<InBattleSkill>();
        inBattleSupports = new List<InBattleSkill>();

        for (int i = 0; i < CharacterStatsBase.defaultSkillItems.Count; i++)
        {
            BaseSkill baseSkill = CharacterStatsBase.defaultSkillItems[i].baseSkill;

            if (baseSkill == null)
                continue;

            InBattleSkill skill = new InBattleSkill(baseSkill, this, CharacterStatsBase.defaultSkillItems[i].IsEnhanced);

            if(baseSkill is OffensiveSkillSO)
                inBattleOffenses.Add(skill);
            else if (baseSkill is SupportSkillSO)
                inBattleSupports.Add(skill);
        }
    }


    #endregion
}
