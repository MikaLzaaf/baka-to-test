﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Timeline;
using UnityEngine.Playables;

public class EntityControlTrack : TrackAsset
{ 
    /// <summary>
    /// Create timeline track
    /// </summary>
    public override Playable CreateTrackMixer(PlayableGraph graph, GameObject go, int inputCount)
    {
        return ScriptPlayable<EntityControlTrackMixer>.Create(graph, inputCount);
    }
}
