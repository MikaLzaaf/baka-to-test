﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class EntityControl : PlayableAsset
{
    public override Playable CreatePlayable(PlayableGraph graph, GameObject owner)
    {
        var playable = ScriptPlayable<EntityControlBehaviour>.Create(graph);

        EntityControlBehaviour objectControlBehaviour = playable.GetBehaviour();
    
        return playable;
    }
}
