﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class EntityControlTrackMixer : PlayableBehaviour
{
    private Transform startTransform;

    public override void ProcessFrame(Playable playable, FrameData info, object playerData)
    {
        Transform obj = playerData as Transform;

        startTransform = obj;

        int inputCount = playable.GetInputCount();
        for (int i = 0; i < inputCount; i++)
        {
            float inputWeight = playable.GetInputWeight(i);
            if (inputWeight > 0)
            {
                ScriptPlayable<EntityControlBehaviour> inputPlayable = (ScriptPlayable<EntityControlBehaviour>)playable.GetInput(i);

                EntityControlBehaviour objectControl = inputPlayable.GetBehaviour();

            }
        }
    }
}
