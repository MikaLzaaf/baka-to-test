﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Timeline;

public static class TimelineDataManager
{
    private const string BattleTimelineDirectoryInResources = "Battle/";


    public static TimelineAsset LoadTimelineData(string name)
    {
        var data = Resources.Load<TimelineAsset>(BattleTimelineDirectoryInResources + name);

        return data;
    }
}
