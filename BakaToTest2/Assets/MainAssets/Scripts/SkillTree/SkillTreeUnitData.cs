﻿using UnityEngine;
using Intelligensia.UI.PlayerMenu;

[System.Serializable]
public class SkillTreeUnitData 
{

    //public int id_Skill;
    public string[] skill_Dependencies;
    public string id = "";
    public string name = "";
    public string description = "";
    public AcademicSkillCategory skillCategory;

    public IlmuCost[] ilmuCosts;

    [SerializeField] private SkillTreeUnitBaseValueSO baseSkillTreeUnit = default;
    public SkillTreeUnitBaseValueSO BaseSkillTreeUnit
    {
        get
        {
            if(baseSkillTreeUnit == null)
            {
                baseSkillTreeUnit = BaseValueLoader.LoadSkillBaseValue(id);
            }

            return baseSkillTreeUnit;
        }
    }

    //public SkillItemBaseValue baseSkillItem;
    public SkillTreeUnitStatus status;

    //public bool isMultiplePurchases = false;
    public int maxPurchaseAmount = 1;

    [SerializeField] private int amountPurchased = 0;

    private const float CostBaseMultiplierValue = 1.34f;


    public bool IsCategory(AcademicSkillCategory category)
    {
        return skillCategory == category;
    }

    public bool IsStatsCategory()
    {
        return skillCategory == AcademicSkillCategory.Stats;
    }

    public bool IsUnlocked()
    {
        return status == SkillTreeUnitStatus.Unlocked;
    }

    public bool IsBought()
    {
        return status == SkillTreeUnitStatus.Bought;
    }

    public bool CanBuy()
    {
        if (IsStatsCategory() && amountPurchased < maxPurchaseAmount)
            return true;

        if (!IsStatsCategory() && IsUnlocked())
            return true;

        return false;
    }


    public void Buy()
    {
        if(IsStatsCategory())
        {
            if (amountPurchased >= maxPurchaseAmount)
                return;
        }

        status = SkillTreeUnitStatus.Bought;

        amountPurchased += 1;
    }

    public void Unlock()
    {
        status = SkillTreeUnitStatus.Unlocked;
    }

    public int GetCostBySubject(Subjects subject)
    {
        int subjectCost = 0;

        for(int i = 0; i < ilmuCosts.Length; i++)
        {
            if (ilmuCosts[i].subject == subject)
                subjectCost = ilmuCosts[i].cost;
        }

        int cost = subjectCost;

        if (IsStatsCategory())
        {
            // Formula for cost multiplication => cost = subjectCost + ((CostBaseMultiplierValue^amountPurchased) - 1)
            if (subjectCost > 0)
                cost = Mathf.RoundToInt(subjectCost + (Mathf.Pow(CostBaseMultiplierValue, amountPurchased) - 1));
        }

        return cost;
    }

    public bool IsType<T>() where T : SkillTreeUnitBaseValueSO
    {
        return BaseSkillTreeUnit is T;
    }

    public T GetBase<T>() where T : SkillTreeUnitBaseValueSO
    {
        return (T)BaseSkillTreeUnit;
    }

#if UNITY_EDITOR
    public void ResetNameAndID()
    {
        if(skillCategory == AcademicSkillCategory.Stats)
        {
            if(IsType<BaseStatsUpgradeSO>())
            {
                var a = GetBase<BaseStatsUpgradeSO>();

                id = a.name;
                name = "Boosts " + a.statsToUpgrade.StatsName;
            }
            else if (IsType<SubjectStatsUpgradeSO>())
            {
                var b = GetBase<SubjectStatsUpgradeSO>();

                id = b.name;
                name = "Boosts " + StringHelper.SplitByCapitalizeFirstLetter(b.subjectStatsToUpgrade.Subject.ToString());
            }
        }
        else if (skillCategory == AcademicSkillCategory.AttackArgument || skillCategory == AcademicSkillCategory.SupportArgument)
        {
            var c = GetBase<BaseSkill>();

            id = c.id;
            name = c.skillName;
        }
    }
#endif
}


public enum SkillTreeUnitStatus
{
    Locked,
    Unlocked,
    Bought
}
