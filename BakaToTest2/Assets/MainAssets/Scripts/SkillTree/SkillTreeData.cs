﻿using System.Collections.Generic;
using Intelligensia.UI.PlayerMenu;

[System.Serializable]
public class SkillTreeData
{
    //public Subjects subject;
    public AcademicSkillCategory skillCategory;

    public SkillTreeUnitData[] skillTreeUnitDatas;

    private Dictionary<string, SkillTreeUnitData> _skillLookup;
    private Dictionary<string, SkillTreeUnitData> skillLookup
    {
        get
        {
            if (_skillLookup == null)
            {
                _skillLookup = new Dictionary<string, SkillTreeUnitData>();

                foreach (SkillTreeUnitData skill in skillTreeUnitDatas)
                {
                    _skillLookup.Add(skill.id, skill);
                }
            }

            return _skillLookup;
        }

        set
        {
            _skillLookup = value;
        }
    }

    public SkillTreeData() { }

    //public SkillTree(SkillTree_Leaf[] skillTree_Leaves, Subjects subject)
    //{
    //    this.subject = subject;
    //    skillLeaves = skillTree_Leaves;
    //}

    public SkillTreeData(SkillTreeUnitData[] skillTreeUnitDatas, AcademicSkillCategory skillCategory)
    {
        this.skillCategory = skillCategory;
        this.skillTreeUnitDatas = skillTreeUnitDatas;
    }


    private SkillTreeUnitData GetSkillTreeUnitData(string skillId)
    {
        skillLookup.TryGetValue(skillId, out SkillTreeUnitData skillTreeUnitData);

        return skillTreeUnitData;
    }

    public void BuySkillTreeUnitData(string skillId)
    {
        SkillTreeUnitData skillTreeUnitData = GetSkillTreeUnitData(skillId);
        
        if (skillTreeUnitData == null)
            return;

        if (skillTreeUnitData.CanBuy())
        {
            skillTreeUnitData.Buy();

            // Unlock other skill according to their dependencies
            RefreshAllSkillStatus();
        }
    }


    private void RefreshAllSkillStatus()
    {
        for (int i = 0; i < skillTreeUnitDatas.Length; i++)
        {
            if (skillTreeUnitDatas[i].status == SkillTreeUnitStatus.Bought ||
                skillTreeUnitDatas[i].status == SkillTreeUnitStatus.Unlocked)
                continue;

            if (CanSkillBeUnlocked(skillTreeUnitDatas[i]))
            {
                skillTreeUnitDatas[i].Unlock();
            }
        }
    }


    public bool IsSkillUnlocked(string skillId)
    {
        SkillTreeUnitData skillTreeUnitData = GetSkillTreeUnitData(skillId);

        return skillTreeUnitData.status == SkillTreeUnitStatus.Unlocked;
    }

    public bool CanSkillBeUnlocked(SkillTreeUnitData skillTreeUnitData)
    {

        bool canUnlock = true;

        string[] dependencies = skillTreeUnitData.skill_Dependencies;
        for (int i = 0; i < dependencies.Length; ++i)
        {
            if (skillLookup.TryGetValue(dependencies[i], out SkillTreeUnitData skillInspected))
            {
                if (skillInspected.status != SkillTreeUnitStatus.Bought)
                {
                    canUnlock = false;
                    break;
                }
            }
            else // If one of the dependencies doesn't exist, the skill can't be unlocked.
            {
                return false;
            }
        }

        return canUnlock;
    }
}
