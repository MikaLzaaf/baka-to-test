﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Intelligensia.UI.PlayerMenu;

[CreateAssetMenu(fileName = "New Skill Tree", menuName = "ScriptableObject/Skill Tree")]
public class SkillTreeBaseValueSO : ScriptableObject
{
    /// <summary>
    /// This Skill Tree is only used as the base value for the character's skill Tree. 
    /// </summary>
    /// 

    public AcademicSkillCategory skillCategory;

    public SkillTreeUnitData[] skillTreeUnitDatas;

    //[HideInInspector] public string skillTreeNodesInJson = "";
    //[HideInInspector] public string skillTreeInJson = "";
}
