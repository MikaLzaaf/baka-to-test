﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectsTag : ScriptableObject
{
    [SerializeField] private string effectName = default;
    [SerializeField] private float effectValue = default;
    [SerializeField] private bool isMultiplicativeEffect = false;

    public string EffectName => effectName;
    public float EffectValue => effectValue;
    public bool IsMultiplicativeEffect => isMultiplicativeEffect;

}
