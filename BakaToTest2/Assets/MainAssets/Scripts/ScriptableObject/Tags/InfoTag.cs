﻿using UnityEngine;
using System.Collections.Generic;

//[CreateAssetMenu(fileName = "New Info Tag", menuName = "ScriptableObject/Info Tag")]
public class InfoTag : ScriptableObject
{
    // The tags are based on this for now:
    // Male or Female (Gender)
    // Type of weapons (Weapon)
    // Subject of skill (Subjects)
    // Effect on battle field e.g Rainy, Noisy, Silent (Field Effect)
    // Days like Monday, Tuesday etc. (Day)

    //Undefined,
    //Male,
    //Female,
    //Blunt,
    //Gun,
    //Knife,
    //Book,
    //BahasaMelayu,
    //English,
    //Mathematics,
    //History,
    //PE,
    //Physics,
    //Biology,
    //Chemistry,
    //Rainy,
    //Noisy,
    //Silent, 
    //Monday,
    //Tuesday,
    //Wednesday,
    //Thursday,
    //Friday,
    //Saturday,
    //Sunday
}
