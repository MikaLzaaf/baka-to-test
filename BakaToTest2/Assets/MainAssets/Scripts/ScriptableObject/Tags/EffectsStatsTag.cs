﻿using UnityEngine;

[CreateAssetMenu(fileName = "New Stats Tag", menuName = "ScriptableObject/Info Tag/Effects Tag/Stats Tag")]
public class EffectsStatsTag : EffectsTag
{
    public enum EffectsStatsEnum
    {
        Undefined,
        HigherAttack,
        LowerAttack,
        HigherDefense,
        LowerDefense,
        HigherStatusResistance,
        LowerStatusResistance,
        BreakLivesOnKnockout,
        KnockoutChanceUp,
        KnockoutChanceBigUp,
        KnockoutChanceDown,
        KnockoutChanceBigDown
    }

    
    [SerializeField] private EffectsStatsEnum statsEnum = default;

    public EffectsStatsEnum StatsEnum => statsEnum;


}


