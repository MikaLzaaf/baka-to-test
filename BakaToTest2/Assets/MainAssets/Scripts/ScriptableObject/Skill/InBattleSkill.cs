﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Intelligensia.Battle
{
    [System.Serializable]
    public class InBattleSkill
    {
        /// <summary>
        /// This class is only used during Battle. Since SO is not suitable for storing dynamic data
        /// due to it's unique nature, a separate class has to be created.
        /// </summary>

        private BaseSkill battleSkillSO;
        private BattleEntity owner;

        private int elapsedCooldownDuration = 0;
        public int ElapsedCooldownDuration => elapsedCooldownDuration;

        public bool isOnCooldown => elapsedCooldownDuration > 0;
        public bool isOffensiveSkill { get; private set; }

        public bool canDealKO { get; private set; }

        public bool isEnhancedArgument { get; private set; }

        public InBattleSkill() { }

        public InBattleSkill(BaseSkill battleSkillSO, BattleEntity owner, bool isEnhanced)
        {
            this.battleSkillSO = battleSkillSO;
            this.owner = owner;
            isEnhancedArgument = isEnhanced;

            if(owner != null)
            {
                if(owner.oneBattleRoundEventChannel != null)
                {
                    owner.oneBattleRoundEventChannel.OnFullRoundEvent += UpdateCooldown;
                }
            }

            isOffensiveSkill = battleSkillSO is OffensiveSkillSO;

            if(isOffensiveSkill)
            {
                OffensiveSkillSO offensiveSkill = (OffensiveSkillSO)battleSkillSO;

                canDealKO = offensiveSkill.canDealKO;
            }
        }

        #region Properties
        public string GetSkillId()
        {
            return battleSkillSO != null ? battleSkillSO.id : "None";
        }

        public string GetSkillName()
        {
            return battleSkillSO != null ? battleSkillSO.skillName : "None";
        }

        public string GetSkillDescription()
        {
            return battleSkillSO != null ? battleSkillSO.skillDescription : "None";
        }

        public AimedTo GetSkillAim()
        {
            return battleSkillSO != null ? battleSkillSO.aimedTo : AimedTo.SingleEnemy;
        }

        public int GetSkillPoint()
        {
            return battleSkillSO != null ? battleSkillSO.skillPoint : 0;
        }

        public Subjects GetSubjectBased()
        {
            return battleSkillSO != null ? battleSkillSO.subjectBased : Subjects.BahasaMelayu;
        }

        //public Sprite GetIcon()
        //{
        //    return battleSkillSO != null ? battleSkillSO.icon : null;
        //}

        public string GetListIcon()
        {
            return battleSkillSO != null ? battleSkillSO.ListIcon : "None";
        }

        //public int GetSkillLevel()
        //{
        //    return battleSkillSO != null ? battleSkillSO.skillLevel : 0;
        //}

        #endregion

        //public void SubscribeToChannels()
        //{
        //    if (battleSkillSO == null)
        //        return;

        //    battleSkillSO.SubscribeToChannels();

        //    battleSkillSO.SkillAppliedEvent += OnSkillApplied;
        //}

        //public void UnsubscribeToChannels()
        //{
        //    if (battleSkillSO == null)
        //        return;

        //    battleSkillSO.UnsubscribeToChannels();

        //    battleSkillSO.SkillAppliedEvent -= OnSkillApplied;
        //}

        public void ApplySkill(BattleEntity target)
        {
            battleSkillSO.Use(target, owner);
        }

        /// <summary>
        /// Applies on each argument in the debate whether you lose or win.
        /// </summary>
        public void ApplyCooldown()
        {
            if (elapsedCooldownDuration == 0)
            {
                ActivateCooldown();
            }
            else
            {
                UpdateCooldownDuration();
            }
        }

        public bool IsRecoveryType(RecoveryType recoveryType)
        {
            if (isOffensiveSkill)
                return false;

            SupportSkillSO supportSkillSO = (SupportSkillSO)battleSkillSO;

            if (supportSkillSO == null) return false;

            return supportSkillSO.CurrentSkillLevelIsRecoveryType(recoveryType);
        }

        public bool CanCureStatusEffect(string statusEffectName)
        {
            if (isOffensiveSkill)
                return false;

            SupportSkillSO supportSkillSO = (SupportSkillSO)battleSkillSO;

            if (supportSkillSO == null) return false;

            return supportSkillSO.CanCureStatusEffect(statusEffectName);
        }

        public string GetRoleInDebate()
        {
            return battleSkillSO.roleInDebate.ToString();
        }

        public string GetDebateEffectDescription()
        {
            return battleSkillSO.GetDebateEffectDescription();
        }

        public DebateEffectType GetDebateEffect()
        {
            return battleSkillSO.debateEffectType;
        }

        public int GetDebateEffectValue()
        {
            return battleSkillSO.debateEffectValue;
        }

        #region Cooldown

        public void ActivateCooldown()
        {
            elapsedCooldownDuration = battleSkillSO.cooldownDuration;
        }

        public void ResetCooldown()
        {
            elapsedCooldownDuration = 0;
        }

        public void UpdateCooldown(BattleEntity fullRoundEntity)
        {
            if (fullRoundEntity == owner)
                UpdateCooldownDuration();
        }

        public void UpdateCooldownDuration()
        {
            elapsedCooldownDuration -= 1;

            if (elapsedCooldownDuration < 0)
                elapsedCooldownDuration = 0;
        }

        public int GetCooldownDuration()
        {
            return battleSkillSO.cooldownDuration;
        }

        #endregion
    }
}

