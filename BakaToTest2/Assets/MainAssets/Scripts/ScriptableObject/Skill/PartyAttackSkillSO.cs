﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Intelligensia.Battle
{
    [CreateAssetMenu(fileName = "New PartyAttack", menuName = "ScriptableObject/Skill/PartyAttack")]
    public class PartyAttackSkillSO : ScriptableObject
    {
        public SkillEffects_Offensive offensiveEffects;
        public SkillEffects_Recovery recoveryEffects;

        public void ApplySkill(BattleEntity[] activePlayers, BattleEntity[] activeEnemies)
        {
            // The effects only applied to activePlayers and activeEnemies
            ApplyDamage(activePlayers, activeEnemies);

            //ApplyRecovery(activePlayers);

            BattleInfoManager.currentStressPoint -= 100;
        }


        private void ApplyDamage(BattleEntity[] activePlayers, BattleEntity[] activeEnemies)
        {
            for (int i = 0; i < activeEnemies.Length; i++)
            {
                float damageToApply = DamageCalculator.CalculatePartyAttackDamage(offensiveEffects.damageValue, activePlayers, activeEnemies[i]);

                activeEnemies[i].ApplyDamage(damageToApply, DamageHitCategory.NormalHit, BattleInfoManager.currentActiveSubject);
            }
        }

        //private void ApplyRecovery(BattleEntity[] activePlayers)
        //{
        //    SkillEffects_Recovery currentEffect = recoveryEffects;

        //    for(int j = 0; j < activePlayers.Length; j++)
        //    {
        //        for (int i = 0; i < currentEffect.recoveryEffects.Count; i++)
        //        {
        //            SupportEffect recoveryEffect = currentEffect.recoveryEffects[i];

        //            if (recoveryEffect.recoveryType == RecoveryType.CureFatigue)
        //            {
        //                CureFatigue(activePlayers[j], recoveryEffect);
        //            }
        //            else if (recoveryEffect.recoveryType == RecoveryType.CureStatusEffect)
        //            {
        //                CureStatusEffect(activePlayers[j], recoveryEffect);
        //            }
        //            else if (recoveryEffect.recoveryType == RecoveryType.AddStatusBuff)
        //            {
        //                AddStatusBuff(activePlayers[j], recoveryEffect);
        //            }
        //            //else if (recoveryEffect.recoveryType == RecoveryType.CureStatusDebuff)
        //            //{
        //            //    CureStatusDebuff(activePlayers[j], recoveryEffect);
        //            //}
        //        }
        //    }
            
        //}

        //private void CureFatigue(BattleEntity target, SupportEffect recoveryEffect)
        //{
        //    float effectiveAmount = recoveryEffect.GetEffectiveValue(target.stamina);

        //    target.AddFatigue(-effectiveAmount);
        //}

        //private void CureStatusEffect(BattleEntity target, SupportEffect recoveryEffect)
        //{
        //    if (target == null)
        //        return;

        //    if (target.statusEffectController == null)
        //        return;

        //    for (int i = 0; i < recoveryEffect.curableStatusEffects.Length; i++)
        //    {
        //        //string effectName = recoveryEffect.curableStatusEffects[i].ToString();
        //        target.statusEffectController.Remove(recoveryEffect.curableStatusEffects[i]);
        //    }
        //}

        //private void AddStatusBuff(BattleEntity target, SupportEffect recoveryEffect)
        //{
        //    if (target == null)
        //        return;

        //    if (target.statusEffectController == null)
        //        return;

        //    //for (int i = 0; i < recoveryEffect.curableStatusEffects.Length; i++)
        //    //{
        //    //    string effectName = recoveryEffect.curableStatusEffects[i].ToString();

        //    //    if (target.statusEffectController.HasEffectWithName(effectName))
        //    //    {
        //    //        // TODO : Remove Status Effect here
        //    //    }
        //    //}
        //}

        //private void CureStatusDebuff(BattleEntity target, SupportEffect recoveryEffect)
        //{
        //    if (target == null)
        //        return;

        //    if (target.statusEffectController == null)
        //        return;

        //    for (int i = 0; i < recoveryEffect.curableStatusEffects.Length; i++)
        //    {
        //        string effectName = recoveryEffect.curableStatusEffects[i].ToString();

        //        if (target.statusEffectController.HasEffectWithName(effectName))
        //        {
        //            // TODO : Remove Status Debuff here
        //        }
        //    }
        //}
    }
}

