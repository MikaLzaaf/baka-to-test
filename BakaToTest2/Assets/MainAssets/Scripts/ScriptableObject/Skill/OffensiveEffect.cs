﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class OffensiveEffect 
{
    public int effectiveValue;
    public AmountType effectiveValueType = AmountType.Fixed;

    // TODO : Add status effect / buffs later

    public float GetEffectiveValue(float intelligenceValue)
    {
        float effValue = intelligenceValue;

        if(effectiveValueType == AmountType.Fixed)
        {
            effValue += effectiveValue;
        }
        else
        {
            effValue += ((float)effectiveValue / 100f * intelligenceValue);
        }

        return effValue;
    }
}
