﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Intelligensia.Battle.StatusEffects;


[Serializable]
public class SkillEffects
{
    //public int level;
}

[Serializable]
public class SkillEffects_Recovery : SkillEffects
{
    public List<SupportEffect> recoveryEffects;

    public string GetDescription()
    {
        string description = "";

        for (int i = 0; i < recoveryEffects.Count; i++)
        {
            string effectDescription = recoveryEffects[i].GetDescription();

            if (i != recoveryEffects.Count - 1)
            {
                if (!string.IsNullOrEmpty(effectDescription))
                {
                    effectDescription += "/";
                }
            }

            description += effectDescription;
        }


        return description;
    }
}


[Serializable]
public class SkillEffects_Offensive : SkillEffects
{
    public int damageValue;
    public AmountType effectiveValueType = AmountType.Fixed;

    //public List<OffensiveEffect> offensiveEffects;
    // TODO : Add status effect / buffs later
    public List<StatusAilment> statusEffectsToInflict;

    [Range(0, 100)]
    public int statusEffectHitProbability; // All effects use the same probability

    private const string StatusEffectProbabilityFormat = " ({0}%)";

    public string GetDescription()
    {
        string effectDescription = "";

        for (int j = 0; j < statusEffectsToInflict.Count; j++)
        {
            string desc = statusEffectsToInflict[j].GetAilmentName() + string.Format(StatusEffectProbabilityFormat, statusEffectHitProbability);

            effectDescription += desc;

            if (j != statusEffectsToInflict.Count - 1)
            {
                effectDescription += "/";
            }
        }

        return effectDescription;
    }
}

