﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public abstract class BaseSkill : SkillTreeUnitBaseValueSO
{
    // What to do here?
    // - Store Skill properties and info (effective value, aimed to who, subject based, target amount)
    // - Initialize skill targets
    // - Listen to SkillUsage channel and use skill

    public string id;
    public string skillName;

    [SerializeField] private UIListItemIcons listIcon = default;
    public int skillPoint = 1;
    public string skillDescription = "A new skill.";

    public Subjects subjectBased;
    public AimedTo aimedTo;
    public int cooldownDuration = 3;

    [Header("Debate")]
    public RoleInDebate roleInDebate;
    public DebateEffectType debateEffectType;
    public int debateEffectValue;

    //[Header("Event Channels")]
    //public UseSkillInBattleEventChannelSO useSkillInBattleEventChannel;

    //public event Action SkillAppliedEvent;

    private const string AimedToSelfLocalizationID = "AimedTo_Self";
    private const string AimedToOneLocalizationID = "AimedTo_One";
    private const string AimedToAllLocalizationID = "AimedTo_All";

    protected const string DescriptionFormat = "[<color=#FED330>{0}</color=#FED330>]\n";
    protected const string APSuffix = " AP";

    public string ListIcon => listIcon.ToString();


    protected abstract string GetSkillDescription();

    protected string GetLocalizedDescription()
    {
        string localizedDescription = LocalizationManager.Localize(id);

        if (localizedDescription == id)
            localizedDescription = skillDescription;

        return localizedDescription;
    }

    protected string GetAimInDescription()
    {
        if (aimedTo == AimedTo.Self)
        {
            return LocalizationManager.Localize(AimedToSelfLocalizationID);
        }
        else if (aimedTo == AimedTo.SingleEnemy || aimedTo == AimedTo.SinglePartyMember)
        {
            return LocalizationManager.Localize(AimedToOneLocalizationID);
        }
        else if (aimedTo == AimedTo.AllEnemies || aimedTo == AimedTo.AllPartyMembers || aimedTo == AimedTo.All)
        {
            return LocalizationManager.Localize(AimedToAllLocalizationID);
        }

        return "";
    }

    public string GetFullDescription(bool withLocalization = true)
    {
        string fullDescription = string.Format(DescriptionFormat, GetSkillDescription());

        if (withLocalization)
            fullDescription += GetLocalizedDescription();

        return fullDescription;
    }

    public string GetDebateEffectDescription()
    {
        string descriptionFormat = LocalizationManager.Localize(debateEffectType.ToString());

        return string.Format(descriptionFormat, debateEffectValue);
    }

    public string GetPathToSkillItemBaseValue()
    {
        string[] split = id.Split('_');

        return split[1];
    }
   
    //public virtual void SubscribeToChannels()
    //{
    //    if(useSkillInBattleEventChannel != null)
    //    {
    //        useSkillInBattleEventChannel.OnSkillUsage += UseSkill;
    //    }
    //}

    //public virtual void UnsubscribeToChannels()
    //{
    //    if (useSkillInBattleEventChannel != null)
    //    {
    //        useSkillInBattleEventChannel.OnSkillUsage -= UseSkill;
    //    }
    //}


    private void UseSkill(string skillId, BattleEntity[] targets, BattleEntity user)
    {
        if(skillId == id)
        {
            Use(targets, user);
        }
    }


    public void Use(BattleEntity battleEntity, BattleEntity user)
    {
        ApplySkill(battleEntity, user);
    }

    public void Use(BattleEntity[] battleEntities, BattleEntity user)
    {
        for(int i = 0; i < battleEntities.Length; i++)
        {
            Use(battleEntities[i], user);
        }
    }


    protected abstract void ApplySkill(BattleEntity battleEntity, BattleEntity user);

    //protected void OnSkillApplied()
    //{
    //    //SkillAppliedEvent?.Invoke();
    //}
}
