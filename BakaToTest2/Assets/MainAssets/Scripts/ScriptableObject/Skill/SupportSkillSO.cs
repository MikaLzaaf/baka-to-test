﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Intelligensia.Battle;
using Intelligensia.Battle.StatusEffects;


[CreateAssetMenu(fileName = "New Support Skill", menuName = "ScriptableObject/Skill/Support")]
public class SupportSkillSO : BaseSkill
{
    public SkillEffects_Recovery supportEffect;

    private const string SupportArgumentPrefix = "Support Argument";

    protected override string GetSkillDescription()
    {
        string desc = SupportArgumentPrefix + " - ";

        string apValue = skillPoint.ToString() + APSuffix + " : ";

        string targetAmount = GetAimInDescription() + " - ";

        string offenseDescription = "";

        if(supportEffect != null)
            offenseDescription += supportEffect.GetDescription();

        desc += apValue + targetAmount + offenseDescription;

        return desc;
    }

    public bool CurrentSkillLevelIsRecoveryType(RecoveryType recoveryType)
    {
        if (supportEffect == null)
            return false;

        for(int i = 0; i < supportEffect.recoveryEffects.Count; i++)
        {
            if (supportEffect.recoveryEffects[i].recoveryType == recoveryType)
                return true;
        }

        return false;
    }

    public bool CanCureStatusEffect(string statusEffectName)
    {
        if (supportEffect == null)
            return false;

        for (int i = 0; i < supportEffect.recoveryEffects.Count; i++)
        {
            if (supportEffect.recoveryEffects[i].CanCureStatusEffect(statusEffectName))
                return true;
        }

        return false;
    }

    protected override void ApplySkill(BattleEntity target, BattleEntity user)
    {
        if (supportEffect == null)
            return;
        // This skill type will only buffs and heals 

        for (int i = 0; i < supportEffect.recoveryEffects.Count; i++)
        {
            SupportEffect recoveryEffect = supportEffect.recoveryEffects[i];

            if (recoveryEffect.recoveryType == RecoveryType.CureFatigue)
            {
                CureFatigue(target, recoveryEffect);
            }
            else if (recoveryEffect.recoveryType == RecoveryType.CureStatusEffect)
            {
                CureStatusEffect(target, recoveryEffect);
            }
            else if (recoveryEffect.recoveryType == RecoveryType.AddStatusBuff)
            {
                AddStatusBuff(target, recoveryEffect);
            }
        }

        //OnSkillApplied();
    }


    private void CureFatigue(BattleEntity target, SupportEffect recoveryEffect)
    {
        float effectiveAmount = recoveryEffect.GetEffectiveValue(target.stamina);

        target.AddFatigue(-effectiveAmount, DamageHitCategory.Recovery);
    }

    private void CureStatusEffect(BattleEntity target, SupportEffect recoveryEffect)
    {
        if (target == null)
            return;

        if (target.statusEffectController == null)
            return;

        for(int i = 0; i < recoveryEffect.curableStatusEffects.Length; i++)
        {
            StatusAilment statusAilment = recoveryEffect.curableStatusEffects[i];

            target.statusEffectController.RemoveAilment(statusAilment);
        }
    }

    private void AddStatusBuff(BattleEntity target, SupportEffect recoveryEffect)
    {
        if (target == null)
            return;

        if (target.statusEffectController == null)
            return;

        for (int i = 0; i < recoveryEffect.statusBoosts.Length; i++)
        {
            StatusBoostType statusBoostType = recoveryEffect.statusBoosts[i];

            target.statusEffectController.AddStatusBoost(statusBoostType);
        }
    }
}

