﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Intelligensia.Battle;

[CreateAssetMenu(fileName = "New Offensive Skill", menuName = "ScriptableObject/Skill/Offensive")]
public class OffensiveSkillSO : BaseSkill
{
    public SkillEffects_Offensive offensiveEffect;

    public bool canDealKO = true;

    private const string AttackArgumentPrefix = "Attack Argument";


    protected override string GetSkillDescription()
    {
        string desc = AttackArgumentPrefix + " - ";

        string apValue = skillPoint.ToString() + APSuffix + " : ";

        string targetAmount = GetAimInDescription() + " - ";

        string offenseDescription = "";

        if(offensiveEffect != null)
            offenseDescription += offensiveEffect.GetDescription();


        desc += apValue + targetAmount + offenseDescription;

        return desc;
    }


    protected override void ApplySkill(BattleEntity target, BattleEntity user)
    {
        // Assuming this skill will only inflict damage
        // damage value will be based on skill effective value + intelligence

        // Total damage (not crit & not hit weakness) = (skill base damage + attacker's intelligence) * minigame multiplier
        // Total damage (crit & not hit weakness) = (skill base damage + attacker's intelligence) * crit dmg multiplier * minigame multiplier
        // Total damage (not crit && hit weakness) = (skill base damage + attacker's intelligence) * minigame multiplier * weakness multiplier
        // Total damage (crit && hit weakness) = (skill base damage + attacker's intelligence) * crit dmg multiplier * minigame multiplier * weakness multiplier

        if (offensiveEffect == null)
            return;

        //SkillEffects_Offensive currentEffect = GetCurrentEffect();

        float damageToApply = offensiveEffect == null ? 0f : offensiveEffect.damageValue;
        DamageHitCategory hitCategory = DamageHitCategory.Undefined;

        //ArgumentHelper currentSupportArgument = BattleInfoManager.currentArgumentHelper == null ? null : 
        //    BattleInfoManager.currentArgumentHelper.GetSupportArgument();

        bool isGuaranteedCrit = false;

        //if(currentSupportArgument != null)
        //{
        //    isGuaranteedCrit = currentSupportArgument.IsCombatSupportType(ArgumentHelper.CombatSupportType.CriticalHitGuarantee);
        //}

        damageToApply = DamageCalculator.CalculateDamage(damageToApply, user, target, 100, canDealKO, isGuaranteedCrit, out hitCategory);

        //damageToApply = GetDamageBasedOnMinigameResult(damageToApply);

        //if(target.IsHitByWeakness(this))
        //{
        //    damageToApply = target.GetDamageBasedOnWeaknessesDiscovered(damageToApply);
        //}

        //if(currentSupportArgument != null)
        //{
        //    if(currentSupportArgument.IsCombatSupportType(ArgumentHelper.CombatSupportType.MultiplySkillDamage))
        //    {
        //        damageToApply *= currentSupportArgument.GetCombatSupportValue(ArgumentHelper.CombatSupportType.MultiplySkillDamage);
        //    }
        //}    

        //Debug.Log(damageToApply + " = damageToApply");
        target.ApplyDamage(damageToApply, hitCategory, subjectBased);

        if (hitCategory == DamageHitCategory.Missed || hitCategory == DamageHitCategory.Undefined)
            return;

        target.statusEffectController.InflictStatusAilment(user, offensiveEffect);

        //if(currentSupportArgument != null)
        //{
        //    if(!currentSupportArgument.IsCombatSupportType(ArgumentHelper.CombatSupportType.RemoveOneSkillCooldown)
        //        && !currentSupportArgument.IsCombatSupportType(ArgumentHelper.CombatSupportType.RemoveAllSkillsCooldown))
        //    {
        //        OnSkillApplied();
        //    }
        //    else
        //    {
        //        currentSupportArgument.ApplyEffect(user);
        //    }
        //}
        //else
        //{
            //OnSkillApplied();
        //}    
    }
}
