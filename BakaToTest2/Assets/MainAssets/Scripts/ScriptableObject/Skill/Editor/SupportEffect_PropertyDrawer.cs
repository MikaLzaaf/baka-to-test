﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Intelligensia.Battle
{
    [CustomPropertyDrawer(typeof(SupportEffect))]
    public class SupportEffect_PropertyDrawer : PropertyDrawer
    {
        private const int HeightExtra = 2;

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            float h = base.GetPropertyHeight(property, label);
            int rows = 1;
            if (property.FindPropertyRelative("_expanded").boolValue)
            {
                rows += 1;
                RecoveryType recoveryType = (RecoveryType)property.FindPropertyRelative("recoveryType").enumValueIndex;
                if(recoveryType == RecoveryType.CureFatigue)
                {
                    rows += 2;
                }
                else if (recoveryType == RecoveryType.CureStatusEffect)
                {
                    rows += 1;

                    SerializedProperty p_curableStatusEffects = property.FindPropertyRelative("curableStatusEffects");

                    if (p_curableStatusEffects.isExpanded)
                    {
                        rows += p_curableStatusEffects.arraySize;

                        rows += 1; // Extra space
                    }
                }
                else if (recoveryType == RecoveryType.AddStatusBuff)
                {
                    rows += 1;

                    SerializedProperty p_statusBoosts = property.FindPropertyRelative("statusBoosts");

                    if(p_statusBoosts.isExpanded)
                    {
                        rows += p_statusBoosts.arraySize;

                        rows += 1; // Extra space
                    }
                }
            }
            
            return (h + HeightExtra) * rows;
        }


        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            // Draw label
            SerializedProperty p_expanded = property.FindPropertyRelative("_expanded");

            float h = 20f;
            float totalH = base.GetPropertyHeight(property, label);
            position.height = totalH;
            p_expanded.boolValue = EditorGUI.Foldout(position, p_expanded.boolValue, label);
            if (p_expanded.boolValue)
            {
                position.y += totalH;
                position.height = GetPropertyHeight(property, label) - totalH;
                EditorGUI.BeginProperty(position, label, property);

                ++EditorGUI.indentLevel;

                // Calculate rects
                float x = position.x,
                    y = position.y,
                    w = position.width;

                Rect r_recovery_type = new Rect(x, y, w, h);

                SerializedProperty p_recovery_type = property.FindPropertyRelative("recoveryType");
    
                EditorGUI.PropertyField(r_recovery_type, p_recovery_type, new GUIContent(p_recovery_type.displayName));
 
                ++EditorGUI.indentLevel;

                RecoveryType recoveryType = (RecoveryType)p_recovery_type.enumValueIndex;
                if (recoveryType == RecoveryType.CureFatigue)
                {
                    y += h;
                    Rect r_effectiveValue = new Rect(x, y, w, h);
                    y += h;
                    Rect r_effectiveValue_type = new Rect(x, y, w, h);

                    SerializedProperty p_effectiveValue = property.FindPropertyRelative("effectiveValue");
                    SerializedProperty p_effectiveValue_type = property.FindPropertyRelative("effectiveValueType");

                    EditorGUI.PropertyField(r_effectiveValue, p_effectiveValue, new GUIContent(p_effectiveValue.displayName));
                    EditorGUI.PropertyField(r_effectiveValue_type, p_effectiveValue_type, new GUIContent(p_effectiveValue_type.displayName));
                }
                else if (recoveryType == RecoveryType.CureStatusEffect)
                {
                    y += h;
                    Rect r_curableStatusEffects = new Rect(x, y, w, h);

                    SerializedProperty p_curableStatusEffects = property.FindPropertyRelative("curableStatusEffects");

                    EditorGUI.PropertyField(r_curableStatusEffects, p_curableStatusEffects, new GUIContent(p_curableStatusEffects.displayName));

                    if (p_curableStatusEffects.isExpanded)
                    {
                        y += h;
                        Rect r_intField = new Rect(x, y, w, h);

                        p_curableStatusEffects.arraySize = EditorGUI.DelayedIntField(r_intField, new GUIContent("Size"), p_curableStatusEffects.arraySize);

                        for (int i = 0; i < p_curableStatusEffects.arraySize; i++)
                        {
                            y += h;
                            Rect r_arraySize = new Rect(x, y, w, h);

                            EditorGUI.PropertyField(r_arraySize, p_curableStatusEffects.GetArrayElementAtIndex(i), new GUIContent(p_curableStatusEffects.GetArrayElementAtIndex(i).displayName));
                        }
                    }
                }
                else if (recoveryType == RecoveryType.AddStatusBuff)
                {
                    SerializedProperty p_statusBoosts = property.FindPropertyRelative("statusBoosts");

                    y += h;
                    Rect r_statusBoosts = new Rect(x, y, w, h);

                    EditorGUI.PropertyField(r_statusBoosts, p_statusBoosts, new GUIContent(p_statusBoosts.displayName));

                    if (p_statusBoosts.isExpanded)
                    {
                        y += h;
                        Rect r_intField = new Rect(x, y, w, h);

                        p_statusBoosts.arraySize = EditorGUI.DelayedIntField(r_intField, new GUIContent("Size"), p_statusBoosts.arraySize);

                        for (int i = 0; i < p_statusBoosts.arraySize; i++)
                        {
                            y += h;
                            Rect r_arraySize = new Rect(x, y, w, h);

                            EditorGUI.PropertyField(r_arraySize, p_statusBoosts.GetArrayElementAtIndex(i), new GUIContent(p_statusBoosts.GetArrayElementAtIndex(i).displayName));
                        }
                    }
                }


                --EditorGUI.indentLevel;

                --EditorGUI.indentLevel;

                EditorGUI.EndProperty();
            }
        }
    }
}

