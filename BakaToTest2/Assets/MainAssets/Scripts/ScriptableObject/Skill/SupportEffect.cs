﻿using UnityEngine;
using Intelligensia.Battle.StatusEffects;


[System.Serializable]
public class SupportEffect
{
    public RecoveryType recoveryType;

    //[Header("Cure Fatigue")]
    public int effectiveValue;
    public AmountType effectiveValueType;

    //[Header("Cure Status Effect")]
    public StatusAilment[] curableStatusEffects;

    //[Header("Add Status Boost")]
    public StatusBoostType[] statusBoosts;

#if UNITY_EDITOR
    public bool _expanded = false;
#endif

    private const string CureFatigueDescriptionFormatID = "CureFatigueDescriptionFormatID";
    private const string CureStatusEffectDescriptionPrefixID = "CureStatusEffectDescriptionPrefixID";
    private const string AddStatusBuffDescriptionPrefixID = "AddStatusBuffDescriptionPrefixID";


    public float GetEffectiveValue(float baseValue)
    {
        float effValue = 0f;

        if (effectiveValueType == AmountType.Fixed)
        {
            effValue = effectiveValue;
        }
        else
        {
            effValue += ((float)effectiveValue / 100f * baseValue);
        }

        return effValue;
    }

    public bool CanCureStatusEffect(string statusEffectName)
    {
        if (curableStatusEffects.Length == 0)
            return false;

        for(int i = 0; i < curableStatusEffects.Length; i++)
        {
            if(curableStatusEffects[i].GetType().ToString() == statusEffectName)
            {
                return true;
            }
        }

        return false;
    }

    public string GetEffectiveValueInDescription()
    {
        return effectiveValueType == AmountType.Fixed ? effectiveValue.ToString() : effectiveValue.ToString() + "%";
    }

    public string GetDescription()
    {
        //string description = "";

        string effectDescription = "";

        if (recoveryType == RecoveryType.CureFatigue)
        {
            string format = LocalizationManager.Localize(CureFatigueDescriptionFormatID);

            effectDescription += string.Format(format, GetEffectiveValueInDescription());
        }
        else if (recoveryType == RecoveryType.CureStatusEffect)
        {
            string prefix = LocalizationManager.Localize(CureStatusEffectDescriptionPrefixID);

            effectDescription += prefix + " ";

            for (int j = 0; j < curableStatusEffects.Length; j++)
            {
                effectDescription += curableStatusEffects[j].GetAilmentName();

                if (j != curableStatusEffects.Length - 1)
                {
                    effectDescription += "/";
                }
            }
        }
        else if (recoveryType == RecoveryType.AddStatusBuff)
        {
            string prefix = LocalizationManager.Localize(AddStatusBuffDescriptionPrefixID);

            effectDescription += prefix + " ";

            for (int j = 0; j < statusBoosts.Length; j++)
            {
                string buffName = StringHelper.SplitByCapitalizeFirstLetter(statusBoosts[j].ToString());

                effectDescription += buffName;

                if (j != statusBoosts.Length - 1)
                {
                    effectDescription += "/";
                }
            }
        }

        return effectDescription;
    }
}
