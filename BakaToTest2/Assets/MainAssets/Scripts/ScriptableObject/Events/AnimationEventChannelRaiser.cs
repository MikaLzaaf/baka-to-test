﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Intelligensia.Battle;

public class AnimationEventChannelRaiser : MonoBehaviour
{
    [Header("Event Channels")]
    public UseSkillInBattleEventChannelSO useSkillInBattleEventChannel;


    public void RaiseApplySkillEvent()
    {
        Debug.Log("Raised");

        if (BattleController.instance == null)
            return;

        if (!BattleController.IsInBattle)
            return;

        //if (useSkillInBattleEventChannel != null)
        //{
        //    BattleEntity[] targets = BattleInfoManager.GetTargetsSelected().ToArray();

        //    useSkillInBattleEventChannel.RaiseEvent(BattleInfoManager.selectedSkill.GetSkillId(), 
        //        targets,
        //        BattleInfoManager.entityOnTurn);
        //}
    }
}
