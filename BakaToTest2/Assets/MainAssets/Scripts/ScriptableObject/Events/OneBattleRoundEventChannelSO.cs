﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


[CreateAssetMenu(menuName = "ScriptableObject/Events/One Battle Round Event Channel")]
public class OneBattleRoundEventChannelSO : ScriptableObject
{
    public UnityAction<BattleEntity> OnFullRoundEvent;


    public void RaiseFullRoundEvent(BattleEntity character)
    {
        if (OnFullRoundEvent != null)
        {
            OnFullRoundEvent.Invoke(character);
        }
        else
        {
            Debug.LogWarning("A full round has ended but nobody picked it up.");
        }
    }
}
