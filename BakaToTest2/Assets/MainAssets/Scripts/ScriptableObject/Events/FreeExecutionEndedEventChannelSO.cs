﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Intelligensia.Battle;
using UnityEngine.Events;


[CreateAssetMenu(fileName = "FreeExecutionEndedEvent_Channel", menuName = "ScriptableObject/Events/Free Execution Ended Event Channel")]
public class FreeExecutionEndedEventChannelSO : ScriptableObject
{
    public UnityAction FreeExecutionEndedEvent;


    public void RaiseEvent()
    {
        FreeExecutionEndedEvent?.Invoke();
    }
}
