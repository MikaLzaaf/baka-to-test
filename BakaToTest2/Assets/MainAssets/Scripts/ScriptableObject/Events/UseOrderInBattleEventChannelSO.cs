﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Intelligensia.Battle;

[CreateAssetMenu(fileName = "UseOrderInBattleEvent_Channel", menuName = "ScriptableObject/Events/Use Order Event Channel")]
public class UseOrderInBattleEventChannelSO : ScriptableObject
{
    public UnityAction<BattleOrder> OnOrderUsage;


    public void RaiseEvent(BattleOrder battleOrder)
    {
        if (OnOrderUsage != null)
        {
            OnOrderUsage.Invoke(battleOrder);
            Debug.Log("Use order In Battle raised!");
        }
        else
        {
            if(battleOrder != null)
            {
                Debug.LogWarning("Order " + battleOrder.orderName + " is used but nobody cared.");
            }
        }
    }
}
