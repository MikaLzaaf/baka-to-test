﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(menuName = "ScriptableObject/Events/Minigame Ended Event Channel")]
public class MinigameEndedEventChannelSO : ScriptableObject
{
    public UnityAction<string, int> OnMinigameEnded;


    public void RaiseEvent(string minigameId, int minigameResult)
    {
        if(OnMinigameEnded != null)
        {
            OnMinigameEnded.Invoke(minigameId, minigameResult);
        }
        else
        {
            Debug.LogWarning("A minigame (" + minigameId + ") has ended but nobody picked it up.");
        }
    }

}
