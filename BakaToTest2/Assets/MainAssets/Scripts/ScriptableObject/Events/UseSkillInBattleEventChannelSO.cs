﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(fileName = "UseSkillInBattleEvent_Channel", menuName = "ScriptableObject/Events/Use Skill Event Channel")]

public class UseSkillInBattleEventChannelSO : ScriptableObject
{
    public UnityAction<string, BattleEntity[], BattleEntity> OnSkillUsage;


    public void RaiseEvent(string skillId, BattleEntity[] targets, BattleEntity user)
    {
        if (OnSkillUsage != null)
        {
            OnSkillUsage?.Invoke(skillId, targets, user);
            //Debug.Log("Use skill In Battle raised!");
        }
        else
        {
            Debug.LogWarning("Skill " + skillId + " is used but nobody cared.");
        }
    }
}
