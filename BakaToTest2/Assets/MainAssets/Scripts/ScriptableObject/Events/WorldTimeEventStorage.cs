﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Intelligensia.Time;
using UnityEngine.Events;

/// <summary>
/// Scriptable store world time events, all editing event will be inside here...
/// </summary>
[CreateAssetMenu(fileName ="WorldTimeEventStorage", menuName = "ScriptableObject/Storage/WorldTimeEvent")]
public class WorldTimeEventStorage : ScriptableObject
{
    [System.Serializable]
    public struct EventStorageList
    {
        public int year;
        public int month;
        public int day;

        public List<EventByDaySession> events;

    }

    [System.Serializable]
    public struct EventByDaySession
    {
        public DaySession daySession;
        public List<EventObj> events;
    }

    [System.Serializable]
    public struct EventObj
    {
        public string name;
        public string description;
        public UnityEvent triggerEvent;
    }

    public List<EventStorageList> events = new List<EventStorageList>();
}
