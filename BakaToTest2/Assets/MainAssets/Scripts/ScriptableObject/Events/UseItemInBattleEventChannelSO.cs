﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(fileName = "UseItemInBattleEvent_Channel", menuName = "ScriptableObject/Events/Use Item Event Channel")]

public class UseItemInBattleEventChannelSO : ScriptableObject
{
    public UnityAction<string, BattleEntity[], bool> OnItemUsage;


    public void RaiseEvent(string itemId, BattleEntity[] targets, bool isUsedByPlayer)
    {
        if (OnItemUsage != null)
        {
            OnItemUsage.Invoke(itemId, targets, isUsedByPlayer);
            Debug.Log("Use item In Battle raised!");
        }
        else
        {
            Debug.LogWarning("Item " + itemId + " is used but nobody cared.");
        }
    }
}
