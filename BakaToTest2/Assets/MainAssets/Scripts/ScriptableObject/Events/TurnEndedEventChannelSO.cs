﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


[CreateAssetMenu(menuName = "ScriptableObject/Events/Turn Ended Event Channel")]
public class TurnEndedEventChannelSO : ScriptableObject
{
    public UnityAction OnTurnEnded;

    public void RaiseEvent()
    {
        if (OnTurnEnded != null)
        {
            OnTurnEnded.Invoke();
        }
        else
        {
            Debug.LogWarning("The turn has ended but nobody picked it up.");
        }
    }

   
}
