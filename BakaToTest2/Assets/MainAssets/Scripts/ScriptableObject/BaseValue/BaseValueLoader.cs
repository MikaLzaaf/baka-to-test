﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif


public static class BaseValueLoader 
{
    private const string SkillBaseValueInResources = "SkillBaseValues/";
    private const string ItemDataInResources = "Items/BaseValues/";

#if UNITY_EDITOR
    private const string CharacterBaseValuesDirectory = "Assets/MainAssets/ScriptableObjects/CharacterStats";
    private const string ClassPrefix = "/Class_";
#endif


    public static BaseSkill LoadSkillBaseValue(string skillId)
    {
        string[] splits = skillId.Split('_');

        string path = SkillBaseValueInResources + splits[0].Trim() + "/" + skillId;

        return Resources.Load<BaseSkill>(path);
    }

    public static ItemBaseValue LoadItemData(string itemId)
    {
        string[] split = itemId.Split('_');

        string path = ItemDataInResources + split[0].Trim() + "/" + itemId;

        return Resources.Load<ItemBaseValue>(path);
    }


#if UNITY_EDITOR
    [MenuItem("Custom/CharacterBaseValues/Assign Name and ID to Skill Trees")]
    private static void AssignNameIDToSkillTrees()
    {
        //var a = AssetDatabase.LoadAllAssetsAtPath(CharacterBaseValuesDirectory + ClassPrefix + "F");
        //var a = AssetDatabase.GetSubFolders(CharacterBaseValuesDirectory);
        //Debug.Log(a.Length);
        string folderPath = CharacterBaseValuesDirectory + ClassPrefix + "F";

        string filter = "t:" + typeof(CharacterStatsBaseValue).ToString();
        var guids = AssetDatabase.FindAssets(filter, new string[] { folderPath });

        for (int j = 0; j < guids.Length; j++)
        {
            string path = AssetDatabase.GUIDToAssetPath(guids[j]);

            CharacterStatsBaseValue c = AssetDatabase.LoadAssetAtPath<CharacterStatsBaseValue>(path);

            if(c != null)
            {
                c.ResetSkillTreesNameAndID();

                EditorUtility.SetDirty(c);
            }
        }

        //for (int i = 0; i < a.Length; i++)
        //{
        //    Debug.Log(a[i]);

        //    string filter = "t:" + typeof(CharacterStatsBaseValue).ToString();

        //    var guids = AssetDatabase.FindAssets(filter, new string[] { a[i] });

        //    for(int j = 0; j < guids.Length; j++)
        //    {
        //        string path = AssetDatabase.GUIDToAssetPath(guids[j]);

        //        var c = AssetDatabase.LoadAssetAtPath<CharacterStatsBaseValue>(path);
        //        Debug.Log(c == null ? "NULL" : c.characterName);
        //    }
            
        //}
    }
#endif
}
