﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Intelligensia.UI.PlayerMenu;

[CreateAssetMenu(fileName = "New Skill", menuName = "ScriptableObject/Items/Skill")]
public class SkillItemBaseValue : ItemBaseValue
{
    [Header("Skill Base Value")]
    public BaseSkill baseSkill;
    public ArgumentsCategory argumentsCategory;

    [SerializeField] private bool isUniqueSkill = false;
    public bool IsUniqueSkill => isUniqueSkill;
    public CharacterId uniqueUser;

    [SerializeField] private bool isEnhanced = false;
    public bool IsEnhanced => isEnhanced;

    public override bool isSellable
    {
        get
        {
            return !isUniqueSkill;
        }
    }

    //public bool IsTheUniqueUser(CharacterId characterId)
    //{
    //    if (!isUniqueSkill)
    //        return false;

    //    return characterId == uniqueUser;
    //}
}
