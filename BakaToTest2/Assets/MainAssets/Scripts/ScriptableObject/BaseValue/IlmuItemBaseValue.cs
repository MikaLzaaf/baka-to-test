﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "Ilmu", menuName = "ScriptableObject/Items/Ilmu")]
public class IlmuItemBaseValue : ItemBaseValue
{
    public Subjects subject;


    private const string IconPrefix = "Ilmu_";


    public Sprite GetIcon()
    {
        //string materialID = IconPrefix + subject.ToString();
        return IconLoader.GetSubjectIcon(subject);
        //return ItemDataManager.LoadItemIcon(materialID);
    }

    
}
