﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Equipment", menuName = "ScriptableObject/Items/Equipment")]
public class EquipmentItemBaseValue : ItemBaseValue
{
    public override bool canEquip => true;

    [SerializeField]
    private EquipmentSection equipmentSection = EquipmentSection.Accessory;
    public EquipmentSection EquipmentSection
    {
        get { return equipmentSection; }
    }

    public BasicStats statsEfective;
    public AcademicStats academicStats;

    public List<StatusEffectResistance> statusEffectResistances;


    // - Tambah resistance stats n immunity to status effect
    // - 
}
