﻿using UnityEngine;
using System.Collections.Generic;
using Intelligensia.Battle;

[CreateAssetMenu(fileName = "Default Stats", menuName = "ScriptableObject/Character Stats Base Value")]
public class CharacterStatsBaseValue : ScriptableObject
{
    /// <summary> This is used for setting up the base value only. 
    /// Any progress after leveling is stored inside CharacterStatsData in GameData </summary>
    
    public string characterName;

    [StringInList(typeof(PropertyDrawersHelper), "AllClassNames")]
    public string studentClass;

    public CausesGenderTag gender;

    public PersonalityType personality;

    public BasicStats baseStats;

    public AcademicStats baseAcademicStats;

    public List<StatusEffectResistance> baseInnerStatusEffectResistances;

    public List<CharacterInfo> characterInfos;

    [Header("Default Skills")]
    public List<SkillItemBaseValue> defaultSkillItems;

    // The plan is to store all learnable skills inside here then retrieve it 
    // using stored id in CharacterStatsData. Also need to differentiate which skills are 
    // usable from the start.
    public DebateHelperSO supportArgument;

    [Header("Skill Trees")]
    public List<SkillTreeBaseValueSO> baseSkillTrees;

    // TODO : Need to differentiate based on their fatigue level
    public Sprite[] battleSprites; // Only have 4 sprites


    // TODO : Use CharacterStatsData to get this for player, use some manager for enemy
    public int GetUnlockedCharacterInfoCount()
    {
        int count = 0;

        for(int i = 0; i < characterInfos.Count; i++)
        {
            if(!characterInfos[i].isLocked)
            {
                count++;
            }
        }

        return count;
    }

    public DebateHelper GetDebateHelper(int argumentLevel)
    {
        if (supportArgument == null)
            return null;

        return supportArgument.GetDebateHelper(argumentLevel);
    }

    public Sprite GetBattleIcon(float currentFatigueLevel, bool isDefeated = false)
    {
        // Fatigue levels
        // - >= 0 && < 0.5 (Healthy)
        // - >= 0.5 && < 0.8 (Tired)
        // - >= 1 (Overfatigued)
        // - Defeated
        if (battleSprites.Length == 0)
            return null;

        if(isDefeated)
        {
            return battleSprites[3];
        }

        if (currentFatigueLevel < 0.6)
        {
            return battleSprites[0];
        }
        else if (currentFatigueLevel >= 0.6f && currentFatigueLevel < 1f)
        {
            return battleSprites[1];
        }
        else if (currentFatigueLevel >= 1f)
        {
            return battleSprites[2];
        }

        return null;
    }

    public string GetOverallGrade()
    {
        float totalScore = 0f;

        for (int i = 0; i < baseAcademicStats.subjectsStats.Count; i++)
        {
            totalScore += baseAcademicStats.subjectsStats[i].Score;
        }

        totalScore /= baseAcademicStats.subjectsStats.Count;

        return GradeChecker.GetGrade(Mathf.FloorToInt(totalScore));
    }

#if UNITY_EDITOR

    public void ResetSkillTreesNameAndID()
    {
        if (baseSkillTrees == null || baseSkillTrees.Count == 0)
            return;

        for(int i = 0; i < baseSkillTrees.Count; i++)
        {
            if (baseSkillTrees[i].skillTreeUnitDatas.Length == 0)
                continue;

            for(int j = 0; j < baseSkillTrees[i].skillTreeUnitDatas.Length; j++)
            {
                baseSkillTrees[i].skillTreeUnitDatas[j].ResetNameAndID();
            }
        }
    }

#endif
}
