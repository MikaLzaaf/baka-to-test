﻿using UnityEngine;


public class ItemBaseValue : ScriptableObject
{
    [SerializeField] private string itemID = default;
    public string ItemID => itemID;

    [SerializeField] private string itemName = default;
    public string ItemName => itemName;

    [SerializeField] private UIListItemIcons itemIcon = default;
    public string ItemIcon => itemIcon.ToString();

    [SerializeField] private ItemCategory itemCategory = default;
    public ItemCategory ItemCategory => itemCategory;

    
    [SerializeField] private string description = default;
    public string Description
    {
        get
        {
            if(!string.IsNullOrEmpty(ItemID))
            {
                if(ItemDataManager.HasKey(ItemID))
                {
                    description = ItemDataManager.GetItemDescriptionLocalized(ItemID);
                }
            }

            return description;
        }
    }

    public int purchasePrice = 100;
    public int sellPrice = 50;

    public bool canPurchaseMultiple = true;
    public bool isDeletable = true;

    public bool isLockedInitially = false;
    public virtual bool canEquip
    {
        get
        {
            return false;
        }
    }


    public bool isPurchasable
    {
        get
        {
            if (isLockedInitially)
            {
                return false;
            }

            // Cannot purchase if the inventory is full
            if (PlayerInventoryManager.instance.IsFull(ItemID))
            {
                return false;
            }

            // Cannot purchase if it is purchased and cannot purchase multiple
            if (!canPurchaseMultiple && PlayerInventoryManager.instance.HasItem(ItemID))
            {
                return false;
            }

            return true;
        }
    }


    public virtual bool isSellable
    {
        get
        {
            if (!isDeletable)
            {
                return false;
            }

            return true;
        }
    }


    public bool isConsumable
    {
        get
        {
            return ItemCategory == ItemCategory.Consumable;
        }
    }


    public virtual void Use(BattleEntity battleEntity)
    {
        Debug.Log("Base use");
    }


    public virtual AimedTo GetAim()
    {
        return AimedTo.SinglePartyMember;
    }
}
