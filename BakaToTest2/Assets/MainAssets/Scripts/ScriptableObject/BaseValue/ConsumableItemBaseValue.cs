﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Intelligensia.Battle.StatusEffects;

[CreateAssetMenu(fileName = "New Consumable", menuName = "ScriptableObject/Items/Consumable Item")]
public class ConsumableItemBaseValue : ItemBaseValue
{
    [Header("Consumable Parameter")]
    public ConsumableItemType consumableType;
    public AimedTo itemTargetUser;

    [SerializeField] private SupportEffect[] supportEffects = default;

    [Header("Event Channels")]
    public UseItemInBattleEventChannelSO useItemInBattleEventChannel;
    //public MinigameEndedEventChannelSO minigameEndedEventChannel;

    private const string AimedToSelfLocalizationID = "AimedTo_Self";
    private const string AimedToOneLocalizationID = "AimedTo_One";
    private const string AimedToAllLocalizationID = "AimedTo_All";

    //private const string CureFatigueDescriptionFormatID = "CureFatigueDescriptionFormatID";
    //private const string CureStatusEffectDescriptionPrefixID = "CureStatusEffectDescriptionPrefixID";
    //private const string AddStatusBuffDescriptionPrefixID = "AddStatusBuffDescriptionPrefixID";


    public override AimedTo GetAim()
    {
        return itemTargetUser;
    }

    public override void Use(BattleEntity battleEntity)
    {
        Consume(battleEntity);
    }

    public void Consume(BattleEntity entity)
    {
        for(int i = 0; i < supportEffects.Length; i++)
        {
            if (supportEffects[i].recoveryType == RecoveryType.CureFatigue)
            {
                CureFatigue(entity, supportEffects[i]);
            }
            else if (supportEffects[i].recoveryType == RecoveryType.CureStatusEffect)
            {
                CureStatusEffect(entity, supportEffects[i]);
            }
            else if (supportEffects[i].recoveryType == RecoveryType.AddStatusBuff)
            {
                AddStatusBuff(entity, supportEffects[i]);
            }
        }
    }


    public bool CanCureFatigue()
    {
        if (supportEffects.Length == 0)
            return false;

        for(int i = 0; i < supportEffects.Length; i++)
        {
            if (supportEffects[i].recoveryType == RecoveryType.CureFatigue)
                return true;
        }

        return false;
    }

    public bool CanCureStatusAilment(string statusEffectName)
    {
        if (supportEffects.Length == 0)
            return false;

        for (int i = 0; i < supportEffects.Length; i++)
        {
            if (supportEffects[i].CanCureStatusEffect(statusEffectName))
                return true;
        }

        return false;
    }


    private void CureFatigue(BattleEntity target, SupportEffect supportEffect)
    {
        float effectiveAmount = supportEffect.GetEffectiveValue(target.stamina);

        target.AddFatigue(-effectiveAmount, DamageHitCategory.NormalHit);
    }

    private void CureStatusEffect(BattleEntity target, SupportEffect recoveryEffect)
    {
        if (target == null)
            return;

        if (target.statusEffectController == null)
            return;

        for (int i = 0; i < recoveryEffect.curableStatusEffects.Length; i++)
        {
            StatusAilment statusAilment = recoveryEffect.curableStatusEffects[i];

            target.statusEffectController.RemoveAilment(statusAilment);
        }
    }

    private void AddStatusBuff(BattleEntity target, SupportEffect recoveryEffect)
    {
        if (target == null)
            return;

        if (target.statusEffectController == null)
            return;

        for (int i = 0; i < recoveryEffect.statusBoosts.Length; i++)
        {
            StatusBoostType statusBoostType = recoveryEffect.statusBoosts[i];

            target.statusEffectController.AddStatusBoost(statusBoostType);
        }
    }

    public string GetAimInDescription()
    {
        if(itemTargetUser == AimedTo.Self)
        {
            return LocalizationManager.Localize(AimedToSelfLocalizationID);
        }
        else if (itemTargetUser == AimedTo.SingleEnemy || itemTargetUser == AimedTo.SinglePartyMember)
        {
            return LocalizationManager.Localize(AimedToOneLocalizationID);
        }
        else if (itemTargetUser == AimedTo.AllEnemies || itemTargetUser == AimedTo.AllPartyMembers || itemTargetUser == AimedTo.All)
        {
            return LocalizationManager.Localize(AimedToAllLocalizationID);
        }

        return "";
    }

    public string GetSupportEffectsDescription()
    {
        string description = "";

        for(int i = 0; i < supportEffects.Length; i++)
        {
            string effectDescription = supportEffects[i].GetDescription();

            if(i != supportEffects.Length - 1)
            {
                if (!string.IsNullOrEmpty(effectDescription))
                {
                    effectDescription += "/";
                }
            }

            description += effectDescription;
        }


        return description;
    }
}
