﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Base Stats Upgrade", menuName = "ScriptableObject/Stats Upgrade/Base Stats")]
public class BaseStatsUpgradeSO : SkillTreeUnitBaseValueSO
{
    public BaseStats statsToUpgrade;

    private const string DescriptionFormat = "Increases {0} by {1}.";


    public string GetDescription()
    {
        return string.Format(DescriptionFormat, statsToUpgrade.StatsName.ToString(), statsToUpgrade.finalValue);
    }

}
