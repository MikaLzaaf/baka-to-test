﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Subject Stats Upgrade", menuName = "ScriptableObject/Stats Upgrade/Subject Stats")]
public class SubjectStatsUpgradeSO : SkillTreeUnitBaseValueSO
{
    public SubjectStats subjectStatsToUpgrade;
    public BaseStats baseStatsToUpgrade;

    private const string DescriptionFormat = "Increases {0} & {1} by {2}.";


    public string GetDescription()
    {
        string subjectName = StringHelper.SplitByCapitalizeFirstLetter(subjectStatsToUpgrade.Subject.ToString());

        return string.Format(DescriptionFormat, subjectName, baseStatsToUpgrade.StatsName, subjectStatsToUpgrade.finalValue);
    }
}
