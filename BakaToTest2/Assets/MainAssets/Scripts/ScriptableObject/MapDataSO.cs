﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



[CreateAssetMenu(fileName = "Map Data", menuName = "ScriptableObject/Data/Map")]
public class MapDataSO : ScriptableObject
{
    /// <summary>
    /// This SO will be used to initialize all map data when a new game data is created.
    /// Then a separate data (probably a json file) will be used to track which map area has been unlocked 
    /// and which fast travel point can be used throughout the whole game.
    /// </summary>
    private const string MapImageInResources = "Images/Maps/";

    public List<MapParentAreaInfo> mapParentAreaInfos;


    public void SaveCurrentSceneMapData()
    {
        MapParentArea parentArea = FindObjectOfType<MapParentArea>();

        if (parentArea == null)
        {
            Debug.LogError("There is no parent area component in the scene!");
            return;
        }

        string parentAreaName = "";
        List<MapMainAreaInfo> mainAreaInfos = new List<MapMainAreaInfo>();

        for(int i = 0; i < parentArea.transform.childCount; i++)
        {
            if(parentArea.transform.GetChild(i).TryGetComponent(out MapArea mainArea))
            {
                if (string.IsNullOrEmpty(parentAreaName) && !string.IsNullOrEmpty(mainArea.parentAreaName))
                    parentAreaName = mainArea.parentAreaName;

                List<MapFastTravelPointInfo> fastTravelPointInfos = new List<MapFastTravelPointInfo>();
                List<MapEntranceInfo> mapEntranceInfos = new List<MapEntranceInfo>();

                mainArea.FindAreaEntrances();

                for(int j = 0; j < mainArea.areaEntrances.Length; j++)
                {
                    MapEntranceTrigger mapEntrance = mainArea.areaEntrances[j];

                    MapEntranceInfo mapEntranceInfo = new MapEntranceInfo(mapEntrance.entranceID, mapEntrance.destinationAreaName, 
                        mapEntrance.destinationEntranceID, mapEntrance.destinationInAnotherParentArea);

                    mapEntranceInfos.Add(mapEntranceInfo);
                }

                mainArea.FindFastTravelPoints();

                for(int j = 0; j < mainArea.fastTravelPoints.Count; j++)
                {
                    MapFastTravelPoint fastTravelPoint = mainArea.fastTravelPoints[j];

                    MapFastTravelPointInfo fastTravelPointInfo = new MapFastTravelPointInfo(fastTravelPoint.pointName, fastTravelPoint.pointDisplayName, 
                        fastTravelPoint.transform.position, fastTravelPoint.showLocationOnMap, fastTravelPoint.canFastTravel);

                    fastTravelPointInfos.Add(fastTravelPointInfo);
                }

                MapMainAreaInfo mainAreaInfo = new MapMainAreaInfo(mainArea, fastTravelPointInfos, mapEntranceInfos);

                mainAreaInfos.Add(mainAreaInfo);

                CheckMapImageInResources(MapImageInResources + mainArea.areaName);
            }
        }
        
        MapParentAreaInfo mapParentAreaInfo = new MapParentAreaInfo(parentAreaName, mainAreaInfos);

        if (mapParentAreaInfos == null)
            mapParentAreaInfos = new List<MapParentAreaInfo>();

        bool isReplaced = false;

        for(int i = 0; i < mapParentAreaInfos.Count; i++)
        {
            if(mapParentAreaInfos[i].parentAreaName == parentAreaName)
            {
                MapParentAreaInfo existingMapParentAreaInfo = mapParentAreaInfos[i];

                for(int j = 0; j < mapParentAreaInfo.mainAreaInfos.Count; j++)
                {
                    MapMainAreaInfo newMapMainAreaInfo = mapParentAreaInfo.mainAreaInfos[j];

                    if (!existingMapParentAreaInfo.HasMapMainArea(newMapMainAreaInfo.mainAreaName))
                    {
                        existingMapParentAreaInfo.AddMapMainArea(newMapMainAreaInfo);
                    }
                    else
                    {
                        MapMainAreaInfo existingMapMainAreaInfo = existingMapParentAreaInfo.GetMainAreaInfo(newMapMainAreaInfo.mainAreaName);

                        int index = existingMapParentAreaInfo.mainAreaInfos.IndexOf(existingMapMainAreaInfo);

                        existingMapParentAreaInfo.mainAreaInfos[index] = newMapMainAreaInfo;
                    }
                }

                CheckEntranceValidity(existingMapParentAreaInfo);

                isReplaced = true;
            }
        }

        if (!isReplaced)
            mapParentAreaInfos.Add(mapParentAreaInfo);
    }


    private void CheckMapImageInResources(string path)
    {
        var image = Resources.Load(path);

        if(image == null)
        {
            Debug.LogError("There is no MapImage at " + path);
        }
    }

    private void CheckEntranceValidity(MapParentAreaInfo mapParentAreaInfo)
    {
        // Need to check whether the destination area name filled is correct
        // and need to check whether the area has the destination entrance id

        for(int i = 0; i < mapParentAreaInfo.mainAreaInfos.Count; i++)
        {
            MapMainAreaInfo currentMainAreaInfo = mapParentAreaInfo.mainAreaInfos[i];

            for(int j = 0; j < currentMainAreaInfo.entranceInfos.Count; j++)
            {
                MapEntranceInfo mapEntranceInfo = currentMainAreaInfo.entranceInfos[j];

                if (mapEntranceInfo.destinationInAnotherParentArea)
                    continue;

                if(!mapParentAreaInfo.HasMapMainArea(mapEntranceInfo.destinationAreaName))
                {
                    Debug.LogError("The entrance " + mapEntranceInfo.entranceID + " in " + currentMainAreaInfo.mainAreaName + " has wrong destinationAreaName!");
                    return;
                }

                MapMainAreaInfo destinationArea = mapParentAreaInfo.GetMainAreaInfo(mapEntranceInfo.destinationAreaName);

                if(!destinationArea.HasEntrance(mapEntranceInfo.destinationEntranceID))
                {
                    Debug.LogError("The entrance " + mapEntranceInfo.entranceID + " in " + currentMainAreaInfo.mainAreaName + " has wrong destinationEntranceID!");
                }
            }
        }
    }
}
