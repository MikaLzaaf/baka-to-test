﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Intelligensia.Battle
{
    [System.Serializable]
    public class SupportArgument_Combat
    {
        //public ArgumentHelper.CombatSupportType combatSupportType;
        //public string description;

        // Support : Shortens/Extends Cooldown
        //public int cooldownExtendDuration = 0;
        // Support :  Multiply Skill Damage
        public float skillDamageMultiplier = 1.0f;
        // Support :  Stress Gauge Affector
        public int stressAffectValue = 0;

#if UNITY_EDITOR
        public bool _expanded = false;
#endif

        //public float GetSupportValue(ArgumentHelper.CombatSupportType combatSupportType)
        //{
        //    if (combatSupportType == ArgumentHelper.CombatSupportType.MultiplySkillDamage)
        //    {
        //        return skillDamageMultiplier;
        //    }
        //    else if (combatSupportType == ArgumentHelper.CombatSupportType.AffectStressGauge)
        //    {
        //        return stressAffectValue;
        //    }

        //    return 1;
        //}
    }
}

