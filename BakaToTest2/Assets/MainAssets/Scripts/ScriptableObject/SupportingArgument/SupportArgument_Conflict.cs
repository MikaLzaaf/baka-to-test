﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Intelligensia.Battle
{
    [System.Serializable]
    public class SupportArgument_Conflict 
    {
        //public ArgumentHelper.ConflictSupportType conflictSupportType;

        public int addDefenseValue;
        public int addAttackValue;
        public int deductOpponentDefenseValue;
        public int deductOpponentAttackValue;
        public int stealOpponentCoresValue;



#if UNITY_EDITOR
        public bool _expanded = false;
#endif

        //public int GetSupportValue(ArgumentHelper.ConflictSupportType conflictSupportType)
        //{
        //    if(conflictSupportType == ArgumentHelper.ConflictSupportType.AddDefense)
        //    {
        //        return addDefenseValue;
        //    }
        //    else if (conflictSupportType == ArgumentHelper.ConflictSupportType.AddAttack)
        //    {
        //        return addAttackValue;
        //    }
        //    else if (conflictSupportType == ArgumentHelper.ConflictSupportType.DeductOpponentDefense)
        //    {
        //        return deductOpponentDefenseValue;
        //    }
        //    else if (conflictSupportType == ArgumentHelper.ConflictSupportType.DeductOpponentAttack)
        //    {
        //        return deductOpponentAttackValue;
        //    }
        //    else if (conflictSupportType == ArgumentHelper.ConflictSupportType.RemoveOpponentDefense)
        //    {
        //        return 0;
        //    }
        //    else if (conflictSupportType == ArgumentHelper.ConflictSupportType.RemoveOpponentAttack)
        //    {
        //        return 0;
        //    }
        //    else if (conflictSupportType == ArgumentHelper.ConflictSupportType.StealOpponentCores)
        //    {
        //        return stealOpponentCoresValue;
        //    }

        //    return 1;
        //}
    }
}

