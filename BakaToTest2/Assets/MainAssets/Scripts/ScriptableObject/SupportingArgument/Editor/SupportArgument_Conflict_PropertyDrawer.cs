﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Intelligensia.Battle
{
#if UNITY_EDITOR
    [CustomPropertyDrawer(typeof(SupportArgument_Conflict))]
    public class SupportArgument_Conflict_PropertyDrawer : PropertyDrawer
    {
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            float h = base.GetPropertyHeight(property, label);
            int rows = 1;
            if (property.FindPropertyRelative("_expanded").boolValue)
            {
                rows += 1;
                //ArgumentHelper.ConflictSupportType itemType = (ArgumentHelper.ConflictSupportType)property.FindPropertyRelative("conflictSupportType").enumValueIndex;
                //if (itemType != ArgumentHelper.ConflictSupportType.Undefined && itemType != ArgumentHelper.ConflictSupportType.RemoveOpponentDefense
                //    && itemType != ArgumentHelper.ConflictSupportType.RemoveOpponentAttack)
                //    rows += 1;
                //else if (itemType == ArgumentHelper.SupportType.RemoveAllSkillsCooldown)
                //    rows += 2;
            }
            return h * rows;
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            // Draw label
            SerializedProperty p_expanded = property.FindPropertyRelative("_expanded");
            float h = base.GetPropertyHeight(property, label);
            position.height = h;
            p_expanded.boolValue = EditorGUI.Foldout(position, p_expanded.boolValue, label);
            if (p_expanded.boolValue)
            {
                position.y += h;
                position.height = GetPropertyHeight(property, label) - h;
                EditorGUI.BeginProperty(position, label, property);

                //position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

                ++EditorGUI.indentLevel;

                // Calculate rects
                float x = position.x,
                    y = position.y,
                    w = position.width;


                //Rect r_cost = new Rect(x, y, w, h);
                //y += h;
                //Rect r_image = new Rect(x, y, w, h);
                //y += h;
                Rect r_conflictSupport_type = new Rect(x, y, w, h);
                //y += h;
                //Rect r_description = new Rect(x, y, w, h);
                // draw properties

                //SerializedProperty p_cost = property.FindPropertyRelative("cost");
                //SerializedProperty p_image = property.FindPropertyRelative("image");
                SerializedProperty p_conflictSupport_type = property.FindPropertyRelative("conflictSupportType");
                //SerializedProperty p_description = property.FindPropertyRelative("description");

                //EditorGUI.PropertyField(r_cost, p_cost, new GUIContent(p_cost.displayName));
                //EditorGUI.PropertyField(r_image, p_image, new GUIContent(p_image.displayName));
                EditorGUI.PropertyField(r_conflictSupport_type, p_conflictSupport_type, new GUIContent(p_conflictSupport_type.displayName));
                //EditorGUI.PropertyField(r_description, p_description, new GUIContent(p_description.displayName));
                // hidden item type fields
                ++EditorGUI.indentLevel;

                //ArgumentHelper.ConflictSupportType itemType = (ArgumentHelper.ConflictSupportType)p_conflictSupport_type.enumValueIndex;
                //if (itemType == ArgumentHelper.ConflictSupportType.AddDefense)
                //{
                //    //y += h;
                //    //Rect r_armor_type = new Rect(x, y, w, h);
                //    y += h;
                //    Rect r_addDefenseValue = new Rect(x, y, w, h);

                //    //SerializedProperty p_armor_type = property.FindPropertyRelative("armor_type");
                //    SerializedProperty p_addDefenseValue = property.FindPropertyRelative("addDefenseValue");

                //    //EditorGUI.PropertyField(r_armor_type, p_armor_type, new GUIContent(p_armor_type.displayName));
                //    EditorGUI.PropertyField(r_addDefenseValue, p_addDefenseValue, new GUIContent(p_addDefenseValue.displayName));
                //}
                //else if (itemType == ArgumentHelper.ConflictSupportType.AddAttack)
                //{
                //    //y += h;
                //    //Rect r_armor_type = new Rect(x, y, w, h);
                //    y += h;
                //    Rect r_addAttackValue = new Rect(x, y, w, h);

                //    //SerializedProperty p_armor_type = property.FindPropertyRelative("armor_type");
                //    SerializedProperty p_addAttackValue = property.FindPropertyRelative("addAttackValue");

                //    //EditorGUI.PropertyField(r_armor_type, p_armor_type, new GUIContent(p_armor_type.displayName));
                //    EditorGUI.PropertyField(r_addAttackValue, p_addAttackValue, new GUIContent(p_addAttackValue.displayName));
                //}
                //else if (itemType == ArgumentHelper.ConflictSupportType.DeductOpponentDefense)
                //{
                //    //y += h;
                //    //Rect r_weapon_type = new Rect(x, y, w, h);
                //    y += h;
                //    Rect r_deductOppDefenseValue = new Rect(x, y, w, h);

                //    //SerializedProperty p_weapon_type = property.FindPropertyRelative("weapon_type");
                //    SerializedProperty p_deductOppDefenseValue = property.FindPropertyRelative("deductOpponentDefenseValue");

                //    //EditorGUI.PropertyField(r_weapon_type, p_weapon_type, new GUIContent(p_weapon_type.displayName));
                //    EditorGUI.PropertyField(r_deductOppDefenseValue, p_deductOppDefenseValue, new GUIContent(p_deductOppDefenseValue.displayName));
                //}
                //else if (itemType == ArgumentHelper.ConflictSupportType.DeductOpponentAttack)
                //{
                //    //y += h;
                //    //Rect r_weapon_type = new Rect(x, y, w, h);
                //    y += h;
                //    Rect r_deductOppAttackValue = new Rect(x, y, w, h);

                //    //SerializedProperty p_weapon_type = property.FindPropertyRelative("weapon_type");
                //    SerializedProperty p_deductOppAttackValue = property.FindPropertyRelative("deductOpponentAttackValue");

                //    //EditorGUI.PropertyField(r_weapon_type, p_weapon_type, new GUIContent(p_weapon_type.displayName));
                //    EditorGUI.PropertyField(r_deductOppAttackValue, p_deductOppAttackValue, new GUIContent(p_deductOppAttackValue.displayName));
                //}
                //else if (itemType == ArgumentHelper.ConflictSupportType.StealOpponentCores)
                //{
                //    //y += h;
                //    //Rect r_weapon_type = new Rect(x, y, w, h);
                //    y += h;
                //    Rect r_stealValue = new Rect(x, y, w, h);

                //    //SerializedProperty p_weapon_type = property.FindPropertyRelative("weapon_type");
                //    SerializedProperty p_stealValue = property.FindPropertyRelative("stealOpponentCoresValue");

                //    //EditorGUI.PropertyField(r_weapon_type, p_weapon_type, new GUIContent(p_weapon_type.displayName));
                //    EditorGUI.PropertyField(r_stealValue, p_stealValue, new GUIContent(p_stealValue.displayName));
                //}
                //else
                //{

                //}


                --EditorGUI.indentLevel;

                --EditorGUI.indentLevel;

                EditorGUI.EndProperty();
            }
        }
    }
#endif
}

