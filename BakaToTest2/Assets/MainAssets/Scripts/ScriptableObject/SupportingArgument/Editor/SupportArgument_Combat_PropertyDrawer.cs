﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Intelligensia.Battle
{
    [CustomPropertyDrawer(typeof(SupportArgument_Combat))]
    public class SupportArgument_Combat_PropertyDrawer : PropertyDrawer
    {
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            float h = base.GetPropertyHeight(property, label);
            int rows = 1;
            //if (property.FindPropertyRelative("_expanded").boolValue)
            //{
            //    rows += 1;
            //    ArgumentHelper.CombatSupportType itemType = (ArgumentHelper.CombatSupportType)property.FindPropertyRelative("combatSupportType").enumValueIndex;
            //    if (itemType == ArgumentHelper.CombatSupportType.MultiplySkillDamage || itemType == ArgumentHelper.CombatSupportType.AffectStressGauge)
            //        rows += 1;
            //    //else if (itemType == ArgumentHelper.SupportType.RemoveAllSkillsCooldown)
            //    //    rows += 2;
            //}
            return h * rows;
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            // Draw label
            SerializedProperty p_expanded = property.FindPropertyRelative("_expanded");
            float h = base.GetPropertyHeight(property, label);
            position.height = h;
            p_expanded.boolValue = EditorGUI.Foldout(position, p_expanded.boolValue, label);
            if (p_expanded.boolValue)
            {
                position.y += h;
                position.height = GetPropertyHeight(property, label) - h;
                EditorGUI.BeginProperty(position, label, property);

                //position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

                ++EditorGUI.indentLevel;

                // Calculate rects
                float x = position.x,
                    y = position.y,
                    w = position.width;

               
                //Rect r_cost = new Rect(x, y, w, h);
                //y += h;
                //Rect r_image = new Rect(x, y, w, h);
                //y += h;
                Rect r_combatSupport_type = new Rect(x, y, w, h);
                //y += h;
                //Rect r_description = new Rect(x, y, w, h);
                // draw properties

                //SerializedProperty p_cost = property.FindPropertyRelative("cost");
                //SerializedProperty p_image = property.FindPropertyRelative("image");
                SerializedProperty p_combatSupport_type = property.FindPropertyRelative("combatSupportType");
                //SerializedProperty p_description = property.FindPropertyRelative("description");

                //EditorGUI.PropertyField(r_cost, p_cost, new GUIContent(p_cost.displayName));
                //EditorGUI.PropertyField(r_image, p_image, new GUIContent(p_image.displayName));
                EditorGUI.PropertyField(r_combatSupport_type, p_combatSupport_type, new GUIContent(p_combatSupport_type.displayName));
                //EditorGUI.PropertyField(r_description, p_description, new GUIContent(p_description.displayName));
                // hidden item type fields
                ++EditorGUI.indentLevel;

                //ArgumentHelper.CombatSupportType itemType = (ArgumentHelper.CombatSupportType)p_combatSupport_type.enumValueIndex;
                //if (itemType == ArgumentHelper.CombatSupportType.MultiplySkillDamage)
                //{
                //    //y += h;
                //    //Rect r_weapon_type = new Rect(x, y, w, h);
                //    y += h;
                //    Rect r_damageMultiplier = new Rect(x, y, w, h);

                //    //SerializedProperty p_weapon_type = property.FindPropertyRelative("weapon_type");
                //    SerializedProperty p_damageMultiplier = property.FindPropertyRelative("skillDamageMultiplier");

                //    //EditorGUI.PropertyField(r_weapon_type, p_weapon_type, new GUIContent(p_weapon_type.displayName));
                //    EditorGUI.PropertyField(r_damageMultiplier, p_damageMultiplier, new GUIContent(p_damageMultiplier.displayName));
                //}
                //else if (itemType == ArgumentHelper.CombatSupportType.AffectStressGauge)
                //{
                //    //y += h;
                //    //Rect r_armor_type = new Rect(x, y, w, h);
                //    y += h;
                //    Rect r_affectValue = new Rect(x, y, w, h);

                //    //SerializedProperty p_armor_type = property.FindPropertyRelative("armor_type");
                //    SerializedProperty p_affectValue = property.FindPropertyRelative("stressAffectValue");

                //    //EditorGUI.PropertyField(r_armor_type, p_armor_type, new GUIContent(p_armor_type.displayName));
                //    EditorGUI.PropertyField(r_affectValue, p_affectValue, new GUIContent(p_affectValue.displayName));
                //}
                //else if (itemType == ArgumentHelper.CombatSupportType.CriticalHitGuarantee)
                //{

                //}
                //else
                //{

                //}


                --EditorGUI.indentLevel;

                --EditorGUI.indentLevel;

                EditorGUI.EndProperty();
            }
        }
    }
}

