﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Debate Helper", menuName = "ScriptableObject/Debate Helper")]
public class DebateHelperSO : ScriptableObject
{
    [SerializeField] private DebateHelper[] debateHelpers = default;

    public DebateHelper GetDebateHelper(int helpLevel)
    {
        for(int i = 0; i < debateHelpers.Length; i++)
        {
            if (debateHelpers[i].Level == helpLevel)
                return debateHelpers[i];
        }

        return null;
    }
}
