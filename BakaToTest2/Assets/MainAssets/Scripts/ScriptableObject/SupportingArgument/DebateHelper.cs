﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Intelligensia.Battle;

[System.Serializable]
public class DebateHelper
{
    /// <summary>
    /// Only used for supporting Offensive Skill during Battle. 
    /// The argument is applied on Offensive Skill's ApplySkill method.
    /// </summary>

    //public enum CombatSupportType
    //{
    //    Undefined,
    //    RemoveOneSkillCooldown,
    //    RemoveAllSkillsCooldown,
    //    MultiplySkillDamage,
    //    AffectStressGauge,
    //    CriticalHitGuarantee
    //}

    //public enum ConflictSupportType
    //{
    //    Undefined, 
    //    AddDefense,
    //    AddAttack,
    //    DeductOpponentDefense,
    //    DeductOpponentAttack,
    //    RemoveOpponentDefense,
    //    RemoveOpponentAttack,
    //    StealOpponentCores,
    //}

    public enum DebateSupportType
    {
        Undefined,
        DeductOpponentArgument,
        AddAllyArgument,
        VoidOpponentArgument, // For level 2
        DoubleAllyArgument // For level 2
            // Later could add support to clear opponent's debate modifier etc.
    }

    [SerializeField] private int level = default;
    public int Level => level;

    [SerializeField] private string argumentName = default;
    public string ArgumentName => argumentName;

    [SerializeField] private Sprite ownerIcon = default;
    public Sprite OwnerIcon => ownerIcon;

    //[Header("Combat Supports")]
    //[SerializeField] private SupportArgument_Combat[] combatSupports = default;

    //[SerializeField] 
    //[TextArea] private string combatHelpDescription = default;
    //public string CombatHelpDescription => combatHelpDescription;

    //[Header("Conflict Supports")]
    //[SerializeField] private SupportArgument_Conflict[] conflictSupports = default;

    //[SerializeField]
    //[TextArea] private string conflictHelpDescription = default;
    //public string ConflictHelpDescription => conflictHelpDescription;

    //[SerializeField] private bool targetAllyArgument = default;
    //public bool TargetAllyArgument => targetAllyArgument;

    [SerializeField] private DebateSupportType _debateSupportType = default;
    public DebateSupportType debateSupportType => _debateSupportType;

    [SerializeField] private int debateSupportValue = default;
    public int DebateSupportValue => debateSupportValue;

    public bool CanUseSupportOnArgument(bool isPlayerArgument)
    {
        bool targetArgumentIsPlayer = true;

        if (_debateSupportType == DebateSupportType.DeductOpponentArgument || _debateSupportType == DebateSupportType.VoidOpponentArgument)
            targetArgumentIsPlayer = false;

        return targetArgumentIsPlayer == isPlayerArgument;
    }

    //#region Combat Support
    //public bool IsCombatSupportType(CombatSupportType type)
    //{
    //    for(int i = 0; i < combatSupports.Length; i++)
    //    {
    //        if (combatSupports[i].combatSupportType == type)
    //            return true;
    //    }

    //    return false;
    //}

    //public float GetCombatSupportValue(CombatSupportType combatSupportType)
    //{
    //    if (combatSupports.Length == 0)
    //        return 1;

    //    for (int i = 0; i < combatSupports.Length; i++)
    //    {
    //        if (combatSupports[i].combatSupportType == combatSupportType)
    //            return combatSupports[i].GetSupportValue(combatSupportType);
    //    }

    //    return 1;
    //}

    //public void ApplyEffect(BattleEntity user)
    //{
    //    if (combatSupports.Length == 0) // Only for combat supports
    //        return;

    //    // Only applicable to RemoveOneSkillCooldown, RemoveAllSkillsCooldown & AffectStressGauge
    //    // since others are implemented somewhere else

    //    for (int i = 0; i < combatSupports.Length; i++)
    //    {
    //        if (combatSupports[i].combatSupportType == CombatSupportType.AffectStressGauge)
    //        {
    //            BattleInfoManager.currentStressPoint += (int)GetCombatSupportValue(CombatSupportType.AffectStressGauge);
    //        }
    //        else if (combatSupports[i].combatSupportType == CombatSupportType.RemoveOneSkillCooldown)
    //        {
    //            PlayerEntity playerEntity = (PlayerEntity)user;

    //            if(playerEntity != null)
    //            {
    //                InBattleSkill usedSkill = playerEntity.GetInBattleSkill(BattleInfoManager.selectedSkill.GetSkillId());

    //                if(usedSkill != null)
    //                {
    //                    usedSkill.ResetCooldown();
    //                }
    //            }
    //        }
    //        else if (combatSupports[i].combatSupportType == CombatSupportType.RemoveAllSkillsCooldown)
    //        {
    //            PlayerEntity playerEntity = (PlayerEntity)user;

    //            if (playerEntity != null)
    //            {
    //                var allOffensiveSkills = playerEntity.GetOffensiveSkillsBySubject(Subjects.BahasaMelayu, true);

    //                for(int j = 0; j < allOffensiveSkills.Length; j++)
    //                {
    //                    if(allOffensiveSkills[j].isOnCooldown)
    //                        allOffensiveSkills[j].ResetCooldown();

    //                }
    //            }
    //        }
    //    }
    //}

//#endregion

    //#region Conflict Support

    //public bool IsConflictSupportType(ConflictSupportType type)
    //{
    //    for (int i = 0; i < conflictSupports.Length; i++)
    //    {
    //        if (conflictSupports[i].conflictSupportType == type)
    //            return true;
    //    }

    //    return false;
    //}

    //public int GetConflictSupportValue(ConflictSupportType conflictSupportType)
    //{
    //    if (conflictSupports.Length == 0)
    //        return 1;

    //    for(int i = 0; i < conflictSupports.Length; i++)
    //    {
    //        if (conflictSupports[i].conflictSupportType == conflictSupportType)
    //            return conflictSupports[i].GetSupportValue(conflictSupportType);
    //    }

    //    return 1;
    //}

    //#endregion
}
