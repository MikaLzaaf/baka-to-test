﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BasicStats
{
   
    public int concentration => GetStatsValue(StatsName.CON); // Determines the value for focus action in battle and act as defense for attack

    public int intelligence => GetStatsValue(StatsName.INT); // Determines the value for attack

    public int speed => GetStatsValue(StatsName.SPD); // Determines the character's turn in a battle

    public float criticalRate => GetStatsValue(StatsName.CRT);

    public List<BaseStats> baseStats;

    private Dictionary<StatsName, BaseStats> statsLookup;
    private Dictionary<StatsName, BaseStats> StatsLookup
    {
        get
        {
            if(statsLookup == null)
            {
                statsLookup = new Dictionary<StatsName, BaseStats>();

                foreach(BaseStats stat in baseStats)
                {
                    statsLookup.Add(stat.StatsName, stat);
                }
            }

            return statsLookup;
        }
    }


 

    public BaseStats GetBaseStats(StatsName statsName)
    {
        StatsLookup.TryGetValue(statsName, out BaseStats stats);

        return stats;
    }


    public int GetStatsValue(StatsName statsName)
    {
        StatsLookup.TryGetValue(statsName, out BaseStats stats);

        if(stats != null)
        {
            return stats.finalValue;
        }

        return 0;
    }


    public void UpdateStatsModifiers(string modifierKey, BasicStats otherStats, bool replaceOldMod = false)
    {
        List<BaseStats> otherBaseStats = otherStats.baseStats;

        if (otherBaseStats == null)
            return;

        // Add other stats to this stats
        for (int i = 0; i < otherBaseStats.Count; i++)
        {
            BaseStats currentStat = GetBaseStats(otherBaseStats[i].StatsName);

            if (currentStat != null)
            {
                currentStat.AddModifier(modifierKey, otherBaseStats[i].finalValue, replaceOldMod);
            }
        }
    }

    public bool HasStats()
    {
        if (baseStats == null)
            return false;

        return baseStats.Count > 0;
    }
}


public enum StatsName
{
    CON,
    INT,
    SPD,
    CRT
}
