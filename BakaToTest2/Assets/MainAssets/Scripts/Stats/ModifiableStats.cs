﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public abstract class ModifiableStats 
{
    [HideInInspector, SerializeField]
    private List<StatModifier> modifiers = new List<StatModifier>();

    private Dictionary<string, StatModifier> _modifierLookup;
    private Dictionary<string, StatModifier> modifierLookup
    {
        get
        {
            if (_modifierLookup == null)
            {
                _modifierLookup = new Dictionary<string, StatModifier>();

                foreach (StatModifier mod in modifiers)
                {
                    _modifierLookup.Add(mod.key, mod);
                }
            }

            return _modifierLookup;
        }
    }

    public int finalValue
    {
        get
        {
            int finalAmount = GetStatsAmount();

            for (int i = 0; i < modifiers.Count; i++)
            {
                finalAmount += modifiers[i].value;
            }

            return finalAmount;
        }
    }

    protected abstract int GetStatsAmount();


    public StatModifier GetModifier(string key)
    {
        modifierLookup.TryGetValue(key, out StatModifier statModifier);

        return statModifier;
    }


    public void AddModifier(string key, int value, bool replace = false)
    {
        if (replace)
        {
            RemoveModifier(key);
        }

        StatModifier statModifier = GetModifier(key);

        if (statModifier == null)
        {
            statModifier = new StatModifier(key, value);

            modifiers.Add(statModifier);
            modifierLookup.Add(key, statModifier);
        }
    }


    public void RemoveModifier(string key)
    {
        StatModifier statModifier = GetModifier(key);

        if (statModifier != null)
        {
            modifiers.Remove(statModifier);
            modifierLookup.Remove(key);
        }
    }

    public void Reset()
    {
        modifiers.Clear();
        modifierLookup.Clear();
    }
}
