﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BaseStats : ModifiableStats
{
    [SerializeField] private StatsName statsName = default;
    [SerializeField] private int statsAmount = default;
    public bool isUpgradable = true;

    public StatsName StatsName => statsName;

    protected override int GetStatsAmount()
    {
        return statsAmount;
    }

    public void Upgrade(int value)
    {
        if(isUpgradable)
        {
            statsAmount += value;
        }
    }

    public string GetDescription()
    {
        string amount = finalValue > 0 ? "+" + finalValue.ToString("N0") : finalValue.ToString("N0");

        return StatsName.ToString() + amount;
    }
}
