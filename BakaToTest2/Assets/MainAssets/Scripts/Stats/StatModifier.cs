﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class StatModifier
{
    public readonly int value;
    public readonly string key;


    public StatModifier(string key, int value)
    {
        this.value = value;
        this.key = key;
    }
}
