﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SubjectStats : ModifiableStats
{
    private const int MaximumScoreValue = 100;

    [SerializeField] private Subjects subject = default;
    [Range(0, 100), SerializeField] private int score = default;
    [SerializeField] private bool isHidden = default;

    public bool IsHidden => isHidden;
    public Subjects Subject => subject;
    public int Score => score;
    public float lastSubjectFatigue { get; set; }
    public bool IsKnockedOut { get; set; }


    protected override int GetStatsAmount()
    {
        return Score;
    }

    public void UnhideStats()
    {
        isHidden = false;
    }

    public void HideStats()
    {
        isHidden = true;
    }

    public Sprite GetIcon()
    {
        return IconLoader.GetSubjectIcon(subject);
    }

    public string GetDescription()
    {
        return subject.ToString() + "+" + finalValue.ToString("N0");
    }

    public float GetBaseNormalizedResistanceValue()
    {
        return (float)score / MaximumScoreValue;
    }

    public float GetEffectiveNormalizedResistanceValue()
    {
        return (float)finalValue / MaximumScoreValue;
    }

    public float GetSubjectFatigue()
    {
        return lastSubjectFatigue >= 0f ? lastSubjectFatigue : 0f;
    }

    public void ResetSubjectFatigue()
    {
        lastSubjectFatigue = 0f;
        IsKnockedOut = false;
    }

    public bool IsOverfatigue()
    {
        return GetSubjectFatigue() >= score;
    }

    public string GetGrade()
    {
        return GradeChecker.GetGrade(score);
    }
    public void AddScore(int amount)
    {
        score += amount;
    }
}
