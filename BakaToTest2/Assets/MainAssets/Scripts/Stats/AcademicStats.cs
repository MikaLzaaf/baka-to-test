﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AcademicStats 
{

    public List<SubjectStats> subjectsStats;

    private Dictionary<Subjects, SubjectStats> statsLookup;
    private Dictionary<Subjects, SubjectStats> StatsLookup
    {
        get
        {
            if (statsLookup == null)
            {
                statsLookup = new Dictionary<Subjects, SubjectStats>();

                foreach (SubjectStats stat in subjectsStats)
                {
                    statsLookup.Add(stat.Subject, stat);
                }
            }

            return statsLookup;
        }
    }


    public SubjectStats GetSubjectStats(Subjects subjectName)
    {
        StatsLookup.TryGetValue(subjectName, out SubjectStats stats);

        return stats;
    }


    public int GetStatsValue(Subjects subjectName)
    {
        StatsLookup.TryGetValue(subjectName, out SubjectStats stats);

        if (stats != null)
        {
            return stats.finalValue;
        }

        return 0;
    }

    public void UpdateStatsModifiers(string modifierKey, AcademicStats otherStats, bool replaceOldMod = false)
    {
        List<SubjectStats> otherSubjectsStats = otherStats.subjectsStats;

        if (otherSubjectsStats == null)
            return;
        // Add other stats to this stats
        for (int i = 0; i < otherSubjectsStats.Count; i++)
        {
            SubjectStats currentStat = GetSubjectStats(otherSubjectsStats[i].Subject);

            if (currentStat != null)
            {
                currentStat.AddModifier(modifierKey, otherSubjectsStats[i].finalValue, replaceOldMod);
            }
        }
    }

    public bool HasStats()
    {
        if (subjectsStats == null)
            return false;

        return subjectsStats.Count > 0;
    }

    public string GetOverallGrade()
    {
        int totalScore = GetOverallScore();

        return GradeChecker.GetGrade(totalScore);
    }

    public int GetOverallScore()
    {
        int totalScore = 0;

        for (int i = 0; i < subjectsStats.Count; i++)
        {
            totalScore += subjectsStats[i].Score;
        }

        totalScore = Mathf.RoundToInt((float)totalScore / subjectsStats.Count);

        return totalScore;
    }
}
