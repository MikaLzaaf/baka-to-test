﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public abstract class SkillMinigame : MonoBehaviour
{
    public Difficulty difficulty;
    public float duration;
    public bool showTimer = false;

    [Header("Presets")]
    public MinigameDifficultyPresets[] difficultyPresets;

    protected MinigameDifficultyPresets selectedDifficultyPreset;
    protected float elapsedTime;
    protected bool isPlaying;

    protected const string PerfectResultSuffix = "Perfect!";
    protected const string GoodResultSuffix = "Good";
    protected const string BadResultSuffix = "Bad";


    public bool canHandleInput { get; set; }
    public abstract string Id { get;}

    


    public abstract void Initialize(Action<int> callbackOnFinished, Difficulty skillDifficulty);


    public abstract void StartMinigame();

    public abstract void EndMinigame();

    public abstract void Replay();


    public MinigameDifficultyPresets GetPreset(Difficulty difficultySelected)
    {
        for(int i = 0; i < difficultyPresets.Length; i++)
        {
            if(difficultyPresets[i].difficulty == difficultySelected)
            {
                return difficultyPresets[i];
            }
        }

        return null;
    }
}
