﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.InputSystem;
using System;
using DG.Tweening;

public class ButtonPromptMinigame : BaseMinigame
{
    public DestroyMethod destroyMethod;
    public GameInputButtons buttonUsed;

    public TimeScaleController timeScaleController;
    public Image buttonIcon;
    public Image indicator;

    public float inputTriggeredVisualDuration = 0.1f;
    public float timeSlowScale = 0.4f;
    public Vector3 buttonPunchScale;

    public bool useSlowMotion = false;

    private Action<int> callbackOnFinished;

    private float elapsedTime = 0f;

    private bool isTriggered = false;

    private bool isPlaying = false;

    public bool canHandleInput { get; set; }
    public bool hasParentMinigame { get; set; }


    public override void Initialize(Action<int> action)
    {
        if (action != null)
        {
            callbackOnFinished = action;
        }

        ResetIndicator();

        elapsedTime = 0f;

        isTriggered = false;
    }
    

    public override void StartMinigame()
    {
        isPlaying = true;

        canHandleInput = true;

        ToggleSlowMotion(useSlowMotion);
    }

    public override void EndMinigame()
    {
        isPlaying = false;
    }


    public override void Replay(bool startImmediately = true)
    {
        ResetIndicator();

        elapsedTime = 0f;

        isTriggered = false;

        if(startImmediately)
            StartMinigame();
    }    


    private void Update()
    {
//#if UNITY_EDITOR
//        if (Input.GetKeyDown(KeyCode.R) && !hasParentMinigame)
//        {
//            if (elapsedTime > 0)
//            {
//                Replay();
//            }
//            else
//            {
//                Initialize(null);
//                StartMinigame();
//            }

//            return;
//        }
//#endif
        if (!isPlaying)
            return;

        if (elapsedTime <= duration && !isTriggered)
        {
            elapsedTime += timeScaleController.deltaTime;

            UpdateIndicator();

            UpdateInput();
        }
        else if (elapsedTime > duration && !isTriggered)
        {
            StartCoroutine(ShowTriggered(false));
        }
    }

    private void ResetIndicator()
    {
        indicator.fillAmount = 1f;

        string buttonName = ButtonNamePrefix + buttonUsed.ToString();

        buttonIcon.sprite = GetButtonIcon(buttonName);
    }


    private void UpdateIndicator()
    {
        float amountToReduce = elapsedTime / duration;

        amountToReduce = Mathf.Min(amountToReduce, 1);

        indicator.fillAmount = 1 - amountToReduce;
    }


    private void UpdateInput()
    {
        if (!canHandleInput)
            return;

        //if (GameInput.instance.GetButtonInput(triggerButton))
        //{
        //    Debug.Log("GREATTTT!!!");
        //    StartCoroutine(ShowTriggered(true));
        //}
        //else if (GameInput.instance.GetAnyInputExcept(triggerButton))
        //{
        //    Debug.Log("Hmph");
        //    StartCoroutine(ShowTriggered(false));
        //}
    }


    private IEnumerator ShowTriggered(bool hasInput)
    {
        isTriggered = true;

        if (hasInput)
        {
            buttonIcon.transform.parent.DOPunchScale(buttonPunchScale, inputTriggeredVisualDuration, 1, 0);
        }

        int eventValue = hasInput ? 0 : -1;

        yield return new WaitForSeconds(inputTriggeredVisualDuration);

        callbackOnFinished?.Invoke(eventValue);

        ToggleSlowMotion(false);

        if (destroyMethod == DestroyMethod.Deactivate)
        {
            gameObject.SetActive(false);
        }
        else if (destroyMethod == DestroyMethod.Destroy)
        {
            Destroy(gameObject);
        }
    }


    private void ToggleSlowMotion(bool active)
    {
        Time.timeScale = active ? timeSlowScale : 1f;
    }


    public void Restart(InputAction.CallbackContext context)
    {
#if UNITY_EDITOR
        if (context.performed)
        {
            if (elapsedTime > 0)
            {
                Replay();
            }
            else
            {
                Initialize(null);
                StartMinigame();
            }
        }
#endif
    }


    public void RegisterInput(InputAction.CallbackContext context)
    {
        if (!isPlaying)
            return;

        if (context.performed)
        {
            StartCoroutine(ShowTriggered(true));
        }
    }


    public void FalseInput(InputAction.CallbackContext context)
    {
        if (!isPlaying)
            return;

        if (context.performed)
        {
            StartCoroutine(ShowTriggered(false));
        }
    }
}
