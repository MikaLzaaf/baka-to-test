﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.InputSystem;
using DG.Tweening;

public class ButtonMashingMinigame : BaseMinigame
{
    public PlayerInput actionInput;
    public DestroyMethod destroyMethod;
    public GameInputButtons buttonUsed;

    public Image buttonIcon;
    public Image indicator;

    public float inputTriggeredVisualDuration = 0.1f;
    public int valuePerButtonMashed = 1;
    public Vector3 buttonPunchScale;
    public float totalValue = 0f;

    public bool useRandomTriggerButton = false;
    public bool handleInputFromStart = false;

    private Action<int> callbackOnButtonPressed;

    private float elapsedTime = 0f;

    private bool isTriggered = false;
    private bool isPlaying = false;

    public bool canHandleInput { get; set; }
    public bool hasParentMinigame { get; set; }


    public override void Initialize(Action<int> action)
    {
        if (action != null)
        {
            callbackOnButtonPressed = action;
        }

        ResetIndicator();

        if(useRandomTriggerButton)
        {
            RandomButtonIconName();
        }
        else
        {
            triggerButton = buttonUsed;
        }

        elapsedTime = 0f;

        isTriggered = false;
    }

    public override void StartMinigame()
    {
        if(BattleController.IsInBattle)
        {
            BattleUIManager.instance.SetInputActiveState(false);
        }

        isPlaying = true;

        canHandleInput = true;
    }

    public override void EndMinigame()
    {
        isPlaying = false;

        if (BattleController.IsInBattle)
        {
            BattleUIManager.instance.SetInputActiveState(true);
        }
    }

    public override void Replay(bool startImmediately = true)
    {
        ResetIndicator();

        elapsedTime = 0f;

        isTriggered = false;

        if(startImmediately)
            StartMinigame();
    }


    private void Update()
    {
//#if UNITY_EDITOR
//        if (Input.GetKeyDown(KeyCode.R) && !hasParentMinigame)
//        {
//            if (elapsedTime > 0)
//            {
//                Replay();
//            }
//            else
//            {
//                Initialize(null);
//                StartMinigame();
//            }

//            return;
//        }
//#endif

        if (!isPlaying)
            return;

        if (elapsedTime <= duration && !isTriggered)
        {
            elapsedTime += Time.deltaTime;

            UpdateIndicator();

            UpdateInput();
        }
        else if (elapsedTime > duration && !isTriggered)
        {
            EndMinigame();
        }
    }

    private void ResetIndicator()
    {
        if (indicator != null)
        {
            indicator.fillAmount = 1f;
        }

        string buttonName = ButtonNamePrefix + buttonUsed.ToString();

        buttonIcon.sprite = GetButtonIcon(buttonName);

        totalValue = 0f;
    }

    private void UpdateIndicator()
    {
        if (indicator == null)
            return;

        float amountToReduce = elapsedTime / duration;

        amountToReduce = Mathf.Min(amountToReduce, 1);

        indicator.fillAmount = 1 - amountToReduce;
    }


    private void UpdateInput()
    {
        if (!canHandleInput)
            return;

        //if (GameInput.instance.GetButtonInput(triggerButton))
        //{
        //    Debug.Log("GREATTTT!!!");
        //    StartCoroutine(ShowTriggered(true));
        //}
    }


    private IEnumerator ShowTriggered(bool hasInput)
    {
        canHandleInput = false;

        if (hasInput)
        {
            buttonIcon.transform.parent.DOPunchScale(buttonPunchScale, inputTriggeredVisualDuration, 1, 0);
        }

        callbackOnButtonPressed?.Invoke(valuePerButtonMashed);

        totalValue += valuePerButtonMashed;

        yield return new WaitForSeconds(inputTriggeredVisualDuration);

        canHandleInput = true;
    }


    public void SetInputActiveState(bool isPlayable)
    {
        switch (isPlayable)
        {
            case false:
                actionInput.DeactivateInput();
                break;

            case true:
                actionInput.ActivateInput();
                break;
        }
    }


    public void Restart(InputAction.CallbackContext context)
    {
#if UNITY_EDITOR
        if (context.performed)
        {
            if (elapsedTime > 0)
            {
                Replay();
            }
            else
            {
                Initialize(null);
                StartMinigame();
            }
        }
#endif
    }


    public void RegisterInput(InputAction.CallbackContext context)
    {
        if (!isPlaying)
            return;

        if (context.performed)
        {
            StartCoroutine(ShowTriggered(true));
        }
    }
}
