﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.InputSystem;



public class TimingButtonPressMinigame : BaseMinigame
{
    public DestroyMethod destroyMethod;
    public GameInputButtons buttonUsed;


    public TimeScaleController timeScaleController;
    public Image buttonIcon;
    public Image indicator;

    public float inputTriggeredVisualDuration = 0.1f;
    public float timeSlowScale = 0.4f;
    public Vector3 buttonPunchScale;


    public bool useSlowMotion = false;

    [Header("Scaler")]
    public Image acceptableRadius;
    public float acceptableRangeMax = 0.5f;
    public float criticalRangeMax = 0.2f;
    public float scaleInitialSize = 2f;
    public float scaleFinalSize = 0.5f;

    private Action<int> callbackOnFinished;

    private float elapsedTime = 0f;
    private float differenceInSize = 0f;

    private bool isTriggered = false;
    private bool isPlaying = false;

    public bool canHandleInput { get; set; }
    public bool hasParentMinigame { get; set; }


    public override void Initialize(Action<int> action)
    {
        if (action != null)
        {
            callbackOnFinished = action;
        }

        ResetIndicator();

        elapsedTime = 0f;

        isTriggered = false;
    }

    public override void StartMinigame()
    {
        isPlaying = true;

        canHandleInput = true;

        ToggleSlowMotion(useSlowMotion);
    }

    public override void EndMinigame()
    {
        isPlaying = false;
    }


    public override void Replay(bool startImmediately = true)
    {
        ResetIndicator();

        elapsedTime = 0f;

        isTriggered = false;

        if(startImmediately)
            StartMinigame();
    }


    private void Update()
    {
//#if UNITY_EDITOR
//        if (Input.GetKeyDown(KeyCode.R) && !hasParentMinigame)
//        {
//            if (elapsedTime > 0)
//            {
//                Replay();
//            }
//            else
//            {
//                Initialize(null);
//                StartMinigame();
//            }

//            return;
//        }
//#endif

        if (!isPlaying)
            return;

        if (elapsedTime <= duration && !isTriggered)
        {
            elapsedTime += timeScaleController.deltaTime;

            UpdateIndicator();

            UpdateInput();
        }
        else if (elapsedTime > duration && !isTriggered)
        {
            StartCoroutine(ShowTriggered(false));
        }
    }

    private void ResetIndicator()
    {
        indicator.transform.localScale = Vector3.one * scaleInitialSize;

        differenceInSize = Mathf.Abs(scaleFinalSize - scaleInitialSize);

        float acceptableSize = scaleInitialSize - (differenceInSize * acceptableRangeMax);

        acceptableRadius.transform.localScale = new Vector2(acceptableSize, acceptableSize);


        string buttonName = ButtonNamePrefix + buttonUsed.ToString();

        buttonIcon.sprite = GetButtonIcon(buttonName);
    }

    private void UpdateIndicator()
    {
        float amountToReduce = (elapsedTime / duration) * differenceInSize;

        Vector3 reduceScale = new Vector3(amountToReduce, amountToReduce, amountToReduce);

        indicator.transform.localScale = Vector3.one * scaleInitialSize - reduceScale;
    }


    private void UpdateInput()
    {
        if (!canHandleInput)
            return;

        //if (GameInput.instance.GetButtonInput(triggerButton))
        //{
        //    StartCoroutine(ShowTriggered(true));
        //}
        //else if (GameInput.instance.GetAnyInputExcept(triggerButton))
        //{
        //    Debug.Log("Hmph");
        //    StartCoroutine(ShowTriggered(false));
        //}
    }


    private IEnumerator ShowTriggered(bool hasInput)
    {
        isTriggered = true;

        if (hasInput)
        {
            buttonIcon.transform.parent.DOPunchScale(buttonPunchScale, inputTriggeredVisualDuration, 1, 0);
        }

        int eventValue = hasInput ? CheckRangeOfInput() : -1;

        yield return new WaitForSeconds(inputTriggeredVisualDuration);

        callbackOnFinished?.Invoke(eventValue);

        ToggleSlowMotion(false);

        if (destroyMethod == DestroyMethod.Deactivate)
        {
            gameObject.SetActive(false);
        }
        else if (destroyMethod == DestroyMethod.Destroy)
        {
            Destroy(gameObject);
        }
    }


    private int CheckRangeOfInput()
    {
        int index;
        float inputTime = 1 - (elapsedTime / duration);

        if (inputTime <= criticalRangeMax)
        {
            Debug.Log("GREATTTT!!!");
            index = 1;
        }
        else if (inputTime <= acceptableRangeMax)
        {
            Debug.Log("Ok!");
            index = 0;
        }
        else
        {
            Debug.Log("Hmph");
            index = -1;
        }

        return index;
    }


    private void ToggleSlowMotion(bool active)
    {
        Time.timeScale = active ? timeSlowScale : 1f;
    }


    public void Restart(InputAction.CallbackContext context)
    {
#if UNITY_EDITOR
        if (context.performed)
        {
            if (elapsedTime > 0)
            {
                Replay();
            }
            else
            {
                Initialize(null);
                StartMinigame();
            }
        }
#endif
    }


    public void RegisterInput(InputAction.CallbackContext context)
    {
        if (!isPlaying)
            return;

        if (context.performed)
        {
            StartCoroutine(ShowTriggered(true));
        }
    }


    public void FalseInput(InputAction.CallbackContext context)
    {
        if (!isPlaying)
            return;

        if (context.performed)
        {
            StartCoroutine(ShowTriggered(false));
        }
    }
}
