﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Random = UnityEngine.Random;


public abstract class BaseMinigame : MonoBehaviour
{
    protected const string ButtonIconsDirectoryInResources = "Images/Buttons/";
    protected const string ButtonNamePrefix = "Button_";

    public string id;
    public float duration;
    public Difficulty difficulty;

    protected GameInputButtons triggerButton = default;
    

    public abstract void Initialize(Action<int> callbackOnFinished);

    public abstract void StartMinigame();

    public abstract void EndMinigame();

    public abstract void Replay(bool startImmediately = true);


    protected Sprite GetButtonIcon(string buttonName)
    {
        Sprite icon = Resources.Load<Sprite>(ButtonIconsDirectoryInResources + buttonName);

        return icon;
    }


    protected string RandomButtonIconName()
    {
        int index = Random.Range(3, 7); // Index of GameInputButtons : Triangle to Square

        triggerButton = (GameInputButtons)index;

        return triggerButton.ToString();
    }


}
