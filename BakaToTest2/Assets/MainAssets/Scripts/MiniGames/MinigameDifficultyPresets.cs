﻿
[System.Serializable]
public class MinigameDifficultyPresets
{
    public Difficulty difficulty;
    public float duration;
    public int maxValue;

    public float goodResultThreshold = 0.7f;
    public float greatResultThreshold = 0.9f;
}
