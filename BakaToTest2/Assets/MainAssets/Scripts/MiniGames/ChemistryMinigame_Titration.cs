﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.InputSystem;
using TMPro;
using Random = UnityEngine.Random;
using DG.Tweening;

public class ChemistryMinigame_Titration : SkillMinigame
{
    
    [Header("Minigames")]
    public ButtonMashingMinigame[] minigames;

    [Header("Indicators")]
    public Image progressIndicator;
    public Image timeIndicator;
    public TextMeshProUGUI resultText;

    [Header("Input")]
    public PlayerInput actionInput;

    private float maxValue;
    private float totalValue;

    public override string Id => gameObject.name;

    private Action<int> callbackOnFinished;
    private const string MinigameControlsInputMap = "Minigame Controls";
  

    public override void Initialize(Action<int> action, Difficulty skillDifficulty)
    {
        if(action != null)
        {
            callbackOnFinished = action;
        }

        difficulty = skillDifficulty;

        SetDifficulty();

        ResetIndicator();

        InitMinigames();

        //SetInputActiveState(true);

        elapsedTime = 0f;
    }


    public override void StartMinigame()
    {
        isPlaying = true;

        canHandleInput = true;

        for (int i = 0; i < minigames.Length; i++)
        {
            minigames[i].StartMinigame();
        }
    }

    public override void EndMinigame()
    {
        if (!isPlaying)
            return;

        isPlaying = false;

        for (int i = 0; i < minigames.Length; i++)
        {
            minigames[i].EndMinigame();
        }

        int result;

        float resultValue = totalValue;

        if (resultValue >= selectedDifficultyPreset.greatResultThreshold)
        {
            result = 1;
        }
        else if (resultValue >= selectedDifficultyPreset.goodResultThreshold)
        {
            result = 2;
        }
        else
        {
            result = 3;
        }

        StartCoroutine(ShowResultIndicator(result));

        //callbackOnFinished?.Invoke(result);
    }

    public override void Replay()
    {
        for (int i = 0; i < minigames.Length; i++)
        {
            minigames[i].Replay(false);
        }

        ResetIndicator();

        elapsedTime = 0f;

        StartMinigame();
    }

    private void Update()
    {
//#if UNITY_EDITOR
//        if (Input.GetKeyDown(KeyCode.R))
//        {
//            if (elapsedTime > 0)
//            {
//                Replay();
//            }
//            else
//            {
//                Initialize(null, difficulty);
//                StartMinigame();
//            }

//            return;
//        }
//#endif

        if (!isPlaying)
            return;

        if (elapsedTime <= duration)
        {
            elapsedTime += Time.deltaTime;

            UpdateTimeIndicator();
        }
        else if (elapsedTime > duration)
        {
            EndMinigame();
        }
    }


    private void SetDifficulty()
    {
        SelectDifficultyPreset();

        duration = selectedDifficultyPreset.duration;
        maxValue = selectedDifficultyPreset.maxValue;
    }


    private void SelectDifficultyPreset()
    {
        for (int i = 0; i < difficultyPresets.Length; i++)
        {
            if (difficultyPresets[i].difficulty == difficulty)
            {
                selectedDifficultyPreset = difficultyPresets[i];
                return;
            }
        }
    }

    private void InitMinigames()
    {
        if (minigames.Length == 0)
            return;

        for (int i = 0; i < minigames.Length; i++)
        {
            minigames[i].Initialize(AddTotalValue);
            minigames[i].hasParentMinigame = true;
        }
    }

    private void ResetIndicator()
    {
        totalValue = 0f;

        if (progressIndicator != null)
        {
            progressIndicator.fillAmount = 0f;
        }

        resultText.gameObject.SetActive(false);

        timeIndicator.gameObject.SetActive(showTimer);

        if (timeIndicator != null && showTimer)
        {
            timeIndicator.fillAmount = 1f;
        }
    }


    private void UpdateProgressIndicator()
    {
        if (progressIndicator == null)
            return;

        float amountToAdd = totalValue / maxValue;

        amountToAdd = Mathf.Min(amountToAdd, 1);

        progressIndicator.fillAmount = amountToAdd;
    }


    private void UpdateTimeIndicator()
    {
        if (timeIndicator == null || !showTimer)
            return;

        float amountToReduce = elapsedTime / duration;

        amountToReduce = Mathf.Min(amountToReduce, 1);

        timeIndicator.fillAmount = 1 - amountToReduce;
    }


    private void AddTotalValue(int value)
    {
        totalValue += value;

        UpdateProgressIndicator();
    }


    private IEnumerator ShowResultIndicator(int result)
    {
        if (resultText == null)
            yield break;

        progressIndicator.fillAmount = 0f;

        if (result == 1)
        {
            resultText.text = PerfectResultSuffix;
            resultText.color = Color.white;
        }
        else if(result == 2)
        {
            resultText.text = GoodResultSuffix;
            resultText.color = Color.blue;
        }
        else if(result == 3)
        {
            resultText.text = BadResultSuffix;
            resultText.color = Color.red;
        }

        resultText.gameObject.SetActive(true);

        yield return new WaitForSeconds(0.5f);

        resultText.gameObject.SetActive(false);

        callbackOnFinished?.Invoke(result);
    }


    public void OnRestart(InputAction.CallbackContext context)
    {
#if UNITY_EDITOR
        if (context.performed)
        {
            if (elapsedTime > 0)
            {
                Replay();
            }
            else
            {
                Initialize(null, difficulty);
                StartMinigame();
            }
        }
#endif
    }

    //public void SetInputActiveState(bool isPlayable)
    //{
    //    switch (isPlayable)
    //    {
    //        case false:
    //            actionInput.DeactivateInput();
    //            ToggleInputActionMap(MinigameControlsInputMap, false);
    //            break;

    //        case true:
    //            ToggleInputActionMap(MinigameControlsInputMap, true);
    //            actionInput.ActivateInput();
    //            break;
    //    }
    //}

    //public void ToggleInputActionMap(string actionMapName, bool active)
    //{
    //    if (actionInput == null)
    //        return;

    //    actionInput.SwitchCurrentActionMap(actionMapName);
    //}
}
