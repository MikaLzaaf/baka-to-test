﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class ChallengeInfo 
{
    [HideInInspector] public string id;
    public BattlePartyMember[] opponents;
    public IlmuCost[] ilmuRewards;
    public Item[] obtainableItems;

    public bool isGameOverBattle;
    public bool isClassBattle;
    public bool isCompleted;
    public bool isRequestedByOpponent;


    public ChallengeInfo() { }
    public ChallengeInfo(BattlePartyMember[] opponents)
    {
        this.opponents = opponents;

        SetIlmuRiskReward();

        SetObtainableItems();
    }

    private void SetIlmuRiskReward()
    {
        Dictionary<Subjects, IlmuCost> ilmuDropLookup = new Dictionary<Subjects, IlmuCost>();
        List<IlmuCost> totalIlmuDrops = new List<IlmuCost>();

        for (int i = 0; i < opponents.Length; i++)
        {
            GenericEnemy enemy = (GenericEnemy)opponents[i].GetCharacterPrefab();

            if (enemy == null)
                continue;

            var ilmuDrops = enemy.ilmuDrop;

            if (ilmuDrops == null || ilmuDrops.Length == 0)
                continue;

            for (int j = 0; j < ilmuDrops.Length; j++)
            {
                IlmuCost ilmuDrop = DeepCopier.DeepCopy(ilmuDrops[j]);

                ilmuDropLookup.TryGetValue(ilmuDrop.subject, out IlmuCost tempDrop);

                if (tempDrop != null)
                {
                    tempDrop.cost += ilmuDrop.cost;
                }
                else
                {
                    ilmuDropLookup.Add(ilmuDrop.subject, ilmuDrop);
                    totalIlmuDrops.Add(ilmuDrop);
                }
            }
        }

        ilmuRewards = totalIlmuDrops.ToArray();
    }

    private void SetObtainableItems()
    {
        Dictionary<string, Item> itemDropLookup = new Dictionary<string, Item>();
        List<Item> totalItemDrops = new List<Item>();

        for (int i = 0; i < opponents.Length; i++)
        {
            GenericEnemy enemy = (GenericEnemy)opponents[i].GetCharacterPrefab();

            if (enemy == null)
                continue;

            var itemDrops = enemy.itemDrop;

            if (itemDrops == null || itemDrops.Count == 0)
                continue;

            for (int j = 0; j < itemDrops.Count; j++)
            {
                Item itemDrop = itemDrops[j].GetItem();

                itemDropLookup.TryGetValue(itemDrop.itemValue.ItemID, out Item tempItem);

                if (tempItem != null)
                {
                    tempItem.amount += itemDrop.amount;
                }
                else
                {
                    itemDropLookup.Add(itemDrop.itemValue.ItemID, itemDrop);
                    totalItemDrops.Add(itemDrop);
                }
            }
        }

        obtainableItems = totalItemDrops.ToArray();
    }

#if UNITY_EDITOR
    public void SetupInfo()
    {
        if (opponents.Length == 0)
            return;

        SetupID();

        SetIlmuRiskReward();

        SetObtainableItems();
    }

    private void SetupID()
    {
        id = "";

        if (opponents == null)
            return;

        for(int i = 0; i < opponents.Length; i++)
        {
            id += opponents[i].GetCharacterName();
        }
    }
#endif

}
