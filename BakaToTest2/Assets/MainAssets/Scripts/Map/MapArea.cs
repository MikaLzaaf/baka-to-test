﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class MapArea : MonoBehaviour
{
    private const string FTPPrefix = "FTP_";
    private const string AreaEntrancePrefix = "ET_";

    public string areaName;
    public string parentAreaName;

    [Tooltip("This value is obtained from the MapScreenshotBoundary gameObject's position. Convert it's x & z position to Vector2 value.")]
    public Vector2 mapImageTranslation;

    [Tooltip("This value is obtained from the MapScreenshotBoundary gameObject's x scale. Make sure it is in 1:1 ratio with z size.")]
    public float mapXSize;
    [Tooltip("This value is obtained from the MapScreenshotBoundary gameObject's z scale. Make sure it is in 1:1 ratio with x size.")]
    public float mapZSize;


    public List<MapFastTravelPoint> fastTravelPoints = default; // Need to check its children first. If there is a MapFastTravelPoint component, then it will add itself 
    public MapEntranceTrigger[] areaEntrances = default; // Need to check its children first. If there is a MapExitTrigger component, then it will add itself 
    public bool switchToThisMapOnEnable = true;
    public bool unloadAreaOnExit = false;
    public bool isAdditiveLoadingArea = false;


    private Dictionary<string, MapEntranceTrigger> _entranceTriggerLookup;
    private Dictionary<string, MapEntranceTrigger> entranceTriggerLookup
    {
        get
        {
            if(_entranceTriggerLookup == null)
            {
                _entranceTriggerLookup = new Dictionary<string, MapEntranceTrigger>();

                if(areaEntrances != null && areaEntrances.Length > 0)
                {
                    for(int i = 0; i < areaEntrances.Length; i++)
                    {
                        _entranceTriggerLookup.Add(areaEntrances[i].entranceID, areaEntrances[i]);
                    }
                }
            }

            return _entranceTriggerLookup;
        }
    }

    private Dictionary<string, MapFastTravelPoint> _fastTravelPointsLookup;
    private Dictionary<string, MapFastTravelPoint> fastTravelPointsLookup
    {
        get
        {
            if (_fastTravelPointsLookup == null)
            {
                _fastTravelPointsLookup = new Dictionary<string, MapFastTravelPoint>();

                if (fastTravelPoints != null && fastTravelPoints.Count > 0)
                {
                    for (int i = 0; i < fastTravelPoints.Count; i++)
                    {
                        _fastTravelPointsLookup.Add(fastTravelPoints[i].pointName, fastTravelPoints[i]);
                    }
                }
            }

            return _fastTravelPointsLookup;
        }
    }



    private void Start()
    {
        ToggleEntranceTriggers(switchToThisMapOnEnable);

        if (MapManager.instance != null)
        {
            MapManager.instance.AddMapArea(this);

            if (switchToThisMapOnEnable)
                MapManager.instance.SwitchMapArea(this);
        }
    }

    private void OnDestroy()
    {
        if (MapManager.instance != null)
        {
            MapManager.instance.RemoveMapArea(this);
        }
    }

    public MapEntranceTrigger GetMapEntranceTrigger(string triggerId)
    {
        entranceTriggerLookup.TryGetValue(triggerId, out MapEntranceTrigger entranceTrigger);

        return entranceTrigger;
    }

    public MapFastTravelPoint GetFastTravelPoint(string fastTravelPointId)
    {
        fastTravelPointsLookup.TryGetValue(fastTravelPointId, out MapFastTravelPoint fastTravelPoint);

        return fastTravelPoint;
    }

    public Transform GetEntranceSpawner(string triggerId)
    {
        MapEntranceTrigger mapExitTrigger = GetMapEntranceTrigger(triggerId);

        if (mapExitTrigger != null)
        {
            return mapExitTrigger.enterSpawnPosition;
        }

        return null;
    }

    //public Vector3 GetFastTravelPointPosition(string fastTravelPointId)
    //{
    //    MapFastTravelPoint fastTravelPoint = GetFastTravelPoint(fastTravelPointId);

    //    if(fastTravelPoint != null)
    //    {
    //        return fastTravelPoint.transform.position;
    //    }

    //    return Vector3.zero;
    //}

    public void ToggleEntranceTriggers(bool active)
    {
        if (areaEntrances.Length == 0)
            return;

        for(int i = 0; i < areaEntrances.Length; i++)
        {
            areaEntrances[i].gameObject.SetActive(active);
        }
    }


#if UNITY_EDITOR
    [ContextMenu("Find Fast Travel Points")]
    public void FindFastTravelPoints()
    {
        List<MapFastTravelPoint> ftps = new List<MapFastTravelPoint>();

        for(int i = 0; i < transform.childCount; i++)
        {
            Transform child = transform.GetChild(i);

            if(child.TryGetComponent(out MapFastTravelPoint ftp))
            {
                if (string.IsNullOrEmpty(ftp.pointName))
                {
                    Debug.LogError("This FTP in " + areaName + " has no pointName!");
                }
                else
                {
                    ftps.Add(ftp);
                    child.gameObject.name = FTPPrefix + ftp.pointName;
                }
            }

            for(int j = 0; j < child.childCount; j++)
            {
                Transform grandChild = child.GetChild(j);

                if (grandChild.TryGetComponent(out MapFastTravelPoint ftp2))
                {
                    if (string.IsNullOrEmpty(ftp2.pointName))
                    {
                        Debug.LogError("This FTP in " + areaName + " has no pointName!");
                    }
                    else
                    {
                        ftps.Add(ftp2);
                        grandChild.gameObject.name = FTPPrefix + ftp2.pointName;
                    }
                }
            }
        }

        fastTravelPoints = ftps;
    }

    [ContextMenu("Find Area Entrances")]
    public void FindAreaEntrances()
    {
        List<MapEntranceTrigger> entrances = new List<MapEntranceTrigger>();

        for (int i = 0; i < transform.childCount; i++)
        {
            Transform child = transform.GetChild(i);

            if (child.TryGetComponent(out MapEntranceTrigger entranceTrigger))
            {
                if (string.IsNullOrEmpty(entranceTrigger.entranceID))
                {
                    Debug.LogError("This entrance trigger in " + areaName + " has no ID!");
                }
                else if(string.IsNullOrEmpty(entranceTrigger.destinationAreaName))
                {
                    Debug.LogError("This entrance trigger in " + areaName + " has no destinationAreaName!");
                }
                else if (string.IsNullOrEmpty(entranceTrigger.destinationEntranceID))
                {
                    Debug.LogError("This entrance trigger in " + areaName + " has no destination EntranceID!");
                }
                else
                {
                    entrances.Add(entranceTrigger);
                    child.gameObject.name = AreaEntrancePrefix + entranceTrigger.entranceID;
                }
            }

            for (int j = 0; j < child.childCount; j++)
            {
                Transform grandChild = child.GetChild(j);

                if (grandChild.TryGetComponent(out MapEntranceTrigger entranceTrigger2))
                {
                    if (string.IsNullOrEmpty(entranceTrigger2.entranceID))
                    {
                        Debug.LogError("This entrance trigger in " + areaName + " has no ID!");
                    }
                    else if (string.IsNullOrEmpty(entranceTrigger2.destinationAreaName))
                    {
                        Debug.LogError("This entrance trigger in " + areaName + " has no destinationAreaName!");
                    }
                    else if (string.IsNullOrEmpty(entranceTrigger2.destinationEntranceID))
                    {
                        Debug.LogError("This entrance trigger in " + areaName + " has no destination EntranceID!");
                    }
                    else
                    {
                        entrances.Add(entranceTrigger2);
                        grandChild.gameObject.name = AreaEntrancePrefix + entranceTrigger2.entranceID;
                    }
                }
            }
        }

        areaEntrances = entrances.ToArray();
        Debug.Log("Entrances found " + entrances.Count);
    }
#endif
}
