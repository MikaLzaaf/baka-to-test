﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Intelligensia.UI.HUD;


public class MapEntranceTrigger : ObjectInteractableBase
{
    [Header("Button Input Labels")]
    [SerializeField] private ButtonInputLabelId[] inputLabelsToDisplay = default;

    [Header("Trigger Value")]
    [Tooltip("The ID assigned to this trigger prefab.")]
    public string entranceID;
    [Tooltip("The position where the character will spawn upon entering.")]
    public Transform enterSpawnPosition;
    //public bool isInteractable = false;


    [Header("Destination")]
    [Tooltip("The name of the MapArea that the character will spawn at.")]
    public string destinationAreaName; // TODO : Create custom inspector for this. Easier for level design later
    [Tooltip("The ID of correspondent trigger located in the targeted MapArea.")]
    public string destinationEntranceID;
    [Tooltip("Tick this if the targeted MapArea is within a different ParentArea.")]
    public bool destinationInAnotherParentArea = false;
    [Tooltip("Tick this if the targeted MapArea needs to be loaded first whether additively or not.")]
    public bool destinationNeedsToLoad = false;
    [Tooltip("Tick this if the targeted MapArea scene needs to be loaded additively.")]
    public bool sceneIsAdditive = false;


    protected override ButtonInputLabelId[] GetButtonInputLabelIds()
    {
        return inputLabelsToDisplay;
    }


    public override void StartInteraction(Transform interactor)
    {
        base.StartInteraction(interactor);

        Activate();
    }

    protected void OnTriggerEnter(Collider otherCollider)
    {
        if (MapManager.instance.isLoadingNewArea)
            return;

        if (canInteract) // Need to interact first before can call the function
            return;

        Activate();

        gameObject.SetActive(false);
    }


    private void Activate()
    {
        if (string.IsNullOrEmpty(destinationAreaName))
            return;

        // Change scene if needed
        if (destinationNeedsToLoad)
        {
            MapManager.instance.LoadNewMainArea(destinationEntranceID, destinationAreaName, sceneIsAdditive);
        }
        else
        {
            // Change map only if not changing scene
            MapManager.instance.EnterNewMainArea(destinationEntranceID, destinationAreaName);
        }

        //Debug.Log(triggerId + " trigger id");
    }
   
}
