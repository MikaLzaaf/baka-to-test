﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class MapManager : MonoBehaviour
{
    public static MapManager instance;

    private const string MapDataSOInResources = "Data/MapDataSO";

    public MapArea currentMapArea;
    public MapUIController mapUIController;
    public MapDataSO mapDataSO;

    private List<MapArea> mapAreas = new List<MapArea>();
    private Dictionary<string, MapParentAreaInfo> mapParentAreaLookup;

    private string areaEntranceID;
    private string fastTravelPointID;

    private string savedMainAreaName;

    public bool isLoadingNewArea { get; private set; }

    public bool isFastTravelling { get; private set; }
    public bool canFastTravel { get; private set; } = true;


    private void Awake()
    {
        if (instance == null)
        {
            instance = this;

            if(mapDataSO == null)
            {
                Debug.LogError("There is no map data assigned yo!");
                return;
            }

            mapParentAreaLookup = new Dictionary<string, MapParentAreaInfo>();

            for (int i = 0; i < mapDataSO.mapParentAreaInfos.Count; i++)
            {
                mapParentAreaLookup.Add(mapDataSO.mapParentAreaInfos[i].parentAreaName, mapDataSO.mapParentAreaInfos[i]);
            }
        }
        else
            Destroy(gameObject);
    }


    public void LoadFromSaveData()
    {
        savedMainAreaName = GameDataManager.gameData.playerMapData.LoadMapName();
    }

    public void LoadNewMainArea(string destinationEntranceId, string newMainAreaName, bool isAdditiveLoad)
    {
        areaEntranceID = destinationEntranceId;

        LoadNewMainArea(newMainAreaName, isAdditiveLoad);
    }

    public void LoadNewMainArea(string newMainAreaName, bool isAdditiveLoad)
    {
        isLoadingNewArea = true;

        PlayerPartyManager.instance.selectedPlayer.transform.SetParent(this.transform, true);

        LoadingScreen.instance.ViewClosedEvent += OnLoadingScreenClosed;
        LoadingScreen.instance.LoadingNewSceneCompletedEvent += OnLoadingNewSceneCompleted;

        string previousAreaToUnload = currentMapArea.unloadAreaOnExit ? currentMapArea.areaName : "";

        LoadingScreen.instance.StartLoadingNewScene(newMainAreaName, previousAreaToUnload, isAdditiveLoad);
    }

    // This method is used by MapExitTrigger for entering indoor area & new parent area
    public void EnterNewMainArea(string destinationTriggerId, string newMainAreaName)
    {
        areaEntranceID = destinationTriggerId;

        EnterNewMainArea(newMainAreaName);
    }

    public void EnterNewMainArea(string newMainAreaName)
    {
        isLoadingNewArea = true;

        LoadingScreen.instance.ViewClosedEvent += OnLoadingScreenClosed;
        LoadingScreen.instance.LoadingNewSceneCompletedEvent += OnLoadingNewSceneCompleted;

        string previousAreaToUnload = currentMapArea.unloadAreaOnExit ? currentMapArea.areaName : "";

        LoadingScreen.instance.StartLoadingNewScene("", previousAreaToUnload, false);

        SwitchMapArea(newMainAreaName);
    }

    public void SwitchMapArea(string areaName)
    {
        if (mapAreas.Count == 0)
            return;

        for(int i = 0; i < mapAreas.Count; i++)
        {
            if (mapAreas[i].areaName == areaName)
            {
                SwitchMapArea(mapAreas[i]);
                break;
            }
        }
    }

    public void SwitchMapArea(MapArea map)
    {
        if (currentMapArea != null)
        {
            currentMapArea.ToggleEntranceTriggers(false);
        }

        currentMapArea = map;

        SetupMapUI(GetMapParentAreaInfo(currentMapArea.parentAreaName), GetCurrentMapMainAreaInfo());
    }

    public void AddMapArea(MapArea mapArea)
    {
        if (mapAreas == null)
            mapAreas = new List<MapArea>();

        if (!mapAreas.Contains(mapArea))
        {
            mapAreas.Add(mapArea);
        }

        if(!string.IsNullOrEmpty(savedMainAreaName))
        {
            if (mapArea.areaName == savedMainAreaName)
            {
                SwitchMapArea(mapArea);

                savedMainAreaName = "";
            }
        }
    }

    public void RemoveMapArea(MapArea map)
    {
        mapAreas.Remove(map);
    }

    private bool HasMapArea(string mapAreaName)
    {
        for (int i = 0; i < mapAreas.Count; i++)
        {
            if (mapAreas[i].areaName == mapAreaName)
                return true;
        }

        return false;
    }

    public MapMainAreaInfo GetCurrentMapMainAreaInfo()
    {
        if (currentMapArea != null)
        {
            return GetMapMainAreaInfo(currentMapArea.parentAreaName, currentMapArea.areaName);
        }
        
        return null;
    }

    public MapMainAreaInfo GetMapMainAreaInfo(string parentAreaName, string mainAreaName)
    {
        MapParentAreaInfo mapParentAreaInfo = GetMapParentAreaInfo(parentAreaName);

        if (mapParentAreaInfo != null)
        {
            return mapParentAreaInfo.GetMainAreaInfo(mainAreaName);
        }

        return null;
    }

    public MapParentAreaInfo GetMapParentAreaInfo(string parentAreaName)
    {
        mapParentAreaLookup.TryGetValue(parentAreaName, out MapParentAreaInfo mapFastTravelInfo);
        return mapFastTravelInfo;
    }

    public void SetupMapUI(MapParentAreaInfo mapParentAreaInfo, MapMainAreaInfo mapMainAreaInfo)
    {
        if (mapUIController != null)
        {
            mapUIController.SetMap(mapParentAreaInfo, mapMainAreaInfo, currentMapArea.mapImageTranslation, currentMapArea.mapXSize, currentMapArea.mapZSize);
        }
    }

    public void FastTravelTo(MapMainAreaInfo destinationMapMainArea, string destinationSubAreaName)
    {
        isFastTravelling = true;

        fastTravelPointID = destinationSubAreaName;

        if (HasMapArea(destinationMapMainArea.mainAreaName))
        {
            // Move within parent area only
            EnterNewMainArea(destinationMapMainArea.mainAreaName);
        }
        else
        {
            // Move to new parent area = need to load new scene and locations
            LoadNewMainArea(destinationMapMainArea.mainAreaName, destinationMapMainArea.isAdditiveArea);
        }
    }

    private void OnLoadingNewSceneCompleted(string newMainAreaName)
    {
        if (HasMapArea(newMainAreaName))
        {
            SwitchMapArea(newMainAreaName);
        }

        OnLoadingCompleted();
    }

    private void OnLoadingCompleted()
    {
        Debug.Log("Load complete!");

        Transform spawner = isFastTravelling ? currentMapArea.GetFastTravelPoint(fastTravelPointID).transform
            : currentMapArea.GetMapEntranceTrigger(areaEntranceID).enterSpawnPosition;

        if (spawner != null)
            PlayerPartyManager.instance.SpawnPlayerOnMap(spawner.position, spawner.rotation);
        else
            PlayerPartyManager.instance.SpawnPlayerOnMap(Vector3.zero, Quaternion.identity);
                     
        MainCameraController.instance.ToggleCameraBrain(true);
        MainCameraController.instance.CenteringThirdPersonCamera();

        LoadingScreen.instance.LoadingCompletedEvent -= OnLoadingCompleted;
        LoadingScreen.instance.LoadingNewSceneCompletedEvent -= OnLoadingNewSceneCompleted;

        isLoadingNewArea = false;
        isFastTravelling = false;

        fastTravelPointID = "";
        areaEntranceID = "";
    }

    private void OnLoadingScreenClosed()
    {
        LoadingScreen.instance.ViewClosedEvent -= OnLoadingScreenClosed;

        PlayerPartyManager.instance.TogglePlayerInput(true);

        currentMapArea.ToggleEntranceTriggers(true);
    }


#if UNITY_EDITOR

    /// <summary>
    /// This function is used to save the changes done on the map in the scene, including
    /// MapMainArea, MapSubArea, FastTravelPoint 
    /// </summary>
    [MenuItem("Custom/MapData/Save Current Scene Map Data")]
    private static void SaveMapData()
    {
        MapDataSO mapDataSO = Resources.Load<MapDataSO>(MapDataSOInResources);

        if(mapDataSO == null)
        {
            Debug.LogError("There is no MapDataSO inside the project!");
            return;
        }

        Debug.Log("Save Current Scene Map Data on MapDataSO");
        mapDataSO.SaveCurrentSceneMapData();
        EditorUtility.SetDirty(mapDataSO);
    }
#endif
}
