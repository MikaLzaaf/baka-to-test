﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using System;


public class MapUIController : MonoBehaviour
{
    public enum MapMode
    {
        Mini, Fullscreen, Deactivate
    }

    public event Action OnMapZoomEvent;


    [SerializeField] MiniMapIcon miniMapIconPrefab = default;
    [SerializeField] Vector2 terrainSize = default;
    [SerializeField] PlayerInput mapInput = default;
    [SerializeField] Vector3 translation = Vector3.zero;

    [Header("Fullscreen Map")]
    [SerializeField] MapFullViewController fullMapContainer = default;
    [SerializeField] RectTransform fullMapContentRectTransform = default;
#if !UNITY_EDITOR
    [SerializeField] float scrollSpeed = 10f;
    [SerializeField] float scrollInterval = 0.1f;
#endif
    [SerializeField] Vector2 zoomMapScaleOne = new Vector2(1000f, 1000f);
    [SerializeField] Vector2 zoomMapScaleTwo = new Vector2(2000f, 2000f);
    [SerializeField] Vector2 zoomMapScaleThree = new Vector2(3000f, 3000f);


    [Header("Mini Map")]
    [SerializeField] ViewController miniMapContainer = default;
    [SerializeField] RectTransform miniMapContentRectTransform = default;
    [SerializeField] Image miniMapImage = default;



    private Dictionary<MiniMapWorldObject, MiniMapIcon> miniMapWorldObjectsLookup = new Dictionary<MiniMapWorldObject, MiniMapIcon>();

    private Matrix4x4 miniMapTransformationMatrix;
    private Matrix4x4 fullMapTransformationMatrix;
    private Matrix4x4 markerMapTransformationMatrix;

    private MapMode currentMiniMapMode = MapMode.Mini;
    private MiniMapIcon playerMiniMapIcon;

    private Vector2 scrollMovement = Vector2.zero;
    private Vector2 lastScrollMovement = Vector2.zero;

    private float mapScrollLimit = 0f;
#if !UNITY_EDITOR
    private float elapsedTimeSinceLastScroll;
#endif
    private bool isScrollingMap = false;
    [SerializeField] private bool canDisplayMap = false;
    private int currentMapMainAreaIndex = 0;

    private const string MapImagesInResourcesPrefix = "Images/Maps/";

    private const int ClampedMapPositionCoefficient = 3;

    private int _zoomIndex = 0;
    private int zoomIndex
    {
        get
        {
            return _zoomIndex;
        }
        set
        {
            _zoomIndex = value;

            if (_zoomIndex > 2)
                _zoomIndex = 0;
        }
    }





    private void Update()
    {
        if (!canDisplayMap)
            return;

        if(currentMiniMapMode == MapMode.Mini)
        {
            UpdateMiniMapIcons();
            CenterMiniMapOnPlayer();
        }
        else if(currentMiniMapMode == MapMode.Fullscreen)
        {

            if(isScrollingMap)
            {
#if UNITY_EDITOR
                ScrollFullMap();
#else
                elapsedTimeSinceLastScroll += Time.deltaTime;

                if(elapsedTimeSinceLastScroll >= scrollInterval)
                {
                    ScrollFullMap();

                    elapsedTimeSinceLastScroll = 0f;
                }
#endif
            }


        }
    }

#region Map calculations
    public void RegisterMiniMapWorldObject(MiniMapWorldObject miniMapWorldObject, bool isPlayer = false)
    {
        var miniMapIcon = Instantiate(miniMapIconPrefab);
        miniMapIcon.transform.SetParent(miniMapContentRectTransform);
        miniMapIcon.SetIcon(miniMapWorldObject.Icon);
        miniMapIcon.SetColor(miniMapWorldObject.IconColor);
        miniMapIcon.SetText(miniMapWorldObject.Text);
        miniMapIcon.SetTextSize(miniMapWorldObject.TextSize);
        miniMapWorldObjectsLookup[miniMapWorldObject] = miniMapIcon;

        if (isPlayer)
            playerMiniMapIcon = miniMapIcon;
    }

    public void DestroyCorrespondingMiniMapIcon(MiniMapWorldObject miniMapWorldObject)
    {
        if (miniMapWorldObjectsLookup.TryGetValue(miniMapWorldObject, out MiniMapIcon icon))
        {
            miniMapWorldObjectsLookup.Remove(miniMapWorldObject);

            if (icon.gameObject != null)
            {
                Destroy(icon.gameObject);
            }
        }
    }

    private void CenterMiniMapOnPlayer()
    {
        if (playerMiniMapIcon != null)
        {
            float mapScale = miniMapContentRectTransform.transform.localScale.x;
            // we simply move the map in the opposite direction the player moved, scaled by the mapscale
            //contentRectTransform.transform.localPosition = (-playerMiniMapIcon.transform.localPosition * mapScale);
            miniMapContentRectTransform.anchoredPosition = (-playerMiniMapIcon.transform.localPosition * mapScale);
        }
    }

    private void UpdateMiniMapIcons()
    {
        // scale icons by the inverse of the mapscale to keep them a consistent size
        float iconScale = 2 / miniMapContentRectTransform.transform.localScale.x;
        foreach (var kvp in miniMapWorldObjectsLookup)
        {
            var miniMapWorldObject = kvp.Key;
            var miniMapIcon = kvp.Value;
            var mapPosition = WorldPositionToMapPosition(miniMapWorldObject.transform.position);

            miniMapIcon.RectTransform.anchoredPosition = mapPosition;
            var rotation = miniMapWorldObject.transform.rotation.eulerAngles;
            miniMapIcon.IconRectTransform.localRotation = Quaternion.AngleAxis(-rotation.y, Vector3.forward);
            miniMapIcon.IconRectTransform.localScale = Vector3.one * iconScale;
        }
    }



    private Vector2 WorldPositionToMapPosition(Vector3 worldPos)
    {
        var pos = new Vector2(worldPos.x, worldPos.z);

        if (currentMiniMapMode == MapMode.Mini)
        {
            return miniMapTransformationMatrix.MultiplyPoint3x4(pos);
        }
        else
        {
            return fullMapTransformationMatrix.MultiplyPoint3x4(pos);
        }
    }

    private void CalculateTransformationMatrix(MapMode mapMode)
    {
        var mapDimensions = mapMode == MapMode.Mini ? miniMapContentRectTransform.rect.size :
            fullMapContentRectTransform.rect.size;

        var terrainDimensions = terrainSize;

        // Use this when using terrain. Now no need
        //var terrainDimensions = new Vector2(terrain.terrainData.size.x, terrain.terrainData.size.z); 

        var scaleRatio = mapDimensions / terrainDimensions;

        Vector3 newTranslation = -translation * scaleRatio;
        // Use this when terrain's origin point is not similar to minimap anchor point. 
        //This example is used when terrain's origin point is on the lower left side while the minimap anchor point is on the center
        //var translation = -miniMapDimensions / 2; 

        if (mapMode == MapMode.Mini)
        {
            miniMapTransformationMatrix = Matrix4x4.TRS(newTranslation, Quaternion.identity, scaleRatio);
            //miniMapScaleRatio = scaleRatio;
        }
        else
        {
            fullMapTransformationMatrix = Matrix4x4.TRS(newTranslation, Quaternion.identity, scaleRatio);
            mapScrollLimit = mapDimensions.x / ClampedMapPositionCoefficient;
        }

        //  {scaleRatio.x,   0,           0,   translation.x},
        //  {  0,        scaleRatio.y,    0,   translation.y},
        //  {  0,            0,           0,            0},
        //  {  0,            0,           0,            0}
    }

    public void SwitchMiniMapImage(string mapName)
    {
        string spritePath = MapImagesInResourcesPrefix + mapName;
        //Debug.Log("Sprite path = " + spritePath);
        Sprite mapSprite = Resources.Load<Sprite>(spritePath);

        if (miniMapImage != null)
        {
            //miniMapImage.enabled = mapSprite != null;

            if (mapSprite != null)
            {
                miniMapImage.sprite = mapSprite;
            }
        }

        canDisplayMap = mapSprite != null;
    }

#endregion

    public void SetMap(MapParentAreaInfo mapParentAreaInfo, MapMainAreaInfo mapMainAreaInfo, Vector3 currentMapAreaTranslation, float currentMapAreaSizeX, float currentMapAreaSizeZ)
    {
        translation = currentMapAreaTranslation;
        terrainSize = new Vector2(currentMapAreaSizeX, currentMapAreaSizeZ);
 
        if (mapParentAreaInfo == null || mapMainAreaInfo == null)
            return;

        fullMapContainer.SetMap(mapParentAreaInfo, mapMainAreaInfo);

        SwitchMiniMapImage(mapMainAreaInfo.mainAreaName);

        currentMapMainAreaIndex = mapParentAreaInfo.mainAreaInfos.IndexOf(mapMainAreaInfo);

        if(canDisplayMap)
        {
            SetMapMode(MapMode.Mini);
        }
        else
        {
            SetMapMode(MapMode.Deactivate);
        }
    }

    private void SwitchMapImage(int index)
    {
        if(fullMapContainer != null)
        {
            fullMapContainer.SwitchMapImage(index);
        }
        
        if(currentMapMainAreaIndex == fullMapContainer.currentMapMainAreaIndex)
        {
            // Show player location on map
            TogglePlayerMiniMapIcon(true);
        }
        else
        {
            // Hide player location on map
            TogglePlayerMiniMapIcon(false);
        }
    }

    private void TogglePlayerMiniMapIcon(bool active)
    {
        if(playerMiniMapIcon != null)
        {
            playerMiniMapIcon.gameObject.SetActive(active);
        }
    }

    public void SetMapMode(MapMode mode)
    {
        switch (mode)
        {
            case MapMode.Mini:
                ToggleFullMap(false);
                ToggleMiniMap(true);

                foreach (var kvp in miniMapWorldObjectsLookup)
                {
                    var miniMapIcon = kvp.Value;
                    miniMapIcon.transform.SetParent(miniMapContentRectTransform);
                }

                TogglePlayerMiniMapIcon(true);

                currentMiniMapMode = MapMode.Mini;

                CalculateTransformationMatrix(currentMiniMapMode);

                UpdateMiniMapIcons();

                ToggleMapInput(false);
                break;

            case MapMode.Fullscreen:
                ToggleMiniMap(false);
                ToggleFullMap(true);

                foreach (var kvp in miniMapWorldObjectsLookup)
                {
                    var miniMapIcon = kvp.Value;
                    miniMapIcon.transform.SetParent(fullMapContainer.playerMapIconHolder);
                }

                currentMiniMapMode = MapMode.Fullscreen;

                CalculateTransformationMatrix(currentMiniMapMode);

                CenterFullMap();
#if !UNITY_EDITOR
                elapsedTimeSinceLastScroll = 0f;
#endif
                UpdateMiniMapIcons();

                ToggleMapInput(true);
                break;

            case MapMode.Deactivate:
                ToggleMiniMap(false);
                ToggleFullMap(false);

                break;
        }
    }

    public void ToggleFullMap(bool active)
    {
        if(fullMapContainer != null)
        {
            if(active)
            {
                fullMapContainer.Show(MapManager.instance.canFastTravel);
            }
            else
            {
                fullMapContainer.Close();
            }
        }
    }

    public void ToggleMiniMap(bool active)
    {
        if (miniMapContainer != null)
        {
            if (active)
            {
                miniMapContainer.Show();
            }
            else
            {
                miniMapContainer.Close();
            }
        }
    }

    private void ToggleMapInput(bool active)
    {
        if(mapInput != null)
        {
            mapInput.enabled = active;
        }
    }

    private void ScrollFullMap()
    {
        if(fullMapContentRectTransform != null)
        {
#if UNITY_EDITOR

            Vector2 scrollValue = lastScrollMovement == scrollMovement ? Vector2.zero : scrollMovement;

            lastScrollMovement = scrollMovement;
#else
            Vector2 scrollValue = scrollMovement * scrollSpeed;
#endif

            Vector2 clampedPosition = new Vector2(Mathf.Clamp(fullMapContentRectTransform.anchoredPosition.x + scrollValue.x, -mapScrollLimit, mapScrollLimit),
                Mathf.Clamp(fullMapContentRectTransform.anchoredPosition.y + scrollValue.y, -mapScrollLimit, mapScrollLimit));

            fullMapContentRectTransform.anchoredPosition = clampedPosition;
        }
    }

    private void ZoomMap()
    {
        if (fullMapContentRectTransform == null)
            return;

        if(zoomIndex == 0)
        {
            fullMapContentRectTransform.sizeDelta = zoomMapScaleOne;
        }
        else if (zoomIndex == 1)
        {
            fullMapContentRectTransform.sizeDelta = zoomMapScaleTwo;
        }
        else if (zoomIndex == 2)
        {
            fullMapContentRectTransform.sizeDelta = zoomMapScaleThree;
        }

        CalculateTransformationMatrix(currentMiniMapMode);

        ScrollFullMap();
        UpdateMiniMapIcons();

        OnMapZoomEvent?.Invoke();
    }

    private void CenterFullMap()
    {
        if(fullMapContentRectTransform != null)
        {
            fullMapContentRectTransform.anchoredPosition = Vector2.zero;
        }
    }

    #region Marker
    public void MoveLocationMarkerOnFullMap(MapFastTravelPointInfo mapSubAreaInfo)
    {
        if(fullMapContainer != null && mapSubAreaInfo != null)
        {
            Vector3 location = MarkerPositionToMapPosition(mapSubAreaInfo.markerLocation);

            fullMapContainer.MoveLocationMarker(mapSubAreaInfo.showLocationOnMap, location);
        }

        if(mapSubAreaInfo == null)
        {
            fullMapContainer.MoveLocationMarker(false, Vector2.zero);
        }
    }

    private Vector2 MarkerPositionToMapPosition(Vector3 worldPos)
    {
        var pos = new Vector2(worldPos.x, worldPos.z);

        return markerMapTransformationMatrix.MultiplyPoint3x4(pos);
    }

    public void CalculateMarkerTransformationMatrix(MapMainAreaInfo currentMainMapAreaInfo)
    {
        var mapDimensions = fullMapContentRectTransform.rect.size;

        var terrainDimensions = currentMainMapAreaInfo != null ? new Vector2 (currentMainMapAreaInfo.mapXSize, currentMainMapAreaInfo.mapZSize) :
            terrainSize;

        var scaleRatio = mapDimensions / terrainDimensions;

        Vector3 newTranslation = currentMainMapAreaInfo != null ? -currentMainMapAreaInfo.mapImageTranslation * scaleRatio :
            -translation * scaleRatio;

        markerMapTransformationMatrix = Matrix4x4.TRS(newTranslation, Quaternion.identity, scaleRatio);
        mapScrollLimit = mapDimensions.x / ClampedMapPositionCoefficient;
    }
#endregion

    #region Input

#if UNITY_EDITOR
    public void OnDragStarted(InputAction.CallbackContext callbackContext)
    {
        if(callbackContext.started)
        {
            isScrollingMap = true;
        }
        else if(callbackContext.canceled)
        {
            isScrollingMap = false;
        }
    }
#endif

    public void OnMapScrolled(InputAction.CallbackContext callbackContext)
    {
#if UNITY_EDITOR

        if(isScrollingMap)
        {
            if (callbackContext.performed)
                scrollMovement = callbackContext.ReadValue<Vector2>();
        }
#else

        if(callbackContext.performed)
        {
            scrollMovement = callbackContext.ReadValue<Vector2>();
            isScrollingMap = true;
        }
        else if(callbackContext.canceled)
        {
            scrollMovement = Vector2.zero;
            isScrollingMap = false;
        }
#endif
    }

    public void OnZoom(InputAction.CallbackContext callbackContext)
    {
        if (isScrollingMap)
            return;

        if (callbackContext.performed)
        {
            zoomIndex += 1;

            ZoomMap();
        }
    }

    public void OnRightTabPressed(InputAction.CallbackContext callbackContext)
    {
        if (isScrollingMap)
            return;

        if (callbackContext.performed)
        {
            SwitchMapImage(1);
        }
    }

    public void OnLeftTabPressed(InputAction.CallbackContext callbackContext)
    {
        if (isScrollingMap)
            return;

        if (callbackContext.performed)
        {
            SwitchMapImage(-1);
        }
    }

    public void OnClosed(InputAction.CallbackContext callbackContext)
    {
        if (isScrollingMap)
            return;

        if (callbackContext.performed)
        {
            GameUIManager.instance.OpenMap(true);
        }
    }

    public void OnFastTravelPressed(InputAction.CallbackContext callbackContext)
    {
        if (isScrollingMap)
            return;

        if (callbackContext.performed)
        {
            //OpenFastTravelPanel();
        }
    }

#endregion
}
