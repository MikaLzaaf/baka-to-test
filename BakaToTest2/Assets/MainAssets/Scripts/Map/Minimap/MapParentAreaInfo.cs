﻿using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class MapParentAreaInfo 
{
    public string parentAreaName;
    public List<MapMainAreaInfo> mainAreaInfos;

    private Dictionary<string, MapMainAreaInfo> _mainAreaLookup;
    private Dictionary<string, MapMainAreaInfo> mainAreaLookup
    {
        get
        {
            if(_mainAreaLookup == null)
            {
                _mainAreaLookup = new Dictionary<string, MapMainAreaInfo>();

                if(mainAreaInfos != null)
                { 
                    for(int i = 0; i < mainAreaInfos.Count; i++)
                    {
                        _mainAreaLookup.Add(mainAreaInfos[i].mainAreaName, mainAreaInfos[i]);
                    }
                }
            }

            return _mainAreaLookup;
        }
    }


    public MapParentAreaInfo() { }

    public MapParentAreaInfo(string parentAreaName, List<MapMainAreaInfo> mainAreaInfo)
    {
        this.parentAreaName = parentAreaName;
        mainAreaInfos = mainAreaInfo;
    }


    public MapMainAreaInfo GetMainAreaInfo(string mainAreaName)
    {
        mainAreaLookup.TryGetValue(mainAreaName, out MapMainAreaInfo mapMainAreaInfo);

        return mapMainAreaInfo;
    }

    public bool HasMapMainArea(string mainAreaName)
    {
        MapMainAreaInfo mapMainAreaInfo = GetMainAreaInfo(mainAreaName);

        return mapMainAreaInfo != null;
    }

    public void AddMapMainArea(MapMainAreaInfo mapMainAreaInfo)
    {
        mainAreaLookup.Add(mapMainAreaInfo.mainAreaName, mapMainAreaInfo);
        mainAreaInfos.Add(mapMainAreaInfo);
    }

    //public MapFastTravelPointInfo[] GetSubAreaList(string mainAreaName)
    //{
    //    MapMainAreaInfo mainAreaInfo = GetMainAreaInfo(mainAreaName);

    //    if (mainAreaInfo != null)
    //        return mainAreaInfo.fastTravelPoints.ToArray();
    //    else
    //        return null;
    //}
}
