﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MapMainAreaInfo 
{
    public string mainAreaName; // Can be filled on the MapArea component in the scene
    public bool isAdditiveArea = false; // Can be filled on the MapArea component in the scene
    public Vector2 mapImageTranslation; // Will be filled by using the MapArea component in the scene
    public float mapXSize; // Will be filled by using the MapArea component in the scene
    public float mapZSize; // Will be filled by using the MapArea component in the scene

    public List<MapFastTravelPointInfo> fastTravelPoints; // Will be filled by using the MapArea component in the scene

    public List<MapEntranceInfo> entranceInfos; // Will be filled by using the MapArea component in the scene

    public MapMainAreaInfo() { }

    public MapMainAreaInfo(MapArea mapArea, List<MapFastTravelPointInfo> ftpInfo, List<MapEntranceInfo> mapEntranceInfos)
    {
        mainAreaName = mapArea.areaName;
        isAdditiveArea = mapArea.isAdditiveLoadingArea;
        fastTravelPoints = ftpInfo;
        entranceInfos = mapEntranceInfos;
        mapImageTranslation = mapArea.mapImageTranslation;
        mapXSize = mapArea.mapXSize;
        mapZSize = mapArea.mapZSize;
    }


    public bool HasEntrance(string entranceId)
    {
        for(int i = 0; i < entranceInfos.Count; i++)
        {
            if (entranceInfos[i].entranceID == entranceId)
                return true;
        }

        return false;
    }
}
