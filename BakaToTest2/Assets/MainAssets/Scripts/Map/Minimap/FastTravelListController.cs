﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FastTravelListController : UIViewController
{
    [Header("Attributes")]
    [SerializeField] FastTravelListItem listItemPrefab = default;
    [SerializeField] Transform listItemContainer = default;


    private List<FastTravelListItem> listItems;
    private MapMainAreaInfo currentMapMainArea;
    private FastTravelListItem currentSelectedListItem;


    protected override void Awake()
    {
        base.Awake();

        if(GameUIManager.instance != null)
        {
            var mapUIController = GameUIManager.instance.mapController;
            mapUIController.OnMapZoomEvent += RecalculateMarkerPosition;
        }
    }

    private void OnDestroy()
    {
        if (GameUIManager.instance != null)
        {
            var mapUIController = GameUIManager.instance.mapController;
            mapUIController.OnMapZoomEvent -= RecalculateMarkerPosition;
        }
    }

    public void GenerateList(MapMainAreaInfo mapMainAreaInfo)
    {
        if (listItems != null && listItems.Count > 0)
        {
            if (listItems.Count > 0)
            {
                for (int i = listItems.Count - 1; i >= 0; i--)
                {
                    Destroy(listItems[i].gameObject);
                }
            }

            listItems.Clear();
        }
        else
        {
            listItems = new List<FastTravelListItem>();
        }

        currentMapMainArea = mapMainAreaInfo;

        if(currentMapMainArea.fastTravelPoints.Count == 0)
        {
            UpdateLocationMarker(null);
            return;
        }

        for (int i = 0; i < currentMapMainArea.fastTravelPoints.Count; i++)
        {
            FastTravelListItem listItem = Instantiate(listItemPrefab);
            listItems.Add(listItem);

            listItem.transform.SetParent(listItemContainer, false);

            listItem.Initialize(currentMapMainArea.fastTravelPoints[i], this);

            listItem.onClick.AddListener(delegate
            {
                OnListItemClicked(listItem);
            });
        }

        UINavigator.instance.SetupVerticalNavigation(listItems, true);

        if(listItems.Count > 0)
            UINavigator.instance.SetDefaultSelectedInRuntime(listItems[0].gameObject);

        MapManager.instance.mapUIController.CalculateMarkerTransformationMatrix(currentMapMainArea);
    }

    private void OnListItemClicked(FastTravelListItem listItem)
    {
        // Do fast travel
        // How? 
        // - Open loading screen
        // - Change to mini map
        // - Switch map image if needed
        // - Move character to fast travel point
        // - Close loading screen

        MapManager.instance.FastTravelTo(currentMapMainArea, listItem.ftpInfo.pointName);
    }

    public void UpdateLocationMarker(FastTravelListItem listItem)
    {
        if (MapManager.instance.mapUIController == null)
            return;

        if(listItem == null)
            MapManager.instance.mapUIController.MoveLocationMarkerOnFullMap(null);
        else
            MapManager.instance.mapUIController.MoveLocationMarkerOnFullMap(listItem.ftpInfo);

        if(listItem != currentSelectedListItem)
            currentSelectedListItem = listItem;
    }

    private void RecalculateMarkerPosition()
    {
        MapManager.instance.mapUIController.CalculateMarkerTransformationMatrix(currentMapMainArea);

        UpdateLocationMarker(currentSelectedListItem);
    }
}
