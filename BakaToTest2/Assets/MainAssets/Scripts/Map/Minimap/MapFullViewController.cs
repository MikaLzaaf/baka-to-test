﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MapFullViewController : ViewController
{
    [Header("Full Map")]
    [SerializeField] private TextMeshProUGUI parentAreaNameText = default;
    [SerializeField] private TextMeshProUGUI mainAreaNameText = default;
    [SerializeField] Image mapImage = default;
    [SerializeField] FastTravelListController fastTravelPanel = default;
    [SerializeField] GameObject locationMarker = default;
    public Transform playerMapIconHolder;


    [Header("Input Labels")]
    [SerializeField] GameObject toggleMapLabel = default;
    [SerializeField] GameObject cityMapLabel = default;
    [SerializeField] GameObject zoomLabel = default;
    //[SerializeField] GameObject cityLabel = default;
    [SerializeField] GameObject closeLabel = default;
    [SerializeField] GameObject scrollLabel = default;
    [SerializeField] GameObject fastTravelLabel = default;
    [SerializeField] GameObject confirmLabel = default;


    private MapParentAreaInfo currentMapParentAreaInfo;
    public MapMainAreaInfo currentMapMainAreaInfo { get; private set; }

    public int currentMapMainAreaIndex { get; private set; } = 0;

    private const string MapImagesInResourcesPrefix = "Images/Maps/";


    public void Show(bool canFastTravelMap)
    {
        if (parentAreaNameText == null || mainAreaNameText == null)
            return;

        if(currentMapParentAreaInfo != null)
        {
            UpdateMapParentAreaName(currentMapParentAreaInfo.parentAreaName);
        }

        if(currentMapMainAreaInfo != null)
        {
            UpdateMapMainAreaName(currentMapMainAreaInfo.mainAreaName);
        }

        Show();

        if (canFastTravelMap)
        {
            fastTravelPanel.GenerateList(currentMapMainAreaInfo);

            fastTravelPanel.Show();

            ToggleNormalViewLabels();
        }
        else
        {

        }

        
    }


    public void ToggleNormalViewLabels()
    {

    }

    public void SetMap(MapParentAreaInfo mapParentAreaInfo, MapMainAreaInfo mapMainAreaInfo)
    {
        currentMapParentAreaInfo = mapParentAreaInfo;
        currentMapMainAreaInfo = mapMainAreaInfo;

        SwitchMapImage(mapMainAreaInfo.mainAreaName);
    }

    public void SwitchMapImage(string mapName)
    {
        string spritePath = MapImagesInResourcesPrefix + mapName;
       
        Sprite mapSprite = Resources.Load<Sprite>(spritePath);

        if (mapImage != null)
        {
            mapImage.sprite = mapSprite;
        }
    }

    public void SwitchMapImage(int index)
    {
        if (currentMapParentAreaInfo == null)
            return;

        currentMapMainAreaIndex += index;

        if (currentMapMainAreaIndex >= currentMapParentAreaInfo.mainAreaInfos.Count)
        {
            currentMapMainAreaIndex = 0;
        }
        else if (currentMapMainAreaIndex < 0)
        {
            currentMapMainAreaIndex = currentMapParentAreaInfo.mainAreaInfos.Count - 1;
        }

        SwitchMapImage(currentMapParentAreaInfo.mainAreaInfos[currentMapMainAreaIndex].mainAreaName);

        UpdateMapMainAreaName(currentMapParentAreaInfo.mainAreaInfos[currentMapMainAreaIndex].mainAreaName);

        // Update fast travel list too
        fastTravelPanel.GenerateList(currentMapParentAreaInfo.mainAreaInfos[currentMapMainAreaIndex]);

        // Remove player map icon if player is not in the map area

    }

    private void UpdateMapMainAreaName(string mainAreaName)
    {
        if(mainAreaNameText != null)
        {
            mainAreaNameText.text = mainAreaName;
        }
    }

    private void UpdateMapParentAreaName(string parentAreaName)
    {
        if(parentAreaNameText != null)
        {
            parentAreaNameText.text = parentAreaName;
        }
    }

    private void ToggleLocationMarker(bool active)
    {
        if(locationMarker != null)
        {
            locationMarker.SetActive(active);
        }
    }

    public void MoveLocationMarker(bool showLocationOnMap, Vector2 markerPosition)
    {
        ToggleLocationMarker(showLocationOnMap);

        if(locationMarker != null)
        {
            locationMarker.transform.localPosition = markerPosition;
        }
    }

    #region Labels

    private void ToggleToggleMapLabel(bool active)
    {
        if (toggleMapLabel != null)
        {
            toggleMapLabel.SetActive(active);
        }
    }

    private void ToggleWorldMapLabel(bool active)
    {
        if (cityMapLabel != null)
        {
            cityMapLabel.SetActive(active);
        }
    }

    private void ToggleZoomLabel(bool active)
    {
        if (zoomLabel != null)
        {
            zoomLabel.SetActive(active);
        }
    }

    //private void ToggleCityLabel(bool active) // Can adjust directly at world map view later
    //{
    //    if (cityLabel != null)
    //    {
    //        cityLabel.SetActive(active);
    //    }
    //}

    private void ToggleCloseLabel(bool active)
    {
        if (closeLabel != null)
        {
            closeLabel.SetActive(active);
        }
    }

    private void ToggleScrollLabel(bool active)
    {
        if (scrollLabel != null)
        {
            scrollLabel.SetActive(active);
        }
    }

    private void ToggleFastTravelLabel(bool active)
    {
        if (fastTravelLabel != null)
        {
            fastTravelLabel.SetActive(active);
        }
    }

    private void ToggleConfirmLabel(bool active)
    {
        if (confirmLabel != null)
        {
            confirmLabel.SetActive(active);
        }
    }

    #endregion
}
