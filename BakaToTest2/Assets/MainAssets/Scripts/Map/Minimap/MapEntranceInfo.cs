﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MapEntranceInfo 
{

    public string entranceID;


    public string destinationAreaName;
    public string destinationEntranceID;
    public bool destinationInAnotherParentArea;


    public MapEntranceInfo(string entranceID, string destinationAreaName, string destinationEntranceID, bool destinationInAnotherParentArea)
    {
        this.entranceID = entranceID;
        this.destinationAreaName = destinationAreaName;
        this.destinationEntranceID = destinationEntranceID;
        this.destinationInAnotherParentArea = destinationInAnotherParentArea;
    }
}
