﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapFastTravelPoint : MonoBehaviour
{
    public string pointName;
    public string pointDisplayName;
    public bool showLocationOnMap = true; // This can be filled on the FastTravelPoint prefab
    public bool canFastTravel = true;
}
