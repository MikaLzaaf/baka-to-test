﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;
using UnityEngine.EventSystems;
using Intelligensia.UI;

public class FastTravelListItem : HighlightingButton, ISelectHandler
{

    [SerializeField] TextMeshProUGUI itemNameText = default;

    //public event Action OnHighlightedEvent;
    public MapFastTravelPointInfo ftpInfo { get; private set; }

    private FastTravelListController fastTravelListController;


    public void Initialize(MapFastTravelPointInfo ftpInfo, FastTravelListController fastTravelListController)
    {
        if(itemNameText != null && ftpInfo != null)
        {
            itemNameText.text = ftpInfo.pointDisplayName;
        }

        this.ftpInfo = ftpInfo;
        this.fastTravelListController = fastTravelListController;
    }

    public override void OnSelect(BaseEventData eventData)
    {
        base.OnSelect(eventData);

        // Update fast travel marker on map
        //OnHighlightedEvent?.Invoke();

        fastTravelListController.UpdateLocationMarker(this);
    }

    //private void UpdateLocationMarker()
    //{
    //    if (MapManager.instance.mapUIController == null)
    //        return;

    //    MapManager.instance.mapUIController.MoveLocationMarkerOnFullMap(ftpInfo);
    //}
}
