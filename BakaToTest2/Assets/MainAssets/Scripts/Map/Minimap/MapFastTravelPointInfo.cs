﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MapFastTravelPointInfo 
{
    public string pointName; // This can be filled on the FastTravelPoint prefab
    public string pointDisplayName; // This can be filled on the FastTravelPoint prefab
    public Vector3 markerLocation; // This will be filled by using the FastTravelPoint prefab's position on scene
    public bool showLocationOnMap = true; // This can be filled on the FastTravelPoint prefab
    public bool canFastTravel = true; // This can be filled on the FastTravelPoint prefab



    public MapFastTravelPointInfo() { }

    public MapFastTravelPointInfo(string pointName, string displayName, Vector3 pointLocation, bool showLocation, bool canFastTravel)
    {
        this.pointName = pointName;
        pointDisplayName = displayName;
        markerLocation = pointLocation;
        showLocationOnMap = showLocation;
        this.canFastTravel = canFastTravel;
    }
}
