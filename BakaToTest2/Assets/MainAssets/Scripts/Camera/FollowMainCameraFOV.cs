﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowMainCameraFOV : MonoBehaviour
{
    Camera mainCamera;
    Camera thisCamera;

    private void Awake()
    {
        mainCamera = Camera.main;
        thisCamera = GetComponent<Camera>();
    }


    private void LateUpdate()
    {
        thisCamera.fieldOfView = mainCamera.fieldOfView;
    }
}
