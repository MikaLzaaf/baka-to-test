﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class AutoOrbit : MonoBehaviour
{
    public CinemachineVirtualCamera vCam_Orbital;
    public float orbitSpeed = 0.1f;

    private CinemachineOrbitalTransposer orbital;


    private void Start()
    {
        orbital = vCam_Orbital.GetCinemachineComponent<CinemachineOrbitalTransposer>();
    }


    private void Update()
    {
        if(orbital != null)
        {
            orbital.m_XAxis.Value += orbitSpeed;
        }
    }
}
