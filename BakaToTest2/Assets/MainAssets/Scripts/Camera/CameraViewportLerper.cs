﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraViewportLerper : MonoBehaviour
{
    public Vector2 targetSize;
    public Vector2 targetPosition;

    public Vector2 startSize;
    public Vector2 startPosition;

    public float lerpDuration = 1f;
    public bool lerpOnEnable = false;
    //public bool startWithPositionZero = true;


    private Camera targetCamera;

    private void Awake()
    {
        targetCamera = GetComponent<Camera>();
    }


    private void OnEnable()
    {
        if(lerpOnEnable)
        {
            if (targetCamera == null)
                return;

            StartCoroutine(LerpViewport(true));
        }
    }


    private IEnumerator LerpViewport(bool toTarget)
    {
        if(toTarget)
        {
            targetCamera.rect = new Rect(startPosition, startSize);
        }
        else
        {
            targetCamera.rect = new Rect(targetPosition, targetSize);
        }

        float elapsedDuration = 0f;

        while(elapsedDuration < lerpDuration)
        {
            elapsedDuration += Time.deltaTime;

            Rect tempRect = targetCamera.rect;

            if (toTarget)
            {
                tempRect.x = Mathf.Lerp(targetCamera.rect.x, targetPosition.x, elapsedDuration / lerpDuration);
            }
            else
            {
                tempRect.x = Mathf.Lerp(targetCamera.rect.x, startPosition.x, elapsedDuration / lerpDuration);
            }
          
            targetCamera.rect = new Rect(tempRect);

            yield return null;
        }
    }

    public void StartLerping(bool toTarget)
    {
        StartCoroutine(LerpViewport(toTarget));
    }
}
