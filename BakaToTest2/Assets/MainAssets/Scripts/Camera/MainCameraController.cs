﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class MainCameraController : MonoBehaviour 
{

    public static MainCameraController instance;

    public CinemachineBrain cinemachineBrain;

    //public MinimapCameraController minimapCamera;
    public CinemachineFreeLook thirdPersonCamera_Single;
    public CinemachineFreeLook thirdPersonCamera_Group;
    public CinemachineTargetGroup thirdPersonTargetGroup;


    private CinemachineVirtualCamera previousVirtualCamera;
    private CinemachineVirtualCamera currentVirtualCamera;

    public Camera mainCamera { get; private set; }

    private float[] defaultOrbitsRadius;
    public bool isCameraChangedSuddenly = false;

    //private readonly float DefaultBlendDuration = 1f;


    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
            //DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }


    private void Start()
    {
        mainCamera = Camera.main;

        if(thirdPersonCamera_Single != null)
        {
            defaultOrbitsRadius = new float[3];

            for(int i = 0; i < thirdPersonCamera_Single.m_Orbits.Length; i++)
            {
                defaultOrbitsRadius[i] = thirdPersonCamera_Single.m_Orbits[i].m_Radius;
            }
        }
    }


    public void ToggleCameraBrain(bool active, bool toggleGameobjectToo = false)
    {
        cinemachineBrain.enabled = active;

        if(toggleGameobjectToo)
            gameObject.SetActive(active);
    }


    public void SetThirdPersonFollowAndLookAtTarget(Transform target)
    {
        SetThirdPersonLookAtTarget(target);
        SetThirdPersonFollowTarget(target);
    }


    public void SetThirdPersonLookAtTarget(Transform target)
    {
        if(thirdPersonCamera_Single != null)
        {
            thirdPersonCamera_Single.m_LookAt = target;
        }
    }


    public void SetThirdPersonFollowTarget(Transform target)
    {
        if(thirdPersonCamera_Single != null)
        {
            thirdPersonCamera_Single.m_Follow = target;
        }
    }


    public void SetCameraBlend(float blendDuration, bool changeStyleToCut = false)
    {
        cinemachineBrain.m_DefaultBlend.m_Style = changeStyleToCut ? CinemachineBlendDefinition.Style.Cut : CinemachineBlendDefinition.Style.EaseInOut;
   
        cinemachineBrain.m_DefaultBlend.m_Time = blendDuration;
    }


    public void ChangeToNextCamera(CinemachineVirtualCamera nextVirtualCamera)
    {
        if (currentVirtualCamera)
        {
            previousVirtualCamera = currentVirtualCamera;
            ToggleCamera(previousVirtualCamera, false);
        }

        if (thirdPersonCamera_Single != null && thirdPersonCamera_Single.m_Priority > 0)
        {
            thirdPersonCamera_Single.m_Priority = 0;
            thirdPersonCamera_Single.gameObject.SetActive(false);
        }

        currentVirtualCamera = nextVirtualCamera;
        ToggleCamera(currentVirtualCamera, true);
    }


    public void ChangeToPreviousCamera()
    {
        if (!previousVirtualCamera)
        {
            ChangeCameraToDefault();
        }
        else
        {
            ToggleCamera(previousVirtualCamera, true);

            ToggleCamera(currentVirtualCamera, false);
        }
    }


    public void ChangeCameraToDefault()
    {
        if (previousVirtualCamera)
        {
            ToggleCamera(previousVirtualCamera, false);
        }

        thirdPersonCamera_Single.m_Priority = 10;
        thirdPersonCamera_Single.gameObject.SetActive(true);
    }


    private void ToggleCamera(CinemachineVirtualCamera virtualCamera, bool active)
    {
        if(virtualCamera == null)
        {
            return;
        }

        virtualCamera.m_Priority = active ? 10 : 0;
        virtualCamera.gameObject.SetActive(active);
    }


    public void CutToNextCamera(CinemachineVirtualCamera virtualCamera, float duration = 1f)
    {
        SetCameraBlend(duration, true);

        ChangeToNextCamera(virtualCamera);
    }


    public void ChangeOrbitSize(bool toInitialSize)
    {
        if(!toInitialSize)
        {
            return;
        }

        ChangeOrbitSize(defaultOrbitsRadius[0], defaultOrbitsRadius[1], defaultOrbitsRadius[2]);
    }


    public void ChangeOrbitSize(float newTopRadius, float newMidRadius, float newBottomRadius)
    {
        if(thirdPersonCamera_Single == null)
        {
            return;
        }

        thirdPersonCamera_Single.m_Orbits[0].m_Radius = newTopRadius;
        thirdPersonCamera_Single.m_Orbits[1].m_Radius = newMidRadius;
        thirdPersonCamera_Single.m_Orbits[2].m_Radius = newBottomRadius;
    }


    public void RecenteredCamera(bool recenter)
    {
        thirdPersonCamera_Single.m_RecenterToTargetHeading.m_enabled = recenter;

        thirdPersonCamera_Single.m_YAxis.m_Recentering.m_enabled = recenter;
    }

    public void CenteringThirdPersonCamera()
    {
        StartCoroutine(RecenteringThirdPersonCamera());
    }

    private IEnumerator RecenteringThirdPersonCamera()
    {
        thirdPersonCamera_Single.m_Transitions.m_InheritPosition = false;

        RecenteredCamera(true);

        yield return new WaitForSeconds(thirdPersonCamera_Single.m_RecenterToTargetHeading.m_RecenteringTime);

        yield return new WaitForSeconds(0.2f);

        RecenteredCamera(false);
    }    


    public void DelaySuddenInputChanges()
    {
        isCameraChangedSuddenly = true;

        //lastMovementInput = movementInputAxis;

        StartCoroutine(DelayingInput());
    }


    private IEnumerator DelayingInput()
    {
        yield return new WaitForSeconds(0.5f);

        //yield return new WaitUntil(() => lastMovementInput != movementInputAxis);

        isCameraChangedSuddenly = false;
    }

    public void SetFollowTarget(Transform target)
    {
        if (cinemachineBrain == null)
            return;

        var activeCam = cinemachineBrain.ActiveVirtualCamera;

        activeCam.Follow = target;
    }

    public void SetLookAtTarget(Transform target)
    {
        if (cinemachineBrain == null)
            return;

        var activeCam = cinemachineBrain.ActiveVirtualCamera;

        activeCam.LookAt = target;
    }

    public void PrepareThirdPersonTargetGroup(Transform[] targets)
    {
        if (thirdPersonTargetGroup == null)
            return;

        var prevTargets = thirdPersonTargetGroup.m_Targets;

        if (prevTargets.Length > 0)
        {
            for (int i = prevTargets.Length - 1; i >= 0; i--)
            {
                thirdPersonTargetGroup.RemoveMember(prevTargets[i].target);
            }
        }

        for (int i = 0; i < targets.Length; i++)
        {
            thirdPersonTargetGroup.AddMember(targets[i], 1f, 2f);
        }
    }

    public void ActivateThirdPersonGroupCamera(bool active)
    {
        if(thirdPersonCamera_Group != null)
        {
            thirdPersonCamera_Group.Priority = active ? 12 : 0;
        }

        if(thirdPersonCamera_Single != null)
        {
            thirdPersonCamera_Single.m_Transitions.m_InheritPosition = !active;
        }
    }

}
