﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum ModelPositionOnScreen
{
    Left,
    Centre,
    Right
}


public class ModelCameraController : MonoBehaviour
{
    public static ModelCameraController instance;

    public Camera modelCamera;
    public Transform modelSpawnPoint;

    public Transform cameraLeftPoint;
    public Transform cameraCentrePoint;
    public Transform cameraRightPoint;

    public EntityModel entityModel { get; private set; }
    //private Vector3 initialModelPosition;
    //private Quaternion initialModelRotation;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }


    public void Activate(ModelPositionOnScreen positionOnScreen, BattleEntity entity)
    {
        if (modelCamera == null)
            return;

        modelCamera.gameObject.SetActive(true);

        if(entityModel != null)
        {
            Destroy(entityModel.gameObject);
        }

        Transform parent = null;

        if(positionOnScreen == ModelPositionOnScreen.Left)
        {
            parent = cameraLeftPoint;
        }
        else if (positionOnScreen == ModelPositionOnScreen.Centre)
        {
            parent = cameraCentrePoint;
        }
        else if (positionOnScreen == ModelPositionOnScreen.Right)
        {
            parent = cameraRightPoint;
        }

        modelCamera.transform.SetParent(parent, false);

        entityModel = entity.entityModelBinder.InstantiateEntityModel(modelSpawnPoint);
        entityModel.transform.position = modelSpawnPoint.position;
        entityModel.transform.rotation = modelSpawnPoint.rotation;
        entityModel.gameObject.SetActive(true);
    }


    public void Deactivate()
    {
        //model.position = initialModelPosition;
        //model.rotation = initialModelRotation;
        Destroy(entityModel.gameObject);
        entityModel = null;

        if (modelCamera != null)
            modelCamera.gameObject.SetActive(false);

    }
}
