﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraEffector_NarrowOrbit : CameraEffector
{
    public float topRadius;
    public float midRadius;
    public float bottomRadius;

    
    public bool changeOrbitToNormal = false;


    private void OnTriggerEnter(Collider otherCollider)
    {
        if (otherCollider.gameObject.tag == PlayerTag)
        {
            if(changeOrbitToNormal)
            {
                MainCameraController.instance.ChangeOrbitSize(true);
            }
            else
            {
                MainCameraController.instance.ChangeOrbitSize(topRadius, midRadius, bottomRadius);
            }
        }

        if (deactivateAfterEnter)
        {
            gameObject.SetActive(false);
        }
    }

}
