﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CameraEffector_TrackMode : CameraEffector
{
    public CinemachineVirtualCamera vCamTrack;



    private void OnTriggerEnter(Collider otherCollider)
    {
        if (otherCollider.gameObject.tag == PlayerTag)
        {
            ToggleCamera(otherCollider.transform);
        }

        if (deactivateAfterEnter)
        {
            gameObject.SetActive(false);
        }
    }


    private void OnTriggerExit(Collider otherCollider)
    {
        if (otherCollider.gameObject.tag == PlayerTag)
        {
            ToggleCamera(null);
        }
    }


    private void ToggleCamera(Transform target)
    {
        if (vCamTrack == null)
            return;

        if(target != null)
        {
            vCamTrack.m_Follow = target;
            vCamTrack.m_LookAt = target;

            MainCameraController.instance.DelaySuddenInputChanges();
        }

        MainCameraController.instance.SetCameraBlend(.5f, target != null);

        MainCameraController.instance.RecenteredCamera(target != null);

        vCamTrack.gameObject.SetActive(target != null);
    }


    protected IEnumerator SetTarget(Transform target)
    {
        yield return new WaitForSeconds(0.05f);

        vCamTrack.m_Follow = target;
        vCamTrack.m_LookAt = target;
    }
}
