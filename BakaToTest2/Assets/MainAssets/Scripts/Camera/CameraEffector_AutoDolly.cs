﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CameraEffector_AutoDolly : CameraEffector
{
    public float dollyDuration = 1f;
    public bool inReverse = false;

    private CinemachineTrackedDolly trackedDolly;
    private float elapsedTime;


    private void Start()
    {
        if(TryGetComponent(out CinemachineVirtualCamera vCamDolly))
        {
            trackedDolly = vCamDolly.GetCinemachineComponent<CinemachineTrackedDolly>();
        }
    }


    public void StartDolly()
    {
        StartCoroutine(Dollying());
    }


    private IEnumerator Dollying()
    {
        elapsedTime = 0f;
        trackedDolly.m_PathPosition = inReverse ? 1f : 0f;

        while (elapsedTime < dollyDuration)
        {
            elapsedTime += Time.deltaTime;

            if(inReverse)
            {
                trackedDolly.m_PathPosition = 1 - elapsedTime / dollyDuration;
            }
            else
            {
                trackedDolly.m_PathPosition += elapsedTime / dollyDuration;
            }
            
            yield return null;
        }

        trackedDolly.m_PathPosition = inReverse ? 0f : 1f;
    }
}
