﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

[RequireComponent(typeof(CinemachineImpulseSource))]
public class CameraShaker : MonoBehaviour
{
    public enum CameraShakeType
    {
        Mild,
        Normal,
        Strong,
        Extreme
    }


    public float loopInterval = 2f;
    public float shakeDuration = 3f;

    public bool shakeOnStart = false;
    public bool loop = false;

    private CinemachineImpulseSource impulseSource;



    private void Awake()
    {
        impulseSource = GetComponent<CinemachineImpulseSource>();
    }


    private void Start()
    {
        if(shakeOnStart)
        {
            if(loop)
            {
                InvokeRepeating("Shake", shakeDuration, loopInterval);
            }
            else
            {
                Invoke("Shake", shakeDuration);
            }
        }
    }


    public void Shake()
    {
        if(impulseSource != null)
        {
            impulseSource.GenerateImpulse();
        }
    }

    //public void Shake(float force)
    //{
    //    if(impulseSource != null)
    //    {
    //        impulseSource.GenerateImpulse(force);
    //    }
    //}
}
