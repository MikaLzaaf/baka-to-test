﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CameraEffector_LookOnly : CameraEffector
{
    public CinemachineVirtualCamera vCameraFollow;


    private void OnTriggerEnter(Collider otherCollider)
    {
        if(otherCollider.gameObject.tag == PlayerTag)
        {
            EntityCameraFocus cameraFocus = otherCollider.transform.GetChild(otherCollider.transform.childCount - 1).GetComponent<EntityCameraFocus>();

            if(cameraFocus != null)
            {
                SetTarget(cameraFocus.headFocus);
                ToggleCamera(true);
            }
            else
            {
                SetTarget(otherCollider.transform);
                ToggleCamera(true);
            }
        }

        if(deactivateAfterEnter)
        {
            gameObject.SetActive(false);
        }
    }


    private void OnTriggerExit(Collider otherCollider)
    {
        if (otherCollider.gameObject.tag == PlayerTag)
        {
            ToggleCamera(false);
        }
    }


    private void SetTarget(Transform target)
    {
        if (vCameraFollow != null)
        {
            vCameraFollow.m_LookAt = target;
        }
    }


    public void ToggleCamera(bool active)
    {
        if(vCameraFollow != null)
        {
            MainCameraController.instance.SetCameraBlend(.5f, active);

            MainCameraController.instance.RecenteredCamera(active);

            if(active)
                MainCameraController.instance.DelaySuddenInputChanges();

            vCameraFollow.gameObject.SetActive(active);
        }
    }
}
