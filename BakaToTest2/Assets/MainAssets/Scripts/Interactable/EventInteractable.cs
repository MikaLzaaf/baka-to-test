﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Intelligensia.UI.HUD;
public class EventInteractable : ObjectInteractableBase
{
    [Header("Button Input Labels")]
    [SerializeField] private ButtonInputLabelId[] inputLabelsToDisplay = default;

    public TimelinePlaybackController timelinePlaybackController;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    protected override ButtonInputLabelId[] GetButtonInputLabelIds()
    {
        return inputLabelsToDisplay;
    }

    public override void StartInteraction(Transform interactor)
    {
        base.StartInteraction(interactor);

        if(timelinePlaybackController)
        {
            timelinePlaybackController.PlayTimeline();
        }
    }
}
