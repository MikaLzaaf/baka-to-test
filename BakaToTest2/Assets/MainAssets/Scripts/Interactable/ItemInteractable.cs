﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Intelligensia.UI.HUD;

public class ItemInteractable : ObjectInteractableBase
{
    public Action OnItemInteract;

    public Item item;

    public bool destroyAfterInteraction = true;

    [Header("Button Input Labels")]
    [SerializeField] private ButtonInputLabelId[] inputLabelsToDisplay = default;


    //public void DisplayItemInfo()
    //{

    //}

    protected override ButtonInputLabelId[] GetButtonInputLabelIds()
    {
        return inputLabelsToDisplay;
    }

    public override void StartInteraction(Transform interactor)
    {
        if (item == null)
            return;

        base.StartInteraction(interactor);

        PlayerInventoryManager.instance.AddItem(item);
        //DisplayItemInfo();
        if(destroyAfterInteraction)
            Destroy(this.gameObject);
    }
}
