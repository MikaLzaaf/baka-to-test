﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Intelligensia.UI.HUD;

public abstract class ObjectInteractableBase : MonoBehaviour, IInteractable
{

    [SerializeField] private string objName = default;
    public string ObjName

    {
        get
        {
            if (string.IsNullOrEmpty(objName))
            {
                return gameObject.name;
            }

            return objName;
        }
    }


    public event Action<Transform> InteractionStartedEvent;
    public event Action InteractionEndedEvent;

    [SerializeField] private bool shouldInteractorFacingThis = default;
    [SerializeField] private bool _canInteract = true;
    public bool canInteract
    {
        get
        {
            return _canInteract;
        }

        set
        {
            _canInteract = value;
        }
    }


    protected abstract ButtonInputLabelId[] GetButtonInputLabelIds();

    protected virtual void ToggleInteractionBubble(bool active)
    {

    }

    public void ToggleInteractionDisplay(bool active)
    {
        ToggleInteractionBubble(active);

        if (GameUIManager.instance == null)
            return;

        HUDManager hudManager = GameUIManager.instance.hudManager;

        if (hudManager == null)
            return;

        if (active)
            hudManager.UpdateButtonInputLabels(GetButtonInputLabelIds());
        else
            hudManager.UpdateButtonInputLabels(null);
    }


    public virtual void StartInteraction(Transform interactor)
    {
        ToggleInteractionDisplay(false);

        InteractionStartedEvent?.Invoke(interactor);

        if(shouldInteractorFacingThis)
        {
            EntityCharacterController entityCharacterController = interactor.GetComponent<EntityCharacterController>();

            if (entityCharacterController != null)
                entityCharacterController.RotateTowards(transform.position - interactor.position);
        }
    }


    public virtual void EndInteraction()
    {
        InteractionEndedEvent?.Invoke();
    }


    protected virtual void OnQuestActivated()
    {

    }


    protected virtual void OnQuestCompleted()
    {

    }

    //received gameobject of the one who start interact
    //make the other do anything or pass to other something here
    public void OnReceivedInteract(GameObject player)
    {
        if (player == null || player == this.transform.gameObject)
        {
            return;
        }

        InteractionStartedEvent?.Invoke(player.transform);
    }
}
