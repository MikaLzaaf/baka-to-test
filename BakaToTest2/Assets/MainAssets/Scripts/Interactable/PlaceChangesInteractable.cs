﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;
using UnityEngine.Events;
using Intelligensia.UI.HUD;


public class PlaceChangesInteractable : ObjectInteractableBase
{
    public enum ChangeType
    {
        Position = 0,
        Scene = 1,
    }

    public UnityEvent OnChangePlaceEvent;
    public UnityEvent SwitchCameraAngleEvent;

    public ChangeType changeType;

    public bool isInitiallyOpen = false;
    public bool autoClose = false;

    public string sceneName;

    [Header("Button Input Labels")]
    [SerializeField] private ButtonInputLabelId[] inputLabelsToDisplay = default;

    private Animator animator;
    private bool isOpened = false;


    protected void Start()
    {
        animator = GetComponent<Animator>();

        if(animator != null)
        {
            animator.SetBool("isOpen", isInitiallyOpen);
        }

        isOpened = isInitiallyOpen;
    }

    protected override ButtonInputLabelId[] GetButtonInputLabelIds()
    {
        return inputLabelsToDisplay;
    }

    public override void StartInteraction(Transform interactor)
    {
        base.StartInteraction(interactor);
        switch (changeType)
        {
            //move to other room, etc.. still same scene
            case ChangeType.Position:
                
                ToggleDoorState(true);

                if(!autoClose)
                {
                    ChangePlace();
                }

                SwitchCameraAngleEvent?.Invoke();

                break;

            //change scene
            case ChangeType.Scene:
                SceneManager.LoadScene(sceneName);
                break;
        }
    }


    public void ToggleDoorState(bool open)
    {
        if (animator != null)
        {
            animator.SetBool("isOpen", open);
        }
    }


    public void ChangePlace()
    {
        OnChangePlaceEvent?.Invoke();
    }
}
