﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractionDetectionController : MonoBehaviour {

    public CollisionMessageRelay interactionTrigger;
    public Collider triggerCollider;
    public float reactivateColliderInterval = 0.5f;

    //private List<ObjectInteractableBase> interactableList = new List<ObjectInteractableBase>();
    //private Dictionary<string, ObjectInteractableBase> interactableListLookup = new Dictionary<string, ObjectInteractableBase>();

    private ObjectInteractableBase currentInteractable;


    private void OnEnable()
    {
        if(interactionTrigger != null)
        {
            interactionTrigger.TriggerEnterEvent -= OnInteractionTriggerEnter;
            interactionTrigger.TriggerEnterEvent += OnInteractionTriggerEnter;

            interactionTrigger.TriggerExitEvent -= OnInteractionTriggerExit;
            interactionTrigger.TriggerExitEvent += OnInteractionTriggerExit;
        }
    }


    private void OnDisable()
    {
        if (interactionTrigger != null)
        {
            interactionTrigger.TriggerEnterEvent -= OnInteractionTriggerEnter;
            interactionTrigger.TriggerExitEvent -= OnInteractionTriggerExit;
        }
    }


    private void OnInteractionTriggerEnter(CollisionMessageRelay messageRelay, Collider otherCollider)
    {
        if(BattleController.IsInBattle)
            return;
       
        AddDetectedInteraction(otherCollider);
    }


    private void OnInteractionTriggerExit(CollisionMessageRelay messageRelay, Collider otherCollider)
    {
        if (BattleController.IsInBattle)
            return;

        RemoveDetectedInteraction(otherCollider);
    }


    private void AddDetectedInteraction(Collider collider)
    {
        ObjectInteractableBase interactableObj = collider.GetComponent<ObjectInteractableBase>();

        if (interactableObj == null)
            return;

        if(currentInteractable != null)
            currentInteractable.ToggleInteractionDisplay(false);

        currentInteractable = interactableObj;
        currentInteractable.ToggleInteractionDisplay(true);
    }


    private void RemoveDetectedInteraction(Collider collider)
    {
        ObjectInteractableBase interactableObj = collider.GetComponent<ObjectInteractableBase>();

        if (interactableObj == null)
            return;

        if(currentInteractable == interactableObj)
        {
            currentInteractable.ToggleInteractionDisplay(false);
            currentInteractable = null;
        }
    }


    public void CheckInteraction(out ObjectInteractableBase interactable)
    {
        interactable = null;

        if (!UINavigator.instance.canCheckInteraction)
            return;

        if (currentInteractable == null || !currentInteractable.canInteract)
            return;

        if(currentInteractable is NPCInteractable || currentInteractable is ItemInteractable)
        {
            MainCameraController.instance.PrepareThirdPersonTargetGroup(GetInteractionTargets());

            MainCameraController.instance.ActivateThirdPersonGroupCamera(true);
        }

        currentInteractable.StartInteraction(transform.parent);

        interactable = currentInteractable;
    }

    public void CheckChallengeAvailability(out ObjectInteractableBase interactable)
    {
        interactable = null;

        if (!UINavigator.instance.canCheckInteraction)
            return;

        if (currentInteractable == null || !currentInteractable.canInteract)
            return;

        if (currentInteractable is NPCInteractable)
        {
            NPCInteractable npc = (NPCInteractable)currentInteractable;

            if (!npc.CanBattle || npc.hasChallengeAdded)
                return;

            npc.ChallengeInteraction(transform.parent);

            interactable = currentInteractable;
        }
    }

    private Transform[] GetInteractionTargets()
    {
        List<Transform> targets = new List<Transform>();

        if(currentInteractable != null)
            targets.Add(currentInteractable.transform);

        if(transform.parent != null)
            targets.Add(transform.parent);

        return targets.ToArray();
    }

    public void ReactivateTrigger()
    {
        StartCoroutine(ReactivatingTrigger());
    }

    private IEnumerator ReactivatingTrigger()
    {
        ToggleCollider(false);

        yield return new WaitForSeconds(reactivateColliderInterval);

        ToggleCollider(true);
    }

    private void ToggleCollider(bool active)
    {
        if (triggerCollider != null)
        {
            triggerCollider.enabled = active;
        }
    }
}
