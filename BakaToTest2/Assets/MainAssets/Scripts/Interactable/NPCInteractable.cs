﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using System;
using PixelCrushers.DialogueSystem;
using Intelligensia.UI.HUD;
using TMPro;

public class NPCInteractable : ObjectInteractableBase, IInteractable 
{
    
    
    public CinemachineVirtualCamera[] vCamAngle;

    [Header("Button Input Labels")]
    [SerializeField] private ButtonInputLabelId[] inputLabelsChallengable = default;
    [SerializeField] private ButtonInputLabelId[] inputLabelsNonChallengable = default;

    [Header("UI")]
    [SerializeField] private ViewController talkBubble = default;
    [SerializeField] private GameObject challengedIcon = default;
    [SerializeField] private GameObject challengableIcon = default;
    [SerializeField] private CanvasGroup challengableCanvasGroup = default;
    [SerializeField] private TextMeshProUGUI nameText = default;

    [Header("Battle")]
    [SerializeField] private bool canBattle = false; // TODO : Add custom inspector that can hide this if cannot battle

    [Tooltip("Click on the 3 dots on the right side > Setup Info. To fill the rewards and items fields.")] 
    public ChallengeInfo challengeInfo;

    [SerializeField] private string isChallengedVariableName = "IsChallenged";

    //[Header("Lua")]
    //[SerializeField] private bool unregisterOnDisable = true;

    public bool CanBattle => canBattle;

    private bool _hasChallengeAdded = false;
    public bool hasChallengeAdded
    {
        get
        {
            return _hasChallengeAdded;
        }
        set
        {
            _hasChallengeAdded = value;

            if (challengableCanvasGroup != null)
                challengableCanvasGroup.alpha = _hasChallengeAdded ? 0.5f : 1f;

            if (challengedIcon != null)
                challengedIcon.SetActive(_hasChallengeAdded);
        }
    }



    private void OnEnable()
    {
        if (NPCInteractionManager.instance != null)
            NPCInteractionManager.instance.RegisterNPC(this);

        DailyInfoManager.ChallengeRemovedEvent += OnChallengeRemoved;
    }

    private void OnDisable()
    {
        if (NPCInteractionManager.instance != null)
            NPCInteractionManager.instance.UnregisterNPC(this);

        DailyInfoManager.ChallengeRemovedEvent -= OnChallengeRemoved;
    }

    private void Start()
    {
        if(canBattle)
            hasChallengeAdded = DailyInfoManager.IsChallengedAlready(challengeInfo);

        if (nameText != null)
            nameText.text = ObjName;

        ToggleChallengableIcon(canBattle);
    }


    protected override ButtonInputLabelId[] GetButtonInputLabelIds()
    {
        if (canBattle && !hasChallengeAdded)
            return inputLabelsChallengable;
        else
            return inputLabelsNonChallengable;
    }

    protected override void ToggleInteractionBubble(bool active)
    {
        if(talkBubble != null && !hasChallengeAdded)
        {
            if (active)
                talkBubble.Show();
            else
                talkBubble.Close();
        }

        if(nameText != null)
        {
            nameText.transform.parent.gameObject.SetActive(active);
        }
    }

    private void ToggleChallengableIcon(bool active)
    {
        if (challengableIcon != null)
            challengableIcon.SetActive(active);
    }

    #region Interactable Implementation

    public override void StartInteraction(Transform interactor)
    {
        if (GameUIManager.instance.HasActiveView)
        {
            return;
        }

        ToggleIsChallengedDialogue(false);

        if (NPCInteractionManager.instance != null)
            NPCInteractionManager.instance.SetCurrentInteractingNPC(ObjName);

        base.StartInteraction(interactor);
    }

    public override void EndInteraction()
    {
        if (NPCInteractionManager.instance != null)
            NPCInteractionManager.instance.SetCurrentInteractingNPC("");

        base.EndInteraction();
    }

    #endregion

    public void ChallengeInteraction(Transform interactor)
    {
        if (GameUIManager.instance.HasActiveView)
        {
            return;
        }

        if (hasChallengeAdded)
            return;

        ToggleIsChallengedDialogue(true);

        if (NPCInteractionManager.instance != null)
            NPCInteractionManager.instance.SetCurrentInteractingNPC(ObjName);

        base.StartInteraction(interactor);
    }

    public void SetInteractionCamera(int index)
    {
        for(int i = 0; i < vCamAngle.Length; i++)
        {
            if(i == index)
            {
                vCamAngle[i].gameObject.SetActive(true);
            }
            else
            {
                vCamAngle[i].gameObject.SetActive(false);
            }
        }
    }

    //public void TriggerBattle()
    //{
    //    if (BattleController.instance == null)
    //        return;

    //    BattleController.instance.TriggerBattle(challengeInfo);
    //}

    public void ToggleIsChallengedDialogue(bool active)
    {
        //Lua.Result isChallenged = DialogueLua.GetVariable(isChallengedVariableName);

        DialogueLua.SetVariable(isChallengedVariableName, active);
    }

    private void OnChallengeRemoved(string id)
    {
        if (id == challengeInfo.id)
            hasChallengeAdded = false;
    }



#if UNITY_EDITOR
    [ContextMenu("Setup Info")]
    public void SetupInfo()
    {
        challengeInfo.SetupInfo();
    }
#endif
}
