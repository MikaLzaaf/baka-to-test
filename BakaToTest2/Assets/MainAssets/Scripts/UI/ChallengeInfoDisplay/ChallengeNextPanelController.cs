﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Intelligensia.UI.Challenges
{
    public class ChallengeNextPanelController : UIViewController
    {
        [Header("Panel")]
        [SerializeField] private UIViewController buttonView = default;
        [SerializeField] private GameObject menuButton = default;


        public override void Show()
        {
            base.Show();

            BattlePhasesController.instance.ToggleCameraBrain(false);

            UINavigator.instance.OpenNewWindow(buttonView, buttonView.content);
        }

        public void OpenChallengeMenu()
        {
            var challengeMenu = GameUIManager.instance.challengesMenuController;

            if(challengeMenu != null)
            {
                UINavigator.instance.OpenNewWindow(challengeMenu, challengeMenu.content);
            }
        }

        public void OpenPlayerMenu()
        {
            var playerMenu = GameUIManager.instance.gameMainMenu;

            if(playerMenu != null)
            {
                playerMenu.Show();
            }
        }

        public void StartNextChallenge()
        {
            ChallengeInfo challengeInfo = DailyInfoManager.GetIncompleteChallenge();

            BattleController.instance.TriggerBattle(challengeInfo, true);

            StartCoroutine(WaitForLoadingScreenReady());
        }


        private IEnumerator WaitForLoadingScreenReady()
        {
            yield return new WaitUntil(() => LoadingScreen.instance.viewState == ViewState.Ready);

            Close();

            UINavigator.instance.ClearWindowsList();
        }
    }
}

