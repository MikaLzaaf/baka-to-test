﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace Intelligensia.UI.Challenges
{
    public class ChallengeItemListItem : MonoBehaviour
    {
        [SerializeField] private Image itemIcon = default;
        [SerializeField] private TextMeshProUGUI nameText = default;



        public void InitializeItem(Item item)
        {
            if (item == null)
                return;

            itemIcon.sprite = IconLoader.GetItemIcon(item.itemValue.ItemIcon);

            nameText.text = item.itemValue.ItemName;
        }

    }
}

