﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Intelligensia.UI.Challenges
{
    public class ChallengeButtonInputLabel : MonoBehaviour
    {
        public ButtonInputLabelId inputLabelId;
    }

    public enum ButtonInputLabelId
    {
        Undefined,
        Remove,
        Close,
        Battle,
        Confirm
    }
}

