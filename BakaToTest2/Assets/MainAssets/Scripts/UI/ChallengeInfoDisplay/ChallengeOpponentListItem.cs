﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace Intelligensia.UI.Challenges
{
    public class ChallengeOpponentListItem : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI classNameText = default;
        [SerializeField] private TextMeshProUGUI opponentNameText = default;


        private const string ClassPrefix = "Class ";


        public void InitializeItem(string className, string opponentName)
        {
            classNameText.text = ClassPrefix + className;
            opponentNameText.text = opponentName;
        }

    }
}

