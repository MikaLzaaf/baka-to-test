﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace Intelligensia.UI.Challenges
{
    public class ChallengeDetailsPanelController : ViewController
    {
        [Header("UI")]
        [SerializeField] private TextMeshProUGUI nameText = default;
        [SerializeField] private TextMeshProUGUI gradeText = default;
        [SerializeField] private GameObject completeLabel = default;

        [Header("ButtonIcons")]
        [SerializeField] private GameObject[] buttonIcons = default;

        [Header("Character Info")]
        [SerializeField] private ChallengeCharacterInfoListItem infoListItemPrefab = default;
        [SerializeField] private Transform infoListContainer = default;

        [Header("Ilmu List")]
        [SerializeField] private Transform ilmuListContainer = default;
        [SerializeField] private ChallengeIlmuListItem ilmuListItemPrefab = default;

        [Header("Item List")]
        [SerializeField] private Transform itemListContainer = default;
        [SerializeField] private ChallengeItemListItem itemListItemPrefab = default;

        private List<ChallengeCharacterInfoListItem> infoListItems;
        private List<ChallengeIlmuListItem> ilmuListItems;
        private List<ChallengeItemListItem> itemListItems;

        private int currentSelectedOpponentIndex = 0;
        private int currentOpponentsCount = 0;

        private ChallengeInfo challengeInfo;


        protected override void OnViewReady()
        {
            base.OnViewReady();

            ChallengesMenuController menuController = GameUIManager.instance.challengesMenuController;

            if (menuController != null)
            {
                menuController.OnRightTabPressedEvent += NextOpponent;
                menuController.OnLeftTabPressedEvent += PreviousOpponent;
            }
        }

        public override void Close()
        {
            base.Close();

            ChallengesMenuController menuController = GameUIManager.instance.challengesMenuController;

            if (menuController != null)
            {
                menuController.OnRightTabPressedEvent -= NextOpponent;
                menuController.OnLeftTabPressedEvent -= PreviousOpponent;
            }
        }


        public void ShowInfo(ChallengeInfo challengeInfo)
        {
            this.challengeInfo = challengeInfo;

            ToggleButtonIcons(challengeInfo.opponents.Length > 1);
            ToggleCompleteLabel(challengeInfo.isCompleted);

            currentSelectedOpponentIndex = 0;
            currentOpponentsCount = challengeInfo.opponents.Length;

            BattleEntity opponentPrefab = challengeInfo.opponents[currentSelectedOpponentIndex].GetCharacterPrefab();

            SetNameAndGrade(opponentPrefab);

            GenerateInfoList(opponentPrefab);

            GenerateIlmuList(challengeInfo.ilmuRewards);

            GenerateItemList(challengeInfo.obtainableItems);

            Show();
        }

        private void SetNameAndGrade(BattleEntity opponentPrefab)
        {
            nameText.text = opponentPrefab.characterName;
            gradeText.text = opponentPrefab.CharacterStatsBase.GetOverallGrade();
        }


        private void GenerateInfoList(BattleEntity opponentPrefab)
        {
            if (infoListItems != null)
            {
                for (int i = infoListItems.Count - 1; i >= 0; i--)
                {
                    Destroy(infoListItems[i].gameObject);
                }

                infoListItems.Clear();
            }
            else
            {
                infoListItems = new List<ChallengeCharacterInfoListItem>();
            }

            var opponentInfos = opponentPrefab.CharacterStatsBase.characterInfos;

            for (int i = 0; i < opponentInfos.Count; i++)
            {
                ChallengeCharacterInfoListItem listItem = Instantiate(infoListItemPrefab, infoListContainer, false);
                infoListItems.Add(listItem);

                listItem.InitializeItem(opponentInfos[i]);
            }
        }

        private void GenerateIlmuList(IlmuCost[] ilmuRewards)
        {
            if (ilmuListItems != null)
            {
                for (int i = ilmuListItems.Count - 1; i >= 0; i--)
                {
                    Destroy(ilmuListItems[i].gameObject);
                }

                ilmuListItems.Clear();
            }
            else
            {
                ilmuListItems = new List<ChallengeIlmuListItem>();
            }

            for (int i = 0; i < ilmuRewards.Length; i++)
            {
                ChallengeIlmuListItem listItem = Instantiate(ilmuListItemPrefab, ilmuListContainer, false);
                ilmuListItems.Add(listItem);

                listItem.InitializeItem(ilmuRewards[i].subject, ilmuRewards[i].cost);
            }
        }

        private void GenerateItemList(Item[] obtainableItems)
        {
            if (itemListItems != null)
            {
                for (int i = itemListItems.Count - 1; i >= 0; i--)
                {
                    Destroy(itemListItems[i].gameObject);
                }

                itemListItems.Clear();
            }
            else
            {
                itemListItems = new List<ChallengeItemListItem>();
            }

            for (int i = 0; i < obtainableItems.Length; i++)
            {
                ChallengeItemListItem listItem = Instantiate(itemListItemPrefab, itemListContainer, false);
                itemListItems.Add(listItem);

                listItem.InitializeItem(obtainableItems[i]);
            }
        }

        private void ToggleButtonIcons(bool active)
        {
            if (buttonIcons != null)
            {
                for (int i = 0; i < buttonIcons.Length; i++)
                {
                    buttonIcons[i].SetActive(active);
                }
            }
        }

        private void ToggleCompleteLabel(bool active)
        {
            if (completeLabel != null)
                completeLabel.SetActive(active);
        }


        private void SwitchOpponent(int value)
        {
            currentSelectedOpponentIndex += value;

            if (currentSelectedOpponentIndex >= currentOpponentsCount)
            {
                currentSelectedOpponentIndex = 0;
            }
            else if (currentSelectedOpponentIndex < 0)
            {
                currentSelectedOpponentIndex = currentOpponentsCount - 1;
            }

            BattleEntity opponentPrefab = challengeInfo.opponents[currentSelectedOpponentIndex].GetCharacterPrefab();

            SetNameAndGrade(opponentPrefab);

            GenerateInfoList(opponentPrefab);
        }

        private void NextOpponent()
        {
            SwitchOpponent(1);
        }

        private void PreviousOpponent()
        {
            SwitchOpponent(-1);
        }
    }
}

