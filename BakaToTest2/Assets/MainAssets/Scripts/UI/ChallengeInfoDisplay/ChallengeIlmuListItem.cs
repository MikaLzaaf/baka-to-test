﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


namespace Intelligensia.UI.Challenges
{
    public class ChallengeIlmuListItem : MonoBehaviour
    {
        [SerializeField] private Image subjectIcon = default;
        [SerializeField] private TextMeshProUGUI amountText = default;
        [SerializeField] private CanvasGroup canvasGroup = default;

        [SerializeField] private Color bmColor = default;
        [SerializeField] private Color englishColor = default;
        [SerializeField] private Color mathsColor = default;
        [SerializeField] private Color historyColor = default;
        [SerializeField] private Color peColor = default;
        [SerializeField] private Color physicsColor = default;
        [SerializeField] private Color biologyColor = default;
        [SerializeField] private Color chemistryColor = default;




        public void InitializeItem(Subjects subject, int amount)
        {
            subjectIcon.sprite = IconLoader.GetSubjectIcon(subject);

            SetColor(subject);

            amountText.text = amount.ToString();

            ToggleCanvasGroup(amount > 0);
        }

        private void SetColor(Subjects subject)
        {
            if (subjectIcon == null)
                return;

            if(subject == Subjects.BahasaMelayu)
            {
                subjectIcon.color = bmColor;
            }
            else if (subject == Subjects.English)
            {
                subjectIcon.color = englishColor;
            }
            else if (subject == Subjects.Mathematics)
            {
                subjectIcon.color = mathsColor;
            }
            else if (subject == Subjects.History)
            {
                subjectIcon.color = historyColor;
            }
            else if (subject == Subjects.PE)
            {
                subjectIcon.color = peColor;
            }
            else if (subject == Subjects.Physics)
            {
                subjectIcon.color = physicsColor;
            }
            else if (subject == Subjects.Biology)
            {
                subjectIcon.color = biologyColor;
            }
            else if (subject == Subjects.Chemistry)
            {
                subjectIcon.color = chemistryColor;
            }
        }

        private void ToggleCanvasGroup(bool active)
        {
            if (canvasGroup != null)
            {
                canvasGroup.alpha = active ? 1f : 0.5f;
            }
        }
    }
}

