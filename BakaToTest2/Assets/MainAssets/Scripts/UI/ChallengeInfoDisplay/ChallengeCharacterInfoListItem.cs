﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace Intelligensia.UI.Challenges
{
    public class ChallengeCharacterInfoListItem : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI infoText = default;


        public void InitializeItem(CharacterInfo characterInfo)
        {
            infoText.text = characterInfo.GetLocalizedInfo();
        }
    }

}
