﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using System;
using Intelligensia.UI.Challenges;

public class ChallengesMenuController : UIViewController
{

    public event Action OnRightTabPressedEvent;
    public event Action OnLeftTabPressedEvent;
    public event Action OnCancelButtonPressedEvent;
    public event Action OnInfoButtonPressedEvent;
    public event Action OnNorthButtonPressedEvent;


    [Header("UI")]
    [SerializeField] private ChallengeNextPanelController nextChallengePanelController = default;
    [SerializeField] private ChallengeInfoDisplayController challengeInfoDisplayController = default;
    [SerializeField] private ChallengeDetailsPanelController challengeDetailsPanelController = default;
    [SerializeField] private ChallengeSlotController[] challengeSlots = default;
    [SerializeField] private ChallengeButtonInputLabel[] inputLabels = default;

    [Header("Confirmation Panel")]
    [SerializeField] private ViewController confirmationPanel = default;
    [SerializeField] private GameObject noButton = default;
    [SerializeField] private GameObject reminderPanel = default;
    [SerializeField] private GameObject removeLabel = default;
    [SerializeField] private GameObject startLabel = default;

    private Dictionary<ButtonInputLabelId, ChallengeButtonInputLabel> inputLabelLookup;

    private ChallengeSlotController selectedSlot => challengeSlots[currentSelectedSlotIndex];
    private int currentSelectedSlotIndex;


    protected override void Awake()
    {
        base.Awake();

        inputLabelLookup = new Dictionary<ButtonInputLabelId, ChallengeButtonInputLabel>();

        if(inputLabels != null)
        {
            for(int i = 0; i < inputLabels.Length; i++)
            {
                inputLabelLookup.Add(inputLabels[i].inputLabelId, inputLabels[i]);
            }
        }
    }

    public override void Show()
    {
        base.Show();

        GenerateSlots();
    }

    private void GenerateSlots()
    {
        currentSelectedSlotIndex = 0;

        var challengeList = DailyInfoManager.currentChallengeInfos;

        for (int i = 0; i < challengeSlots.Length; i++)
        {
            if (i < challengeList.Count)
                challengeSlots[i].InitializeSlot(challengeList[i].opponents, challengeList[i].isCompleted);
            else
                challengeSlots[i].InitializeSlot(null, false);
        }

        if (challengeList.Count > 0)
            UINavigator.instance.SetDefaultSelectedInRuntime(challengeSlots[0].gameObject);

        ToggleInputLabel(ButtonInputLabelId.Battle, DailyInfoManager.HasNextChallenge());
        ToggleInputLabel(ButtonInputLabelId.Close, true);
        ToggleInputLabel(ButtonInputLabelId.Confirm, false);

    }

    public void ShowChallengeDetails(int index)
    {
        currentSelectedSlotIndex = index;

        if (currentSelectedSlotIndex >= DailyInfoManager.currentChallengeInfos.Count)
        {
            challengeDetailsPanelController.Close();
            ToggleInputLabel(ButtonInputLabelId.Remove, false);
            return;
        }

        ToggleInputLabel(ButtonInputLabelId.Remove, true);

        ChallengeInfo challengeInfo = DailyInfoManager.currentChallengeInfos[currentSelectedSlotIndex];

        challengeDetailsPanelController.ShowInfo(challengeInfo);
    }

    public void ShowChallengeInfo(ChallengeInfo challengeInfo)
    {
        if (challengeInfoDisplayController != null)
            challengeInfoDisplayController.ShowInfo(challengeInfo);
    }

    public void CloseChallengeInfo()
    {
        if (challengeInfoDisplayController != null)
            challengeInfoDisplayController.Close();
    }

    public void ShowNextChallenge()
    {
        if (nextChallengePanelController != null)
        {
            nextChallengePanelController.Show();
        }
    }

    private ChallengeButtonInputLabel GetButtonInputLabel(ButtonInputLabelId inputLabelId)
    {
        inputLabelLookup.TryGetValue(inputLabelId, out ChallengeButtonInputLabel buttonInputLabel);
        return buttonInputLabel;
    }

    public void ToggleInputLabel(ButtonInputLabelId inputLabelId, bool active)
    {
        ChallengeButtonInputLabel removeInputLabel = GetButtonInputLabel(inputLabelId);

        if(removeInputLabel != null)
        {
            removeInputLabel.gameObject.SetActive(active);
        }
    }

    public void RemoveChallenge()
    {
        ChallengeInfo challengeToRemove = DailyInfoManager.currentChallengeInfos[currentSelectedSlotIndex];

        DailyInfoManager.RemoveChallenge(challengeToRemove);

        GenerateSlots();

        ToggleConfirmationPanel(false);
    }

    private void ToggleConfirmationPanel(bool active)
    {
        if(confirmationPanel != null)
        {
            if (active)
            {
                confirmationPanel.Show();

                UINavigator.instance.SetDefaultSelectedInRuntime(noButton);

                ToggleInputLabel(ButtonInputLabelId.Battle, false);
                ToggleInputLabel(ButtonInputLabelId.Remove, false);

                ToggleInputLabel(ButtonInputLabelId.Confirm, true);
            }
            else
            {
                confirmationPanel.Close();

                UINavigator.instance.SetDefaultSelectedInRuntime(challengeSlots[currentSelectedSlotIndex].gameObject);

                ToggleInputLabel(ButtonInputLabelId.Battle, true);
                ToggleInputLabel(ButtonInputLabelId.Remove, true);

                ToggleInputLabel(ButtonInputLabelId.Confirm, false);
            }
        }
    }


    private void ClosePanel(bool closeImmediately = false)
    {
        if(closeImmediately)
        {
            confirmationPanel.CloseImmediately();
            Close();
            return;
        }
 
        if(BattleController.IsInBattle)
        {
            UINavigator.instance.OpenPreviousWindow();
            return;
        }

        if (selectedSlot != null && selectedSlot.isRemoving)
            selectedSlot.CloseConfirmationPanel();
        else if (confirmationPanel.viewState == ViewState.Ready && !selectedSlot.isRemoving)
            ToggleConfirmationPanel(false);
        else if (confirmationPanel.viewState == ViewState.Closed && !selectedSlot.isRemoving)
            GameUIManager.instance.ToggleChallengesMenu(false);
    }

    public void TriggerBattle()
    {
        if (BattleController.instance == null)
            return;

        if (DailyInfoManager.currentChallengesCount <= 0)
            return;

        ClosePanel(true);

        ChallengeInfo challengeInfo = DailyInfoManager.GetIncompleteChallenge();

        BattleController.instance.TriggerBattle(challengeInfo);
    }

    public void CloseConfirmationPanel()
    {
        ToggleConfirmationPanel(false);
    }

    #region Input related

    public void ClosePanel(InputAction.CallbackContext callbackContext)
    {
        if(callbackContext.performed)
        {
            ClosePanel();
        }
    }

    public void OnInfoButtonPressed(InputAction.CallbackContext callbackContext)
    {
        if (callbackContext.performed)
        {
            if(DailyInfoManager.HasNextChallenge() && !selectedSlot.isRemoving)
            {
                ToggleConfirmationPanel(true);
            }
        }
    }

    public void OnNorthButtonPressed(InputAction.CallbackContext callbackContext)
    {
        if (callbackContext.performed)
        {
            if(currentSelectedSlotIndex < DailyInfoManager.currentChallengeInfos.Count && 
                confirmationPanel.viewState == ViewState.Closed)
            {
                if(selectedSlot != null)
                    selectedSlot.ShowConfirmationPanel();
            }
        }
    }

    public void OnRightTabPressed(InputAction.CallbackContext callbackContext)
    {
        if (callbackContext.performed)
        {
            OnRightTabPressedEvent?.Invoke();
        }
    }

    public void OnLeftTabPressed(InputAction.CallbackContext callbackContext)
    {
        if (callbackContext.performed)
        {
            OnLeftTabPressedEvent?.Invoke();
        }
    }

#endregion
}
