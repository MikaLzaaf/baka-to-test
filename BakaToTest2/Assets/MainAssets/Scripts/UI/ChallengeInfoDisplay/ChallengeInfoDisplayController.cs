﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace Intelligensia.UI.Challenges
{
    public class ChallengeInfoDisplayController : ViewController
    {

        [Header("Opponent List")]
        [SerializeField] private Transform opponentListContainer = default;
        [SerializeField] private ChallengeOpponentListItem opponentListItemPrefab = default;

        [Header("Ilmu List")]
        [SerializeField] private Transform ilmuListContainer = default;
        [SerializeField] private ChallengeIlmuListItem ilmuListItemPrefab = default;

        [Header("Item List")]
        [SerializeField] private Transform itemListContainer = default;
        [SerializeField] private ChallengeItemListItem itemListItemPrefab = default;

        private List<ChallengeOpponentListItem> opponentListItems;
        private List<ChallengeIlmuListItem> ilmuListItems;
        private List<ChallengeItemListItem> itemListItems;


        public void ShowInfo(ChallengeInfo challengeInfo)
        {
            GenerateOpponentList(challengeInfo.opponents);

            GenerateIlmuList(challengeInfo.ilmuRewards);

            GenerateItemList(challengeInfo.obtainableItems);

            Show();
        }


        private void GenerateOpponentList(BattlePartyMember[] opponents)
        {
            if (opponentListItems != null)
            {
                for (int i = opponentListItems.Count - 1; i >= 0; i--)
                {
                    Destroy(opponentListItems[i].gameObject);
                }

                opponentListItems.Clear();
            }
            else
            {
                opponentListItems = new List<ChallengeOpponentListItem>();
            }

            for (int i = 0; i < opponents.Length; i++)
            {
                ChallengeOpponentListItem listItem = Instantiate(opponentListItemPrefab, opponentListContainer, false);
                opponentListItems.Add(listItem);

                listItem.InitializeItem(opponents[i].className, opponents[i].GetCharacterName());
            }
        }

        private void GenerateIlmuList(IlmuCost[] ilmuRewards)
        {
            if (ilmuListItems != null)
            {
                for (int i = ilmuListItems.Count - 1; i >= 0; i--)
                {
                    Destroy(ilmuListItems[i].gameObject);
                }

                ilmuListItems.Clear();
            }
            else
            {
                ilmuListItems = new List<ChallengeIlmuListItem>();
            }

            for (int i = 0; i < ilmuRewards.Length; i++)
            {
                ChallengeIlmuListItem listItem = Instantiate(ilmuListItemPrefab, ilmuListContainer, false);
                ilmuListItems.Add(listItem);

                listItem.InitializeItem(ilmuRewards[i].subject, ilmuRewards[i].cost);
            }
        }

        private void GenerateItemList(Item[] obtainableItems)
        {
            if (itemListItems != null)
            {
                for (int i = itemListItems.Count - 1; i >= 0; i--)
                {
                    Destroy(itemListItems[i].gameObject);
                }

                itemListItems.Clear();
            }
            else
            {
                itemListItems = new List<ChallengeItemListItem>();
            }

            for (int i = 0; i < obtainableItems.Length; i++)
            {
                ChallengeItemListItem listItem = Instantiate(itemListItemPrefab, itemListContainer, false);
                itemListItems.Add(listItem);

                listItem.InitializeItem(obtainableItems[i]);
            }
        }
    }
}

