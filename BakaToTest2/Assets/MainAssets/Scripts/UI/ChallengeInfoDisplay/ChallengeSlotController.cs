﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace Intelligensia.UI.Challenges
{
    public class ChallengeSlotController : Button, ISelectHandler
    {
        [SerializeField] private ChallengesMenuController menuController = default;
        [SerializeField] private int slotIndex = 0;
        [SerializeField] private ChallengeOpponentListItem opponentListItemPrefab = default;
        [SerializeField] private Transform listContainer = default;
        [SerializeField] private GameObject highlighter = default;

        [Header("Contents")]
        [SerializeField] private GameObject content = default;
        [SerializeField] private GameObject noChallengeContent = default;
        [SerializeField] private GameObject completeContent = default;

        [Header("Remove Confirmation")]
        [SerializeField] private GameObject confirmationPanel = default;
        [SerializeField] private GameObject noButton = default;


        private List<ChallengeOpponentListItem> listItems;
        public bool isRemoving { get; private set; }


        protected override void OnDisable()
        {
            base.OnDisable();

            ToggleHighlight(false);
        }


        public void InitializeSlot(BattlePartyMember[] opponents, bool isCompleted)
        {
            if (listItems != null)
            {
                for (int i = listItems.Count - 1; i >= 0; i--)
                {
                    Destroy(listItems[i].gameObject);
                }

                listItems.Clear();
            }
            else
            {
                listItems = new List<ChallengeOpponentListItem>();
            }

            if (opponents == null)
            {
                ToggleContents(false);
            }
            else
            {
                for (int i = 0; i < opponents.Length; i++)
                {
                    ChallengeOpponentListItem listItem = Instantiate(opponentListItemPrefab, listContainer, false);
                    listItems.Add(listItem);

                    listItem.InitializeItem(opponents[i].className, opponents[i].GetCharacterName());
                }

                ToggleContents(true);
            }

            ToggleCompleteLabel(isCompleted);
            ToggleConfirmationPanel(false);
        }


        private void ToggleContents(bool hasOpponent)
        {
            if (content != null)
                content.SetActive(hasOpponent);

            if (noChallengeContent != null)
                noChallengeContent.SetActive(!hasOpponent);
        }

        private void ToggleCompleteLabel(bool active)
        {
            if (completeContent != null)
                completeContent.SetActive(active);
        }

        private void ToggleHighlight(bool active)
        {
            if (highlighter != null)
                highlighter.SetActive(active);
        }

        private void ToggleConfirmationPanel(bool active)
        {
            if (confirmationPanel != null)
                confirmationPanel.SetActive(active);

            ToggleInputLabels(active);

            isRemoving = active;
        }

        public override void OnSelect(BaseEventData eventData)
        {
            base.OnSelect(eventData);

            ToggleHighlight(true);

            menuController.ShowChallengeDetails(slotIndex);
        }

        public override void OnDeselect(BaseEventData eventData)
        {
            base.OnDeselect(eventData);

            if(!isRemoving)
                ToggleHighlight(false);
        }

        public void RemoveChallenge()
        {
            menuController.RemoveChallenge();

            isRemoving = false;
        }

        public void ShowConfirmationPanel()
        {
            ToggleConfirmationPanel(true);

            UINavigator.instance.SetDefaultSelectedInRuntime(noButton);

            isRemoving = true;
        }

        public void CloseConfirmationPanel()
        {
            ToggleConfirmationPanel(false);

            UINavigator.instance.SetDefaultSelectedInRuntime(gameObject);

            isRemoving = false;
        }

        private void ToggleInputLabels(bool active)
        {
            menuController.ToggleInputLabel(ButtonInputLabelId.Confirm, active);

            menuController.ToggleInputLabel(ButtonInputLabelId.Remove, !active);
            menuController.ToggleInputLabel(ButtonInputLabelId.Battle, !active);
        }
    }
}

