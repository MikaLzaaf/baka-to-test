﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

public class LoadingScreen : MonoBehaviour
{
    public static LoadingScreen instance;

    public GameObject loadingScreen;
    public CanvasGroup canvasGroup;

    public float fadeDuration = 1f;
    public float holdAfterCompleteDuration = 1f;
    public bool testLoad = false;

    [Header("Scene Names")]
    public string sceneToLoad;
    public string battleScene = "BattleScene 1";

    public event Action<string> LoadingNewSceneCompletedEvent;
    public event Action LoadingCompletedEvent;
    public event Action ViewClosedEvent;

    private string preBattleScene;

    public ViewState viewState { get; private set; }


    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Update()
    {
        if(testLoad)
        {
            StartLoadingNewScene(sceneToLoad, "");
            testLoad = false;
        }
    }


    public void StartLoadingNewScene(string sceneToLoad, string previousSceneToUnload, bool isSceneAdditive = false)
    {
        StartCoroutine(StartLoadNewScene(sceneToLoad, previousSceneToUnload, isSceneAdditive));
    }

    public void StartLoadingBattleScene(bool reload)
    {
        if(reload)
            StartCoroutine(StartLoadNewScene(battleScene, battleScene, true, true));
        else
        {
            preBattleScene = SceneManager.GetActiveScene().name;

            StartCoroutine(StartLoadNewScene(battleScene, "", true, true));
        }  
    }

    public void UnloadBattleScene()
    {
        StartCoroutine(StartLoadNewScene(preBattleScene, battleScene, false, true));
    }


    public void StartLoadingEmpty()
    {
        StartCoroutine(StartLoad());
    }

    private IEnumerator StartLoadNewScene(string sceneToLoad, string previousSceneToUnload, bool isSceneAdditive, bool setNewSceneAsActiveScene = false)
    {
        // TODO : Add SFX later

        yield return StartCoroutine(FadeLoadingScreen(1, fadeDuration));

        yield return StartCoroutine(UnloadScene(previousSceneToUnload));

        yield return StartCoroutine(LoadNewScene(sceneToLoad, isSceneAdditive, setNewSceneAsActiveScene));

        yield return new WaitForSeconds(holdAfterCompleteDuration);

        yield return StartCoroutine(FadeLoadingScreen(0, fadeDuration));

        ViewClosedEvent?.Invoke();

        loadingScreen.SetActive(false);
    }

    private IEnumerator LoadNewScene(string sceneToLoad, bool isAdditiveLoading, bool setNewSceneAsActiveScene)
    {
        if (!SceneManager.GetSceneByName(sceneToLoad).isLoaded && !string.IsNullOrEmpty(sceneToLoad))
        {
            AsyncOperation operation = isAdditiveLoading ? SceneManager.LoadSceneAsync(sceneToLoad, LoadSceneMode.Additive) :
          SceneManager.LoadSceneAsync(sceneToLoad);

            while (!operation.isDone)
            {
                yield return null;
            }

            if(setNewSceneAsActiveScene)
                SceneManager.SetActiveScene(SceneManager.GetSceneByName(sceneToLoad));
        }

        LoadingNewSceneCompletedEvent?.Invoke(sceneToLoad);
    }

    private IEnumerator UnloadScene(string sceneToUnload)
    {
        if (!string.IsNullOrEmpty(sceneToUnload))
        {
            var unloadedScene = SceneManager.GetSceneByName(sceneToUnload);

            if (unloadedScene != null)
            {
                AsyncOperation unloadingOperation = SceneManager.UnloadSceneAsync(unloadedScene);

                while (!unloadingOperation.isDone)
                {
                    yield return null;
                }
            }
        }
    }

    private IEnumerator StartLoad()
    {
        if (canvasGroup != null)
        {
            canvasGroup.alpha = 0f;
        }

        loadingScreen.SetActive(true);

        // TODO : Add SFX later

        yield return StartCoroutine(FadeLoadingScreen(1, fadeDuration));

        //LoadingNewSceneCompletedEvent?.Invoke();

        //yield return new WaitForSeconds(holdAfterCompleteDuration);

        LoadingCompletedEvent?.Invoke();

        yield return new WaitForSeconds(holdAfterCompleteDuration);

        yield return StartCoroutine(FadeLoadingScreen(0, fadeDuration));

        ViewClosedEvent?.Invoke();

        loadingScreen.SetActive(false);
    }

    private IEnumerator FadeLoadingScreen(float targetValue, float duration)
    {
        if(!loadingScreen.activeSelf)
            loadingScreen.SetActive(true);

        if (targetValue > 0)
        {
            if (canvasGroup != null)
                canvasGroup.alpha = 0f;
        }

        float startValue = canvasGroup.alpha;
        float time = 0;

        while (time < duration)
        {
            canvasGroup.alpha = Mathf.Lerp(startValue, targetValue, time / duration);
            time += Time.deltaTime;
            yield return null;
        }

        viewState = targetValue > 0 ? ViewState.Ready : ViewState.Closed;

        canvasGroup.alpha = targetValue;
    }
}
