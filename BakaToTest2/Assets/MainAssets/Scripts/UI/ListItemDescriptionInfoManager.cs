﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ListItemDescriptionInfoManager 
{
    private const string SelfTarget = "Self";
    private const string SingleEnemyTarget = "1 Enemy";
    private const string SinglePartyMemberTarget = "1 Ally";
    private const string AllEnemiesTarget = "All Enemies";
    private const string AllPartyMembersTarget = "All Allies";

    private const string SingleTarget = "Single";
    private const string MultipleTarget = "All";

    private const string SkillPowerLight = "Light";
    private const string SkillPowerMedium = "Medium";
    private const string SkillPowerHeavy = "Heavy";
    private const string SkillPowerExtreme = "Extreme";



    public static string GetTargetAmountDescription(AimedTo aimedTo)
    {
        string targetAmount = "";

        if (aimedTo == AimedTo.Self)
        {
            targetAmount = SelfTarget;
        }
        else if (aimedTo == AimedTo.SingleEnemy)
        {
            targetAmount = SingleTarget;
        }
        else if (aimedTo == AimedTo.SinglePartyMember)
        {
            targetAmount = SingleTarget;
        }
        else if (aimedTo == AimedTo.AllEnemies)
        {
            targetAmount = MultipleTarget;
        }
        else if (aimedTo == AimedTo.AllPartyMembers)
        {
            targetAmount = MultipleTarget;
        }

        return targetAmount;
    }


    public static string GetSkillPowerDescription(int skillLevel)
    {
        string skillPower = "";

        if (skillLevel == 1)
        {
            skillPower = SkillPowerLight;
        }
        else if (skillLevel == 2)
        {
            skillPower = SkillPowerMedium;
        }
        else if (skillLevel == 3)
        {
            skillPower = SkillPowerHeavy;
        }
        else if (skillLevel == 4)
        {
            skillPower = SkillPowerExtreme;
        }

        return skillPower;
    }
}
