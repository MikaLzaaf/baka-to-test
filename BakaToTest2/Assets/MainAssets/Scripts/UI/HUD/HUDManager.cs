﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Intelligensia.UI.HUD;

public class HUDManager : MonoBehaviour
{
    [Header("Panels")]
    [SerializeField] private HUDButtonInputController buttonInputController = default;
    [SerializeField] private HUDDateWeatherController dateWeatherController = default;
    [SerializeField] private HUDChallengesController challengesController = default;



    private void Awake()
    {
        if (buttonInputController != null)
            buttonInputController.Setup();
    }

    private void Start()
    {
        buttonInputController.Close();
        dateWeatherController.Show();
        challengesController.Show();
    }


    public void UpdateButtonInputLabels(ButtonInputLabelId[] inputLabelsToDisplay)
    {
        if(buttonInputController != null)
        {
            if (inputLabelsToDisplay == null)
                buttonInputController.Close();
            else
                buttonInputController.ShowInputLabels(inputLabelsToDisplay);
        }
    }
}
