﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace Intelligensia.UI.HUD
{
    public class HUDChallengesController : ViewController
    {
        [Header("UI")]
        [SerializeField] private TextMeshProUGUI countText = default;
        [SerializeField] private HUDButtonInputLabel buttonInputLabel = default;

        private const string CountFormat = "{0} <size=60%><color=#ffff>/ {1}";

        private int maxCount = 0;
        //public int currentCount { get; private set; }



        protected override void Awake()
        {
            base.Awake();

            DailyInfoManager.ChallengesUpdateEvent += UpdateCount;

            maxCount = DailyInfoManager.MaxChallengesCount;
        }

        private void OnDestroy()
        {
            DailyInfoManager.ChallengesUpdateEvent -= UpdateCount;
        }


        public override void Show()
        {
            base.Show();

            UpdateCount(DailyInfoManager.currentChallengesCount);
        }


        public void UpdateCount(int newCount)
        {
            if (countText == null)
                return;

            countText.text = string.Format(CountFormat, newCount, maxCount);
        }
    }
}

