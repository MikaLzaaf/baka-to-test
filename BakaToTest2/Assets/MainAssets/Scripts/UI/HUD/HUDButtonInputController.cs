﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Intelligensia.UI.HUD
{
    public class HUDButtonInputController : ViewController
    {
        [Header("Button Input Labels")]
        [SerializeField] private HUDButtonInputLabel[] buttonInputLabels = default;

        private Dictionary<ButtonInputLabelId, HUDButtonInputLabel> labelLookup;


        public void Setup()
        {
            labelLookup = new Dictionary<ButtonInputLabelId, HUDButtonInputLabel>();

            if (buttonInputLabels != null)
            {
                for(int i = 0; i < buttonInputLabels.Length; i++)
                {
                    labelLookup.Add(buttonInputLabels[i].id, buttonInputLabels[i]);
                }
            }
        }


        private HUDButtonInputLabel GetButtonInputLabel(ButtonInputLabelId id)
        {
            labelLookup.TryGetValue(id, out HUDButtonInputLabel buttonInputLabel);

            return buttonInputLabel;
        }

        private void ToggleLabel(ButtonInputLabelId id, bool active)
        {
            HUDButtonInputLabel buttonInputLabel = GetButtonInputLabel(id);

            if(buttonInputLabel != null)
            {
                buttonInputLabel.ToggleGameObject(active);
            }
        }


        public void ShowInputLabels(ButtonInputLabelId[] inputLabelsToDisplay)
        {
            for(int i = 0; i < buttonInputLabels.Length; i++)
            {
                buttonInputLabels[i].ToggleGameObject(false);
            }

            for(int i = 0; i < inputLabelsToDisplay.Length; i++)
            {
                ToggleLabel(inputLabelsToDisplay[i], true);
            }

            Show();
        }
    }
}

