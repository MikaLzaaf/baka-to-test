﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Intelligensia.UI.HUD
{
    public class HUDButtonInputLabel : MonoBehaviour
    {
        public ButtonInputLabelId id;
        public CanvasGroup canvasGroup;


        public void ToggleGameObject(bool active)
        {
            gameObject.SetActive(active);
        }


        public void ToggleInteractability(bool active)
        {
            if (canvasGroup != null)
            {
                canvasGroup.interactable = active;

                canvasGroup.alpha = active ? 1f : 0.5f;
            }
        }

        //public bool IsSelectable
        //{
        //    get
        //    {
        //        if (canvasGroup == null)
        //            return false;

        //        return canvasGroup.interactable;
        //    }
        //}
    }

    public enum ButtonInputLabelId
    {
        Undefined,
        Talk,
        Challenge,
        Battle,
        Map,
        Inspect,
        Open
    }
}

