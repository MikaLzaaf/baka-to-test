﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

namespace Intelligensia.UI.HUD
{
    public class HUDDateWeatherController : ViewController
    {
        [Header("UI")]
        [SerializeField] private TextMeshProUGUI dateText = default;
        [SerializeField] private TextMeshProUGUI dayText = default;
        [SerializeField] private TextMeshProUGUI sessionText = default;
        [SerializeField] private Image weatherIcon = default;


        public void InitializePanel()
        {

        }
    }
}

