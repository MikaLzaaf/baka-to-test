﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public class GameUIManager : MonoBehaviour 
{

    public static GameUIManager instance;

    public Action<ViewState> ViewStateChangedEvent;


    public PlayerMenuManager gameMainMenu;
    public ChallengesMenuController challengesMenuController;
    //public DialogPanelManager dialogPanelManager;
    //public BattleTransitionController battleTransitionEffect;
    //public LoadingViewController loadingView;
    //public Transform QTEContainer;
    public ViewController whiteFade;
    public MapUIController mapController;
    public HUDManager hudManager;


    private ViewController _currentActiveView;
    public ViewController currentActiveView
    {
        get
        {
            return _currentActiveView;
        }
        set
        {
            if(_currentActiveView == value)
            {
                return;
            }

            _currentActiveView = value;

            currentActiveView.ViewReadyEvent -= OnViewReady;
            currentActiveView.ViewReadyEvent += OnViewReady;

            currentActiveView.ViewClosedEvent -= OnViewClosed;
            currentActiveView.ViewClosedEvent += OnViewClosed;
        }
    }


    protected void Awake()
    {
        if(instance == null)
        {
            instance = this;

            //transform.SetParent(null);
            DontDestroyOnLoad(transform.parent.gameObject);
        }
        else
        {
            Destroy(transform.parent.gameObject);
        }
    }


    public bool HasActiveView
    {
        get
        {
            if (currentActiveView == null)
            {
                return false;
            }

            return currentActiveView.viewState != ViewState.Closed;
        }
    }


    private void OnViewReady()
    {
        ViewStateChangedEvent?.Invoke(ViewState.Ready);
    }


    private void OnViewClosed()
    {
        ViewStateChangedEvent?.Invoke(ViewState.Closed);
    }


    //public void ShowLoadingView()
    //{
    //    if(loadingView != null)
    //    {
    //        loadingView.Show();
    //    }
    //}


    //public void ShowBattleTransition(bool beginBattle, bool retryBattle = false)
    //{
    //    if(retryBattle)
    //    {
    //        battleTransitionEffect.ShowRetryTransition();
    //    }
    //    else
    //    {
    //        battleTransitionEffect.ShowTransition(beginBattle);
    //    }
    //}

    public void OpenFullMap()
    {
        OpenMap(false);
    }
   
    public void OpenMap(bool isMiniMap)
    {
        if (mapController == null)
            return;

        if(isMiniMap)
        {
            PlayerPartyManager.instance.TogglePlayerInput(true);

            MainCameraController.instance.ToggleCameraBrain(true);

            mapController.SetMapMode(MapUIController.MapMode.Mini);
        }
        else
        {
            PlayerPartyManager.instance.TogglePlayerInput(false);

            MainCameraController.instance.ToggleCameraBrain(false);

            mapController.SetMapMode(MapUIController.MapMode.Fullscreen);
        }
    }


    public void OpenMainMenu()
    {
        if (gameMainMenu != null && !gameMainMenu.isOpened)
        {
            PlayerPartyManager.playerCannotMove = true;

            MainCameraController.instance.ToggleCameraBrain(false);
            mapController.SetMapMode(MapUIController.MapMode.Deactivate);

            //UINavigator.instance.OpenNewWindow(gameMainMenu, gameMainMenu.content);
            gameMainMenu.Show();

            //ToggleBackground(true);
        }
    }

    public void ToggleChallengesMenu(bool active)
    {
        if (challengesMenuController == null)
            return;

        if(active)
        {
            challengesMenuController.Show();

            mapController.SetMapMode(MapUIController.MapMode.Deactivate);
        }
        else
        {
            challengesMenuController.Close();

            mapController.SetMapMode(MapUIController.MapMode.Mini);
        }

        PlayerPartyManager.instance.TogglePlayerInput(!active);

        MainCameraController.instance.ToggleCameraBrain(!active);
    }


    public void ShowWhiteFade()
    {
        if(whiteFade != null)
        {
            whiteFade.Show();
        }
    }
}
