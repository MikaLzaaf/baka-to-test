﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealingTargetSelectionPanelItem : Button
{
    private const string LevelPrefix = "Level ";


    public Image characterIcon;

    public Text characterName;
    public Text characterLevel;

    public Text healthValueText;
    public Text manaValueText;

    public Slider healthSlider;
    public Slider manaSlider;


    public ControllableEntity playerEntity { get; private set; }


    public void Initialize(ControllableEntity playerEntity)
    {
        this.playerEntity = playerEntity;

        characterName.text = playerEntity.characterName;
        //characterLevel.text = LevelPrefix + playerEntity.playerStatsData.currentLevel;

        //healthSlider.maxValue = playerEntity.maxAP;
        //healthSlider.value = playerEntity.currentAP;

        //healthValueText.text = playerEntity.currentAP.ToString();
        

        //playerEntity.APUpdateEvent += OnHealthChanged;

        healthSlider.onValueChanged.AddListener(OnHealthSliderChanged);
    }


    private void OnHealthSliderChanged(float value)
    {
        // Later add icon changes when health changes
        healthValueText.text = value.ToString();
    }


    private void OnHealthChanged(int health)
    {
        healthSlider.value = health * 1.0f;
    }
}
