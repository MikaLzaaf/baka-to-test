﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.InputSystem;
using Intelligensia.UI.PlayerMenu;

public class PlayerMenuManager : UIViewController
{
    public event Action OnCancelPressedEvent;
    public event Action OnRightTriggerPressedEvent;
    public event Action OnLeftTriggerPressedEvent;
    public event Action OnRightTabPressedEvent;
    public event Action OnLeftTabPressedEvent;
    public event Action OnInfoButtonPressedEvent;
    public event Action OnNorthButtonPressedEvent;

    [Header("Panels")]
    public MainMenuPanelController mainMenuPanel;
    public InventoryPanelController inventoryPanelController;
    public EquipmentPanelController equipmentPanelController;
    public HealingTargetSelectionPanelController healingTargetSelectionPanelController;
    public PlayerStatusScreenController playerStatusScreenController;
    public AcademicPanelController academicPanelController;
    public ArgumentsPanelController argumentsPanelController;
    public CalendarPanelController calendarPanelController;
    public PlayerInput menuInput;

    public bool isOpened { get; set; }
    public bool isOnMainMenu { get; set; }
    public int panelIndex { get; private set; }

    private string itemIdSelected;



    private void Start()
    {
        healingTargetSelectionPanelController.PlayerSelectedEvent -= OnPlayerSelected;
        healingTargetSelectionPanelController.PlayerSelectedEvent += OnPlayerSelected;
    }


    public override void Show()
    {
        base.Show();

        OpenMainMenu();

        isOpened = true;
    }


    public override void Close()
    {
        if(isOnMainMenu)
        {
            base.Close();

            isOpened = false;

            if(!BattleController.IsInBattle)
                GameUIManager.instance.mapController.SetMapMode(MapUIController.MapMode.Mini);
        }

        panelIndex = -1;
    }

    public void OpenMainMenu()
    {
        isOnMainMenu = true;
        UINavigator.instance.OpenNewWindow(mainMenuPanel, mainMenuPanel.content);
    }

    public void OpenInventory()
    {
        isOnMainMenu = false;
        UINavigator.instance.OpenNewWindow(inventoryPanelController, inventoryPanelController.content);

        panelIndex = 0;
    }


    public void OpenEquipment()
    {
        isOnMainMenu = false;
        UINavigator.instance.OpenNewWindow(equipmentPanelController, equipmentPanelController.content);

        panelIndex = 1;
    }


    public void OpenStatus()
    {
        isOnMainMenu = false;
        UINavigator.instance.OpenNewWindow(playerStatusScreenController);

        panelIndex = 2;
    }


    public void OpenAcademic()
    {
        isOnMainMenu = false;
        UINavigator.instance.OpenNewWindow(academicPanelController, academicPanelController.content);

        panelIndex = 3;
    }

    public void OpenCalendar()
    {
        isOnMainMenu = false;
        UINavigator.instance.OpenNewWindow(calendarPanelController, calendarPanelController.content);

        panelIndex = 4;
    }

    public void OpenArguments()
    {
        isOnMainMenu = false;
        UINavigator.instance.OpenNewWindow(argumentsPanelController, argumentsPanelController.content);

        panelIndex = 5;
    }


    // Later add skill as well
    public void SelectPlayer(string ItemID)
    {
        itemIdSelected = ItemID;

        UINavigator.instance.SetDefaultSelectedInRuntime(healingTargetSelectionPanelController.content);
    }


    private void OnPlayerSelected(ControllableEntity playerEntity)
    {
        if(itemIdSelected != null)
        {
            Item item = PlayerInventoryManager.instance.GetItem(itemIdSelected);

            //item.Use(playerEntity); // TODO : Need to check if need to use items outside Battle. If no need, then make it similar like in FF13

            itemIdSelected = null;

            if(item.amount > 0)
            {
                UINavigator.instance.SetDefaultSelectedInRuntime(inventoryPanelController.content);
            }
        }
    }


    #region Inputs handling

    public void OnCancelPressed(InputAction.CallbackContext callbackContext)
    {
        if(callbackContext.performed)
        {
            OnCancelPressedEvent?.Invoke();
        }
    }

    public void OnRightTriggerPressed(InputAction.CallbackContext callbackContext)
    {
        if (callbackContext.performed)
        {
            OnRightTriggerPressedEvent?.Invoke();
        }
    }

    public void OnLeftTriggerPressed(InputAction.CallbackContext callbackContext)
    {
        if (callbackContext.performed)
        {
            OnLeftTriggerPressedEvent?.Invoke();
        }
    }

    public void OnRightTabPressed(InputAction.CallbackContext callbackContext)
    {
        if (callbackContext.performed)
        {
            OnRightTabPressedEvent?.Invoke();
        }
    }

    public void OnLeftTabPressed(InputAction.CallbackContext callbackContext)
    {
        if (callbackContext.performed)
        {
            OnLeftTabPressedEvent?.Invoke();
        }
    }

    public void OnInfoButtonPressed(InputAction.CallbackContext callbackContext)
    {
        if (callbackContext.performed)
        {
            OnInfoButtonPressedEvent?.Invoke();
        }
    }

    public void OnNorthButtonPressed(InputAction.CallbackContext callbackContext)
    {
        if (callbackContext.performed)
        {
            OnNorthButtonPressedEvent?.Invoke();
        }
    }

    #endregion
}
