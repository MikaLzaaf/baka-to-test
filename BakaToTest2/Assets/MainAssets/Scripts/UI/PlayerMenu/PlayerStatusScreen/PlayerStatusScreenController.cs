﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.InputSystem;

namespace Intelligensia.UI.PlayerMenu
{
    public class PlayerStatusScreenController : UIViewController
    {
        public PlayerInput actionInput;
        public PlayerStatsSubPanelController playerStatsPanelController;
        public PlayerSelectionPanelController playerSelectionPanelController;

        public Image characterPortrait;


        public override void Show()
        {
            base.Show();

            playerSelectionPanelController.PlayerChangedEvent += GenerateStatsPanel;

            playerSelectionPanelController.GeneratePlayerIcons();

            ToggleInput(true);
        }


        public override void Close()
        {
            base.Close();

            playerSelectionPanelController.PlayerChangedEvent -= GenerateStatsPanel;

            playerStatsPanelController.Close();

            playerSelectionPanelController.Close();

            ToggleInput(false);
        }


        private void ToggleInput(bool active)
        {
            if (actionInput == null)
                return;

            if (active)
            {
                actionInput.ActivateInput();
            }
            else
            {
                actionInput.DeactivateInput();
            }
        }

        private void GenerateStatsPanel(ControllableEntity player)
        {
            characterPortrait.sprite = PlayerInfoManager.LoadCharacterPortrait(player.characterId);

            playerStatsPanelController.GenerateStatsPanel(player);
        }


        public void NextCharacter(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                playerSelectionPanelController.SwitchCharacter(1);
            }
        }


        public void PreviousCharacter(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                playerSelectionPanelController.SwitchCharacter(-1);
            }
        }


        public void OnCancelMenu(InputAction.CallbackContext callbackContext)
        {
            if (callbackContext.performed)
            {
                UINavigator.instance.OpenPreviousWindow();
            }
        }
    }
}

