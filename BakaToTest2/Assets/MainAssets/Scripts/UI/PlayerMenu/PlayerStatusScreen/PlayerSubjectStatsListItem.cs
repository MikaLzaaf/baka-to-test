﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

namespace Intelligensia.UI.PlayerMenu
{
    public class PlayerSubjectStatsListItem : MonoBehaviour
    {
        public Subjects subjectName;

        //public TextMeshProUGUI statsNameText;
        [SerializeField] private Image statsIcon = default;
        [SerializeField] private TextMeshProUGUI statsAmountText = default;

        [SerializeField] private GameObject upArrow = default;
        [SerializeField] private GameObject downArrow = default;

        [SerializeField] private Color neutralColor = Color.black;
        [SerializeField] private Color betterColor = Color.green;
        [SerializeField] private Color worseColor = Color.red;

        private int statsValue;


        public void Initialize(SubjectStats subjectStats)
        {
            //statsNameText.text = subjectStats.subject.ToString();
            statsIcon.sprite = subjectStats.GetIcon();

            statsValue = subjectStats.finalValue;

            statsAmountText.text = statsValue.ToString();

            Reset();
        }


        public void CompareValue(int newValue)
        {
            statsAmountText.text = newValue.ToString();

            if (newValue > statsValue)
            {
                statsAmountText.color = betterColor;

                upArrow.SetActive(true);
                downArrow.SetActive(false);
            }
            else if (newValue < statsValue)
            {
                statsAmountText.color = worseColor;

                upArrow.SetActive(false);
                downArrow.SetActive(true);
            }
            else
            {
                statsAmountText.color = neutralColor;

                upArrow.SetActive(false);
                downArrow.SetActive(false);
            }
        }


        public void Reset()
        {
            //statsAmountText.text = statsValue.ToString();
            statsAmountText.color = neutralColor;

            upArrow.SetActive(false);
            downArrow.SetActive(false);
        }
    }
}

