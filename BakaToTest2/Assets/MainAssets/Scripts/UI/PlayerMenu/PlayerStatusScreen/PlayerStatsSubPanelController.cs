﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace Intelligensia.UI.PlayerMenu
{
    public class PlayerStatsSubPanelController : ViewController
    {
        public TextMeshProUGUI nameText;

        [Header("Basic Stats List Item")]
        public PlayerBasicStatsListItem[] basicStatsListItems;

        [Header("Subject Stats List Item")]
        public PlayerSubjectStatsListItem[] subjectStatsListItems;



        public void GenerateStatsPanel(ControllableEntity player)
        {
            Show();

            nameText.text = player.characterName;

            BasicStats currentStats = player.playerStatsData.GetEffectiveBasicStats();

            for (int i = 0; i < basicStatsListItems.Length; i++)
            {
                basicStatsListItems[i].Initialize(currentStats);
            }

            AcademicStats currentAcademic = player.playerStatsData.GetEffectiveAcademicStats();

            for(int i = 0; i < subjectStatsListItems.Length; i++)
            {
                SubjectStats subjectStat = currentAcademic.GetSubjectStats(subjectStatsListItems[i].subjectName);

                subjectStatsListItems[i].Initialize(subjectStat);
            }
        }


        public void CompareBasicStats(BasicStats previewStats)
        {
            // Compare stats with current equipped items and new equipped items

            for (int i = 0; i < basicStatsListItems.Length; i++)
            {
                basicStatsListItems[i].CompareValue(previewStats.GetStatsValue(basicStatsListItems[i].statsName));
            }
        }

        public void CompareAcademicStats(AcademicStats previewStats)
        {
            // Compare stats with current equipped items and new equipped items

            for (int i = 0; i < subjectStatsListItems.Length; i++)
            {
                subjectStatsListItems[i].CompareValue(previewStats.GetStatsValue(subjectStatsListItems[i].subjectName));
            }
        }


        public void ResetStatsPanelItems()
        {
            for (int i = 0; i < basicStatsListItems.Length; i++)
            {
                basicStatsListItems[i].Reset();
            }
        }
    }
}

