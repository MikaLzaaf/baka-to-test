﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace Intelligensia.UI.PlayerMenu
{
    public class PlayerBasicStatsListItem : MonoBehaviour
    {
        public StatsName statsName;

        [SerializeField] private TextMeshProUGUI statsNameText = default;
        [SerializeField] private TextMeshProUGUI statsAmountText = default;

        [SerializeField] private GameObject upArrow = default;
        [SerializeField] private GameObject downArrow = default;

        [SerializeField] private Color neutralColor = Color.black;
        [SerializeField] private Color betterColor = Color.green;
        [SerializeField] private Color worseColor = Color.red;

        private int statsValue;


        public void Initialize(BasicStats basicStats)
        {
            statsNameText.text = statsName.ToString();

            statsValue = basicStats.GetStatsValue(statsName);

            statsAmountText.text = statsValue.ToString();

            Reset();
        }


        public void CompareValue(int newValue)
        {
            statsAmountText.text = newValue.ToString();

            if (newValue > statsValue)
            {
                statsAmountText.color = betterColor;

                upArrow.SetActive(true);
                downArrow.SetActive(false);
            }
            else if (newValue < statsValue)
            {
                statsAmountText.color = worseColor;

                upArrow.SetActive(false);
                downArrow.SetActive(true);
            }
            else
            {
                statsAmountText.color = neutralColor;

                upArrow.SetActive(false);
                downArrow.SetActive(false);
            }
        }


        public void Reset()
        {
            //statsAmountText.text = statsValue.ToString();
            statsAmountText.color = neutralColor;

            upArrow.SetActive(false);
            downArrow.SetActive(false);
        }
    }
}

