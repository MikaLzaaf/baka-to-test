﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

namespace Intelligensia.UI.PlayerMenu
{
    public class PlayerSelectionPanelController : ViewController
    {
        public Action<ControllableEntity> PlayerChangedEvent;

        public PlayerSelectionListItem listItemPrefab;
        public Transform characterIconsContainer;
        public Transform nextInputIcon;

        private List<PlayerSelectionListItem> partyMemberIcons = new List<PlayerSelectionListItem>();
        private List<ControllableEntity> partyMemberList = new List<ControllableEntity>();
        private int selectedMemberIndex = 0;


        public void GeneratePlayerIcons()
        {
            Show();

            if (partyMemberIcons != null && partyMemberIcons.Count > 0)
            {
                ClearList();
            }
            else
            {
                partyMemberIcons = new List<PlayerSelectionListItem>();
            }

            partyMemberList = PlayerPartyManager.instance.partyMembers;
            // Generate member selection panel
            for (int i = 0; i < partyMemberList.Count; i++)
            {
                PlayerSelectionListItem listItem = Instantiate(listItemPrefab);
                listItem.transform.SetParent(characterIconsContainer, false);

                listItem.InitializeItem(partyMemberList[i].characterId);

                int index = i;

                listItem.onClick.AddListener(delegate
                {
                    SwitchCharacterOnClicked(index);
                });

                partyMemberIcons.Add(listItem);
            }

            nextInputIcon.SetAsLastSibling();

            //selectedMemberIndex = 0;
            SwitchCharacterOnClicked(0);
        }


        private void ClearList()
        {
            for (int i = partyMemberIcons.Count - 1; i >= 0; i--)
            {
                Destroy(partyMemberIcons[i].gameObject);
            }

            partyMemberIcons.Clear();
        }


        public void SwitchCharacter(int value)
        {
            if (partyMemberList.Count <= 1)
                return;

            int nextIndex = (selectedMemberIndex + value) % partyMemberList.Count;

            if (nextIndex < 0)
            {
                nextIndex = partyMemberList.Count - 1;
            }

            partyMemberIcons[nextIndex].onClick?.Invoke();

            //if(selectedMemberIndex != nextIndex)
            //{
            //    partyMemberIcons[selectedMemberIndex].ToggleHighlight(false);
            //}
           
            //partyMemberIcons[nextIndex].ToggleHighlight(true);

            //selectedMemberIndex = nextIndex;

            //PlayerChangedEvent?.Invoke(partyMemberList[selectedMemberIndex]);
        }

        public void NextCharacter()
        {
            SwitchCharacter(1);
        }

        public void PreviousCharacter()
        {
            SwitchCharacter(-1);
        }

        public void SwitchCharacterOnClicked(int index)
        {
            if (selectedMemberIndex != index)
            {
                partyMemberIcons[selectedMemberIndex].ToggleHighlight(false);
            }
     
            selectedMemberIndex = index;

            partyMemberIcons[selectedMemberIndex].ToggleHighlight(true);

            PlayerChangedEvent?.Invoke(partyMemberList[selectedMemberIndex]);
        }

        public ControllableEntity GetSelectedCharacter()
        {
            return partyMemberList[selectedMemberIndex];
        }
    }
}

