﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

namespace Intelligensia.UI.PlayerMenu
{
    public class PlayerSelectionListItem : HighlightingTabButton
    {
        //[SerializeField] private Transform iconHolder = default;
        [SerializeField] private Image icon = default;
        //[SerializeField] private ImageColorLerper iconHighlighter = default;

        //[Header("Move variables")]
        //[SerializeField] float moveDuration = 0.3f;
        //[SerializeField] float moveVerticalAmount = 10f;

        //private Vector3 originalPosition;

        //private void OnDisable()
        //{
        //    ToggleHighlight(false);
        //}

        public void InitializeItem(CharacterId characterId)
        {
            if (icon == null)
                return;

            icon.sprite = PlayerInfoManager.LoadCharacterIcon(characterId);

            originalPosition = iconHolder.transform.localPosition;
        }

        //public void ToggleHighlight(bool active)
        //{
        //    if (iconHighlighter != null)
        //    {
        //        iconHighlighter.gameObject.SetActive(active);
        //    }

        //    MoveUpOrDown(active);
        //}

        //private void MoveUpOrDown(bool up)
        //{
        //    float destination = up ? originalPosition.y + moveVerticalAmount : originalPosition.y;

        //    iconHolder.DOLocalMoveY(destination, moveDuration);
        //}
    }
}

