﻿using TMPro;
using UnityEngine;

namespace Intelligensia.UI.PlayerMenu
{
    public class AcademicIlmuListItem : MonoBehaviour
    {
        public Subjects subject;

        [SerializeField] private TextMeshProUGUI materialAmountText = default;


        public void SetAmount(int amount)
        {
            if(materialAmountText != null)
            {
                materialAmountText.text = amount.ToString("N0");
            }
        }
    }
}

