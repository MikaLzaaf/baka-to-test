﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace Intelligensia.UI.PlayerMenu
{
    public class AcademicChartGrade : MonoBehaviour
    {
        public Subjects subject;

        [SerializeField] private TextMeshProUGUI gradeText = default;
        [SerializeField] private TextMeshProUGUI scoreText = default;

        [SerializeField] private string scoreTextFormat = "{0} <color=#F7B731>{1}</color>";


        public void Generate(int score, int differences)
        {
            gradeText.text = GradeChecker.GetGrade(score);

            string differencesValue = differences > 0 ? "+ " + differences.ToString() : "";

            scoreText.text = string.Format(scoreTextFormat, score, differencesValue);
        }
    }
}

