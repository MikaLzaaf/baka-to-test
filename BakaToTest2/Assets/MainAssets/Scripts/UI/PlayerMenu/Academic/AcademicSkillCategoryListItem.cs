﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Intelligensia.UI.PlayerMenu
{
    public class AcademicSkillCategoryListItem : HighlightingTabButton
{
        public AcademicSkillCategory category;
    }
}

