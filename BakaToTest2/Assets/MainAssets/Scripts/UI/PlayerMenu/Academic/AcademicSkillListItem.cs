﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;
using System;

namespace Intelligensia.UI.PlayerMenu
{
    public class AcademicSkillListItem : HighlightingButton, ISelectHandler
    {

        private const string HiddenTitle = " ? ? ?";

        public Action<SkillTreeUnitData> OnSkillHighlighted;

        [SerializeField] private CanvasGroup canvasGroup = default;
        [SerializeField] private TextMeshProUGUI titleText = default;

        [SerializeField] private TextMeshProUGUI costBMText = default;
        [SerializeField] private TextMeshProUGUI costEnglishText = default;
        [SerializeField] private TextMeshProUGUI costMathsText = default;
        [SerializeField] private TextMeshProUGUI costHistoryText = default;
        [SerializeField] private TextMeshProUGUI costPEText = default;
        [SerializeField] private TextMeshProUGUI costPhysicsText = default;
        [SerializeField] private TextMeshProUGUI costBioText = default;
        [SerializeField] private TextMeshProUGUI costChemistryText = default;

        [SerializeField] private Transform statusIconContainer = default;
        [SerializeField] private GameObject statsCategoryIcon = default;


        public SkillTreeUnitData skillTreeUnitData { get; private set; }
        private CharacterId currentCharacterId;


        public void Initialize(SkillTreeUnitData skillTreeUnitData, CharacterId currentCharacterId)
        {
            if (skillTreeUnitData == null || currentCharacterId == CharacterId.Undefined)
                return;

            this.skillTreeUnitData = skillTreeUnitData;
            this.currentCharacterId = currentCharacterId;

            titleText.text = skillTreeUnitData.status == SkillTreeUnitStatus.Locked ? HiddenTitle : skillTreeUnitData.name;

            SetCosts(skillTreeUnitData.CanBuy());
            ToggleIcons(skillTreeUnitData.IsStatsCategory());

            if(!skillTreeUnitData.IsStatsCategory())
                ToggleStatusIcon(skillTreeUnitData.status);

            ToggleInteractability();
        }

        private void SetCosts(bool showCostText)
        {
            if(costBMText != null)
            {
                int cost = skillTreeUnitData.GetCostBySubject(Subjects.BahasaMelayu);

                costBMText.text = showCostText && cost > 0 ? cost.ToString("N0") : "";
            }

            if (costEnglishText != null)
            {
                int cost = skillTreeUnitData.GetCostBySubject(Subjects.English);

                costEnglishText.text = showCostText && cost > 0 ? cost.ToString("N0") : "";
            }

            if (costMathsText != null)
            {
                int cost = skillTreeUnitData.GetCostBySubject(Subjects.Mathematics);

                costMathsText.text = showCostText && cost > 0 ? cost.ToString("N0") : "";
            }

            if (costHistoryText != null)
            {
                int cost = skillTreeUnitData.GetCostBySubject(Subjects.History);

                costHistoryText.text = showCostText && cost > 0 ? cost.ToString("N0") : "";
            }

            if (costPEText != null)
            {
                int cost = skillTreeUnitData.GetCostBySubject(Subjects.PE);

                costPEText.text = showCostText && cost > 0 ? cost.ToString("N0") : "";
            }

            if (costPhysicsText != null)
            {
                int cost = skillTreeUnitData.GetCostBySubject(Subjects.Physics);

                costPhysicsText.text = showCostText && cost > 0 ? cost.ToString("N0") : "";
            }

            if (costBioText != null)
            {
                int cost = skillTreeUnitData.GetCostBySubject(Subjects.Biology);

                costBioText.text = showCostText && cost > 0 ? cost.ToString("N0") : "";
            }

            if (costChemistryText != null)
            {
                int cost = skillTreeUnitData.GetCostBySubject(Subjects.Chemistry);

                costChemistryText.text = showCostText && cost > 0 ? cost.ToString("N0") : "";
            }
        }

        private void ToggleIcons(bool canMultiplePurchase)
        {
            if(statsCategoryIcon != null)
            {
                statsCategoryIcon.SetActive(canMultiplePurchase);
            }

            if(statusIconContainer != null)
            {
                statusIconContainer.gameObject.SetActive(!canMultiplePurchase);
            }
        }

        private void ToggleStatusIcon(SkillTreeUnitStatus skillLeafStatus)
        {
            if (statusIconContainer == null || statusIconContainer.childCount < 3)
                return;

            for (int i = 0; i < statusIconContainer.childCount; i++)
            {
                statusIconContainer.GetChild(i).gameObject.SetActive(false);
            }

            int childIndex = (int)skillLeafStatus;

            statusIconContainer.GetChild(childIndex).gameObject.SetActive(true);
        }

        private void ToggleInteractability()
        {
            if(canvasGroup != null)
            {
                bool isInteractable = IsItemInteractable();

                if(!skillTreeUnitData.IsBought())
                    canvasGroup.alpha = isInteractable ? 1f : 0.5f;

                interactable = isInteractable;
            }
        }

        public bool IsItemInteractable()
        {
            if (!skillTreeUnitData.CanBuy())
                return false;

            if (!skillTreeUnitData.IsStatsCategory() && skillTreeUnitData.status == SkillTreeUnitStatus.Bought)
                return false;

            if (skillTreeUnitData.status == SkillTreeUnitStatus.Locked)
                return false;

            if (!HasEnoughIlmuToPay())
                return false;

            return true;
        }

        private bool HasEnoughIlmuToPay()
        {
            bool hasEnough = true;

            var ilmus = PlayerInventoryManager.instance.GetItemsByCategory(ItemCategory.Ilmu);

            for(int i = 0; i < ilmus.Length; i++)
            {
                IlmuItemBaseValue xpMaterial = (IlmuItemBaseValue)ilmus[i].itemValue;

                if (xpMaterial == null)
                    continue;

                if (ilmus[i].amount < skillTreeUnitData.GetCostBySubject(xpMaterial.subject))
                {
                    hasEnough = false;
                    break;
                }
            }

            return hasEnough;
        }

        public override void OnSelect(BaseEventData eventData)
        {
            base.OnSelect(eventData);

            string description = "";

            if (skillTreeUnitData.BaseSkillTreeUnit == null)
                return;

            if(skillTreeUnitData.IsStatsCategory())
            {
                if (skillTreeUnitData.IsType<BaseStatsUpgradeSO>())
                {
                    BaseStatsUpgradeSO baseStatsUpgradeSO = skillTreeUnitData.GetBase<BaseStatsUpgradeSO>();

                    description = baseStatsUpgradeSO.GetDescription();
                }
                else if (skillTreeUnitData.IsType<SubjectStatsUpgradeSO>())
                {
                    SubjectStatsUpgradeSO subjectStatsUpgradeSO = skillTreeUnitData.GetBase<SubjectStatsUpgradeSO>();

                    description = subjectStatsUpgradeSO.GetDescription();
                }

            }
            else
            {
                BaseSkill baseSkill = skillTreeUnitData.GetBase<BaseSkill>();

                description = baseSkill.GetFullDescription();
            }

            BottomPanelController.instance.SetDescription(description);
        }

    }
}

