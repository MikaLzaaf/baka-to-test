﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using TMPro;

namespace Intelligensia.UI.PlayerMenu
{
    public class AcademicPanelController : UIViewController
    {
        [Header("Panels")]
        [SerializeField] private PlayerStatsSubPanelController playerStatsPanelController = default;
        [SerializeField] private PlayerSelectionPanelController playerSelectionPanelController = default;
        [SerializeField] private AcademicChartGradeController gradeController = default;
        [SerializeField] private AcademicSkillListController skillListController = default;
        [SerializeField] private UIRadarChart effectiveSubjectStatsChart = default;
        [SerializeField] private UIRadarChart baseSubjectStatsChart = default;
        //[SerializeField] private GameObject firstPage = default;
        [SerializeField] private GameObject radarChartsHolder = default;

        [Header("UI")]
        [SerializeField] private TextMeshProUGUI characterName = default;
        [SerializeField] private TextMeshProUGUI overallGradeText = default;
        [SerializeField] private TextMeshProUGUI inspectedCategoryText = default;


        [Header("List Items")]
        [SerializeField] private AcademicIlmuListItem[] ilmuListItems = default;
        [SerializeField] private AcademicSkillCategoryListItem[] categoryListItems = default;


        //private int categoryCount = 0;
        private int categoryIndex = 0;
        private bool isOpeningRadarChart;

        private Dictionary<Subjects, AcademicIlmuListItem> ilmuListItemLookup;

        // TODO : Create a switchable background that corresponds with each character
        // E.g. Genshin Impact, FF 13

        protected override void Awake()
        {
            base.Awake();

            //categoryCount = System.Enum.GetNames(typeof(AcademicSkillCategory)).Length;

            ilmuListItemLookup = new Dictionary<Subjects, AcademicIlmuListItem>();

            for(int i = 0; i < ilmuListItems.Length; i++)
            {
                ilmuListItemLookup.Add(ilmuListItems[i].subject, ilmuListItems[i]);
            }
        }


        public override void Show()
        {
            base.Show();

            // Showing flow - playerSelectionPanelController.GeneratePlayerIcons > PlayerChangedEvent invoked > GenerateScreen called
            playerSelectionPanelController.PlayerChangedEvent += GenerateScreen;

            playerSelectionPanelController.GeneratePlayerIcons();

            RefreshIlmuList();

            isOpeningRadarChart = false;
            SwitchChartOrStats();

            SwitchCategoryOnClicked(0);

            BottomPanelController.instance.SetCurrentPanelName("Academic");
            BottomPanelController.instance.ShowInputLabel(ButtonInputLabelId.AcademicPanelInput);
        }

        protected override void OnViewReady()
        {
            base.OnViewReady();

            PlayerMenuManager playerMenuManager = GameUIManager.instance.gameMainMenu;

            if (playerMenuManager != null)
            {
                playerMenuManager.OnRightTabPressedEvent += playerSelectionPanelController.NextCharacter;
                playerMenuManager.OnLeftTabPressedEvent += playerSelectionPanelController.PreviousCharacter;

                playerMenuManager.OnRightTriggerPressedEvent += NextCategory;
                playerMenuManager.OnLeftTriggerPressedEvent += PreviousCategory;

                playerMenuManager.OnCancelPressedEvent += OnCancelPressed;
                playerMenuManager.OnNorthButtonPressedEvent += SwitchChartOrStats;
                playerMenuManager.OnInfoButtonPressedEvent += SortList;
            }

            skillListController.ListItemClickedEvent += BuyNewSkill;
        }

        public override void Close()
        {
            base.Close();

            PlayerMenuManager playerMenuManager = GameUIManager.instance.gameMainMenu;

            if (playerMenuManager != null)
            {
                playerMenuManager.OnRightTabPressedEvent -= playerSelectionPanelController.NextCharacter;
                playerMenuManager.OnLeftTabPressedEvent -= playerSelectionPanelController.PreviousCharacter;

                playerMenuManager.OnRightTriggerPressedEvent -= NextCategory;
                playerMenuManager.OnLeftTriggerPressedEvent -= PreviousCategory;

                playerMenuManager.OnCancelPressedEvent -= OnCancelPressed;
                playerMenuManager.OnNorthButtonPressedEvent -= SwitchChartOrStats;
                playerMenuManager.OnInfoButtonPressedEvent -= SortList;
            }

            playerSelectionPanelController.PlayerChangedEvent -= GenerateScreen;
            skillListController.ListItemClickedEvent -= BuyNewSkill;

            playerSelectionPanelController.Close();
        }

        private void GenerateScreen(ControllableEntity playerEntity)
        {
            characterName.text = playerEntity.characterName;
            overallGradeText.text = playerEntity.playerStatsData.currentAcademicStats.GetOverallGrade();

            // Generate background here

            GenerateCharts(playerEntity);

            if (!isOpeningRadarChart)
                playerStatsPanelController.GenerateStatsPanel(playerEntity);

            skillListController.GenerateList(playerEntity, (AcademicSkillCategory)categoryIndex);
        }

        private void GenerateCharts(ControllableEntity playerEntity) // TODO : Need to generate extra chart with modifier stats added
        {
            if (baseSubjectStatsChart == null || effectiveSubjectStatsChart == null)
                return;

            List<float> baseSubjectStatsToGenerate = new List<float>();
            List<float> effectiveSubjectStatsToGenerate = new List<float>();

            AcademicStats academicStats = playerEntity.playerStatsData.currentAcademicStats;

            for (int i = 0; i < academicStats.subjectsStats.Count; i++)
            {
                SubjectStats subjectStat = academicStats.subjectsStats[i];

                baseSubjectStatsToGenerate.Add(subjectStat.GetBaseNormalizedResistanceValue());
                effectiveSubjectStatsToGenerate.Add(subjectStat.GetEffectiveNormalizedResistanceValue());

                gradeController.GenerateGrade(subjectStat.Subject, subjectStat.Score, subjectStat.finalValue);
            }

            baseSubjectStatsChart.UpdateRadarVisual(baseSubjectStatsToGenerate.ToArray());
            effectiveSubjectStatsChart.UpdateRadarVisual(effectiveSubjectStatsToGenerate.ToArray());
        }

        private void RefreshIlmuList()
        {
            if (ilmuListItems == null || ilmuListItems.Length == 0)
                return;

            Item[] currentItemList = PlayerInventoryManager.instance.GetItemsByCategory(ItemCategory.Ilmu);

            for (int i = 0; i < currentItemList.Length; i++)
            {
                IlmuItemBaseValue xpMaterial = (IlmuItemBaseValue)currentItemList[i].itemValue;

                if(ilmuListItemLookup.TryGetValue(xpMaterial.subject, out AcademicIlmuListItem ilmuListItem))
                {
                    ilmuListItem.SetAmount(currentItemList[i].amount);
                }
            }
        }


        private void SwitchCategory(int value)
        {
            int nextIndex = categoryIndex + value;

            if (nextIndex >= categoryListItems.Length)
            {
                nextIndex = 0;
            }
            else if (nextIndex < 0)
            {
                nextIndex = categoryListItems.Length - 1;
            }

            categoryListItems[nextIndex].onClick?.Invoke();
        }

        public void NextCategory()
        {
            SwitchCategory(1);
        }

        public void PreviousCategory()
        {
            SwitchCategory(-1);
        }

        // Assign on inspector of AcademicSkillCategoryListItems
        public void SwitchCategoryOnClicked(int index)
        {
            if (categoryIndex != index)
            {
                categoryListItems[categoryIndex].ToggleHighlight(false);
            }

            categoryIndex = index;

            categoryListItems[categoryIndex].ToggleHighlight(true);

            ControllableEntity currentEntity = playerSelectionPanelController.GetSelectedCharacter();

            skillListController.GenerateList(currentEntity, (AcademicSkillCategory)categoryIndex);

            inspectedCategoryText.text = StringHelper.SplitByCapitalizeFirstLetter(((AcademicSkillCategory)categoryIndex).ToString());
        }

        public void SwitchChartOrStats()
        {
            isOpeningRadarChart = !isOpeningRadarChart;

            if (playerStatsPanelController != null)
            {
                if (!isOpeningRadarChart)
                {
                    ControllableEntity currentEntity = playerSelectionPanelController.GetSelectedCharacter();

                    playerStatsPanelController.GenerateStatsPanel(currentEntity);
                }
                else
                    playerStatsPanelController.Close();
            }

            if (radarChartsHolder != null)
            {
                radarChartsHolder.SetActive(isOpeningRadarChart);
            }
        }


        private void BuyNewSkill(SkillTreeUnitData skillTreeUnitData)
        {
            ControllableEntity currentEntity = playerSelectionPanelController.GetSelectedCharacter();

            PlayerInventoryManager.instance.UseIlmuToPay(currentEntity.characterId, skillTreeUnitData.ilmuCosts);
            
            currentEntity.BuyNewSkill(skillTreeUnitData, (AcademicSkillCategory)categoryIndex);

            GenerateScreen(currentEntity);

            RefreshIlmuList();
        }

        private void OnCancelPressed()
        {
            int panelIndex = GameUIManager.instance.gameMainMenu.panelIndex;

            UINavigator.instance.OpenPreviousWindow(panelIndex);
        }

        private void SortList()
        {
            skillListController.SortListItems();
        }
    }
}

