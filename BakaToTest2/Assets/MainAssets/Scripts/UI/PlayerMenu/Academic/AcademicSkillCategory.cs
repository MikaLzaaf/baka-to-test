﻿
namespace Intelligensia.UI.PlayerMenu
{
    public enum AcademicSkillCategory
    {
        Stats,
        AttackArgument,
        SupportArgument,
        Others
    }
}

