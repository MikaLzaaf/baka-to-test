﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Linq;

namespace Intelligensia.UI.PlayerMenu
{
    public class AcademicSkillListController : MonoBehaviour
    {
        public Action<SkillTreeUnitData> ListItemClickedEvent;

        public AcademicSkillListItem listItemPrefab;
        public Transform listContainer;

        private List<AcademicSkillListItem> listItems;

        private SkillTreeData currentSkillTree;
        private SkillTreeData lastSkillTree;

        private CharacterId currentCharacterId;

        private int lastSelectedIndex = 0;

        public void GenerateList(ControllableEntity currentEntity, AcademicSkillCategory skillCategory)
        {
            currentSkillTree = currentEntity.playerStatsData.GetSubjectSkillTree(skillCategory);
            currentCharacterId = currentEntity.characterId;

            if (currentSkillTree == null)
            {
                ClearList();
                return;
            }

            GenerateList(currentSkillTree.skillTreeUnitDatas);
        }

        public void GenerateList(SkillTreeUnitData[] skillTreeUnitDatas)
        {
            ClearList();

            for (int i = 0; i < skillTreeUnitDatas.Length; i++)
            {
                AcademicSkillListItem listItem = Instantiate(listItemPrefab);
                listItems.Add(listItem);

                listItem.transform.SetParent(listContainer, false);
                listItem.Initialize(skillTreeUnitDatas[i], currentCharacterId);

                listItem.onClick.AddListener(delegate
                {
                    BuySkillUnit(listItem);
                });
            }

            UINavigator.instance.SetupVerticalNavigation(listItems, true);

            if (listItems.Count > 0)
            {
                if (lastSkillTree == currentSkillTree)
                {
                    int index = lastSelectedIndex < listItems.Count ? lastSelectedIndex : listItems.Count - 1;

                    UINavigator.instance.SetDefaultSelectedInRuntime(listItems[index].gameObject);
                }
                else
                {
                    UINavigator.instance.SetDefaultSelectedInRuntime(listItems[0].gameObject);
                }
            }

            if (currentSkillTree != null)
            {
                lastSkillTree = currentSkillTree;
            }
        }

        private void ClearList()
        {
            if (listItems != null && listItems.Count > 0)
            {
                if (listItems.Count > 0)
                {
                    for (int i = listItems.Count - 1; i >= 0; i--)
                    {
                        Destroy(listItems[i].gameObject);
                    }
                }

                listItems.Clear();
            }
            else
            {
                listItems = new List<AcademicSkillListItem>();
            }
        }

        private void BuySkillUnit(AcademicSkillListItem listItem)
        {
            if (listItem.IsItemInteractable())
            {
                lastSelectedIndex = listItems.IndexOf(listItem);

                ListItemClickedEvent?.Invoke(listItem.skillTreeUnitData);
            }
        }

        public void SortListItems()
        {
            var sortedList = listItems.OrderBy(x => (int)x.skillTreeUnitData.status).ToList();

            List<SkillTreeUnitData> skillTreeUnitDatas = new List<SkillTreeUnitData>();

            for(int i = 0; i < sortedList.Count; i++)
            {
                skillTreeUnitDatas.Add(sortedList[i].skillTreeUnitData);
            }

            GenerateList(skillTreeUnitDatas.ToArray());
        }
    }
}

