﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Intelligensia.UI.PlayerMenu
{
    public class AcademicChartGradeController : MonoBehaviour
    {
        [SerializeField] private List<AcademicChartGrade> listItems = default;
        private Dictionary<Subjects, AcademicChartGrade> listLookup;


        private void Awake()
        {
            listLookup = new Dictionary<Subjects, AcademicChartGrade>();

            foreach (AcademicChartGrade item in listItems)
            {
                listLookup.Add(item.subject, item);
            }
        }

        private AcademicChartGrade GetChartGrade(Subjects subject)
        {
            listLookup.TryGetValue(subject, out AcademicChartGrade chartGrade);

            return chartGrade;
        }


        public void GenerateGrade(Subjects subject, int subjectScore, int finalValue)
        {
            AcademicChartGrade chartGrade = GetChartGrade(subject);

            int differences = finalValue - subjectScore;

            chartGrade.Generate(subjectScore, differences);
        }
    }
}


