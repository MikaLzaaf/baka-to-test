﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;


namespace Intelligensia.UI.PlayerMenu
{
    public class EquipmentListController : UIViewController
    {

        public event Action<Item> UpdateStatsPanelEvent;
        public event Action<Item> OnListItemClickedEvent;

        [SerializeField] private EquipmentListItem listItemPrefab = default;
        [SerializeField] private Transform listContainer = default;


        private List<EquipmentListItem> listItems;
        private EquipmentSection currentEquipmentSection;


        public void GenerateEquipmentList(EquipmentSection selectedSection, int selectionIndex = 0)
        {
            Show();

            currentEquipmentSection = selectedSection;

            if (listItems != null && listItems.Count > 0)
            {
                ClearList();
            }
            else
            {
                listItems = new List<EquipmentListItem>();
            }

            CreateListItem(null);

            List<Item> listOfEquipments = PlayerInventoryManager.instance.GetEquipmentListBySection(currentEquipmentSection);

            for (int i = 0; i < listOfEquipments.Count; i++)
            {
                if (listOfEquipments[i].GetEquipableAmount() == 0)
                    continue;

                CreateListItem(listOfEquipments[i]);
            }

            if(listItems.Count > 0)
            {
                UINavigator.instance.SetupVerticalNavigation(listItems, true);

                int index = selectionIndex < listItems.Count ? selectionIndex : 0;

                UINavigator.instance.SetDefaultSelectedInRuntime(listItems[index].gameObject);
            }
        }

        private void CreateListItem(Item item)
        {
            EquipmentListItem listItem = Instantiate(listItemPrefab);
            listItems.Add(listItem);

            listItem.transform.SetParent(listContainer, false);

            if(item == null)
            {
                listItem.InitializeUnequip(this);
            }
            else
            {
                listItem.Initialize(item, this);
            }
        }

        private void ClearList()
        {
            if (listItems.Count > 0)
            {
                for (int i = listItems.Count - 1; i >= 0; i--)
                {
                    Destroy(listItems[i].gameObject);
                }
            }

            listItems.Clear();
        }

        public void UpdateStatsPanel(EquipmentListItem listItem)
        {
            UpdateStatsPanelEvent?.Invoke(listItem.equipmentItem);
        }

        public void OnListItemClicked(EquipmentListItem listItem)
        {
            OnListItemClickedEvent?.Invoke(listItem.equipmentItem);

            int listItemIndex = listItems.IndexOf(listItem);

            GenerateEquipmentList(currentEquipmentSection, listItemIndex);
        }
    }
}

