﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using TMPro;


namespace Intelligensia.UI.PlayerMenu
{
    public class EquipmentListItem : HighlightingButton, ISelectHandler, ISubmitHandler
    {
        private const string UnequipPrefix = "Unequip";
        private const string UnequipButtonDescriptionID = "UnequipButtonDescriptionID";

        public Action<Item> UpdateStatsPanelEvent;

        [SerializeField] private GameObject multiplierLabel = default;
        [SerializeField] private Image itemIcon = default;
        [SerializeField] private TextMeshProUGUI itemNameText = default;
        [SerializeField] private TextMeshProUGUI itemAmountText = default;

        public Item equipmentItem { get; set; }

        private EquipmentListController equipmentListController;


        public void Initialize(Item item, EquipmentListController equipmentListController)
        {
            equipmentItem = item;
            this.equipmentListController = equipmentListController;

            itemNameText.text = item.itemValue.ItemName;

            itemAmountText.text = item.GetEquipableAmount() > 0 ? item.GetEquipableAmount().ToString() : "";

            itemIcon.sprite = IconLoader.GetItemIcon(item.itemValue.ItemIcon);

            ToggleLabels(true);
        }


        public void InitializeUnequip(EquipmentListController equipmentListController)
        {
            equipmentItem = null;
            this.equipmentListController = equipmentListController;

            itemNameText.text = UnequipPrefix;

            itemAmountText.text = "";

            ToggleLabels(false);
        }

        public override void OnSelect(BaseEventData eventData)
        {
            base.OnSelect(eventData);

            if (interactable && equipmentListController != null)
            {
                equipmentListController.UpdateStatsPanel(this);

                string description = equipmentItem != null ? equipmentItem.GetDescription() 
                    : LocalizationManager.Localize(UnequipButtonDescriptionID);

                BottomPanelController.instance.SetDescription(description);
            }
        }

        public override void OnSubmit(BaseEventData eventData)
        {
            base.OnSubmit(eventData);

            if(interactable && equipmentListController != null)
            {
                equipmentListController.OnListItemClicked(this);
            }
        }

        private void ToggleLabels(bool active)
        {
            if(multiplierLabel != null)
            {
                multiplierLabel.gameObject.SetActive(active);
            }

            if(itemIcon != null)
            {
                itemIcon.gameObject.SetActive(active);
            }
        }
    }
}

