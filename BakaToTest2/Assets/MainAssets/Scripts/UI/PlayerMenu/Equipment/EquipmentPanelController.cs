﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.InputSystem;

namespace Intelligensia.UI.PlayerMenu
{
    public class EquipmentPanelController : UIViewController
    {
        [SerializeField] private EquipmentListController equipmentListController = default;
        [SerializeField] private PlayerStatsSubPanelController playerStatsPanelController = default;
        [SerializeField] private PlayerSelectionPanelController playerSelectionPanelController = default;

        [SerializeField] private List<EquipmentPanelItem> equipmentPanelItems = default;

        private ControllableEntity selectedPlayer;
        private EquipmentSection selectedSection;

        private Dictionary<EquipmentSection, EquipmentPanelItem> _equipmentPanelItemsLookup;
        private Dictionary<EquipmentSection, EquipmentPanelItem> equipmentPanelItemsLookup
        {
            get
            {
                if (_equipmentPanelItemsLookup == null)
                {
                    _equipmentPanelItemsLookup = new Dictionary<EquipmentSection, EquipmentPanelItem>();

                    foreach (EquipmentPanelItem item in equipmentPanelItems)
                    {
                        _equipmentPanelItemsLookup.Add(item.equipmentSection, item);
                    }
                }

                return _equipmentPanelItemsLookup;
            }
        }


        protected override void Awake()
        {
            base.Awake();

            if(equipmentListController != null)
            {
                equipmentListController.UpdateStatsPanelEvent += UpdateStatsPanel;
                equipmentListController.OnListItemClickedEvent += OnListItemClick;
            }
 
            for (int i = 0; i < equipmentPanelItems.Count; i++)
            {
                EquipmentSection section = equipmentPanelItems[i].equipmentSection;

                equipmentPanelItems[i].onClick.AddListener(delegate
                {
                    OnPanelItemClicked(section);
                });
            }
        }

        private void OnDestroy()
        {
            if (equipmentListController != null)
            {
                equipmentListController.UpdateStatsPanelEvent -= UpdateStatsPanel;
                equipmentListController.OnListItemClickedEvent -= OnListItemClick;
            }
        }

        public override void Show()
        {
            base.Show();

            UINavigator.instance.SetupVerticalNavigation(equipmentPanelItems, true, false);

            selectedPlayer = null;
            selectedSection = EquipmentSection.Undefined;

            playerSelectionPanelController.PlayerChangedEvent += InitializeEquippedItems;

            playerSelectionPanelController.GeneratePlayerIcons();

            BottomPanelController.instance.SetCurrentPanelName("Equipment");
            BottomPanelController.instance.ShowInputLabel(ButtonInputLabelId.Undefined);
        }

        public override void Close()
        {
            base.Close();

            PlayerMenuManager playerMenuManager = GameUIManager.instance.gameMainMenu;

            if (playerMenuManager != null)
            {
                playerMenuManager.OnCancelPressedEvent -= OnCancel;

                if (playerSelectionPanelController != null)
                {
                    playerMenuManager.OnRightTabPressedEvent -= playerSelectionPanelController.NextCharacter;
                    playerMenuManager.OnLeftTabPressedEvent -= playerSelectionPanelController.PreviousCharacter;
                }
            }

            playerSelectionPanelController.PlayerChangedEvent -= InitializeEquippedItems;

            playerStatsPanelController.Close();
            playerSelectionPanelController.Close();
        }

        protected override void OnViewReady()
        {
            base.OnViewReady();

            PlayerMenuManager playerMenuManager = GameUIManager.instance.gameMainMenu;

            if (playerMenuManager != null)
            {
                playerMenuManager.OnCancelPressedEvent += OnCancel;

                if(playerSelectionPanelController != null)
                {
                    playerMenuManager.OnRightTabPressedEvent += playerSelectionPanelController.NextCharacter;
                    playerMenuManager.OnLeftTabPressedEvent += playerSelectionPanelController.PreviousCharacter;
                }
            }

            UINavigator.instance.SetDefaultSelectedInRuntime(equipmentPanelItems[0].gameObject);
        }

        private void InitializeEquippedItems(ControllableEntity playerEntity)
        {
            playerStatsPanelController.GenerateStatsPanel(playerEntity);

            selectedPlayer = playerEntity;

            for (int i = 0; i < equipmentPanelItems.Count; i++)
            {
                equipmentPanelItems[i].Initialize(playerEntity.playerEquipment);
            }
        }

        private EquipmentPanelItem GetPanelItem(EquipmentSection section)
        {
            equipmentPanelItemsLookup.TryGetValue(section, out EquipmentPanelItem equipmentPanelItem);

            return equipmentPanelItem;
        }

        private void TogglePanelItemSelected(bool active)
        {
            EquipmentPanelItem panelItem = GetPanelItem(selectedSection);

            if(panelItem != null)
            {
                if (active)
                    panelItem.SelectButton();
                else
                    panelItem.DeselectButton();
            }
        }

        public void OnPanelItemClicked(EquipmentSection equipmentSection)
        {
            selectedSection = equipmentSection;

            TogglePanelItemSelected(true);

            //EquipmentSection equipmentSection = (EquipmentSection)index;

            equipmentListController.GenerateEquipmentList(selectedSection);
        }

        private void UpdateStatsPanel(Item newEquipment)
        {
            // Get current equipment list
            PlayerEquipment playerEquipment = selectedPlayer.playerEquipment;

            Item equippedItem = playerEquipment.GetEquippedItem(selectedSection);

            List<Item> previewEquipments = new List<Item>();

            for (int i = 0; i < playerEquipment.equippedItems.Count; i++)
            {
                if (equippedItem != null)
                {
                    if (playerEquipment.equippedItems[i].IsSame(equippedItem))
                        continue;
                }

                previewEquipments.Add(playerEquipment.equippedItems[i]);
            }

            if(newEquipment != null)
                previewEquipments.Add(newEquipment);

            BasicStats previewStats = selectedPlayer.playerStatsData.GetPreviewBasicStats(previewEquipments);

            AcademicStats previewAcademicStats = selectedPlayer.playerStatsData.GetPreviewAcademicStats(previewEquipments);

            playerStatsPanelController.CompareBasicStats(previewStats);

            playerStatsPanelController.CompareAcademicStats(previewAcademicStats);
        }

        private void OnListItemClick(Item equipmentItem)
        {
            if (equipmentItem == null)
            {
                selectedPlayer.playerEquipment.UnequipItem(selectedSection);
            }
            else
            {
                selectedPlayer.playerEquipment.EquipItem(equipmentItem);
            }

            InitializeEquippedItems(selectedPlayer);
        }

        private void CloseEquipmentList()
        {
            equipmentListController.Close();

            InitializeEquippedItems(selectedPlayer);

            GameObject lastSelected = GetPanelItem(selectedSection).gameObject;

            UINavigator.instance.SetDefaultSelectedInRuntime(lastSelected);
        }

        public void OnCancel()
        {
            if (equipmentListController.gameObject.activeSelf)
            {
                TogglePanelItemSelected(false);

                CloseEquipmentList();
            }
            else
            {
                int panelIndex = GameUIManager.instance.gameMainMenu.panelIndex;

                UINavigator.instance.OpenPreviousWindow(panelIndex);
            }
        }
    }
}

