﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;

namespace Intelligensia.UI.PlayerMenu
{
    public class EquipmentPanelItem : HighlightingButton
    {
        public EquipmentSection equipmentSection;
        [SerializeField] private TextMeshProUGUI equipmentName = default;
        //public bool isSelected;

        private Item currentItem;

        private const string NotEquipped = "None";


        public void Initialize(PlayerEquipment playerEquipment)
        {
            currentItem = playerEquipment.GetEquippedItem(equipmentSection);

            equipmentName.text = currentItem != null ? currentItem.itemValue.ItemName : NotEquipped;
        }

        public override void OnSelect(BaseEventData eventData)
        {
            base.OnSelect(eventData);

            string description = currentItem != null ? currentItem.GetDescription() : NotEquipped;

            BottomPanelController.instance.SetDescription(description);
        }
    }
}

