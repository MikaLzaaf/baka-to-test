﻿using UnityEngine;
using TMPro;
using System;

namespace Intelligensia.UI.PlayerMenu
{
    public class ActiveSubjectSelectionPanelController : UIViewController
    {
        public event Action<Subjects> SubjectSwitchedEvent;

        [SerializeField] private TextMeshProUGUI subjectText = default;

        private int currentIndex = 0;
        private int noOfSubjects = 0;

        protected override void Awake()
        {
            base.Awake();

            noOfSubjects = System.Enum.GetNames(typeof(Subjects)).Length;
        }

        public override void Show()
        {
            base.Show();

            Subjects currentSubject = DailyInfoManager.activeSubject;

            subjectText.text = StringHelper.SplitByCapitalizeFirstLetter(currentSubject.ToString());

            currentIndex = (int)currentSubject;

            SubjectSwitchedEvent?.Invoke(currentSubject);
        }

        private void SetSubject(Subjects subject)
        {
            subjectText.text = StringHelper.SplitByCapitalizeFirstLetter(subject.ToString());

            DailyInfoManager.activeSubject = subject;

            SubjectSwitchedEvent?.Invoke(subject);
        }

        public void SwitchSubject(int indexChange)
        {
            currentIndex += indexChange;

            if (currentIndex >= noOfSubjects)
            {
                currentIndex = 0;
            }
            else if (currentIndex < 0)
            {
                currentIndex = noOfSubjects - 1;
            }

            Subjects currentSubject = (Subjects)currentIndex;

            SetSubject(currentSubject);
        }


        public void NextSubject()
        {
            SwitchSubject(1);
        }

        public void PreviousSubject()
        {
            SwitchSubject(-1);
        }
    }
}

