﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using Intelligensia.Time;
using TMPro;
using System.Linq;

namespace Intelligensia.UI.PlayerMenu
{
    public class CalendarPanelController : UIViewController
    {
        #region variable & property

        public CalendarController controller;

        public static readonly int max_day_slots = 37;
        
        [Header("UI")]
        public Button btn_nextMonth;
        public Button btn_prevMonth;
        public Button btn_calendar;
        public TextMeshProUGUI label_year;
        public TextMeshProUGUI label_month;
        public TextMeshProUGUI label_dayOfWeek;
        public TextMeshProUGUI label_dayNumber;
        public TextMeshProUGUI label_numberEvents;

        private List<DayObject> days = new List<DayObject>();
        [SerializeField] private GameObject daySlotPrefab;
        [SerializeField] private List<Transform> dayParent;

        // List of days based on day name
        private List<Button> sundayList = new List<Button>();
        private List<Button> mondayList = new List<Button>();
        private List<Button> tuesdayList = new List<Button>();
        private List<Button> wednesdayList = new List<Button>();
        private List<Button> thursdayList = new List<Button>();
        private List<Button> fridayList = new List<Button>();
        private List<Button> saturdayList = new List<Button>();

        private bool isSelectionOnNormalList = true;
        #endregion

        private void Awake()
        {
            
        }

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        public override void Show()
        {
            base.Show();

            isSelectionOnNormalList = true;

            controller.ChangeCalendarTime(controller.currentWorldTime);

        }

        protected override void OnViewReady()
        {
            base.OnViewReady();

            //controller.delegate_oninsertdays += InsertDays;
            var date = controller.currentCalendarTime;
            
            InsertDays(controller.GetIndexOfFirstSlotInMonth(), controller.GetTotalDays());

            UpdateDayNumber(date.day.ToString());
            UpdateMonthLabel(controller.GetMonthStringFromNumber(date.month));
            UpdateYearLabel(date.year.ToString());
            UpdateDayOfWeek(controller.GetDayOfWeek(date.year, date.month, date.day));
            //UpdateNumberEvents(TimeManager.instance.GetEventCountByDay(date.year, date.month, date.day).ToString() + " Daily Events");

            
        }

        public void UpdateYearLabel(string year)
        {
            if (label_year) label_year.text = year;
        }

        public void UpdateMonthLabel(string month)
        {
            if (label_month) label_month.text = month;
        }

        public void UpdateDayOfWeek(string dayOfWeek)
        {
            if (label_dayOfWeek) label_dayOfWeek.text = dayOfWeek;
        }

        public void UpdateDayNumber(string dayNumber)
        {
            if (label_dayNumber) label_dayNumber.text = dayNumber;
        }

        public void UpdateNumberEvents(string numberEvents)
        {
            if (label_numberEvents) label_numberEvents.text = numberEvents;
        }

        void addEventsListener()
        {
            /* btn_nextMonth.onClick.AddListener(() => evtListener_NextMonth());
             btn_prevMonth.onClick.AddListener(() => evtListener_PreviousMonth());
             btn_calendar.onClick.AddListener(() => evtListener_GoToNowday());
             for (int i = 0; i < max_day_slots; i++)
                 GameObject.Find("Slot_" + (i + 1)).GetComponent<Button>().onClick.AddListener(() => evtListener_DaySelected());*/
        }

        public void MarkSelectionDay(int day)
        {
            /* GameObject day_slot = GameObject.Find("Slot_" + (day + currentTime.dayOffset));

             // Change Image
             if (!checkEventExist(currentTime.year, currentTime.month, day))
             {
                 Sprite sprite = Resources.Load<Sprite>("img/circle_unfilled");
                 day_slot.GetComponent<Image>().sprite = sprite;
                 day_slot.GetComponent<Image>().enabled = true;
                 day_slot.GetComponent<Image>().color = FlatCalendarStyle.color_bubbleSelectionMarker;
                 day_slot.GetComponent<Button>().GetComponentInChildren<Text>().color = FlatCalendarStyle.color_dayTextNormal;
             }

             // Update Text
             label_dayOfWeek.text = currentTime.dayOfWeek;
             label_dayNumber.text = "" + currentTime.day;*/
        }

        void unmarkSelctionDay(int day)
        {
            /* GameObject day_slot = GameObject.Find("Slot_" + (day + currentTime.dayOffset));

             // Change Image
             if (!checkEventExist(currentTime.year, currentTime.month, day))
             {
                 setNormalSlot(day + currentTime.dayOffset);
             }*/
        }

        public Transform GetDayParent(int daySlot)
        {
            if (daySlot >= 0 && daySlot <= 6)
            {
                return dayParent[0];
            }
            else if (daySlot >= 7 && daySlot <= 13)
            {
                return dayParent[1];
            }
            else if (daySlot >= 14 && daySlot <= 20)
            {
                return dayParent[2];
            }
            else if (daySlot >= 21 && daySlot <= 27)
            {
                return dayParent[3];
            }
            else if (daySlot >= 28 && daySlot <= 34)
            {
                return dayParent[4];
            }

            return dayParent[5];
        }

        public void InsertDays(int indexFirstSlot, int totalDays)
        {
            List<DayObject> days = new List<DayObject>();

            int totalSlot = indexFirstSlot + totalDays;

            int currentDay = 1;

            for (int i = 0; i < 42; i++)
            {
                var day = ObjectPoolExtensions.Spawn(daySlotPrefab, GetDayParent(i));
                day.transform.localScale = new Vector3(1, 1, 1);    ///to avoid weird bug scale increase when spawn
                var dayObject = day.GetComponent<DayObject>();
                Debug.Log(indexFirstSlot);
                if (i < indexFirstSlot || i >= totalSlot)
                {
                    dayObject.ResetDefault();
                    continue;
                }
                if (currentDay == controller.currentCalendarTime.day)
                {
                    dayObject.SetDayDate(new System.DateTime(controller.currentCalendarTime.year, controller.currentCalendarTime.month, currentDay), true);
                    UINavigator.instance.SetDefaultSelectedInRuntime(dayObject.gameObject);
                }
                else
                {
                    dayObject.SetDayDate(new System.DateTime(controller.currentCalendarTime.year, controller.currentCalendarTime.month, currentDay), false);
                }

                GroupingDayByName(dayObject);

                currentDay++;

                ///Remark : will be insert day event mark here...

                days.Add(dayObject);
            }

            this.days = days;

            UINavigator.instance.SetupVerticalNavigationWithoutReset(sundayList, true);
            UINavigator.instance.SetupVerticalNavigationWithoutReset(mondayList, true);
            UINavigator.instance.SetupVerticalNavigationWithoutReset(tuesdayList, true);
            UINavigator.instance.SetupVerticalNavigationWithoutReset(wednesdayList, true);
            UINavigator.instance.SetupVerticalNavigationWithoutReset(thursdayList, true);
            UINavigator.instance.SetupVerticalNavigationWithoutReset(fridayList, true);
            UINavigator.instance.SetupVerticalNavigationWithoutReset(saturdayList, true);

            

            for(int i = 0; i < dayParent.Count; i++)
            {
                List<Button> week = new List<Button>();

                if (dayParent[i].childCount > 0)
                {
                    week = (dayParent[i].GetComponentsInChildren<Button>()).ToList();

                    UINavigator.instance.SetupHorizontalNavigationWithoutReset(week, true);
                }
            }

            
        }

        public void GroupingDayByName(DayObject day)
        {
            var date = day.GetDate();

            string dayName = controller.GetNameOfDay(date.Year, date.Month, date.Day);

            if (dayName == "Sunday") sundayList.Add(day.gameObject.GetComponent<Button>());
            if (dayName == "Monday") mondayList.Add(day.gameObject.GetComponent<Button>());
            if (dayName == "Tuesday") tuesdayList.Add(day.gameObject.GetComponent<Button>());
            if (dayName == "Wednesday") wednesdayList.Add(day.gameObject.GetComponent<Button>());
            if (dayName == "Thursday") thursdayList.Add(day.gameObject.GetComponent<Button>());
            if (dayName == "Friday") fridayList.Add(day.gameObject.GetComponent<Button>());
            if (dayName == "Saturday") saturdayList.Add(day.gameObject.GetComponent<Button>());
        }
    }

}
