﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;

namespace Intelligensia.UI.PlayerMenu
{
    public class ArgumentsListItem : HighlightingButton, ISelectHandler
    {
        [SerializeField] private Image itemIcon = default;
        [SerializeField] private TextMeshProUGUI itemNameText = default;
        [SerializeField] private TextMeshProUGUI itemAmountText = default;
        [SerializeField] private TextMeshProUGUI itemPointText = default;
        [SerializeField] private CanvasGroup canvasGroup = default;
        [SerializeField] private GameObject equippedLabel = default;
        [SerializeField] private GameObject overlimitLabel = default;

        public Item item { get; set; }
        public int argumentPoint { get; private set; }

        private const string ItemAmountTextFormat = "x {0}";
        private const string ItemPointTextFormat = "{0} <size=60%>AP";


        public void InitializeItem(Item item, bool canInteract, bool isForEquippedList, bool willOverlimit = false)
        {
            this.item = item;

            itemNameText.text = item.itemValue.ItemName;

            itemAmountText.text = isForEquippedList ? "" : string.Format(ItemAmountTextFormat, item.GetEquipableAmount());

            if (isForEquippedList)
            {
                ToggleItemAmountText(false);
                ToggleEquippedLabel(false);
                ToggleOverlimitLabel(false);
            }
            else
            {
                ToggleItemAmountText(true);

                if(willOverlimit)
                {
                    ToggleEquippedLabel(false);
                    ToggleOverlimitLabel(true);
                }
                else
                {
                    ToggleEquippedLabel(!canInteract);
                    ToggleOverlimitLabel(false);
                }
            }

            SkillItemBaseValue skillItemBase = (SkillItemBaseValue)item.itemValue;

            argumentPoint = skillItemBase.baseSkill.skillPoint;
            itemPointText.text = string.Format(ItemPointTextFormat, argumentPoint);

            itemIcon.sprite = IconLoader.GetItemIcon(item.itemValue.ItemIcon);

            interactable = willOverlimit ? false : canInteract;
            SetCanvasGroupAlpha();
        }

        private void SetCanvasGroupAlpha()
        {
            if (canvasGroup != null)
                canvasGroup.alpha = interactable ? 1f : 0.5f;
        }

        private void ToggleEquippedLabel(bool active)
        {
            if(equippedLabel != null)
                equippedLabel.SetActive(active);
        }

        private void ToggleOverlimitLabel(bool active)
        {
            if (overlimitLabel != null)
                overlimitLabel.SetActive(active);
        }

        private void ToggleItemAmountText(bool active)
        {
            if (itemAmountText != null)
                itemAmountText.gameObject.SetActive(active);
        }

        public override void OnSelect(BaseEventData eventData)
        {
            base.OnSelect(eventData);

            if (item != null)
            {
                string description = item.GetDescription();

                BottomPanelController.instance.SetDescription(description);
            }
        }
    }
}

