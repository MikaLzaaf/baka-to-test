﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Linq;

namespace Intelligensia.UI.PlayerMenu
{
    public class ArgumentsEquippedListController : MonoBehaviour
    {
        [SerializeField] private ArgumentsListItem listItemPrefab = default;
        [SerializeField] private Transform listContainer = default;
        //[SerializeField] private ArgumentsListController normalListController = default;

        public event Action<Item> ListItemClickedEvent;
        public Selectable firstListItem
        {
            get
            {
                if (listItems == null || listItems.Count == 0)
                    return null;

                return listItems[0];
            }
        }

        private List<ArgumentsListItem> listItems;

        private CharacterId currentCharacterId;
        private CharacterId lastCharacterId;
        private int lastSelectedIndex = 0;

        public void GenerateList(ControllableEntity currentEntity)
        {
            currentCharacterId = currentEntity.characterId;

            Item[] equippedSkillItems = currentEntity.playerStatsData.skillsEquipped.ToArray();

            GenerateList(equippedSkillItems);
        }

        public void GenerateList(Item[] skillItems)
        {
            ClearList();

            for (int i = 0; i < skillItems.Length; i++)
            {
                ArgumentsListItem listItem = Instantiate(listItemPrefab);
                listItems.Add(listItem);

                listItem.transform.SetParent(listContainer, false);
                listItem.InitializeItem(skillItems[i], true, true);

                listItem.onClick.AddListener(delegate
                {
                    SelectArgumentItem(listItem);
                });
            }

            UINavigator.instance.SetupVerticalNavigation(listItems, true);

            lastCharacterId = currentCharacterId;
        }

        private void ClearList()
        {
            if (listItems != null && listItems.Count > 0)
            {
                if (listItems.Count > 0)
                {
                    for (int i = listItems.Count - 1; i >= 0; i--)
                    {
                        Destroy(listItems[i].gameObject);
                    }
                }

                listItems.Clear();
            }
            else
            {
                listItems = new List<ArgumentsListItem>();
            }
        }

        private void SelectArgumentItem(ArgumentsListItem listItem)
        {
            lastSelectedIndex = listItems.IndexOf(listItem);

            ListItemClickedEvent?.Invoke(listItem.item);
        }

        public void SetSelectedItem()
        {
            if (listItems.Count > 0)
            {
                if (lastCharacterId == currentCharacterId)
                {
                    int index = lastSelectedIndex < listItems.Count ? lastSelectedIndex : listItems.Count - 1;

                    UINavigator.instance.SetDefaultSelectedInRuntime(listItems[index].gameObject);
                }
                else
                    UINavigator.instance.SetDefaultSelectedInRuntime(listItems[0].gameObject);
            }
        }

        public void SortListItems()
        {
            var sortedList = listItems.OrderByDescending(x => x.argumentPoint).ToList();

            List<Item> items = new List<Item>();

            for (int i = 0; i < sortedList.Count; i++)
            {
                items.Add(sortedList[i].item);
            }

            GenerateList(items.ToArray());
        }
    }
}

