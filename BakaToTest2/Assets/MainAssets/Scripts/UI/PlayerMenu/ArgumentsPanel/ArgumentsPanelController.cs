﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace Intelligensia.UI.PlayerMenu
{
    public class ArgumentsPanelController : UIViewController
    {
        [Header("Panels")]
        [SerializeField] private PlayerSelectionPanelController playerSelectionPanelController = default;
        [SerializeField] private ArgumentsListController normalListController = default;
        [SerializeField] private ArgumentsEquippedListController equippedListController = default;

        [Header("UI")]
        [SerializeField] private TextMeshProUGUI characterName = default;
        [SerializeField] private TextMeshProUGUI overallGradeText = default;
        [SerializeField] private TextMeshProUGUI argumentsWeightText = default;

        [Header("List Items")]
        [SerializeField] private ArgumentsCategoryListItem[] categoryListItems = default;

        private int categoryIndex = 0;
        private bool isSelectionOnNormalList = true;


        public override void Show()
        {
            base.Show();

            isSelectionOnNormalList = true;

            // Showing flow - playerSelectionPanelController.GeneratePlayerIcons > PlayerChangedEvent invoked > GenerateScreen called
            playerSelectionPanelController.PlayerChangedEvent += GenerateScreen;

            playerSelectionPanelController.GeneratePlayerIcons();

            SwitchCategoryOnClicked(0);

            BottomPanelController.instance.SetCurrentPanelName("Arguments");
            //BottomPanelController.instance.ShowInputLabel(ButtonInputLabelId.ArgumentPanelInputNormal);
        }


        protected override void OnViewReady()
        {
            base.OnViewReady();

            PlayerMenuManager playerMenuManager = GameUIManager.instance.gameMainMenu;

            if (playerMenuManager != null)
            {
                playerMenuManager.OnRightTabPressedEvent += playerSelectionPanelController.NextCharacter;
                playerMenuManager.OnLeftTabPressedEvent += playerSelectionPanelController.PreviousCharacter;

                playerMenuManager.OnRightTriggerPressedEvent += NextCategory;
                playerMenuManager.OnLeftTriggerPressedEvent += PreviousCategory;

                playerMenuManager.OnCancelPressedEvent += OnCancelPressed;
                playerMenuManager.OnInfoButtonPressedEvent += SortList;
                playerMenuManager.OnNorthButtonPressedEvent += SwitchSelectionToOtherList;
            }

            normalListController.ListItemClickedEvent += EquipNewSkill;
            equippedListController.ListItemClickedEvent += UnequipSkill;
        }

        public override void Close()
        {
            base.Close();

            PlayerMenuManager playerMenuManager = GameUIManager.instance.gameMainMenu;

            if (playerMenuManager != null)
            {
                playerMenuManager.OnRightTabPressedEvent -= playerSelectionPanelController.NextCharacter;
                playerMenuManager.OnLeftTabPressedEvent -= playerSelectionPanelController.PreviousCharacter;

                playerMenuManager.OnRightTriggerPressedEvent -= NextCategory;
                playerMenuManager.OnLeftTriggerPressedEvent -= PreviousCategory;

                playerMenuManager.OnCancelPressedEvent -= OnCancelPressed;
                playerMenuManager.OnInfoButtonPressedEvent -= SortList;
                playerMenuManager.OnNorthButtonPressedEvent -= SwitchSelectionToOtherList;
            }

            playerSelectionPanelController.PlayerChangedEvent -= GenerateScreen;
            normalListController.ListItemClickedEvent -= EquipNewSkill;
            equippedListController.ListItemClickedEvent -= UnequipSkill;

            playerSelectionPanelController.Close();
        }


        private void GenerateScreen(ControllableEntity playerEntity)
        {
            characterName.text = playerEntity.characterName;
            overallGradeText.text = playerEntity.playerStatsData.currentAcademicStats.GetOverallGrade();
            argumentsWeightText.text = playerEntity.playerStatsData.GetArgumentWeightDescription();

            normalListController.GenerateList(playerEntity, (ArgumentsCategory)categoryIndex);
            equippedListController.GenerateList(playerEntity);

            SetSelectedItem();
        }

        private void SetSelectedItem()
        {
            if (equippedListController.firstListItem == null || isSelectionOnNormalList && normalListController.firstListItem != null)
            {
                normalListController.SetSelectedItem();
                BottomPanelController.instance.ShowInputLabel(ButtonInputLabelId.ArgumentPanelInputNormal);

                isSelectionOnNormalList = true;
            }
            else if (normalListController.firstListItem == null || !isSelectionOnNormalList && equippedListController.firstListItem != null)
            {
                equippedListController.SetSelectedItem();
                BottomPanelController.instance.ShowInputLabel(ButtonInputLabelId.ArgumentPanelInputEquipped);

                isSelectionOnNormalList = false;
            }
        }

        public void EquipNewSkill(Item skillItem)
        {
            ControllableEntity currentEntity = playerSelectionPanelController.GetSelectedCharacter();
            currentEntity.playerStatsData.EquipSkill(skillItem);

            GenerateScreen(currentEntity);
        }

        public void UnequipSkill(Item skillItem)
        {
            ControllableEntity currentEntity = playerSelectionPanelController.GetSelectedCharacter();
            currentEntity.playerStatsData.UnequipSkill(skillItem);

            GenerateScreen(currentEntity);
        }

        private void SwitchCategory(int value)
        {
            int nextIndex = categoryIndex + value;

            if (nextIndex >= categoryListItems.Length)
            {
                nextIndex = 0;
            }
            else if (nextIndex < 0)
            {
                nextIndex = categoryListItems.Length - 1;
            }

            categoryListItems[nextIndex].onClick?.Invoke();
        }

        public void NextCategory()
        {
            SwitchCategory(1);
        }

        public void PreviousCategory()
        {
            SwitchCategory(-1);
        }

        // Assign on inspector of AcademicSkillCategoryListItems
        public void SwitchCategoryOnClicked(int index)
        {
            if (categoryIndex != index)
            {
                categoryListItems[categoryIndex].ToggleHighlight(false);
            }

            categoryIndex = index;

            categoryListItems[categoryIndex].ToggleHighlight(true);

            ControllableEntity currentEntity = playerSelectionPanelController.GetSelectedCharacter();
            normalListController.GenerateList(currentEntity, (ArgumentsCategory)categoryIndex);

            if(isSelectionOnNormalList && normalListController.firstListItem != null)
                normalListController.SetSelectedItem();
        }

        private void OnCancelPressed()
        {
            int panelIndex = GameUIManager.instance.gameMainMenu.panelIndex;

            UINavigator.instance.OpenPreviousWindow(panelIndex);
        }

        private void SortList()
        {
            ControllableEntity currentEntity = playerSelectionPanelController.GetSelectedCharacter();

            normalListController.SortListItems(currentEntity);
            equippedListController.SortListItems();

            SetSelectedItem();
        }

        private void SwitchSelectionToOtherList()
        {
            if (normalListController == null || equippedListController == null)
                return;

            if (normalListController.firstListItem == null || equippedListController.firstListItem == null)
                return;

            isSelectionOnNormalList = !isSelectionOnNormalList;

            if(isSelectionOnNormalList)
            {
                UINavigator.instance.SetDefaultSelectedInRuntime(normalListController.firstListItem.gameObject);
                BottomPanelController.instance.ShowInputLabel(ButtonInputLabelId.ArgumentPanelInputNormal);
            }
            else
            {
                UINavigator.instance.SetDefaultSelectedInRuntime(equippedListController.firstListItem.gameObject);
                BottomPanelController.instance.ShowInputLabel(ButtonInputLabelId.ArgumentPanelInputEquipped);
            }
        }
    }
}

