﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Linq;

namespace Intelligensia.UI.PlayerMenu
{
    public class ArgumentsListController : MonoBehaviour
    {
        [SerializeField] private ArgumentsListItem listItemPrefab = default;
        [SerializeField] private Transform listContainer = default;
        //[SerializeField] private ArgumentsEquippedListController equippedListController = default;


        public event Action<Item> ListItemClickedEvent;
        public Selectable firstListItem
        {
            get
            {
                if (listItems == null || listItems.Count == 0)
                    return null;

                return listItems[0];
            }
        }

        private List<ArgumentsListItem> listItems;

        private CharacterId currentCharacterId;
        private CharacterId lastCharacterId;
        private int lastSelectedIndex = 0;

        public void GenerateList(ControllableEntity currentEntity, ArgumentsCategory argumentsCategory)
        {
            currentCharacterId = currentEntity.characterId;

            Item[] items = PlayerInventoryManager.instance.GetItemsByCategory(ItemCategory.Skill);

            List<Item> argumentItems = new List<Item>();

            for (int i = 0; i < items.Length; i++)
            {
                SkillItemBaseValue skillItemBase = (SkillItemBaseValue)items[i].itemValue;

                if(skillItemBase.IsUniqueSkill)
                {
                    if (skillItemBase.uniqueUser != currentCharacterId)
                        continue;
                }

                if (argumentsCategory == ArgumentsCategory.All)
                {
                    argumentItems.Add(items[i]);
                    continue;
                }

                if (skillItemBase.argumentsCategory == argumentsCategory)
                    argumentItems.Add(items[i]);
            }

            GenerateList(currentEntity, argumentItems.ToArray());
        }

        public void GenerateList(ControllableEntity currentEntity, Item[] argumentItems)
        {
            ClearList();

            for (int i = 0; i < argumentItems.Length; i++)
            {
                if (argumentItems[i].GetEquipableAmount() == 0)
                    continue;

                bool canInteract = !currentEntity.playerStatsData.IsSkillEquipped(argumentItems[i]);
                bool willOverlimit = currentEntity.playerStatsData.WillOverlimit(argumentItems[i]);

                ArgumentsListItem listItem = Instantiate(listItemPrefab);
                listItems.Add(listItem);

                listItem.transform.SetParent(listContainer, false);
                listItem.InitializeItem(argumentItems[i], canInteract, false, willOverlimit);

                listItem.onClick.AddListener(delegate
                {
                    SelectArgumentItem(listItem);
                });
            }

            UINavigator.instance.SetupVerticalNavigation(listItems, true);

            //if (listItems.Count > 0 && setSelectedHere)
            //{
            //    if (lastCharacterId == currentCharacterId)
            //    {
            //        int index = lastSelectedIndex < listItems.Count ? lastSelectedIndex : listItems.Count - 1;

            //        UINavigator.instance.SetDefaultSelectedInRuntime(listItems[index].gameObject);
            //    }
            //    else
            //        UINavigator.instance.SetDefaultSelectedInRuntime(listItems[0].gameObject);
            //}

            lastCharacterId = currentCharacterId;
        }

        private void ClearList()
        {
            if (listItems != null && listItems.Count > 0)
            {
                if (listItems.Count > 0)
                {
                    for (int i = listItems.Count - 1; i >= 0; i--)
                    {
                        Destroy(listItems[i].gameObject);
                    }
                }

                listItems.Clear();
            }
            else
            {
                listItems = new List<ArgumentsListItem>();
            }
        }

        private void SelectArgumentItem(ArgumentsListItem listItem)
        {
            lastSelectedIndex = listItems.IndexOf(listItem);

            ListItemClickedEvent?.Invoke(listItem.item);
        }

        public void SetSelectedItem()
        {
            if (listItems.Count > 0)
            {
                if (lastCharacterId == currentCharacterId)
                {
                    int index = lastSelectedIndex < listItems.Count ? lastSelectedIndex : listItems.Count - 1;

                    UINavigator.instance.SetDefaultSelectedInRuntime(listItems[index].gameObject);
                }
                else
                    UINavigator.instance.SetDefaultSelectedInRuntime(listItems[0].gameObject);
            }
        }

        public void SortListItems(ControllableEntity currentEntity)
        {
            var sortedList = listItems.OrderByDescending(x => x.argumentPoint).ToList();

            List<Item> items = new List<Item>();

            for (int i = 0; i < sortedList.Count; i++)
            {
                items.Add(sortedList[i].item);
            }

            GenerateList(currentEntity, items.ToArray());
        }
    }
}

