﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;

namespace Intelligensia.UI.PlayerMenu
{
    public class PartyRosterSelectionListItem_ActiveMember : HighlightingButton, ISelectHandler, ISubmitHandler
    {
        [SerializeField] private TextMeshProUGUI nameText = default;
        [SerializeField] private TextMeshProUGUI ilmuAmountText = default;
        [SerializeField] private TextMeshProUGUI subjectScoreText = default;

        [SerializeField] private GameObject leaderLabel = default;
        [SerializeField] private Image memberPortrait = default;
        //[SerializeField] private ImageColorLerper iconHighlighter = default;
        [SerializeField] private GameObject selectedHighlight = default;

        //private const string IlmuPrefix = "Ilmu_";

        private PartyRosterSelectionPanelController rosterSelectionPanelController;
        public ControllableEntity activeMember { get; private set; }
        public bool isMemberSelected;


        public void InitializeItem(ControllableEntity member, PartyRosterSelectionPanelController rosterSelectionPanelController, bool closeSelectedHighlight = false)
        {
            activeMember = member;
            this.rosterSelectionPanelController = rosterSelectionPanelController;

            nameText.text = member == null ? "" : activeMember.characterName;
            
            memberPortrait.sprite = member == null ? null : PlayerInfoManager.LoadCharacterPortrait(activeMember.characterId);

            if (closeSelectedHighlight)
            {
                ToggleSelectedHighlight(false);
            }
        }

        public void SetSubjectScore(Subjects subjects)
        {
            if (activeMember == null)
                return;

            int subjectScore = activeMember.playerStatsData.currentAcademicStats.GetStatsValue(subjects);

            subjectScoreText.text = subjectScore.ToString();

            Item ilmuItem = PlayerInventoryManager.instance.GetIlmuBySubject(subjects);

            ilmuAmountText.text = ilmuItem == null ? "0" : ilmuItem.amount.ToString("N0");
        }

        public void ToggleLeaderLabel(bool active)
        {
            if (leaderLabel != null)
                leaderLabel.SetActive(active);
        }

        //public void ToggleHighlight(bool active)
        //{
        //    if (iconHighlighter != null)
        //    {
        //        iconHighlighter.gameObject.SetActive(active);
        //    }
        //}

        public void ToggleSelectedHighlight(bool active)
        {
            if (selectedHighlight != null)
            {
                selectedHighlight.SetActive(active);
            }

            isMemberSelected = active;
        }

        public override void OnSelect(BaseEventData eventData)
        {
            base.OnSelect(eventData);

            rosterSelectionPanelController.SetInspectedActiveMember(this);

            //ToggleHighlight(true);
        }

        //public override void OnDeselect(BaseEventData eventData)
        //{
        //    base.OnDeselect(eventData);

        //    ToggleHighlight(false);
        //}

        public override void OnSubmit(BaseEventData eventData)
        {
            base.OnSubmit(eventData);

            rosterSelectionPanelController.StartSwitching();
        }
    }
}

