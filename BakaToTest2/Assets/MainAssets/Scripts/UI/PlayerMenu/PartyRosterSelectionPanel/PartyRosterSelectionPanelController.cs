﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Intelligensia.UI.PlayerMenu
{
    public class PartyRosterSelectionPanelController : UIViewController
    {
        [Header("Active Members")]
        [SerializeField] private PartyRosterSelectionListItem_ActiveMember activeMemberPrefab = default;
        [SerializeField] Transform activeMemberContainer = default;

        [Header("Reserve Members")]
        [SerializeField] private PartyRosterSelectionListItem_ReserveMember reserveMemberPrefab = default;
        [SerializeField] Transform reserveMemberContainer = default;

        private List<PartyRosterSelectionListItem_ActiveMember> activeMembers;
        private List<PartyRosterSelectionListItem_ReserveMember> reserveMembers;

        private PartyRosterSelectionListItem_ActiveMember inspectedActiveMember;
        private PartyRosterSelectionListItem_ReserveMember inspectedReserveMember;

        private PartyRosterSelectionListItem_ActiveMember selectedActiveMember;
        private PartyRosterSelectionListItem_ReserveMember selectedReserveMember;

        //private ControllableEntity selectedMember;
        private bool selectedIsReserveMember;

        public GameObject firstActiveMember => activeMembers[0].gameObject;

        public bool isSwitchingMember { get; private set; }



        public override void Show()
        {
            base.Show();

            GenerateActiveMembersList();
            GenerateReserveMembersList();
        }

        public override void Close()
        {
            base.Close();

            inspectedActiveMember = null;
            inspectedReserveMember = null;

            SortPartyMembers();
        }

        private void GenerateActiveMembersList()
        {
            if (activeMembers != null && activeMembers.Count > 0)
            {
                for (int i = 0; i < activeMembers.Count; i++)
                {
                    Destroy(activeMembers[i].gameObject);
                }

                activeMembers.Clear();
            }
            else
                activeMembers = new List<PartyRosterSelectionListItem_ActiveMember>();

            var allMembers = PlayerPartyManager.instance.partyMembers;

            for (int i = 0; i < allMembers.Count; i++)
            {
                if (i == PlayerPartyManager.instance.activeMembersCount)
                    break;

                CreateActiveMemberListItem(allMembers[i]);
            }
        }

        private void CreateActiveMemberListItem(ControllableEntity activeMember)
        {
            PartyRosterSelectionListItem_ActiveMember listItem = Instantiate(activeMemberPrefab);
            listItem.transform.SetParent(activeMemberContainer, false);

            listItem.InitializeItem(activeMember, this);

            activeMembers.Add(listItem);
        }

        private void GenerateReserveMembersList()
        {
            if (reserveMembers != null && reserveMembers.Count > 0)
            {
                for (int i = 0; i < reserveMembers.Count; i++)
                {
                    Destroy(reserveMembers[i].gameObject);
                }

                reserveMembers.Clear();
            }
            else
                reserveMembers = new List<PartyRosterSelectionListItem_ReserveMember>();

            var allMembers = PlayerPartyManager.instance.partyMembers;

            if (allMembers.Count > PlayerPartyManager.instance.activeMembersCount)
            {
                for (int i = PlayerPartyManager.instance.activeMembersCount; i < allMembers.Count; i++)
                {
                    CreateReserveMemberListItem(allMembers[i]);
                }
            }

            SetupActiveMembersNavigation(activeMembers, false, !isSwitchingMember && reserveMembers.Count > 0);
            SetupReserveMembersNavigation(reserveMembers, true, true);
        }

        private void CreateReserveMemberListItem(ControllableEntity reserveMember)
        {
            PartyRosterSelectionListItem_ReserveMember listItem = Instantiate(reserveMemberPrefab);
            listItem.transform.SetParent(reserveMemberContainer, false);

            listItem.InitializeItem(reserveMember, this);

            reserveMembers.Add(listItem);
        }

        public void SetInspectedActiveMember(PartyRosterSelectionListItem_ActiveMember listItem)
        {
            inspectedReserveMember = null;
            inspectedActiveMember = listItem;

            BottomPanelController.instance.ShowInputLabel(ButtonInputLabelId.PartyOrderActiveMember);
        }

        public void SetInspectedReserveMember(PartyRosterSelectionListItem_ReserveMember listItem)
        {
            inspectedActiveMember = null;
            inspectedReserveMember = listItem;

            BottomPanelController.instance.ShowInputLabel(ButtonInputLabelId.PartyOrderReserveMember);
        }

        public void MoveMember()
        {
            if (inspectedActiveMember == null)
            {
                MoveReserveMemberToActive();
            }
            else if (inspectedReserveMember == null)
            {
                MoveActiveMemberToReserve();
            }
        }

        public void MoveActiveMemberToReserve()
        {
            if (PlayerPartyManager.instance.activeMembersCount <= 1)
                return;

            CreateReserveMemberListItem(inspectedActiveMember.activeMember);

            activeMembers.Remove(inspectedActiveMember);
            Destroy(inspectedActiveMember.gameObject);

            inspectedActiveMember = null;

            PlayerPartyManager.instance.activeMembersCount -= 1;

            SetupActiveMembersNavigation(activeMembers, false, !isSwitchingMember && reserveMembers.Count > 0);
            SetupReserveMembersNavigation(reserveMembers, true, true);

            UINavigator.instance.SetDefaultSelectedInRuntime(reserveMembers[0].gameObject);
        }

        public void MoveReserveMemberToActive()
        {
            if (PlayerPartyManager.instance.activeMembersCount >= 4)
                return;

            CreateActiveMemberListItem(inspectedReserveMember.reserveMember);

            reserveMembers.Remove(inspectedReserveMember);
            Destroy(inspectedReserveMember.gameObject);

            inspectedReserveMember = null;

            PlayerPartyManager.instance.activeMembersCount += 1;

            SetupActiveMembersNavigation(activeMembers, false, !isSwitchingMember && reserveMembers.Count > 0);
            SetupReserveMembersNavigation(reserveMembers, true, true);

            UINavigator.instance.SetDefaultSelectedInRuntime(activeMembers[0].gameObject);
        }

        public void SwitchMember()
        {
            bool targetIsReserveMember = inspectedReserveMember != null;

            ControllableEntity targetMember = targetIsReserveMember ?
                inspectedReserveMember.reserveMember : inspectedActiveMember.activeMember;

            ControllableEntity selectedMember = selectedIsReserveMember ?
                selectedReserveMember.reserveMember : selectedActiveMember.activeMember;

            if (selectedIsReserveMember)
                selectedReserveMember.InitializeItem(targetMember, this, true);
            else
                selectedActiveMember.InitializeItem(targetMember, this, true);

            if (targetIsReserveMember)
                inspectedReserveMember.InitializeItem(selectedMember, this, true);
            else
                inspectedActiveMember.InitializeItem(selectedMember, this, true);

            selectedReserveMember = null;
            selectedActiveMember = null;

            isSwitchingMember = false;

            SetupActiveMembersNavigation(activeMembers, false, !isSwitchingMember && reserveMembers.Count > 0);
            SetupReserveMembersNavigation(reserveMembers, true, true);

            // Return to the inspected active member list item
            UINavigator.instance.SetDefaultSelectedInRuntime(targetIsReserveMember ?
                inspectedReserveMember.gameObject : inspectedActiveMember.gameObject);
        }

        public void StartSwitching()
        {
            if (selectedActiveMember != null || selectedReserveMember != null)
            {
                SwitchMember();
                return;
            }

            if (inspectedActiveMember != null)
            {
                selectedActiveMember = inspectedActiveMember;
                selectedIsReserveMember = false;

                inspectedActiveMember.ToggleSelectedHighlight(true);
            }
            else if (inspectedReserveMember != null)
            {
                selectedReserveMember = inspectedReserveMember;
                selectedIsReserveMember = true;

                inspectedReserveMember.ToggleSelectedHighlight(true);
            }

            isSwitchingMember = true;

            BottomPanelController.instance.ShowInputLabel(ButtonInputLabelId.PartyOrderSwitching);
        }

        public void CancelSwitching()
        {
            isSwitchingMember = false;

            selectedReserveMember = null;
            selectedActiveMember = null;

            SetupActiveMembersNavigation(activeMembers, false, !isSwitchingMember && reserveMembers.Count > 0);
            SetupReserveMembersNavigation(reserveMembers, true, true);

            if (inspectedActiveMember.isMemberSelected)
            {
                inspectedActiveMember.ToggleSelectedHighlight(false);

                UINavigator.instance.SetDefaultSelectedInRuntime(activeMembers[0].gameObject);
            }
            else if (inspectedReserveMember.isMemberSelected)
            {
                inspectedReserveMember.ToggleSelectedHighlight(false);

                UINavigator.instance.SetDefaultSelectedInRuntime(reserveMembers[0].gameObject);
            }
        }

        private void SortPartyMembers()
        {
            List<CharacterId> partyMembersIdList = new List<CharacterId>();
            List<ControllableEntity> partyList = new List<ControllableEntity>();

            for (int i = 0; i < activeMembers.Count; i++)
            {
                partyMembersIdList.Add(activeMembers[i].activeMember.characterId);
                partyList.Add(activeMembers[i].activeMember);
            }

            for (int i = 0; i < reserveMembers.Count; i++)
            {
                partyMembersIdList.Add(reserveMembers[i].reserveMember.characterId);
                partyList.Add(reserveMembers[i].reserveMember);
            }

            PlayerPartyManager.instance.partyMembersIdList = partyMembersIdList;
            PlayerPartyManager.instance.partyMembers = partyList;
        }

        public List<T> SetupActiveMembersNavigation<T>(List<T> listItems, bool loop, bool canMoveToReserve) where T : Button
        {
            for (int i = 0; i < listItems.Count; i++)
            {
                Navigation navi = listItems[i].navigation;

                if (i == 0)
                {
                    navi.selectOnUp = reserveMembers.Count > 0 && canMoveToReserve ? reserveMembers[reserveMembers.Count - 1] : null;
                    navi.selectOnLeft = loop ? listItems[listItems.Count - 1] : null;
                }
                else
                {
                    navi.selectOnUp = reserveMembers.Count > 0 && canMoveToReserve ? reserveMembers[reserveMembers.Count - 1] : null;
                    navi.selectOnLeft = listItems[i - 1];
                }

                if (i == listItems.Count - 1)
                {
                    navi.selectOnDown = reserveMembers.Count > 0 && canMoveToReserve ? reserveMembers[0] : null;
                    navi.selectOnRight = canMoveToReserve ? reserveMembers[0] : null;
                    //navi.selectOnRight = loop ? listItems[0] : null;
                }
                else
                {
                    navi.selectOnDown = reserveMembers.Count > 0 && canMoveToReserve ? reserveMembers[0] : null;
                    navi.selectOnRight = listItems[i + 1];
                }

                listItems[i].navigation = navi;
            }

            return listItems;
        }

        public List<T> SetupReserveMembersNavigation<T>(List<T> listItems, bool loop, bool canMoveToActive) where T : Button
        {
            for (int i = 0; i < listItems.Count; i++)
            {
                Navigation navi = listItems[i].navigation;

                if (i == 0)
                {
                    navi.selectOnUp = loop ? listItems[listItems.Count - 1] : null;
                    navi.selectOnLeft = canMoveToActive ? activeMembers[activeMembers.Count - 1] : null;
                }
                else
                {
                    navi.selectOnUp = listItems[i - 1];
                    navi.selectOnLeft = canMoveToActive ? activeMembers[activeMembers.Count - 1] : null;
                }

                if (i == listItems.Count - 1)
                {
                    navi.selectOnDown = loop ? listItems[0] : null;
                    navi.selectOnRight = null;
                    //navi.selectOnRight = canMoveToActive ? activeMembers[0] : null;
                }
                else
                {
                    navi.selectOnDown = listItems[i + 1];
                    navi.selectOnRight = null;
                }

                listItems[i].navigation = navi;
            }

            return listItems;
        }

        public void ChangeSubjectScore(Subjects subject)
        {
            for (int i = 0; i < activeMembers.Count; i++)
            {
                activeMembers[i].SetSubjectScore(subject);
            }

            for (int i = 0; i < reserveMembers.Count; i++)
            {
                reserveMembers[i].SetSubjectScore(subject);
            }
        }
    }
}

