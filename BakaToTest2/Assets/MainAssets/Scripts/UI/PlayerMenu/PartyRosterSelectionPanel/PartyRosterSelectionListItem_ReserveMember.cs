﻿using UnityEngine;
using TMPro;
using UnityEngine.EventSystems;

namespace Intelligensia.UI.PlayerMenu
{
    public class PartyRosterSelectionListItem_ReserveMember : HighlightingButton, ISelectHandler, ISubmitHandler
    {
        [SerializeField] private TextMeshProUGUI nameText = default;
        [SerializeField] private TextMeshProUGUI ilmuAmountText = default;
        [SerializeField] private TextMeshProUGUI subjectScoreText = default;

        //[SerializeField] private ImageColorLerper iconHighlighter = default;
        [SerializeField] private GameObject selectedHighlight = default;

        //private const string IlmuPrefix = "Ilmu_";

        private PartyRosterSelectionPanelController rosterSelectionPanelController;
        public ControllableEntity reserveMember { get; private set; }
        public bool isMemberSelected;

        public void InitializeItem(ControllableEntity member, PartyRosterSelectionPanelController rosterSelectionPanelController, bool closeSelectedHighlight = false)
        {
            reserveMember = member;
            this.rosterSelectionPanelController = rosterSelectionPanelController;

            nameText.text = reserveMember.characterName;
            //ilmuAmountText.text = member == null ? "0" : "0";

            if (closeSelectedHighlight)
            {
                ToggleSelectedHighlight(false);
            }
        }

        public void SetSubjectScore(Subjects subjects)
        {
            //SubjectStats subjectStats = reserveMember.playerStatsData.currentAcademicStats.GetSubjectStats(subjects);

            int subjectScore = reserveMember.playerStatsData.currentAcademicStats.GetStatsValue(subjects);

            subjectScoreText.text = subjectScore.ToString();

            //Item ilmuItem = PlayerInventoryManager.instance.GetItem(IlmuPrefix + subjects.ToString());
            Item ilmuItem = PlayerInventoryManager.instance.GetIlmuBySubject(subjects);

            ilmuAmountText.text = ilmuItem == null ? "0" : ilmuItem.amount.ToString("N0");
        }

        //public void ToggleHighlight(bool active)
        //{
        //    if (iconHighlighter != null)
        //    {
        //        iconHighlighter.gameObject.SetActive(active);
        //    }
        //}

        public void ToggleSelectedHighlight(bool active)
        {
            if (selectedHighlight != null)
            {
                selectedHighlight.SetActive(active);
            }

            isMemberSelected = active;
        }

        public override void OnSelect(BaseEventData eventData)
        {
            base.OnSelect(eventData);

            rosterSelectionPanelController.SetInspectedReserveMember(this);

            //ToggleHighlight(true);
        }

        //public override void OnDeselect(BaseEventData eventData)
        //{
        //    base.OnDeselect(eventData);

        //    ToggleHighlight(false);
        //}

        public override void OnSubmit(BaseEventData eventData)
        {
            base.OnSubmit(eventData);

            rosterSelectionPanelController.StartSwitching();
        }
    }
}

