﻿using UnityEngine;

namespace Intelligensia.UI.PlayerMenu
{
    public class ButtonInputLabel : MonoBehaviour
    {
        public ButtonInputLabelId inputLabelId;
    }

    public enum ButtonInputLabelId
    {
        Undefined,
        ToPartyOrder,
        PartyOrderActiveMember,
        PartyOrderReserveMember,
        PartyOrderSwitching,
        SortItems,
        AcademicPanelInput,
        ArgumentPanelInputNormal,
        ArgumentPanelInputEquipped
    }
}
