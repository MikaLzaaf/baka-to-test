﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace Intelligensia.UI.PlayerMenu
{
    public class DescriptorButtons : Button, ISelectHandler
    {
        public string descriptionID;

        public override void OnSelect(BaseEventData eventData)
        {
            base.OnSelect(eventData);

            string description = LocalizationManager.Localize(descriptionID);

            BottomPanelController.instance.SetDescription(description);
        }
    }
}

