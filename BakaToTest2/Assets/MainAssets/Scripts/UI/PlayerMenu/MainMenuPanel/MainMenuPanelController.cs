﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Intelligensia.UI.PlayerMenu
{
    public class MainMenuPanelController : UIViewController
    {
        [Header("Panels")]
        public ActiveSubjectSelectionPanelController activeSubjectSelectionPanel;
        public PartyRosterSelectionPanelController rosterSelectionPanelController;
        public GameObject[] mainButtons;


        private const string ChangePartyOrderDescriptionID = "MainMenu_PartyRoster";


        private bool _isNavigatingRoster = false;
        private bool isNavigatingRoster
        {
            get
            {
                return _isNavigatingRoster;
            }
            set
            {
                _isNavigatingRoster = value;
                UINavigator.instance.canOpenPreviousWindows = !_isNavigatingRoster;
            }
        }


        protected override void Awake()
        {
            base.Awake();

            if (rosterSelectionPanelController != null && activeSubjectSelectionPanel != null)
            {
                activeSubjectSelectionPanel.SubjectSwitchedEvent += rosterSelectionPanelController.ChangeSubjectScore;
            }
        }


        private void OnDestroy()
        {
            if (rosterSelectionPanelController != null && activeSubjectSelectionPanel != null)
            {
                activeSubjectSelectionPanel.SubjectSwitchedEvent -= rosterSelectionPanelController.ChangeSubjectScore;
            }
        }


        public override void Show()
        {
            base.Show();

            rosterSelectionPanelController.Show();
            activeSubjectSelectionPanel.Show();

            GameUIManager.instance.gameMainMenu.isOnMainMenu = true;

            BottomPanelController.instance.SetCurrentPanelName("Main Menu");
            BottomPanelController.instance.ShowInputLabel(ButtonInputLabelId.ToPartyOrder);
        }

        protected override void OnViewReady()
        {
            base.OnViewReady();

            PlayerMenuManager playerMenuManager = GameUIManager.instance.gameMainMenu;

            if (playerMenuManager != null)
            {
                playerMenuManager.OnCancelPressedEvent += NavigateToMainButtons;
                playerMenuManager.OnInfoButtonPressedEvent += NavigateToRosterSelectionPanel;

                if (activeSubjectSelectionPanel != null)
                {
                    playerMenuManager.OnRightTabPressedEvent += activeSubjectSelectionPanel.NextSubject;
                    playerMenuManager.OnLeftTabPressedEvent += activeSubjectSelectionPanel.PreviousSubject;
                }
            }
        }

        public override void Close()
        {
            base.Close();

            rosterSelectionPanelController.Close();
            activeSubjectSelectionPanel.Close();

            PlayerMenuManager playerMenuManager = GameUIManager.instance.gameMainMenu;

            if (playerMenuManager != null)
            {
                playerMenuManager.OnCancelPressedEvent -= NavigateToMainButtons;
                playerMenuManager.OnInfoButtonPressedEvent -= NavigateToRosterSelectionPanel;

                if (activeSubjectSelectionPanel != null)
                {
                    playerMenuManager.OnRightTabPressedEvent -= activeSubjectSelectionPanel.NextSubject;
                    playerMenuManager.OnLeftTabPressedEvent -= activeSubjectSelectionPanel.PreviousSubject;
                }

                playerMenuManager.Close();
            }
        }

        public void NavigateToRosterSelectionPanel()
        {
            if (isNavigatingRoster)
            {
                rosterSelectionPanelController.MoveMember();
                return;
            }

            UINavigator.instance.SetDefaultSelectedInRuntime(rosterSelectionPanelController.firstActiveMember);

            isNavigatingRoster = true;

            string description = LocalizationManager.Localize(ChangePartyOrderDescriptionID);

            BottomPanelController.instance.SetDescription(description);
        }

        public void NavigateToMainButtons()
        {
            if (!isNavigatingRoster)
            {
                UINavigator.instance.OpenPreviousWindow();
                return;
            }

            if (rosterSelectionPanelController.isSwitchingMember)
            {
                rosterSelectionPanelController.CancelSwitching();

                return;
            }

            UINavigator.instance.SetDefaultSelectedInRuntime(mainButtons[0]);

            isNavigatingRoster = false;

            BottomPanelController.instance.ShowInputLabel(ButtonInputLabelId.ToPartyOrder);
        }
    }
}

