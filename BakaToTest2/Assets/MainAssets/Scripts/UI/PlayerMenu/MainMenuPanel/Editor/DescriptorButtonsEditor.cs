﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.UI;

namespace Intelligensia.UI.PlayerMenu
{
    [CustomEditor(typeof(DescriptorButtons))]
    public class DescriptorButtonsEditor : ButtonEditor
    {
        private SerializedProperty descriptorProp;


        protected override void OnEnable()
        {
            base.OnEnable();

            descriptorProp = serializedObject.FindProperty("descriptionID");
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            EditorGUILayout.PropertyField(descriptorProp);

            serializedObject.ApplyModifiedProperties();
        }
    }
}


