﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace Intelligensia.UI.PlayerMenu
{
    public class BottomPanelController : MonoBehaviour
    {
        public static BottomPanelController instance;

        [SerializeField] private TextMeshProUGUI currentPanelNameText = default;
        [SerializeField] private TextMeshProUGUI descriptionText = default;
        [SerializeField] private TextMeshProUGUI moneyText = default;
        [SerializeField] private TextMeshProUGUI timePlayedText = default;

        [SerializeField] private ButtonInputLabel[] buttonLabels = default;

        private Dictionary<ButtonInputLabelId, ButtonInputLabel> buttonLabelLookup;
        private ButtonInputLabel currentButtonInputLabel;

        private const string TimePlayedFormat = "{0} : {1} : {2}";


        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }
            else
                return;

            buttonLabelLookup = new Dictionary<ButtonInputLabelId, ButtonInputLabel>();

            for (int i = 0; i < buttonLabels.Length; i++)
            {
                buttonLabelLookup.Add(buttonLabels[i].inputLabelId, buttonLabels[i]);
            }
        }

        private void OnEnable()
        {
            if (GameDataManager.gameData != null)
            {
                SetMoney(GameDataManager.gameData.currency);
            }
        }

        public void SetDescription(string description)
        {
            //string description = LocalizationManager.Localize(descriptionID);

            if (descriptionText != null)
            {
                descriptionText.text = description;
            }
        }

        public void SetMoney(int moneyAmount)
        {
            if (moneyText != null)
            {
                moneyText.text = moneyAmount.ToString("N0");
            }
        }

        public void SetTimePlayed(string time)
        {
            if (timePlayedText != null)
            {
                timePlayedText.text = time;
            }
        }

        public void SetCurrentPanelName(string panelName)
        {
            if (currentPanelNameText != null)
            {
                currentPanelNameText.text = panelName;
            }
        }

        private ButtonInputLabel GetButtonInputLabel(ButtonInputLabelId id)
        {
            buttonLabelLookup.TryGetValue(id, out ButtonInputLabel inputLabel);

            return inputLabel;
        }

        public void ShowInputLabel(ButtonInputLabelId id)
        {
            if (currentButtonInputLabel != null)
            {
                if (currentButtonInputLabel.inputLabelId == id)
                    return;

                currentButtonInputLabel.gameObject.SetActive(false);
            }

            currentButtonInputLabel = GetButtonInputLabel(id);

            if (currentButtonInputLabel != null)
                currentButtonInputLabel.gameObject.SetActive(true);
        }
    }
}




