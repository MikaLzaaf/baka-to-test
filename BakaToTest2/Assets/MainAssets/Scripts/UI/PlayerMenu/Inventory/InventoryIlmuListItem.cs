﻿using UnityEngine.UI;
using TMPro;
using UnityEngine;

namespace Intelligensia.UI.PlayerMenu
{
    public class InventoryIlmuListItem : MonoBehaviour
    {
        public Image materialIcon;
        public TextMeshProUGUI materialAmountText;


        public void Initialize(Item material)
        {
            if (material.itemValue is IlmuItemBaseValue)
            {
                IlmuItemBaseValue xpMaterial = (IlmuItemBaseValue)material.itemValue;

                if (xpMaterial == null)
                    return;

                materialIcon.sprite = xpMaterial.GetIcon();

                materialAmountText.text = material.amount.ToString("N0");
            }
            else
            {
                gameObject.SetActive(false);
            }
        }


        public void SetAmountText(int amount)
        {
            if (materialAmountText != null)
            {
                materialAmountText.text = amount.ToString("N0");
            }
        }
    }
}

