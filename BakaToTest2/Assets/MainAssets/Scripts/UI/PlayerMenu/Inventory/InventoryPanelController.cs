﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;

namespace Intelligensia.UI.PlayerMenu
{
    public class InventoryPanelController : UIViewController
    {
        [SerializeField] private ItemCategorySelectionPanelController itemCategorySelectionPanel = default;
        [SerializeField] private InventoryIlmuListItem ilmuListItemPrefab = default;
        [SerializeField] private Transform ilmuListContent = default;

        public InventoryListItem listItemPrefabGeneral;

        private List<InventoryIlmuListItem> ilmuListItems;
        private List<InventoryListItem> listItems;
        private InventoryListItem selectedListItem;

        public bool isShowingItemOptions { get; set; }


        protected override void Awake()
        {
            base.Awake();

            if (itemCategorySelectionPanel != null)
            {
                itemCategorySelectionPanel.ItemCategoryChangedEvent += GenerateItemList;
            }
        }

        private void OnDestroy()
        {
            if (itemCategorySelectionPanel != null)
            {
                itemCategorySelectionPanel.ItemCategoryChangedEvent -= GenerateItemList;
            }
        }


        public override void Show()
        {
            base.Show();

            itemCategorySelectionPanel?.Show();

            GenerateIlmuList();

            BottomPanelController.instance.SetCurrentPanelName("Inventory");
            BottomPanelController.instance.ShowInputLabel(ButtonInputLabelId.SortItems);
        }

        protected override void OnViewReady()
        {
            base.OnViewReady();

            PlayerMenuManager playerMenuManager = GameUIManager.instance.gameMainMenu;

            if (playerMenuManager != null)
            {
                playerMenuManager.OnCancelPressedEvent += OnCancel;
                playerMenuManager.OnInfoButtonPressedEvent += SortList;

                if (itemCategorySelectionPanel != null)
                {
                    playerMenuManager.OnRightTabPressedEvent += itemCategorySelectionPanel.NextCategory;
                    playerMenuManager.OnLeftTabPressedEvent += itemCategorySelectionPanel.PreviousCategory;
                }
            }
        }

        public override void Close()
        {
            base.Close();

            itemCategorySelectionPanel?.Close();

            PlayerMenuManager playerMenuManager = GameUIManager.instance.gameMainMenu;

            if (playerMenuManager != null)
            {
                playerMenuManager.OnCancelPressedEvent -= OnCancel;
                playerMenuManager.OnInfoButtonPressedEvent -= SortList;

                if (itemCategorySelectionPanel != null)
                {
                    playerMenuManager.OnRightTabPressedEvent -= itemCategorySelectionPanel.NextCategory;
                    playerMenuManager.OnLeftTabPressedEvent -= itemCategorySelectionPanel.PreviousCategory;
                }
            }
        }


        private void GenerateIlmuList()
        {
            if (ilmuListItems != null && ilmuListItems.Count > 0)
            {
                if (ilmuListItems.Count > 0)
                {
                    for (int i = ilmuListItems.Count - 1; i >= 0; i--)
                    {
                        Destroy(ilmuListItems[i].gameObject);
                    }
                }

                ilmuListItems.Clear();
            }
            else
            {
                ilmuListItems = new List<InventoryIlmuListItem>();
            }

            Item[] currentItemList = PlayerInventoryManager.instance.GetItemsByCategory(ItemCategory.Ilmu);

            for (int i = 0; i < currentItemList.Length; i++)
            {
                InventoryIlmuListItem listItem = Instantiate(ilmuListItemPrefab);
                ilmuListItems.Add(listItem);

                listItem.transform.SetParent(ilmuListContent, false);

                listItem.Initialize(currentItemList[i]);
            }
        }

        public void GenerateItemList(ItemCategory itemCategory)
        {
            Item[] currentItemList = PlayerInventoryManager.instance.GetItemsByCategory(itemCategory);

            GenerateItemList(currentItemList);
        }

        public void GenerateItemList(Item[] itemList)
        {
            if (listItems != null && listItems.Count > 0)
            {
                ClearList();
            }
            else
            {
                listItems = new List<InventoryListItem>();
            }

            Item[] currentItemList = itemList;

            for (int i = 0; i < currentItemList.Length; i++)
            {
                InventoryListItem listItem = Instantiate(listItemPrefabGeneral);
                listItems.Add(listItem);

                listItem.transform.SetParent(content, false);

                listItem.Initialize(currentItemList[i], true, this);

                listItem.onClick.AddListener(delegate
                {
                    OnListItemClick(listItem);
                });
            }

            if (listItems.Count > 0)
            {
                UINavigator.instance.SetupVerticalNavigation(listItems, true);
                UINavigator.instance.SetDefaultSelectedInRuntime(listItems[0].gameObject);
            }
            else
            {
                BottomPanelController.instance.SetDescription("");
            }
        }

        private void ClearList()
        {
            if (listItems.Count > 0)
            {
                for (int i = listItems.Count - 1; i >= 0; i--)
                {
                    Destroy(listItems[i].gameObject);
                }
            }

            listItems.Clear();
        }

        private void OnListItemClick(InventoryListItem listItem)
        {
            listItem.ShowMiniInfo();
            selectedListItem = listItem;
            isShowingItemOptions = true;
        }

        public void UpdateListItem(InventoryListItem listItem)
        {
            listItems.Remove(listItem);

            selectedListItem = null;
            isShowingItemOptions = false;

            if (listItems.Count > 0)
            {
                UINavigator.instance.SetDefaultSelectedInRuntime(listItems[0].gameObject);
            }
            else
            {
                BottomPanelController.instance.SetDescription("");
            }
        }

        public void OnCancel()
        {
            if (isShowingItemOptions)
            {
                selectedListItem.Cancel();
                isShowingItemOptions = false;
            }
            else
            {
                int panelIndex = GameUIManager.instance.gameMainMenu.panelIndex;

                UINavigator.instance.OpenPreviousWindow(panelIndex);
            }
        }

        public void SortList()
        {
            var sortedList = listItems.OrderBy(x => x.item.ItemID).ToList();

            List<Item> items = new List<Item>();

            for(int i = 0; i < sortedList.Count; i++)
            {
                items.Add(sortedList[i].item);
            }

            GenerateItemList(items.ToArray());
        }
    }
}

