﻿using UnityEngine;
using System;

namespace Intelligensia.UI.PlayerMenu
{
    public class ItemCategorySelectionPanelController : UIViewController
    {

        public event Action<ItemCategory> ItemCategoryChangedEvent;

        [SerializeField] private ItemCategorySelectionListItem[] listItems = default;

        private int selectedCategoryIndex = 0;


        public override void Show()
        {
            base.Show();

            selectedCategoryIndex = 0;

            SwitchCategoryOnClicked(0);
        }


        public void SwitchCategory(int value)
        {
            if (listItems.Length <= 1)
                return;

            int nextIndex = (selectedCategoryIndex + value) % listItems.Length;

            if (nextIndex < 0)
            {
                nextIndex = listItems.Length - 1;
            }

            listItems[selectedCategoryIndex].ToggleHighlight(false);
            listItems[nextIndex].ToggleHighlight(true);

            selectedCategoryIndex = nextIndex;

            ItemCategoryChangedEvent?.Invoke(listItems[selectedCategoryIndex].category);
        }

        public void NextCategory()
        {
            SwitchCategory(1);
        }

        public void PreviousCategory()
        {
            SwitchCategory(-1);
        }

        public void SwitchCategoryOnClicked(int index)
        {
            if (selectedCategoryIndex != index)
            {
                listItems[selectedCategoryIndex].ToggleHighlight(false);
            }

            selectedCategoryIndex = index;

            listItems[selectedCategoryIndex].ToggleHighlight(true);

            ItemCategoryChangedEvent?.Invoke(listItems[selectedCategoryIndex].category);
        }
    }
}
    
