﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using TMPro;

namespace Intelligensia.UI.PlayerMenu
{
    public class InventoryListItem : HighlightingButton, ISelectHandler, ISubmitHandler
    {
        //public event Action<string> UpdateDescriptionEvent;

        [SerializeField] private UIViewController itemOptions = default;
        //[SerializeField] private ImageColorLerper iconHighlighter = default;
        [SerializeField] private Image itemIcon = default;
        [SerializeField] private TextMeshProUGUI itemNameText = default;
        [SerializeField] private TextMeshProUGUI itemAmountText = default;
        [SerializeField] private CanvasGroup canvasGroup = default;

        public Item item { get; set; }

        private InventoryPanelController inventoryController;
        private bool itemIsUsed = false;
        //private bool isSelected = false;


        public void Initialize(Item item, bool canInteract = false, InventoryPanelController inventoryPanel = null)
        {
            this.item = item;

            itemNameText.text = item.itemValue.ItemName;
            itemAmountText.text = item.amount > 0 ? item.amount.ToString() : "";

            itemIcon.sprite = IconLoader.GetItemIcon(item.itemValue.ItemIcon);

            interactable = canInteract;
            SetCanvasGroupAlpha();

            inventoryController = inventoryPanel;

            item.ItemUsedEvent -= UpdateAmountText;
            item.ItemUsedEvent += UpdateAmountText;
        }


        private void UpdateAmountText(Item item)
        {
            if (item.amount <= 0)
            {
                RemoveItem();
                return;
            }

            this.item = item;
            itemNameText.text = item.itemValue.ItemName;
            itemAmountText.text = item.amount.ToString();
        }


        public override void OnSelect(BaseEventData eventData)
        {
            base.OnSelect(eventData);

            //if (interactable)
            //{
            //    UpdateDescriptionEvent?.Invoke(item.ItemID);
            //}

            if(item != null)
            {
                string description = item.GetDescription();

                BottomPanelController.instance.SetDescription(description);
            }
        }

        //public override void OnDeselect(BaseEventData eventData)
        //{
        //    if (isSelected)
        //        return;

        //    base.OnDeselect(eventData);
        //}


        public void ShowMiniInfo()
        {
            SelectButton();

            itemOptions.Show();
            UINavigator.instance.SetDefaultSelectedInRuntime(itemOptions.transform);
        }


        public void Use()
        {
            Debug.Log("USE");

            // Open player selection menu
            GameUIManager.instance.gameMainMenu.SelectPlayer(item.itemValue.ItemID);

            itemIsUsed = true;

            Cancel();
        }

        public void OpenItemInfo()
        {
            Debug.Log("INFO");
        }

        public void DropItem()
        {
            Debug.Log("DROP");
            RemoveItem(true);

            //Cancel();
        }


        public void Cancel()
        {
            Debug.Log("CANCEL");
            DeselectButton();

            itemOptions.Close();

            if (!itemIsUsed)
            {
                UINavigator.instance.SetDefaultSelectedInRuntime(this.gameObject);
            }

            itemIsUsed = false;
        }


        private void RemoveItem(bool removeFromData = false)
        {
            item.ItemUsedEvent -= UpdateAmountText;

            inventoryController.UpdateListItem(this);

            if (removeFromData)
            {
                PlayerInventoryManager.instance.RemoveItem(item);
            }

            Destroy(gameObject);
        }

        public override void ToggleHighlight(bool active)
        {
            if (isSelected)
                return;

            base.ToggleHighlight(active);
        }

        private void SetCanvasGroupAlpha()
        {
            if (canvasGroup != null)
            {
                canvasGroup.alpha = interactable ? 1f : 0.5f;
            }
        }
    }
}

