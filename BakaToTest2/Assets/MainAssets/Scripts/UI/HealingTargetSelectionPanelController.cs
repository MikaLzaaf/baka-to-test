﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class HealingTargetSelectionPanelController : UIViewController
{
    public HealingTargetSelectionPanelItem itemPrefab;

    public event Action<ControllableEntity> PlayerSelectedEvent;

    private List<HealingTargetSelectionPanelItem> partyList = new List<HealingTargetSelectionPanelItem>();


    public override void Show()
    {
        base.Show();

        GeneratePartyList();
    }


    public void GeneratePartyList()
    {
        if (partyList != null && partyList.Count > 0)
        {
            ClearList();
        }
        else
        {
            partyList = new List<HealingTargetSelectionPanelItem>();
        }

        //List<PlayerEntity> players = PlayerPartyManager.instance.partyMembers;
        List<ControllableEntity> players = PlayerPartyManager.instance.partyMembers;

        //PartyStatusInfoManager.GetAllPlayers();
        //List<PlayerEntity> players = GameDataManager.gameData.playerEntities;

        for (int i = 0; i < players.Count; i++)
        {
            HealingTargetSelectionPanelItem listItem = Instantiate(itemPrefab);
            partyList.Add(listItem);

            listItem.transform.SetParent(content, false);

            listItem.Initialize(players[i]);

            listItem.onClick.AddListener(delegate
            {
                OnListItemClick(listItem);
            });
        }

        UINavigator.instance.SetupVerticalNavigation(partyList, true);
    }


    private void ClearList()
    {
        for (int i = 0; i < partyList.Count; i++)
        {
            Destroy(partyList[i].gameObject);
        }

        partyList.Clear();
    }


    private void OnListItemClick(HealingTargetSelectionPanelItem listItem)
    {
        PlayerSelectedEvent?.Invoke(listItem.playerEntity);
    }

}
