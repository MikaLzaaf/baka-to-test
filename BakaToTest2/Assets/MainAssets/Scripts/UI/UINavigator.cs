﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Intelligensia.Battle;

public class UINavigator : Singleton<UINavigator>
{
    public Action OnWindowChange;

    //public bool inBattle { get; set; }
    public bool showingBattleWindow;
    public bool canOpenPreviousWindows = true;


    [SerializeField]
    private List<UIViewController> previousWindows = new List<UIViewController>();

    private UIViewController currentWindow = default;
    public UIViewController CurrentWindow => currentWindow;

    private UIViewController closingWindow = default;


    private const int ScrollListItemJumpAmount = 5;

    public bool isWindowOpening { get; private set; }

    private static EventSystem eventSystem
    {
        get { return EventSystem.current; }
        set { EventSystem.current = value; }
    }

    public bool canCheckInteraction
    {
        get
        {
            return !BattleController.IsInBattle && currentWindow == null;
        }
    }

    private bool canControlInput
    {
        get
        {
            return currentWindow != null && (previousWindows.Count > 1 || !BattleController.IsInBattle);
        }
    }

    public bool isMapInputAvailable
    {
        get
        {
            return currentWindow == null && !BattleController.IsInBattle;
        }
    }

    public bool inBattleIdle
    {
        get
        {
            return currentWindow is BattleUIPanel_ActionSelection;
        }
    }

    public bool hasPreviousWindows
    {
        get
        {
            return previousWindows.Count > 0;
        }
    }

    private bool isUsingKeyboard;
    private GameObject lastSelectedGameObject;


    private void Update()
    {
        if(isUsingKeyboard)
        {
            if (UnityEngine.InputSystem.Mouse.current.delta.ReadValue().sqrMagnitude > 0f)
            {
                isUsingKeyboard = false;

                lastSelectedGameObject = eventSystem.currentSelectedGameObject;
                eventSystem.SetSelectedGameObject(null);
            }
        }
        else
        {
            if (UnityEngine.InputSystem.Keyboard.current.anyKey.isPressed)
            {
                isUsingKeyboard = true;

                if (lastSelectedGameObject != null)
                    eventSystem.SetSelectedGameObject(lastSelectedGameObject);
            }
        }

       

        //if (!canControlInput)
        //{
        //    return;
        //}
    }

    public void OpenNewWindow(UIViewController window, Transform contentTransform = null, int indexToOpen = 0, bool closeCurrentWindow = true)
    {
        if (isWindowOpening)
            return;

        if (BattleController.IsInBattle)
        {
            showingBattleWindow = true;
        }

        if (currentWindow != null)
        {
            previousWindows.Add(currentWindow);

            if(closeCurrentWindow)
            {
                currentWindow.Close();
            }
        }

        currentWindow = window;

        currentWindow.Show();

        StartCoroutine(WaitForWindowReady(contentTransform, indexToOpen));
    }


    public void OpenPreviousWindow(int indexToOpen = 0)
    {
        if (!canOpenPreviousWindows)
            return;

        if (currentWindow == null || isWindowOpening)
            return;

        closingWindow = currentWindow;
        closingWindow.Close();

        StartCoroutine(WaitForWindowClosed());

        if (previousWindows.Count > 0)
        {
            currentWindow = previousWindows[previousWindows.Count - 1];

            currentWindow.Show();

            StartCoroutine(WaitForWindowReady(currentWindow.content, indexToOpen));

            previousWindows.Remove(currentWindow);
        }
        else
        {
            currentWindow = null;

            if(!BattleController.IsInBattle)
            {
                PlayerPartyManager.playerCannotMove = false;

                MainCameraController.instance.ToggleCameraBrain(true);

                //GameUIManager.instance.ToggleBackground(false);
            }
        }
    }


    private IEnumerator WaitForWindowReady(Transform contentWindow = null, int indexToOpen = 0)
    {
        isWindowOpening = true;

        if (currentWindow == null)
        {
            yield break;
        }

        eventSystem.SetSelectedGameObject(null);

        yield return StartCoroutine(currentWindow.WaitForReady());

        Transform windowTransform = contentWindow == null ? currentWindow.transform : contentWindow;
       
        SetDefaultSelectedInRuntime(windowTransform, indexToOpen);

        isWindowOpening = false;
    }

    private IEnumerator WaitForWindowClosed()
    {
        //isWindowClosing = true;

        if (closingWindow == null)
        {
            yield break;
        }

        eventSystem.SetSelectedGameObject(null);

        yield return StartCoroutine(closingWindow.WaitForClosed());

        if (BattleController.IsInBattle && previousWindows.Count == 0 && currentWindow == null)
        {
            showingBattleWindow = false;
        }

        //isWindowClosing = false;
    }

    public void SetDefaultSelectedInRuntime()
    {
        if (currentWindow == null)
            return;

        if (currentWindow.content == null)
            return;

        GameObject target = currentWindow.content.GetChild(0).gameObject;

        SetDefaultSelectedInRuntime(target);
    }
    public void SetDefaultSelectedInRuntime(GameObject newDefaultSelected)
    {
        StartCoroutine(SetSelectedGameObject(newDefaultSelected));
    }

    public void SetDefaultSelectedInRuntime(Transform parent, int indexToOpen = 0)
    {
        if(parent.childCount == 0)
        {
            return;
        }

        GameObject defaultChild = parent.GetChild(indexToOpen).gameObject;

        SetDefaultSelectedInRuntime(defaultChild);
    }

    private IEnumerator SetSelectedGameObject(GameObject selectedObject)
    {
        eventSystem.SetSelectedGameObject(null);

        yield return new WaitForEndOfFrame();

        eventSystem.SetSelectedGameObject(selectedObject);
    }


    public void ClearPreviousWindows()
    {
        previousWindows.Clear();
    }

    public void ClearWindowsList()
    {
        previousWindows.Clear();
        currentWindow = null;
    }


    public List<T> SetupNavigation<T>(List<T> listItems)where T : Button
    {
        for (int i = 0; i < listItems.Count; i++)
        {
            Navigation navi = listItems[i].navigation;

            if (i == 0)
            {
                navi.selectOnUp = listItems[listItems.Count - 1];
                navi.selectOnLeft = listItems[listItems.Count - 1];
            }
            else
            {
                navi.selectOnUp = listItems[i - 1];
                navi.selectOnLeft = listItems[i - 1];
            }

            if (i == listItems.Count - 1)
            {
                navi.selectOnDown = listItems[0];
                navi.selectOnRight = listItems[0];
            }
            else
            {
                navi.selectOnDown = listItems[i + 1];
                navi.selectOnRight = listItems[i + 1];
            }

            listItems[i].navigation = navi;
        }

        return listItems;
    }

    public List<T> SetupHorizontalNavigation<T>(List<T> listItems, bool loop) where T : Button
    {
        for (int i = 0; i < listItems.Count; i++)
        {
            Navigation navi = listItems[i].navigation;

            if (i == 0)
            {
                navi.selectOnUp = null;
                navi.selectOnLeft = loop ? listItems[listItems.Count - 1] : null;
            }
            else
            {
                navi.selectOnUp = null;
                navi.selectOnLeft = listItems[i - 1];
            }

            if (i == listItems.Count - 1)
            {
                navi.selectOnDown = null;
                navi.selectOnRight = loop ? listItems[0] : null;
            }
            else
            {
                navi.selectOnDown = null;
                navi.selectOnRight = listItems[i + 1];
            }

            listItems[i].navigation = navi;
        }

        return listItems;
    }

    public List<T> SetupVerticalNavigation<T>(List<T> listItems, bool loop, bool canJump = true) where T : Button
    {
        for (int i = 0; i < listItems.Count; i++)
        {
            Navigation navi = listItems[i].navigation;

            if (i == 0)
            {
                navi.selectOnUp = loop ? listItems[listItems.Count - 1] : null;
                navi.selectOnLeft = null;
            }
            else
            {
                navi.selectOnUp = listItems[i - 1];

                if (canJump)
                    navi.selectOnLeft = i - ScrollListItemJumpAmount > 0 ? listItems[i - ScrollListItemJumpAmount] : listItems[0];
                else
                    navi.selectOnLeft = null;
            }

            if (i == listItems.Count - 1)
            {
                navi.selectOnDown = loop ? listItems[0] : null;
                navi.selectOnRight = null;
            }
            else
            {
                navi.selectOnDown = listItems[i + 1];

                if (canJump)
                    navi.selectOnRight = i + ScrollListItemJumpAmount < listItems.Count - 1 ? listItems[i + ScrollListItemJumpAmount] : listItems[listItems.Count - 1];
                else
                    navi.selectOnRight = null;
                
            }

            listItems[i].navigation = navi;
        }

        return listItems;
    }

    public List<T> SetupHorizontalNavigationWithoutReset<T>(List<T> listItems, bool loop) where T : Button
    {
        for (int i = 0; i < listItems.Count; i++)
        {
            Navigation navi = listItems[i].navigation;

            if (i == 0)
            {
                navi.selectOnLeft = loop ? listItems[listItems.Count - 1] : null;
            }
            else
            {
                navi.selectOnLeft = listItems[i - 1];
            }

            if (i == listItems.Count - 1)
            {
                navi.selectOnRight = loop ? listItems[0] : null;
            }
            else
            {
                navi.selectOnRight = listItems[i + 1];
            }

            listItems[i].navigation = navi;
        }

        return listItems;
    }

    public List<T> SetupVerticalNavigationWithoutReset<T>(List<T> listItems, bool loop) where T : Button
    {
        for (int i = 0; i < listItems.Count; i++)
        {
            Navigation navi = listItems[i].navigation;

            if (i == 0)
            {
                navi.selectOnUp = loop ? listItems[listItems.Count - 1] : null;
            }
            else
            {
                navi.selectOnUp = listItems[i - 1];
            }

            if (i == listItems.Count - 1)
            {
                navi.selectOnDown = loop ? listItems[0] : null;
            }
            else
            {
                navi.selectOnDown = listItems[i + 1];
            }

            listItems[i].navigation = navi;
        }

        return listItems;
    }
}
