﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public class UIArrowPointer : MonoBehaviour
{
    public enum ScreenBorder
    {
        Undefined,
        Top,
        Bottom,
        Left,
        Right
    }

    public Action<ScreenBorder> TargetIsNearingScreenBorder;

    public Transform target;
    public RectTransform pointer;
    public RectTransform holder;

    [SerializeField] Vector3 offset = default;
    [SerializeField] float bufferRange = default;
    [SerializeField] float textBufferRange = default;


    public Vector3 targetPositionOffset;

    public bool hidePointerWhenReachTarget = true;
    public bool hidePointerWhenTargetOffscreen = false;

    private Camera originCamera;
    private ScreenBorder lastScreenBorderNear = ScreenBorder.Undefined;

    private Vector3 targetPositionScreenPoint
    {
        get { return originCamera.WorldToScreenPoint(target.position + targetPositionOffset); }
    }


    void Start()
    {
        originCamera = Camera.main;
    }

    void Update()
    {
        if (target == null || pointer == null)
            return;
        // Get the position of the object in screen space
        Vector3 objScreenPos = targetPositionScreenPoint;

        // Get the directional vector between your arrow and the object
        Vector3 dir = (objScreenPos - pointer.position).normalized;

        // Calculate the angle 
        // We assume the default arrow position at 0° is "up"
        float angle = Mathf.Rad2Deg * Mathf.Acos(Vector3.Dot(dir, Vector3.up));

        // Use the cross product to determine if the angle is clockwise
        // or anticlockwise
        Vector3 cross = Vector3.Cross(dir, Vector3.up);
        angle = -Mathf.Sign(cross.z) * angle;

        // Update the rotation of your arrow
        pointer.localEulerAngles = new Vector3(pointer.localEulerAngles.x, pointer.localEulerAngles.y, angle);

        CalculateUIPointerMoveToTargetPosition();
    }

    private void CalculateUIPointerMoveToTargetPosition()
    {
        IsArrowNearingHorizontalBorder();

        if (IsArrowOffScreen())
        {
            if(hidePointerWhenTargetOffscreen)
            {
                if(holder.gameObject.activeSelf)
                    holder.gameObject.SetActive(false);
            }
            else
            {
                Vector3 clampped = targetPositionScreenPoint;

                clampped.x = Mathf.Clamp(clampped.x, 0 + bufferRange, Screen.width - bufferRange);
                clampped.y = Mathf.Clamp(clampped.y, 0 + bufferRange, Screen.height - bufferRange);

                holder.position = clampped;
                holder.localPosition = new Vector3(holder.localPosition.x, holder.localPosition.y, 0.0f);

                holder.gameObject.SetActive(true);
            }
          
        }
        else
        {
            holder.position = targetPositionScreenPoint;
            holder.localPosition = new Vector3(holder.localPosition.x, holder.localPosition.y, 0.0f);
            holder.localPosition += offset;

            if (hidePointerWhenReachTarget) 
                holder.gameObject.SetActive(false);
            else
                holder.gameObject.SetActive(true);
        }
    }


    private bool IsArrowOffScreen()
    {
        bool isOffScreen =
            (targetPositionScreenPoint.x <= 0 + bufferRange)
            || (targetPositionScreenPoint.x >= Screen.width - bufferRange)
            || (targetPositionScreenPoint.y <= 0 + bufferRange)
            || (targetPositionScreenPoint.y >= Screen.height - bufferRange);

        return isOffScreen;
    }


    private void IsArrowNearingHorizontalBorder()
    {
        ScreenBorder screenBorderArrowNearing = lastScreenBorderNear;

        if (targetPositionScreenPoint.x <= 0 + textBufferRange)
        {
            screenBorderArrowNearing = ScreenBorder.Left;
        }
        else if (targetPositionScreenPoint.x >= Screen.width - textBufferRange)
        {
            screenBorderArrowNearing = ScreenBorder.Right;
        }
        else
        {
            screenBorderArrowNearing = ScreenBorder.Undefined;
        }

        if(lastScreenBorderNear != screenBorderArrowNearing)
        {
            TargetIsNearingScreenBorder?.Invoke(screenBorderArrowNearing);

            lastScreenBorderNear = screenBorderArrowNearing;
        }
    }
}
