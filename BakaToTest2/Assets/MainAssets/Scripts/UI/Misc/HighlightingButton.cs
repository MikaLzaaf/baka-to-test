﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace Intelligensia.UI
{
    public class HighlightingButton : Button, ISelectHandler
    {

        [Header("Highlighting Button")]
        [SerializeField] private bool stayHighlightedUponClicked = false;
        [SerializeField] private ImageColorLerper iconHighlighter = default;


        protected bool isSelected = false;


        protected override void OnDisable()
        {
            DeselectButton();

            base.OnDisable();
        }


        public override void OnSelect(BaseEventData eventData)
        {
            base.OnSelect(eventData);

            ToggleHighlight(true);
        }

        public override void OnDeselect(BaseEventData eventData)
        {
            base.OnDeselect(eventData);

            ToggleHighlight(false);
        }

        public virtual void SelectButton()
        {
            isSelected = true;
        }

        public virtual void DeselectButton()
        {
            isSelected = false;
            ToggleHighlight(false);
        }    

        public virtual void ToggleHighlight(bool active)
        {
            if(stayHighlightedUponClicked)
            {
                if (isSelected)
                    return;
            }

            if (iconHighlighter != null)
            {
                iconHighlighter.gameObject.SetActive(active);
            }
        }
    }
}

