﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class UITabController : MonoBehaviour
{
    public UITabItem[] tabItems;

    private int tabCount = 0;
    public int TabCount
    {
        get { return tabCount; }
    }

    private int currentTabIndex = 0;
    public int CurrentTabIndex => currentTabIndex;

    private UITabItem lastTabSelected = default;


    private void Awake()
    {
        if(tabItems != null)
        {
            tabCount = tabItems.Length;
        }
    }


    public void SwitchTab(int tabIndex)
    {
        if (tabCount == 0)
            return;

        if (lastTabSelected != null)
        {
            lastTabSelected.ToggleHighlightColor(false);
        }

        currentTabIndex += tabIndex;

        if(currentTabIndex >= tabCount)
        {
            currentTabIndex = 0;
        }
        else if(currentTabIndex < 0)
        {
            currentTabIndex = tabCount - 1;
        }

        tabItems[currentTabIndex].ToggleHighlightColor(true);

        lastTabSelected = tabItems[currentTabIndex];
    }



    public void SwitchToFirstTab()
    {
        currentTabIndex = 0;

        SwitchTab(0);
    }
}
