﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;


namespace Intelligensia.UI
{
    public class HighlightingTabButton : Button
    {
        [SerializeField] protected Transform iconHolder = default;
        [SerializeField] private ImageColorLerper iconHighlighter = default;

        [Header("Move variables")]
        [SerializeField] float moveDuration = 0.3f;
        [SerializeField] float moveVerticalAmount = 5f;
        [SerializeField] bool moveVertical = true;

        protected Vector3? originalPosition;

        protected override void OnEnable()
        {
            base.OnEnable();
            if (!originalPosition.HasValue)
                originalPosition = iconHolder.transform.localPosition;
        }

        protected override void OnDisable()
        {
            ToggleHighlight(false);

            base.OnDisable();
        }

        public void ToggleHighlight(bool active)
        {
            if (iconHighlighter != null)
            {
                iconHighlighter.gameObject.SetActive(active);
            }

            if(moveVertical)
                MoveUpOrDown(active);
        }

        private void MoveUpOrDown(bool up)
        {
            if (!originalPosition.HasValue)
                return;

            float destination = up ? originalPosition.Value.y + moveVerticalAmount : originalPosition.Value.y;

            iconHolder.transform.DOLocalMoveY(destination, moveDuration);
        }

    }
}

