﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FillIndicatorLerper : MonoBehaviour
{

    public Image previewFiller;
    public Image indicatorFiller;
    public GameObject[] overfilledIndicators;


    public float lerpDuration = 0.4f;
    public float waitDuration = 0.2f;

    public bool lerpUpwards = true;
    public bool waitBeforeLerp = true;

    [Header("Indicator Color")]
    public Gradient gradient;
    public bool changeIndicatorColorOnGradient;

    [Header("Testing")]
    public float testTarget;
    public float testValue;
    public bool test = false;

    private float elapsedTweenTime = 0f;


    private void Update()
    {
        if(test)
        {
            StartLerp(testTarget);

            test = false;
        }
    }

    public void StartLerp(float targetAmount, float startAmount = 0f)
    {
        if (previewFiller == null || indicatorFiller == null)
            return;

        elapsedTweenTime = 0f;

        float currentAmount = startAmount > 0f ? startAmount : indicatorFiller.fillAmount;

        ToggleOverfilledIndicators(currentAmount >= 1f);

        previewFiller.gameObject.SetActive(true);
        indicatorFiller.gameObject.SetActive(true);

        lerpUpwards = targetAmount >= currentAmount;

        if(gameObject.activeInHierarchy)
        {
            StartCoroutine(LerpIndicator(targetAmount, currentAmount));
        }
        else
        {
            previewFiller.fillAmount = targetAmount;
            indicatorFiller.fillAmount = targetAmount;

            ToggleOverfilledIndicators(targetAmount >= 1f);
        }
    }


    private IEnumerator LerpIndicator(float targetAmount, float currentAmount)
    {
        previewFiller.fillAmount = lerpUpwards ? targetAmount : currentAmount;
        indicatorFiller.fillAmount = lerpUpwards ? currentAmount : targetAmount;

        if(waitBeforeLerp)
        {
            yield return new WaitForSeconds(waitDuration);
        }    

        if(changeIndicatorColorOnGradient)
        {
            indicatorFiller.color = gradient.Evaluate(targetAmount);
        }

        while(elapsedTweenTime < lerpDuration)  
        {
            elapsedTweenTime += Time.deltaTime;

            if(lerpUpwards)
            {
                indicatorFiller.fillAmount = Mathf.Lerp(currentAmount, targetAmount, elapsedTweenTime / lerpDuration);
            }
            else
            {
                previewFiller.fillAmount = Mathf.Lerp(currentAmount, targetAmount, elapsedTweenTime / lerpDuration);
            }

            yield return null;
        }

        if(lerpUpwards)
        {
            indicatorFiller.fillAmount = targetAmount;
        }
        else
        {
            previewFiller.fillAmount = targetAmount;
        }

        ToggleOverfilledIndicators(targetAmount >= 1f);
    }


    public void ResetIndicator()
    {
        if (previewFiller == null || indicatorFiller == null)
            return;

        StopAllCoroutines();

        previewFiller.fillAmount = 0f;
        indicatorFiller.fillAmount = 0f;

        ToggleOverfilledIndicators(false);
    }


    private void ToggleOverfilledIndicators(bool active)
    {
        if (overfilledIndicators.Length == 0)
            return;

        for (int i = 0; i < overfilledIndicators.Length; i++)
        {
            overfilledIndicators[i].SetActive(active);
        }
    }
}
