﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImageColorLerper : MonoBehaviour
{

    public Image coloredImage;

    public Color startColor;
    public Color endColor;

    public float lerpSpeed = 2f;

    public bool loop = true;

    private void Awake()
    {
        if(coloredImage == null)
        {
            coloredImage = GetComponent<Image>();
        }

        if (coloredImage == null)
            return;

        coloredImage.color = startColor;
    }

    private void Update()
    {
        if (coloredImage == null)
            return;

        float elapsedTime = loop ? (Mathf.Sin(Time.time * lerpSpeed) + 1) / 2.0f :
            Time.time * lerpSpeed;

        coloredImage.color = Color.Lerp(startColor, endColor, elapsedTime);
    }
}
