﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HoldButtonIndicator : MonoBehaviour
{

    public Image fillImage;
    public GameObject holdLabel;

    //[SerializeField] private float holdDurationOffset = 0.1f;
    private float fillAmountFactor = 0f;
    private float durationToHold = 0f;
    private float elapsedTimeSinceStartHolding = 0f;

    private bool canFill = false;


    private void OnEnable()
    {
        CancelHolding();
    }

    private void Update()
    {
        if (!canFill)
            return;

        if (fillImage == null)
            return;

        elapsedTimeSinceStartHolding += Time.deltaTime;

        fillImage.fillAmount += fillAmountFactor * Time.deltaTime;

        if(elapsedTimeSinceStartHolding >= durationToHold)
        {
            SuccessHold();
        }
    }


    public void StartHolding(float durationToHold)
    {
        this.durationToHold = durationToHold;
        elapsedTimeSinceStartHolding = 0f;

        fillAmountFactor = 1f / durationToHold;

        ToggleLabel(false);

        ResetFillAmount();

        canFill = true;
    }


    public void CancelHolding()
    {
        ResetFillAmount();

        ToggleLabel(true);

        canFill = false;
    }


    private void ToggleLabel(bool active)
    {
        if(holdLabel != null)
        {
            holdLabel.SetActive(active);
        }
    }


    private void ResetFillAmount()
    {
        if (fillImage != null)
        {
            fillImage.fillAmount = 0f;
        }
    }

    private void SuccessHold()
    {
        ResetFillAmount();
        canFill = false;
    }
}
