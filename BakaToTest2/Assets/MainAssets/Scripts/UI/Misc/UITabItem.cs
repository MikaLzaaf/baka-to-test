﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UITabItem : MonoBehaviour
{
    public TextMeshProUGUI tabNameText;
    public Image highlightImage;
    public Color highlightedColor;


    private Color initialColor;


    protected void Awake()
    {
        if(highlightImage != null)
        {
            initialColor = highlightImage.color;
        }
    }


    public void ToggleHighlightColor(bool active)
    {
        if (highlightImage == null)
            return;

        highlightImage.color = active ? highlightedColor : initialColor;
    }
}
