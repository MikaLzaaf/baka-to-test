﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class IconLoader
{

    private const string SubjectIconInResources = "Images/SubjectIcons/";
    private const string WeatherIconInResources = "Images/WeatherIcons/";
    private const string BattleActionIconInResources = "Images/BattleActionIcons/";
    private const string StatusEffectIconInResources = "Images/StatusEffectIcons/";
    private const string ItemIconDirectoryInResources = "Images/ListItemIcons/";



    public static Sprite GetSubjectIcon(Subjects subject)
    {
        string path = SubjectIconInResources + subject.ToString();

        Sprite icon = Resources.Load<Sprite>(path);

        if (icon == null)
        {
            Debug.LogError("Failed to load icon at Resources/" + path);
        }

        return icon;
    }


    public static Sprite GetWeatherIcon(string weather)
    {
        string path = WeatherIconInResources + weather;

        Sprite icon = Resources.Load<Sprite>(path);

        if (icon == null)
        {
            Debug.LogError("Failed to load icon at Resources/" + path);
        }

        return icon;
    }

    public static Sprite GetBattleActionIcon(string action)
    {
        string path = BattleActionIconInResources + action;

        Sprite icon = Resources.Load<Sprite>(path);

        if (icon == null)
        {
            Debug.LogError("Failed to load icon at Resources/" + path);
        }

        return icon;
    }

    public static Sprite GetStatusEffectIcon(string statusEffect)
    {
        string path = StatusEffectIconInResources + statusEffect;

        Sprite icon = Resources.Load<Sprite>(path);

        if (icon == null)
        {
            Debug.LogError("Failed to load icon at Resources/" + path);
        }

        return icon;
    }

    public static Sprite GetItemIcon(string itemIconID)
    {
        string path = ItemIconDirectoryInResources + itemIconID;

        Sprite icon = Resources.Load<Sprite>(path);

        if (icon == null)
        {
            Debug.LogError("Failed to load icon at Resources/" + path);
        }

        return icon;
    }
}
