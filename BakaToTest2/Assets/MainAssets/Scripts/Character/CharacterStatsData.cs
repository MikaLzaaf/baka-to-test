﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

[Serializable]
public class CharacterStatsData
{
    /// <summary> This is used for storing any progress done by the player on this character. 
    /// Stuffs like currentStats, skills learned are stored here.</summary> 

    public string characterName;

    public BasicStats currentStats;
    public AcademicStats currentAcademicStats;
    //public List<SubjectStats> currentSubjectStats;
    public List<StatusEffectResistance> currentInnerStatusEffectResistances;

    public List<CharacterInfoData> characterInfoDatas;



    public CharacterStatsData()
    {
        
    }

    public CharacterStatsData(string characterId, CharacterStatsBaseValue characterStatsBase)
    {
        this.characterName = characterId;

        currentStats = characterStatsBase.baseStats;

        currentAcademicStats = characterStatsBase.baseAcademicStats;

        currentInnerStatusEffectResistances = characterStatsBase.baseInnerStatusEffectResistances;

        characterInfoDatas = new List<CharacterInfoData>();

        for(int i = 0; i < characterStatsBase.characterInfos.Count; i++)
        {
            CharacterInfoData characterInfoData = new CharacterInfoData(characterStatsBase.characterInfos[i]);
            characterInfoDatas.Add(characterInfoData);
        }
    }

    public void ResetSubjectsFatigue()
    {
        for (int i = 0; i < currentAcademicStats.subjectsStats.Count; i++)
        {
            currentAcademicStats.subjectsStats[i].ResetSubjectFatigue();
        }
    }

    public int GetLivesAmount()
    {
        int totalValue = 0;

        for (int i = 0; i < currentAcademicStats.subjectsStats.Count; i++)
        {
            totalValue += currentAcademicStats.subjectsStats[i].finalValue;
        }

        float lives = Mathf.Floor(totalValue / 100);

        return (int)lives;
    }

    public CharacterInfoData GetCharacterInfoData(string infoId)
    {
        for(int i = 0; i < characterInfoDatas.Count; i++)
        {
            if (characterInfoDatas[i].IsSame(infoId))
                return characterInfoDatas[i];
        }

        return null;
    }

    public string GetOverallGrade()
    {
        float totalScore = 0f;

        for (int i = 0; i < currentAcademicStats.subjectsStats.Count; i++)
        {
            totalScore += currentAcademicStats.subjectsStats[i].Score;
        }

        totalScore /= currentAcademicStats.subjectsStats.Count;

        return GradeChecker.GetGrade(Mathf.FloorToInt(totalScore));
    }

    public int GetActiveInfoEffectCount()
    {
        int count = 0;

        for(int i = 0; i < characterInfoDatas.Count; i++)
        {
            if (characterInfoDatas[i].isInEffect)
                count += 1;
        }

        return count;
    }
}
