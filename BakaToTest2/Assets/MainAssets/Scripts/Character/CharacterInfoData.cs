﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CharacterInfoData 
{
    public string infoId;
    public bool isLocked;
    public bool isInEffect;

    public CharacterInfoData()
    {

    }

    public CharacterInfoData(CharacterInfo characterInfo)
    {
        if(characterInfo != null)
        {
            infoId = characterInfo.infoId;
            isLocked = characterInfo.isLocked;
        }
    }

    public bool IsSame(string infoId)
    {
        return this.infoId == infoId;
    }

    public void Unlock()
    {
        isLocked = false;
    }
}
