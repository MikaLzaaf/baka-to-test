﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Intelligensia.Battle;

[System.Serializable]
public class BattlePartyMember 
{
    [StringInList(typeof(PropertyDrawersHelper), "AllClassNames")] public string className;
    [StringInList(typeof(PropertyDrawersHelper), "CharacterNamesByClass", "A")] public string studentNameA;
    [StringInList(typeof(PropertyDrawersHelper), "CharacterNamesByClass", "B")] public string studentNameB;
    [StringInList(typeof(PropertyDrawersHelper), "CharacterNamesByClass", "C")] public string studentNameC;
    [StringInList(typeof(PropertyDrawersHelper), "CharacterNamesByClass", "D")] public string studentNameD;
    [StringInList(typeof(PropertyDrawersHelper), "CharacterNamesByClass", "E")] public string studentNameE;
    [StringInList(typeof(PropertyDrawersHelper), "CharacterNamesByClass", "F")] public string studentNameF;

    public bool isLeader;


#if UNITY_EDITOR
    public bool _expanded = false;
#endif


    public BattlePartyMember() { }

    public BattlePartyMember(string className, string characterName, bool isLeader)
    {
        this.className = className;

        SetCharacterName(characterName);

        this.isLeader = isLeader;
    }

    public BattleEntity GetCharacterPrefab()
    {
        BattleEntity prefab = PlayerInfoManager.LoadBattleEntityPrefab(className, GetCharacterName());

        return prefab;
    }

    public string GetCharacterName()
    {
        if (className == "A")
            return studentNameA;
        else if (className == "B")
            return studentNameB;
        else if (className == "C")
            return studentNameC;
        else if (className == "D")
            return studentNameD;
        else if (className == "E")
            return studentNameE;
        else if (className == "F")
            return studentNameF;

        return "";
    }

    private void SetCharacterName(string characterName)
    {
        if (className == "A")
            studentNameA = characterName;
        else if (className == "B")
            studentNameB = characterName;
        else if (className == "C")
            studentNameC = characterName;
        else if (className == "D")
            studentNameD = characterName;
        else if (className == "E")
            studentNameE = characterName;
        else if (className == "F")
            studentNameF = characterName;
    }
}
