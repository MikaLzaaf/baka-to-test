﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public class CharacterSpawnerOnScene : MonoBehaviour
{
    public event Action<ControllableEntity> PlayerSpawnedEvent;

    public CharacterSpawnerSO characterSpawner;
    public CharacterId characterId;
    public ControllableEntity playerPrefab;
    public int order;

    public bool spawnOnAwake = true;
    public bool applySpawnPositionOffset = false;

    public bool overrideInitialPlayerCostume = false;
    public string initialPlayerCostumeName;


    private void Awake()
    {
        if (spawnOnAwake)
        {
            SpawnPlayer();
        }
    }


    public void SpawnPlayer()
    {
        ControllableEntity playerEntity = null;

        if (playerPrefab != null)
        {
            playerEntity = characterSpawner.SpawnControllablePlayer(
                playerPrefab,
                transform.position,
                transform.rotation,
                applySpawnPositionOffset,
                overrideInitialPlayerCostume,
                initialPlayerCostumeName);
        }
        else
        {
            playerEntity = characterSpawner.SpawnControllablePlayer(
                characterId,
                transform.position,
                transform.rotation,
                applySpawnPositionOffset,
                overrideInitialPlayerCostume,
                initialPlayerCostumeName);
        }

        this.gameObject.SetActive(false);

        PlayerSpawnedEvent?.Invoke(playerEntity);
    }


#if UNITY_EDITOR

    //private void OnDrawGizmos()
    //{
    //    Gizmos.DrawWireSphere(transform.position, 0.7f);

    //    string icon = null;

    //    if (!string.IsNullOrEmpty(customGizmosIcon))
    //    {
    //        icon = customGizmosIcon;
    //    }
    //    else
    //    {
    //        icon = string.Format("PlayerIcons/Icon{0}.png", characterId.ToString());
    //    }

    //    Gizmos.DrawIcon(transform.position, icon);
    //}

#endif
}
