﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(fileName = "New Character Info", menuName = "ScriptableObject/Character Info")]
public class CharacterInfo : ScriptableObject
{
    // What should be here?
    // - info about character (General or Battle related)
    // - unlock condition
    // - id

    // Need to apply InfoTag effects properly
    // e.g. If weakness is Female, what would happen?
    // e.g. If a character is weak in a subject, what would happen?

    // For now, the effects are split into 2 categories
    // - Causes : Days, Genders, Environments and SUbjects InfoTags are used 
    // - Effects : Stats, OnHit and Conflict InfoTags are used 

    // - For Effects InfoTags, the character will gain the characteristic described in the infoTag. Can be combined with other infoTags or just apply it solo to make it applied permanently.

    private const string LockedInfo = "???";
    private const string CausesWordFormat = "<color=#FED330>{0}</color>";

    public string infoId;
    public bool isLocked;

    [TextArea]
    public string infoDescriptionFormat;

    [SerializeField] private CausesTag causesTag = default;
    public CausesTag CausesTag => causesTag;

    [SerializeField] private EffectsTag effectsTag = default;
    public EffectsTag EffectsTag => effectsTag;



    public string GetLocalizedInfo()
    {
        if (isLocked)
            return LockedInfo;

        //string infoDescription = LocalizationManager.Localize(infoId);

        //if(string.IsNullOrEmpty(infoDescription))
        //{
        //    infoDescription = infoDescriptionFormat;
        //}

        string causeTagName = causesTag == null ? "Null" : Split(causesTag.name);

        string causeWord = string.Format(CausesWordFormat, causeTagName);

        string infoDescription = string.Format(infoDescriptionFormat, causeWord);

        return infoDescription;
    }

    private string Split(string input)
    {
        string result = "";

        for (int i = 0; i < input.Length; i++)
        {
            if (char.IsUpper(input[i]))
            {
                result += ' ';
            }

            result += input[i];
        }

        return result.Trim();
    }

    //public bool IsMatchingTags(EffectsTag[] tags)
    //{
    //    int infoTagCount = effectsTags.Count;

    //    for(int i = 0; i < tags.Length; i++)
    //    {
    //        if(effectsTags.Contains(tags[i]))
    //        {
    //            infoTagCount -= 1;
    //        }
    //    }

    //    return infoTagCount <= 0;
    //}



    // Flow of checking whether current battle conditions matched the Tags

    // - Causes : Days, Genders, Environments and Subjects (Active Subject only)

    // These are for StatsEffects tags. Can only applied once per character info
    // - Days & Environments are checked when battle first started
    // - Genders checked when battle first started and when swapping members occurred. Check genders of all opponents.
    // - Subjects checked when battle first started and when active subject changes

    // These are for ConflictEffects tags. 
    // - All Causes are checked before Conflict proceeds. Check gender of the opponent currently facing.

    public bool HasEffectsTag<T>()
    {
        return effectsTag is T;
    }

    public string GetEffectDescription()
    {
        string description = "";

        if (isLocked)
            description = LockedInfo;
        else if (effectsTag == null)
        {
            Debug.LogError("There is no Effects Tag assign on this " + infoId + " CharacterInfo.");
            description = LockedInfo;
        }
        else
        {
            string effectTagName = effectsTag.EffectName;

            string effectValue = "";

            if(effectsTag.IsMultiplicativeEffect)
                effectValue = " x " + effectsTag.EffectValue;
            else
                effectValue = effectsTag.EffectValue > 0 ? " +" + effectsTag.EffectValue : " " + effectsTag.EffectValue;
            
            description = effectTagName + effectValue;
        }

        return description;
    }
}


