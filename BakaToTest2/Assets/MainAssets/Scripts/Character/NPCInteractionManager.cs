﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PixelCrushers.DialogueSystem;

public class NPCInteractionManager : Singleton<NPCInteractionManager>
{

    private string currentInteractingNPC = "";

    private List<NPCInteractable> interactableNpcs = new List<NPCInteractable>();
    private Dictionary<string, NPCInteractable> npcLookup = new Dictionary<string, NPCInteractable>();


    private void OnEnable()
    {
        RegisterLuaFunction();
    }

    private void OnDisable()
    {
        UnregisterLuaFunction();
    }

    private NPCInteractable GetNPC(string npcName)
    {
        npcLookup.TryGetValue(npcName, out NPCInteractable npc);
        return npc;
    }    

    public void RegisterNPC(NPCInteractable npc)
    {
        if (GetNPC(npc.ObjName) != null)
            return;

        npcLookup.Add(npc.ObjName, npc);
        interactableNpcs.Add(npc);
    }

    public void UnregisterNPC(NPCInteractable npc)
    {
        if (GetNPC(npc.ObjName) == null)
            return;

        npcLookup.Remove(npc.ObjName);
        interactableNpcs.Remove(npc);
    }

    public void SetCurrentInteractingNPC(string npcName)
    {
        currentInteractingNPC = npcName;
    }


    private void RegisterLuaFunction()
    {
        Lua.RegisterFunction(nameof(AddChallenge), this, SymbolExtensions.GetMethodInfo(() => AddChallenge()));
        Lua.RegisterFunction(nameof(HasChallengeAdded), this, SymbolExtensions.GetMethodInfo(() => HasChallengeAdded()));
        Lua.RegisterFunction(nameof(ShowChallengeInfo), this, SymbolExtensions.GetMethodInfo(() => ShowChallengeInfo()));
        Lua.RegisterFunction(nameof(CloseChallengeInfo), this, SymbolExtensions.GetMethodInfo(() => CloseChallengeInfo()));
    }

    void UnregisterLuaFunction()
    {
        Lua.UnregisterFunction(nameof(AddChallenge));
        Lua.UnregisterFunction(nameof(HasChallengeAdded));
        Lua.UnregisterFunction(nameof(ShowChallengeInfo));
        Lua.UnregisterFunction(nameof(CloseChallengeInfo));
    }

    public void AddChallenge()
    {
        NPCInteractable currentNpc = GetNPC(currentInteractingNPC);

        if (currentNpc == null)
            return;

        DailyInfoManager.AddChallenge(currentNpc.challengeInfo);

        currentNpc.hasChallengeAdded = true;
    }

    public bool HasChallengeAdded()
    {
        NPCInteractable currentNpc = GetNPC(currentInteractingNPC);

        if (currentNpc == null)
            return false;

        return currentNpc.hasChallengeAdded;
    }

    public void ShowChallengeInfo()
    {
        NPCInteractable currentNpc = GetNPC(currentInteractingNPC);

        if (currentNpc == null)
            return;

        GameUIManager.instance.challengesMenuController.ShowChallengeInfo(currentNpc.challengeInfo);
    }

    public void CloseChallengeInfo()
    {
        GameUIManager.instance.challengesMenuController.CloseChallengeInfo();
    }
}
