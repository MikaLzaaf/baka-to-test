﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(BattlePartyMember))]
public class BattlePartyMemberPropertyDrawer : PropertyDrawer
{
    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        float h = base.GetPropertyHeight(property, label);
        int rows = 1;
        if (property.FindPropertyRelative("_expanded").boolValue)
        {
            rows += 1;

            string className = property.FindPropertyRelative("className").stringValue;

            if(string.IsNullOrEmpty(className) || className == "Undefined")
            {

            }
            else
            {
                rows += 1;

                // for is Leader boolean
                rows += 1;
            }
                
        }
        return h * rows;
    }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        // Draw label
        SerializedProperty p_expanded = property.FindPropertyRelative("_expanded");

        float h = base.GetPropertyHeight(property, label);
        position.height = h;

        p_expanded.boolValue = EditorGUI.Foldout(position, p_expanded.boolValue, label);

        if (p_expanded.boolValue)
        {
            position.y += h;
            position.height = GetPropertyHeight(property, label) - h;
            EditorGUI.BeginProperty(position, label, property);

            ++EditorGUI.indentLevel;

            // Calculate rects
            float x = position.x,
                y = position.y,
                w = position.width;

            Rect r_className = new Rect(x, y, w, h);

            SerializedProperty p_className = property.FindPropertyRelative("className");

            EditorGUI.PropertyField(r_className, p_className, new GUIContent(p_className.displayName));


            string className = p_className.stringValue;

            if (className != "Undefined")
            {
                y += h;
                Rect r_studentName = new Rect(x, y, w, h);

                SerializedProperty p_studentName = null;

                if (className == "A")
                {
                    p_studentName = property.FindPropertyRelative("studentNameA");
                }
                else if (className == "B")
                {
                    p_studentName = property.FindPropertyRelative("studentNameB");
                }
                else if (className == "C")
                {
                    p_studentName = property.FindPropertyRelative("studentNameC");
                }
                else if (className == "D")
                {
                    p_studentName = property.FindPropertyRelative("studentNameD");
                }
                else if (className == "E")
                {
                    p_studentName = property.FindPropertyRelative("studentNameE");
                }
                else if (className == "F")
                {
                    p_studentName = property.FindPropertyRelative("studentNameF");
                }

                EditorGUI.PropertyField(r_studentName, p_studentName, new GUIContent(p_studentName.displayName));


                y += h;
                Rect r_isLeader = new Rect(x, y, w, h);
                SerializedProperty p_isLeader = property.FindPropertyRelative("isLeader");

                EditorGUI.PropertyField(r_isLeader, p_isLeader, new GUIContent(p_isLeader.displayName));


                --EditorGUI.indentLevel;
            }

            EditorGUI.EndProperty();
        }
    }
}
