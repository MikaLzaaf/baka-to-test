﻿public enum CharacterId 
{
    Undefined,
    MC,
    Haris,
    Sudin,
    Ming,
    Leha,
    Yana,
    Sylvia,
    Nabil,
    LiNa,
    Uriel
}
