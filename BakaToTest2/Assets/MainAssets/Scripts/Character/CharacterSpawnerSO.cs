﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Character Spawner", menuName = "ScriptableObject/Character Spawner")]
public class CharacterSpawnerSO : ScriptableObject
{

    //public PlayerEntity SpawnPlayer(
    //    CharacterId characterId,
    //    Vector3 spawnPosition,
    //    Quaternion spawnRotation,
    //    bool applySpawnPositionOffset,
    //    bool overrideInitialPlayerCostume = false,
    //    string initialPlayerCostumeName = null)
    //{
    //    GameObject playerPrefabObject = PlayerInfoManager.LoadPlayerPrefab(characterId);

    //    if (playerPrefabObject != null)
    //    {
    //        PlayerEntity playerPrefab = playerPrefabObject.GetComponent<PlayerEntity>();

    //        return SpawnPlayer(playerPrefab, spawnPosition, spawnRotation, applySpawnPositionOffset,
    //            overrideInitialPlayerCostume, initialPlayerCostumeName);
    //    }

    //    return null;
    //}


    public PlayerEntity SpawnPlayer(
        PlayerEntity playerPrefab,
        Vector3 spawnPosition,
        Quaternion spawnRotation,
        bool applySpawnPositionOffset,
        bool overrideInitialPlayerCostume = false,
        string initialPlayerCostumeName = null)
    {
        if (playerPrefab != null)
        {
            if (applySpawnPositionOffset)
            {
                spawnPosition += playerPrefab.spawnPositionOffset;
            }

            PlayerEntity playerEntity = (PlayerEntity)Instantiate(playerPrefab, spawnPosition, spawnRotation);

            // Set player costume
            if (playerEntity.costumeController != null)
            {
                if (overrideInitialPlayerCostume)
                {
                    playerEntity.costumeController.initialSelectedModelName = initialPlayerCostumeName;
                }
                // Set player's initial costume based on the game data
                else
                {
                    //var equipmentSet = GameDataManager.gameData.playerEquipments.GetCharacterEquipmentSet(playerPrefab.characterId);

                    //var equippedCostume = equipmentSet.GetEquipment(EquipmentType.Costume);

                    //playerEntity.playerCostumeController.initialSelectedModelName = equippedCostume;
                }
            }

            return playerEntity;
        }

        return null;
    }

    public ControllableEntity SpawnControllablePlayer(
        CharacterId characterId,
        Vector3 spawnPosition,
        Quaternion spawnRotation,
        bool applySpawnPositionOffset,
        bool overrideInitialPlayerCostume = false,
        string initialPlayerCostumeName = null)
    {
        GameObject playerPrefabObject = PlayerInfoManager.LoadControllablePlayerPrefab(characterId);

        if (playerPrefabObject != null)
        {
            ControllableEntity playerPrefab = playerPrefabObject.GetComponent<ControllableEntity>();

            return SpawnControllablePlayer(playerPrefab, spawnPosition, spawnRotation, applySpawnPositionOffset,
                overrideInitialPlayerCostume, initialPlayerCostumeName);
        }

        return null;
    }


    public ControllableEntity SpawnControllablePlayer(
        ControllableEntity playerPrefab,
        Vector3 spawnPosition,
        Quaternion spawnRotation,
        bool applySpawnPositionOffset,
        bool overrideInitialPlayerCostume = false,
        string initialPlayerCostumeName = null)
    {
        if (playerPrefab != null)
        {
            if (applySpawnPositionOffset)
            {
                spawnPosition += playerPrefab.spawnPositionOffset;
            }

            ControllableEntity playerEntity = (ControllableEntity)Instantiate(playerPrefab, spawnPosition, spawnRotation);

            // Set player costume
            if (playerEntity.costumeController != null)
            {
                if (overrideInitialPlayerCostume)
                {
                    playerEntity.costumeController.initialSelectedModelName = initialPlayerCostumeName;
                }
                // Set player's initial costume based on the game data
                else
                {
                    //var equipmentSet = GameDataManager.gameData.playerEquipments.GetCharacterEquipmentSet(playerPrefab.characterId);

                    //var equippedCostume = equipmentSet.GetEquipment(EquipmentType.Costume);

                    //playerEntity.playerCostumeController.initialSelectedModelName = equippedCostume;
                }
            }

            return playerEntity;
        }

        return null;
    }


    public GenericEnemy SpawnEnemy(
        GenericEnemy enemyPrefab,
        Vector3 spawnPosition,
        Quaternion spawnRotation,
        bool applySpawnPositionOffset)
    {
        if (enemyPrefab != null)
        {
            if (applySpawnPositionOffset)
            {
                spawnPosition += enemyPrefab.spawnPositionOffset;
            }

            GenericEnemy enemy = (GenericEnemy)Instantiate(enemyPrefab, spawnPosition, spawnRotation);

            return enemy;
        }

        return null;
    }
}
