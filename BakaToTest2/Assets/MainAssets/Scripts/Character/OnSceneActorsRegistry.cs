﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PixelCrushers.DialogueSystem;


public class OnSceneActorsRegistry : Singleton<OnSceneActorsRegistry>
{
    private List<Actor> _actors = new List<Actor>();
    public List<Actor> actors
    {
        get
        {
            return _actors;
        }
        private set
        {
            _actors = value;
        }
    }


    private Dictionary<string, Actor> _actorsLookup;
    public Dictionary<string, Actor> actorsLookup
    {
        get
        {
            if (_actorsLookup == null)
            {
                _actorsLookup = new Dictionary<string, Actor>();

                if (actors != null)
                {
                    foreach (Actor entity in actors)
                    {
                        _actorsLookup.Add(entity.actorName, entity);
                    }
                }
            }

            return _actorsLookup;
        }
    }


    public Actor GetActor(string name)
    {
        actorsLookup.TryGetValue(name, out Actor actor);

        return actor;
    }


    public void AddActor(Actor actorToAdd)
    {
        Actor actor = GetActor(actorToAdd.actorName);

        if(actor == null)
        {
            actors.Add(actorToAdd);
            actorsLookup.Add(actorToAdd.actorName, actorToAdd);
        }
    }


    public void RemoveActor(Actor actorToRemove)
    {
        Actor actor = GetActor(actorToRemove.actorName);

        if(actor != null)
        {
            actors.Remove(actor);
            actorsLookup.Remove(actor.actorName);
        }
    }

    public void SetActorExpression(string actorName, ActorExpression expressionType)
    {
        Actor targetActor = GetActor(actorName);

        if (targetActor != null)
        {
            targetActor.SetExpression(expressionType, false);
        }
    }

    #region Lua implementation

    void OnEnable()
    {
        // Make the functions available to Lua: (Replace these lines with your own.)
        Lua.RegisterFunction(nameof(SetActorExpression), this, SymbolExtensions.GetMethodInfo(() => SetActorExpression(string.Empty, (double)0)));
    }

    void OnDisable()
    {
        Lua.UnregisterFunction(nameof(SetActorExpression));
    }


    public void SetActorExpression(string actorName, double expressionType)
    {
        int expressionIndex = (int)expressionType;
        SetActorExpression(actorName, (ActorExpression)expressionIndex);
    }
    

    #endregion
}
