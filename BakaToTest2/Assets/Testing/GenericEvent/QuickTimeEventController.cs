﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu]
public class QuickTimeEventController : ScriptableObject
{
    public Action<int> QuickTimeEvent;

    public QuickTimeEventTrigger quickTimeTrigger_TimerPrefab;
    public QuickTimeEventTrigger quickTimeTrigger_ScalerPrefab;


    private List<QuickTimeEventTrigger> quickTriggers = new List<QuickTimeEventTrigger>();



    public void SpawnQTETrigger(float triggerDuration, Action<int> action, bool isTimerType, bool useSlowMotion = false)
    {
        Debug.Log("Spawn QTE");
        QuickTimeEventTrigger triggerPrefab = isTimerType ? quickTimeTrigger_TimerPrefab : quickTimeTrigger_ScalerPrefab;

        QuickTimeEventTrigger quickTrigger = Instantiate(triggerPrefab) as QuickTimeEventTrigger;
        quickTriggers.Add(quickTrigger);

        quickTrigger.Initialize(triggerDuration, action, isTimerType, useSlowMotion);

        quickTrigger.OnDestroyEvent += UpdateTriggerList;

        //if (quickTriggers.Count > 1)
        //{
        //    quickTrigger.canHandleInput = false;
        //}
    }


    private void UpdateTriggerList(QuickTimeEventTrigger quickTrigger)
    {
        quickTrigger.OnDestroyEvent -= UpdateTriggerList;

        quickTriggers.RemoveAt(0);

        //if(quickTriggers.Count > 0)
        //{
        //    quickTriggers[0].canHandleInput = true;
        //}
    }


    public void ExecuteQTE(int value)
    {
        QuickTimeEvent?.Invoke(value);
    }
}
