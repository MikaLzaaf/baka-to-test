﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorAnimationBehaviour : StateMachineBehaviour
{

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);

        PlaceChangesInteractable door = animator.GetComponent<PlaceChangesInteractable>();

        if (door != null)
        {
            door.ChangePlace();
        }
    }
}
