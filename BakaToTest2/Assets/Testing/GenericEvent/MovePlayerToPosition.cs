﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using UnityEngine.Events;

public class MovePlayerToPosition : MonoBehaviour
{
    [Header("To Inside")]
    public Transform targetPosition_In;
    public CinemachineVirtualCamera vCam_LookAtIn;

    [Header("To Outside")]
    public Transform targetPosition_Out;
    public CinemachineVirtualCamera vCam_LookAtOut;

    public UnityEvent MoveCompleteEvent;

    public bool moveToInside = false;

    private ControllableEntity player;


    public void Activate()
    {
        StartCoroutine(ProcessingMovement(player));
    }


    private IEnumerator ProcessingMovement(ControllableEntity player)
    {
        //GameUIManager.instance.loadingView.Show(LoadingState.Loading);

        PlayerPartyManager.playerCannotMove = true;

        MainCameraController.instance.thirdPersonCamera_Single.m_RecenterToTargetHeading.m_enabled = true;

        yield return new WaitForSeconds(0.4f);

        Transform targetPos = moveToInside ? targetPosition_In : targetPosition_Out;

        //player.playerCharacterController.AutoMove(false, targetPos.position);
        //yield return new WaitUntil(() => !player.playerCharacterController.IsProcessingAutoMovement);

        //player.transform.SetPositionAndRotation(targetPos.position, targetPos.rotation);

        MoveCompleteEvent?.Invoke();

        yield return new WaitForSeconds(1f);

        MainCameraController.instance.thirdPersonCamera_Single.m_RecenterToTargetHeading.m_enabled = false;

        MainCameraController.instance.SetCameraBlend(1f, false);

        CinemachineVirtualCamera activeCam = moveToInside ? vCam_LookAtIn : vCam_LookAtOut;

        if (activeCam != null)
        {
            activeCam.gameObject.SetActive(false);
        }

        moveToInside = !moveToInside;

        PlayerPartyManager.playerCannotMove = false;

        //GameUIManager.instance.loadingView.ChangeState(LoadingState.Succeeded);
    }


    public void SwitchOnCamera()
    {
        player = PlayerPartyManager.instance.selectedPlayer;

        Vector3 directionToTarget = transform.position - player.transform.position;
        float angle = Vector3.Angle(transform.forward, directionToTarget);

        if (Mathf.Abs(angle) > 90)
        {
            Debug.Log("target is behind me");
            moveToInside = true;
        }
        else
        {
            Debug.Log("target is in front of me");
            moveToInside = false;
        }

        MainCameraController.instance.SetCameraBlend(1f, true);

        CinemachineVirtualCamera activeCam = moveToInside ? vCam_LookAtIn : vCam_LookAtOut;

        if (activeCam != null)
        {
            activeCam.gameObject.SetActive(true);
        }
    }

}
