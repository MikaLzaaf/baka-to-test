﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using System;
using Random = UnityEngine.Random;
using DG.Tweening;
using UnityEngine.InputSystem;


public class QuickTimeEventTrigger : MonoBehaviour
{
    public Action<int> QuickTimeEvent;
    public Action<QuickTimeEventTrigger> OnDestroyEvent;

    private const string ButtonIconsDirectoryInResources = "Images/Buttons/";
    private const string ButtonNamePrefix = "Button_";

    public TimeScaleController timeScaleController;
    public Image buttonIcon;
    public Image indicator;

    public float eventDuration = 1f;
    public float inputTriggeredVisualDuration = 0.1f;
    public float timeSlowScale = 0.4f;
    public Vector3 buttonPunchScale;

    public bool isTimerType = true;
    public bool initOnStart = false;

    [Header("Scaler")]
    public Image acceptableRadius;
    public float acceptableRangeMax = 0.5f;
    public float criticalRangeMax = 0.2f;
    public float scaleInitialSize = 2f;
    public float scaleFinalSize = 0.5f;


    private GameInputButtons triggerButton;

    private float elapsedTime = 0f;
    private float differenceInSize = 0f;

    private bool isTriggered = false;
    private bool isInitialized = false;

    public bool canHandleInput { get; set; }


    private void Start()
    {
        if(initOnStart)
        {
            Initialize(2f, Testta, isTimerType);
        }
    }


    private void OnDestroy()
    {
        OnDestroyEvent?.Invoke(this);
    }


    private void Testta(int eventValue)
    {
        Debug.Log("Event value is " + eventValue);
    }


    public void Initialize(float duration, Action<int> action, bool isTimer, bool useSlowMotion = false)
    {
        // set duration and button icon
        eventDuration = duration;

        isTimerType = isTimer;

        string buttonName = ButtonNamePrefix + RandomButtonIconName();

        buttonIcon.sprite = Resources.Load<Sprite>(ButtonIconsDirectoryInResources + buttonName);
       
        // Set input to detect button trigger

        if(action != null)
        {
            QuickTimeEvent += action;
        }

        if(isTimer)
        {
            indicator.fillAmount = 1f;
        }
        else
        {
            indicator.transform.localScale = Vector3.one * scaleInitialSize;

            differenceInSize = Mathf.Abs(scaleFinalSize - scaleInitialSize);

            float acceptableSize = scaleInitialSize - (differenceInSize * acceptableRangeMax);

            acceptableRadius.transform.localScale = new Vector2(acceptableSize, acceptableSize);
        }

        elapsedTime = 0f;

        isInitialized = true;

        canHandleInput = true;

        isTriggered = false;

        ToggleSlowMotion(useSlowMotion);
    }


    private void ToggleSlowMotion(bool active)
    {
        Time.timeScale = active ? timeSlowScale : 1f;
    }


    private string RandomButtonIconName()
    {
        int index = Random.Range(3, 7); // Index of GameInputButtons : Triangle to Square

        triggerButton = (GameInputButtons)index;

        return triggerButton.ToString();
    }


    private void Update()
    {
        if(isInitialized && elapsedTime <= eventDuration && !isTriggered)
        {
            elapsedTime += timeScaleController.deltaTime;

            UpdateIndicator();

            UpdateInput();
        }
        else if(elapsedTime > eventDuration && !isTriggered)
        {
            StartCoroutine(ExecuteEvent(false));
        }
    }


    private void UpdateIndicator()
    {
        if(isTimerType)
        {
            float amountToReduce = elapsedTime / eventDuration;

            amountToReduce = Mathf.Min(amountToReduce, 1);

            indicator.fillAmount = 1 - amountToReduce;
        }
        else
        {
            float amountToReduce = (elapsedTime / eventDuration) * differenceInSize;

            Vector3 reduceScale = new Vector3(amountToReduce, amountToReduce, amountToReduce);

            indicator.transform.localScale = Vector3.one * scaleInitialSize - reduceScale;
        }
    }


    private void UpdateInput()
    {
        if (!canHandleInput)
            return;

        //if (GameInput.instance.GetButtonInput(triggerButton))
        //{
        //    Debug.Log("GREATTTT!!!");
        //    StartCoroutine(ExecuteEvent(true));
        //}
        //else if(GameInput.instance.GetAnyInputExcept(triggerButton))
        //{
        //    Debug.Log("Hmph");
        //    StartCoroutine(ExecuteEvent(false));
        //}
    }


    private IEnumerator ExecuteEvent(bool hasInput)
    {
        isTriggered = true;

        if(hasInput)
        {
            buttonIcon.transform.parent.DOPunchScale(buttonPunchScale, inputTriggeredVisualDuration, 1, 0);
        }
        
        int eventValue = hasInput ? CheckRangeOfInput() : -1;

        yield return new WaitForSeconds(inputTriggeredVisualDuration);

        QuickTimeEvent?.Invoke(eventValue);

        ToggleSlowMotion(false);

        Destroy(gameObject);
    }


    private int CheckRangeOfInput()
    {
        int index;
        float inputTime = 1 - (elapsedTime / eventDuration);

        if(inputTime <= criticalRangeMax)
        {
            index = 1;
        }
        else if(inputTime <= acceptableRangeMax)
        {
            index = 0;
        }
        else
        {
            index = -1;
        }

        return index;
    }


    public void RegisterInput(InputAction.CallbackContext context)
    {
        if (!canHandleInput)
            return;

        if (context.performed)
        {
            StartCoroutine(ExecuteEvent(true));
        }
    }


    public void FalseInput(InputAction.CallbackContext context)
    {
        if (!canHandleInput)
            return;

        if (context.performed)
        {
            StartCoroutine(ExecuteEvent(false));
        }
    }
}
