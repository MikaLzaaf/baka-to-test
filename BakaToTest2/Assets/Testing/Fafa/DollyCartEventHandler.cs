﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Cinemachine;

public class DollyCartEventHandler : MonoBehaviour
{
    public CinemachineSmoothPath dollyPath;
    public CinemachineDollyCart dollyCart;

    public UnityEvent OnDollyEndedEvent;

    private bool isFinished = false;



    private void Update()
    {
        if (dollyCart.m_UpdateMethod == CinemachineDollyCart.UpdateMethod.Update)
            return;

        if(dollyCart.m_Position >= dollyPath.PathLength - 0.1f && !isFinished)
        {
            isFinished = true;

            OnDollyEnded();
        }
        else if(dollyCart.m_Position < dollyPath.PathLength - 0.1f && isFinished)
        {
            isFinished = false;
        }
    }

    private void FixedUpdate()
    {
        if (dollyCart.m_UpdateMethod == CinemachineDollyCart.UpdateMethod.FixedUpdate)
            return;

        if (dollyCart.m_Position >= dollyPath.PathLength - 0.1f && !isFinished)
        {
            isFinished = true;

            OnDollyEnded();
        }
        else if (dollyCart.m_Position < dollyPath.PathLength - 0.1f && isFinished)
        {
            isFinished = false;
        }
    }

    private void LateUpdate()
    {
        if (dollyCart.m_UpdateMethod != CinemachineDollyCart.UpdateMethod.LateUpdate)
            return;

        if (dollyCart.m_Position >= dollyPath.PathLength - 0.1f && !isFinished)
        {
            isFinished = true;

            OnDollyEnded();
        }
        else if (dollyCart.m_Position < dollyPath.PathLength - 0.1f && isFinished)
        {
            isFinished = false;
        }
    }


    private void OnDollyEnded()
    {
        OnDollyEndedEvent?.Invoke();
    }


    public void Testing()
    {
        Debug.Log("Testing");
    }
}
