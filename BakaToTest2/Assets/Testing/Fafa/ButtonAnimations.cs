using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.EventSystems;

namespace Intelligensia.UI
{
    [RequireComponent(typeof(EventTrigger))]
    public class ButtonAnimations : MonoBehaviour
    {
        //[SerializeField] private bool stayHighlightedUponClicked = default;

        [Header("Filling")]
        [SerializeField] private Image fillingHighlight = default;
        [SerializeField] private float fillingDuration = 0.1f;

        [Header("Scaling")]
        [SerializeField] private Transform transformToScale = default;
        [SerializeField] private float scaleMultiplerOffset = 1.1f;
        [SerializeField] private float scaleDuration = 0.1f;

        [Header("Pointing")]
        [SerializeField] private GameObject pointer = default;
        [SerializeField] private float pointingInterval = 0.2f;
        [SerializeField] private float pointingSlideOffset = 10f;

        [Header("Outlining")]
        [SerializeField] private GameObject outliner = default;

        private Vector3? pointerOriginalPosition;
        private Sequence pointingSequence;


        private bool isFilling;
        private bool isScaling;
        private bool isPointing;


        public void FillingHighlight(bool active)
        {
            if (fillingHighlight == null)
                return;

            if (active == isFilling)
                return;

            isFilling = active;

            fillingHighlight.fillAmount = active ? 0f : 1f;

            fillingHighlight.gameObject.SetActive(true);

            float endValue = active ? 1f : 0f;

            fillingHighlight.DOFillAmount(endValue, fillingDuration).OnComplete(
                delegate {
                    if (!active)
                        fillingHighlight.gameObject.SetActive(false);
            });
        }

        public void ScaleButton(bool active)
        {
            if (transformToScale == null)
                return;

            if (active == isScaling)
                return;

            isScaling = active;

            if (active)
                transformToScale.DOScale(scaleMultiplerOffset, scaleDuration);
            else
                transformToScale.DOScale(1f, scaleDuration);
        }

        public void BeginPointing(bool active)
        {
            if (pointer == null)
                return;

            if (active == isPointing)
                return;

            isPointing = active;

            if (active)
            {
                if (!pointerOriginalPosition.HasValue)
                    pointerOriginalPosition = pointer.transform.localPosition;

                pointer.SetActive(true);

                if (pointingSequence != null)
                    pointingSequence.Kill();

                pointingSequence = DOTween.Sequence();
                pointingSequence.SetDelay(pointingInterval);

                Tween yoyo = pointer.transform.DOLocalMoveX(pointerOriginalPosition.Value.x + pointingSlideOffset, pointingInterval);
                pointingSequence.Append(yoyo);

                pointingSequence.SetLoops(-1, LoopType.Yoyo);
            }
            else
            {
                pointer.SetActive(false);

                if (pointingSequence != null)
                    pointingSequence.Kill();

                pointer.transform.localPosition = pointerOriginalPosition.Value;
            }
        }

        public void ToggleOutline(bool active)
        {
            if (outliner == null)
                return;

            if (active == outliner.activeSelf)
                return;

            outliner.SetActive(active);
        }
    }
}

