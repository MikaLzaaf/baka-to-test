using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace Intelligensia.UI
{
    public class MiniCharacterWindow : MonoBehaviour
    {
        public string characterName;
        [SerializeField] Transform cameraPivot = default;
        [SerializeField] Camera windowCamera = default;
        [SerializeField] RawImage windowImage = default;
        [SerializeField] Vector3 lookAtAngleOffset = default;
        [SerializeField] TextMeshProUGUI characterNameText = default;

        [Header("Debug")]
        [SerializeField] private Actor targetCharacter = default;
        [SerializeField] private int lookAtIndex = 0;
        [SerializeField] private int expressionIndex = 0;
        [SerializeField] private bool activateOnStart = default;
        [SerializeField] private bool testExpression = default;


        public int windowIndex = -1;

        private Actor characterActor;

        private const string RenderTexturesInResources = "RenderTextures/CharacterWindow_";


        private void Start()
        {
            if (activateOnStart)
                SetupWindow();
        }

        private void Update()
        {
            if (testExpression)
            {
                SetCharacterExpression((ActorExpression)expressionIndex);
                testExpression = false;
            }
        }

        public void SetupWindowDebate(int index, BattleEntity battleEntity)
        {
            int lookAtIndex = battleEntity.entityTag == EntityTag.Player ? 1 : 2;

            Actor actor = battleEntity.GetComponent<Actor>();

            SetupWindow(index, lookAtIndex, actor);
        }


        public void SetupWindow()
        {
            SetupWindow(0, lookAtIndex, targetCharacter);
        }

        public void SetupWindow(int index, int characterLookAtIndex, Actor targetCharacter)
        {
            if (windowCamera == null || windowImage == null || targetCharacter == null)
                return;
            
            windowIndex = index;

            string path = RenderTexturesInResources + windowIndex;

            RenderTexture renderTexture = Resources.Load<RenderTexture>(path);

            if(renderTexture == null)
            {
                Debug.LogError("There is no RenderTexture in " + path);
                return;
            }

            windowCamera.targetTexture = renderTexture;
            windowImage.texture = renderTexture;

            cameraPivot.position = targetCharacter.transform.position;

            characterActor = targetCharacter;

            if(characterNameText != null)
                characterNameText.text = characterActor.actorName;

            SetCharacterLookAt(characterLookAtIndex);
        }

        public void SetCharacterLookAt(int characterLookAtIndex)
        {
            if (cameraPivot == null)
                return;

            // 0 = look straight
            // 1 = look slightly to right
            // 2 = look slightly to left

            if (characterLookAtIndex == 1)
            {
                cameraPivot.localRotation = Quaternion.Euler(lookAtAngleOffset);
                //cameraPivot.localRotation = 
            }
            else if (characterLookAtIndex == 2)
            {
                //cameraPivot.localRotation = Quaternion.Euler(-lookAtAngleOffset);
                cameraPivot.localRotation = Quaternion.Euler(new Vector3 (0f, -135f, 0f));
            }
            else
                cameraPivot.localRotation = Quaternion.identity;
        }

        public void SetCharacterExpression(ActorExpression actorExpression)
        {
            if (characterActor == null)
                return;

            characterActor.SetExpression(actorExpression, true);
        }

        [ContextMenu("Set Camera To Character")]
        private void SetCameraToCharacter()
        {
            cameraPivot.position = characterActor.transform.position;
        }
    }

}

