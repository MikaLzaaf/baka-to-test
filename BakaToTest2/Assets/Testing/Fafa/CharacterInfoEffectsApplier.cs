﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Intelligensia.Battle.StatusEffects;


namespace Intelligensia.Battle
{
    public static class CharacterInfoEffectsApplier
    {



        //static CharacterInfoEffectsApplier()
        //{
        //    Initialize();
        //}

        //private static void Initialize()
        //{

        //}

        public static bool IsCharacterInfoAffecting<T>(BattleEntity character, BattleEntity opponent, out EffectsTag[] effectsTags)
        {
            List<EffectsTag> effectsTagsList = new List<EffectsTag>();

            effectsTags = null;

            if (character == null)
                return false;

            //bool isAffecting = false;
            bool isPlayerCharacter = character.entityTag == EntityTag.Player;

            for (int i = 0; i < character.CharacterStatsBase.characterInfos.Count; i++)
            {
                CharacterInfo affectingInfo = character.CharacterStatsBase.characterInfos[i];

                CharacterInfoData infoData = character.characterStatsData.GetCharacterInfoData(affectingInfo.infoId);

                if (infoData == null)
                    continue;

                if (affectingInfo.HasEffectsTag<T>() && !infoData.isLocked)
                {
                    bool isStatsEffectsTag = typeof(T) == typeof(EffectsStatsTag);

                    bool isInfoMatchesConditions = IsCharacterInfoMatchesConditions(affectingInfo, isStatsEffectsTag, isPlayerCharacter, opponent);

                    if (isInfoMatchesConditions && !infoData.isInEffect)
                    {
                        effectsTagsList.Add(affectingInfo.EffectsTag);
                    }
                   
                    if (!isStatsEffectsTag)
                        continue;

                    if (infoData.isInEffect && !isInfoMatchesConditions)
                    {
                        // Remove info effect
                        RemoveInfoEffect(character, affectingInfo.EffectsTag);
                    }

                    infoData.isInEffect = isInfoMatchesConditions;
                }
            }

            effectsTags = effectsTagsList.ToArray();

            return effectsTags != null && effectsTags.Length > 0;
        }

        public static int CharacterInfoMatchesCount(BattleEntity character, BattleEntity opponent)
        {
            int match = 0;

            if (character == null)
                return match;

            //bool isAffecting = false;
            bool isPlayerCharacter = character.entityTag == EntityTag.Player;

            for (int i = 0; i < character.CharacterStatsBase.characterInfos.Count; i++)
            {
                CharacterInfo affectingInfo = character.CharacterStatsBase.characterInfos[i];

                CharacterInfoData infoData = character.characterStatsData.GetCharacterInfoData(affectingInfo.infoId);

                if (infoData == null)
                    continue;

                if (!infoData.isLocked && IsCharacterInfoMatchesConditions(affectingInfo, false, isPlayerCharacter, opponent))
                    match += 1;
            }

            return match;
        }


        public static bool IsCharacterInfoMatchesConditions(CharacterInfo affectingInfo, bool isStatsEffectsTag, bool isPlayerCharacter, BattleEntity opponent = null)
        {
            bool isMatching = false;

            if (affectingInfo != null)
            {
                if (affectingInfo.CausesTag is CausesDayTag)
                {
                    //Debug.Log("Cause tag : Day");
                    isMatching = affectingInfo.CausesTag.name == BattleInfoManager.currentDay.ToString();
                }
                else if (affectingInfo.CausesTag is CausesEnvironmentTag)
                {
                    // For now just ignore
                    //Debug.Log("Cause tag : Environment");
                }
                else if (affectingInfo.CausesTag is CausesGenderTag)
                {
                    //Debug.Log("Cause tag : Gender");
                    if(isStatsEffectsTag) // Checks all opponents
                    {
                        if (isPlayerCharacter)
                        {
                            for (int i = 0; i < BattleInfoManager.activeEnemies.Count; i++)
                            {
                                if (BattleInfoManager.activeEnemies[i].CharacterStatsBase.gender == affectingInfo.CausesTag)
                                {
                                    isMatching = true;
                                    break;
                                }
                            }
                        }
                        else
                        {
                            for (int i = 0; i < BattleInfoManager.activePlayers.Count; i++)
                            {
                                if (BattleInfoManager.activePlayers[i].CharacterStatsBase.gender == affectingInfo.CausesTag)
                                {
                                    isMatching = true;
                                    break;
                                }
                            }
                        }
                    }
                    else
                    {
                        if(opponent != null)
                        {
                            isMatching = affectingInfo.CausesTag == opponent.CharacterStatsBase.gender;
                        }
                    }
                   
                }
                else if (affectingInfo.CausesTag is CausesSubjectTag)
                {
                    //Debug.Log("Cause tag : Subject");
                    isMatching = affectingInfo.CausesTag.name == BattleInfoManager.currentActiveSubject.ToString();
                }
            }

            return isMatching;
        }

        /// <summary>
        /// This is used only for Combat stats effect. Not for Conflict.
        /// </summary>
        /// <param name="target"></param>
        /// <param name="effectsTags"></param>
        public static void ApplyInfoEffect(BattleEntity target, EffectsTag[] effectsTags)
        {
            for (int j = 0; j < effectsTags.Length; j++)
            {
                EffectsStatsTag effectsStatsTag = (EffectsStatsTag)effectsTags[j];

                if (effectsStatsTag.StatsEnum == EffectsStatsTag.EffectsStatsEnum.HigherAttack)
                {
                    target.statusEffectController.AddStatusBoost(StatusBoostType.AttackUp, 999);
                }
                else if (effectsStatsTag.StatsEnum == EffectsStatsTag.EffectsStatsEnum.HigherDefense)
                {
                    target.statusEffectController.AddStatusBoost(StatusBoostType.DefenseUp, 999);
                }
                else if (effectsStatsTag.StatsEnum == EffectsStatsTag.EffectsStatsEnum.HigherStatusResistance)
                {
                    target.statusEffectController.AddStatusBoost(StatusBoostType.StatusResistanceUp, 999);
                }
                else if (effectsStatsTag.StatsEnum == EffectsStatsTag.EffectsStatsEnum.KnockoutChanceDown)
                {
                    target.statusEffectController.AddStatusBoost(StatusBoostType.KnockoutChanceDown, 999);
                }
                else if (effectsStatsTag.StatsEnum == EffectsStatsTag.EffectsStatsEnum.KnockoutChanceBigDown)
                {
                    target.statusEffectController.AddStatusBoost(StatusBoostType.KnockoutChanceDownBig, 999);
                }
                else if (effectsStatsTag.StatsEnum == EffectsStatsTag.EffectsStatsEnum.LowerAttack) // Ailments
                {
                    target.statusEffectController.AddStatusAilment(StatusAilmentType.AttackDown, 999);
                }
                else if (effectsStatsTag.StatsEnum == EffectsStatsTag.EffectsStatsEnum.LowerDefense)
                {
                    target.statusEffectController.AddStatusAilment(StatusAilmentType.DefenseDown, 999);
                }
                else if (effectsStatsTag.StatsEnum == EffectsStatsTag.EffectsStatsEnum.LowerStatusResistance)
                {
                    target.statusEffectController.AddStatusAilment(StatusAilmentType.StatusResistanceDown, 999);
                }
                else if (effectsStatsTag.StatsEnum == EffectsStatsTag.EffectsStatsEnum.BreakLivesOnKnockout)
                {
                    target.statusEffectController.AddStatusAilment(StatusAilmentType.BreakLivesUp, 999);
                }
                else if (effectsStatsTag.StatsEnum == EffectsStatsTag.EffectsStatsEnum.KnockoutChanceUp)
                {
                    target.statusEffectController.AddStatusAilment(StatusAilmentType.KnockoutChanceUp, 999);
                }
                else if (effectsStatsTag.StatsEnum == EffectsStatsTag.EffectsStatsEnum.KnockoutChanceBigUp)
                {
                    target.statusEffectController.AddStatusAilment(StatusAilmentType.KnockoutChanceUpBig, 999);
                }
            }
        }

        public static void RemoveInfoEffect(BattleEntity target, EffectsTag effectsTag)
        {
            EffectsStatsTag effectsStatsTag = (EffectsStatsTag)effectsTag;

            if (effectsStatsTag.StatsEnum == EffectsStatsTag.EffectsStatsEnum.HigherAttack)
            {
                target.statusEffectController.RemoveEffect<StatusEffect_AttackUp>(true);
                //Debug.Log("Remove effect " + effectsStatsTag.statsEnum);
            }
            else if (effectsStatsTag.StatsEnum == EffectsStatsTag.EffectsStatsEnum.HigherDefense)
            {
                target.statusEffectController.RemoveEffect<StatusEffect_DefenseUp>(true);
                //Debug.Log("Remove effect " + effectsStatsTag.statsEnum);
            }
            else if (effectsStatsTag.StatsEnum == EffectsStatsTag.EffectsStatsEnum.HigherStatusResistance)
            {
                target.statusEffectController.RemoveEffect<StatusEffect_StatusResistanceUp>(true);
                //Debug.Log("Remove effect " + effectsStatsTag.statsEnum);
            }
            else if (effectsStatsTag.StatsEnum == EffectsStatsTag.EffectsStatsEnum.LowerAttack) // Ailments
            {
                target.statusEffectController.RemoveEffect<StatusEffect_AttackDown>(true);
                //Debug.Log("Remove effect " + effectsStatsTag.statsEnum);
            }
            else if (effectsStatsTag.StatsEnum == EffectsStatsTag.EffectsStatsEnum.LowerDefense)
            {
                target.statusEffectController.RemoveEffect<StatusEffect_DefenseDown>(true);
                //Debug.Log("Remove effect " + effectsStatsTag.statsEnum);
            }
            else if (effectsStatsTag.StatsEnum == EffectsStatsTag.EffectsStatsEnum.LowerStatusResistance)
            {
                target.statusEffectController.RemoveEffect<StatusEffect_StatusResistanceDown>(true);
                //Debug.Log("Remove effect " + effectsStatsTag.statsEnum);
            }
            else if (effectsStatsTag.StatsEnum == EffectsStatsTag.EffectsStatsEnum.BreakLivesOnKnockout)
            {
                target.statusEffectController.RemoveEffect<StatusEffect_BreakLivesUp>(true);
                //Debug.Log("Remove effect " + effectsStatsTag.statsEnum);
            }
        }
    }
}

