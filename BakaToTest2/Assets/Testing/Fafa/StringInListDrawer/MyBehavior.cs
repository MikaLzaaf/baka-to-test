using UnityEngine;

public class MyBehavior : MonoBehaviour 
{
    // This will store the string value
    [StringInList("Cat", "Dog")] public string Animal;
    // This will store the index of the array value
    [StringInList("John", "Jack", "Jim")] public int PersonID;
    
    // Showing a list of loaded scenes
    [StringInList(typeof(PropertyDrawersHelper), "AllSceneNames")] public string SceneName;

    //[StringInList("A", "B", "C", "D", "E", "F")] public string className;
    [StringInList(typeof(PropertyDrawersHelper), "AllClassNames")] public string className;

    [StringInList(typeof(PropertyDrawersHelper), "CharacterNamesByClass", ss)] public string studentName;

    private const string ss = "F";
}