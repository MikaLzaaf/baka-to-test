using UnityEditor;
using UnityEngine;
using System.Collections.Generic;

public static class PropertyDrawersHelper {
  #if UNITY_EDITOR

    public static string[] AllSceneNames()
    {
        var temp = new List<string>();

        foreach (EditorBuildSettingsScene S in EditorBuildSettings.scenes)
        {
            if (S.enabled)
            {
                string name = S.path.Substring(S.path.LastIndexOf('/')+1);
                name = name.Substring(0,name.Length-6);
                temp.Add(name);
            }
        }

        return temp.ToArray();
    }

    public static string[] CharacterNamesByClass(string className)
    {
        string[] ss = PlayerInfoManager.GetClassmatesNames(className);
        return ss;
    }

    public static string[] AllClassNames()
    {
        string[] ss = PlayerInfoManager.classesNames;
        return ss;
    }

    public static string[] AllGradeResults()
    {
        var temp = new List<string>();

        temp.Add(GradeChecker.GreatResult);
        temp.Add(GradeChecker.GoodResult);
        temp.Add(GradeChecker.BadResult);

        return temp.ToArray();
    }

  #endif
}