using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class ConfirmationMessageBox : MessageBox
{
    [SerializeField] private TextMeshProUGUI contentText = default;
    [SerializeField] string testMessage = default;

    protected override void AttachButtonEventHandler(GameObject button)
    {
        
    }

    protected override void DetachButtonEventHandler(GameObject button)
    {
        
    }

    protected override string GetContentDisplayText()
    {
        return null;
    }

    protected override void UpdateContentDisplay()
    {
        if (contentText != null)
            contentText.text = messageContent;
	}

    protected override void OnViewReady()
    {
        base.OnViewReady();

        switch (buttonsType)
        {
            case MessageBoxButtons.None:
                UINavigator.instance.SetDefaultSelectedInRuntime(null);
                break;

            case MessageBoxButtons.Ok:
                UINavigator.instance.SetDefaultSelectedInRuntime(buttonOk);
                break;

            case MessageBoxButtons.YesNo:
                List<Button> buttons = new List<Button>();
                buttons.Add(buttonYes.GetComponent<Button>());
                buttons.Add(buttonNo.GetComponent<Button>());

                UINavigator.instance.SetupHorizontalNavigation(buttons, true);

                UINavigator.instance.SetDefaultSelectedInRuntime(buttonNo);
                break;

            case MessageBoxButtons.Cancel:
                UINavigator.instance.SetDefaultSelectedInRuntime(buttonCancel);
                break;
        }
    }


    public void TestMessage()
    {
        Show(testMessage, MessageBoxButtons.Ok, null);
    }
}
