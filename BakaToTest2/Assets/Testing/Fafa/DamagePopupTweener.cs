﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using CharTween;
using DG.Tweening;
using TMPro;
using System;

public class DamagePopupTweener : MonoBehaviour
{
    public TMP_Text Target;

    public int tweenType = 6;

    [Header("Normal Shake")]
    public float shakeStrength = 20f;
    public int vibrato = 20;
    public float duration = 0.4f;
    public bool snap = false;

    [Header("Critical Shake")]
    public float critShakeStrength = 20f;
    public int critVibrato = 30;
    public float critDuration = 0.4f;
    public Vector3 punchVector = new Vector3(3,3,3);
    public float elasticity = 0f;
    public float sizeIncrease = 8;

    public bool test = false;

    //private CharTweener _tweener;
    private Action callbackOnComplete;
    private float originalFontSize;

    public void Start()
    {
        //_tweener = Target.GetCharTweener();
        originalFontSize = Target.fontSize;

        //if (tweenType == 1)
        //{
        //    ApplyTweenToLine(0, Tween1);
        //}
        //else if (tweenType == 2)
        //{
        //    ApplyTweenToLine(0, Tween2);
        //}
        //else if (tweenType == 3)
        //{
        //    ApplyTweenToLine(0, Tween3);
        //}
        //else if (tweenType == 4)
        //{
        //    ApplyTweenToLine(0, Tween4);
        //}
        //else if (tweenType == 5)
        //{
        //    ApplyTweenToLine(0, TweenReal);
        //}
        //else if (tweenType == 6)
        //{
        //    ApplyTweenToLine(0, TestTween);
        //}
    }


    private void Update()
    {
        if(test)
        {
            //if (tweenType == 1)
            //{
            //    ApplyTweenToLine(0, Tween1);
            //}
            //else if (tweenType == 2)
            //{
            //    ApplyTweenToLine(0, Tween2);
            //}
            //else if (tweenType == 3)
            //{
            //    ApplyTweenToLine(0, Tween3);
            //}
            //else if (tweenType == 4)
            //{
            //    ApplyTweenToLine(0, Tween4);
            //}
            //else if (tweenType == 5)
            //{
            //    ApplyTweenToLine(0, TweenReal);
            //}
            //else if (tweenType == 6)
            //{
            //    //ApplyTweenToLine(0, TweenShake);
            //}

            if(tweenType == 6)
            {
                StartTween(DamageHitCategory.NormalHit, Color.green);
            }
            else if(tweenType == 7)
            {
                StartTween(DamageHitCategory.Missed, Color.white);
            }
            else
            {
                StartTween(DamageHitCategory.FatalCriticalHit, Color.green);
            }

            test = false;
        }
    }

    public void StartTween(Color endColor, Action callbackOnFinished = null)
    {
        //if (_tweener == null)
        //    _tweener = Target.GetCharTweener();

        callbackOnComplete = callbackOnFinished;

        var lineInfo = Target.textInfo.lineInfo[0];
        TweenShake(lineInfo.firstCharacterIndex, lineInfo.lastCharacterIndex, endColor);
    }

    public void StartTween(DamageHitCategory hitCategory, Color endColor, Action callbackOnFinished = null)
    {
        //if (_tweener == null)
        //{
        //    _tweener = Target.GetCharTweener();
        //    originalFontSize = Target.fontSize;
        //}
        //else
        //{
        //    Target.fontSize = originalFontSize;
        //}

        callbackOnComplete = callbackOnFinished;

        var lineInfo = Target.textInfo.lineInfo[0];

        if(hitCategory == DamageHitCategory.NormalHit || hitCategory == DamageHitCategory.Undefined)
        {
            TweenShake(lineInfo.firstCharacterIndex, lineInfo.lastCharacterIndex, endColor);
        }
        else if(hitCategory == DamageHitCategory.FatalCriticalHit || hitCategory == DamageHitCategory.NonFatalCriticalHit)
        {
            TweenCriticalShake(lineInfo.firstCharacterIndex, lineInfo.lastCharacterIndex, endColor);
        }
        else if(hitCategory == DamageHitCategory.Missed)
        {
            TweenMissHit(lineInfo.firstCharacterIndex, lineInfo.lastCharacterIndex);
        }
        else if (hitCategory == DamageHitCategory.Recovery)
        {
            TweenShake(lineInfo.firstCharacterIndex, lineInfo.lastCharacterIndex, endColor);
        }
    }


    private void TweenReal(int start, int end)
    {
        //var sequence = DOTween.Sequence();

        for (int i = start; i <= end; ++i)
        {
            // Used for sequential tweening
            var timeOffset = Mathf.Lerp(0, 1, (i - start) / (float)(end - start + 1));

            float upLimit = UnityEngine.Random.Range(0.3f, 0.6f);
            float downLimit = UnityEngine.Random.Range(-0.3f, -0.6f);

            //Tween upMovement = _tweener.DOLocalMoveY(i, upLimit, 0.05f).SetEase(Ease.InOutCubic);
            //Tween downMovement = _tweener.DOLocalMoveY(i, downLimit, 0.05f).SetEase(Ease.InOutCubic);
            //Tween toOriginalPosition = _tweener.DOLocalMoveY(i, 0f, 0.05f).SetEase(Ease.OutBounce);

            //Tween firstColorTween = _tweener.DOColor(i, Color.white, 0.15f);
            //firstColorTween.fullPosition = timeOffset;

            //Tween secondColorTween = _tweener.DOColor(i, Color.white, 0.25f);
            //secondColorTween.fullPosition = timeOffset;

            //Tween shakePosition = _tweener.DOShakePosition(i, 1, shakeStrength, vibrato, 90, false, false);

            var charSequence = DOTween.Sequence();


            if (i % 2 == 0)
            {
                //charSequence.Append(upMovement).Join(firstColorTween).
                //    Append(downMovement).
                //    Append(toOriginalPosition).
                //    Append(shakePosition).Join(secondColorTween);

            }
            else
            {
                //charSequence.Append(downMovement).Join(firstColorTween).
                //   Append(upMovement).
                //   Append(toOriginalPosition).
                //   Append(shakePosition).Join(secondColorTween);
            }

            //charSequence.SetLoops(-1, LoopType.Restart);
            //sequence.Insert(timeOffset, charSequence);
        }
    }

    private void TweenShake(int start, int end, Color endColor)
    {
        for (var i = start; i <= end; ++i)
        {
            var charSequence = DOTween.Sequence();

            var timeOffset = Mathf.Lerp(0, 1, (i - start) / (float)(end - start + duration));


            //var redTween = _tweener.DOColor(i, Color.red,
            //       0.1f);
            //redTween.fullPosition = timeOffset;


             
            //var shaker = _tweener.DOShakePosition(i, duration, shakeStrength, vibrato, 90, snap, false);

            //var colorTween = _tweener.DOColor(i, endColor,
            //        0.3f);
            //colorTween.fullPosition = timeOffset;

            //charSequence.Append(redTween).Join(shaker).Append(colorTween).
            //    OnComplete(delegate {
            //        callbackOnComplete?.Invoke();
            //    }
            //);
        }
    }

    private void TweenCriticalShake(int start, int end, Color endColor)
    {
        Target.fontSize += sizeIncrease;

        for (var i = start; i <= end; ++i)
        {
            var charSequence = DOTween.Sequence();

            var timeOffset = Mathf.Lerp(0, 1, (i - start) / (float)(end - start + critDuration));

            //var redTween = _tweener.DOColor(i, Color.yellow,
            //       0.1f);
            //redTween.fullPosition = timeOffset;



            //var shaker = _tweener.DOShakePosition(i, critDuration, critShakeStrength, critVibrato, 
            //    90, snap, false);

            //var scaler = _tweener.DOPunchScale(i, punchVector, critDuration, critVibrato, elasticity);

            //var colorTween = _tweener.DOColor(i, endColor,
            //        0.3f);
            //colorTween.fullPosition = timeOffset;

            //charSequence.Append(redTween).Join(shaker).Join(scaler).Append(colorTween).
            //    OnComplete(delegate {
            //        callbackOnComplete?.Invoke();
            //        _tweener.DOScale(i, sizeIncrease, .01f);
            //    }
            //);
        }
    }

    private void TweenMissHit(int start, int end)
    {
        for (var i = start; i <= end; ++i)
        {
            var timeOffset = Mathf.Lerp(0, 1, (i - start) / (float)(end - start + 1));

            var charSequence = DOTween.Sequence();

            //charSequence.Append(_tweener.DOLocalMoveY(i, 0.5f, 0.5f).SetEase(Ease.InOutCubic))
            //    .Join(_tweener.DOFade(i, 0, 0.5f).From())
            //    .Join(_tweener.DOScale(i, 0, 0.5f).From().SetEase(Ease.OutBack, 5))
            //    .Append(_tweener.DOLocalMoveY(i, 0, 0.5f).SetEase(Ease.OutBounce)).
            //    OnComplete(delegate {
            //        callbackOnComplete?.Invoke();
            //        _tweener.DOScale(i, sizeIncrease, .01f);
            //    }
            //);
        }
    }

    // Basic example 1, oscillating characters and color
    private void Tween1(int start, int end)
    {
        for (var i = start; i <= end; ++i)
        {
            var timeOffset = Mathf.Lerp(0, 1, (i - start) / (float)(end - start + 1));
            //var circleTween = _tweener.DOCircle(i, 0.05f, 0.5f)
            //    .SetEase(Ease.Linear)
            //    .SetLoops(-1, LoopType.Restart);
            //var colorTween = _tweener.DOColor(i, Color.yellow, 0.5f)
            //    .SetLoops(-1, LoopType.Yoyo);
            //circleTween.fullPosition = timeOffset;
            //colorTween.fullPosition = timeOffset;
        }
    }

    // Basic example 2, jittery characters + random color
    private void Tween2(int start, int end)
    {
        for (var i = start; i <= end; ++i)
        {
            var timeOffset = Mathf.Lerp(0, 1, (i - start) / (float)(end - start + 1));
            //_tweener.DOShakePosition(i, 1, 0.05f, 50, 90, false, false)
            //    .SetLoops(-1, LoopType.Restart);
            //var colorTween = _tweener.DOColor(i, UnityEngine.Random.ColorHSV(0, 1, 1, 1, 1, 1),
            //        UnityEngine.Random.Range(0.1f, 0.5f))
            //    .SetLoops(-1, LoopType.Yoyo);
            //colorTween.fullPosition = timeOffset;
        }
    }

    // Sequence example, bubbly fade-in + bounce
    private void Tween3(int start, int end)
    {
        var sequence = DOTween.Sequence();

        for (var i = start; i <= end; ++i)
        {
            var timeOffset = Mathf.Lerp(0, 1, (i - start) / (float)(end - start + 1));
            var charSequence = DOTween.Sequence();
            //charSequence.Append(_tweener.DOLocalMoveY(i, 0.5f, 0.5f).SetEase(Ease.InOutCubic))
            //    .Join(_tweener.DOFade(i, 0, 0.5f).From())
            //    .Join(_tweener.DOScale(i, 0, 0.5f).From().SetEase(Ease.OutBack, 5))
            //    .Append(_tweener.DOLocalMoveY(i, 0, 0.5f).SetEase(Ease.OutBounce));
            //sequence.Insert(timeOffset, charSequence);
        }

        sequence.SetLoops(-1, LoopType.Yoyo);
    }

    // Rotation example
    private void Tween4(int start, int end)
    {
        for (var i = start; i <= end; ++i)
        {
            var timeOffset = Mathf.Lerp(0, 1, (i - start) / (float)(end - start + 1));
            //var rotationTween = _tweener.DOLocalRotate(i, UnityEngine.Random.onUnitSphere * 360, 2, RotateMode.FastBeyond360)
            //    .SetEase(Ease.Linear)
            //    .SetLoops(-1, LoopType.Incremental);
            //rotationTween.fullPosition = timeOffset;
        }
    }

    private void ApplyTweenToLine(int line, Action<int, int> tween)
    {
        if (line >= Target.textInfo.lineCount)
            return;

        var lineInfo = Target.textInfo.lineInfo[line];
        tween(lineInfo.firstCharacterIndex, lineInfo.lastCharacterIndex);
    }

}
