﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UITextLocalizer : MonoBehaviour
{
	public string key;


	private Text _targetText;
	public Text targetText
	{
		get 
		{
			if (_targetText == null)
			{
				_targetText = GetComponent<Text>();
			}

			return _targetText;
		}
	}


	private void Awake()
	{
		if (targetText != null)
		{
			LocalizationManager.LanguageChangedEvent += OnLanguageChanged;

			UpdateText();
		}
	}


    private void OnDestroy()
    {
        LocalizationManager.LanguageChangedEvent -= OnLanguageChanged;
    }


    public void UpdateText()
	{
		if (targetText == null)
		{
			return; 
		}
		
		if (string.IsNullOrEmpty(key))
		{
			return;
		}

		if (!LocalizationManager.HasKey(key))
		{
			return;
		}

		targetText.text = LocalizationManager.Localize(key);
	}


	private void OnLanguageChanged(string language)
	{
		UpdateText();
	}
}