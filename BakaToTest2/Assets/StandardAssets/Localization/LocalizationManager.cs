﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public static class LocalizationManager
{
	public const string DefaultLanguage = "English";
    private const string LocalizationDataInResources = "Data/UILocalizationData";
    //private const string CharacterInfoDataInResources = "CharacterInfoData";


    private const string KeyHeader = "Key";
    private const int KeyIndex = 0;



    public static event Action<string> LanguageChangedEvent;



	private static List<string> _languages = new List<string>();
	public static List<string> languages
	{
		get 
		{
			return _languages;
		}
		private set 
		{
			_languages = value;
		}
	}


	private static Dictionary<string, Dictionary<string, string>> _localizationTable = new Dictionary<string, Dictionary<string, string>>();
	public static Dictionary<string, Dictionary<string, string>> localizationTable 
	{
		get 
		{
			return _localizationTable;
		}
		private set 
		{
			_localizationTable = value;
		}
	}


    public static string currentLanguage { get; private set; }



	static LocalizationManager()
	{
		Initialize();
	}

	
	public static void Initialize()
	{
		languages = new List<string>();
		localizationTable = new Dictionary<string, Dictionary<string, string>>();

		// Load character infos localization
		LoadLocalizationData(LocalizationDataInResources);


		// Set the default language
		AddLanguage(DefaultLanguage);
		ChangeLanguage(DefaultLanguage);
	}


	public static void LoadLocalizationData(string dataPathInResources)
	{
		TextAsset dataAsset = Resources.Load<TextAsset>(dataPathInResources);

		if (dataAsset != null)
		{
			LoadLocalizationData(dataAsset);
		}
	}


    public static void LoadLocalizationData(TextAsset dataAsset)
	{
		using (StringReader reader = new StringReader(dataAsset.text))
		{
			CsvParser parser = new CsvParser();

			string[][] data = parser.Parse(reader);
			string[] headers = null;

			if (data == null || data.Length == 0)
			{
				return;
			}

			headers = data[0];

			if (headers == null || headers.Length <= 1)
			{
				return;
			}

			if (headers[KeyIndex] != KeyHeader)
			{
				Debug.LogWarning("The header of first column should be: " + KeyHeader);
				return;
			}

			// Add languages
			for (int column = KeyIndex + 1; column < headers.Length; column++)
			{
				AddLanguage(headers[column]);
			}

			for (int row = 1; row < data.Length; row++)
			{
				string[] dataRow = data[row];

				string key = dataRow[KeyIndex];

				if (string.IsNullOrEmpty(key))
				{
					continue;
				}

				for (int column = KeyIndex + 1; column < dataRow.Length; column++)
				{
					string value = dataRow[column];
					string language = headers[column];

					localizationTable[language][key] = value;
				}
			}
		}
	}


    public static Dictionary<string, Dictionary<string, string>> LoadLocalizationData(TextAsset dataAsset, int keyIndex, string keyHeader)
    {
        Dictionary<string, Dictionary<string, string>> localizedDict = new Dictionary<string, Dictionary<string, string>>();

        using (StringReader reader = new StringReader(dataAsset.text))
        {
            CsvParser parser = new CsvParser();

            string[][] data = parser.Parse(reader);
            string[] headers = null;

            if (data == null || data.Length == 0)
            {
                return null;
            }

            headers = data[0];

            if (headers == null || headers.Length <= 1)
            {
                return null;
            }

            if (headers[keyIndex] != keyHeader)
            {
                Debug.LogWarning("The header of first column should be: " + keyHeader);
                return null;
            }

            // Add languages
            for (int column = keyIndex + 1; column < headers.Length; column++)
            {
                if (!localizedDict.ContainsKey(headers[column]))
                {
                    localizedDict[headers[column]] = new Dictionary<string, string>();
                }
            }

            for (int row = 1; row < data.Length; row++)
            {
                string[] dataRow = data[row];

                string key = dataRow[keyIndex];

                if (string.IsNullOrEmpty(key))
                {
                    continue;
                }

                for (int column = keyIndex + 1; column < dataRow.Length; column++)
                {
                    string value = dataRow[column];
                    string language = headers[column];

                    localizedDict[language][key] = value;
                }
            }
        }

        return localizedDict;
    }



    public static bool HasKey(string key)
	{
		return HasKey(key, currentLanguage);
	}


	public static bool HasKey(string key, string language)
	{
		Dictionary<string, string> dataTable = null;

		if (localizationTable.TryGetValue(language, out dataTable))
		{
			return dataTable.ContainsKey(key);
		}

		return false;
	}


	public static string Localize(string key)
	{
		return Localize(key, currentLanguage);
	}


	public static string Localize(string key, string language)
	{
		if (string.IsNullOrEmpty(key))
		{
			return null;
		}
		
		if (HasLanguage(language))
		{
			Dictionary<string, string> dataList = null;

			if (localizationTable.TryGetValue(language, out dataList))
			{
				string result = null;

				if (dataList.TryGetValue(key, out result))
				{
					return result;
				}
			}
		}

		return key;
	}


	public static void AddLanguage(string language)
	{
		if (string.IsNullOrEmpty(language))
		{
			return;
		}

		if (languages.Contains(language))
		{
			return;
		}

		languages.Add(language);
	
		if (!localizationTable.ContainsKey(language))
		{
			localizationTable[language] = new Dictionary<string, string>();
		}
	}


	public static bool HasLanguage(string language)
	{
		return languages.Contains(language);
	}


	public static void ChangeLanguage(string language)
	{
		if (language == currentLanguage)
		{
			return;
		}

		if (!HasLanguage(language))
		{
			return;
		}

		currentLanguage = language;

		Action<string> handler = LanguageChangedEvent;

		if (handler != null)
		{
			handler(currentLanguage);
		}
	}
}
