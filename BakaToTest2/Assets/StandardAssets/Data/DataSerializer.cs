using UnityEngine;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Reflection;
using System.Xml;
using System.Xml.Serialization;


/// <summary>
/// It is used to serialize and deserialize object data in XML format.
/// </summary>
public static class DataSerializer
{	
	
	/// <summary>
	/// Serialize the object to the specified XML data file.
	/// </summary>
	public static void SerializeXmlToFile<T>(T dataObject, string dataFilePath)
	{
		try
		{
			using (StreamWriter writer = new StreamWriter(dataFilePath))
			{
				XmlSerializer serializer = new XmlSerializer(typeof(T));
				
				serializer.Serialize(writer, dataObject);
			}
		}
		catch (Exception ex)
		{
			Debug.LogError(ex);	
		}
	}
	
	
	/// <summary>
	/// Deserialize the object from the XML data file in Resources.
	/// </summary>
	public static T DeserializeXmlInResources<T>(string dataFileInResources)
	{
		T dataObject = default(T);


		TextAsset dataXml = Resources.Load<TextAsset>(dataFileInResources);
			
		if (dataXml != null)
		{
			dataObject = DeserializeXml<T>(dataXml.text);
		}
		else
		{
			//Debug.LogError("Can't find the XML data file: " + dataFileInResources);	
		}

		
		return dataObject;
	}
	
	
	/// <summary>
	/// Deserialize the object from the provided XML data.
	/// </summary>	
	public static T DeserializeXml<T>(string xmlContent)
	{
		T dataObject = default(T);
		
		try
		{
			XmlDocument doc = new XmlDocument();
			
			doc.LoadXml(xmlContent);
			
			XmlReader reader = new XmlNodeReader(doc);
			
			XmlSerializer serializer = new XmlSerializer(typeof(T));
			
			System.Object result = serializer.Deserialize(reader);

			if (result != null)
			{
				dataObject = (T)result;	
			}
		}
		catch (Exception ex)
		{
			Debug.LogError(ex);	
		}
		
		return dataObject;
	}
	
	
	/// <summary>
	/// Deserialize the object from the specified XML data file.
	/// </summary>	
	public static T DeserializeXmlFromFile<T>(string dataFilePath)
	{
		T dataObject = default(T);
		
		try
		{
			using (FileStream fileStream = new FileStream(dataFilePath, FileMode.Open))
			{
				XmlSerializer serializer = new XmlSerializer(typeof(T));
				
				System.Object result = serializer.Deserialize(fileStream);
				
				if (result != null)
				{
					dataObject = (T)result;	
				}
			}
		}
		catch (Exception ex)
		{
			Debug.LogError(ex);	
		}
		
		return dataObject;
	}
	
	
	public static void SerializeBinaryData(object dataObject, string dataFilePath)
	{
		try
		{
			EnableMonoReflectionSerializer();

			using (FileStream fileStream = new FileStream(dataFilePath, FileMode.OpenOrCreate))
			{
				BinaryFormatter formatter = new BinaryFormatter();

				formatter.Binder = new VersionDeserializationBinder();

				formatter.Serialize(fileStream, dataObject);
			}
		}
		catch (Exception ex)
		{
			Debug.LogError(ex);	
		}
	}


	public static byte[] SerializeBinaryData(object dataObject)
	{
		try
		{
			EnableMonoReflectionSerializer();

			using (MemoryStream memoryStream = new MemoryStream())
			{
				BinaryFormatter formatter = new BinaryFormatter();

				formatter.Binder = new VersionDeserializationBinder();

				formatter.Serialize(memoryStream, dataObject);

				memoryStream.Flush();

				return memoryStream.ToArray();
			}
		}
		catch (Exception ex)
		{
			Debug.LogError(ex);	
			return null;
		}
	}
	
	
	public static object DeserializeBinaryData(string dataFilePath)
	{
		try
		{
			EnableMonoReflectionSerializer();

			using (FileStream fileStream = new FileStream(dataFilePath, FileMode.Open))
			{
				BinaryFormatter formatter = new BinaryFormatter();

				formatter.Binder = new VersionDeserializationBinder();

				return formatter.Deserialize(fileStream);
			}
		}
		catch (Exception ex)
		{
			Debug.LogError(ex);	
		}
		
		return null;
	}


	public static object DeserializeBinaryData(byte[] data)
	{
		try
		{
			EnableMonoReflectionSerializer();

			using (MemoryStream memoryStream = new MemoryStream(data))
			{
				BinaryFormatter formatter = new BinaryFormatter();

				formatter.Binder = new VersionDeserializationBinder();

				return formatter.Deserialize(memoryStream);
			}
		}
		catch (Exception ex)
		{
			Debug.LogError(ex);	
		}

		return null;
	}


	private static void EnableMonoReflectionSerializer()
	{
		// It is required to avoid run-time code generation that will break iOS compatibility
		Environment.SetEnvironmentVariable("MONO_REFLECTION_SERIALIZER", "yes");
	}
}


public sealed class VersionDeserializationBinder : SerializationBinder 
{ 
	public override Type BindToType(string assemblyName, string typeName)
	{ 
		if (!string.IsNullOrEmpty(assemblyName) && 
		    !string.IsNullOrEmpty(typeName)) 
		{ 
			assemblyName = Assembly.GetExecutingAssembly().FullName; 

			Type typeToDeserialize = Type.GetType( String.Format("{0}, {1}", typeName, assemblyName)); 
			
			return typeToDeserialize; 
		}
		
		return null; 
	} 
}
