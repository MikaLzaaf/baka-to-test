﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System;

public class CsvWriter 
{

    private const string DefaultLanguage = "English";
    private const string DialogueID = "DialogueID";

    private const string DialogueDataInResources = "CSV/";
    private const string DialogueDataDirectory = "/MainAssets/Resources/DialogueData/";
    private const string FileType = ".csv";

    private List<string[]> rowData = new List<string[]>();
    private List<int> rowToOverwrite = new List<int>();

    private int lastDataRow = -1;

    private string[][] currentData;

    // Use this for initialization
    //void Start()
    //{
        //Save();
    //}



    public void CheckWhichRowToOverwrite(string[] dialogueIDs, TextAsset dataAsset)
    {
        using (StringReader reader = new StringReader(dataAsset.text))
        {
            CsvParser parser = new CsvParser();

            currentData = parser.Parse(reader);

            lastDataRow = currentData.Length;

            if(lastDataRow == 0)
            {
                return;
            }

            rowToOverwrite = new List<int>();

            for(int row = 1; row < currentData.Length; row++)
            {
                string[] dataRow = currentData[row];

                if(dataRow.Length == 0)
                {
                    break;
                }
               
                for(int i = 0; i < dialogueIDs.Length; i++)
                {
                    if (dataRow[0] == dialogueIDs[i])
                    {
                        rowToOverwrite.Add(row);
                    }
                }
            }
        }
    }


    public void SaveDialogues(string[] dialogueIDs, string[] datas, string fileName)
    {
        // Check which line is last filled
        string path = DialogueDataInResources + fileName;

        TextAsset dataAsset = Resources.Load<TextAsset>(path);

        if(dataAsset != null)
        {
            CheckWhichRowToOverwrite(dialogueIDs, dataAsset);
        }
        else
        {
            lastDataRow = 0;
        }


        if(lastDataRow == 0)
        {
            // Meaning no data is available, should create a new one
            // Creating First row of titles manually..
            string[] headers = new string[2];
            headers[0] = DialogueID;
            headers[1] = DefaultLanguage;
            rowData.Add(headers);

            // Add next row of data
            for (int i = 0; i < dialogueIDs.Length; i++)
            {
                string[] dataRow = new string[headers.Length];
                dataRow[0] = dialogueIDs[i]; 
                dataRow[1] = datas[i]; 

                rowData.Add(dataRow);
            }

            string[][] output = new string[rowData.Count][];

            for (int i = 0; i < output.Length; i++)
            {
                output[i] = rowData[i];
            }

            int length = output.GetLength(0);
            string delimiter = ",";

            StringBuilder sb = new StringBuilder();

            for (int index = 0; index < length; index++)
                sb.AppendLine(string.Join(delimiter, output[index]));


            string filePath = Application.dataPath + DialogueDataDirectory + fileName + FileType;

            StreamWriter outStream = System.IO.File.CreateText(filePath);
            outStream.WriteLine(sb);
            outStream.Close();
        }
        else
        {
            List<string> newDialogues = CreateNewList(dialogueIDs);

            List<string> newDatas = CreateNewList(datas);

            if (rowToOverwrite.Count > 0)
            {
                // Perlu overwrite
                for (int i = 0; i < rowToOverwrite.Count; i++)
                {
                    string[] dataRow = currentData[rowToOverwrite[i]];
                    dataRow[0] = dialogueIDs[i];
                    dataRow[1] = datas[i];

                    currentData[rowToOverwrite[i]] = dataRow;

                    newDialogues.Remove(dialogueIDs[i]);
                    newDatas.Remove(datas[i]);
                }
            }

            string[][] output = null;

            // If has a new dialogue to add
            if (newDatas.Count > 0 && newDialogues.Count > 0)
            {
                rowData.Clear();

                for (int row = 0; row < currentData.Length; row++)
                {
                    string[] currentDataRow = currentData[row];

                    if (currentDataRow.Length != 0)
                    {
                        rowData.Add(currentData[row]);
                    }
                }

                for (int i = 0; i < newDialogues.Count; i++)
                {
                    string[] dataRow = new string[2];
                    dataRow[0] = newDialogues[i];
                    dataRow[1] = newDatas[i];

                    rowData.Add(dataRow);
                }

                output = new string[rowData.Count][];

                for (int i = 0; i < output.Length; i++)
                {
                    output[i] = rowData[i];
                }
            }
            else
            {
                output = currentData;
            }

            int length = output.GetLength(0);
            string delimiter = ",";

            StringBuilder sb = new StringBuilder();

            for (int index = 0; index < length; index++)
                sb.AppendLine(string.Join(delimiter, output[index]));

            string filePath = Application.dataPath + DialogueDataDirectory + fileName + FileType;

            StreamWriter outStream = System.IO.File.CreateText(filePath);
            outStream.WriteLine(sb);
            outStream.Close();
        }
    }


    private List<string> CreateNewList(string[] list)
    {
        List<string> newList = new List<string>();

        for (int j = 0; j < list.Length; j++)
        {
            newList.Add(list[j]);
        }

        return newList;
    }


    public void Save(string[] datas)
    {
        // Creating First row of titles manually..
        string[] headers = new string[1];
        headers[0] = "Key";
        //rowDataTemp[1] = "ID";
        //rowDataTemp[2] = "Income";
        rowData.Add(headers);

        // You can add up the values in as many cells as you want.
        for (int i = 0; i < datas.Length; i++)
        {
            string[] dataRow = new string[1];
            dataRow[0] = datas[i] + i; // name
            dataRow[1] = datas[i]; // ID
            //rowDataTemp[2] = "$" + UnityEngine.Random.Range(5000, 10000); // Income
            rowData.Add(dataRow);
        }

        string[][] output = new string[rowData.Count][];

        for (int i = 0; i < output.Length; i++)
        {
            output[i] = rowData[i];
        }

        int length = output.GetLength(0);
        string delimiter = ",";

        StringBuilder sb = new StringBuilder();

        for (int index = 0; index < length; index++)
            sb.AppendLine(string.Join(delimiter, output[index]));


        string filePath = getPath();

        StreamWriter outStream = System.IO.File.CreateText(filePath);
        outStream.WriteLine(sb);
        outStream.Close();


        // Example #4: Append new text to an existing file.
        // The using statement automatically flushes AND CLOSES the stream and calls 
        // IDisposable.Dispose on the stream object.
        //using (System.IO.StreamWriter file =
        //    new System.IO.StreamWriter(@"C:\Users\Public\TestFolder\WriteLines2.txt", true))
        //{
        //    file.WriteLine("Fourth line");
        //}
    }


    // Following method is used to retrive the relative path as device platform
    private string getPath()
    {
#if UNITY_EDITOR
        return Application.dataPath + "/Resources/CSV/" + "Saved_data.csv";
#elif UNITY_ANDROID
        return Application.persistentDataPath+"Saved_data.csv";
#elif UNITY_IPHONE
        return Application.persistentDataPath+"/"+"Saved_data.csv";
#else
        return Application.dataPath +"/"+"Saved_data.csv";
#endif
    }
}

