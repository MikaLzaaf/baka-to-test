﻿using UnityEngine;
using System.Collections;

public abstract class ViewTransition : MonoBehaviour
{
	public ViewController viewController { get; protected set; }

	public bool isTransitioning { get; protected set; }


	protected bool shouldResetTransitionBeforeReady = true;
	protected IEnumerator transitioningViewReadyCoroutine;
	protected IEnumerator transitioningViewClosedCoroutine;


	public abstract IEnumerator TransitioningViewReady();	
	public abstract IEnumerator TransitioningViewClosed();


	public void StartTransitionViewReady()
	{
		transitioningViewReadyCoroutine = TransitioningViewReady();
		StartCoroutine(transitioningViewReadyCoroutine);
	}


	public void StartTransitionViewClosed()
	{
		transitioningViewClosedCoroutine = TransitioningViewClosed();
		StartCoroutine(transitioningViewClosedCoroutine);
	}


	public virtual void Initialize(ViewController viewController)
	{
		this.viewController = viewController;

		shouldResetTransitionBeforeReady = true;
	}


	protected void ResetTransitionBeforeReady()
	{
		if (!shouldResetTransitionBeforeReady)
		{
			return;
		}

		shouldResetTransitionBeforeReady = false;

		ResetTransition();
	}


	protected virtual void ResetTransition()
	{
	}


	public void SetResetTransitionBeforeReady()
	{
		shouldResetTransitionBeforeReady = true;
	}


	public virtual void StopTransition()
	{
		if (transitioningViewReadyCoroutine != null)
		{
			StopCoroutine(transitioningViewReadyCoroutine);
			transitioningViewReadyCoroutine = null;
		}

		if (transitioningViewClosedCoroutine != null)
		{
			StopCoroutine(transitioningViewClosedCoroutine);
			transitioningViewClosedCoroutine = null;
		}
	}
}
