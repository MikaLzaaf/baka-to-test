using UnityEngine;
using System;
using System.Collections;

public class ViewController : MonoBehaviour
{
	
	#region Events

	public event Action ViewStartShowingEvent;
	public event Action ViewReadyEvent;
	public event Action ViewStartClosingEvent;
	public event Action ViewClosedEvent;
	
	#endregion
	
	
	
	#region Public Fields
	
	public Transform view;
	
	/// <summary>
	/// Indicates whether the view has transition or not when it is showing or closing.
	/// </summary>
	public bool hasViewTransition = true;
	
	/// <summary>
	/// Indicates whether the view is Ready initially or not.
	/// </summary>
	public bool isViewReadyInitially = false;

	public bool needToActivateDeactivateView = true;

	public bool interruptibleViewTransition = false;
	public ViewTransition[] viewTransitions;

	public bool autoClose = false;
	public float autoCloseDelay = 1;
	public bool autoCloseDelayInRealtime = false;

	#endregion
	
	
	
	#region Properties
	
	// The default view state is Closed
	private ViewState _viewState = ViewState.Closed;
	public ViewState viewState
	{
		get
		{
			return _viewState;
		}
		protected set
		{
			_viewState = value;	
		}
	}


	private bool _isLocked = false;
	public bool isLocked
	{
		get
		{
			return _isLocked;	
		}
		set
		{
			if (_isLocked != value)
			{
				_isLocked = value;
				
				OnViewLockStateChanged();
			}
		}
	}


	#endregion


	#region Protected Fields

	protected bool isInitialized = false;

	protected IEnumerator showingViewCoroutine;
	protected IEnumerator closingViewCoroutine;
	protected IEnumerator transitioningViewReadyCoroutine;
	protected IEnumerator transitioningViewClosedCoroutine;
	protected IEnumerator autoCloseCoroutine;

	#endregion
	
	

	#region MonoBehaviour Functions
	
	protected virtual void Awake()
	{
		// Set the view state to Ready if it is already Ready initially
		if (isViewReadyInitially)
		{
			viewState = ViewState.Ready;	
		}

		if (!isInitialized)
		{
			Initialize();
		}
	}
	
	#endregion
	
	
	
	#region Functions

	protected virtual void Initialize()
	{
		isInitialized = true;

		viewTransitions = GetComponents<ViewTransition>();

		if (viewTransitions != null)
		{
			foreach (ViewTransition transition in viewTransitions)
			{
				transition.Initialize(this);
			}
		}
	}


	public virtual void Show()
	{
		// Check whether the view can be showed or not
		if (!CanShowView())
		{
			return;
		}		
		
		// Make sure that this game object is active so that we can start coroutine
		this.gameObject.SetActive(true);		

		if (!isInitialized)
		{
			Initialize();
		}

		if (interruptibleViewTransition && viewState == ViewState.TransitioningClosed)
		{
			StopClosingView();
		}
		if (!gameObject.activeInHierarchy)
			Debug.Break();
		showingViewCoroutine = ShowingView();
		StartCoroutine(showingViewCoroutine);
	}
	
	
	public virtual void Close()
	{
		// Check whether the view can be closed or not
		if (!CanCloseView())
		{
			return;	
		}		
		
		// Make sure that this game object is active so that we can start coroutine
		this.gameObject.SetActive(true);		

		if (interruptibleViewTransition && viewState == ViewState.TransitioningReady)
		{
			StopShowingView();
		}
		if (!gameObject.activeInHierarchy)
			Debug.Break();
		closingViewCoroutine = ClosingView();
		StartCoroutine(closingViewCoroutine);
	}	

	
	public virtual void ShowImmediately()
	{
		ActivateView();

		
		// The view is Ready now
		viewState = ViewState.Ready;
		
		
		OnViewReady();		
	}
	
	
	public virtual void CloseImmediately()
	{
		// The view is Closed now
		viewState = ViewState.Closed;

		OnViewClosed();
		
		DeactivateView();	


		if (viewTransitions != null)
		{
			foreach (ViewTransition transition in viewTransitions)
			{
				transition.SetResetTransitionBeforeReady();
			}
		}
	}

	
	public virtual bool CanShowView()
	{
		if (viewState == ViewState.Ready || viewState == ViewState.TransitioningReady)
		{
			return false;	
		}

		if (viewState == ViewState.TransitioningClosed && !interruptibleViewTransition)
		{
			return false;
		}
		
		return true;
	}
	
	
	public virtual bool CanCloseView()
	{
		if (viewState == ViewState.Closed || viewState == ViewState.TransitioningClosed)
		{
			return false;	
		}

		if (viewState == ViewState.TransitioningReady && !interruptibleViewTransition)
		{
			return false;
		}
		
		return true;
	}
	
	
	protected virtual void ActivateView()
	{
		if (needToActivateDeactivateView)
		{
			if (view != null)
			{
				view.gameObject.SetActive(true);	
			}
		}
	}
	
	
	protected virtual void DeactivateView()
	{
		if (needToActivateDeactivateView)
		{
			if (view != null)
			{
				view.gameObject.SetActive(false);	
			}
		}
	}	


	public void StopShowingView()
	{
		if (showingViewCoroutine != null)
		{
			StopCoroutine(showingViewCoroutine);
			showingViewCoroutine = null;
		}

		if (transitioningViewReadyCoroutine != null)
		{
			StopCoroutine(transitioningViewReadyCoroutine);
			transitioningViewReadyCoroutine = null;

			StopViewTransitions();
		}
	}


	public void StopClosingView()
	{
		if (closingViewCoroutine != null)
		{
			StopCoroutine(closingViewCoroutine);
			closingViewCoroutine = null;
		}

		if (transitioningViewClosedCoroutine != null)
		{
			StopCoroutine(transitioningViewClosedCoroutine);
			transitioningViewClosedCoroutine = null;

			StopViewTransitions();
		}
	}


	protected void StopViewTransitions()
	{
		if (viewTransitions != null && viewTransitions.Length > 0)
		{
			foreach (ViewTransition transition in viewTransitions)
			{
				transition.StopTransition();
			}
		}
	}
		

	protected virtual IEnumerator ShowingView()
	{
		RaiseViewStartShowingEvent();


		ActivateView();
		
		
		// Transition the view
		if (hasViewTransition)
		{			
			// The view is Transitioning now
			viewState = ViewState.TransitioningReady;

			// Lock the view
			isLocked = true;

			// Wait until the transition is over
			transitioningViewReadyCoroutine = TransitioningViewReady();
			yield return StartCoroutine(transitioningViewReadyCoroutine);

			// Unlock the view 
			isLocked = false;
		}
		
		
		// The view is Ready now
		viewState = ViewState.Ready;
		
		
		OnViewReady();


		showingViewCoroutine = null;
		transitioningViewReadyCoroutine = null;
	}
	
	
	protected virtual IEnumerator ClosingView()
	{		
		RaiseViewStartClosingEvent();


		// Transition the view
		if (hasViewTransition)
		{
			// The view is Transitioning now
			viewState = ViewState.TransitioningClosed;

			// Lock the view 
			isLocked = true;	

			// Wait until the transition is over
			transitioningViewClosedCoroutine = TransitioningViewClosed();
			yield return StartCoroutine(transitioningViewClosedCoroutine);

			// Unlock the view 
			isLocked = false;
		}
		

		// The view is Closed now
		viewState = ViewState.Closed;
		
		
		OnViewClosed();
		
		
		DeactivateView();


		closingViewCoroutine = null;
		transitioningViewClosedCoroutine = null;
	}	


	protected virtual IEnumerator TransitioningViewReady()
	{
		if (viewTransitions != null && viewTransitions.Length > 0)
		{
			foreach (ViewTransition transition in viewTransitions)
			{
				transition.StartTransitionViewReady();
			}

			bool isTransitioning = true;

			while (isTransitioning)
			{
				isTransitioning = false;

				foreach (ViewTransition transition in viewTransitions)
				{
					if (transition.isTransitioning)
					{
						isTransitioning = true;
						break;
					}
				}

				yield return null;
			}
		}
		else
		{
			yield return null;	
		}
	}
	
	
	protected virtual IEnumerator TransitioningViewClosed()
	{
		if (viewTransitions != null && viewTransitions.Length > 0)
		{
			foreach (ViewTransition transition in viewTransitions)
			{
				transition.StartTransitionViewClosed();
			}
			
			bool isTransitioning = true;
			
			while (isTransitioning)
			{
				isTransitioning = false;
				
				foreach (ViewTransition transition in viewTransitions)
				{
					if (transition.isTransitioning)
					{
						isTransitioning = true;
						break;
					}
				}
				
				yield return null;
			}
		}
		else
		{
			yield return null;	
		}
	}	
	
	
	protected virtual void OnViewLockStateChanged()
	{	
	}


	protected virtual void OnViewReady()
	{
		RaiseViewReadyEvent();

		if (autoClose)
		{
			autoCloseCoroutine = AutoClose(autoCloseDelay, autoCloseDelayInRealtime);
			StartCoroutine(autoCloseCoroutine);
		}
	}


	protected virtual void OnViewClosed()
	{
		RaiseViewClosedEvent();
	}


	private IEnumerator AutoClose(float delay, bool delayInRealtime)
	{
		if (delayInRealtime)
		{
			yield return new WaitForSecondsRealtime(delay);
		}
		else
		{
			yield return new WaitForSeconds(delay);
		}

		Close();
	}


	protected void RaiseViewStartShowingEvent()
	{
		Action handler = ViewStartShowingEvent;
		
		if (handler != null)
		{
			handler();	
		}
	}

	
	protected void RaiseViewReadyEvent()
	{
		Action handler = ViewReadyEvent;
		
		if (handler != null)
		{
			handler();	
		}
	}


	protected void RaiseViewStartClosingEvent()
	{
		Action handler = ViewStartClosingEvent;
		
		if (handler != null)
		{
			handler();	
		}
	}
	
	
	protected void RaiseViewClosedEvent()
	{
		Action handler = ViewClosedEvent;
		
		if (handler != null)
		{
			handler();
		}
	}	

	
    public IEnumerator WaitForReady()
    {
        while (viewState != ViewState.Ready) 
		{
            yield return null;
        }
		
        yield break;
    }
	
	
    public IEnumerator WaitForClosed()
    {
        while (viewState != ViewState.Closed) 
		{
            yield return null;
        }
		
        yield break;
    }	
	
	#endregion
	
}
