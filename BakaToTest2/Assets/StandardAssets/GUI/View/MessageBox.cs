﻿using UnityEngine;
using System;
using System.Collections;

public abstract class MessageBox : UIViewController
{
	private const string MessageBoxPrefabInResources = "UI/MessageBox";

	private static MessageBox _instance;
	public static MessageBox instance 
	{
		get 
		{
			if (_instance == null)
			{
				_instance = GameObject.FindObjectOfType<MessageBox>();

				if (_instance == null)
				{
					GameObject prefab = Resources.Load<GameObject>(MessageBoxPrefabInResources);

					GameObject newInstance = (GameObject)Instantiate(prefab);

					_instance = newInstance.GetComponent<MessageBox>();
				}
			}

			return _instance;
		}
	}

	
	public GameObject buttonOk;
	public GameObject buttonYes;
	public GameObject buttonNo;
	public GameObject buttonCancel;
	public GameObject buttonClose;
	
	public bool closeViewOnButtonSelected = true;



	public object contentTag { get; set; }


	
	protected string messageContent;
	protected MessageBoxButtons buttonsType;

	protected Action<MessageBoxResult> resultHandler;
	


	protected virtual void OnEnable()
	{
		GameObject[] buttons = { buttonOk, buttonYes, buttonNo, buttonCancel, buttonClose };

		foreach (GameObject button in buttons)
		{
			if (button != null)
			{
				AttachButtonEventHandler(button);
			}
		}
	}
	
	
	protected virtual void OnDisable()
	{
		GameObject[] buttons = { buttonOk, buttonYes, buttonNo, buttonCancel, buttonClose };
		
		foreach (GameObject button in buttons)
		{
			if (button != null)
			{
				DetachButtonEventHandler(button);
			}
		}	
	}
	
	public void Show(string content, MessageBoxButtons buttonsType, Action<MessageBoxResult> resultHandler)
	{
		// Set the content
		messageContent = content;		
		
		// Set the buttonsType
		this.buttonsType = buttonsType;


		this.resultHandler = resultHandler;
		// Show the view
		Show();
	}
	
	
	public void ChangeContentAndButtons(string content, MessageBoxButtons buttonsType, Action<MessageBoxResult> resultHandler)
	{
		// Set the content
		messageContent = content;		
		
		// Set the buttonsType
		this.buttonsType = buttonsType;		

		this.resultHandler = resultHandler;
		
		UpdateContentDisplay();
		UpdateButtonDisplay();
	}
	
	
	protected override void ActivateView()
	{
		base.ActivateView();
		
		UpdateContentDisplay();
		UpdateButtonDisplay();
	}
	
	
	protected void ToggleButton(GameObject button, bool visible)
	{
		if (button != null)
		{
			button.SetActive(visible);	
		}
	}
	
	
	protected void UpdateButtonDisplay()
	{	
		// Toggle the buttons based on the buttonsType
		
		// Don't toggle the buttons if the view is closed already
		if (view.gameObject.activeInHierarchy)
		{	
			switch (buttonsType)
			{
			case MessageBoxButtons.None:
				ToggleButton(buttonOk, false);
				ToggleButton(buttonYes, false);
				ToggleButton(buttonNo, false);
				ToggleButton(buttonCancel, false);
				break;			
				
			case MessageBoxButtons.Ok:
				ToggleButton(buttonOk, true);
				ToggleButton(buttonYes, false);
				ToggleButton(buttonNo, false);
				ToggleButton(buttonCancel, false);
				break;
				
			case MessageBoxButtons.YesNo:
				ToggleButton(buttonOk, false);
				ToggleButton(buttonYes, true);
				ToggleButton(buttonNo, true);
				ToggleButton(buttonCancel, false);
				break;
				
			case MessageBoxButtons.Cancel:
				ToggleButton(buttonOk, false);
				ToggleButton(buttonYes, false);
				ToggleButton(buttonNo, false);
				ToggleButton(buttonCancel, true);
				break;			
			}	
		}
	}


	protected abstract string GetContentDisplayText();


	protected abstract void UpdateContentDisplay();

	
	protected abstract void AttachButtonEventHandler(GameObject button);
	
	
	protected abstract void DetachButtonEventHandler(GameObject button);
	
	
	protected void RaiseMessageBoxButtonSelectedEvent(MessageBoxResult result)
	{
		if (resultHandler != null)
		{
			resultHandler(result);
		}
	}

	
	public virtual void OnButtonClick(GameObject sender)
	{
		if (isLocked)
		{
			return;	
		}
		
		
		if (sender == buttonClose)
		{
			Close();
			return;
		}
		
		
		MessageBoxResult result = MessageBoxResult.None;
		
		if (sender == buttonOk)
		{
			result = MessageBoxResult.Ok;
		}
		else if (sender == buttonYes)
		{
			result = MessageBoxResult.Yes;
		}
		else if (sender == buttonNo)
		{
			result = MessageBoxResult.No;
		}
		else if (sender == buttonCancel)
		{
			result = MessageBoxResult.Cancel;
		}		
		
		
		RaiseMessageBoxButtonSelectedEvent(result);
		
		if (closeViewOnButtonSelected)
		{
			Close();	
		}
	}
}
