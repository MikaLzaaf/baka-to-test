﻿using UnityEngine;
using System.Collections;

public class UIViewSingleton<T> : UIViewController where T : UIViewController
{
	protected static T _instance;
	
	private static object _lock = new object();
	
	
	public static T instance
	{
		get
		{
			lock (_lock)
			{
				if (_instance == null)
				{
					_instance = (T)FindObjectOfType(typeof(T));
				}
				
				return _instance;
			}
		}
	}


	protected override void ActivateView()
	{
		base.ActivateView();

		if (_instance == null)
		{
			_instance = this as T;
		}
	}
}
