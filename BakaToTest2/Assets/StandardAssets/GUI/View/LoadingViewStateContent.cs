﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LoadingViewStateContent : MonoBehaviour
{
	public LoadingState state;
	public Text textMessage;


	private string _message;
	public string message
	{
		get
		{
			return _message;
		}
		set 
		{
			_message = value;

			if (textMessage != null)
			{
				textMessage.text = _message;
			}
		}
	}


	public void ToggleContent(bool active)
	{
		this.gameObject.SetActive(active);
	}
}
