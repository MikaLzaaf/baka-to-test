﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UIViewManager : Singleton<UIViewManager>
{
	public UIViewController startupViewController;


	private List<UIViewController> viewControllerStacks = new List<UIViewController>();


	private void Start()
	{
		if (startupViewController != null)
		{
			SwitchView(startupViewController);
		}
	}


	public UIViewController GetPreviousView()
	{
		if (viewControllerStacks.Count > 1)
		{
			return viewControllerStacks[viewControllerStacks.Count - 2];
		}
		
		return null;
	}


	public UIViewController GetCurrentView()
	{
		if (viewControllerStacks.Count > 0)
		{
			return viewControllerStacks[viewControllerStacks.Count - 1];
		}
		
		return null;
	}


	public void SwitchView(UIViewController viewController, bool closeCurrentView = true)
	{
		if (!viewController.CanShowView())
		{
			return;
		}


		UIViewController currentViewController = GetCurrentView();

		if (currentViewController != null)
		{
			if (closeCurrentView)
			{
				if (!currentViewController.CanCloseView())
				{
					return;
				}

				currentViewController.Close();
			}
		}


		viewController.Show();
		
		viewControllerStacks.Add(viewController);
	}


	public void SwitchToPreviousView()
	{
		if (viewControllerStacks.Count < 2)
		{
			return;
		}


		UIViewController previousViewController = GetPreviousView();

		if (previousViewController != null && !previousViewController.CanShowView())
		{
			return;
		}


		UIViewController currentViewController = GetCurrentView();
		
		if (currentViewController != null && !currentViewController.CanCloseView())
		{
			return;
		}
			

		currentViewController.Close();
			
		viewControllerStacks.Remove(currentViewController);

		previousViewController.Show();
	}
}
