﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UISequentialViewsController : UIViewController
{
    public ViewController[] viewControllers;


    private int currentViewIndex = 0;
    private ViewController currentViewController
    {
        get
        {
            if (currentViewIndex < 0 || currentViewIndex >= viewControllers.Length)
            {
                return null;
            }

            return viewControllers[currentViewIndex];
        }
    }


    public override void Show()
    {
        base.Show();

        currentViewIndex = 0;
        currentViewController.Show();
    }


    protected override void DeactivateView()
    {
        base.DeactivateView();

        for (int i = 0; i < viewControllers.Length; i++)
        {
            viewControllers[i].CloseImmediately();
        }
    }


    public void SwitchNextView()
    {
        if (currentViewController == null)
        {
            return;
        }

        if (!currentViewController.CanCloseView())
        {
            return;
        }


        bool hasNextView = currentViewIndex < viewControllers.Length - 1;
        if (!hasNextView)
        {
            return;
        }

        var nextViewController = viewControllers[currentViewIndex + 1];

        if (!nextViewController.CanShowView())
        {
            return;
        }


        currentViewController.Close();
        nextViewController.Show();

        currentViewIndex += 1;
    }
}
