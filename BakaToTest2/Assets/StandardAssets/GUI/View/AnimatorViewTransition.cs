﻿using UnityEngine;
using System.Collections;

public class AnimatorViewTransition : ViewTransition
{
	public Animator animator;
	public string readyStateName;
	public string closeStateName;
	public float crossFadeDuration = 0;

	
	public override IEnumerator TransitioningViewReady()
	{
		return StartTransition(readyStateName);
	}
	
	
	public override IEnumerator TransitioningViewClosed()
	{
		return StartTransition(closeStateName);
	}


	private IEnumerator StartTransition(string stateName)
	{
		isTransitioning = true;

		if (!string.IsNullOrEmpty(stateName))
		{
			if (crossFadeDuration <= 0)
			{
				animator.Play(stateName);
			}
			else
			{
				animator.CrossFade(stateName, crossFadeDuration);
			}

			// Wait for 1 frame so that the normalizedTime will be reset
			yield return null;

			while (animator.GetCurrentAnimatorStateInfo(0).normalizedTime < 1)
			{
				yield return null;
			}
		}

		isTransitioning = false;
	}
}
