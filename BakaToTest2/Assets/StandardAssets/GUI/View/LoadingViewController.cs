﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class LoadingViewController : UIViewController 
{
	public bool autoCloseForSucceededState = false;
	public bool autoCloseForFailedState = false;

	public List<LoadingViewStateContent> stateContents;


	private LoadingState _state;
	public LoadingState state 
	{
		get 
		{
			return _state;
		}
		set
		{
			_state = value;

			foreach (var stateContent in stateContents)
			{
				if (stateContent.state == _state)
				{
					stateContent.ToggleContent(true);
				}
				else
				{
					stateContent.ToggleContent(false);
				}
			}
		}
	}



	public void Show(LoadingState state)
	{
		this.state = state;
	
		Show();
	}


	public void ChangeState(LoadingState state)
	{
		this.state = state;

		bool shouldAutoClose = false;

		if (state == LoadingState.Succeeded && autoCloseForSucceededState)
		{
			shouldAutoClose = true;
		}
		else if (state == LoadingState.Failed && autoCloseForFailedState)
		{
			shouldAutoClose = true;
		}

		if (shouldAutoClose)
		{
			StartCoroutine(this.WaitFor(autoCloseDelay, delegate {
				Close();
			}));
		}
	}
}


public enum LoadingState
{
    Loading,
    Succeeded,
    Failed
}
