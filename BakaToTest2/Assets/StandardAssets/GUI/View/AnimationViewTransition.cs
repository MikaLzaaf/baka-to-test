﻿using UnityEngine;
using System.Collections;

public class AnimationViewTransition : ViewTransition
{
	public Animation viewAnimation;
	public AnimationClip viewReadyClip;
	public AnimationClip viewClosedClip;
	
	
	public override IEnumerator TransitioningViewReady()
	{
		viewAnimation.Play(viewReadyClip.name);
		
		while (viewAnimation.isPlaying)
		{
			yield return null;	
		}
	}
	
	
	public override IEnumerator TransitioningViewClosed()
	{
		viewAnimation.Play(viewClosedClip.name);
		
		while (viewAnimation.isPlaying)
		{
			yield return null;	
		}
	}	
}
