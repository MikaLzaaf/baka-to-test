﻿public enum MessageBoxResult
{
	None,
	Ok,
	Yes,
	No,
	Cancel
}