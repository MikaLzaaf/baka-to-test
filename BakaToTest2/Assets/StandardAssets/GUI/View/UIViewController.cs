﻿using UnityEngine;
using System.Collections;

public class UIViewController : ViewController
{
	public CanvasGroup viewCanvasGroup;
	public bool setAsLastSiblingOnActivated = false;

    public AudioClip viewShowedSound;
    public AudioClip viewClosedSound;

	public Canvas attachedCanvas;
    public Transform content;

	public bool interactable
	{
		get
		{
			if (viewCanvasGroup != null)
			{
				return viewCanvasGroup.interactable;
			}

			return true;
		}
		set
		{
			if (viewCanvasGroup != null)
			{
				viewCanvasGroup.interactable = value;
			}
		}
	}

	public int attachedCanvasSortOrder
	{
		get
		{
			if (attachedCanvas != null)
			{
				return attachedCanvas.sortingOrder;
			}

			return -1;
		}
		set
		{
			if (attachedCanvas != null)
			{
				attachedCanvas.sortingOrder = value;
			}
		}
	}

	public bool canProcessInput => viewState == ViewState.Ready;


	protected override void OnViewLockStateChanged()
	{
		base.OnViewLockStateChanged();

		if (viewCanvasGroup != null)
		{
			interactable = !isLocked;
		}
	}


	protected override void ActivateView()
	{
		base.ActivateView();

		if (setAsLastSiblingOnActivated)
		{
			transform.SetAsLastSibling();
		}
	}


    public override void Show()
    {
        if (!CanShowView())
        {
            return;
        }   

        base.Show();

        if (viewShowedSound != null)
        {
            //AudioManager.PlaySoundOneShot(viewShowedSound);
        }
    }


    public override void Close()
    {
        if (!CanCloseView())
        {
            return;
        }  

        base.Close();

        if (viewClosedSound != null)
        {
            //AudioManager.PlaySoundOneShot(viewClosedSound);
        }
    }
}
