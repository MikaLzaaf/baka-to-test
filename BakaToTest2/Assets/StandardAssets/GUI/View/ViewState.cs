using UnityEngine;
using System.Collections;

public enum ViewState
{
	Ready,
	Closed,
	TransitioningReady,
	TransitioningClosed
}
