using UnityEngine;
using System.Collections;
using DG.Tweening;

public class DoSlideViewTransition : DoTweenViewTransition
{
	public Transform target;

	public Vector3 viewReadyPositionOffset;
	public Vector3 viewClosedPositionOffset;


	private Tween tween;
	private Vector3 initialViewPosition;


	public override void Initialize (ViewController viewController)
	{
		base.Initialize(viewController);

		if (target == null)
		{
			target = viewController.view;
		}

		initialViewPosition = target.localPosition;	
	}


	protected override void ResetTransition()
	{
		base.ResetTransition();

		target.localPosition = initialViewPosition + viewReadyPositionOffset;
	}


	public override void StopTransition()
	{
		base.StopTransition();

		if (tween != null)
		{
			tween.Kill();
		}
	}


	public override IEnumerator TransitioningViewReady()
	{
		isTransitioning = true;

		ResetTransitionBeforeReady();

		Vector3 targetViewPosition = initialViewPosition;		

		tween = 
			target.DOLocalMove(targetViewPosition, transitionReadyDuration).
				SetEase(transitionReadyEaseType).
				SetUpdate(updateType, isIndependentUpdate);
		
		yield return tween.WaitForCompletion();

		isTransitioning = false;
	}
	
	
	public override IEnumerator TransitioningViewClosed()
	{
		isTransitioning = true;

		Vector3 targetViewPosition = initialViewPosition + viewClosedPositionOffset;

		tween = 
			target.DOLocalMove(targetViewPosition, transitionClosedDuration).
				SetEase(transitionClosedEaseType).
				SetUpdate(updateType, isIndependentUpdate);
		
		yield return tween.WaitForCompletion();

		shouldResetTransitionBeforeReady = true;

		isTransitioning = false;
	}
}
