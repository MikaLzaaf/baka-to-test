﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Reflection;
using DG.Tweening;

public class DoFadeCanvasTransition : DoTweenViewTransition
{
	public CanvasGroup fadeTarget;

	private Tween tween;


	public override void Initialize (ViewController viewController)
	{
		base.Initialize(viewController);
	}


	protected override void ResetTransition()
	{
		base.ResetTransition();

		fadeTarget.alpha = 0;
	}


	public override void StopTransition()
	{
		base.StopTransition();

		if (tween != null)
		{
			tween.Kill();
		}
	}


	public override IEnumerator TransitioningViewReady()
	{
		isTransitioning = true;

		ResetTransitionBeforeReady();

		tween =
			fadeTarget.DOFade(1, transitionReadyDuration).
				SetEase(transitionReadyEaseType).
				SetUpdate(updateType, isIndependentUpdate);

		yield return tween.WaitForCompletion();

		isTransitioning = false;
	}
	
	
	public override IEnumerator TransitioningViewClosed()
	{
		isTransitioning = true;

		tween =
			fadeTarget.DOFade(0, transitionClosedDuration).
				SetEase(transitionClosedEaseType).
				SetUpdate(updateType, isIndependentUpdate);
		
		yield return tween.WaitForCompletion();

		shouldResetTransitionBeforeReady = true;

		isTransitioning = false;
	}
}
