using UnityEngine;
using System.Collections;
using DG.Tweening;

public class DoScaleViewTransition : DoTweenViewTransition
{
	public Transform target;

	public Vector3 viewReadyScale = Vector3.zero;
	public Vector3 viewClosedScale = Vector3.zero;


	private Tween tween;
	private Vector3 initialViewScale;


	public override void Initialize (ViewController viewController)
	{
		base.Initialize(viewController);

		if (target == null)
		{
			target = viewController.view;
		}

		initialViewScale = target.localScale;	
	}


	protected override void ResetTransition()
	{
		base.ResetTransition();

		target.localScale = viewReadyScale;
	}


	public override void StopTransition()
	{
		base.StopTransition();

		if (tween != null)
		{
			tween.Kill();
		}
	}


	public override IEnumerator TransitioningViewReady()
	{
		isTransitioning = true;

		ResetTransitionBeforeReady();	

		tween = 
			target.DOScale(initialViewScale, transitionReadyDuration).
			SetEase(transitionReadyEaseType).
			SetUpdate(updateType, isIndependentUpdate);
		
		yield return tween.WaitForCompletion();

		isTransitioning = false;
	}
	
	
	public override IEnumerator TransitioningViewClosed()
	{
		isTransitioning = true;

		tween = 
			target.DOScale(viewClosedScale, transitionClosedDuration).
				SetEase(transitionClosedEaseType).
				SetUpdate(updateType, isIndependentUpdate);
		
		yield return tween.WaitForCompletion();

		shouldResetTransitionBeforeReady = true;

		isTransitioning = false;
	}
}
