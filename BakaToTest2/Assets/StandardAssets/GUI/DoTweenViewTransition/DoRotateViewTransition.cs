using UnityEngine;
using System.Collections;
using DG.Tweening;

public class DoRotateViewTransition : DoTweenViewTransition
{
	public Transform target;

	public Vector3 viewReadyAnglesOffset;
	public Vector3 viewClosedAnglesOffset;


	private Tween tween;
	private Vector3 initialViewAngles;


	public override void Initialize (ViewController viewController)
	{
		base.Initialize(viewController);

		if (target == null)
		{
			target = viewController.view;
		}

        initialViewAngles = target.localEulerAngles;	
	}


	protected override void ResetTransition ()
	{
		base.ResetTransition();

		target.localEulerAngles = initialViewAngles + viewReadyAnglesOffset;
	}


	public override void StopTransition()
	{
		base.StopTransition();

		if (tween != null)
		{
			tween.Kill();
		}
	}


	public override IEnumerator TransitioningViewReady()
	{
		isTransitioning = true;

		ResetTransitionBeforeReady();

        Vector3 targetViewAngles = initialViewAngles;		

		tween = 
            target.DOLocalRotate(targetViewAngles, transitionReadyDuration).
				SetEase(transitionReadyEaseType).
				SetUpdate(updateType, isIndependentUpdate);
		
		yield return tween.WaitForCompletion();

		isTransitioning = false;
	}
	
	
	public override IEnumerator TransitioningViewClosed()
	{
		isTransitioning = true;

        Vector3 targetViewAngles = initialViewAngles + viewClosedAnglesOffset;

		tween = 
            target.DOLocalRotate(targetViewAngles, transitionClosedDuration).
				SetEase(transitionClosedEaseType).
				SetUpdate(updateType, isIndependentUpdate);
		
		yield return tween.WaitForCompletion();

		shouldResetTransitionBeforeReady = true;

		isTransitioning = false;
	}
}
