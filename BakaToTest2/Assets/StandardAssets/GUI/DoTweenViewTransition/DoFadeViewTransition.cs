﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Reflection;
using DG.Tweening;

public class DoFadeViewTransition : DoTweenViewTransition
{
	public Image fadeTarget;


	private Color initialTargetColor;
	private Tween tween;


	public override void Initialize (ViewController viewController)
	{
		base.Initialize(viewController);

		initialTargetColor = fadeTarget.color;	
	}


	protected override void ResetTransition()
	{
		base.ResetTransition();

		Color color = fadeTarget.color;
		color.a = 0;
		fadeTarget.color = color;
	}


	public override void StopTransition()
	{
		base.StopTransition();

		if (tween != null)
		{
			tween.Kill();
		}
	}


	public override IEnumerator TransitioningViewReady()
	{
		isTransitioning = true;

		ResetTransitionBeforeReady();
	
		tween =
			DOTween.To(() => fadeTarget.color,
			           x => fadeTarget.color = x,
			           initialTargetColor,
			           transitionReadyDuration).
				SetEase(transitionReadyEaseType).
				SetUpdate(updateType, isIndependentUpdate);

		yield return tween.WaitForCompletion();

		isTransitioning = false;
	}
	
	
	public override IEnumerator TransitioningViewClosed()
	{
		isTransitioning = true;

		Color color = fadeTarget.color;
		color.a = 0;
		
		tween =
			DOTween.To(() => fadeTarget.color,
			           x => fadeTarget.color = x,
			           color,
			           transitionClosedDuration).
				SetEase(transitionClosedEaseType).
				SetUpdate(updateType, isIndependentUpdate);
		
		yield return tween.WaitForCompletion();

		shouldResetTransitionBeforeReady = true;

		isTransitioning = false;
	}
	
}
