﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public abstract class DoTweenViewTransition : ViewTransition
{
	public float transitionReadyDuration = 0.5f;
	public Ease transitionReadyEaseType = Ease.Linear;
	
	public float transitionClosedDuration = 0.5f;
	public Ease transitionClosedEaseType = Ease.Linear;
	
	public UpdateType updateType = UpdateType.Normal;
	public bool isIndependentUpdate = false;
}
