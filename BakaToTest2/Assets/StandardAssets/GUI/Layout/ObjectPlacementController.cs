using UnityEngine;
using System;
using System.Collections;

/// <summary>
/// It is used to control the relative placement and alignment of GameObject.
/// </summary>
public class ObjectPlacementController : MonoBehaviour 
{
	
	#region Inner Classes
	
	/// <summary>
	/// It defines the space for the sides of an object.
	/// </summary>
	[Serializable]
	public class Margin
	{
		public float left;
		public float top;
		public float right;
		public float bottom;
		
		
		public Margin()
		{
		}
		
		
		public Margin(float left, float top, float right, float bottom)
		{
			this.left = left;
			this.top = top;
			this.right = right;
			this.bottom = bottom;
		}
	}	
	
	#endregion
	
	
	
	#region Inner Enums
	
	public enum HorizontalAlignment
	{
		None,
		Left,
		Center,
		Right,
		Stretch
	}
	
	
	public enum VerticalAlignment
	{
		None,
		Top,
		Center,
		Bottom,
		Stretch
	}	
	
	
	public enum ObjectPlane
	{
		XY,
		XZ
	}
	
	#endregion
	
	
	
	#region Public Fields
	
	public Camera renderCamera;
	
	public string renderCameraTag = "MainCamera";
	
	public HorizontalAlignment horizontalAlignment;
	public VerticalAlignment verticalAlignment;
	
	public Margin margin;
	
	
	/// <summary>
	/// Indicates whether it should constrain its original proportions of size if 
	/// either the horizontal or vertical alignment is set to Stretch.
	/// </summary>
	public bool constrainProportions = true;
	
	
	/// <summary>
	/// The plane that defines the size of this object.
	/// </summary>
	/// <remarks>
	/// We use this value to determine the width and height of this object.
	/// For example, if its plane is XY, then its width is represented by its scale's x,
	/// and its height is represented by its scale's y.
	/// </remarks>
	public ObjectPlane objectPlane = ObjectPlane.XY;
	
	
	/// <summary>
	/// The number of meter in one scale unit.
	/// </summary>
	public float scalePerMeter = 1;

	/// <summary>
	/// Set local position instead of world position.
	/// </summary>
	public bool isRelative = false;
	
	/// <summary>
	/// Indicates whether it should set its placement in each Update.
	/// </summary>
	public bool setPlacementInUpdate = false;
	
	#endregion
	
	
	
	#region Private Fields
	
	private	float originalHorizontalScale;
		
	private	float originalVerticalScale;	
	
	#endregion
	
	
	
	#region MonoBehaviour Functions
	
	private void Awake()
	{
		// Find its render camera
		if (renderCamera == null)
		{
			foreach (Camera camera in Camera.allCameras)
			{
				if (camera.tag == renderCameraTag)
				{
					renderCamera = camera;
					break;
				}
			}
		}
		
		
		// Cache its original size
		originalHorizontalScale = this.transform.localScale.x;
		
		originalVerticalScale = this.transform.localScale.y;
		
		if (objectPlane == ObjectPlane.XZ)
		{
			originalVerticalScale = this.transform.localScale.z;	
		}	


		// Set its placement
		SetPlacement();
	}
	
	
	private void Update()
	{
		if (setPlacementInUpdate)
		{
			SetPlacement();	
		}
	}
	
	#endregion
	
	
	
	#region Instance Functions
	
	private void SetPlacement()
	{
		Transform thisTransform = this.transform;
		
		Vector3 screenBottomLeft = GetScreenBottomLeftWorldPoint(thisTransform.position.z, renderCamera);
		Vector3 screenTopRight = GetScreenTopRightWorldPoint(thisTransform.position.z, renderCamera);		
		
		
		Vector3 placementPosition = new Vector3();

		if (isRelative)
		{
			placementPosition = thisTransform.localPosition;
		}
		else
		{
			placementPosition = thisTransform.position;
		}


		float newHorizontalScale = originalHorizontalScale;
		float newVerticalScale = originalVerticalScale;
		
		
		switch (horizontalAlignment)
		{
			case HorizontalAlignment.Left:
				placementPosition.x =  screenBottomLeft.x + margin.left;
				break;
			
			case HorizontalAlignment.Right:
				placementPosition.x = screenTopRight.x - margin.right;
				break;
			
			case HorizontalAlignment.Center:
				placementPosition.x = (screenBottomLeft.x + screenTopRight.x) / 2f;
				break;
			
			case HorizontalAlignment.Stretch:
				
				// Stretch its horizontal scale
				newHorizontalScale = (screenTopRight.x - screenBottomLeft.x) * scalePerMeter;
				newHorizontalScale -= (margin.left + margin.right);

				placementPosition.x = (screenBottomLeft.x + screenTopRight.x) / 2f;
				placementPosition.x += (margin.left / 2f);
				placementPosition.x -= (margin.right / 2f);

				// If its size proportions needs to be constrained and the vertical alignnment is not Stretch,
				// then we need to update its vertical scale in order to maintain its size proportions
				if (constrainProportions && verticalAlignment != VerticalAlignment.Stretch)
				{
					newVerticalScale = newHorizontalScale * originalVerticalScale / originalHorizontalScale;
				}
			
				break;
		}
		
		
		switch (verticalAlignment)
		{
			case VerticalAlignment.Top:
				placementPosition.y =  screenTopRight.y - margin.top;
				break;
			
			case VerticalAlignment.Center:
				placementPosition.y = (screenTopRight.y + screenBottomLeft.y) / 2f;
				break;
			
			case VerticalAlignment.Bottom:
				placementPosition.y = screenBottomLeft.y + margin.bottom;
				break;
			
			case VerticalAlignment.Stretch:
				// Place it at the center
				placementPosition.y = (screenTopRight.y + screenBottomLeft.y) / 2f;
			
				// Stretch its vertical scale
				newVerticalScale = (screenTopRight.y - screenBottomLeft.y) * scalePerMeter;
			
				// If its size proportions needs to be constrained and the horizontal alignnment is not Stretch,
				// then we need to update its horizontal scale in order to maintain its size proportions
				if (constrainProportions && horizontalAlignment != HorizontalAlignment.Stretch)
				{
					newHorizontalScale = newVerticalScale * originalHorizontalScale / originalVerticalScale;
				}			
			
				break;
		}
		
		
		// Update its position
		if (isRelative)
		{
			thisTransform.localPosition = placementPosition;
		}
		else
		{
			thisTransform.position = placementPosition;
		}

		// Update its scale
		
		Vector3 newScale = thisTransform.localScale;
		newScale.x = newHorizontalScale;
		
		if (objectPlane == ObjectPlane.XZ)
		{
			newScale.z = newVerticalScale;
		}
		else
		{
			newScale.y = newVerticalScale;	
		}
		
		thisTransform.localScale = newScale;
	}
	
	#endregion
	
	
	
	#region Static Functions
	
	public static Vector3 GetScreenBottomLeftWorldPoint(float positionZ, Camera renderCamera)
	{
		float distanceToRenderCamera = positionZ - renderCamera.transform.position.z;

		return renderCamera.ViewportToWorldPoint(new Vector3(0, 0, distanceToRenderCamera));
	}
	
	
	public static Vector3 GetScreenTopRightWorldPoint(float positionZ, Camera renderCamera)
	{
		float distanceToRenderCamera = positionZ - renderCamera.transform.position.z;

		return renderCamera.ViewportToWorldPoint(new Vector3(1, 1, distanceToRenderCamera));
	}
	
	#endregion
	
}
