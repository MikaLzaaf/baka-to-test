﻿using UnityEngine;
using System.Collections;

public abstract class GameEventObject : MonoBehaviour 
{
	public bool playOneTimeOnly = false;
	public bool destroyOnEnded = true;
    public bool playOnStart = false;


    protected virtual void Start()
    {
        if (playOnStart)
        {
            GameEventManager.instance.PlayEvent(this);
        }
    }


    public virtual void OnEventStarted()
	{
	}
	
	
	public virtual void OnEventEnded()
	{
	}

	
	public abstract IEnumerator Play();



	public void PlayEvent()
	{
		GameEventManager.instance.PlayEvent(this);
	}
}
