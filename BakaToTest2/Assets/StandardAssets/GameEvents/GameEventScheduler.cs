﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameEventScheduler : MonoBehaviour 
{
	public GameEventScheduleEntry[] scheduleEntries;


	private List<GameEventScheduleEntry> availableScheduleEntries;


	private void Awake()
	{
		availableScheduleEntries = new List<GameEventScheduleEntry>(scheduleEntries);

		PlayEvents(GameEventPlayTime.Awake);
	}


	private IEnumerator Start()
	{
		PlayEvents(GameEventPlayTime.Start);
		
		yield return new WaitForFixedUpdate();
		
		PlayEvents(GameEventPlayTime.WaitForFixedUpdate);
	}


	private void PlayEvents(GameEventPlayTime playTime)
	{
		for (int i = 0; i < availableScheduleEntries.Count; i++) 
		{
			if (availableScheduleEntries[i].playTime == playTime)
			{
                GameEventManager.instance.PlayEvent(availableScheduleEntries[i].eventName, null);

				availableScheduleEntries.RemoveAt(i);
				i--;
			}
		}
	}
}
