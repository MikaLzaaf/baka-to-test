﻿using UnityEngine;
using System.Collections;

public class GameEventPlayer : MonoBehaviour
{
	public enum PlayTime
	{
		Awake,
		Start,
		WaitForFixedUpdate
	}


	public string eventName;
	public PlayTime playTime;


	private void Awake()
	{
		if (playTime == PlayTime.Awake)
		{
			PlayEvent();
		}
	}


	private IEnumerator Start()
	{
		if (playTime == PlayTime.Start)
		{
			PlayEvent();
			yield break;
		}

		yield return new WaitForFixedUpdate();

		if (playTime == PlayTime.WaitForFixedUpdate)
		{
			PlayEvent();
			yield break;
		}
	}



	private void PlayEvent()
	{
		if (!gameObject.activeInHierarchy)
		{
			return;
		}

		GameEventManager.instance.PlayEvent(eventName, null);
		gameObject.SetActive(false);
	}
}
