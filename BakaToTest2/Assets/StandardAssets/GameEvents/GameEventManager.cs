﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class GameEventManager : Singleton<GameEventManager>
{
	private const string GameEventsDirectoryInResources = "GameEvents";


	private bool _isProcessingEvent = false;
	public bool isProcessingEvent
	{
		get
		{
			return _isProcessingEvent;	
		}
		private set
		{
			_isProcessingEvent = value;	
		}
	}


    private List<GameEventObject> eventsQueue = new List<GameEventObject>();

	private GameEventObject currentEventObject;
	private IEnumerator currentEventCoroutine;
   

	private GameEventObject CreateGameEventObject(string eventName)
	{
		string eventObjectPath = GameEventsDirectoryInResources + "/" + eventName;
		
		GameObject eventGameObject = Resources.Load<GameObject>(eventObjectPath);
		
		GameEventObject eventObject = null;
		
		if (eventGameObject != null)
		{
			eventObject = eventGameObject.GetComponent<GameEventObject>();
		}
		
		if (eventObject == null)
		{
			//Debug.LogError("Cannot load the event object at Resources/" + eventObjectPath);
			return null;
		}

		// If the event should be played for one time only, then we need to check whether
		// it was played before or not
		if (eventObject.playOneTimeOnly && IsEventPlayedBefore(eventName))
		{
			return null;	
		}


		GameEventObject createdEventObject = (GameEventObject)Instantiate(eventObject);
		createdEventObject.name = eventName;

        createdEventObject.transform.SetParent(this.transform);

		return createdEventObject;
	}


	public GameEventObject PlayEvent(string eventName, Action<GameEventObject> eventObjectInitialization, bool playImmediatelyAfterCurrentEvent = false)
	{
        GameEventObject eventObject = CreateGameEventObject(eventName);

        if (eventObject == null)
        {
			return null;
        }

		if (eventObjectInitialization != null)
		{
			eventObjectInitialization(eventObject);
		}

		PlayEvent(eventObject, playImmediatelyAfterCurrentEvent);

		return eventObject;
	}


	public void PlayEvent(GameEventObject eventObject, bool playImmediatelyAfterCurrentEvent = false)
	{
        if (eventsQueue.Contains(eventObject))
        {
            return;
        }

		if (playImmediatelyAfterCurrentEvent)
		{
			eventsQueue.Insert(0, eventObject);
		}
		else
		{
			eventsQueue.Add(eventObject);
		}

		eventObject.transform.SetParent(this.transform);

		StartCoroutine("ProcessingEvents");
	}

	
	private IEnumerator ProcessingEvents()
	{
		if (isProcessingEvent)
		{
			yield break;
		}
		
		isProcessingEvent = true;
		
		while (eventsQueue.Count > 0)
		{
            currentEventObject = eventsQueue[0];
            eventsQueue.RemoveAt(0);

			if (currentEventObject != null)
			{
				// Record this event as played before
                SaveEventRecord(currentEventObject.name);

				currentEventObject.OnEventStarted();

				currentEventCoroutine = currentEventObject.Play();

                yield return StartCoroutine(currentEventCoroutine);	

                if (currentEventObject != null)
                {
    				currentEventObject.OnEventEnded();

					if (currentEventObject.destroyOnEnded)
					{
						Destroy(currentEventObject.gameObject);
					}

					currentEventObject = null;
                }
			}
		}
		
		isProcessingEvent = false;
	}
	
	
	private void SaveEventRecord(string eventName)
	{
		PlayerPrefs.SetInt(eventName, 1);
	}


	public void StopCurrentEvent()
	{
		if (currentEventCoroutine != null)
		{
			StopCoroutine(currentEventCoroutine);

			currentEventCoroutine = null;
        }

        if (currentEventObject != null)
        {
			if (currentEventObject.destroyOnEnded)
			{
				Destroy(currentEventObject.gameObject);
			}

			currentEventObject = null;
        }

        StopProcessingEvents();

        StartCoroutine("ProcessingEvents");
	}


    public void StopProcessingEvents()
    {
        if (isProcessingEvent)
        {
            isProcessingEvent = false;

            StopCoroutine("ProcessingEvents");
        }
    }

	
	public static bool IsEventPlayedBefore(string eventName)
	{
		return PlayerPrefs.HasKey(eventName);	
	}


	public static void DeleteAllEventRecords()
	{
		GameObject[] eventObjects = Resources.LoadAll<GameObject>(GameEventsDirectoryInResources);

		for (int i = 0; i < eventObjects.Length; i++)
		{
			GameObject eventObject = eventObjects[i];

			PlayerPrefs.DeleteKey(eventObject.name);

			eventObjects[i] = null;

			Resources.UnloadAsset(eventObject);
		}
	}
}
