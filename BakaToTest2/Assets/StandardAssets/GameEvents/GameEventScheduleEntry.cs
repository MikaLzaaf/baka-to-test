﻿using UnityEngine;
using System;
using System.Collections;

[Serializable]
public class GameEventScheduleEntry
{
	public string eventName;
	public GameEventPlayTime playTime;
}
